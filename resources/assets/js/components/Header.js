import React from 'react'
import { Link } from 'react-router-dom'
import Modal from 'react-responsive-modal'
import Logo_Background from '../images/logo_curve.png'
import Preview_Background from '../images/header_right_shape.png'
import { connect } from 'react-redux';

const styles = {
    fontFamily: 'sans-serif',
    textAlign: 'center'
}

const logo_bg = {
    backgroundImage: 'url(' + Logo_Background + ')'
}

const preview_bg = {
    backgroundImage: 'url(' + Preview_Background + ')'
}

var stopPreview = 0;


const mapStateToProps = state => {
    return {
        common: state.common
    }};
  
  const mapDispatchToProps = dispatch => ({
  
  });

class Header extends React.Component {

    constructor(props) {
        super(props);
        this.handleMouseHoverPage = this.handleMouseHoverPage.bind(this);
        this.handleMouseHoverSite = this.handleMouseHoverSite.bind(this);

        this.handleMouseHoverCode = this.handleMouseHoverCode.bind(this);
        this.handleMouseHoverHelp = this.handleMouseHoverHelp.bind(this);

        this.handleMouseHoverZoom = this.handleMouseHoverZoom.bind(this);
        this.handleMouseHoverUndo = this.handleMouseHoverUndo.bind(this);
        this.handleMouseHoverRedo = this.handleMouseHoverRedo.bind(this);
        this.handleClickRedo = this.handleClickRedo.bind(this);
        this.handleClickUndo = this.handleClickUndo.bind(this);
        this.handleMouseHoverMobile = this.handleMouseHoverMobile.bind(this);
        this.handleMouseHoverDesktop = this.handleMouseHoverDesktop.bind(this);

        this.switchToMobile = this.switchToMobile.bind(this);
        this.switchToDesktop = this.switchToDesktop.bind(this);

        this.previewWindow = this.previewWindow.bind(this);

        this.handleMouseHoverSave = this.handleMouseHoverSave.bind(this);
        this.handleMouseHoverPreview = this.handleMouseHoverPreview.bind(this);
        this.handleMouseHoverPublish = this.handleMouseHoverPublish.bind(this);

        //     Hover Under Site Hover

        this.handleMouseHoverSite_Preview = this.handleMouseHoverSite_Preview.bind(this);
        this.handleMouseOutSite_Preview = this.handleMouseOutSite_Preview.bind(this);


        this.handleMouseHoverSite_GetFeedback = this.handleMouseHoverSite_GetFeedback.bind(this);
        this.handleMouseOutSite_GetFeedback = this.handleMouseOutSite_GetFeedback.bind(this);

        this.handleMouseHoverSite_Publish = this.handleMouseHoverSite_Publish.bind(this);
        this.handleMouseOutSite_Publish = this.handleMouseOutSite_Publish.bind(this);


        this.handleMouseHoverSite_ViewPublishedSite = this.handleMouseHoverSite_ViewPublishedSite.bind(this);
        this.handleMouseOutSite_ViewPublishedSite = this.handleMouseOutSite_ViewPublishedSite.bind(this);


        this.handleMouseHoverSite_GetFoundOnGoogle = this.handleMouseHoverSite_GetFoundOnGoogle.bind(this);
        this.handleMouseOutSite_GetFoundOnGoogle = this.handleMouseOutSite_GetFoundOnGoogle.bind(this);


        this.handleMouseHoverSite_Accessibility = this.handleMouseHoverSite_Accessibility.bind(this);
        this.handleMouseOutSite_Accessibility = this.handleMouseOutSite_Accessibility.bind(this);


        this.handleMouseHoverSite_ConnectDomain = this.handleMouseHoverSite_ConnectDomain.bind(this);
        this.handleMouseOutSite_ConnectDomain = this.handleMouseOutSite_ConnectDomain.bind(this);


        this.handleMouseHoverSite_Upgrade = this.handleMouseHoverSite_Upgrade.bind(this);
        this.handleMouseOutSite_Upgrade = this.handleMouseOutSite_Upgrade.bind(this);


        this.handleMouseHoverSite_MobileEditor = this.handleMouseHoverSite_MobileEditor.bind(this);
        this.handleMouseOutSite_MobileEditor = this.handleMouseOutSite_MobileEditor.bind(this);


        this.handleMouseHoverSite_ImageSharpening = this.handleMouseHoverSite_ImageSharpening.bind(this);
        this.handleMouseOutSite_ImageSharpening = this.handleMouseOutSite_ImageSharpening.bind(this);


        this.handleMouseHoverSite_MyDashboard = this.handleMouseHoverSite_MyDashboard.bind(this);
        this.handleMouseOutSite_MyDashboard = this.handleMouseOutSite_MyDashboard.bind(this);


        this.handleMouseHoverSite_SiteManager = this.handleMouseHoverSite_SiteManager.bind(this);
        this.handleMouseOutSite_SiteManager = this.handleMouseOutSite_SiteManager.bind(this);


        this.handleMouseHoverSite_SiteHistory = this.handleMouseHoverSite_SiteHistory.bind(this);
        this.handleMouseOutSite_SiteHistory = this.handleMouseOutSite_SiteHistory.bind(this);


        this.handleMouseHoverSite_ExitEditor = this.handleMouseHoverSite_ExitEditor.bind(this);
        this.handleMouseOutSite_ExitEditor = this.handleMouseOutSite_ExitEditor.bind(this);
        this.escFunction = this.escFunction.bind(this);

        this.state = {
            //  Hover Function States
            isHoveringPage: false,
            isHoveringSite: false,

            isHoveringCode: false,
            isHoveringHelp: false,


            //  Hover Icons
            isHoveringZoom: false,
            isHoveringUndo: false,
            isHoveringRedo: false,
            isHoveringMobile: false,
            isHoveringDesktop:false,
            isHoveringSave: false,
            isHoveringPreview: false,
            isHoveringPublish: false,
            isHoveringImageSharpening: false,

            //  Click Function States
            showPageMenu: false,
            showHeader: true,
            showMobileMenu: false,

            //  Modal Function

            openSaveModal: false,
            openPublishModal: false,
            openGetFeedbackModal: false,
            openSiteHistoryModal: false,
            openImageSharpeningModal: false,
            openDomainModal: false,
            domains: [],
            currentPage: 'index',
            selectedDomain:0,
            currentPageIndex:0,
            //  Inside Site Hover
            pages:[],
            imgSrc: require('./../images/filter-logo.png'),
            imgCode: require('./../images/filter-logo.png'),
            imgHelp: require('./../images/filter-logo.png')
        };
        this.showPageMenu = this.showPageMenu.bind(this);
        this.closePageMenu = this.closePageMenu.bind(this);
        this.showHeader = this.showHeader.bind(this);
        this.showMobileMenu = this.showMobileMenu.bind(this);
        this.hideMobileMenu = this.hideMobileMenu.bind(this);
        this.onOpenSaveModal = this.onOpenSaveModal.bind(this);
        this.onCloseSaveModal = this.onCloseSaveModal.bind(this);
        this.onOpenPublishModal = this.onOpenPublishModal.bind(this);
        this.onClosePublishModal = this.onClosePublishModal.bind(this);
        this.onCloseDomainModal= this.onCloseDomainModal.bind(this);
        this.onOpenGetFeedbackModal = this.onOpenGetFeedbackModal.bind(this);
        this.onCloseGetFeedbackModal = this.onCloseGetFeedbackModal.bind(this);
        this.onOpenSiteHistoryModal = this.onOpenSiteHistoryModal.bind(this);
        this.onCloseSiteHistoryModal = this.onCloseSiteHistoryModal.bind(this);
        this.onOpenImageSharpeningModal = this.onOpenImageSharpeningModal.bind(this);
        this.onCloseImageSharpeningModal = this.onCloseImageSharpeningModal.bind(this);
        this.savePage=this.savePage.bind(this);
        this.onExitEditor=this.onExitEditor.bind(this);

    }
    componentWillMount()
	{
        var c_element = this;
        
        setTimeout(function(){
            c_element.setState({
                currentPage: c_element.props.common.projectDetail.currentPage,
                currentPageIndex: c_element.props.common.projectDetail.currentPageIndex
            });
        },1000);
        
    }

    savePage(){
        var html = window.editor.getHtml();
        var css = window.editor.getCss();
        var currentPage = this.state.currentPage;
        var projectId = this.props.common.projectDetail.projectId;
        var currentPageIndex = this.state.currentPageIndex;
        var project = this.props.common.projectDetail.project;
        var pages = project.pages;
        pages[currentPageIndex]['html'] = html;
        
        fetch(window.baseURL+"/api/projects/update-page", {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        credentials: 'include',
        body: JSON.stringify({
            css: css,
            js: project.js,
            framework: project.model.framework,
            name:project.model.name,
            template: project.model.template,
            theme: project.model.theme,
            pages: pages,
            currentPage: currentPage,
            projectId:projectId
        })
        }).then((response) => {
            swal({
                title: "Success!",
                text: "Project saved successfully!",
                icon: "success",
                showConfirmButton: false,
                buttons: false,
                timer: 3000,
              });
            this.setState({
                openSaveModal: false
            });
        });
        
    }
    copyFeedbackLink(){
        var projectDetail = this.props.common.projectDetail;
        var link = window.baseURL+'/get-feedback/'+projectDetail.project.model.name;
        const el = document.createElement('textarea');
        el.value = link;
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
        swal({
            title: "Success!",
            text: "Coppied to clipboard successfully!",
            icon: "success",
            showConfirmButton: false,
            buttons: false,
            timer: 3000,
          });
    }
    connectDomain(){
        var projectId = this.props.common.projectDetail.projectId;
        fetch(window.baseURL+"/api/projects/get-domains", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            credentials: 'include',
            body: JSON.stringify({
                projectId:projectId
            })
            }).then(res => res.json())
            .then(
              (result) => {
                  var selectedDomain = 0;
                result.forEach(function(key,index){
                    if(projectId == key.project_id){
                        selectedDomain = key.id;
                    }
                });
                this.setState({
                    openDomainModal: true,
                    domains:result,
                    selectedDomain:selectedDomain
                });
            });
    }
    connectDomainSubmit(){
        var domainId = document.getElementById("domains_list").value;
        var projectId = this.props.common.projectDetail.projectId;
        fetch(window.baseURL+"/api/projects/set-domain", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            credentials: 'include',
            body: JSON.stringify({
                projectId:projectId,
                domainId:domainId
            })
            }).then(res => res.json())
            .then(
              (result) => {
                swal({
                    title: "Success!",
                    text: "Domain connected successfully!",
                    icon: "success",
                    showConfirmButton: false,
                    buttons: false,
                    timer: 3000,
                  });
                this.setState({
                    domains:result,
                    openDomainModal: false
                });
            });
    }
    onCloseDomainModal(){
        this.setState({
            openDomainModal: false
        });
    }
    onExitEditor(){
        var projectId = this.props.common.projectDetail.projectId;
        window.location.href = window.baseURL+'/admin/panda-pages/'+projectId+'/edit';
    }
    switchPage(index){
       var pages = this.state.pages;
       var c_element = this;
           c_element.setState({
               currentPage: pages[index]['name'],
               currentPageIndex: index
           });
           
           window.editor.setComponents(pages[index]['html']);
    }
    openNewTab(page){
        var win = window.open(window.baseURL+'/'+page, '_blank');
        win.focus();
    }
    onOpenSaveModal(){
        this.setState({
            openSaveModal: true
        });
    };

    onCloseSaveModal(){
        this.setState({
            openSaveModal: false
        });
    };

    onOpenPublishModal(){
        this.setState({
            openPublishModal: true
        });
    };

    onClosePublishModal(){
        this.setState({
            openPublishModal: false
        });
    };

    onOpenGetFeedbackModal(){
        this.setState({
            openGetFeedbackModal: true
        });
    };

    onCloseGetFeedbackModal(){
        this.setState({
            openGetFeedbackModal: false
        });
    };

    onOpenSiteHistoryModal(){
        this.setState({
            openSiteHistoryModal: true
        });
    };

    onCloseSiteHistoryModal(){
        this.setState({
            openSiteHistoryModal: false
        });
    };

    onOpenImageSharpeningModal(){
        this.setState({ 
            openImageSharpeningModal: true
        });
    };

    onCloseImageSharpeningModal(){
        this.setState({
            openImageSharpeningModal: false
        });
    };

    //      %%  Top Image to hide and Show Header  %%

    showHeader () {
        this.setState(prev => ({ 
            showHeader: !prev.showHeader 
        }));
    };

    handleMouseHoverPage() {
        this.setState(this.toggleHoverStatePage);
    }

    toggleHoverStatePage(state) {
        return {
            isHoveringPage: !state.isHoveringPage,
        };
    }
    
    handleMouseHoverSite() {
        this.setState(this.toggleHoverStateSite);
    }

    toggleHoverStateSite(state) {
        return {
            isHoveringSite: !state.isHoveringSite,
        };
    }
    
    handleMouseHoverCode() {
        this.setState(this.toggleHoverStateCode);
    }

    toggleHoverStateCode(state) {
        return {
            isHoveringCode: !state.isHoveringCode,
        };
    }
    
    handleMouseHoverHelp() {
        this.setState(this.toggleHoverStateHelp);
    }

    toggleHoverStateHelp(state) {
        return {
            isHoveringHelp: !state.isHoveringHelp,
        };
    }
    
    handleMouseHoverZoom() {
        this.setState(this.toggleHoverStateZoom);
    }

    toggleHoverStateZoom(state) {
        return {
            isHoveringZoom: !state.isHoveringZoom,
        };
    }
    
    handleMouseHoverUndo() {
        this.setState(this.toggleHoverStateUndo);
    }

    toggleHoverStateUndo(state) {
        return {
            isHoveringUndo: !state.isHoveringUndo,
        };
    }
    
    handleMouseHoverRedo() {
        this.setState(this.toggleHoverStateRedo);
    }
    handleClickRedo() {
        window.editor.UndoManager.redo();
    }
    handleClickUndo() {
        window.editor.UndoManager.undo();
    }

    toggleHoverStateRedo(state) {
        return {
            isHoveringRedo: !state.isHoveringRedo,
        };
    }
    handleMouseHoverMobile() {
        this.setState(this.toggleHoverStateMobile);
    }
    handleMouseHoverDesktop() {
        this.setState(this.toggleHoverStateDesktop);
    }

    switchToMobile(){
        window.editor.setDevice('Mobile portrait');
    }
    switchToDesktop(){
        window.editor.setDevice('Desktop');
    }

    toggleHoverStateDesktop(state) {
        return {
            isHoveringDesktop: !state.isHoveringDesktop,
        };
    }

    toggleHoverStateMobile(state) {
        return {
            isHoveringMobile: !state.isHoveringMobile,
        };
    }

    previewWindow(){
        window.editor.runCommand('preview');
        
        stopPreview = 1;
    }

    escFunction(event){
        if(event.keyCode === 27 && stopPreview === 1) {
          window.editor.stopCommand('preview');
          stopPreview = 0;
        }
    }

    componentDidMount(){
        document.addEventListener("keydown", this.escFunction, false);
    }
    componentWillUnmount(){
        document.removeEventListener("keydown", this.escFunction, false);
    }
    handleMouseHoverSave() {
        this.setState(this.toggleHoverStateSave);
    }
    toggleHoverStateSave(state) {
        return {
            isHoveringSave: !state.isHoveringSave,
        };
    }
    handleMouseHoverPreview() {
        this.setState(this.toggleHoverStatePreview);
    }

    toggleHoverStatePreview(state) {
        return {
            isHoveringPreview: !state.isHoveringPreview,
        };
    }
    handleMouseHoverPublish() {
        this.setState(this.toggleHoverStatePublish);
    }

    toggleHoverStatePublish(state) {
        return {
            isHoveringPublish: !state.isHoveringPublish,
        };
    }
    handleMouseHoverSite_Preview() {
        this.setState(this.toggleHoverStateSite_Preview);
    }

    toggleHoverStateSite_Preview(state) {
        return {
            imgSrc: require('./../images/browser_window.jpg')
        };
    }

    handleMouseOutSite_Preview() {
        this.setState(this.toggleOutStateSite_Preview);
    }

    toggleOutStateSite_Preview(state) {
        return {
            imgSrc: require('./../images/floppy-disk.png'),
        };
    }
    handleMouseHoverSite_GetFeedback() {
        this.setState(this.toggleHoverStateSite_GetFeedback);
    }

    toggleHoverStateSite_GetFeedback(state) {
        return {
            imgSrc: require('./../images/get_feedback.png'),
        };
    }

    handleMouseOutSite_GetFeedback() {
        this.setState(this.toggleOutStateSite_GetFeedback);
    }

    toggleOutStateSite_GetFeedback(state) {
        return {
            imgSrc: require('./../images/globe.jpg')
        };
    }

    handleMouseHoverSite_Publish() {
        this.setState(this.toggleHoverStateSite_Publish);
    }

    toggleHoverStateSite_Publish(state) {
        return {
            imgSrc: require('./../images/browser_window.jpg')
        };
    }

    handleMouseOutSite_Publish() {
        this.setState(this.toggleOutStateSite_Publish);
    }

    toggleOutStateSite_Publish(state) {
        return {
            imgSrc: require('./../images/get_feedback.png'),
        };
    }

    handleMouseHoverSite_ViewPublishedSite() {
        this.setState(this.toggleHoverStateSite_ViewPublishedSite);
    }

    toggleHoverStateSite_ViewPublishedSite(state) {
        return {
            imgSrc: require('./../images/floppy-disk.png'),
        };
    }

    handleMouseOutSite_ViewPublishedSite() {
        this.setState(this.toggleOutStateSite_ViewPublishedSite);
    }

    toggleOutStateSite_ViewPublishedSite(state) {
        return {
            imgSrc: require('./../images/filter-logo.png')
        };
    }
    handleMouseHoverSite_GetFoundOnGoogle() {
        this.setState(this.toggleHoverStateSite_GetFoundOnGoogle);
    }

    toggleHoverStateSite_GetFoundOnGoogle(state) {
        return {
            imgSrc: require('./../images/Flat-Web-Browsers.png'),
        };
    }

    handleMouseOutSite_GetFoundOnGoogle() {
        this.setState(this.toggleOutStateSite_GetFoundOnGoogle);
    }

    toggleOutStateSite_GetFoundOnGoogle(state) {
        return {
            imgSrc: require('./../images/Flat-Web-Browsers.png')
        };
    }
    handleMouseHoverSite_Accessibility() {
        this.setState(this.toggleHoverStateSite_Accessibility);
    }

    toggleHoverStateSite_Accessibility(state) {
        return {
            imgSrc: require('./../images/top-arrow.png'),
        };
    }

    handleMouseOutSite_Accessibility() {
        this.setState(this.toggleOutStateSite_Accessibility);
    }

    toggleOutStateSite_Accessibility(state) {
        return {
            imgSrc: require('./../images/filter-logo.png')
        };
    }
    handleMouseHoverSite_ConnectDomain() {
        this.setState(this.toggleHoverStateSite_ConnectDomain);
    }

    toggleHoverStateSite_ConnectDomain(state) {
        return {
            imgSrc: require('./../images/top-arrow.png'),
        };
    }

    handleMouseOutSite_ConnectDomain() {
        this.setState(this.toggleOutStateSite_ConnectDomain);
    }

    toggleOutStateSite_ConnectDomain(state) {
        return {
            imgSrc: require('./../images/filter-logo.png')
        };
    }
    handleMouseHoverSite_Upgrade() {
        this.setState(this.toggleHoverStateSite_Upgrade);
    }

    toggleHoverStateSite_Upgrade(state) {
        return {
            imgSrc: require('./../images/top-arrow.png'),
        };
    }

    handleMouseOutSite_Upgrade() {
        this.setState(this.toggleOutStateSite_Upgrade);
    }

    toggleOutStateSite_Upgrade(state) {
        return {
            imgSrc: require('./../images/filter-logo.png')
        };
    }
    handleMouseHoverSite_MobileEditor() {
        this.setState(this.toggleHoverStateSite_MobileEditor);
    }

    toggleHoverStateSite_MobileEditor(state) {
        return {
            imgSrc: require('./../images/top-arrow.png'),
        };
    }

    handleMouseOutSite_MobileEditor() {
        this.setState(this.toggleOutStateSite_MobileEditor);
    }

    toggleOutStateSite_MobileEditor(state) {
        return {
            imgSrc: require('./../images/filter-logo.png')
        };
    }

    handleMouseHoverSite_ImageSharpening() {
        this.setState(this.toggleHoverStateSite_ImageSharpening);
    }

    toggleHoverStateSite_ImageSharpening(state) {
        return {
            imgSrc: require('./../images/top-arrow.png'),
        };
    }

    handleMouseOutSite_ImageSharpening() {
        this.setState(this.toggleOutStateSite_ImageSharpening);
    }

    toggleOutStateSite_ImageSharpening(state) {
        return {
            imgSrc: require('./../images/filter-logo.png')
        };
    }

    //  ** Site_ImageSharpening Hover End **


    //  ** Site_Mydashboard Hover Start **

    handleMouseHoverSite_MyDashboard() {
        this.setState(this.toggleHoverStateSite_MyDashboard);
    }

    toggleHoverStateSite_MyDashboard(state) {
        return {
            imgSrc: require('./../images/top-arrow.png'),
        };
    }

    handleMouseOutSite_MyDashboard() {
        this.setState(this.toggleOutStateSite_MyDashboard);
    }

    toggleOutStateSite_MyDashboard(state) {
        return {
            imgSrc: require('./../images/filter-logo.png')
        };
    }

    //  ** Site_Mydashboard Hover End **



    //  ** Site_SiteManager Hover Start **

    handleMouseHoverSite_SiteManager() {
        this.setState(this.toggleHoverStateSite_SiteManager);
    }

    toggleHoverStateSite_SiteManager(state) {
        return {
            imgSrc: require('./../images/top-arrow.png'),
        };
    }

    handleMouseOutSite_SiteManager() {
        this.setState(this.toggleOutStateSite_SiteManager);
    }

    toggleOutStateSite_SiteManager(state) {
        return {
            imgSrc: require('./../images/filter-logo.png')
        };
    }

    //  ** Site_SiteManager Hover End **


    //  ** Site_SiteHistory Hover Start **

    handleMouseHoverSite_SiteHistory() {
        this.setState(this.toggleHoverStateSite_SiteHistory);
    }

    toggleHoverStateSite_SiteHistory(state) {
        return {
            imgSrc: require('./../images/top-arrow.png'),
        };
    }

    handleMouseOutSite_SiteHistory() {
        this.setState(this.toggleOutStateSite_SiteHistory);
    }

    toggleOutStateSite_SiteHistory(state) {
        return {
            imgSrc: require('./../images/filter-logo.png')
        };
    }

    //  ** Site_SiteHistory Hover End **


    //  ** Site_ExitEditor Hover Start **

    handleMouseHoverSite_ExitEditor() {
        this.setState(this.toggleHoverStateSite_ExitEditor);
    }

    toggleHoverStateSite_ExitEditor(state) {
        return {
            imgSrc: require('./../images/top-arrow.png'),
        };
    }

    handleMouseOutSite_ExitEditor() {
        this.setState(this.toggleOutStateSite_ExitEditor);
    }

    toggleOutStateSite_ExitEditor(state) {
        return {
            imgSrc: require('./../images/filter-logo.png')
        };
    }

    showPageMenu(event) {
        event.preventDefault();
        var project = this.props.common.projectDetail.project;
        
        var pages = project.pages;
      
        this.setState({ showPageMenu: true, pages:pages }, () => {
            document.addEventListener('click', this.closePageMenu);
        });
    }

    closePageMenu() {
        this.setState({ showPageMenu: false }, () => {
            document.removeEventListener('click', this.closePageMenu);
        });
    }
    // ## Page Click End ##


    // ## Mobile Click Start ##
    showMobileMenu(event) {
        event.preventDefault();
        this.setState({ showMobileMenu: true }, () => {
            document.addEventListener('click', this.hideMobileMenu);
        });
    }

    hideMobileMenu() {
        this.setState({ showMobileMenu: false }, () => {
            document.removeEventListener('click', this.hideMobileMenu);
        });
    }
    // ## Mobile Click End ##

    // ############  Click Function End   #############

    render() {
        return (
            <div className={this.props.common.projectDetail.currentPage}>
                <div className="top-img"
                    onClick={this.showHeader}
                >
                    <i className={this.state.showHeader ? 'fa fa-chevron-up' : 'fa fa-chevron-down'}></i>
                </div>
                {
                    this.state.showHeader
                        ? (
                            <nav className="navbar navbar-light">
                                <div>
                                    <div className="logo_bg_curve" style={logo_bg}>
                                        <Link to="/" className="navbar-brand"><img alt="" src={require('./../images/filter-logo.png')} /></Link>
                                    </div>
                                    <ul className="nav navbar-nav pull-xs-left">
                                        <li className="nav-item"
                                            onMouseEnter={this.handleMouseHoverPage}
                                            onMouseLeave={this.handleMouseHoverPage}
                                            onClick={this.showPageMenu} >
                                            <span>{this.state.currentPage} <i className="flaticon-down-arrow"></i></span>
                                            {
                                                this.state.isHoveringPage && !this.state.showPageMenu &&
                                                <div className="dropdown_menu">
                                                    <h6><b>Switch Page</b></h6>
                                                    <p>
                                                        see all the pages
                                                    </p>
                                                </div>
                                            }
                                            {
                                                this.state.showPageMenu
                                                    ? (
                                                        <div className="dropdown_menu home_drop">
                                                            <div className="home_drop_inner">
                                                                <ul>
                                                                {
                                                                this.state.pages.map((page, key) => (
                                                                <li onClick={() => { this.switchPage(key) }} key={'pages'+key}>{page.name}</li>
                                                                ))
                                                                }
                                                                </ul>
                                                            </div>    
                                                        </div>
                                                    )
                                                    : (
                                                        null
                                                    )
                                            }
                                        </li>
                                        <li className="nav-item"
                                            onMouseEnter={this.handleMouseHoverSite}
                                            onMouseLeave={this.handleMouseHoverSite}
                                        >
                                            Site
                                {
                                                this.state.isHoveringSite && !this.state.showPageMenu &&
                                                <div className="dropdown_menu site_dropdown">
                                                    <div className="row">
                                                        <div className="col-md-7">
                                                            <ul className="generic_drop">
                                                                <li onClick={this.onOpenSaveModal}>
                                                                    Save
                                                                </li>
                                                                <li
                                                                    onClick={this.previewWindow}
                                                                    onMouseEnter={this.handleMouseHoverSite_Preview}
                                                                    onMouseLeave={this.handleMouseOutSite_Preview}
                                                                >
                                                                    Preview
                                                                </li>
                                                                <li
                                                                    onClick={this.onOpenGetFeedbackModal}
                                                                    onMouseEnter={this.handleMouseHoverSite_GetFeedback}
                                                                    onMouseLeave={this.handleMouseOutSite_GetFeedback}
                                                                >
                                                                    Get Feedback
                                                                </li>
                                                                <li
                                                                    onClick={this.onOpenPublishModal}
                                                                    onMouseEnter={this.handleMouseHoverSite_Publish}
                                                                    onMouseLeave={this.handleMouseOutSite_Publish}
                                                                >
                                                                    Publish
                                                                </li>
                                                                <li
                                                                    
                                                                    onMouseEnter={this.handleMouseHoverSite_ViewPublishedSite}
                                                                    onMouseLeave={this.handleMouseOutSite_ViewPublishedSite}
                                                                >
                                                                    View Published Site
                                                                </li>
                                                          
                                                            
                                                                <li
                                                                    onClick={this.onOpenSiteHistoryModal}
                                                                    onMouseEnter={this.handleMouseHoverSite_SiteHistory}
                                                                    onMouseLeave={this.handleMouseOutSite_SiteHistory}
                                                                >
                                                                    Site History
                                                                </li>
                                                                <hr/>
                                                                <li
                                                                    onMouseEnter={this.handleMouseHoverSite_ExitEditor}
                                                                    onMouseLeave={this.handleMouseOutSite_ExitEditor}
                                                                    onClick={this.onExitEditor}
                                                                >
                                                                    Exit Editor
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div className="col-md-5">
                                                            <div className="menu_img_wrap">
                                                                <Link to="/" className="navbar-brand"><img alt="" src={this.state.imgSrc} /></Link>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            }
                                        </li>

                                        <li className="nav-item"
                                            onMouseEnter={this.handleMouseHoverCode}
                                            onMouseLeave={this.handleMouseHoverCode}
                                        >
                                            Code
                                {
                                                this.state.isHoveringCode && !this.state.showPageMenu &&
                                                <div className="dropdown_menu code_drop">
                                                    <h5>Control Panda (beta): Get Advanced Capabilities</h5>
                                                    <div className="row">
                                                        <div className="col-md-8 develop_tool">
                                                            <div>
                                                                Turn on Developer Tools so you can:
                                                </div>
                                                            <div>
                                                                <i className="fa fa-check"></i> <span>Build database collections</span>
                                                            </div>
                                                            <div>
                                                                <i className="fa fa-check"></i> <span>Use repeating layouts</span>
                                                            </div>
                                                            <div>
                                                                <i className="fa fa-check"></i> <span>Create Custom User input forms</span>
                                                            </div>
                                                            <div>
                                                                <i className="fa fa-check"></i> <span>Add behavior with javaScript & Control Panda APIs</span>
                                                            </div>

                                                            <button type="button" className="btn btn-default">
                                                                Turn on Developer Tools
                                                </button>
                                                        </div>
                                                        <div className="col-md-4">
                                                            <Link to="/" className="navbar-brand"><img className="top-img" alt="" src={this.state.imgCode} /> </Link>
                                                        </div>
                                                    </div>
                                                </div>
                                            }
                                        </li>
                                        <li className="nav-item"
                                            onMouseEnter={this.handleMouseHoverHelp}
                                            onMouseLeave={this.handleMouseHoverHelp}
                                        >
                                            Help
                                {
                                                this.state.isHoveringHelp && !this.state.showPageMenu &&
                                                <div className="dropdown_menu help_drop">
                                                    <div className="row">
                                                        <div className="col-md-8">
                                                            <ul className="generic_drop">
                                                                <li>
                                                                    Editor Help Center
                                                    </li>
                                                                <li>
                                                                    Keyboard Shortcuts
                                                    </li>
                                                                <hr />
                                                                <li>
                                                                    Step-by-Step Videos
                                                    </li>
                                                                <li onClick={() => { this.connectDomain() }}>
                                                                    Connecting a Domain
                                                    </li>
                                                                <li>
                                                                    Getting Found Online (SEO)
                                                    </li>
                                                                <hr />
                                                                <li onClick={() => { this.openNewTab('terms') }}>
                                                                    Terms of Use
                                                    </li>
                                                                <li onClick={() => { this.openNewTab('privacy') }}>
                                                                    Privacy Policy
                                                    </li>
                                                            </ul>
                                                        </div>

                                                        <div className="col-md-4">
                                                            <div className="menu_img_wrap">
                                                                <Link to="/" className="navbar-brand"><img className="top-img" alt="" src={this.state.imgHelp} /> </Link>
                                                            </div>    
                                                        </div>
                                                    </div>
                                                </div>
                                            }
                                        </li>

                                    </ul>

                                    <ul className="nav navbar-nav pull-xs-right">
                                        <li className="nav-item"
                                            onMouseEnter={this.handleMouseHoverZoom}
                                            onMouseLeave={this.handleMouseHoverZoom}
                                        >
                                            <Link to="/" className="navbar-brand"><img alt="" src={require('./../images/reorder.svg')} /> </Link>
                                            {
                                                this.state.isHoveringZoom && !this.state.showPageMenu &&
                                                <div className="dropdown_menu">
                                                    <div className="row">
                                                        <div className="col-md-12">
                                                            <h6>Zoom Out & Reorder</h6>
                                                            <div>
                                                                View and organize all the sections on your page at once.
                                                </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            }
                                        </li>
                                        <li className="nav-item"
                                            onMouseEnter={this.handleMouseHoverUndo}
                                            onMouseLeave={this.handleMouseHoverUndo}
                                            onClick={this.handleClickUndo}
                                        >
                                            <Link to="/" className="navbar-brand"><img alt="" src={require('./../images/undo.svg')} /> </Link>
                                            {
                                                this.state.isHoveringUndo && !this.state.showPageMenu &&
                                                <div className="dropdown_menu">
                                                    <h6>Undo</h6>
                                                </div>
                                            }
                                        </li>
                                        <li className="nav-item"
                                            onMouseEnter={this.handleMouseHoverRedo}
                                            onMouseLeave={this.handleMouseHoverRedo}
                                            onClick={this.handleClickRedo}
                                        >
                                            <Link to="/" className="navbar-brand"><img alt="" src={require('./../images/redo.svg')} /> </Link>
                                            {
                                                this.state.isHoveringRedo && !this.state.showPageMenu &&
                                                <div className="dropdown_menu">
                                                    <h6>Redo</h6>
                                                </div>
                                            }
                                        </li>
                                        <li className="nav-item pad_0"
                                            onMouseEnter={this.handleMouseHoverDesktop}
                                            onMouseLeave={this.handleMouseHoverDesktop}
                                            onClick={this.switchToDesktop}
                                        >
                                            <div className="nav_icon">
                                                <i className="flaticon-desktop"></i>
                                            </div>
                                            {
                                                this.state.isHoveringDesktop && !this.state.showPageMenu && !this.state.showDesktopMenu &&
                                                <div className="dropdown_menu mobile_view">
                                                    <h6>Switch to Desktop</h6>
                                                    <p>
                                                        Edit your site for desktop.
                                        </p>
                                                </div>
                                            }
                                        </li>
                                        <li className="nav-item pad_0"
                                            onMouseEnter={this.handleMouseHoverMobile}
                                            onMouseLeave={this.handleMouseHoverMobile}
                                            onClick={this.switchToMobile}
                                        >
                                            <div className="nav_icon">
                                                <i className="flaticon-technology"></i>
                                            </div>

                                            {
                                                this.state.isHoveringMobile && !this.state.showPageMenu && !this.state.showMobileMenu &&
                                                <div className="dropdown_menu mobile_view">
                                                    <h6>Switch to Mobile</h6>
                                                    <p>
                                                        Edit your site for mobile.
                                        </p>
                                                </div>
                                            }
                                        </li>
                                        <li className="nav-item"
                                            onMouseEnter={this.handleMouseHoverSave}
                                            onMouseLeave={this.handleMouseHoverSave}
                                            onClick={this.onOpenSaveModal}
                                        >


                                            <i className="flaticon-tick-inside-circle"></i>


                                            {
                                                this.state.isHoveringSave && !this.state.showPageMenu &&
                                                <div className="dropdown_menu">
                                                    <h6>Save</h6>
                                                    <p>
                                                        Go ahead and save your website
                                        </p>
                                                </div>
                                            }

                                        </li>
                                        <li className="nav-item"
                                            onMouseEnter={this.handleMouseHoverPreview}
                                            onMouseLeave={this.handleMouseHoverPreview}
                                            onClick={this.previewWindow}
                                        >
                                            <i className="flaticon-eye"></i>
                                            {
                                                this.state.isHoveringPreview && !this.state.showPageMenu &&
                                                <div className="dropdown_menu">
                                                    <h6>Preview</h6>
                                                    <p>
                                                        See what your site looks like on desktop and mobile before you go live.
                                        </p>
                                                </div>
                                            }
                                        </li>
                                        <li className="nav-item"
                                            onMouseEnter={this.handleMouseHoverPublish}
                                            onMouseLeave={this.handleMouseHoverPublish}
                                            onClick={this.onOpenPublishModal}
                                        >
                                            <div className="preview_bg_curve" style={preview_bg}>
                                                <span>Publish</span>
                                            </div>
                                            {
                                                this.state.isHoveringPublish && !this.state.showPageMenu &&
                                                <div className="dropdown_menu">
                                                    <h6>Publish</h6>
                                                    <p>
                                                        Click Publish to share your creation with the world!
                                        </p>
                                                </div>
                                            }
                                        </li>
                                    </ul>
                                </div>
                            </nav>) : (null)}

                <Modal
                    style={styles}
                    open={this.state.openSaveModal}
                    onClose={this.onCloseSaveModal}
                    contentLabel="Example Modal"
                    overlayClassName="overLay-publish_modal"
                >
                    <div className="modal-header">
                        <div className="center-data">
                            <h4>Your changes were saved</h4>
                        </div>
                        <div className="col-md-12 center-data">
                            <p>Publish to see your changes live at the following domain:</p>
                        </div>
                    </div>

                    <div className="domain_modal_inner">
                        <div className="publish_site">
                            <div className="domain_bx_wrap">
                                <button type="button" className="btn fa fa-arrow-left" disabled></button>
                                <button type="button" className="btn fa fa-refresh" disabled></button>
                                <input type="text" value="http://myname.controlpanda.com/" readOnly />
                                <input type="text" placeholder="mysite"/>
                            </div>
                            <button type="button" className="btn theme_btn float-right">Edit</button>
                        </div>    
                        <div className="what_next">
                          <span>What's Next</span>                  
                        </div>
                        <div className="publish_now">
                           <img alt="" src="/images/publish.png"/>                 
                           <div className="">
                             <h4>Connect to our own branded domain</h4>          
                             <a href="!#">Publish Now</a>     
                           </div>
                        </div>
                    </div>
                    <div className="publish_modal_bottom">
                        <div className="custom_check">
                           <input id="not_show" type="checkbox"/>
                           <label htmlFor="not_show">Dont't show this again</label>   
                        </div>                    
                        
                        <button type="button" onClick={this.savePage} className="btn theme_btn">Done</button>
                        <button type="button" onClick={this.savePage} className="btn theme_btn">Publish Now</button>
                    </div>

                </Modal>
                
                <Modal
                    style={styles}
                    open={this.state.openDomainModal}
                    onClose={this.onCloseDomainModal}
                    contentLabel="Connect Domain"
                    overlayClassName="overLay-publish_modal"
                >
                    <div className="modal-header">
                        <div className="center-data">
                            <h4>Connect Domain</h4>
                        </div>
                    </div>
                    <div className="domain_modal_inner">
                        <div className="publish_site save_modal">
                        {'hello world'+this.state.selectedDomain}
                            <select defaultValue={this.state.selectedDomain} className="form-control" id="domains_list">
                            { (this.state.domains.length) ?
                            this.state.domains.map((domain) => (
                                <option key={'domainOption'+domain.id} value={domain.id}>{domain.name}</option>
                            )) : ''
                            }
                            </select>
                        </div>
                    </div>
                    <div className="publish_modal_bottom">
                        <button type="submit" className="btn theme_btn" onClick={() => { this.connectDomainSubmit() }}>Done</button>
                    </div>
                </Modal>

                <Modal
                    style={styles}
                    open={this.state.openPublishModal}
                    onClose={this.onClosePublishModal}
                    contentLabel="Example Modal"
                    overlayClassName="overLay-publish_modal"
                >
                    <div className="modal-header">
                        <div className="center-data">
                            <h4>Congratulations</h4>
                        </div>
                        <div className="col-md-12 center-data">
                            <p>Your site is published and is live online</p>
                        </div>
                    </div>
                    <div className="domain_modal_inner">
                        <div className="publish_site save_modal">
                            <div className="domain_bx_wrap">
                                <button type="button" className="btn fa fa-arrow-left" disabled></button>
                                <button type="button" className="btn fa fa-refresh" disabled></button>
                                <input type="text" value="http://myname.controlpanda.com/mysite" readOnly />
                            </div>
                            <button type="button" className="btn theme_btn float-right">View Site</button>
                        </div>    
                        <div className="what_next">
                          <span>What's Next</span>                  
                        </div>
                        <div className="publish_now">
                           <img alt="" src="/images/publish.png"/>                 
                           <div className="">
                             <h4>Publish your site so people can see it</h4>          
                             <a href="!#">Publish Now</a>     
                           </div>
                        </div>
                    </div>
                    <div className="publish_modal_bottom">
                        <div className="custom_check">
                           <input id="not_show" type="checkbox"/>
                           <label htmlFor="not_show">Dont't show this again</label>   
                        </div>                    
                        <button type="submit" className="btn theme_btn">Done</button>
                    </div>
                </Modal>

                <Modal
                    style={styles}
                    open={this.state.openGetFeedbackModal}
                    onClose={this.onCloseGetFeedbackModal}
                    contentLabel="Example Modal"
                    overlayClassName="overLay-publish_modal"
                >
                    <div className="modal-header">
                        <div className="center-data">
                            <h4>Get Feedback</h4>
                        </div>
                        <div className="col-md-12 center-data">
                            <p>See your latest comments. or share with more people</p>
                        </div>
                    </div>
                    <div className="get_feedback_inner">
                        <div className="get_feedback_tiles">
                          <img alt="" width="25" src="/images/chat.png"/>
                          <span>Read your comments</span>                  
                          <i className="fa fa-chevron-right"></i>
                          <i className="fa fa-share-square-o"></i>
                        </div> 
                        <div className="get_feedback_tiles" onClick={() => { this.copyFeedbackLink() }}>
                          <img alt="" width="25" src="/images/share.png"/>
                          <span>Share your site</span>                  
                          <i className="fa fa-chevron-right"></i>
                          <i className="fa fa-share-square-o"></i>
                        </div>                    
                    </div>
                    <div className="get_feedback_bottom">
                      <button className="btn theme_btn">Maybe Later</button>
                    </div>
                </Modal>


                <Modal
                    style={styles}
                    open={this.state.openSiteHistoryModal}
                    onClose={this.onCloseSiteHistoryModal}
                    contentLabel="Example Modal"
                    overlayClassName="overLay-publish_modal"
                >
                    <div className="modal-header">
                        <div className="center-data">
                            <h4>Visit Your Site History</h4>
                        </div>
                    </div>
                    <div className="site_history">
                      <div className="img_wrap">
                         <img alt="" width="200" src="/images/clock.png"/>
                      </div>
                      <div className="dtl">
                        <p>Go back in time to view or restore any version of your site thata you'vr ever Saved or Published.When you click below a new tab will opwn showing all your Site History.</p> 

                      </div>                          
                    </div>
                    <div className="text-right pad_15">
                        <button className="btn theme_btn">Continue to Site History</button>
                    </div>
                </Modal>

                <Modal
                    style={styles}
                    open={this.state.openImageSharpeningModal}
                    onClose={this.onCloseImageSharpeningModal}
                    contentLabel="Example Modal"
                    overlayClassName="overLay-publish_modal"
                >
                    <div className="modal-header">
                        <div className="center-data">
                            <h4>Image Sharpening (Unsharp Mask)</h4>
                        </div>
                        
                    </div>
                    <div className="domain_modal_inner">
                       <p>Choose the sharpening option that's best for you.it will apply to all the images on your site.</p>
                       <ul className="image_sharpening">
                           <li>
                               <div className="diagonal_none"></div>
                               <span>None</span>
                           </li>
                           <li>
                                <div className="img_wrap">
                                 <img alt="" width="70" src="/images/rainbow.png"/>
                                </div> 
                               <span>Mild</span>
                           </li>
                           <li>
                               <div className="img_wrap">
                                <img alt="" width="70" src="/images/girl-face.png"/>
                               </div> 
                               <span>Classic</span>
                           </li>
                           <li>
                               <div className="img_wrap">
                                <img alt="" width="70" src="/images/plant.png"/>
                               </div> 
                               <span>Moderate</span>
                           </li>
                           <li>
                              <div className="img_wrap">
                               <img alt="" width="70" src="/images/skyline.png"/>
                              </div> 
                               <span>Strong</span>
                           </li>
                           <li>
                              <div className="img_wrap"> 
                               <img alt="" width="70" src="/images/translate.png"/>
                              </div> 
                               <span>Max</span>
                           </li>
                       </ul>
                    </div>
                    <div className="image_sharpening_bottom">                  
                        <button type="button" className="btn btn-default">Cancle</button>
                        <button type="button" className="btn theme_btn float_right">Apply</button>
                    </div>
                </Modal>

            </div>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Header);