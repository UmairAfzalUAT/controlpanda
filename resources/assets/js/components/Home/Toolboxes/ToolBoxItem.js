import React from 'react';
import _ from 'lodash';

const addImagesToPage = (event) => {
    var item = event.target;
    var elementClickedHtml = '<h1 class="componentElement" style="position:absolute;font-family:'+item.getAttribute('font-family')+'; font-size: '+item.getAttribute('font-size')+'; font-weight: '+item.getAttribute('font-weight')+'" height="'+item.style.height+'" width="'+item.style.minWidth+'">'+item.getAttribute('content')+'</h1>';
    var addedComponent = editor.getComponents().add(elementClickedHtml);
    addedComponent.addClass('componentElement');
    addedComponent.attributes.attributes.componentUniqueAttribute = 'componentUniqueAttribute'+window.incrementalValueForAttribute;
    addedComponent.addClass('componentUniqueAttribute'+window.incrementalValueForAttribute);
    var scrollTop = $(window).scrollTop();
    var scrollLeft = $(window).scrollLeft();
    var windowWidth = $(window).width();
    var windowHeight = $(window).height();
    var currentlyAddedElement = $('iframe').contents().find('html').find('body').find('.componentUniqueAttribute'+window.incrementalValueForAttribute);
    var currentlyAddedElementWidth = currentlyAddedElement.width();
    var currentlyAddedElementHeight = currentlyAddedElement.height();
    var calculatedTop = Math.max(0, ((windowHeight - currentlyAddedElementHeight) / 2) + scrollTop);
    var calculatedLeft = Math.max(0, ((windowWidth - currentlyAddedElementWidth) / 2) + scrollLeft);
    window.incrementalValueForAttribute++;       
    addedComponent.setStyle(
    {
        "z-index": window.zIndex,
        "top": calculatedTop,
        "position": "absolute",
        "left": calculatedLeft
    });
    window.zIndex++;
    window.newElement =false;
}

export default class ToolBoxItem extends React.Component {
    render() {
        const { imgUrl, addFce, container } = this.props;

        var divStyle = {
            position: 'relative',
            backgroundImage: 'url(' + imgUrl + ')',
            backgroundRepeat: 'no-repeat',
            width: container.style.width,
            height: container.style.height
        };




        var containerWidth = container.style && container.style.width;
        return (
            <div style={divStyle}>
                {
                    _.map(container.boxes, function (item, index) {
                        var itemNew = item;
                        var itemStyle = _.clone(item.style);
                        itemStyle.borderWidth = 1;
                        itemStyle.minWidth = item.style.minWidth;
                        itemStyle.minHeight = 40;
                        itemStyle.width = item.style.width || containerWidth;
                        itemStyle.height = item.style.height;
                        itemStyle.position = 'absolute';
                        itemStyle[':hover'] = {
                            backgroundColor: 'lightblue',
                            opacity: 0.5
                        };
                        return (<div className="singleElementDragger" onDragStart={(event) => {
                            var item = event.target;
                            console.log(item);
                            var text = '<h1 class="componentElement" style="position:absolute;font-family:'+item.getAttribute('font-family')+'; font-size: '+item.getAttribute('font-size')+'; font-weight: '+item.getAttribute('font-weight')+'" height="'+item.style.height+'" width="'+item.style.minWidth+'">'+item.getAttribute('content')+'</h1>';
                            event.dataTransfer.setData("text", text);
                        }} onClick={(event) => { addImagesToPage(event) }} draggable="true" key={'tool' + index} style={itemStyle} fontSize={item.props.font.fontSize}
                        fontFamily={item.props.font.fontFamily}
                        fontWeight={item.props.font.bold ? 'blod' : 'normal'}
                        content={item.props.content}
                            onMouseOver={(e) => {
                                var item = e.target;
                                item.style['background-color'] = 'lightblue';
                                item.style['opacity'] = 0.5;
                            }}
                            onMouseOut={(e) => {
                                var item = e.target;
                                item.style['background-color'] = 'transparent';
                                item.style['opacity'] = 1;
                            }}></div>)
                    }, this)}
            </div>

        )
    }
}