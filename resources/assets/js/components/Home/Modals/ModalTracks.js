import React from 'react'
import ReactDOM from 'react-dom'
import Modal from 'react-modal'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs'
import Dropdown from 'react-dropdown'
import 'react-dropdown/style.css'
import InfiniteScroll from 'react-infinite-scroll-component'

const sortOptions = [
  'A-Z', 'Z-A', 'Newest', 'Oldest'
]
const categoryOptions = [
  'Blog', 'Designer', 'Restaurants', 'Events', 'Portfolio & CV', 'Accommodation', 'Bussiness', 'Online Stroe', 'Photography', 'Music', 'Other'
]
const defaultCategory = categoryOptions[0]
const defaultSortOption = sortOptions[2]

const gridLayout = [
  { value: '3x3', label: <i className="fa fa-th"></i> },
  { value: '2x2', label: <i className="fa fa-th-large"></i> },
  { value: '1x1', label: <i className="fa fa-list"></i> },
]
const defaultgridLayout = gridLayout[0]
const customStyles = {
  content: {
    top: '62.2%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)'
  }
}

class ModalTracks extends React.Component {
    constructor() {
      super();
      this.state = {
        modalIsOpen: false
      };
      this.openModal = this.openModal.bind(this);
      this.afterOpenModal = this.afterOpenModal.bind(this);
      this.closeModal = this.closeModal.bind(this);
    }
    openModal() {
      this.setState({ modalIsOpen: true });
    }
    afterOpenModal() {
      this.subtitle.style.color = '#f00';
    }
    closeModal() {
      this.setState({ modalIsOpen: false });
    }
  
    render() {
      return (
        <div className="row" onClick={this.openModal}>
          <div className="col-md-3 image-img">
            <img alt="" width="80" src="https://cdn1.iconfinder.com/data/icons/devices-7/128/devices-26-512.png" />
          </div>
          <div className="col-md-6 image-txt">
            <h4>Tracks</h4>
          </div>
          <div className="col-md-3 image-icon">
            <button className="btn"><i className="fa fa-chevron-right"></i></button>
          </div>
          <Modal
            isOpen={this.state.modalIsOpen}
            onAfterOpen={this.afterOpenModal}
            onRequestClose={this.closeModal}
            style={customStyles}
            contentLabel="Tracks Modal"
            overlayClassName="generic_modal"
          >
  
            <h2 ref={subtitle => this.subtitle = subtitle}>Manage your Tracks
              <span className="modal_btn pull-right">
                <button className="btn">?</button>
                <button className="btn" onClick={this.closeModal}>X</button>
              </span>
            </h2>
  
            <div className="modal_inner">
              <Tabs>
                <TabList className="modal_top_tabs">
                  <Tab className="my-docs-modal">My Tracks</Tab>
                </TabList>
                <TabPanel>
                  <div className="Upload_image">
                    <div className="row">
                      <div className="col-md-7">
                        <p>
                          Upload or drag and drop tracks in MP3 format. Each file can be upto 50MB.
                      </p>
                      </div>
                      <div className="col-md-5 text-right">
                        <Dropdown className="docSort" options={sortOptions} onChange={this._onSelect} value={defaultSortOption} placeholder="Sort By" />
                        <div className="modal_search">
                          <i className="fa fa-search"></i> <input className="" type="text" placeholder="Search" />
                        </div>
                        <div className="layout_grid">
                          <Dropdown className="docDisplay" options={gridLayout} onChange={this._onSelect} value={defaultgridLayout} />
                        </div>
                      </div>
                    </div>
                  </div>
                  <Tabs>
                    <div className="row">
                      <div className="col-md-3">
                        <div className="Left_Inner_Tabs_wrap">
                          <TabList className="Left_Inner_Tabs">
                            <Tab className="all-media-sidebar">All Media</Tab>
                            <Tab className="purchased-sidebar">Purchased</Tab>
                          </TabList>
                          <button>
                            <i className="fa fa-plus-square-o"></i> Add New Folder
                        </button>
                        </div>
                      </div>
  
                      <div className="col-md-9">
                        <TabPanel>
                          <div>
                            Farhan
                        </div>
                        </TabPanel>
                        <TabPanel>
                          <div>
                            Ali
                        </div>
                        </TabPanel>
                      </div>
                    </div>
                  </Tabs>
                  <div className="modal_bottom_btn"><button className="">Add to Page</button></div>
  
                </TabPanel>
              </Tabs>
            </div>
          </Modal>
        </div>
      );
    }
  } export default ModalTracks;