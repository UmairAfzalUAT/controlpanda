import React from 'react'
import ReactDOM from 'react-dom'
import Modal from 'react-modal'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs'
import Dropdown from 'react-dropdown'
import 'react-dropdown/style.css'
import InfiniteScroll from 'react-infinite-scroll-component'

const sortOptions = [
  'A-Z', 'Z-A', 'Newest', 'Oldest'
]
const categoryOptions = [
  'Blog', 'Designer', 'Restaurants', 'Events', 'Portfolio & CV', 'Accommodation', 'Bussiness', 'Online Stroe', 'Photography', 'Music', 'Other'
]
const defaultCategory = categoryOptions[0]
const defaultSortOption = sortOptions[2]

const gridLayout = [
  { value: '3x3', label: <i className="fa fa-th"></i> },
  { value: '2x2', label: <i className="fa fa-th-large"></i> },
  { value: '1x1', label: <i className="fa fa-list"></i> },
]
const defaultgridLayout = gridLayout[0]
const customStyles = {
  content: {
    top: '62.2%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)'
  }
}


class ModalVector extends React.Component {
    constructor() {
      super();
      this.state = {
        modalIsOpen: false
      };
      this.openModal = this.openModal.bind(this);
      this.afterOpenModal = this.afterOpenModal.bind(this);
      this.closeModal = this.closeModal.bind(this);
    }
    openModal() {
      this.setState({ modalIsOpen: true });
    }
    afterOpenModal() {
      this.subtitle.style.color = '#f00';
    }
    closeModal() {
      this.setState({ modalIsOpen: false });
    }
  
    render() {
      return (
        <div className="row" onClick={this.openModal}>
          <div className="col-md-3 image-img">
            <img alt="" width="80" src="https://images.emojiterra.com/google/android-oreo/512px/1f335.png" />
          </div>
          <div className="col-md-6 image-txt">
            <h4>Vector Art</h4>
          </div>
          <div className="col-md-3 image-icon">
            <button className="btn"><i className="fa fa-chevron-right"></i></button>
          </div>
          <Modal
            isOpen={this.state.modalIsOpen}
            onAfterOpen={this.afterOpenModal}
            onRequestClose={this.closeModal}
            style={customStyles}
            contentLabel="Vector Art Modal"
            overlayClassName="generic_modal"
          >
  
            <h2 ref={subtitle => this.subtitle = subtitle}>Manage your Vector Art
            <span className="modal_btn pull-right">
                <button className="btn">?</button>
                <button className="btn" onClick={this.closeModal}>X</button>
              </span>
            </h2>
  
            <div className="modal_inner">
              <Tabs>
                <TabList className="modal_top_tabs">
                  <Tab className="my-images-modal">My Vector Art</Tab>
                  <Tab className="free-from-controlpanda-vector-modal">Free from ControlPanda</Tab>
                  <button className="btn upload_btn pull-right"><i className="fa fa-upload"></i> Upload Vector Art</button>
                </TabList>
                <TabPanel>
                  <div className="Upload_image">
                    <div className="row">
                      <div className="col-md-7">
                        <p>
                          Upload or drag and drop Vector Art in SVG format. Each file can be upto 250K.
                        </p>
                      </div>
  
                      <div className="col-md-5 text-right">
                        <Dropdown className="vectorSort" options={sortOptions} onChange={this._onSelect} value={defaultSortOption} placeholder="Sort By" />
                        <div className="modal_search">
                          <i className="fa fa-search"></i> <input className="" type="text" placeholder="Search" />
                        </div>
                        <div className="layout_grid">
                          <Dropdown className="vectorDisplay" options={gridLayout} onChange={this._onSelect} value={defaultgridLayout} />
                        </div>
                      </div>
                    </div>
                  </div>
  
                  <Tabs>
                    <div className="row">
                      <div className="col-md-3">
                        <div className="Left_Inner_Tabs_wrap">
                          <TabList className="Left_Inner_Tabs">
                            <Tab className="all-media-sidebar">All Media</Tab>
                            <Tab className="purchased-sidebar">Purchased</Tab>
                          </TabList>
                          <button><i className="fa fa-plus-square-o"></i> Add New Folder</button>
                        </div>
                      </div>
  
                      <div className="col-md-9">
                        <TabPanel>
                          <div>
                            Farhan
                          </div>
                        </TabPanel>
                        <TabPanel>
                          <div>
                            Ali
                          </div>
                        </TabPanel>
                      </div>
                    </div>
                  </Tabs>
                  <div className="modal_bottom_btn">
                    <button className="">
                      Add to Page
                      </button>
                  </div>
  
                </TabPanel>
  
                <TabPanel>
                  <Tabs>
                    <div className="Upload_image">
                      <div className="row">
                        <div className="col-md-8">
  
                          <TabList className="vector_inner_tabs">
                            <Tab>
                              Vector
                            </Tab>
                            <Tab>
                              Basic Shapes
                            </Tab>
                          </TabList>
  
                        </div>
  
                        <div className="col-md-4 text-xs-right">
                          <div className="modal_search before_none">
                            <i className="fa fa-search"></i> <input className="" type="text" placeholder="Search" />
                          </div>
                          <div className="layout_grid">
                            <Dropdown className="vectorDisplay" options={gridLayout} onChange={this._onSelect} value={defaultgridLayout} />
                          </div>
                        </div>
                      </div>
                    </div>
                    <TabPanel>
                      <Tabs>
                        <div className="row">
                          <div className="col-md-3">
                            <div className="Left_Inner_Tabs_wrap">
                              <TabList className="Left_Inner_Tabs">
                                <Tab className="">All</Tab>
                                <Tab className="">Abstract</Tab>
                                <Tab className="">Animals & Nature</Tab>
                                <Tab className="">Art & Music</Tab>
                                <Tab className="">Characters</Tab>
                                <Tab className="">Events & Holidays</Tab>
                                <Tab className="">Fashion</Tab>
                                <Tab className="">Flags</Tab>
                                <Tab className="">Health & Wellness</Tab>
                                <Tab className="">Icons</Tab>
                                <Tab className="">Food & Drink</Tab>
                                <Tab className="">Objects</Tab>
                                <Tab className="">Logos & Badges</Tab>
                                <Tab className="">Industry</Tab>
                                <Tab className="">Sport</Tab>
                                <Tab className="">Street Art</Tab>
                                <Tab className="">Technology</Tab>
                                <Tab className="">Travel & Transportation</Tab>
                                <Tab className="">Typography</Tab>
                              </TabList>
                            </div>
                          </div>
  
                          <div className="col-md-9">
  
                            <TabPanel>
  
                              <div>
                                <img alt="" />
                              </div>
  
                            </TabPanel>
                            <TabPanel>
  
                              <div>
                                <img alt="" />
                              </div>
  
                            </TabPanel>
                            <TabPanel>
  
                              <div>
                                <img alt="" />
                              </div>
  
                            </TabPanel>
                            <TabPanel>
  
                              <div>
                                <img alt="" />
                              </div>
  
                            </TabPanel>
                            <TabPanel>
  
                              <div>
                                <img alt="" />
                              </div>
  
                            </TabPanel>
                            <TabPanel>
  
                              <div>
                                <img alt="" />
                              </div>
  
                            </TabPanel>
                            <TabPanel>
  
                              <div>
                                <img alt="" />
                              </div>
  
                            </TabPanel>
                            <TabPanel>
  
                              <div>
                                <img alt="" />
                              </div>
  
                            </TabPanel>
                            <TabPanel>
  
                              <div>
                                <img alt="" />
                              </div>
  
                            </TabPanel>
                            <TabPanel>
  
                              <div>
                                <img alt="" />
                              </div>
  
                            </TabPanel>
                            <TabPanel>
  
                              <div>
                                <img alt="" />
                              </div>
  
                            </TabPanel>
                            <TabPanel>
  
                              <div>
                                <img alt="" />
                              </div>
  
                            </TabPanel>
                            <TabPanel>
  
                              <div>
                                <img alt="" />
                              </div>
  
                            </TabPanel>
                            <TabPanel>
  
                              <div>
                                <img alt="" />
                              </div>
  
                            </TabPanel>
                            <TabPanel>
  
                              <div>
                                <img alt="" />
                              </div>
  
                            </TabPanel>
                            <TabPanel>
  
                              <div>
                                <img alt="" />
                              </div>
  
                            </TabPanel>
                            <TabPanel>
  
                              <div>
                                <img alt="" />
                              </div>
  
                            </TabPanel>
                            <TabPanel>
  
                              <div>
                                <img alt="" />
                              </div>
  
                            </TabPanel>
                            <TabPanel>
  
                              <div>
                                <img alt="" />
                              </div>
  
                            </TabPanel>
                          </div>
                        </div>
                        <div className="modal_bottom_btn"><button className="">Add to Page</button></div>
                      </Tabs>
                    </TabPanel>
  
                    <TabPanel>
                      <Tabs>
                        <div className="row">
                          <div className="col-md-3">
                            <div className="Left_Inner_Tabs_wrap">
                              <TabList className="Left_Inner_Tabs">
                                <Tab className="">All</Tab>
                                <Tab className="">Actions</Tab>
                                <Tab className="">Arrows</Tab>
                              </TabList>
                            </div>
                          </div>
                          <div className="col-md-9">
                            <TabPanel>
                              <div>
                                <img alt="" />
                              </div>
                            </TabPanel>
                            <TabPanel>
                              <div>
                                <img alt="" />
                              </div>
                            </TabPanel>
                            <TabPanel>
                              <div>
                                <img alt="" />
                              </div>
                            </TabPanel>
                          </div>
                        </div>
                        <div className="modal_bottom_btn"><button>Add to Page</button></div>
                      </Tabs>
                    </TabPanel>
                  </Tabs>
                </TabPanel>
              </Tabs>
            </div>
          </Modal>
        </div>
      );
    }
  } export default ModalVector;