import React from 'react'
import Modal from 'react-modal'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs'
import Dropdown from 'react-dropdown'
import axios from 'axios'
import 'react-dropdown/style.css'
import InfiniteScroll from 'react-infinite-scroll-component'
import { OldSocialLogin as SocialLogin } from 'react-social-login'
import { isNullOrUndefined } from 'util';
import $ from 'jquery';

// import {GoogleAPI,GoogleLogin,GoogleLogout} from 'react-google-oauth'
// import InstagramLogin from 'react-instagram-login';
Object.size = function(obj) {
  var size = 0, key;
  for (key in obj) {
      if (obj.hasOwnProperty(key)) size++;
  }
  return size;
};

const sortOptions = [
  'A-Z', 'Z-A', 'Newest', 'Oldest'
]
const categoryOptions = [
  'Blog', 'Designer', 'Restaurants', 'Events', 'Portfolio & CV', 'Accommodation', 'Bussiness', 'Online Stroe', 'Photography', 'Music', 'Other'
]
const defaultCategory = categoryOptions[0]
const defaultSortOption = sortOptions[2]

const gridLayout = [
  { value: '3x3', label: <i className="fa fa-th"></i> },
  { value: '2x2', label: <i className="fa fa-th-large"></i> },
  { value: '1x1', label: <i className="fa fa-list"></i> },
]
const defaultgridLayout = gridLayout[0]
const customStyles = {
  content: {
    top: '62.2%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)'
  }
}


const addImagesToPage = (user, err) => {
    var imagesSelectedHere = '';
    var currentContainerSlected = $('iframe').contents().find('html').find('#slider-deli');
    var myComponent = editor.getComponents().at(9);
    var scrollTop = $(window).scrollTop();
    var scrollLeft = $(window).scrollLeft();
    var windowWidth = $(window).width();
    var windowHeight = $(window).height();
    var calculatedTop = Math.max(0, ((windowHeight - 500) / 2) + scrollTop);
    var calculatedLeft = Math.max(0, ((windowWidth - 500) / 2) + scrollLeft);
    var sizeAdditive = 0;
    
    $('.imageSelector').each(function(index, element)
    {
      var splittedUlr = $(this).find("img").attr("src").split("thumbnails/");
      var thisUrl = splittedUlr[0]+splittedUlr[1];
      imagesSelectedHere = '<img style="position:absolute; top:0; right:50%" src="'+thisUrl+'"/>';
      
      var addedComponent = editor.getComponents().add(imagesSelectedHere);
      
      addedComponent.addClass('componentElement');
      addedComponent.attributes.attributes.componentUniqueAttribute = 'componentUniqueAttribute'+window.incrementalValueForAttribute;
      addedComponent.addClass('componentUniqueAttribute'+window.incrementalValueForAttribute);
      window.incrementalValueForAttribute++;       
      addedComponent.setStyle(
      {
          "z-index": window.zIndex,
          "top": (calculatedTop + sizeAdditive),
          "position": "absolute",
          "left": (calculatedLeft + sizeAdditive),
          "width":"500px"
      });
      sizeAdditive += 40;
      window.zIndex++;
    });
    window.newElement =false;
}





const handleSocialLogin = (user, err) => {
  console.log(user)
  //console.log(err)
  console.log("Id is: "+user.profile.id)
  console.log("Token is: "+user.token.accessToken)
  if(user){
    axios({
      method:'get',
      url:'https://graph.facebook.com/v3.0/me/albums',
      responseType:'stream',
      headers: { Authorization : 'Bearer ' +user.token.accessToken }
    })
      .then(function(response) {
        console.log(response)
        response.data.pipe(fs.createWriteStream('ada_lovelace.jpg'))
    });
  }
}


const handleSocialLoginGooglePhotos = (user, err) => {
  //console.log(user)
  console.log("Id is: "+user.profile.id)
  console.log("Access Token is: "+user.token.accessToken)
 if(user){
    axios({
      method:'get',
      url: 'https://photoslibrary.googleapis.com/v1/albums',
      apiKey: 'AIzaSyAGDM9ZnErXnV7WGbsE7tgXFw39o8d-43Y',
      responseType:'stream',
      headers: { Authorization : 'Bearer ' +user.token.accessToken }
    })
      .then(function(response) {
        console.log(response)
        response.data.pipe(fs.createWriteStream('ada_lovelace.jpg'))
      
    });
  }
}


const handleSocialLoginGoogleDrive = (user, err) => {
  console.log(user)
  console.log("Profile Id for G-Drive is: "+user.profile.id)
  console.log("Access Token for G-Drive is: "+user.token.accessToken)
 if(user){
    axios({
      method:'get',
      url: 'https://www.googleapis.com/drive/v3/files',
      apiKey: 'AIzaSyAGDM9ZnErXnV7WGbsE7tgXFw39o8d-43Y',
      responseType:'stream',
      headers: { Authorization : 'Bearer ' +user._token.accessToken }
    })
      .then(function(response) {
        console.log(response)
        response.data.pipe(fs.createWriteStream('ada_lovelace.jpg'))
      
    });
  }
}

class ModalImage extends React.Component {
    constructor() {
      super();
      this.state = {
        modalIsOpen: false,
        freeImages: [],
        freeCatImages: [],
        freeImagesCat: [],
        freeImagesParts: [],
        freeCatImagesParts: [],
        startCatImg:[],
        endCatImg:[],
        hasMoreCatImg:[],
        insta_img: [],
        start: 0,
        end: 19,
        shutterPage: [],
        shutterPerPage: 20,
        shutterCats: [],
        shutterImagesParts: [],
        shutterCatLoaded: false,
        freeImagesLoaded: false,
        error: null,
        hasMore:true,
        hasMoreShutter: true
      };
      this.openModal = this.openModal.bind(this);
      this.afterOpenModal = this.afterOpenModal.bind(this);
      this.closeModal = this.closeModal.bind(this);
      this.fetchMoreData = this.fetchMoreData.bind(this);
    }
  
    componentDidMount() {
      $("body").on("click", ".image-holder", function()
      {
          $(this).toggleClass("imageSelector");
      });
      this.setState({
        modalIsOpen : this.props.modalState
      })
      var insta_images = [];
      var access_token = window.location.hash.substr(1);
      if(access_token !== ""){
        console.log(access_token);
        var sliced_access_token = access_token.slice(12);
        console.log("After Slice: "+sliced_access_token);
        if(sliced_access_token !== ""){
          axios({
            method:'get',
            url: 'https://api.instagram.com/v1/users/self/media/recent/?access_token'+sliced_access_token
          })
            .then((response) => {
              for(var i = 0; i < response.data.data.length; i++)
              {
                insta_images.push(response.data.data[i].images.low_resolution.url);
              }
              console.log("Insta Images: "+insta_images)
              this.setState({
                insta_img: insta_images
              });
              console.log("State of Image Array: "+insta_img);
          });
        }
      }else{
        console.log("No accessToken")
      }
      
    }

    openModal() {
      this.setState({ modalIsOpen: true });
    }
    afterOpenModal() {
      this.subtitle.style.color = '#f00';
    }
    closeModal() {
      this.setState({ modalIsOpen: false });
    }
    loadFreeImages() {  
      var currentState = this;
      if (!this.state.freeImagesLoaded) {
        fetch(window.baseURL+"/api/images/cp-all-image-categories",{credentials: 'include'})
          .then(res => res.json())
          .then(
            (result) => {
              var startCatImg =[],endCatImg=[],hasMoreCatImg=[];
              result.forEach(function(key,index){
                startCatImg[key.id] = 0;
                endCatImg[key.id] = 19;
                hasMoreCatImg[key.id] = true;
              });
              currentState.setState({
                freeImagesCat: result,
                startCatImg: startCatImg,
                endCatImg: endCatImg,
                hasMoreCatImg: hasMoreCatImg
              });
            },
            (error) => {
              currentState.setState({
                error
              });
            }
          )
        fetch(window.baseURL+"/api/images/cp-all-images",{credentials: 'include'})
          .then(res => res.json())
          .then(
            (result) => {
              var freeCatImagesParts= [];
              var freeCatImages=[];
              result.cat_images.forEach(function(key,index){
                freeCatImages[key.id] = key.images;
                freeCatImagesParts[key.id] = key.images.slice(currentState.state.startCatImg[key.id], currentState.state.endCatImg[key.id]);
              });
              currentState.setState({
                freeImagesLoaded: true,
                freeImages: result.all_images,
                freeCatImages: freeCatImages,
                freeImagesParts: result.all_images.slice(currentState.state.start, currentState.state.end),
                freeCatImagesParts: freeCatImagesParts
              });
            },
            (error) => {
              currentState.setState({
                freeImagesLoaded: true,
                error
              });
            }
          )
      }
    }
    loadShutterstockImages() {
      if (!this.state.shutterCatLoaded) {
      fetch(window.baseURL+"/api/images/shutterstock-images-categories",{credentials: 'include'})
      .then(res => res.json())
      .then(
        (result) => {
          var shutterPage =[];
          var shutterImagesParts = [];
          shutterPage['all'] = 1;
          shutterImagesParts['all'] = [];
              result.data.forEach(function(key,index){
              shutterPage[key.id] = 1;
              shutterImagesParts[key.id] = [];
              });
              this.setState({
                shutterCats: result.data,
                shutterPage: shutterPage,
                shutterImagesParts:shutterImagesParts,
                shutterCatLoaded:true
              });
        },
        (error) => {
          this.setState({
            error
          });
        }
      )
      }
      fetch(window.baseURL+"/api/images/shutterstock-images",{
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        credentials: 'include',
        body: JSON.stringify({
            per_page: this.state.shutterPerPage,
            page: 1,
            category_id: ''
        })
      })
      .then(res => res.json())
      .then(
        (result) => {
          var shutterImagesParts = this.state.shutterImagesParts;
              shutterImagesParts['all'] = result.data;
              this.setState({
                shutterImagesParts: shutterImagesParts
              });
        },
        (error) => {
          this.setState({
            error
          });
        }
      )
    }
    fetchMoreData() {
      var old_end = this.state.end;
      this.setState({ end: old_end + 20 });
      this.setState({ freeImagesParts: this.state.freeImages.slice(this.state.start, this.state.end) });
      if(this.state.freeImages.length <= this.state.end){
        this.setState({ hasMore: false });
      }
    }
    fetchMoreCatData(id) {
      var old_end_cat = this.state.endCatImg;
      old_end_cat[id] = old_end_cat[id] +20;
      this.setState({ endCatImg: old_end_cat });
      if(typeof this.state.freeCatImages[id] == undefined)
      {
        var freeCatImagesParts = this.state.freeCatImagesParts;
        freeCatImagesParts[id] = this.state.freeCatImages[id].slice(this.state.startCatImg[id], this.state.endCatImg[id]);
        this.setState({ freeCatImagesParts: freeCatImagesParts});

        if(this.state.freeCatImages[id].length <= this.state.endCatImg[id]){
          var hasMoreCatImg = this.state.hasMoreCatImg;
          hasMoreCatImg[id] = false;
          this.setState({ hasMoreCatImg: hasMoreCatImg });
        }
      }
      else
      {
        console.log("coming in undefined else");
      }
    }
    fetchShutterData(id) {
      var shutterImagesParts = this.state.shutterImagesParts;
        fetch(window.baseURL+"/api/images/shutterstock-images",{
          method: 'POST',
          headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
          },
          credentials: 'include',
          body: JSON.stringify({
              per_page: this.state.shutterPerPage,
              page: 1,
              category_id: id
          })
        })
        .then(res => res.json())
        .then(
          (result) => {
            shutterImagesParts[id] = result.data;
                this.setState({
                  shutterImagesParts: shutterImagesParts
                });
          },
          (error) => {
            this.setState({
              error
            });
          }
        )
    }
    fetchMoreShutterData(id) {
      var old_shutterPage = this.state.shutterPage;
      old_shutterPage[id] = parseInt(old_shutterPage[id]) + 1;
      this.setState({ shutterPage: old_shutterPage });
        var shutterImagesParts = this.state.shutterImagesParts;
        fetch(window.baseURL+"/api/images/shutterstock-images",{
          method: 'POST',
          headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
          },
          credentials: 'include',
          body: JSON.stringify({
              per_page: this.state.shutterPerPage,
              page: this.state.shutterPage[id],
              category_id: id
          })
        })
        .then(res => res.json())
        .then(
          (result) => {
            shutterImagesParts[id] = shutterImagesParts[id].concat(result.data);
                this.setState({
                  shutterImagesParts: shutterImagesParts
                });
          },
          (error) => {
            this.setState({
              error
            });
          }
        )
    }
    uploadImage() {
      window.editor.runCommand('open-assets');
    }

    render() {
  
      return (
        <div className="row" onClick={this.openModal}>
          <div className="col-md-3 image-img">
            <img alt="" width="80" src="https://png.kisspng.com/20180213/uqw/kisspng-camera-cartoon-clip-art-digital-camera-5a83a89dca1d26.3216328915185778218279.png" />
          </div>
          <div className="col-md-6 image-txt">
            <h4>Images</h4>
          </div>
  
          <div className="col-md-3 image-icon">
            <button className="btn"><i className="fa fa-chevron-right"></i></button>
          </div>
          <Modal
            isOpen={this.state.modalIsOpen}
            onAfterOpen={this.afterOpenModal}
            onRequestClose={this.closeModal}
            style={customStyles}
            contentLabel="Image Modal"
            overlayClassName="generic_modal"
  
          >
            <h2 ref={subtitle => this.subtitle = subtitle}>Manage your images <span className="modal_btn pull-right"> <button className="btn">?</button><button className="btn" onClick={this.closeModal}>X</button></span></h2>
            <div className="modal_inner">
              <Tabs>
                <TabList className="modal_top_tabs">
                  <Tab className="my-images-modal" > My Images</Tab>
                  <Tab className="social-images-modal">Social Images</Tab>
                  <Tab onClick={() => { this.loadFreeImages() }} className="free-from-controlpanda-modal">Free from ControlPanda</Tab>
                  <Tab className="shutterstock-images-modal" onClick={() => { this.loadShutterstockImages() }}>Shutterstock Images</Tab>
                  <button className="btn upload_btn pull-right" onClick={() => { this.uploadImage() }}><i className="fa fa-upload"></i> Upload Images</button>
                </TabList>
                <TabPanel>
                  <div className="Upload_image">
                    <div className="row">
                      <div className="col-md-6">
                        <p>
                          Upload or drag and drop images in JPEG, PNG or GIF formats. Each file can be up to 15MB.
                      </p>
                      </div>
  
                      <div className="col-md-6 text-right">
                        <button className="btn Delete_btn"><i className="fa fa-trash"></i> Delete</button>
                        <Dropdown className="docSort" options={sortOptions} onChange={this._onSelect} value={defaultSortOption} placeholder="Sort By" />
                        <div className="modal_search">
                          <i className="fa fa-search"></i> <input className="" type="text" placeholder="Search" />
                        </div>
                        <div className="layout_grid">
                          <Dropdown className="docDisplay" options={gridLayout} onChange={this._onSelect} value={defaultgridLayout} />
                        </div>
                      </div>
                    </div>
                  </div>
  
                  <Tabs>
                    <div className="row">
                      <div className="col-md-3">
                        <div className="Left_Inner_Tabs_wrap">
                          <TabList className="Left_Inner_Tabs">
                            <Tab className="all-media-sidebar">All Media</Tab>
                            <Tab className="purchased-sidebar">Purchased</Tab>
                            <Tab className="unsorted-sidebar">Unsorted</Tab>
                          </TabList>
                          <button><i className="fa fa-plus-square-o"></i> Add New Folder</button>
                        </div>
                      </div>
  
                      <div className="col-md-9 Right_Inner_Tabs">
                        <TabPanel>
                          <div className="Right_Tab_Content">
                            <img width="120" src="/images/photo-camera.png"/>
                            <h4>Add Images to Your Site</h4>
                            <p>Drag and drop them here, or click Upload Images.</p>
                            <p>Your images will also appear in your Site Media folder, so they're easy to find and use.
                              <label id="#bb"> Upload Image
                                <input type="file" id="File" size="60" />
                              </label>
                            </p>
                          </div>
                        </TabPanel>
                        <TabPanel>
                          <div className="Right_Tab_Content">
                            <img width="120" src="/images/photo-camera.png"/>
                            <h4>Purchase Pro Images</h4>
                            <p>For high quality images to purchase and own, browse the 
                              <button type="button" className="btn stock-collection"> Stock footage collection</button></p>
                          </div>
                        </TabPanel>
                        <TabPanel>
                          <div className="Right_Tab_Content">
                            <img width="120" src="/images/photo-camera.png"/>
                            <h4>Add Images to Your Site</h4>
                            <p>Drag and drop them here, or click Upload Images.</p>
                            <p>Your images will also appear in your Site Media folder, so they're easy to find and use. <label id="#bb"> Upload Image
                                <input type="file" id="File" size="60" />
                              </label></p>
                          </div>
                        </TabPanel>
                      </div>
                    </div>
                  </Tabs>
                  <div className="modal_bottom_btn">
                    <button className="">Add to Page</button>
                  </div>
                </TabPanel>
  
                <TabPanel>
                  <div className="social_images">
                    <p>
                      Add iamges from your Facebook, Instagram, Google Drive, Google Photo or Flickr accounts at the click of a button.
                    </p>
                    <div className="social_images_inner">
                      <h2>
                        All your Images, Right Here
                      </h2>
                      <p>
                        Instantly add your images from any of these social networks to your site.
                      </p>
                      <div className="social_btns">                       
                        
                        <SocialLogin
                          provider='facebook'
                          appId='153041438778802'
                          scope = "user_photos"
                          callback={handleSocialLogin}
                        >
                          <button>Facebook</button>
                        </SocialLogin>
                        <button> <a href="https://api.instagram.com/oauth/authorize/?client_id=9a98443bf4e9460d9a05153ce2d050f0&redirect_uri=http://localhost:8000/builder/995&response_type=token&scope=likes+comments+follower_list" >  </a> Instagram </button>
                        <SocialLogin
                          provider='dropbox'
                          appId='p8hiold50os988e'
                          callback={handleSocialLogin}
                        >
                          <button>Dropbox</button>
                        </SocialLogin>
                        <SocialLogin
                          provider='google'
                          appId='205318788731-cfj3pctnj7aaqe1bohg91g408vt623qm.apps.googleusercontent.com'
                          Scope = "https://www.googleapis.com/auth/drive"
                          callback={handleSocialLoginGoogleDrive}
                        >
                          <button>Google Drive</button>
                        </SocialLogin>
                        <SocialLogin
                          provider='flickr'
                          api_key='5a17879baf156d2969292016acd9bebf'
                          callback={handleSocialLogin}
                        >
                          <button>Flickr</button>
                        </SocialLogin>

                        <button id="login-button-google-photos">Google Photos</button>
                        

                      </div>
                    </div>

                    <div>
                      Show Images here.
                      {this.state.insta_img.map((img) => (
                        <div className="image-holder" key={img.id}>
                          <img alt="" src={img} />
                        </div>
                      ))} 
                    </div>
                  </div>
                  <div className="modal_bottom_btn"><button className="">Add to Page</button></div>
                </TabPanel>
  
                <TabPanel>
  
                  <div className="Upload_image">
                    <div className="row">
                      <div className="col-md-7">
                        <button className="btn theme_btn">Images</button>
                      </div>
  
                      <div className="col-md-5 text-xs-right">
                        <div className="modal_search">
                          <i className="fa fa-search"></i> <input className="" type="text" placeholder="Search" />
                        </div>
                        <div className="layout_grid">
                          <Dropdown className="docDisplay" options={gridLayout} onChange={this._onSelect} value={defaultgridLayout} />
                        </div>
                      </div>
                    </div>
                  </div>
                  <Tabs>
                  <div className="row">
                      <div className="col-md-3">
                        <div className="Left_Inner_Tabs_wrap">
                          <TabList className="Left_Inner_Tabs">
                            <Tab className="">All</Tab>
                            {this.state.freeImagesCat.map((cats) => (
                              typeof this.state.freeCatImagesParts[cats.id] == "undefined"  ? "" : 
                              <Tab key={'tab'+cats.id} className="">{cats.name}</Tab>
                            ))}
                          </TabList>
                        </div>
                      </div>
  
                      <div className="col-md-9">
  
                        <TabPanel>
                          <InfiniteScroll
                            dataLength={this.state.freeImagesParts.length}
                            next={this.fetchMoreData}
                            hasMore={this.state.hasMore}
                            loader={<b>Loading...</b>}
                            height={500}
                          >
                            {this.state.freeImagesParts.map((img) => (
                              <div className="image-holder" key={img.id}>
                                <i className="fa fa-check imageChecked" aria-hidden="true"></i>
                                <img alt="" src={window.baseURL+'/uploads/images/thumbnails/'+img.file_name} />
                              </div>
                            ))}
                          </InfiniteScroll>
                        </TabPanel>
                       {
                        this.state.freeImagesCat.map((cats) => (
                          typeof this.state.freeCatImagesParts[cats.id] == "undefined"  ? "" : 
                              <TabPanel key={'tabpanel'+cats.id}>
                              <InfiniteScroll
                            dataLength={this.state.freeCatImagesParts[cats.id].length}
                            next={() => this.fetchMoreCatData(cats.id)}
                            hasMore={this.state.hasMoreCatImg[cats.id]}
                            loader={<b>Loading...</b>}
                            height={500}
                          >
                            {
                              typeof this.state.freeCatImagesParts[cats.id] == "undefined"  ? "" : this.state.freeCatImagesParts[cats.id].map((img) => (
                              <div className="image-holder" key={'cat-img'+img.id}>
                                <img alt="" src={window.baseURL+'/uploads/images/thumbnails/'+img.file_name} />
                              </div>
                            ))}
                          </InfiniteScroll>
                              </TabPanel>
                        ))
                      }
                      </div>
                    </div>
                  </Tabs>
                  <div className="modal_bottom_btn">
                    <button className="" data-dismiss="modal" onClick={(event) => { this.closeModal(); addImagesToPage();}}>Add to Page</button>
                  </div>
  
                </TabPanel>
  
                <TabPanel>
                  <div className="Upload_image shutter_stock">
                    <div className="row">
                      <div className="col-md-8">
                        <p>
                          The Shutterstock collection offers high quality professional images that you can purchase and use accross your CP sites, at a discount price.
                        </p>
                        <div className="orientation_switch">
                          <div className="btn-group">
                            <button type="button" className="btn btn-primary"><i className="fa fa-picture-o"></i></button>
                            <button type="button" className="btn btn-primary"><i className="fa fa-picture-o"></i></button>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-4 text-xs-right">
                        <div className="modal_search">
                          <i className="fa fa-search"></i>
                          <input className="" type="text" placeholder="Search" />
                        </div>
                        <div className="layout_grid">
                          <Dropdown className="docDisplay" options={gridLayout} onChange={this._onSelect} value={defaultgridLayout} />
                        </div>
                      </div>
                    </div>
                  </div>
  
                  <Tabs>
                    <div className="row">
                      <div className="col-md-3">
                        <div className="Left_Inner_Tabs_wrap">
                          <TabList className="Left_Inner_Tabs">
                          { 
                            (typeof this.state.shutterImagesParts['all'] == "undefined") ? "" : <Tab className="">All</Tab> 
                          }
                            { 
                              this.state.shutterCats.map((cats) => (
                              (typeof this.state.shutterImagesParts[cats.id] == "undefined") ? "" : <Tab onClick={() => this.fetchShutterData(cats.id)} key={'shutterTab'+cats.id} className="">{cats.name}</Tab>
                            ))
                            }
                          </TabList>
                        </div>
                      </div>
  
                      <div className="col-md-9">
                      { (typeof this.state.shutterImagesParts['all'] == "undefined") ? "" :
                      <TabPanel key={'shuttertabpanelall'}>
                            <InfiniteScroll
                            dataLength={this.state.shutterImagesParts['all'].length}
                            next={() => this.fetchMoreShutterData('all')}
                            hasMore={this.state.hasMoreShutter}
                            loader={<b>Loading...</b>}
                            height={500}
                          >
                            {
                              typeof this.state.shutterImagesParts['all'] == "undefined"  ? "" : this.state.shutterImagesParts['all'].map((img) => (
                              <div className="image-holder shutterimg" key={'shutter-img'+img.id}>
                                <img alt="" src={img.assets.huge_thumb.url} />
                              </div>
                            ))}
                          </InfiniteScroll>
                        </TabPanel>
                      }
                      
                      { this.state.shutterCats.map((cats) => (
                        (typeof this.state.shutterImagesParts[cats.id] == "undefined") ? "" :
                        <TabPanel key={'shuttertabpanel'+cats.id}>
                            <InfiniteScroll
                            dataLength={this.state.shutterImagesParts[cats.id].length}
                            next={() => this.fetchMoreShutterData(cats.id)}
                            hasMore={this.state.hasMoreShutter}
                            loader={<b>Loading...</b>}
                            height={500}
                          >
                            {
                              typeof this.state.shutterImagesParts[cats.id] == "undefined"  ? "" : this.state.shutterImagesParts[cats.id].map((img) => (
                              <div className="image-holder shutterimg" key={'shutter-img'+img.id}>
                                <img alt="" src={img.assets.huge_thumb.url} />
                              </div>
                            ))}
                          </InfiniteScroll>
                        </TabPanel>
                        ))}
                      </div>
                    </div>
                  </Tabs>
                  <div className="modal_bottom_btn">
                    <button className="">Buy Images</button>
                  </div>
                </TabPanel>
              </Tabs>
            </div>
          </Modal>
        </div>
      );
    }
  } export default ModalImage;