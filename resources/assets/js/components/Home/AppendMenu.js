import React from 'react'
import Popup from './Popup'
import ConfirmationModal from './Modals/ConfirmationModal'

class AppendMenu extends React.Component{

    constructor(props) {
      super(props);
      this.enterFunction = this.enterFunction.bind(this);
      this.OnHideMenu = this.OnHideMenu.bind(this);
      this.OnDeleteMenu = this.OnDeleteMenu.bind(this);
      this.ConfirmDelete = this.ConfirmDelete.bind(this);
      this.onBlurInput = this.onBlurInput.bind(this);
      this.onAddSubMenu = this.onAddSubMenu.bind(this);

      this.state = {
        del : true,
        modal_state : false,
        hideMenu : false,
        myid:'',
        showPopup : false
      }
    }
  
    OnRenameMenu(readOnly,id) {
      this.props.renameMenu(readOnly, id)
    }  
    
    enterFunction(event){
      if(event.keyCode === 13) {
        this.props.pressEnterAfterRename();
      }
    }
  
    OnHideMenu(id){
      alert(id);
      this.props.onhideMenu(id);
    }
  
    OnDeleteMenu(id){
      alert(id);
      this.setState({
        modal_state : true
      });
    }

    ConfirmDelete(){
      this.setState({
        del :false
      });
    }

    onAddFolder(tname, cname){
      this.props.addFolder(tname, cname);
    }

    onAddSubMenu(id,name, item){
      console.log("Item is: "+item)
      this.props.addSubpage(id, name, item);
    }

    onAddMainMenu(id,name, item){
      console.log(item);
      this.props.addToMainMenu(id, name, item);
    }

    onBlurInput(){
      this.props.blurInput();
    }

    showPopup(){
      this.setState({
        showPopup : true
      });
    }
    render(){
      return(        
          <ul>
            <li className="SubMenu_wrap" style={{ display: this.state.del ? 'block' : 'none' }} >
              <i className="fa fa-minus"></i>
              <div className="pages-tiles">
                <input type="text" 
                  id={this.props.id}
                  className={this.props.className} 
                  placeholder={this.props.placeholder} 
                  onKeyDown={this.enterFunction} 
                  onBlur={this.onBlurInput} 
                  readOnly={this.props.readOnly === false && this.props.id === this.props.myid ? false  : true} 
                />
                <div className="page_right_btn" >
                
                  <Popup 
                    hideMenuCallBack={()=>this.OnHideMenu(id)} 
                    deleteMenu={(id)=>this.OnDeleteMenu(id)} 
                    tabName={this.props.tabName} 
                    className={this.props.className}
                    id={this.props.id}
                    isMain={this.props.isMain}
                    isSub={this.props.isSub}
                    mainMenu={this.props.mainMenu}
                    renameMenu={(readOnly, id)=>this.OnRenameMenu(readOnly, id)} 
                    duplicateMenu={(tname, cname)=>this.props.duplicatePage(tname, cname)} 
                    val_hide={this.props.hideMenu} 
                    addsubmenu={(id, name, item)=>this.onAddSubMenu(id, name, item)} 
                    onHideExtraPanel={this.props.onHideExtraPanel}
                  /> 
                </div>
              </div>
            </li>
            {this.props.subMenu ? this.props.subMenu.map((item,index)=>{
            return (
            <li>
              <ul className="SubMenu">
                <li>
                  <div className="pages-tiles"> 
                    <input type="text" 
                      id={item.id}
                      className={item.className} 
                      placeholder={item.placeholder} 
                      onKeyDown={this.enterFunction} 
                      onBlur={this.onBlurInput} 
                      readOnly={this.props.readOnly === false && item.id === this.props.myid ? false  : true}
                    />
                    <div className="page_right_btn">
                      <Popup 
                        hideMenuCallBack={()=>this.OnHideMenu(id)} 
                        deleteMenu={(id)=>this.OnDeleteMenu(id)} 
                        tabName={this.props.tabName} 
                        className={this.props.className}
                        id={this.props.id}
                        item={item}
                        renameMenu={(readOnly, id)=>this.OnRenameMenu(readOnly, item.id)} 
                        duplicateMenu={(tname, cname)=>this.props.duplicatePage(tname, cname)} 
                        addmainmenu={(id,name, item)=>this.onAddMainMenu(id,name, item)}
                        val_hide={this.props.hideMenu}  
                        onHideExtraPanel={this.props.onHideExtraPanel}
                      /> 
                    </div>
                  </div>
                </li> 
              </ul>  
            </li>  
             )
              }):""}
            {this.state.modal_state ? <ConfirmationModal confirmDel={(data)=>this.ConfirmDelete(data)} /> : ""}
          </ul>
      )
    }
  } export default AppendMenu;