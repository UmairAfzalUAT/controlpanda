import React from 'react';
import grapesjs from 'grapesjs';
import { ADDPROJECTDETAIL} from '../../constants/actionTypes';
import { Aviary } from 'grapesjs-aviary/dist/grapesjs-aviary.min.js';
import $ from 'jquery';
import { connect } from 'react-redux';

const mapStateToProps = state => {return {}};

const mapDispatchToProps = dispatch => ({
 addProjectDetail: (data) => dispatch({ type: ADDPROJECTDETAIL , payload:{projectDetail:data}})
});
class MainView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            project:[],
            currentPage:''
		}
	}
	componentDidMount()
	{
       var projectId = window.location.href.substr(window.location.href.lastIndexOf('/') + 1);
        fetch(window.baseURL+"/api/projects/"+projectId,{credentials: 'include'})
            .then(res => res.json())
            .then(
                (result) => {
                    var project = result.project;
                    this.setState({project: project,currentPage: project.pages[0].name,projectId: projectId,currentPageIndex: 0});
                    this.props.addProjectDetail({project: project,currentPage: project.pages[0].name,projectId:projectId,currentPageIndex: 0})
                    window.editor = grapesjs.init({
                        container: '#playground-editor',
                        components: project.pages[0].html,
                        style: project.css,
                        height: '100%',
                        plugins: ['gjs-aviary'],
                        pluginsOpts: {
                            'gjs-aviary': {}
                        },
                        storageManager: {
                            type: 'remote',
                            stepsBeforeSave: 100
                        }
                    });
                    
                    /*************Umer Surkhail Code For Dragging Start**************/
                    var top = 0;
                    var left = 0;
                    var elementSelectedPositionTopPercent = 0; 
                    var elementSelectedPositionleftPercent = 0;
                    window.newElement = true;
                    window.incrementalValueForAttribute = 0;
                    window.zIndex = 999999999;
                    var currentElementToDrag;
                    var mY = 0;
                    var upwordDirection = 0;
                    var globalLeft = 0;
                    var globalTop = 0;
                    //check the percentage from where the handle was grabbed (left, top)
                    $("body").on("mousedown", ".singleElementDragger", function(evt)
                    {
                        var offset = $(this).offset();
                        var offsetLeft = parseInt(evt.pageX - offset.left);
                        var offsetTop = parseInt(evt.pageY - offset.top);
                        var elementWidth = parseInt($(this).width());
                        var elementHeight = parseInt($(this).height());
                        elementSelectedPositionleftPercent = parseInt((offsetLeft/elementWidth) * 100);
                        elementSelectedPositionTopPercent = parseInt((offsetTop/elementHeight) * 100);
                    });

                    // window.editor.on('component:selected', () => {
                    //     const selected = editor.getSelected();
                    //     if (!selected || !selected.get('draggable')) return;
                    //     const el = selected.view.el;
                    //     if (!el._hasCustomEvent) {
                    //         el._hasCustomEvent = 1;
                    //         el.addEventListener('mousedown', () => { editor.runCommand('tlb-move') })
                    //     }
                    // });
                    
                     // this function is called recursively while element is being dragged
                     window.editor.on('sorter:drag', function (dt, model)
                     {
                         globalLeft = dt.x;
                         globalTop = dt.y;
                     });
                    
                     $(document).keydown(function(e)
                     {
                        var currentElementSelected = $('iframe').contents().find('html').find('.gjs-comp-selected');
                        var currentToolBar = $('.gjs-toolbar');
                        console.log(currentToolBar);
                        if(currentElementSelected.length)
                        {
                            e.preventDefault(); // prevent the default action (scroll / move caret)
                            console.log("element is selected");
                            switch(e.which)
                            {
                                case 37: // left
                                var thisElementLeft = currentElementSelected.offset().left;
                                currentElementSelected.css({"left": thisElementLeft-2});
                                var toolBoxLeft = currentToolBar.offset().left;
                                currentToolBar.css({"left": toolBoxLeft-2});
                                break;
                        
                                case 38: // up
                                var thisElementTop = currentElementSelected.offset().top;
                                currentElementSelected.css({"top": thisElementTop-2});
                                currentToolBar.css({"top": thisElementTop-28});
                                break;
                        
                                case 39: // right
                                var thisElementLeft = currentElementSelected.offset().left;
                                currentElementSelected.css({"left": thisElementLeft+2});
                                var toolBoxLeft = currentToolBar.offset().left;
                                currentToolBar.css({"left": toolBoxLeft+2});
                                break;
                        
                                case 40: // down
                                var thisElementTop = currentElementSelected.offset().top;
                                currentElementSelected.css({"top": thisElementTop+2});
                                currentToolBar.css({"top": thisElementTop-24});
                                break;
                        
                                default: return; // exit this handler for other keys
                            }
                        }
                    });
                    
                    $('iframe').contents().find('html').find('body').on('mousedown', ".componentElement", function(e)
                    {
                        if($(this).css("position")=="absolute")
                        {
                            e.preventDefault();
                            var offset = $(this).offset();
                            var offsetLeft = parseInt(e.pageX - offset.left);
                            var offsetTop = parseInt(e.pageY - offset.top);
                            var currentThis = this;
                            var currentThisJQuery = $(this);
                            var elementWidth = currentThisJQuery.outerWidth();
                            
                            $('iframe').contents().find('html').find('body').on('mousemove', function(event)
                            {
                                //console.log("start mousemove");
                                if(currentThisJQuery.hasClass("fullSizeElement"))
                                {
                                    left = 0;
                                }
                                else
                                {
                                    left = event.pageX-offsetLeft;
                                }
                                top = e.pageY;
                                $(currentThis).css({"top": event.pageY-offsetTop, "position": "absolute","left": left});
                                if(currentThisJQuery.hasClass('gjs-comp-selected'))
                                {
                                    $('.gjs-toolbar').css({"top": event.pageY-offsetTop-26,"left": (event.pageX-offsetLeft+elementWidth-115)});
                                }
                            });
                            $('iframe').contents().find('html').find('body').on('mouseup', function()
                            {
                                //console.log("stop mousemove");
                                $(this).unbind("mousemove");
                            });
                        }
                    });


					//the function whent the drag is started for the elements on console
					window.editor.on('sorter:drag:start', function (dt, currentComp)
					{
                        //return false;
                        //this function should be called only if the element is already in the DOM,
                        // and not the new one, because for the new element the function is "canvas:drop"
                        if(currentComp)
                        {
                            currentElementToDrag =$(dt);
                            var currentElementSelectedHere = $('iframe').contents().find('html').find( "."+currentComp.attributes.attributes.componentUniqueAttribute );
                            var currentWidth = (currentElementToDrag.outerWidth())-87;
                            //if the element has absolute position, handle it absolutely,
                            //else the framework already handles relative positions
                            if(currentElementToDrag.css("position")=="absolute")
                            {
                                $('iframe').contents().find('html').on('mousemove', function (e)
                                {
                                    // if(upwordDirection)
                                    // {
                                        
                                    // }
                                    // else
                                    // {
                                        
                                    // }
                                    if(currentElementSelectedHere.hasClass("fullSizeElement"))
                                    {
                                        left = 0;
                                    }
                                    else
                                    {
                                        left = e.pageX-currentWidth;
                                    }
                                    top = e.pageY;
                                    
                                    currentElementToDrag.css({"top": top, "position": "absolute","left": left})
                                });
                            }
                        }
                    });
					window.editor.on('sorter:drag:end', (dt, model) => {
                        console.log("called");
                        if(!window.newElement)
                        {
                            setTimeout(function()
                            {
                                var currentElementSelectedHere = $('iframe').contents().find('html').find( "."+model.attributes.attributes.componentUniqueAttribute );
                                currentElementSelectedHere.css({"top": top, "position": "absolute","left": left});
                                $('iframe').contents().find('html').unbind( "mousemove" );
                                // model.setStyle(
                                // {
                                //     "z-index": "999999999",
                                //     "top": top,
                                //     "position": "absolute",
                                //     "left": left
                                // });
                            },0)
                        }
					});
					
					window.editor.on('canvas:dragenter', (dt, model) => {
						window.newElement = true;
					});
					
					window.editor.on('canvas:drop', (dt, model) => {
                        //model.set({draggable:false});
                        window.newElement = false;
                        model.attributes.attributes.componentUniqueAttribute = 'componentUniqueAttribute'+window.incrementalValueForAttribute;
                        model.addClass('componentUniqueAttribute'+window.incrementalValueForAttribute);
                        window.incrementalValueForAttribute++;
                        
                        if(model.attributes.attributes.fullsizeelement)
                        {
                            model.addClass('excludeNewElement');
                            var draggedElementHeight = parseInt(model.attributes.attributes.height);
                            var elementsAfterFullSizeElement = [];
                            var beforeElementsEndsAt = 0;
                            var afterElementsStartAt = 99999999;
                            //$('iframe').contents().find('html').on('mousemove', function (e)
                            {
                                $('iframe').contents().find('html').unbind( "mousemove" );
                                var currentElementsInDom = $('iframe').contents().find('html').find(".componentElement:not(.excludeNewElement)");
                                $('iframe').contents().find('html').find(".excludeNewElement").removeClass('excludeNewElement');
                                var currentIteration = 0;
                                currentElementsInDom.each(function(index, element)
                                {
                                    var currentElement = $(element);
                                    var currentElementTop = currentElement.offset().top;
                                    var currentElementHeight = currentElement.height();
                                    var currentElementEndsAt = currentElementTop + currentElementHeight;
                                    
                                    if(((currentElementTop)+(currentElementHeight/2)) > globalTop)
                                    {
                                        elementsAfterFullSizeElement.push(currentElement);
                                        if(currentElementTop < afterElementsStartAt)
                                        {
                                            afterElementsStartAt = currentElementTop;
                                        }
                                    }
                                    else
                                    {
                                        if(currentElementEndsAt > beforeElementsEndsAt)
                                        {
                                            beforeElementsEndsAt = currentElementEndsAt;
                                        }
                                    }
                                    currentIteration++;
                                    if(currentIteration == currentElementsInDom.length)
                                    {
                                        // if there is not any after element
                                        if(afterElementsStartAt == 99999999)
                                        {
                                            model.setStyle(
                                            {
                                                "z-index": window.zIndex,
                                                "top": (beforeElementsEndsAt),
                                                    "position": "absolute",
                                                "left": 0
                                            });
                                            window.zIndex++;
                                        }
                                        //if there are both, before and after elements and the space is less than the height
                                        else if(parseInt(afterElementsStartAt-beforeElementsEndsAt) < draggedElementHeight)
                                        {
                                            for(var i=0;i<elementsAfterFullSizeElement.length; i++)
                                            {
                                                elementsAfterFullSizeElement[i].css("top", (elementsAfterFullSizeElement[i].offset().top + (draggedElementHeight-(afterElementsStartAt-beforeElementsEndsAt)) ));
                                            }
                                            model.setStyle(
                                            {
                                                "z-index": window.zIndex,
                                                "top": (beforeElementsEndsAt),
                                                    "position": "absolute",
                                                "left": 0
                                            });
                                            window.zIndex++;
                                        }
                                        else if(parseInt(afterElementsStartAt-beforeElementsEndsAt) >= draggedElementHeight)
                                        {
                                            model.setStyle(
                                            {
                                                "z-index": window.zIndex,
                                                "top": (beforeElementsEndsAt),
                                                    "position": "absolute",
                                                "left": 0
                                            });
                                            window.zIndex++;
                                        }
                                        else
                                        {
                                            console.log("else iterations");
                                        }
                                    }
                                });
                            }//);
                        }
                        else
                        {
                            
                            var elementToFindParent = $('iframe').contents().find('html').find(".componentUniqueAttribute"+(window.incrementalValueForAttribute-1));
                            var firstParent = elementToFindParent.parents().filter(function()
                            {
                                // reduce to only relative position or "body" elements
                                var $this = $(this);
                                return $this.is('body') || $this.css('position') == 'relative';
                            }).slice(0,1); // grab only the "first"
                            
                            var firstParentTop = firstParent.offset().top;
                            var firstParentLeft = firstParent.offset().left;
                            var position = '';
                            
                            if(firstParent[0].nodeName.toLowerCase()==='body')
                            {
                                position = 1;
                            }
                            else
                            {
                                position = 0;
                            }

                            var width = 0;
                            var height = 0;
                            if(model.attributes.attributes.height)
                            {
                                height = parseInt(model.attributes.attributes.height);
                                top = (globalTop - firstParentTop - parseInt((elementSelectedPositionTopPercent * height) / 100));
                                //top = e.pageY;
                            }
                            else
                            {
                                top = globalTop - firstParentTop;
                            }

                            if(model.attributes.attributes.width)
                            {
                                width = parseInt(model.attributes.attributes.width);
                                left = globalLeft - firstParentLeft -parseInt((elementSelectedPositionleftPercent * width) / 100);
                            }
                            else
                            {
                                left = globalLeft - firstParentLeft;
                            }
                            if(position)
                            {
                                model.setStyle(
                                {
                                    "z-index": window.zIndex,
                                    "top": top,
                                    "position": "absolute",
                                    "left": left
                                });
                            }
                            window.zIndex++;
                            $('iframe').contents().find('html').unbind( "mousemove" );
                        }
					});
					
					/*************Umer Surkhail Code For Dragging End**************/
					
                    window.editor.Panels.removeButton("views", "open-blocks");
                    window.editor.Panels.removeButton("views", "open-layers");
                },
                (error) => {
                   console.log(error);
                }
            )
    }
    render() {
        return ( <div className = "playground"> <div id = "playground-editor"> </div></div> )
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(MainView);
