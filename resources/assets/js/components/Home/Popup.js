
import React from 'react'
import FontAwesome from 'react-fontawesome'
import { PopupMenu, PopupTable } from 'react-rectangle-popup-menu'

const showPopupButton = (<FontAwesome name="ellipsis-h" />);

class Popup extends React.Component {
    constructor(props) {
      super(props);
    }

  render() {
    return (
      <div>
        <PopupMenu width={190} direction="right" button={showPopupButton}>
          <PopupTable rowItems={1}>
            <div className="PopupTable_item_inner settings" >
              <FontAwesome name="cog" />
              <span onClick={()=> this.props.onHideExtraPanel()} >Settings</span>
            </div>
            <div className="PopupTable_item_inner page-seo" >
              <FontAwesome name="search" />
              <span onClick={()=> this.props.onHideExtraPanel()} >Page SEO</span>
            </div>
            <div className="PopupTable_item_inner rename">
              <FontAwesome name="text-width" />
              <span onClick={()=>this.props.renameMenu(false, this.props.id)} >Rename</span>
            </div>
            <div className="PopupTable_item_inner duplicate" >
              <FontAwesome name="files-o" />
              <span onClick={()=>this.props.duplicateMenu(this.props.tabName, this.props.className)} >Duplicate</span>
            </div>
            {this.props.hideMenu ?
            <div className="PopupTable_item_inner menu-hide">
              <FontAwesome name="eye-slash" /> 
                <span onClick={()=>this.props.hideMenuCallBack(this.props.id)} > Hide </span>
            </div> : 
            <div className="PopupTable_item_inner menu-show">
              <FontAwesome name="eye" />  
                <span onClick={()=>this.props.hideMenuCallBack(this.props.id)} > Show </span>
            </div>
            }
            <div className="PopupTable_item_inner menu-del" style={{ display: this.props.id === 'main0' ? 'none' : 'block' }} >
              <FontAwesome name="trash" />
              <span onClick={()=>this.props.deleteMenu(this.props.id)} >Delete</span>
            </div>  
            <div className="PopupTable_item_inner">
              <FontAwesome name="level-up" />
              <span onClick={()=>this.props.addmainmenu(this.props.id, this.props.tabName, this.props.item)} >Main Page</span>
            </div> 
            <div className="PopupTable_item_inner sub_page" >
              <FontAwesome name="level-up" />
              <span onClick={()=>this.props.addsubmenu(this.props.id, this.props.tabName, this.props.mainMenu)} >Sub Page</span>
            </div> 
          </PopupTable>
        </PopupMenu>
      </div>
    )
  }
} export default Popup;