import React from 'react'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs'
import Modal from 'react-modal'
import 'react-dropdown/style.css'
import { SketchPicker } from 'react-color'
import { connect } from 'react-redux';
  // Our Own Imported Files

/************ JS Imported Files for Drag Drop Featues Start ************/
import ToolBoxItem from './Toolboxes/ToolBoxItem.js'
import ToolBoxGallery from './Toolboxes/ToolBoxGallery.js'
import ToolBoxButton from './Toolboxes/ToolBoxButton.js'
import ToolBoxHoverInteractive from './Toolboxes/ToolBoxHoverInteractive'
import ToolBoxSliderInteractive from './Toolboxes/ToolBoxSliderInteractive.js'
import ToolBoxSliderBox1Interactive from './Toolboxes/ToolBoxSliderBox1Interactive.js'
import ToolBoxSliderBox2Interactive from './Toolboxes/ToolBoxSliderBox2Interactive.js'
import ToolBoxStripAbout from './Toolboxes/ToolBoxStripAbout.js'
import ToolBoxStripContactUs from './Toolboxes/ToolBoxStripContactUs.js';
import ToolBoxStripWelcome from './Toolboxes/ToolBoxStripWelcome.js';
import ToolBoxStripServices from './Toolboxes/ToolBoxStripServices.js';
import PopularSocialIcons from './Toolboxes/PopularSocialIcons.js';
import SocialIconsBar from './Toolboxes/SocialIconsBar.js';  
import Contact from './Toolboxes/Contact.js';  
import ContactGetSubcribers from './Toolboxes/ContactGetSubcribers.js';
import ToolBoxThemedMenu from './Toolboxes/ToolBoxThemedMenu.js'
import ToolBoxHorizontalMenu from './Toolboxes/ToolBoxHorizontalMenu.js'
import ToolBoxVerticalMenu from './Toolboxes/ToolBoxVerticalMenu.js'
import ToolBoxAnchorMenu from './Toolboxes/ToolBoxAnchorMenu.js'
import ToolBoxShapesBasic from './Toolboxes/ToolBoxShapesBasic.js'
import ToolBoxShapesArrows from './Toolboxes/ToolBoxShapesArrows.js'
import ToolBoxShapesHorizontal from './Toolboxes/ToolBoxShapesHorizontal.js'
/************ JS Imported Files  for Drag Drop Featues End ************/
import UpgradePopup from './UpgradePopup'
import ExtraPanel from './ExtraPanel'
import AppendMenu from './AppendMenu'
import ModalImage from './Modals/ModalImage'
import ModalVector from './Modals/ModalVector'
import ModalVideos from './Modals/ModalVideos'
import ModalFonts from './Modals/ModalFonts'
import ModalDocs from './Modals/ModalDocs'
import ModalTracks from './Modals/ModalTracks'
import CategoriesModal from './Modals/CategoriesModal'


Modal.setAppElement('#root');
var maincount = 0;
var subcount = 0;

const mapStateToProps = state => {
  return {
      common: state.common
  }};

const mapDispatchToProps = dispatch => ({

});

class InspectorTab extends React.Component {
  constructor(props) {
    super(props);
    this.handleMouseHoverUpgrade = this.handleMouseHoverUpgrade.bind(this);
    this.handleMouseHoverTools = this.handleMouseHoverTools.bind(this);
    this.OnClickShowPanel = this.OnClickShowPanel.bind(this);
    this.addToMainMenu = this.addToMainMenu.bind(this);
    this.onhideMenu = this.onhideMenu.bind(this);
    this.onBlurInput = this.onBlurInput.bind(this);
    this.OnRenameMenu = this.OnRenameMenu.bind(this);
    this.pressEnterAfterRename = this.pressEnterAfterRename.bind(this);

    this.state = {
      isHoveringTools: false,
      isHoveringUpgrade: false,
      showGridLines: true,
      showToolbar: true,
      showPanel: false,
      hideMenu: false,
      readOnly: true,
      id:'',
      myMainClass: 'home-page-menu',
      mySubClass: 'sub-page-menu',
      projectDetail: [],
      mainMenu:[
        {
          className:'home-page-menu',
          placeholder:'Home',
          tabName:'Home',
          id:'main'+maincount,
          subMenu:[
            {
              className:'sub-menu-page',
              placeholder:'SubMenu',
              tabName:'SubMenu',
              id:'sub'+subcount++
            },
            {
              className:'sub-menu-page',
              placeholder:'SubMenu',
              tabName:'SubMenu',
              id:'sub'+subcount++
            }
          ]
        }
      ]
    }
  }

  componentDidMount(){
   /* this.setState({
      projectDetail: this.props.common.projectDetail
  });*/
  }

  OnClickShowPanel() {
    this.setState({showPanel: !this.state.showPanel})
  }

  //  ** Upgrade Hover Start **
  handleMouseHoverUpgrade() {
    this.setState(this.toggleHoverStateUpgrade)
  }
  toggleHoverStateUpgrade(state) {
    return {
      isHoveringUpgrade: !state.isHoveringUpgrade
    }
  }
  //  ** Upgrade Hover End **

  //  ** Tools Hover Start **
  handleMouseHoverTools() {
    this.setState(this.toggleHoverStateTools);
  }

  toggleHoverStateTools(state) {
    return {
      isHoveringTools: !state.isHoveringTools
    }
  }

  hideTabsMain() {
    document.getElementById("react-tabs-0").click();
  }
  //  ** Tools Hover End **

  showHideGridLines() {
    this.setState({ showGridLines: !this.state.showGridLines });
    if (this.state.showGridLines) {
      window.editor.stopCommand('sw-visibility');
    } else {
      window.editor.runCommand('sw-visibility');
    }
  }

  showHideToolbar() {
    this.setState({ showToolbar: !this.state.showToolbar });
    if (this.state.showToolbar) {
      document.getElementsByClassName('gjs-pn-panels')[0].style.display = "none";
    } else {
      document.getElementsByClassName('gjs-pn-panels')[0].style.display = "block";
    }
  }

  handleDrag(event) {
    event.dataTransfer.setData("text", event.target.outerHTML);
  }
  
  addPage(){
    let arr = Object.assign(this.state.mainMenu);
    maincount = maincount + 1;
    arr.push({
      className:"home-page-menu",
      placeholder:"New Page",
      tabName:"page",
      id:'main'+maincount
    }) 
    this.setState({
      mainMenu:arr
    })
  }

  addToSubPage(id,name, mitem){
    debugger;
    let array = Object.assign(this.state.mainMenu);
    subcount = subcount + 1;
    this.state.mainMenu.map((item, i)=>{
     // if (item.id === id && item.id === array[i].id && id === array[i].id){
        // var res = array.splice(array[i].id, 1);
        // if(res){
          //alert("data deleted successfully");
        // }
     // }
      if(item.subMenu){
        let arr = Object.assign(item.subMenu);
        subcount++;
        arr.push({
          className:"sub-page-menu",
          placeholder:mitem.placeholder,
          tabName:mitem.tabName,
          id:'sub'+subcount
        }) 
        this.setState({
          subMenu:arr
        })
      }
    })
  }

  addToMainMenu(id,name, item){
    let array = Object.assign(this.state.mainMenu);
    this.state.mainMenu.map((item1, i)=>{
      if(item1.subMenu){
        let arr = Object.assign(item1.subMenu);
        maincount++;
        var res = arr.splice(arr.id, 1);
        if(res){
          console.log("data deleted successfully");
        }
      }
    });
 
    array.push({
      className: "home-page-menu",
      placeholder:item.placeholder,
      tabName:item.tabName,
      id:'main'+maincount
    }) 
    this.setState({
      subMenu:array
    })
  }

  duplicatePage(tname, cname){
    let arr = Object.assign(this.state.mainMenu);
    ++maincount;
    console.log("For Duplicate Menu Tab: "+tname);
    arr.push({
      className:"home-page-menu",
      placeholder:tname,
      tabName:tname,
      id:'main'+maincount
    }) 
    this.setState({
      mainMenu:arr
    })
  }

  onhideMenu(id){
    console.log("Hidden Id is: "+id);
    this.setState({hideMenu: !this.state.hideMenu});
  }

  OnRenameMenu(readOnly, id) {
    console.log("Rename is: "+id);
    this.setState({
      readOnly: false,
      id: id
    });
  }

  onBlurInput(){
    console.log("I am in Blurr");
    this.setState({
      readOnly: true
    });
  }

  pressEnterAfterRename(){
    this.setState({
      readOnly: true
    });
  }

  render() {
    var textTitles = require('./templates/TextTitles.json').containers[0];
    var gallaryTitles = require('./templates/gallaryTitles.json').containers[0];
    var buttonTitles = require('./templates/ButtonTitles.json').containers[0];
    var interactiveTitles = require('./templates/InteractiveHoverBoxTitles.json').containers[0];
    var interactiveSliderTitles = require('./templates/InteractiveSliderTitles.json').containers[0];
    var interactiveSliderBox1Titles = require('./templates/InteractiveSliderBox1Titles.json').containers[0];
    var interactiveSliderBox2Titles = require('./templates/InteractiveSliderBox2Titles.json').containers[0];
    var stripAbout = require('./templates/StripAbout.json').containers[0];
    var stripContactUs = require('./templates/StripContactUs.json').containers[0];
    var stripWelcome = require('./templates/StripWelcome.json').containers[0];
    var stripServices = require('./templates/StripServices.json').containers[0];
    var popularSocialIcons = require('./templates/PopularSocialIcons.json').containers[0];
    var socialIconsBar = require('./templates/SocialIconsBar.json').containers[0];
    var contact = require('./templates/Contact.json').containers[0];
    var contactGetSubcribers = require('./templates/ContactGetSubcribers.json').containers[0];
    var menuThemedTitles = require('./templates/MenuThemedTitles.json').containers[0];
    var menuHorizonatlTitles = require('./templates/MenuHorizontalTitles.json').containers[0];
    var menuVerticalTitles = require('./templates/MenuVerticalTitles.json').containers[0];
    var menuAnchorTitles = require('./templates/MenuAnchorTitles.json').containers[0];
    var shapesBasicTitles = require('./templates/ShapesBasicTitles.json').containers[0];
    var shapesArrowsTitles = require('./templates/ShapesArrowsTitles.json').containers[0];
    var shapesHorizontalTitles = require('./templates/ShapesHorizontalTitles.json').containers[0];

    var button1theme = {
      padding:'8px 25px',
      fontSize:'18px',
      cursor:'pointer',
      outline:0,
      transition:'all .3s',
      fontWeight:700,
      border:'none'
    };
    var button2theme = {
      padding:'8px 25px',
      fontSize:'18px',
      cursor:'pointer',
      outline:0,
      transition:'all .3s',
      fontWeight:700,
      border:'2px solid #edf1f5',
      color:'#edf1f5',
      background:'0 0'
    };
    var button3theme = {
      padding:'8px 25px',
      fontSize:'18px',
      cursor:'pointer',
      outline:0,
      transition:'all .3s',
      fontWeight:700,
      border:'2px solid #edf1f5',
      color:'#020202',
      background:'#edf1f5'
    };
    return (
      <Tabs>
        <TabList>
          <Tab className="dummy-tab"></Tab>
          <Tab className="menu-pages"><i className="flaticon-menu-button-of-three-horizontal-lines"></i><span> Menu & Pages</span></Tab>
          <Tab className="my-uploads"><i className="flaticon-background"></i><span>Background</span></Tab>
          <Tab className="add"><i className="flaticon-mathematical-addition-sign"></i> <span>Add Element</span></Tab>
          <Tab className="add-apps"><i className="flaticon-add"></i> <span>Select CP Apps</span></Tab>
          <Tab className="background"><i className="flaticon-upload"></i><span>My Uploads</span></Tab>
          <Tab className="schedule"><i className="flaticon-time-left"></i> <span>Schedule</span></Tab>
          <Tab className="start-bloging"><i className="flaticon-blogger-logo"></i> <span>Create CP Blog</span></Tab>
          <Tab className="upgrade"><i className="flaticon-up-arrow"></i> <span>Upgrade</span></Tab>
          <Tab className="tools"><i className="flaticon-settings"></i> <span>Tools</span></Tab>
        </TabList>
        <TabPanel className="dummy-tab-pannel"></TabPanel>
        <TabPanel className="left_panel_wrap menu_panel_wrap">
          <Tabs>
            <TabList>
              <Tab className="site-menu">Site Menu</Tab>
              <Tab className="page-transations">Page Transitions</Tab>
            </TabList>
            <TabPanel>
              <div className="tab-panel-head"> Site Menu
                <span className="modal_btn pull-right">
                  <button className="btn">?</button>
                  <button className="btn" onClick={() => { this.hideTabsMain() }}>X</button>
                </span>
              </div>
              <div className="tab-panel-body">
                <div className="menu_panel_inner">
                  {this.state.mainMenu.map((item, index)=>{
                    return <AppendMenu key={name+index} 
                      duplicatePage={(tname, cname)=>this.duplicatePage(tname, cname)} 
                      addSubpage={(id,name,item)=>this.addToSubPage(id,name,item)} 
                      subMenu={item.subMenu}
                      mainMenu={item} 
                      className={item.className} 
                      id={item.id} 
                      tabName={item.tabName} 
                      placeholder={item.placeholder}
                      readOnly={this.state.readOnly}
                      addToMainMenu={(id,name, item)=>this.addToMainMenu(id,name, item)} 
                      onhideMenu={(name)=>this.onhideMenu(name)}
                      hideMenu={this.state.hideMenu}
                      isMain={this.state.myMainClass}
                      isSub={this.state.mySubClass}
                      blurInput={()=>this.onBlurInput()}
                      renameMenu={(readOnly,id)=>this.OnRenameMenu(readOnly,id)}
                      pressEnterAfterRename={()=>this.pressEnterAfterRename()}
                      myid={this.state.id}
                      onHideExtraPanel={this.OnClickShowPanel}
                      />
                    })
                  }
                </div> 
                {this.state.showPanel ? <ExtraPanel onHideExtraPanel={this.OnClickShowPanel} /> : null} 
              </div>
              <div className="tab-panel-footer">
                <button className="btn theme_btn" onClick={()=>this.addPage()}>Add Page</button>
                <button className="btn theme_btn">Link</button>
              </div>
              
            </TabPanel>
            <TabPanel>
              <div className="tab-panel-head">Page Transitions
                  <span className="modal_btn pull-right">
                  <button className="btn" >?</button>
                  <button className="btn" onClick={() => { this.hideTabsMain() }}>X</button>
                </span>
              </div>
              <div className="tab-panel-body col-md-12">
                <div className="row">
                  <div className="col-md-4">
                    <div className="transitions-tiles_wrap">
                      <div className="transitions-tiles">
                        <i className="fa fa-ban"></i>
                      </div>
                      <a href="">None</a>
                    </div>
                  </div>
                  <div className="col-md-4">
                    <div className="transitions-tiles_wrap">
                      <div className="transitions-tiles">
                        <i className="fa fa-file-text"></i>
                      </div>
                      <a href="">Horizontal</a>
                    </div>
                  </div>
                  <div className="col-md-4">
                    <div className="transitions-tiles_wrap">
                      <div className="transitions-tiles">
                        <i className="fa fa-file-text"></i>
                      </div>
                      <a href="">Vertical</a>
                    </div>
                  </div>
                  <div className="col-md-4">
                    <div className="transitions-tiles_wrap">
                      <div className="transitions-tiles">
                        <i className="fa fa-files-o"></i>
                      </div>
                      <a href="">Cross Fade</a>
                    </div>
                  </div>
                  <div className="col-md-4">
                    <div className="transitions-tiles_wrap">
                      <div className="transitions-tiles">
                        <i className="fa fa-file-o"></i>
                      </div>
                      <a href="">Out-in</a>
                    </div>
                  </div>
                  <ul id="authors"></ul>
                </div>
              </div>
            </TabPanel>
          </Tabs>
        </TabPanel>
        <TabPanel className="left_panel_wrap page_bg_panel">
          <div className="tab-panel-head">Page Background
            <div className="modal_btn pull-right">
              <button className="btn">?</button>
              <button className="btn" onClick={() => { this.hideTabsMain() }}>X</button>
            </div>
          </div>
          <Tabs>
            <div className="tab-panel-body">
              <div className="generic_tabs">
                <TabList>
                  <Tab className="flaticon-painter-palette"> Color </Tab>
                  <Tab className="flaticon-picture"> Image </Tab>
                  <Tab className="flaticon-video-camera"> Video </Tab>
                </TabList>
              </div>
              <div className="background-tiles-wrap">
                <div className="page_bg_tabs_inner">
                  <div className="row">
                    <TabPanel>
                      <SketchPicker />
                    </TabPanel>
                    <TabPanel>
                      <ModalImage modalState={true}/>
                    </TabPanel>
                    <TabPanel>
                      <ModalVideos modalState={true}/>
                    </TabPanel>
                  </div>
                </div>
              </div>
            </div>
          </Tabs>
        </TabPanel>
        <TabPanel className="left_panel_wrap add_data_wrap">
          <Tabs>
            <TabList>
              <Tab className="text">Text</Tab>
              <Tab className="image">Image</Tab>
              <Tab className="gallery">Gallery</Tab>
              <Tab className="vector-art">Vector Art</Tab>
              <Tab className="shape">Shape</Tab>
              <Tab className="interactive">Interactive</Tab>
              <Tab className="button">Button</Tab>
              <Tab className="box">Box</Tab>
              <Tab className="strip">Strip</Tab>
              <Tab className="video">Video</Tab>
              <Tab className="music">Music</Tab>
              <Tab className="social">Social</Tab>
              <Tab className="contact">Contact</Tab>
              <Tab className="menu">Menu</Tab>
              <Tab className="lightbox">Lightbox</Tab>
              <Tab className="more">More</Tab>
            </TabList>
            <TabPanel>
              
              <div className="tab-panel-head">Add Text
                <div className="modal_btn pull-right">
                  <button className="btn">?</button>
                  <button className="btn" onClick={() => { this.hideTabsMain() }}>X</button>
                </div>
              </div>
              <div className="tab-panel-body" id="text-panel-body">
                
                  <div>
                    <h5 className="inner-heading">Themed Text <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <h1 className="text-tile singleElementDragger componentElement" height="44px" width="257px" title="Add Heading 1" onDragStart={this.handleDrag} draggable="true">Add Heading 1</h1>
                    <h2 className="text-tile singleElementDragger componentElement" height="35px" width="205px" title="Add Heading 2" onDragStart={this.handleDrag} draggable="true">Add Heading 2</h2>
                    <h3 className="text-tile singleElementDragger componentElement" height="30px" width="180px" title="Add Heading 3" onDragStart={this.handleDrag} draggable="true">Add Heading 3</h3>
                    <h4 className="text-tile singleElementDragger componentElement" height="26px" width="154px" title="Add Heading 4" onDragStart={this.handleDrag} draggable="true">Add Heading 4</h4>
                    <h5 className="text-tile singleElementDragger componentElement" height="22px" width="128px" title="Add Heading 5" onDragStart={this.handleDrag} draggable="true">Add Heading 5</h5>
                    <h6 className="text-tile singleElementDragger componentElement" height="17px" width="102px" title="Add Heading 6" onDragStart={this.handleDrag} draggable="true">Add Heading 6</h6>
                    <p className="singleElementDragger componentElement" onDragStart={this.handleDrag} draggable="true">I'm a paragraph. Click here to add your own text and edit me. It's easy.</p>
                    <p className="singleElementDragger componentElement" onDragStart={this.handleDrag} draggable="true">I'm a paragraph. Click here to add your own text and edit me. It's easy.</p>
                    <p className="singleElementDragger componentElement" onDragStart={this.handleDrag} draggable="true">I'm a paragraph. Click here to add your own text and edit me. It's easy.</p>
                  </div>
                
                
                  <div>
                    <h5 className=" inner-heading">Titles <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <ToolBoxItem imgUrl={require('./../../images/titlesSection.png')} container={textTitles}/>
                  </div>
               
                  <div>
                    <h5 className="inner-heading">Paragraphs <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <p onDragStart={this.handleDrag} draggable="true">I'm a paragraph. Click here to add your own text and edit me. It's easy.</p>
                    <p onDragStart={this.handleDrag} draggable="true">I'm a paragraph. Click here to add your own text and edit me. It's easy.</p>
                    <p onDragStart={this.handleDrag} draggable="true">I'm a paragraph. Click here to add your own text and edit me. It's easy.</p>
                  </div>
                
              </div>
            </TabPanel>

            <TabPanel>
              <div className="tab-panel-head">Add an Image
                <div className="modal_btn pull-right">
                  <button className="btn">?</button>
                  <button className="btn" onClick={() => { this.hideTabsMain() }}>X</button>
                </div>
              </div>

              <div className="tab-panel-body" id="image-panel-body">
               
                  <div>
                    <h5 className="inner-heading">My Uploads <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="image-img">
                          <img alt="" src="/images/add_data_img.jpg" />
                        </div>
                      </div>
                      <div className="col-md-6 image-txt">
                        <h4>My Image Uploads</h4>
                        <p>Upload and add your own images to your site.</p>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-chevron-right"></i></button>
                      </div>
                    </div>
                  </div>
               
               
                  <div>
                    <h5 className="col-md-12 inner-heading">Image Collections <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="image-img">
                          <img alt="" src="/images/add_data_img02.jpg" />
                        </div>
                      </div>
                      <div className="col-md-6 image-txt">
                        <h4>My Image Uploads</h4>
                        <p>Upload and add your own images to your site.</p>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-chevron-right"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="image-img">
                          <img alt="" src="/images/add_data_img03.jpg" />
                        </div>
                      </div>
                      <div className="col-md-6 image-txt">
                        <h4>My Image Uploads</h4>
                        <p>Upload and add your own images to your site.</p>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-chevron-right"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="image-img">
                          <img alt="" src="/images/add_data_img04.jpeg" />
                        </div>
                      </div>
                      <div className="col-md-6 image-txt">
                        <h4>My Image Uploads</h4>
                        <p>Upload and add your own images to your site.</p>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-chevron-right"></i></button>
                      </div>
                    </div>
                  </div>
                
                  <div className="add_data_social">
                    <h5 className="inner-heading">My Social Images <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <i className="fa fa-facebook"></i>
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>My Facebook</h4>
                          <p>Use images from your Facebook on your site.</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img width="20" alt="" src="/images/flat-instagram.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>My Instagram</h4>
                          <p>Add beautifull images from your Instagram.</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img width="20" alt="" src="/images/picasa.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>My Picasa</h4>
                          <p>Add images from your Picasa account.</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img width="20" alt="" src="/images/Flickr.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>My Picasa</h4>
                          <p>Add images from your Picasa account.</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>
                  </div>
                
                  <div className="add_data_social related_apps">
                    <h5 className="inner-heading">Related Apps <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img width="20" alt="" src="/images/magic_stick.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>Rollover Image Effects</h4>
                          <p>Engage visitors with a fun rollover button</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img width="20" alt="" src="/images/image_frame_icon.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>Photo Editor</h4>
                          <p>Easily edit, filter & animate your images</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img width="20" alt="" src="/images/double_sec.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>Before and After Slider</h4>
                          <p>Easily compare photos on your site</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>
                  </div>
               
              </div>
            </TabPanel>
            <TabPanel>
              
              <div className="tab-panel-head">Add a Gallery
                <span className="modal_btn pull-right">
                  <button className="btn">?</button>
                  <button className="btn" onClick={() => { this.hideTabsMain() }}>X</button>
                </span>
              </div>
              <div className="tab-panel-body col-md-12" id="gallery-panel-body">
               
                  <div>
                    <h5 className="col-md-12 inner-heading">Galleries <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <ToolBoxGallery imgUrl={require('./../../images/gallary.png')} container={gallaryTitles}/>
                  </div>
                
                  <div>
                    <h5 className="col-md-12 inner-heading">Grid Galleries <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <img alt="" src={require('./../../images/gallary.png')} />
                  </div>
                
              </div>
            </TabPanel>
            <TabPanel>

              <div className="tab-panel-head">Add Vector Art
                <span className="modal_btn pull-right">
                  <button className="btn">?</button>
                  <button className="btn" onClick={() => { this.hideTabsMain() }}>X</button>
                </span>
              </div>
              <div className="tab-panel-body col-md-12" id="vector-panel-body">
                
                  <div>
                    <h5 className="col-md-12 inner-heading">Vector <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <img alt="" src={require('./../../images/vector.png')} />
                  </div>
               
              </div>
            </TabPanel>
            <TabPanel>
            
              <div className="tab-panel-head">Add a Shape
                <span className="modal_btn pull-right">
                  <button className="btn">?</button>
                  <button className="btn" onClick={() => { this.hideTabsMain() }}>X</button>
                </span>
              </div>
              <div className="tab-panel-body" id="shapes-panel-body">
                
                  <div>
                    <h5 className="inner-heading">Themed Lines <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <div className="col-md-6"><div className="header-line-1"></div></div>
                    <div className="col-md-6"><div className="header-line-2"></div></div>
                    <div className="col-md-6"><div className="header-line-3"></div></div>
                    <div className="col-md-6"><div className="header-line-4"></div></div>
                  </div>
                
                  <div>
                    <h5 className="col-md-12 inner-heading">Basic Shapes <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <ToolBoxShapesBasic imgUrl={require('./../../images/b-shapes.png')} container={shapesBasicTitles}/>
                  </div>
                
                  <div>
                    <h5 className="col-md-12 inner-heading">Arrows <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <ToolBoxShapesArrows imgUrl={require('./../../images/arrow-shapes.png')} container={shapesArrowsTitles}/>
                  </div>
                
                  <div>
                    <h5 className="col-md-12 inner-heading">Horizontal Lines <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <ToolBoxShapesHorizontal imgUrl={require('./../../images/horizontal-lines.png')} container={shapesHorizontalTitles}/>
                  </div>
                
                  <div>
                    <h5 className="col-md-12 inner-heading">Vertical Lines <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <img alt="" src={require('./../../images/vertical-lines.png')} />
                  </div>
                
              </div>
            </TabPanel>
            <TabPanel>
              
              <div className="tab-panel-head">Add an Interective Element
                <span className="modal_btn pull-right">
                  <button className="btn">?</button>
                  <button className="btn" onClick={() => { this.hideTabsMain() }}>X</button>
                </span>
              </div>
              <div className="tab-panel-body col-md-12" id="interactive-panel-body">
                
                  <div>
                    <h5 className="col-md-12 inner-heading">Hover slideshows <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <ToolBoxHoverInteractive imgUrl={require('./../../images/hover-boxes.jpg')} container={interactiveTitles}/>
                  </div>
                
                  <div>
                    <h5 className="col-md-12 inner-heading">Full width slideshows <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <ToolBoxSliderInteractive imgUrl={require('./../../images/slideshows-fw.png')} container={interactiveSliderTitles}/>
                  </div>
               
                  <div>
                    <h5 className="col-md-12 inner-heading">Box slideshows <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <ToolBoxSliderBox1Interactive imgUrl={require('./../../images/slideshows-box.png')} container={interactiveSliderBox1Titles}/>
                    <ToolBoxSliderBox2Interactive imgUrl={require('./../../images/slideshows-box2.png')} container={interactiveSliderBox2Titles}/>
                  </div>
               
              </div>
            </TabPanel>
            <TabPanel>
              
              <div className="tab-panel-head">Add a Button
                <span className="modal_btn pull-right">
                  <button className="btn">?</button>
                  <button className="btn" onClick={() => { this.hideTabsMain() }}>X</button>
                </span>
              </div>
              <div className="tab-panel-body col-md-12" id="button-panel-body">
               
                  <div>
                    <h5 className="col-md-12 inner-heading">Themed Buttons <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <div className="row themed-buttons-cont">
                      <div className="col-md-6">
                      <button className="singleElementDragger componentElement" height='43' width='108' style={button1theme} onDragStart={this.handleDrag} draggable="true">Button</button>
                      </div>
                      <div className="col-md-6">
                      <button className="singleElementDragger componentElement" height='47' width='112' style={button2theme} onDragStart={this.handleDrag} draggable="true">Button</button>
                      </div>
                      <div className="col-md-6">
                      <button className="singleElementDragger componentElement" height='47' width='112' style={button3theme} onDragStart={this.handleDrag} draggable="true">Button</button>
                      </div>
                      <div className="col-md-6">
                        <button className="btn theme_btn singleElementDragger componentElement" height='38' width='72' onDragStart={this.handleDrag} draggable="true">Button</button>
                      </div>
                    </div>
                  </div>
               
                  <div>
                <h5 className="col-md-12 inner-heading">Text Buttons <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                <ToolBoxButton imgUrl={require('./../../images/text-buttons.png')} container={buttonTitles}/>
                </div>
                
                  <div>
                <h5 className="col-md-12 inner-heading">Icon Buttons <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                
                <ToolBoxButton imgUrl={require('./../../images/icon-buttons.png')} container={buttonTitles}/>
                </div>
                
                  <div>
                    <h5 className="col-md-12 inner-heading">Related Apps <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                  

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>Back to Top</h4>
                          <p>Let visitors easily scroll through your site.</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>PayPal Button</h4>
                          <p>Accept payment with a custom PayPal button.</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>Buy it Now</h4>
                          <p>Add a 'Now on Sale' or 'Buy it Now' button.</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>Sell Downloads</h4>
                          <p>Sell digital music, eBooks, videos and designs.</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>
                  </div>
               
              </div>
            </TabPanel>
            <TabPanel>
              
              <div className="tab-panel-head">Add a Box
                <span className="modal_btn pull-right">
                  <button className="btn">?</button>
                  <button className="btn" onClick={() => { this.hideTabsMain() }}>X</button>
                </span>
              </div>
              <div className="tab-panel-body" id="box-panel-body">
                
                  <div>
                    <h5 className="inner-heading">Themed Boxes <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <div className="row">
                      <div className="col-md-6">
                        <button className="btn theme_btn box-thm-1" onDragStart={this.handleDrag} draggable="true">Button 1</button>
                      </div>
                      <div className="col-md-6">
                        <button className="btn theme_btn box-thm-2" onDragStart={this.handleDrag} draggable="true">Button 2</button>
                      </div>
                      <div className="col-md-6">
                        <button className="btn theme_btn box-thm-3" onDragStart={this.handleDrag} draggable="true">Button 3</button>
                      </div>
                      <div className="col-md-6">
                        <button className="btn theme_btn box-thm-4" onDragStart={this.handleDrag} draggable="true">Button 4</button>
                      </div>
                    </div>
                  </div>
                
                  <div>
                    <h5 className="inner-heading">Container Boxes <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <img alt="" src={require('./../../images/container-boxes.png')} />
                  </div>
                
                  <div>
                    <h5 className="col-md-12 inner-heading">Hover Boxes <button className="btn pull-right">i</button></h5>
                    <img alt="" src={require('./../../images/hover-boxes.jpg')} />
                  </div>
                
              </div>
            </TabPanel>
            <TabPanel>
            
              <div className="tab-panel-head">Add a Strip
              <span className="modal_btn pull-right">
                  <button className="btn">?</button>
                  <button className="btn" onClick={() => { this.hideTabsMain() }}>X</button>
                </span>
              </div>
              <div className="tab-panel-body col-md-12" id="strip-panel-body">
                
                  <div>
                    <h5 className="col-md-12 inner-heading">About <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <ToolBoxStripAbout imgUrl={require('./../../images/about-strip.png')} container={stripAbout}/>
                  </div>
               
                  <div>
                    <h5 className="col-md-12 inner-heading">Contact <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <ToolBoxStripContactUs imgUrl={require('./../../images/contact-strip.png')} container={stripContactUs}/>
                  </div>
                
                  <div>
                    <h5 className="col-md-12 inner-heading">Welcome <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <ToolBoxStripWelcome imgUrl={require('./../../images/welcome-strip.png')} container={stripWelcome}/>
                    
                  </div>
                
                  <div>
                    <h5 className="col-md-12 inner-heading">Services <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <ToolBoxStripServices imgUrl={require('./../../images/services-strip.png')} container={stripServices}/>
                  </div>
                
                  <div>
                    <h5 className="col-md-12 inner-heading">Team <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <img alt="" src={require('./../../images/team-strip.png')} />
                  </div>
                
                  <div>
                    <h5 className="col-md-12 inner-heading">Testimonials <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <img alt="" src={require('./../../images/testimonials-strip.png')} />
                  </div>
                
                  <div>
                    <h5 className="col-md-12 inner-heading">Classic <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <img alt="" src={require('./../../images/classic-strip.png')} />
                  </div>
                
              </div>
            </TabPanel>
            <TabPanel>
              
              <div className="tab-panel-head">Add a Video
                <span className="modal_btn pull-right">
                  <button className="btn">?</button>
                  <button className="btn" onClick={() => { this.hideTabsMain() }}>X</button>
                </span>
              </div>
              <div className="tab-panel-body col-md-12" id="video-panel-body">
                
                  <div>
                    <h5 className="col-md-12 inner-heading">Social Players <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <img alt="" src={require('./../../images/social-players.png')} />
                  </div>
                
                  <div>
                    <h5 className="col-md-12 inner-heading">Panda Videos <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <img alt="" src={require('./../../images/panda-videos.png')} />
                  </div>
                
                  <div>
                    <h5 className="col-md-12 inner-heading">Related Apps <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>IPlayerHD Video Hosting</h4>
                          <p>Upload, manage and publish ad-free videos.</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>Panda Pro Gallery</h4>
                          <p>Showcase photos in best quality possible.</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>Video and Image Slider</h4>
                          <p>Wow visitors with a photo & video gallery.</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>123videoads</h4>
                          <p>Get professional loking video ads.</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>
                  </div>
                
              </div>
            </TabPanel>
            <TabPanel>
               

              <div className="tab-panel-head">Add Music
              <span className="modal_btn pull-right">
                  <button className="btn">?</button>
                  <button className="btn" onClick={() => { this.hideTabsMain() }}>X</button>
                </span>
              </div>


              <div className="tab-panel-body" id="music-panel-body">
               
                  <div>
                    <h5 className="inner-heading">Themed Players <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <div className="col-md-12 theamed-players"></div>
                  </div>
                
                  <div>
                <h5 className="col-md-12 inner-heading">Panda Music <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                <img alt="" src={require('./../../images/panda-music.png')} />
                </div>
                
                  <div>
                <h5 className="col-md-12 inner-heading">SoundCloud Player <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                <img alt="" src={require('./../../images/soundcloud-player.png')} />
                </div>
                
                  <div>
                <h5 className="col-md-12 inner-heading">Spotify Player <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                <img alt="" src={require('./../../images/spotify-player.png')} />
                </div>
                
                  <div>
                <h5 className="col-md-12 inner-heading">Mini Players <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                <img alt="" src={require('./../../images/mini-player.png')} />
                <img alt="" src={require('./../../images/mini-player2.png')} />
                </div>
                
                  <div>
                <h5 className="col-md-12 inner-heading">iTunes Buttons <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                <img alt="" src={require('./../../images/itune-button.png')} />
                </div>
                
                  <div>
                    <h5 className="col-md-12 inner-heading">Related Apps <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>SoundCloud</h4>
                          <p>Bring SoundCloud to your site in a click.</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>
                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>SoundCloud</h4>
                          <p>Bring SoundCloud to your site in a click.</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>Bandsintown</h4>
                          <p>Sell tickets and promote your band's shows.</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>Panda Events</h4>
                          <p>Easily manage and share your event online.</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>Sell Downloads</h4>
                          <p>Sell digital music, eBooks, videos and designs.</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>
                  </div>
                
              </div>
            </TabPanel>
            <TabPanel>
              
              <div className="tab-panel-head">Add Social Tool <span className="modal_btn pull-right">
                <button className="btn">?</button>
                <button className="btn" onClick={() => { this.hideTabsMain() }}>X</button>
              </span>
              </div>
              <div className="tab-panel-body" id="social-panel-body">
                
                  <div>
                    <h5 className="inner-heading">Popular Social Tools <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                      
                    <PopularSocialIcons imgUrl={require('./../../images/ps-tools.png')} container={popularSocialIcons}/>
                    
                  </div>
                
                  <div>
                    <h5 className="inner-heading">Social Bar <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <SocialIconsBar imgUrl={require('./../../images/social-bars.png')} container={socialIconsBar}/>
                    </div>
                
                  <div>

                    <h5 className="inner-heading">Facebook <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <div className="">Facebook Images Here</div>
                  </div>
                
                  <div>
                    <h5 className="inner-heading">Twitter <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <div className="">Twitter Images Here</div>
                  </div>
                
                  <div>
                    <h5 className="inner-heading">Pinterest <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <div className="">Pinterest Images Here</div>
                  </div>
                
                  <div>
                    <h5 className="inner-heading">Youtube <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <div className="">Youtube Images Here</div>
                    </div>
                
                  <div>
                    <h5 className="inner-heading">Google+ <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <div className="">Google+ Images Here</div>
                  </div>
                
                  <div>
                    <h5 className="inner-heading">VK <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <div className="">VK Images Here</div>
                  </div>
                
                  <div>
                    <h5 className="inner-heading">Spotify <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <div className="">Spotify Images Here</div>
                  </div>
                
                  <div>
                    <h5 className="inner-heading">Related Apps <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>Panda Forums</h4>
                          <p>Everything you need for your onilne community.</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>Instagram Feed</h4>
                          <p>Showcase your Instagram pictures and videos.</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>Social Media Stream</h4>
                          <p>Stream all your social feeds to your panda site.</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>Comments</h4>
                          <p>Interact with visitors with a comment box.</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>Facebook like Popup</h4>
                          <p>Add a Facebook like popup for site vistors.</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>Social Media Icons</h4>
                          <p>Customize your own beautifull icons in seconds.</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>Panda Events</h4>
                          <p>Easily manage and share your event online.</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>Inffuse Testimonials</h4>
                          <p>Show how loved you are with testimonials.</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>Live Messenger</h4>
                          <p>Get Facebook messages from your site visitors.</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>
                  </div>
                
              </div>
            </TabPanel>
            <TabPanel>
              
              <div className="tab-panel-head">
                Add a Contact Tool
                <span className="modal_btn pull-right">
                  <button className="btn">?</button>
                  <button className="btn" onClick={() => { this.hideTabsMain() }}>X</button>
                </span>
              </div>
              <div className="tab-panel-body col-md-12" id="contact-panel-body">
              
                  <div>
                    <h5 className="col-md-12 inner-heading">Themed Contact Tools <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <div className="col-md-6">Form 1</div>
                    <div className="col-md-6">Form 2</div>
                    <div className="col-md-12">Map</div>
                    <h5 className="inner-heading">Get Subscribers <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <ContactGetSubcribers imgUrl={require('./../../images/get-subscribers.png')} container={contactGetSubcribers}/>
                  </div>
                 
                  <div>
                    <h5 className="inner-heading">Panda Chat <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <img alt="" src={require('./../../images/panda-chat.png')} />
                  </div>
                
                  <div>
                    <h5 className="inner-heading">Contact Forms <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <Contact imgUrl={require('./../../images/contact-forms.png')} container={contact}/>
                  </div>
                
                  <div>
                    <h5 className="inner-heading">Events <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <img alt="" src={require('./../../images/panda-events.png')} />
                  </div>
                
                  <div>
                    <h5 className="inner-heading">Google Maps <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <img alt="" src={require('./../../images/google-maps.png')} />
                  </div>
                
                  <div>
                    <h5 className="inner-heading">Full Width Google Maps <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <img alt="" src={require('./../../images/fw-google-maps.png')} />
                    </div>
                
                  <div>
                    <h5 className="inner-heading">Skype Call <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <img alt="" src={require('./../../images/skype-call.png')} />
                  </div>
                
                  <div>
                    <h5 className="inner-heading">Related Apps <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>123 Form Builder</h4>
                          <p>Create Dynamic contact order and RSVP forms.</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>Panda Events</h4>
                          <p>Easily manage and share your events online.</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>Form Builder Plus+</h4>
                          <p>Create a fully customized form in seconds.</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>Online Business Card</h4>
                          <p>Share essential business and contact details.</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>Panda Bookings</h4>
                          <p>Grow your business with easy onilne scheduling.</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>Panda Get Subscribers</h4>
                          <p>Add a signup to your site and send newsletters.</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>Moovit - Maps & Routes</h4>
                          <p>Give visitors transit options to your business.</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>Located - Store Mapper</h4>
                          <p>Display all your store locations on a map.</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>Located - Store Mapper</h4>
                          <p>Display all your store locations on a map.</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>Tidio Live Chat</h4>
                          <p>Add a free chat to your site with no signup.</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>
                  </div>
                
              </div>
            </TabPanel>
            <TabPanel>
              
              <div className="tab-panel-head">Add a Menu
                <span className="modal_btn pull-right">
                  <button className="btn">?</button>
                  <button className="btn" onClick={() => { this.hideTabsMain() }}>X</button>
                </span>
              </div>
              <div className="tab-panel-body col-md-12" id="menu-panel-body">
                
                  <div>
                    <h5 className="inner-heading">Themed Menu <button className="btn pull-right">i</button></h5>
                    <ToolBoxThemedMenu imgUrl={require('./../../images/themed-menu.jpg')} container={menuThemedTitles}/>
                    </div>
                  <div>
                    <h5 className="inner-heading">Horizontal Menu <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <ToolBoxHorizontalMenu imgUrl={require('./../../images/horizontal-menu.png')} container={menuHorizonatlTitles}/>
                  </div>
                
                  <div>
                    <h5 className="col-md-12 inner-heading">Vertical Menu <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <ToolBoxVerticalMenu imgUrl={require('./../../images/vertical-menu.png')} container={menuVerticalTitles}/>
                  </div>
                
                  <div>
                    <h5 className="col-md-12 inner-heading">Anchor Menu <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <ToolBoxAnchorMenu imgUrl={require('./../../images/anchor-menu.png')} container={menuAnchorTitles}/>
                  </div>
              </div>
            </TabPanel>
            <TabPanel>
              <div className="tab-panel-head">Add a Lightbox
                <span className="modal_btn pull-right">
                  <button className="btn">?</button>
                  <button className="btn" onClick={() => { this.hideTabsMain() }}>X</button>
                </span>
              </div>
              <div className="tab-panel-body col-md-12" id="lightbox-panel-body">
                
                  <div>
                    <h5 className="inner-heading">Welcome <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <img alt="" src={require('./../../images/welcome-lightbox.png')} />
                  </div>
                
                  <div>
                    <h5 className=" inner-heading">Subscribe<button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <img alt="" src={require('./../../images/subscribe-lightbox.png')} />
                  </div>
                
                  <div>
                    <h5 className="inner-heading">Promotion<button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <img alt="" src={require('./../../images/promotions-lightbox.png')} />
                  </div>
                
                  <div>
                    <h5 className="inner-heading">Contact<button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <img alt="" src={require('./../../images/contact-lightbox.png')} />
                  </div>
                
              </div>
            </TabPanel>
            <TabPanel>

              <div className="tab-panel-head">Add More
                <span className="modal_btn pull-right">
                  <button className="btn">?</button>
                  <button className="btn" onClick={() => { this.hideTabsMain() }}>X</button>
                </span>
              </div>
              <div className="tab-panel-body" id="more-panel-body">
                
                  <div>
                    <h5 className="inner-heading">Embeds <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <img alt="" src={require('./../../images/embads-more.png')} />
                  </div>
                
                  <div>
                    <h5 className="inner-heading">Webmaster Login<button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <img alt="" src={require('./../../images/webmaster-more.png')} />
                    </div>
                
                  <div>
                    <h5 className="inner-heading">Paypal Buttons<button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <img alt="" src={require('./../../images/paypal-buttons.png')} />
                  </div>
                
                  <div>
                    <h5 className="inner-heading">Anchor<button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <img alt="" src={require('./../../images/anchors-more.png')} />
                  </div>
                
                  <div>
                    <h5 className="inner-heading">Anchor Menu<button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <img alt="" src={require('./../../images/anchor-menu2.png')} />
                  </div>
                
                  <div>
                    <h5 className="inner-heading">Document Buttons<button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                    <img alt="" src={require('./../../images/documents-buttons.png')} />
                  </div>
                
                  <div>
                    <h5 className="inner-heading">Related Apps <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>Panda Forum</h4>
                          <p>Everything you need for your online community</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>Panda Events</h4>
                          <p>Easily manage and share your event online.</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>Panda Hit Counter</h4>
                          <p>Show your number of visitors with a counter</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>Site Search</h4>
                          <p>Let visitors find your products with search</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>Events Calendar</h4>
                          <p>Display calendar events directly on your site</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>Web-Stat</h4>
                          <p>Measure your traffic in real time &amp; get stats</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>Web-Stat</h4>
                          <p>Measure your traffic in real time &amp; get stats</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>Inffuse Testimonials</h4>
                          <p>Show how loved you are with testimonials</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>Google AdSense</h4>
                          <p>Earn money through relevant ads on your site</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>Panda Weather</h4>
                          <p>Let site visitors view the weather forecast.</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>Get Files to Dropbox</h4>
                          <p>Let anyone upload files to your Dropbox</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>

                    <div className="row mb_20">
                      <div className="col-md-4">
                        <div className="Image-wrap">
                          <img alt="" src="/images/icon-people.png" />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="Image-Txt">
                          <h4>Panda FAQ</h4>
                          <p>Provide answers to common client questions</p>
                        </div>
                      </div>
                      <div className="col-md-2 image-icon">
                        <button className="btn"><i className="fa fa-plus"></i></button>
                      </div>
                    </div>
                  </div>
              </div>
            </TabPanel>
          </Tabs >
        </TabPanel >
        <TabPanel className="left_panel_wrap panda_app_wrap">
          <div className="tab-panel-head">Panda App Market
            <div className="modal_btn pull-right">
              <button className="btn">?</button>
              <button className="btn" onClick={() => { this.hideTabsMain() }}>X</button>
            </div>
          </div>
          <div className="tab-panel-body col-md-12">
            <button className="btn theme_btn pull-left">Categories</button>
            <div className="modal_search">
              <button className="btn" type="submit">
                <i className="fa fa-search"></i>
              </button>
              <input type="text" className="form-control pull-right search-app" placeholder="What do you want to add to your site?" />
            </div>
          </div>
          <div className="appstore">
            <h2 className="appstore_head">5 Powerful Apps for Your <CategoriesModal/> </h2> 
            <div className="panda_booking_wrap">
              <div className="panda_booking">
                <img width="45" src="/images/calendar-icon.png" />
                <p className="panda_booking_descrip">Grow your bussiness with easy online scheduling</p>
                <h4>Panda Booking</h4>
                <div className="pnada_booking_pricing">
                  <a href="!#">Free/Premium</a>
                </div>
                <button className="btn theme_btn"><i className="fa fa-plus"></i> Add to Site</button>
                <hr />
                <div className="booking_rating">
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <span>(832)</span>
                </div>
                <div className="show_download">
                  <i className="fa fa-download"></i>
                  <span>1,789,957</span>
                </div>
              </div>
            </div>
          </div>
          <div className="panda_booking_wrap panda_booking_wrap02">
          <div className="panda_booking">
                <img width="45" src="/images/calendar-icon.png" />
                <p className="panda_booking_descrip">Grow your bussiness with easy online scheduling</p>
                <h4>Panda Booking</h4>
                <div className="pnada_booking_pricing">
                  <a href="!#">Free/Premium</a>
                </div>
                <button className="btn theme_btn"><i className="fa fa-plus"></i> Add to Site</button>
                <hr />
                <div className="booking_rating">
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <span>(832)</span>
                </div>
                <div className="show_download">
                  <i className="fa fa-download"></i>
                  <span>1,789,957</span>
                </div>
              </div>
              <div className="panda_booking">
                <img width="45" src="/images/calendar-icon.png" />
                <p className="panda_booking_descrip">Grow your bussiness with easy online scheduling</p>
                <h4>Panda Booking</h4>
                <div className="pnada_booking_pricing">
                  <a href="!#">Free/Premium</a>
                </div>
                <button className="btn theme_btn"><i className="fa fa-plus"></i> Add to Site</button>
                <hr />
                <div className="booking_rating">
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <span>(832)</span>
                </div>
                <div className="show_download">
                  <i className="fa fa-download"></i>
                  <span>1,789,957</span>
                </div>
              </div>
              <div className="panda_booking">
                <img width="45" src="/images/calendar-icon.png" />
                <p className="panda_booking_descrip">Grow your bussiness with easy online scheduling</p>
                <h4>Panda Booking</h4>
                <div className="pnada_booking_pricing">
                  <a href="!#">Free/Premium</a>
                </div>
                <button className="btn theme_btn"><i className="fa fa-plus"></i> Add to Site</button>
                <hr />
                <div className="booking_rating">
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <span>(832)</span>
                </div>
                <div className="show_download">
                  <i className="fa fa-download"></i>
                  <span>1,789,957</span>
                </div>
              </div>
              <div className="panda_booking">
                <img width="45" src="/images/calendar-icon.png" />
                <p className="panda_booking_descrip">Grow your bussiness with easy online scheduling</p>
                <h4>Panda Booking</h4>
                <div className="pnada_booking_pricing">
                  <a href="!#">Free/Premium</a>
                </div>
                <button className="btn theme_btn"><i className="fa fa-plus"></i> Add to Site</button>
                <hr />
                <div className="booking_rating">
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <span>(832)</span>
                </div>
                <div className="show_download">
                  <i className="fa fa-download"></i>
                  <span>1,789,957</span>
                </div>
              </div>
          </div>
        </TabPanel>
        <TabPanel className="My_Upload">
          <div className="tab-panel-head">My Uploads
              <span className="modal_btn pull-right">
              <button className="btn">?</button>
              <button className="btn" onClick={() => { this.hideTabsMain() }}>X</button>
            </span>
          </div>
          <div className="tab-panel-body col-md-12">
            <ModalImage />
            <ModalVector />
            <ModalVideos />
            <ModalFonts />
            <ModalDocs />
            <ModalTracks />
          </div>
        </TabPanel>
        <TabPanel className="left_panel_wrap booking_panel_wrap">
          <Tabs>
            <TabList>
              <Tab> Bookings Manager </Tab>
              <Tab> Add Booking Elements </Tab>
              <Tab> Learn More </Tab>
              <div className="upgrade_accept"><a href="" > <UpgradePopup/> </a>  </div>
            </TabList>

            <TabPanel>
              <div className="tab-panel-head">
                Bookings Manager
                <span className="modal_btn pull-right"><button className="btn">?</button><button className="btn" onClick={() => { this.hideTabsMain() }}>X</button></span>
              </div>
              <div className="tab-panel-body">
                <div className="booking_manager_inner">
                  <img src="/images/manageBookings_01.png"/>
                  <p className="mt_20">Get booked. Get paid.It's that easy!</p>
                  <button className="btn theme_btn">Manager Services</button>
                </div>
              </div>  
            </TabPanel>
            <TabPanel>
              <div className="tab-panel-head">
                Add Booking Elements
                <span className="modal_btn pull-right"><button className="btn">?</button><button className="btn" onClick={() => { this.hideTabsMain() }}>X</button></span>
              </div>
              <div className="tab-panel-body">
                <h5 className="inner-heading">
                  Service Widget
                  <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button>
                </h5>
                <div className="service_widget_box">
                  <div className="img_wrap">
                    <img src="/images/service_widget.jpg"/>
                  </div>
                  <div className="service_widget_text">
                    <h6>Service Name</h6>
                    <p>1 hr | $62.00</p>
                    <button className="btn">Book It</button>
                  </div>
                </div>
              </div>  
            </TabPanel>
            <TabPanel>
              <div className="tab-panel-head">
                Learn More
                <span className="modal_btn pull-right">
                  <button className="btn" onClick={() => { this.hideTabsMain() }}>X</button>
                </span>
              </div>
              <div className="tab-panel-body">
                <a href="#" className="booking_leanMore">
                  <img src="/images/booking_learn01.jpg"/>
                  <div className="booking_leanMore_title">
                    Control Pnada Bookings Support Froum <i className="fa fa-chevron-right"></i>
                  </div>
                </a>
                <a href="#" className="booking_leanMore">
                  <img src="/images/booking_learn02.jpg"/>
                  <div className="booking_leanMore_title">
                    More Tips from the Wix Blog <i className="fa fa-chevron-right"></i>
                  </div>
                </a>
              </div>
            </TabPanel>

          </Tabs>


        </TabPanel>
        <TabPanel className="left_panel_wrap blog_panel_wrap">
          <Tabs>
            <TabList>
              <Tab className="blog-manager">Blog Manager</Tab>
              <Tab className="add-blog-elements">Add Blog Elements</Tab>
              <Tab className="learn-more">Learn More</Tab>
            </TabList>
            <TabPanel>
              <div className="tab-panel-head">Blog Manager
                <span className="modal_btn pull-right">
                  <button className="btn">?</button>
                  <button className="btn" onClick={() => { this.hideTabsMain() }}>X</button>
                </span>
              </div>
              <div className="tab-panel-body col-md-12 text-center">
                <div className="">
                  <img alt="" width="320" src="/images/frame.jpg" />
                </div>
                <div className="blog_manager_body">
                  <div className=""><h3>What do you want to do?</h3></div>
                  <div className=""><button className="btn theme_btn">Create a Post</button></div>
                  <div className=""><button className="btn theme_btn">Manage Posts</button></div>
                </div>
              </div>
            </TabPanel>
            <TabPanel>
              <div className="tab-panel-head">Add Blog Elements         <span className="modal_btn pull-right">
                <button className="btn">?</button>
                <button className="btn" onClick={() => { this.hideTabsMain() }}>X</button>
              </span>
              </div>
              <div className="tab-panel-body add_blog_wrap col-md-12">
                <h5 className="col-md-12 inner-heading">Custom Feed <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                <img alt="" src={require('./../../images/custom-feeds.png')} />
                <h5 className="col-md-12 inner-heading">Recent Posts <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>
                <img alt="" src={require('./../../images/recent-posts.png')} />
                <h5 className="col-md-12 inner-heading">Related Apps <button className="btn info_btn pull-right"><i className="fa fa-info"></i></button></h5>

                <div className="row">
                  <div className="col-md-4">
                    <div className="Image-wrap">
                      <img alt="" src="/images/icon-people.png" />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="Image-Txt">
                      <h4>Panda Forum</h4>
                      <p>Everything you need for your online community</p>
                    </div>
                  </div>
                  <div className="col-md-2 image-icon"><button className="btn"><i className="fa fa-plus"></i></button></div>
                </div>

                <div className="row">
                  <div className="col-md-4">
                    <div className="Image-wrap">
                      <img alt="" src="/images/social-instagram.png" />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="Image-Txt">
                      <h4>Panda Events</h4>
                      <p>Easily manage and share your event online.</p>
                    </div>
                  </div>
                  <div className="col-md-2 image-icon"><button className="btn"><i className="fa fa-plus"></i></button></div>
                </div>

                <div className="row">
                  <div className="col-md-4">
                    <div className="Image-wrap">
                      <img alt="" src="/images/calendar-icon.png" />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="Image-Txt">
                      <h4>Instagram Feed</h4>
                      <p>Showcase your Instagram pictures and videos.</p>
                    </div>
                  </div>
                  <div className="col-md-2 image-icon"><button className="btn"><i className="fa fa-plus"></i></button></div>
                </div>

                <div className="row">
                  <div className="col-md-4">
                    <div className="Image-wrap">
                      <img alt="" src="/images/messaging-icon.png" />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="Image-Txt">
                      <h4>Panda Get Subscribers</h4>
                      <p>Add a signup to your site and send newsletters.</p>
                    </div>
                  </div>
                  <div className="col-md-2 image-icon"><button className="btn"><i className="fa fa-plus"></i></button></div>
                </div>

              </div>
            </TabPanel>
            <TabPanel>
              <div className="tab-panel-head">Learn More
                <div className="modal_btn pull-right">
                  <button className="btn">?</button>
                  <button className="btn" onClick={() => { this.hideTabsMain() }}>X</button>
                </div>
              </div>
              <div className="tab-panel-body">
                <div>View the blog help center</div>
                <div>Open the Blog Support Forum</div>
                <div>Tips from Bloging Professionals</div>
              </div>
            </TabPanel>
          </Tabs>
        </TabPanel>
        <TabPanel className="left_panel_wrap upgrade_panel_wrap">
          {
            <div className="upgrade_body">
              <span className="modal_btn pull-right">
                <button className="btn">?</button>
                <button className="btn" onClick={() => { this.hideTabsMain() }}>X</button>
              </span>
              <div className="row">
                <div className="col-md-12">
                  <h6>Upgrade your yearly Unlimited and Save!</h6>
                </div>
              </div>
              <div className="row">
                <div className="col-md-8">
                  <ul>
                    <li>
                      <i className="fa fa-check"></i>
                      <span>Get a FREE domain to connect to your site</span>
                    </li>
                    <li>
                      <i className="fa fa-check"></i>
                      <span>Enjoy unlimites bandwidth & extra storage</span>
                    </li>
                    <li>
                      <i className="fa fa-check"></i>
                      <span>Connect Google Analytics</span>
                    </li>
                    <li>
                      <i className="fa fa-check"></i>
                      <span>Remove CP brand ads</span>
                    </li>
                    <li>
                      <i className="fa fa-check"></i>
                      <span>To upgrade, start by saving your site</span>
                    </li>
                    <li>
                      <i className="fa fa-check"></i>
                      <span>To upgrade, start by saving your site</span>
                    </li>
                  </ul>
                  <button type="button" className="btn theme_btn">Save Site</button>
                </div>
                <div className="col-md-4">
                  <img alt="" src="/images/open-giftBox.png" />
                </div>
              </div>
            </div>
          }
        </TabPanel>
        <TabPanel className="left_panel_wrap tool_panel_wrap">

          <div className="tab-panel-head">
            Tools
            <span className="modal_btn pull-right">
              <button className="btn">?</button>
              <button className="btn" onClick={() => { this.hideTabsMain() }}>X</button>
            </span>
          </div>
          <div className="tool_pane_inner">
            <div className="row">
              <div className="col-md-6">
                <ul>
                  <li>
                    <input type="checkbox" id="toolbar" checked={this.state.showToolbar ? "checked" : ""} onChange={() => { this.showHideToolbar() }} />
                    <label htmlFor="toolbar">Toolbar</label>
                  </li>
                  <li>
                    <input id="ruler" type="checkbox" className="" />
                    <label htmlFor="ruler">Rulers</label>
                  </li>
                  <li>
                    <input id="gridline" type="checkbox" checked={this.state.showGridLines ? "checked" : ""} onChange={() => { this.showHideGridLines() }} />
                    <label htmlFor="gridline">Gridlines</label>
                  </li>
                  <li>
                    <input id="snap_object" type="checkbox" className="" />
                    <label htmlFor="snap_object">Snap to Objects</label>
                  </li>
                </ul>
              </div>
              <div className="col-md-6">
                <div className="tool_img_wrap">
                  <img alt="" width="80" src="/images/browse.png" />
                  <div className="tool_txt">
                    <h6>Gridlines</h6>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </TabPanel>
      </Tabs >
    );
  }
}

const Inspector = props => {
  return (
    <div className="inspictor">
      <InspectorTab />

    </div>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(Inspector);
