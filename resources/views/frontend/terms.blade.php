@extends('frontend.layouts.app')
@section('content')
<div id="myCarousel" class="carousel slide">
    <div class="carousel-inner">
        <div class="item active">
            <div class="container">
                <div class="carousel-caption" data-aos="zoom-in">
                    <h1>Terms & Conditions</h1>
                    <p>ControlPanda Software respects each individual's right to personal privacy. We will collect and use information through our website only in the ways disclosed in this statement.</p>
                </div>
            </div>
        </div>
    </div> 
</div>
<section id="features">
    <div class="container">    
        <div class="row boxes">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="bordered-box">
                    <h1>Terms</h1>
                    <p style="text-align:justify">
                        By accessing this site, you agree to be bound by these terms. If you do not agree to any of these conditions, you may not use or access this site or content. Materials contained in this site are protected by applicable copyright and trademark laws.
                    </p>
                    <h1>Conditions</h1>
                    <p style="text-align:justify">Permission is required to temporarily download a copy of the content ( any information or software) such as:</p>
                    <p> i) Modify or copy the materials.</p>
                    <p> ii) Use the materials for any commercial purpose.</p>
                    <p> iii) Remove any copyright or other proprietary notations from the materials.</p>
                    <p>  iv) Transfer the materials to another person on any other server. </p>
                    <h1>Notices</h1>
                    <p style="text-align:justify">
                        i) Any notice which must be given under the Agreement may either be delivered personally or posted.</p>
                    <p> ii) Notice given by post must be pre-paid and correctly addressed to the recipient company at its registered office.</p>
                    <p> iii) A notice delivered personally is deemed served upon delivery.</p>
                    <p> iv) A posted notice which complies with paragraph 10(b) above is deemed served on the second business day after the day of posting. </p>
                </div>
            </div>
        </div>    
    </div>
</section>
<div class="clear50"></div>
@endsection