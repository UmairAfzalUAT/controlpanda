@extends('admin.layouts.app')

@section('content')

<section>
    <div id="slider-wrap-support">
        <div class="container">
            <div class="caption-heading-support"></div>
            <h2>Support</h2>
        </div>
    </div>
</section>
<section>
    <div class="container-fluid">
        <div class="Control-box">
            <div class="container">
                <div class="content">
                    <h1>Advice and answers from the ControlPanda Team</h1>
                    <form autocomplete="off" action="{{ url('support') }}" method="get" class="search" data-searching-for-phrase="Search results for:" data-search-failed-phrase="Looks like something went wrong. Try searching again.">
                        <input type="text" autocomplete="off" class="search_input form" placeholder="Search for Answer" tabindex="1" name="q" value="{!! @$searchtext !!}">
                        <button type="submit" class="hidden-btn"><i class="fa fa-search"></i></button>
                    </form>
                </div>
                
                @foreach($help_articles as $articles)
                
                <div class="content-wrap-box">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"><img class="img-responsive" src="{{asset('uploads/helparticles/'.$articles->images)}}"></div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                            <h2><a href="support/{{$articles->slug}}">{{$articles->question}}</a></h2>
                            <p>{{  substr(strip_tags($articles->answer),0,150) }}...</p>
                            <div class="avatar">
                                <div class="avatar_photo"><img src="{{asset('uploads/users/'.$articles->profile_image)}}" class="avatar_image"></div>
                                <div class="avatar_info"><br> Written by <strong>{{{ $articles->first_name ." ". $articles->last_name }}}</strong></div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                
                
            </div>
        </div>
    </div>
</section>







        
        
        @endsection
        