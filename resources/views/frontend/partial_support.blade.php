            @foreach($help_articles as $articles)
                <div class="content">
                    <section>
                        <div class="paper"> 
                            <div class="collection row">
                                <div class="collection_photo col-md-2 col-sm-2 col-xs-12"> <img src="{{asset('uploads/helparticles/'.$articles->images)}}" class="img-responsive"> </div>
                                <div class="collection_meta col-md-10 col-sm-10 col-xs-12">
                                    <a target="_blank" href="{{ url('support/'.$articles->slug)}}"><h4 class="t">{{$articles->question}}</h4></a>
                                    <p class="">{{  substr(strip_tags($articles->answer),0,100) }}...</p>
                                    <div class="avatar">
                                        <div class="avatar_photo"> <img src="{{asset('uploads/users/'.$articles->profile_image)}}" class="avatar_image"> </div>
                                        <div class="avatar_info">
                                            <div>  <br>
                                                Written by <span class="c_darker"> {{{ $articles->first_name ." ". $articles->last_name }}}</span> </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="btn btn-success col-sm-9 solvedbtn" role="button" tabindex="0"><i class="fa fa-thumbs-up pull-left"></i><div class="action pull-left">Solved! This answers my question</div> </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>            
            @endforeach
            
            
