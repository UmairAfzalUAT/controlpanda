@extends('frontend.layouts.app')
@section('content')
<div id="myCarousel" class="carousel slide">
    <div class="carousel-inner">
        <div class="item active">
            <div class="container">
                <div class="carousel-caption" data-aos="zoom-in">
                    <h1>Privacy Policy</h1>
                    <p>ControlPanda Software respects each individual's right to personal privacy. We will collect and use information through our website only in the ways disclosed in this statement.</p>
                </div>
            </div>
        </div>
    </div> 
</div>

<section id="features">
    <div class="container">    
        <div class="row boxes">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="bordered-box">
                    <h1>Privacy Policy</h1>
                    <p>ControlPanda Software respects each individual's right to personal privacy. We will collect and use information through our website only in the ways disclosed in this statement. This statement applies solely to information collected at ControlPanda Software's Website(s).

                        If you choose to use our Service, then you agree to the collection and use of information in relation with this policy. The Personal Information that we collect are used for providing and improving the Service. We will not use or share your information with anyone except as described in this Privacy Policy.  </p>

                    <h3>Information Collection and Use:</h3>

                    <p style="text-align:justify">
                        For a better experience while using our Service, we may require you to provide us with certain personally identifiable information, including but not limited to your name, phone number, and postal address. The information that we collect will be used to contact or identify you.</p>



                    <h3>Log Data:</h3>

                    <p style="text-align:justify">We want to inform you that whenever you visit our Service, we collect information that your browser sends to us that is called Log Data. This Log Data may include information such as your computer’s Internet Protocol (“IP”) address, browser version, pages of our Service that you visit, the time and date of your visit, the time spent on those pages, and other statistics.

                    </p>

                    <h3>Cookies:</h3>

                    <p style="text-align:justify">

                        Cookies are files with small amount of data that is commonly used an anonymous unique identifier. These are sent to your browser from the website that you visit and are stored on your computer’s hard drive.

                        Our website uses these “cookies” to collection information and to improve our Service. You have the option to either accept or refuse these cookies, and know when a cookie is being sent to your computer. If you choose to refuse our cookies, you may not be able to use some portions of our Service.</p>

                    <h3>Security:</h3>

                    <p style="text-align:justify">We value your trust in providing us your Personal Information, thus we are striving to use commercially acceptable means of protecting it. But remember that no method of transmission over the internet, or method of electronic storage is 100% secure and reliable, and we cannot guarantee its absolute security.</p>


                    <h3>Changes to This Privacy Policy</h3>


                    <p style="text-align:justify">We may update our Privacy Policy from time to time. Thus, we advise you to review this page periodically for any changes. We will notify you of any changes by posting the new Privacy Policy on this page. These changes are effective immediately, after they are posted on this page. </p>
                </div>
            </div>
        </div>    
    </div>
</section>

<div class="clear50"></div>

@endsection