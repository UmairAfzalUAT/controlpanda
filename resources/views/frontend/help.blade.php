@extends('admin.layouts.app')
@section('content')
    <section>
        <div id="slider-wrap-support">
            <div class="container">
                <div class="caption-heading-support"></div>
                <h2>Support</h2>
            </div>
        </div>
    </section>
    <section>
        <div class="container-fluid">
            <div class="Control-box-detail">
                @foreach($helparticle as $help)
                <div class="container">
                    <div class="content-detail">
                        <div class="jumbotron">
                            <a href="{!! $help->slug !!}"><h1>{!!$help->question!!}</h1></a>
                            <div class="avatar">
                                <div class="avatar_photo"><img src="{{asset('uploads/users/'.$help->profile_image)}}" class="avatar_image"></div>
                                <div class="avatar_info"><br><br> Written by <strong>{{{ $help->first_name ." ". $help->last_name }}}</strong></div>
                            </div>
                        </div>
                        <div class="random-padd"><p>{!!$help->answer!!}</p></div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>

                
@endsection