<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="{{asset('public/frontend/images/fav.png')}}" type="image/png" sizes="16x16">
        <title>Support</title>

        <!-- CSS Files -->
       <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" media="screen">
        <link rel="stylesheet" href="{{asset('public/frontend/css/bootstrap.min.css')}}">

        <!-- Bootstrap Dropdown Hover CSS -->
        <link href="{{asset('public/frontend/css/animate.min.css')}}" rel="stylesheet">
        <link href="{{asset('public/frontend/css/bootstrap-dropdownhover.min.css')}}" rel="stylesheet">
        <link href="{{asset('public/frontend/css/after-login.css')}}" rel="stylesheet" media="screen">
    </head>
    <body class="body-pages">
        <!-- Fixed navbar -->
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header"><a class="navbar-brand" href="{{url('admin/dashboard')}}"><img src="{{asset('public/frontend/images/logo.png')}}" alt=""></a> </div>
                <!--/.nav-collapse --> 
            </div>
        </nav>
        <header class="header-support">
            <div class="container">
                <div class="content">
                    <h1 class="header__headline">Advice and answers from the ControlPanda Team</h1>
                    <form autocomplete="off" action="{{ url('support') }}" method="get" class="search" data-searching-for-phrase="Search results for:" data-search-failed-phrase="Looks like something went wrong. Try searching again.">
                        <input type="text" autocomplete="off" class="search_input" placeholder="Search for answers..." tabindex="1" name="q" value="{!! @$searchtext !!}">
                        <button type="submit" class="hidden-btn"><i class="fa fa-search text-info"></i></button>
                    </form>
                </div>
            </div>
        </header>

        <div class="clear40"></div>
        <!-- Begin page content -->

        <div class="container">
            
            <h3>{!!@$searchstring!!}</h3>
           
            @foreach($help_articles as $articles)
            
                <div class="content">
                    <section>
                        <div class="paper"> 
                            <div class="collection row">
                                <div class="collection_photo col-md-2 col-sm-2 col-xs-12"> <img src="{{asset('uploads/helparticles/'.$articles->images)}}" class="img-responsive"> </div>
                                <div class="collection_meta col-md-10 col-sm-10 col-xs-12">
                                    <a href="support/{{$articles->slug}}"><h2 class="t">{{$articles->question}}</h2></a>
                                    <p class="">{{  substr(strip_tags($articles->answer),0,150) }}...</p>
                                    <div class="avatar">
                                        <div class="avatar_photo"> <img src="{{asset('uploads/users/'.$articles->profile_image)}}" class="avatar_image"> </div>
                                        <div class="avatar_info">
                                            <div>  <br>
                                                Written by <span class="c_darker"> {{{ $articles->first_name ." ". $articles->last_name }}}</span> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            
            @endforeach

                

                

               
            
        </div>
        <div class="clear80"></div>
        <section class="copyright">
            <div class="container text-center"> Copyright © 2018 All rights reserved. <span>Creative work by ArhamSoft</span> </div>
        </section>
        <!-- Java script files --> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
        <script src="{{asset('public/frontend/js/bootstrap.min.js')}}"></script> 
    </body>
</html>