@php 
$segment1 = Request::segment(1);
$page_detail = App\DbModel::page_detail($segment1);
@endphp
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>

        <title>@if($page_detail) {{$page_detail->title}} - @endif ControlPanda</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta itemprop="name" content="@if($page_detail) {{$page_detail->meta_title}} - @endif ControlPanda">
        <meta itemprop="image" content="{{URL::to('frontend/img/meta_img.png')}}">
        <meta itemprop="description" name="description" content="@if($page_detail) {{$page_detail->meta_description}} @endif ">
        <meta name="keywords" content="@if($page_detail) {{$page_detail->meta_keywords}} - @endif ControlPanda">
        <meta name="author" content="ArhamSoft (Pvt) Ltd - https://www.arhamsoft.com">
        <link rel="icon" href="{{ asset('frontend/images/fav.png')}}" type="image/png" sizes="16x16">

        <!--- Social Share -->
        @if(isset($post) && $post != '')
        <meta property="og:title" content="{{$post->title}}">
        <meta property="og:image" content="{{url("/uploads/blogs")}}/{{$post->image}}">
        <meta property="og:description" content="{{$post->short_description}}">
        <meta property="og:url" content="{{URL::to('blog/post-details/'.$post->slug)}}">
        <meta property="og:type" content="website">
        <meta property="og:image:width" content="475">
        <meta property="og:image:height" content="355">
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:site" content="{{URL::to('blog/post-details/'.$post->slug)}}">
        <meta name="twitter:title" content="{{$post->title}}">
        <meta name="twitter:creator" content="arhamsoft.com">
        <meta name="twitter:description" content="{{$post->short_description}}">
        <meta name="twitter:image" content="{{url("/uploads/blogs")}}/{{$post->image}}">
        @endif
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!--<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" media="screen">-->
        <link rel="stylesheet" href="{{asset('frontend/css/bootstrap.min.css')}}">
        <link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">
        <link href="{{asset('frontend/css/style.css')}}" rel="stylesheet" media="screen">
        <script src="{{asset('frontend/js/jquery.min.js')}}"></script>
        <script src="{{asset('js/bootstrap.min.js')}}"></script>
        <script>var site_url = "{!!url('/')!!}"; var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');</script>
    </head>
    <body>

        <!--Top-section-->
        @include('frontend.include.top_section')
        @include('sweet::alert')    
        @yield('content')

        @include('frontend.include.bottom_section')  
        @include('frontend.include.footer')

        <script>
                    $('.navbar a[href*="#"]')
                    .not('[href="javascript:void(0);"]')
                    .not('[href="#0"]')
                                .click(function (event) {
                        if (
                                location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
                                &&
                                location.hostname == this.hostname
                            ) {
                            var target = $(this.hash);
                            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                                if (target.length) {
                                event.preventDefault();
                                    $('html, body').animate({
                                scrollTop: target.offset().top - 94
                                    }, 1000, function () {
                                    var $target = $(target);
                                    $target.focus();
                                        if ($target.is(":focus")) { 
                                    return false;
                                    }
                                ;
                        });
                    }
            }
                    });
                    $('.footer a[href*="#"]')
                    .not('[href="javascript:void(0);"]')
                    .not('[href="#0"]')
                                .click(function (event) {
                        if (
                                location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
                                &&
                                location.hostname == this.hostname
                            ) {
                            var target = $(this.hash);
                            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                                if (target.length) {
                                event.preventDefault();
                                    $('html, body').animate({
                                scrollTop: target.offset().top - 94
                                    }, 1000, function () {
                                    var $target = $(target);
                                    $target.focus();
                                        if ($target.is(":focus")) {
                                    return false;
                                    }
                                ;
                        });
                    }
                        }
                    });
        </script>
        <script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>
        <script>
                $(document).ready(function () {
                    $('.carousel').carousel({
            interval: 4000
            });
                });
            AOS.init({
            easing: 'ease-in-out-sine'
            });
            $('.navbar-wrapper').affix({offset: {top: 147}});
        </script>
    </body>
</html>
