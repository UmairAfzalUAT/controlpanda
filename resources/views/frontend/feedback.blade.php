@extends('frontend.layouts.app')
@section('content')
<!-- Carousel -->
<div class="empty-crusal"></div>

<section id="features">
    <div class="container">
    <div class="col-md-12 text-center">
    <div class="clear10"></div>
        <button class="btn btn-primary" id="start_feedback">Start Feedback</button>
        <p>Double click on site to add more feedback</p>
        <div class="clear10"></div>
    </div>
    <iframe frameborder="0" scrolling="no" width="100%" onload="resizeIframe(this)" src="{!!url('sites/'.$name)!!}"></iframe>
    </div>
</section>
<script>
    var clientid = '';
    function guidGenerator() {
        var S4 = function() {
        return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
        };
        return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
    }
  $(document).ready(function(){
    clientid = localStorage.getItem('clientid');
    if(clientid == '' || clientid == null){
        localStorage.setItem('clientid', guidGenerator());
    }
    clientid = localStorage.getItem('clientid');
    
  });
  function resizeIframe(obj) {
    obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
    loadcss();
  }
    var my_name = '';
  $("#start_feedback").click(function(){
      var html = '<div class="feedbackContainerCommon" style="position: absolute;top: 200px; left: 500px;"><div class=" round_DotCirlce" ></div><div class="comment_box"> <div class="head"><i class="flaticon-move-arrows"></i> <p>Drag the icon to whatever you want to comment on.</p></div>';
      if(my_name == ''){
        html = html+'<div class="form_group"><input class="feedback-field name" type="text" placeholder="My name..."><i class="flaticon-smiling"></i></div>';
      }else{
        html = html+'<div class="form_group"><div class="name"></div></div>'; 
      }
      html = html+'<div class="form_group"><textarea class="feedback-field comment" placeholder="My comment..."></textarea></div><div class="bottom"><button type="button" class="send_btn">Send</button><button type="button" class="delete_btn"><i class="flaticon-garbage"></i></button></div></div></div>';
      $('iframe').contents().find('body').append(html);

  });

 function loadcss()
{
setTimeout(() => {
    var $head = $("iframe").contents().find("head");
$head.append($("<link/>",
    { rel: "stylesheet", href: "{!!asset('frontend/css/feedback.css')!!}", type: "text/css" }));

    $.ajax({
        url: site_url + "/getall-feedback",
        type: 'post',
        data: {client_id:clientid,_token: CSRF_TOKEN},
        success: function (data) {
            var result = $.parseJSON(data);
            $.each(result,function(index,value){
                my_name = value.name;
                var html = '<div class="feedbackContainerCommon" style="position: absolute;top: '+value.page_y+'; left: '+value.page_x+';"><div class=" round_DotCirlce" ></div><div class="comment_box"> <div class="head"><i class="flaticon-move-arrows"></i> <p>Drag the icon to whatever you want to comment on.</p></div><div class="form_group"><div class="heading"><b>'+my_name+'</b> <small> at '+value.created_at+'</small></div></div><div class="form_group"><p class="discription">'+value.comment+'</p></div><div class="bottom"><button type="button" data-id="'+value.id+'" class="edit_btn">Edit</button><button type="button" data-id="'+value.id+'" class="delete_btn"><i class="flaticon-garbage"></i></button></div></div></div>';
                $('iframe').contents().find('body').append(html);
                
            });
        }
    });


    $('iframe').contents().find('html').find('body').on('mousedown', '.feedbackContainerCommon .round_DotCirlce', function(e)
{
        var pressedMouseDown = 1;
        var offset = $(this).parent('.feedbackContainerCommon').offset();
        var offsetLeft = parseInt(e.pageX - offset.left);
        var offsetTop = parseInt(e.pageY - offset.top);
        var currentThisJQuery = $(this).parent('.feedbackContainerCommon');
        $('iframe').contents().find('html').find('body').on('mousemove', '.feedbackContainerCommon', function(event)
        {
            if(pressedMouseDown)
            {
               currentThisJQuery.css({"top": event.pageY-offsetTop, "left": event.pageX-offsetLeft});
            }
        });
        $('iframe').contents().find('html').find('body').on('mouseup', '.feedbackContainerCommon', function()
        {
            pressedMouseDown = 0;
        });
});
    $('iframe').contents().find('html').on('dblclick','body', function(e){
        if($(e.target).closest('.feedbackContainerCommon').length){
            return false;
        }
        var pageX = e.pageX-140;
        var pageY = e.pageY-20;
        var html = '<div class="feedbackContainerCommon" style="position: absolute;top: '+pageY+'px; left: '+pageX+'px;"><div class=" round_DotCirlce" ></div><div class="comment_box"> <div class="head"><i class="flaticon-move-arrows"></i> <p>Drag the icon to whatever you want to comment on.</p></div>';
      if(my_name == ''){
        html = html+'<div class="form_group"><input class="feedback-field name" type="text" placeholder="My name..."><i class="flaticon-smiling"></i></div>';
      }else{
        html = html+'<div class="form_group"><div class="name"></div></div>'; 
      }
      html = html+'<div class="form_group"><textarea class="feedback-field comment" placeholder="My comment..."></textarea></div><div class="bottom"><button type="button" class="send_btn">Send</button><button type="button" class="delete_btn"><i class="flaticon-garbage"></i></button></div></div></div>';
      $('iframe').contents().find('body').append(html);
    });
    $('iframe').contents().find('html').on('click','a', function(e){
       // e.preventDefault();
    });
    $('iframe').contents().find('html').find('body').on('click', '.feedbackContainerCommon .send_btn', function(e){
        var element_ = $(this);
        var parent_container = element_.parents('.feedbackContainerCommon');
        var go_submit = true;
        var id = element_.attr('data-id');
        if(id){}else{id = 0}
        parent_container.find('.feedback-field').removeClass('has-error');
        parent_container.find('.feedback-field').each(function(){
            if($(this).val() == ''){
                go_submit = false;
                $(this).addClass('has-error');
            }
        });
        
        if(go_submit){
           if(my_name == ''){
            my_name = parent_container.find('.name').val();
           }
            var comment = parent_container.find('.comment').val();
            var project = '{!!$name!!}';
            var page_x = parent_container.css("left");
            var page_y = parent_container.css("top");
            var page = $('iframe')["0"].contentDocument.URL.substr($('iframe')["0"].contentDocument.URL.lastIndexOf('/') + 1);
            if(page == project){
                page = 'index.html';
            }
            
            var Pdata = {id:id, name: my_name, comment: comment, project:project, page_x:page_x, page_y:page_y , page:page, client_id:clientid,_token: CSRF_TOKEN};
            
            $.ajax({
                url: site_url + "/post-feedback",
                type: 'post',
                data: Pdata,
                success: function (data) {
                    var result = $.parseJSON(data);
                    element_.attr('data-id',result.id);
                    element_.html('Edit');
                    element_.removeClass('send_btn').addClass('edit_btn');
                    element_.next(".delete_btn").attr('data-id',result.id);
                    parent_container.find('.name').parent('.form_group').html('<div class="heading"><b>'+my_name+'</b> <small> at '+result.created_at+'</small></div>');
                    parent_container.find('.comment').replaceWith('<p class="discription">'+comment+'</p>');
                }
            });
        }
    });
    $('iframe').contents().find('html').find('body').on('click', '.feedbackContainerCommon .delete_btn', function(e){
        var element_ = $(this);
        var parent_container = element_.parents('.feedbackContainerCommon');
        var id = element_.attr('data-id');
        if(id){
            $.ajax({
                url: site_url + "/remove-feedback",
                type: 'post',
                data: {id:id,_token: CSRF_TOKEN},
                success: function (data) {
                    parent_container.remove();
                }
            });
        }else{
            parent_container.remove();
        }
    });
    $('iframe').contents().find('html').find('body').on('click', '.feedbackContainerCommon .edit_btn', function(e){
        var element_ = $(this);
        var parent_container = element_.parents('.feedbackContainerCommon');
        var id = element_.attr('data-id');
        if(id){
            $.ajax({
                url: site_url + "/one-feedback",
                type: 'post',
                data: {id:id,_token: CSRF_TOKEN},
                success: function (data) {
                    var result = $.parseJSON(data);
                    element_.attr('data-id',result.id);
                    element_.html('Send');
                    element_.removeClass('edit_btn').addClass('send_btn');
                    element_.next(".delete_btn").attr('data-id',result.id);
                    parent_container.find('.heading').parent('.form_group').html('<div class="form_group"><div class="name"></div></div>');
                    parent_container.find('.discription').replaceWith('<textarea class="feedback-field comment" placeholder="My comment...">'+result.comment+'</textarea>');
                }
            });
        }
    });
    /*$('iframe').contents().find('html').find('body').on('click', '.feedbackContainerCommon .edit_btn', function(e){});*/
}, 1500);
}
</script>
@endsection
