@extends('frontend.layouts.app')
@section('content')
<!-- Carousel -->
<div id="myCarousel" class="carousel slide">
    <div class="carousel-inner">
        <div class="item active">
            <div class="container">
                <div class="carousel-caption" data-aos="zoom-in">
                    <h1>Control Panda</h1>
                    <p>We’ve made over $1m in 8 weeks using this software. I don’t think you guys realise what you’ve just done to the competition!</p>
                    <a href="javascript:void(0);" class="btn btn-request">WATCH THE DEMO NOW</a>
                </div>
            </div>
        </div>
    </div> 
</div>
<!-- /.carousel -->

<section id="features">
    <div class="container">    
        <div class="row boxes">
            <h1>The easiest way to create sales and leads in your <br>business in 4 simple steps</h1>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="bordered-box">
                    <div class="inside">
                        <img src="{{asset('frontend/images/minisite-icon.png')}}" alt="Find your domain" />
                        <h2>Find your domain?</h2>
                        <br><p>No 3rd party domain providers needed. Search for your domain within Control Panda, purchase and set it up with 1 click and seamlessly integrate it to any of your mini sites in seconds. Unlimited domains.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="bordered-box">
                    <div class="inside">
                        <img src="{{asset('frontend/images/domain-icon.png')}}" alt="Choose panda minisite" />
                        <h2>Choose panda minisite</h2>
                        <br><p>No developers needed. Select your mini-sites from our library of pre made themes designed to not only look good but convert. Customise with our drag and drop page builder and publish in seconds. Unlimited sites.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="bordered-box">
                    <div class="inside">
                        <img src="{{asset('frontend/images/integration-icon.png')}}" alt="Select integration" />
                        <h2>Select integration</h2>
                        <br><p>No 3rd party software needed. Control Panda has a suite of custom made marketing solutions including Email, SMS, Voip Dialler, Heat Maps & Dedicated CRM’s, all set up and running with 1 click.</p>

                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="bordered-box">
                    <div class="inside">
                        <img src="{{asset('frontend/images/publish-icon.png')}}" alt="Publish leads & sales" />
                        <h2>Collect leads & sales</h2>
                        <br><p>What took a developer weeks, now takes you minutes! Set up your complete mini site with fully automated lead and sale processing systems all within 1 easy to use marketing tool.</p>

                    </div>
                </div>
            </div>
        </div>    
    </div>
</section>

<div class="clear50"></div>

<section id="stuff" class="capture-section">  
    <div class="container">
        <h1>The world first all in one marketing solution</h1>
        <p class="text-center">Developed by some of the worlds most marketers from the inside out.</p>
        <div class="clear60"></div>
        <div class="row">
            <div class="text-center col-md-6 col-sm-6 col-xs-12" data-aos="fade-right">
                <img src="{{asset('frontend/images/panda-mail.png')}}" class="img-responsive" alt="Panda Mail" />
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12" data-aos="fade-left">
                <div class="capture">
                    <img src="{{asset('frontend/images/one.png')}}" class="pull-left" alt="1" /> <h1 class="pull-left">Panda Mail</h1>
                    <div class="clearfix"></div>
                    <p>Integrated email system which allows you to collect contacts, segment your lists, send broadcasts and follow up new leads with automated email sequences. Provide value to your customers and leads plus increase sales with easy to setup and implement email marketing.</p>
                    <ul>
                        <li>Unlimited contacts</li>
                        <li>Unlimited contact lists</li>
                        <li>Set customization tags for your subscribers</li>
                        <li>Abandon cart emails</li>
                        <li>Automated email follow up sequences</li>
                        <li>Email templates</li>
                        <li>Drag and drop email builder</li>
                    </ul>
                    <a href="#" class="btn btn-default btn-findout">FIND OUT MORE</a>
                </div>
            </div>
        </div>
    </div>
    <div class="clear100"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12" data-aos="fade-right">
                <div class="capture">
                    <img src="{{asset('frontend/images/two.png')}}" class="pull-left" alt="1" /> <h1 class="pull-left">Panda SMS</h1>
                    <div class="clearfix"></div>
                    <p>A fully integrated sms marketing platform that allows you to automatically follow up with your leads and customers without any 3rd party software. This means cheaper message fees than anyone else on the market plus increased contact and conversion rates with the click of a button.</p>
                    <ul>
                        <li>Automated SMS messaging </li>
                        <li>Abandon cart SMS</li>
                        <li>SMS marketing</li>
                        <li>SMS contact lists</li>
                        <li>Customizable tags for list segmenting</li>
                    </ul>
                    <a href="#" class="btn btn-default btn-findout">FIND OUT MORE</a>
                </div>
            </div>
            <div class="text-center col-md-6 col-sm-6 col-xs-12" data-aos="fade-left">
                <img src="{{asset('frontend/images/panda-sms.png')}}" class="img-responsive" alt="Panda SMS" />
            </div>
        </div>
    </div>
    <div class="clear100"></div>
    <div class="container">
        <div class="row">
            <div class="text-center col-md-6 col-sm-6 col-xs-12" data-aos="fade-right">
                <img src="{{asset('frontend/images/panda-voip.png')}}" class="img-responsive" alt="Panda Voip" />
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12" data-aos="fade-left">
                <div class="capture">
                    <img src="{{asset('frontend/images/three.png')}}" class="pull-left" alt="1" /> <h1 class="pull-left">Panda Voip</h1>
                    <div class="clearfix"></div>
                    <p>Simply plug in your usb headset and start contacting leads the second they opt in to your contact form. With dedicated business phone numbers, you could be on the phone with your potential customers within minutes, all from your laptop.</p>
                    <p>With rates no one can beat in the market and one click set ups, it’s never been easier or more affordable to contact your leads and customers over the phone.</p>
                    <ul>
                        <li>Simple one click integration with any minisite</li>
                        <li>Cheapest rates on the market</li>
                        <li>Automated dialing</li>
                        <li>All from a USB headset on your computer or laptop</li>
                    </ul>
                    <a href="#" class="btn btn-default btn-findout">FIND OUT MORE</a>
                </div>
            </div>
        </div>
    </div>
    <div class="clear100"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12" data-aos="fade-right">
                <div class="capture">
                    <img src="{{asset('frontend/images/four.png')}}" class="pull-left" alt="1" /> <h1 class="pull-left">Panda CRM</h1>
                    <div class="clearfix"></div>
                    <p>A highly sophisticated CRM system for large enterprises that require a high volume of leads. No other CRM on the market allows its users to seamlessly integrate with landing pages built within the same system.</p>
                    <p>Set rules on your contact forms, data validation on email, phone and address, prohibit duplicate leads on the same form plus more!</p>
                    <ul>                                
                        <li>Seamless integration with your minisites</li>
                        <li>Data validation</li>
                        <li>Lead segregation and distribution</li>
                        <li>Prohibits duplicate contacts on the same form</li>
                        <li>Auto populate automatic phone dialers</li>
                        <li>Plus more</li>

                    </ul>
                    <a href="#" class="btn btn-default btn-findout">FIND OUT MORE</a>
                </div>
            </div>
            <div class="text-center col-md-6 col-sm-6 col-xs-12" data-aos="fade-left">
                <img src="{{asset('frontend/images/panda-crm.png')}}" class="img-responsive" alt="Panda CRM" />
            </div>
        </div>
    </div>
    <div class="clear100"></div>
    <div class="container">
        <div class="row">
            <div class="text-center col-md-6 col-sm-6 col-xs-12" data-aos="fade-right">
                <img src="{{asset('frontend/images/panda-flow.png')}}" class="img-responsive" alt="Panda Flow" />
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12" data-aos="fade-left">
                <div class="capture">
                    <img src="{{asset('frontend/images/five.png')}}" class="pull-left" alt="1" /> <h1 class="pull-left">Panda Flow</h1>
                    <div class="clearfix"></div>
                    <p>Custom built mouse tracking software specifically designed to integrate with Control Panda. Panda Flow records the users movements, highlighting heat spots on your screens allowing you to make strategic changes to your landing pages based on the users behavior.</p>
                    <p>No more guessing what will work, simply activate your Panda Flow recorder with one click and you will have an automated report highlighting exactly how users are navigating around your site.</p>
                    <ul>                                
                        <li>Record users behaviour on your landing pages</li>
                        <li>Make changes based on the heat map reports</li>
                        <li>Set up with one click within your control panel</li>
                        <li>Make strategic changes based on the users behaviour</li>
                    </ul>
                    <a href="#" class="btn btn-default btn-findout">FIND OUT MORE</a>
                </div>
            </div>
        </div>
    </div>
    <div class="clear100"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12" data-aos="fade-right">
                <div class="capture">
                    <img src="{{asset('frontend/images/six.png')}}" class="pull-left" alt="1" /> <h1 class="pull-left">Panda Reporting</h1>
                    <div class="clearfix"></div>
                    <p>Custom reporting dashboard highlighting the real time stats on your landing pages. Set different reporting parameters based on the functionality of your landing pages and the outcomes you are trying to achieve.</p>
                    <p>With the simple interface on your dashboard you can easily track and report the progress of your individual landing pages, making it easier to highlight areas of improvement so your campaigns will always remain profitable.</p>
                    <ul>                                
                        <li>Simple, easy to use interface</li>
                        <li>Real time reporting</li>
                        <li>Track sales and leads on all your campaigns</li>
                        <li>Track spend and ROI on your campaigns</li>
                        <li>Set warnings and conditions based on your max lead or sale cost</li>
                    </ul>
                    <a href="#" class="btn btn-default btn-findout">FIND OUT MORE</a>
                </div>
            </div>
            <div class="text-center col-md-6 col-sm-6 col-xs-12" data-aos="fade-left">
                <img src="{{asset('frontend/images/panda-reporting.png')}}" class="img-responsive" alt="Panda Reporting" />
            </div>
        </div>
    </div>
    <div class="clear100"></div>
</section>

<section id="whydifferent">
    <div class="container">    
        <div class="row boxes">
            <h1>Why is Control Panda Different from other marketing software?</h1>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="bordered-box">
                    <div class="inside">
                        <img src="{{asset('frontend/images/allinone.png')}}" alt="All In One System" />
                        <h2>All In One System</h2>
                        <br><p>Control Panda has everything you need  with no complicated, time consuming 3rd party integrations. Just quick, easy 1 click set ups.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="bordered-box">
                    <div class="inside">
                        <img src="{{asset('frontend/images/voip.png')}}" alt="Integrated Voip and Dialer" />
                        <h2>Integrated Voip and Dialer</h2>
                        <br><p>Make calls to your clients with the automated lead processing voip and dialling systems. Market beating rates plus 1 click set ups.</p>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="bordered-box">
                    <div class="inside">
                        <img src="{{asset('frontend/images/mouseflow.png')}}" alt="Mouse Flow" />
                        <h2>Mouse Flow</h2>
                        <br><p>Records how users navigate around your mini site, helping you make changes based on consumer behaviour, all set up with 1 click of a button.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="bordered-box">
                    <div class="inside">
                        <img src="{{asset('frontend/images/domain.png')}}" alt="In House Domains" />
                        <h2>In House Domains</h2>
                        <br><p>Buy dominos within your control panel and link them to any mine site with 1 click. No integrations or DNS settings needed. Just click and publish.</p>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="bordered-box">
                    <div class="inside">
                        <img src="{{asset('frontend/images/reporting.png')}}" alt="Detailed Reporting" />
                        <h2>Detailed Reporting</h2>
                        <br><p>Instantly access the data that matters for all of your mini sites. No fluff, just hard cold facts that tell you if your site is profitable or not.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="bordered-box">
                    <div class="inside">
                        <img src="{{asset('frontend/images/splittest.png')}}" alt="One Click Split Test" />
                        <h2>One Click Split Test</h2>
                        <br><p>Test multiple pages with 1 click. Simply duplicate the page, make your changes with the page builder and publish for testing. Simple.</p>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>    
    </div>
</section>

<section id="pricing" class="pricing-table">
    <div class="container">
        <h1>Packages & Prices</h1>
        <p>Working internationally in any country and any currency, we support start ups, SME’s and large<br>blue-chip companies.</p>
        <div class="col-xs-12 col-sm-12 table-responsive text-center">
            <div class='pricing-table-inner'>
               
                <div class='features'>
                    <ol>
                        
                                    <li># Of Funnels</li>
                                    <li># Of Pages</li>
                                    <li># Of Visitors</li>
                                    <li># Of Contacts</li>
                                    <li>Custom Domains</li>
                                    <li>A/B Split Tests</li>
                                    <li>Email Integrations</li>
                                    <li>Optin Funnels</li>
                                    <li>ClickPops</li>
                                    <li>ClickOptin</li>
                                    <li>All Advanced Funnels</li>
                                    <li>Sales Funnels</li>
                                    <li>Membership Funnels</li>
                                    <li># Of Members</li>
                                    <li>Auto Webinar Funnels</li>
                                    <li>Webinar Funnels</li>
                                    <li>Hangout Funnels</li>
                                    <li>Order Pages</li>
                                    <li>Upsell Pages</li>
                                    <li>Downsale Pages</li>
                                    <li>Share Your Funnels</li>
                                    <li>Priority Support</li>
                                    <li>Priority Template Requests</li>
                                    
                    </ol>
                </div>
                
                
                @foreach($packages as $package)
                
                <div class='column {!!$package->div_class!!}'>
                    <h2 class='title'>{!!$package->title!!}<br><span>${!!$package->price!!}</span> per month</h2>
                    <div class='details'>
                                    <span>{!!$package->no_of_funnels!!}</span>
                                    <span>{!!$package->no_of_pages!!}</span>
                                    <span>{!!$package->no_of_visitors!!}</span>
                                    <span>{!!$package->no_of_contacts!!}</span>
                                    <span>{!!$package->custom_domains!!}</span>
                                    
                                    @if ($package->a_b_split_tests === 1)
                                    <span><img src="{{asset('frontend/images/ok.png')}}" alt=""></span>
                                    @else 
                                    <span><img src="{{asset('frontend/images/close.png')}}" alt=""></span>
                                    @endif
                                    
                                    
                                    @if ($package->email_integrations === 1)
                                    <span><img src="{{asset('frontend/images/ok.png')}}" alt=""></span>
                                    @else 
                                    <span><img src="{{asset('frontend/images/close.png')}}" alt=""></span>
                                    @endif
                                    
                                    @if ($package->optin_funnels === 1)
                                    <span><img src="{{asset('frontend/images/ok.png')}}" alt=""></span>
                                    @else 
                                    <span><img src="{{asset('frontend/images/close.png')}}" alt=""></span>
                                    @endif
                                    
                                    @if ($package->clickpops === 1)
                                    <span><img src="{{asset('frontend/images/ok.png')}}" alt=""></span>
                                    @else 
                                    <span><img src="{{asset('frontend/images/close.png')}}" alt=""></span>
                                    @endif
                                    
                                    @if ($package->clickoptin === 1)
                                    <span><img src="{{asset('frontend/images/ok.png')}}" alt=""></span>
                                    @else 
                                    <span><img src="{{asset('frontend/images/close.png')}}" alt=""></span>
                                    @endif
                                    
                                    
                                    
                                    @if ($package->all_advanced_funnels === 1)
                                    <span><img src="{{asset('frontend/images/ok.png')}}" alt=""></span>
                                    @else 
                                    <span><img src="{{asset('frontend/images/close.png')}}" alt=""></span>
                                    @endif
                                    
                                    @if ($package->sales_funnels === 1)
                                    <span><img src="{{asset('frontend/images/ok.png')}}" alt=""></span>
                                    @else 
                                    <span><img src="{{asset('frontend/images/close.png')}}" alt=""></span>
                                    @endif
                                    
                                    @if ($package->membership_funnels === 1)
                                    <span><img src="{{asset('frontend/images/ok.png')}}" alt=""></span>
                                    @else 
                                    <span><img src="{{asset('frontend/images/close.png')}}" alt=""></span>
                                    @endif
                                    
                                   
                                    
                                    @if ($package->no_of_members === 0)
                                    <span><img src="{{asset('frontend/images/close.png')}}" alt=""></span>
                                    @else 
                                    <span>{!!$package->no_of_members!!}</span>
                                    @endif
                                    
                                    @if ($package->auto_webinar_funnels === 1)
                                    <span><img src="{{asset('frontend/images/ok.png')}}" alt=""></span>
                                    @else 
                                    <span><img src="{{asset('frontend/images/close.png')}}" alt=""></span>
                                    @endif
                                    
                                    @if ($package->webinar_funnels === 1)
                                    <span><img src="{{asset('frontend/images/ok.png')}}" alt=""></span>
                                    @else 
                                    <span><img src="{{asset('frontend/images/close.png')}}" alt=""></span>
                                    @endif
                                    
                                    @if ($package->hangout_funnels === 1)
                                    <span><img src="{{asset('frontend/images/ok.png')}}" alt=""></span>
                                    @else 
                                    <span><img src="{{asset('frontend/images/close.png')}}" alt=""></span>
                                    @endif
                                    
                                    @if ($package->order_pages === 1)
                                    <span><img src="{{asset('frontend/images/ok.png')}}" alt=""></span>
                                    @else 
                                    <span><img src="{{asset('frontend/images/close.png')}}" alt=""></span>
                                    @endif
                                    
                                    @if ($package->upsell_pages === 1)
                                    <span><img src="{{asset('frontend/images/ok.png')}}" alt=""></span>
                                    @else 
                                    <span><img src="{{asset('frontend/images/close.png')}}" alt=""></span>
                                    @endif
                                    
                                    @if ($package->downsale_pages === 1)
                                    <span><img src="{{asset('frontend/images/ok.png')}}" alt=""></span>
                                    @else 
                                    <span><img src="{{asset('frontend/images/close.png')}}" alt=""></span>
                                    @endif
                                    
                                    @if ($package->share_your_funnels === 1)
                                    <span><img src="{{asset('frontend/images/ok.png')}}" alt=""></span>
                                    @else 
                                    <span><img src="{{asset('frontend/images/close.png')}}" alt=""></span>
                                    @endif
                                    
                                    @if ($package->priority_support === 1)
                                    <span><img src="{{asset('frontend/images/ok.png')}}" alt=""></span>
                                    @else 
                                    <span><img src="{{asset('frontend/images/close.png')}}" alt=""></span>
                                    @endif
                                    
                                    @if ($package->priority_template_requests === 1)
                                    <span><img src="{{asset('frontend/images/ok.png')}}" alt=""></span>
                                    @else 
                                    <span><img src="{{asset('frontend/images/close.png')}}" alt=""></span>
                                    @endif
                                    
                                   
                    </div>
                    <a href="{{url('signup/'.$package->id.'/'.strtolower($package->title))}}" class='chooseaplan-btn'>Choose a Plan</a>
                    
                </div>

                @endforeach

            </div>

            <div class="clear50"></div>
        </div>
    </div>
    <div class="clear50"></div>
    
</section>

<section id="contact" class="contact-section">
    <div class="container">
        <div class="col-md-7 col-sm-6 col-xs-12">
            <h1>Frequently asked question<br>about controlpanda</h1>
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Lorem ipsum dolor sit amet</a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip exeea commodo consequat. 
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">Can I submit my own Bootstrap templates or themes?</a>
                        </h4>
                    </div>
                    <div id="collapseTen" class="panel-collapse collapse">
                        <div class="panel-body">
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip exeea commodo consequat. 
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven">What is the currency used for all transactions?</a>
                        </h4>
                    </div>
                    <div id="collapseEleven" class="panel-collapse collapse">
                        <div class="panel-body">
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip exeea commodo consequat. 
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Who cen sell items?</a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip exeea commodo consequat. 
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">I want to sell my items - what are the steps?</a>
                        </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-body">
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip exeea commodo consequat. 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5 col-sm-6 col-xs-12">
            <div class="contactbox">
                <h1>Do you have questions?</h1>
                <form action="{{url('/contact-us')}}" method="POST">
                    <input type="text" name="name" required="" placeholder="Full name">
                    {{ csrf_field() }}
                    <input type="email" name="email" required="" placeholder="Email Address">
                    <input type="text" name="phone" required="" placeholder="Phone">
                    <textarea name="message" placeholder="Your Question"></textarea>
<!--                    <a href="javascript:void(0);" class="btn btn-send">Send Your Question</a>-->
                        <button class="btn btn-send" type="submit">Send Your Question</button>
                </form>
            </div>
        </div>
    </div>
</section>
<section class="customer-section">
    <div class="container">
        <h1>Meet panda customers</h1>
        <div class="clear50"></div>
        <ul class="customers">
            <li><img src="{{asset('frontend/images/customer1.png')}}" alt=""></li>
            <li><img src="{{asset('frontend/images/customer2.png')}}" alt=""></li>
            <li><img src="{{asset('frontend/images/customer3.png')}}" alt=""></li>
            <li><img src="{{asset('frontend/images/customer4.png')}}" alt=""></li>
            <li><img src="{{asset('frontend/images/customer5.png')}}" alt=""></li>
        </ul>
    </div>

</section>
@endsection