<script src="http://code.jquery.com/jquery-3.3.1.min.js"></script>
<?php
if ($pages->libraries != '' && $pages->libraries != '[]') {
    $libraries = json_decode($pages->libraries);
    foreach ($libraries as $lib) {
        create_library_link($lib);
    }
}
?>
@if(count($seo_data))
<title>@if($seo_data->title) {!!$seo_data->title!!} @else ControlPanda @endif</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta itemprop="name" content="@if($seo_data->title) {!!$seo_data->title!!} @else ControlPanda @endif">
<meta itemprop="image" content="@if($seo_data->social_image) {!!URL::to('uploads/social_images/'.$seo_data->social_image)!!} @else {!!URL::to('public/frontend/img/meta_img.png')!!} @endif">
<meta itemprop="description" name="description" content="@if($seo_data->description) {!!$seo_data->description!!} @else ControlPanda @endif">
<meta name="keywords" content="@if($seo_data->keywords) {!!$seo_data->keywords!!} @else ControlPanda @endif">
<meta name="author" content="@if($seo_data->author) {!!$seo_data->author!!} @else Arhamsoft @endif">
<meta property="og:title" content="@if($seo_data->title) {!!$seo_data->title!!} @else ControlPanda @endif">
<meta property="og:image" content="@if($seo_data->social_image) {!!URL::to('uploads/social_images/'.$seo_data->social_image)!!} @else {!!URL::to('public/frontend/img/meta_img.png')!!} @endif">
<meta property="og:description" content="@if($seo_data->description) {!!$seo_data->description!!} @else ControlPanda @endif">
<meta property="og:url" content="{{URL::to('page/'.$project->name)}}">
<meta property="og:type" content="website">
<meta property="og:image:width" content="475">
<meta property="og:image:height" content="355">
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="{{URL::to('page/'.$project->name)}}">
<meta name="twitter:title" content="@if($seo_data->title) {!!$seo_data->title!!} @else ControlPanda @endif">
<meta name="twitter:creator" content="@if($seo_data->author) {!!$seo_data->author!!} @else Arhamsoft @endif">
<meta name="twitter:description" content="@if($seo_data->description) {!!$seo_data->description!!} @else ControlPanda @endif">
<meta name="twitter:image" content="{{URL::to('page/'.$project->name)}}">
@endif
<link href="{!!asset('builder/themes/'.strtolower($pages->theme).'/stylesheet.css')!!}" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet">
<style><?php echo str_replace('url(templates/', 'url(../builder/templates/', str_replace('url("templates/', 'url("../builder/templates/', str_replace("url('templates/", "url('../builder/templates/", $pages->css))); ?></style>
@if(count($tracking_codes))
{!!$tracking_codes->header_code!!}
@endif
{!! str_replace("href='templates/","href='../builder/templates/",str_replace('href="templates/','href="../builder/templates/',str_replace('url(templates/','url(../builder/templates/',str_replace('url("templates/','url("../builder/templates/',str_replace("url('templates/","url('../builder/templates/",str_replace("src='templates/","src='../builder/templates/",str_replace('src="templates/','src="../builder/templates/',str_replace('data-vide-bg="templates/','data-vide-bg="../../builder/templates/',str_replace('data-bg-image="templates/','data-bg-image="../../builder/templates/',str_replace("data-bg-image='templates/","data-bg-image='../../builder/templates/",$pages->html))))))))))!!}
<script>
$('a').each(function () {
var link = $(this).attr('href');
if (link) {
var link_array = link.split('.');
if (link_array[1] && link_array[1] == 'html' && link_array[0].indexOf("/") < 0) {
$(this).attr('href', '<?php echo url('/') ?>/' + link_array[0]);
}
}
});
$('head').append('<link rel="icon" href="{!!asset('public / frontend / images / fav.png')!!}" type="image/png" sizes="16x16">');
@foreach($email_lists as $el)
        $('form').append('<input type="hidden" name="expected_list[]" value="{!!$el->email_list_id!!}">');
$('form').append('<input type="hidden" name="expected_list_actions[]" value="{!!$el->action!!}">');
@endforeach
</script>
<script>
<?php echo $pages->js; ?>
</script>
@if(count($tracking_codes))
{!!$tracking_codes->footer_code!!}
@endif
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-115314865-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments); }
    gtag('js', new Date());
    gtag('config', 'UA-115314865-1');
</script>
