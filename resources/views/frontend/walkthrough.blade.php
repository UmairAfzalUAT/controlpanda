@extends('frontend.layouts.app')
@section('content')
<!-- Carousel -->
<div id="myCarousel" class="carousel slide">
    <div class="carousel-inner">
        <div class="item active">
            <div class="container">
                <div class="carousel-caption" data-aos="zoom-in">
                    <h1>Control Panda - Walkthrough</h1>
                    <p>This video contains all the related guidelines regarding ControlPanda and covers the basic flows. Recommended  !</p>
                    <!-- <a href="javascript:void(0);" class="btn btn-request">WATCH THE DEMO NOW</a> -->
                </div>
            </div>
        </div>
    </div> 
</div>

<section id="features">
    <div class="container"> 
        <div class="intro_video">
            <h2 class="text-center">Unlock <img width="200" src="{{asset('public/frontend/images/intergration/01.png')}}" alt=""> in your account</h2>
            <iframe width="100%" height="400" src="https://www.youtube.com/embed/pofwrk9s0P8" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            <h2 class="text-center">Let Me Show You Around Actionetics Before You Get Started...</h2>
            <a class="btn btn-block" href="">Access Actionetics In Your Account Now</a>
        </div>
    </div>
</section>
@endsection