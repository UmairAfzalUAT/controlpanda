@php
$logo = settingValue('site_logo');
$segment1 = Request::segment(1);
$segment2 = Request::segment(2);

@endphp

<div class="navbar-wrapper navbar-fixed-top" data-aos="fade-down">
    <div class="container">
        <div class="navbar navbar-static-top">      
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{url('/')}}"><img src="{{asset('frontend/images/logo.png')}}" alt="Control Panda" /></a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a class="features-anchor" href="@if($segment1 != '' ){{url('/')}}@endif#stuff">Features</a></li>
                    <li><a href="@if($segment1 != '' ){{url('/')}}@endif#features">How we help you</a></li>
                    <li><a href="@if($segment1 != '' ){{url('/')}}@endif#pricing">Pricing</a></li>
                    <li><a href="{{url('login')}}">Login</a></li>
                    <li><a href="@if($segment1 != '' ){{url('/')}}@endif#pricing" class="btn login-btn">Free Trial</a></li>
                </ul>
            </div>
        </div>
    </div><!-- /container -->
</div><!-- /navbar wrapper -->

<div class="clearfix"></div>