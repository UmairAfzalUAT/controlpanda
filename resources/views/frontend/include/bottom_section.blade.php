@php

$segment1 = Request::segment(1);


@endphp
   

   <section class="footer">
    <div class="container">
        <h1>Follow us</h1>
        <ul class="social">
            <li><a href="javascript:void(0);"><img src="{{asset('frontend/images/facebook.png')}}" alt=""></a></li>
            <li><a href="javascript:void(0);"><img src="{{asset('frontend/images/twitter.png')}}" alt=""></a></li>
            <li><a href="javascript:void(0);"><img src="{{asset('frontend/images/instagram.png')}}" alt=""></a></li>
        </ul>
        <ul class="navigation-links">                           

            <li><a href="#myCarousel">Home</a></li>
            <li><a href="@if($segment1 != '' ){{url('/')}}@endif#stuff">Features</a></li>
            <li><a href="@if($segment1 != '' ){{url('/')}}@endif#features">How we help you</a></li>
            <li><a href="@if($segment1 != '' ){{url('/')}}@endif#pricing">Pricing</a></li>
            <li><a href="@if($segment1 != '' ){{url('/')}}@endif#contact">Contact</a></li>
            <li><a href="{{url('privacy')}}">Privacy</a></li>
            <li><a href="{{url('terms')}}">Terms</a></li>
        </ul>
    </div>
</section>