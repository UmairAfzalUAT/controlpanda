<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="{{asset('images/fav.png')}}">
  <link rel="stylesheet" href="{{asset('css/main.css')}}">
  <link rel="stylesheet" href="{{asset('css/custom.css')}}">
  <link rel="stylesheet" href="{{asset('css/googleapis.css')}}">
  <link rel="stylesheet" href="{{asset('css/ionicons.min.css')}}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> 
  <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
  <link rel="stylesheet" href="{{ asset('css/sweetalert.css') }}">
  <script src="{{ asset('js/sweetalert.min.js') }}"></script>
  <title>ControlPanda - Builder</title>
</head>

<body>
  <div id="LoaderStart" class="LoaderStart">
    <img id="loading" src="{{asset('images/pandaloading.png')}}">
  </div>
  <div id="root" ></div> 
  <script src="{{asset('js/editor.js')}} " ></script>
  <script src="{{asset('js/app.js')}}" ></script>
</body>
<script>

  // Called when Google Javascript API Javascript is loaded
  function HandleGoogleApiLibrary() {
    
    // Load "client" & "auth2" libraries
    gapi.load('client:auth2', {
      callback: function() {
        // Initialize client library
        // clientId & scope is provided => automatically initializes auth2 library
        gapi.client.init({
            apiKey: 'AIzaSyAGDM9ZnErXnV7WGbsE7tgXFw39o8d-43Y',
            clientId: '205318788731-cfj3pctnj7aaqe1bohg91g408vt623qm.apps.googleusercontent.com',
            scope: 'https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/photoslibrary.readonly',
            response_type: 'token'
        }).then(
          // On success
          function(success) {
              // After library is successfully loaded then enable the login button
              $("#login-button").removeAttr('disabled');
          }, 
          // On error
          function(error) {
            alert('Error : Failed to Load Library');
            }
        );
      },
      onerror: function() {
        // Failed to load libraries
      }
    });
  }
  
  // Click on login button
  $("body").on('click', "#login-button-google-photos",function() {
    $("#login-button-google-photos").attr('disabled', 'disabled');
        
    // API call for Google login
    gapi.auth2.getAuthInstance().signIn().then(
      // On success
      function(success) {
        // API call to get user information
        gapi.client.request({ path: 'https://photoslibrary.googleapis.com/v1/albums' }).then(
          // On success
          function(success) {
            console.log(success);
            var user_info = JSON.parse(success.body);
            // console.log(user_info);
            // console.log("Single Album: "+user_info.albums.albums.[0])
  
            // $("#user-information div").eq(0).find("span").text(user_info.displayName);
            // $("#user-information div").eq(1).find("span").text(user_info.id);
            // $("#user-information div").eq(2).find("span").text(user_info.gender);
            // $("#user-information div").eq(3).find("span").html('<img src="' + user_info.image.url + '" />');
            // $("#user-information div").eq(4).find("span").text(user_info.emails[0].value);
  
            // $("#user-information").show();
            // $("#login-button").hide();
          },
          // On error
          function(error) {
            $("#login-button").removeAttr('disabled');
            alert('Error : Failed to get user user information');
          }
        );
      },
      // On error
      function(error) {
        $("#login-button").removeAttr('disabled');
        alert('Error : Login Failed');
      }
    );
  });
  
  </script>
  <script async defer src="https://apis.google.com/js/api.js" onload="this.onload=function(){};HandleGoogleApiLibrary()" onreadystatechange="if (this.readyState === 'complete') this.onload()"></script>
</html>