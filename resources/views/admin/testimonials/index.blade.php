@extends('admin.layouts.app')

@section('content')

<div class="breadcrumbs contentarea">
    <div class="container-fluid">
    <ul>
        <li>
            <a href="{{url('/admin/dashboard')}}">Dashboard</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a>Testimonials</a>
        </li>
    </ul>
    <div class="close-bread">
        <a href="#"><i class="icon-remove"></i></a>
    </div>
</div>
</div>
<section class="contentarea">
    <div class="container-fluid">
        <div class="page-header"><h1>Testimonials  <span class="badge">{{@$total}}</span>
                @if(have_premission(36))
                <a href="{{url('/admin/testimonials/create')}}" class="btn btn-info pull-right">Add New Testimonial</a>
                @endif
            </h1></div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-content">
                        <div class="table-responsive">
                            <table class="table table-hover table-nomargin no-margin table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Message</th>
                                        <th>Rating</th>
                                        <th>Featured</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($testimonials as $testimonial) 
                                    <tr>
                                        <td>{!!$testimonial->id!!}</td>
                                        <td>{!!$testimonial->name!!}</td>
                                        <td>{!!$testimonial->message!!}</td>
                                        <td>{!!$testimonial->rating!!}</td>
                                        <td>@if($testimonial->featured == 1) <label class="label label-success">Yes</label> @else <label class="label label-danger">No</label> @endif</td>
                                        <td>
                                            @if(have_premission(37))
                                            <a href="{{ url('/admin/testimonials/'.$testimonial->id.'/edit')}}"><i class="fa fa-edit fa-fw"></i></a>
                                            @endif
                                            @if(have_premission(38))
                                            {!! Form::open([
                                            'method'=>'DELETE',
                                            'url' => ['admin/testimonials', $testimonial->id],
                                            'style' => 'display:inline'
                                            ]) !!}
                                            {!! Form::button('<i class="fa fa-trash fa-fw" title="Delete Testimonial"></i>', ['class' => 'delete-form-btn']) !!}
                                            {!! Form::submit('Delete', ['class' => 'hidden deleteSubmit']) !!}
                                            {!! Form::close() !!}
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                    @if (count($testimonials) == 0)
                                    <tr><td colspan="6"><div class="no-record-found alert alert-warning">No testimonial found!</div></td></tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <nav class="pull-right">{!! $testimonials->render() !!}</nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection