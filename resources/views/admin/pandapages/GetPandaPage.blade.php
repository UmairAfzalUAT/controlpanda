@extends('admin.layouts.app')
@section('content')
<div class="clear40"></div>
<section class="contentarea">
    <div class="container-fluid">        
        <div class="rt_getPanda">
            <div class="col-md-9 col-sm-8 col-xs-12">
                <div class="ui grid">
                    <h2 class="ui header">{!!@$industry->title!!} | {!!$template['config']['display_name']!!} &nbsp;&nbsp;&nbsp;<div class="btn-group change-device-btn"><button class="btn btn-info desktop-view-triger active"><i class="fa fa-desktop"></i></button><button class="btn btn-info mobile-view-triger"><i class="fa fa-mobile"></i></button></div></h2>
                </div>
            </div>
           
            <div class="text-center col-md-3 col-sm-4 col-xs-12">
                <div class="GetPanda_btn">
                    <a class="btn" href="{!!$project_edit!!}">Edit</a>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="row desktop-view">
            <div class="col-md-12">
                <div class="inner">
                        <iframe frameborder="0" scrolling="no" width="100%" onload="resizeIframe(this)" src="{!!$project_link!!}"></iframe>
                </div>
            </div>
        </div>
        <div class="row mobile-view">
            <div class="col-md-12">
                <div class="inner">
                    <iframe frameborder="0" scrolling="no" width="100%" onload="resizeIframe(this)" src="{!!$project_link!!}"></iframe>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(".desktop-view-triger").click(function(){
        $(".desktop-view-triger").addClass("active");
        $(".mobile-view-triger").removeClass("active");
        $(".mobile-view").toggle('slide');
        $(".desktop-view").delay(500).toggle('slide');
    });
     $(".mobile-view-triger").click(function(){
        $(".desktop-view-triger").removeClass("active");
        $(".mobile-view-triger").addClass("active");
        $(".desktop-view").toggle('slide');
        $(".mobile-view").delay(500).toggle('slide');
        
    });
    $(document).ready(function(){
        setTimeout(function(){
            $(".mobile-view").hide();
        },2000);
    });
</script>
<script>
  function resizeIframe(obj) {
    obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
  }
</script>
@endsection