@extends('admin.layouts.app')

@section('content')

@include('admin.sections.subheader')
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
    .dropdown-menu {
        min-width: auto;
    }
  #feedback { font-size: 1.4em; }
  #selectable .ui-selecting { background: none; }
  #selectable .ui-selected { background: none; color: #7ab428; }
  #selectable { list-style-type: none; margin: 0; padding: 0; width: 60%; }
  #selectable tr { margin: 3px; padding: 0.4em; font-size: 1.4em; height: 18px; }
  table tr td{ color: rgb(207, 206, 206);}
  </style>
<div class="container-fluid">
<section class="inner-full-width-panel pr-30">


		
					<div class="tab-content">
  <div id="menu4" class="tab-pane in">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-4">
							<div class="left-menu">
								<div class="heading-title">Website Statistics & opt in</div>
								<ul class="nav nav-tabs left-panel-menu">
                                            
                                            
                                            
                                            
                                           
                                            
                                            @foreach($pages as $page)
                                                    <li><a data-toggle="tab" href="#{!!$page!!}">{!!$page!!}</a></li>
                                            @endforeach
									
								</ul>
							</div>
						</div>
						<div class="col-lg-9 col-md-9 col-sm-8">
							<div class="row right-content pl-20">
								<div class="top-header">
									<ul>
										<li>Sorting</li>
										<li><div class="form-group">
						                <div class="input-group date form_date col-md-5" data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
						                    <input class="form-control badge-custom"  type="text" value="From" readonly>
						                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
											<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
						                </div>
										<input type="hidden" id="dtp_input2" value="" /><br/>
						            </div></li>
						            <li><div class="form-group">
						                <div class="input-group date form_date col-md-5" data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
						                    <input class="form-control badge-custom"  type="text" value="To" readonly>
						                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
											<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
						                </div>
										<input type="hidden" id="dtp_input2" value="" /><br/>
						            </div></li>
									</ul>
 
								</div>
							</div>
							<div class="tab-content">
                                                            
                                                            @foreach($pages as $page)
<!--								<div id="#" class="tab-pane in active show">-->
								<div id="{!!$page!!}" class="tab-pane">
									<div class="row">
										<div class="col-xl-6 col-lg-12">
											<div class="single-panel">
												<div class="left">
													<div class="padding-panel">
													<div class="viewQuote">
													{!!$pages_views[$page]!!}
													Views
													</div>
													</div>												
													<div class="separator-border-panel"></div>
													<div class="padding-panel float-left w-100">
													<ul class="single-panel-list">
														<li><a href="#">All</a></li>
														<li><a href="#">Unique</a></li>
													</ul>
													</div>
												</div>
												<div class="right">
													<div class="header">Weekly Page views</div>
													<div class="body">
									<img src="{{asset('frontend/images/graph1.jpg')}}">
													</div>
													<div class="footer">
													<img src="{{asset('frontend/images/day-graph.png')}}">
													</div>
												</div>											
											</div>
										</div>
										<div class="col-xl-6 col-lg-12">
											<div class="single-panel">
												<div class="left op2">
													<div class="padding-panel">
														<div class="viewQuote">
														{!!$pages_unique_views[$page]!!}
														Views
														</div>
													</div>												
													<div class="separator-border-panel"></div>
													<div class="padding-panel float-left w-100">
														<ul class="single-panel-list">
															<li><a href="#">All</a></li>
															<li><a href="#">Unique</a></li>
														</ul>
													</div>
												</div>
												<div class="right">
													<div class="header">Weekly Opt-ins</div>
													<div class="body">
													<img src="{{asset('frontend/images/graph2.jpg')}}">
													</div>
													<div class="footer">
													<img src="{{asset('frontend/images/day-graph.png')}}">
													</div>	
												</div>											
											</div>
										</div>									
									</div>
								</div>
                                                            @endforeach
								
								
								
							</div>
						</div>
					</div>
				</div>
				<div id="menu9" class="tab-pane">
					<div class="right-content right-content-space fixed-width">
                                <div class="header-title">
                                  <h3 class="warning left-part">Contacts</h3>
                                  <div class="right-part">
                                   <span>Export Contacts</span>
                                   <a href="#" class="icon-green">
                                    <img src="{{asset('frontend/images/msword.svg')}}">
                                   </a>
                                   <a href="#" class="icon-green">
                                    <img src="{{asset('frontend/images/print.svg')}}">
                                   </a>
                                  </div>
                                </div>
                            <div class="editor-dashboard-container">
                                <div class="dashboard-Contact-box">
                                	<div class="table-responsive">
                                        <table class="table min-width">
                                            @foreach($contacts as $cont)
                                            <tr>
                                              <td><div class="h6">1</div></td>
                                              <td>@if($cont->title){!!$cont->title!!}@endif @if($cont->full_name) {!!$cont->full_name!!} @else {!!$cont->first_name.' '.$cont->last_name!!} @endif</td>
                                              <td>
                                              <img class="img-fluid" src="{{asset('frontend/images/icon-4.jpg')}}">
                                              <span> {!!$cont->email!!}</span></td>
                                              <td> <img class="img-fluid" src="{{asset('frontend/images/icon-5.jpg')}}"> <span>{!!$cont->phone!!}</span></td>
                                              <td> <img class="img-fluid" src="{{asset('frontend/images/icon-6.jpg')}}"> <span>{!!$cont->ip_address!!}</span></td>
                                              <td> <img class="img-fluid" src="{{asset('frontend/images/icon-7.jpg')}}"> <span>{!!$cont->page_name!!}</span></td>
                                              <td>@if($cont->lead_status == 1)<img class="img-fluid" src="{{asset('frontend/images/icon-1.jpg')}}">  @else <img class="img-fluid" src="{{asset('frontend/images/icon-1.jpg')}}"> @endif</td>
                                              <td class="text-center" width="12%">
                                              <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".myModal">View Detail</button>
												<div class="modal myModal">
													<div class="modal-dialog')}}">
														<div class="modal-content">
															<div class="modal-header">
															<button type="button" class="close" data-dismiss="modal">&times;</button>
																<div class="popup-top-two">Tristan Mike</div>
			                                                    <div class="table-box">
			                                                    	<table class="table table-striped table-nomargin no-margin">
			                                                    		<thead>
																		    <tr>
																		      <th>Key</th>
																		      <th>Value</th>
																		    </tr>
																		  </thead>
				                                                    	<tbody>
				                                                            <tr>
				                                                                <td width="50%" height="30">Title</td>
				                                                                <td width="50%" height="30">MasterPillow</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Full Name</td>
				                                                                <td width="50%" height="30">TristanMike</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Full Name</td>
				                                                                <td width="50%" height="30">Trisan</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Last Name</td>
				                                                                <td width="50%" height="30">Mike</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Email</td>
				                                                                <td width="50%" height="30">masterpillow@gmail.com</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Phone</td>
				                                                                <td width="50%" height="30">+44 9784 847 8</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Fax</td>
				                                                                <td width="50%" height="30">+44 9784 878 1</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Zip</td>
				                                                                <td width="50%" height="30">STD5</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Address</td>
				                                                                <td width="50%" height="30">82 Grand Ave, Surbiton KT5 9HX, UK</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">City</td>
				                                                                <td width="50%" height="30">Bistin</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Company</td>
				                                                                <td width="50%" height="30">MasterPillow</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Designation</td>
				                                                                <td width="50%" height="30">HR Manager</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Date oF Birth</td>
				                                                                <td width="50%" height="30">May 10, 1984</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Ip Address</td>
				                                                                <td width="50%" height="30">34.145.11.14</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Lead Status</td>
				                                                                <td width="50%" height="30">5</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Created At</td>
				                                                                <td width="50%" height="30">May 31, 2018 02:44 pm
		</td>
				                                                            </tr>
				                                                        </tbody>
				                                                    </table>
			                                                    </div>
															</div>
														</div>
													</div>
												</div>
                                              </td>
                                            </tr>
                                            @endforeach
                                          </table>
                                      </div>
                                </div>
                            </div>
                            <div class="selected-item">
                                <span>Show </span>
                                <select>
                                  <option value="volvo">1</option>
                                  <option value="saab">2</option>
                                  <option value="vw">3</option>
                                  <option value="volvo">4</option>
                                  <option value="saab">5</option>
                                  <option value="volvo">6</option>
                                  <option value="saab">7</option>
                                  <option value="vw">8</option>
                                  <option value="volvo">9</option>
                                  <option value="saab">10</option>
                                  <option value="audi" selected>10</option>
                                </select>
                                <strong>user per page</strong>
                            </div>
              </div>
				</div>
				<div id="menu1" class="tab-pane active">
                                    

                                    
                                    
                                    <form action="{{ url('/admin/connect-domain')}}" id="connectdomainfrmdata" method="POST">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="project_id" id="connectdomproid" value="{{$ProjectData->id}}"  />
					<div class="right-content-space fix-width">
					<div class="editor-domain-container-heading')}}">
                                    <h3 class="Duplicate">Connect Domain</h3>
                                </div>
                                            
                                            
                        <div class="editor-domain-container">
                           <div class="domain-box">
                                <div class="table-responsive">
                                    <table class="table">
                                     <tbody id="selectable">
                                        @foreach($domains as $key => $dom)
                                        <tr data-id="{!!$dom->id!!}" id="checkedd{!!$dom->id!!}">
                                          <td><div class="h6">{!!$key+1!!}</div></td>
                                          <td style="width:80%;">{!!$dom->name!!}</td>
                                          <td class="text-center" style="width:15%;">
                                              <i class="far fa-check-circle" style="background-color:none;"></i>
                                          </td>
                                         </tr>
                                         
                                         
                                         <script>
  $(function(){
      
      $('#checkedd{!!$dom->id!!}').click(function(){
          $('tr').children().css( "color", "rgb(207, 206, 206)" );
          $("#checkedd{!!$dom->id!!} td" ).css( "color", "#7ab428" );
          domain_name = $("tr#checkedd{!!$dom->id!!}" ).attr( "data-id" );
        //alert(varss);
    });    
  });
</script>                               
         
                                         
                                        @endforeach
                                    </tbody></table>
                                </div>
                            </div>
                        </div>                                            
                                            
                                            
                                   
                    
                        <div class="editor-footer w-100 editor-domain-container-btn">
                            <button type="button" class="overbtnnone" id="connectiondomain">
                            <span class="fancy-btn-reverse float-right" title="">
                                <svg class="fancy-btn-reverse-left" width="466" height="603" viewBox="0 0 100 100" preserveAspectRatio="none">
                                    <path d="M0,0 L100,0 C15,50 50,100 0,100z"></path>
                                </svg>
                                Use Domain
                                <svg class="fancy-btn-reverse-right" width="466" height="603" viewBox="0 0 100 100" preserveAspectRatio="none">
                                    <path d="M0,0 L100,0 C15,50 50,100 0,100z"></path>
                                </svg>
                            </span>
                                </button>
                            <a href="{{url('admin/dashboard')}}" class="btn-preview float-right">Preview</a>
                        </div> 
                        </div>   
                                        
                                        
                                        </form>
				</div>
				<div id="menu2" class="tab-pane fade">
					<div class="right-content-space fix-width">
                                            <form action="{{ url('/admin/update-site-info')}}" method="POST">
                                                {{ csrf_field() }}
					<div class="editor-rename-container-heading')}}">
                        <h3 class="Duplicate">Rename Site</h3></div>
                        <div class="editor-rename-container">
                            <div class="objective-wrap">
                             <div class="Rename-box">
                                 
                                     <input type="hidden" name="action" value="rename"  />
                    <input type="hidden" name="project_id" value="{{$ProjectData->id}}"  />
                                     Current Name<br>
                                     <input type="text" name="firstname" class="bg-primary" disabled="" placeholder="{{$ProjectData->site_name}}"><br>
                                     New Site Name
                                     <input type="text" name="site_name" placeholder="Example" value="" required="">
           
                                </div>
                            </div>
                        </div>
                        <div class="editor-footer w-100 editor-rename-container-btn">       
                            <button type="submit" class="overbtnnone">
                            <span class="fancy-btn-reverse float-right" title="">
                                <svg class="fancy-btn-reverse-left" width="466" height="603" viewBox="0 0 100 100" preserveAspectRatio="none">
                                    <path d="M0,0 L100,0 C15,50 50,100 0,100z"></path>
                                </svg>
                                Save
                                <svg class="fancy-btn-reverse-right" width="466" height="603" viewBox="0 0 100 100" preserveAspectRatio="none">
                                    <path d="M0,0 L100,0 C15,50 50,100 0,100z"></path>
                                </svg>
                            </span>
                            </button>
                        </div>
                           </form>                 
                    </div>
				</div>
				<div id="menu3" class="tab-pane fade">
                                    <form action="{{ url('/admin/update-site-info')}}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="action" value="duplicate"  />
                    <input type="hidden" name="project_id" value="{{$ProjectData->id}}"  />
                    <input type="hidden" name="template" value="{{$ProjectData->template}}"  />
                    <input type="hidden" name="ind_id" value="{{$ProjectData->ind_id}}"  />
					<div class="right-content-space fix-width">
					<div class="editor-duplicate-container-heading')}}"><h3 class="Duplicate">Duplicate Site</h3></div>
					<div class="editor-duplicate-container">
                            <div class="objective-wrap">
                             <div class="duplicate-box">
                                  Enter Duplicate Site Name<br>
           <input type="text" name="site_name" value="" id="site_name" placeholder="Happy Life Hacks" required="required">
                                </div>
                            </div>
                        </div>
                        <div class="editor-footer w-100 editor-duplicate-container-btn">                 
                            <button type="submit" class="overbtnnone">
                            <span class="fancy-btn-reverse float-right" href="#" title="">
                                <svg class="fancy-btn-reverse-left" width="466" height="603" viewBox="0 0 100 100" preserveAspectRatio="none">
                                    <path d="M0,0 L100,0 C15,50 50,100 0,100z"></path>
                                </svg>
                                Confirm
                                <svg class="fancy-btn-reverse-right" width="466" height="603" viewBox="0 0 100 100" preserveAspectRatio="none">
                                    <path d="M0,0 L100,0 C15,50 50,100 0,100z"></path>
                                </svg>
                            </span>
                            </button>
                            
                        </div>
                    </div>
                               </form>     
                                    
				</div>
				<div class="tab-pane">
					
				</div>
				<div id="menu5" class="tab-pane fade">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-4">
							<div class="left-menu">
								<div class="heading-title">Settings</div>
								<ul class="nav nav-tabs left-panel-menu">
									<li><a data-toggle="tab" href="#setting-tab-1" class="active show">Email Settings</a></li>
									<li><a data-toggle="tab" href="#setting-tab-2" class="">Email List Integrations</a></li>
									<li><a data-toggle="tab" href="#setting-tab-3" class="">SMS Settings</a></li>
									<li><a data-toggle="tab" href="#setting-tab-4" class="">Domain Settings</a></li>
									<li><a data-toggle="tab" href="#setting-tab-5" class="">Tracking Codes</a></li>
								</ul>
							</div>
						</div>
						<div class="col-lg-9 col-md-9 col-sm-8">
						<div class="tab-content">
							<div id="setting-tab-1" class="tab-pane active in">
								<div class="email-setting">
                                                                   <form action="{{url('admin/add-action-leads')}}" method="post">
                                                                       
                                                                       {{csrf_field()}}
                        <input type="hidden" name="project_id" value="{{$ProjectData->id}}"  />
                        <input type="hidden" name="action" value="email"  />
									<div class="email-setting-head">Email Settings <p>(Enter the email address of all that will receive the enquiriess contact details.)</p></div>
									<div class="editor-dashboard-container dashboard-Contact-box">
										<div class="table-responsive">
											<table class="table">
                                                                                            <tbody id="email_form_list">
							
                        @if(isset($emailsdata->emails) && $emailsdata->emails!= '')
                        @php $emaillist = explode(',', $emailsdata->emails);@endphp
                        @foreach($emaillist as $key=>$mail)
                                                                                                    
                                                            <tr>
		                                              <td width="5%"><div class="h6">{{$key+1}}</div></td>
		                                              <td width="30%">
		                                              	<input name="email[]" type="email" value="{{$mail}}" id="email1" placeholder="Enter the email address"/>
		                                              </td>
                                                              @if($key > 0)
                                                              <td width="30%" class="text-right"><a href="javascript:void(0)" class="remove-email-address"><i class="fas fa-minus-circle "></i> Remove Email</a></td>
                                                              @endif
                                                              
<!--                                                              <td width="30%" class="text-right"></td>-->
		                                            </tr>
                                                            
                                                            @endforeach
                                                            @endif
		                                            </tbody>
                                                        </table>
                                                                                    
                                                                    <a href="javascript:void(0)" class="add-email-address">
                                                                        <button type="button" class="btn btn-primary">
                                                                            <i class="fas fa-plus-circle"></i> Add More Email 
                                                                        </button>
                                                                    </a>
                                                                		</div>
									</div>
						<div class="editor-footer w-100 editor-duplicate-container-btn">                 
                                                <button type="submit" class="overbtnnone">
			                            <span class="fancy-btn-reverse float-right" href="#" title="">
			                                <svg class="fancy-btn-reverse-left" width="466" height="603" viewBox="0 0 100 100" preserveAspectRatio="none">
			                                    <path d="M0,0 L100,0 C15,50 50,100 0,100z"></path>
			                                </svg>
			                                Confirm Email Address
			                                <svg class="fancy-btn-reverse-right" width="466" height="603" viewBox="0 0 100 100" preserveAspectRatio="none">
			                                    <path d="M0,0 L100,0 C15,50 50,100 0,100z"></path>
			                                </svg>
			                            </span>
                                                </button>
			                        </div>
                                                                </form>
								</div>
							</div>
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
							<div id="setting-tab-2" class="tab-pane fade">
                                                            <form>
                                                                <div id="alreadyaddedintegrations">

                    </div>
								<div class="integrations-code">
									<h3 class="">Email List Integrations</h3>
									<div class="editor-duplicate-container">
			                            <div class="integrations-wrap">
			                            	<div class="integrations-head">Action</div>
			                            	<select id="action_list" name="action">
			                            		<option value="add_to">Add to List</option>
			                            		<option value="remove_from">Remove from list</option>
			                            	</select>
			                            	<div class="integrations-head">Select List</div>
			                            	<select name="email_list_id" id="email_lists">
			                            		<option value="">-</option>
			                            	</select>
			                            	<div class="integrations-head">Pages</div>
			                            	<select id="page_name" name="page_name">
                                                            @foreach($paged as $page)
                                                                <option value="{!!$page['name']!!}">{!!$page['name']!!}</option>
                                                            @endforeach
			                            	</select>
			                            </div>
									</div>
									<div class="editor-footer w-100 editor-duplicate-container-btn">                 
			                            <button type="submit" class="overbtnnone">
			                            <span class="fancy-btn-reverse float-right" href="#" title="">
			                                <svg class="fancy-btn-reverse-left" width="466" height="603" viewBox="0 0 100 100" preserveAspectRatio="none">
			                                    <path d="M0,0 L100,0 C15,50 50,100 0,100z"></path>
			                                </svg>
			                                Save
			                                <svg class="fancy-btn-reverse-right" width="466" height="603" viewBox="0 0 100 100" preserveAspectRatio="none">
			                                    <path d="M0,0 L100,0 C15,50 50,100 0,100z"></path>
			                                </svg>
			                            </span>
                                                    </button>
			                        </div>
								</div>
                                                                </form>
							</div>
                                                    
                                                    
<script>
    $(document).on('click', '#setting-tab-2', function () {
        var project_name = '{!!$ProjectDatas->name!!}';
        $.ajax({
            type: 'GET',
            url: site_url + '/admin/get_email_lists',
            success: function (data) {
                $("#email_lists").html(data);
            }
        });
        $.ajax({
            type: 'POST',
            url: site_url + '/admin/get_integrations',
            data: {'project_name': project_name},
            success: function (data) {
                var html = '';
                var data_obj = jQuery.parseJSON(data);
                $.each(data_obj, function (e, v) {
                    html += '<div class="contentmenu-filter"><a data-id="' + v.id + '" href="javascript:void(0)" class="contentmenu-filter-remove" title="Remove Filter"><i class="fa fa-times"></i></a>' + v.action.replace("_", " ") + ' ' + v.list_title + ' - ' + v.page_name + '</div>';
                });
                $("#alreadyaddedintegrations").html(html);
            }
        });
        //$('#integrations-modal').modal('show');
    });

    $(document).on('submit', '#setting-tab-2 form', function (e) {
        var project_name = '{!!$ProjectDatas->name!!}';
        var page_name = $("#page_name").val();
        var email_list_id = $("#email_lists").val();
        var action = $("#action_list").val();
        $.ajax({
            type: 'POST',
                url: site_url + '/admin/add_integrations',
            data: {project_name: project_name, page_name: page_name, email_list_id: email_list_id, action: action},
            success: function (data) {

            }
        });
        //$('#integrations-modal').modal('hide');
        return false;
    });
    $(document).on("click", ".contentmenu-filter-remove", function (e) {
        e.stopPropagation();
        var element = $(this);
        var id = $(this).attr('data-id');
        $.ajax({
            type: 'POST',
            url: site_url + '/admin/remove_integrations',
            data: {'id': id},
            success: function (data) {
                element.parent('.contentmenu-filter').remove();
            }
        });
    });
</script>                                                    
                                                    
                                                    
                                                    
                                                    
							<div id="setting-tab-3" class="tab-pane fade">
								<div class="sms-settings">
                                                                    <form action="{{url('admin/add-action-leads')}}" method="post">
                                                                       
                                                                       {{csrf_field()}}
                        <input type="hidden" name="project_id" value="{{$ProjectData->id}}"  />
                        <input type="hidden" name="action" value="text"  />
									<div class="email-setting-head">SMS Settings <p>(Enter the mobile phone number of all that will receive the enquiries contact details.)</p></div>
                                                                        <div class="editor-dashboard-container dashboard-Contact-box">
                                                                            <div class="table-responsive">
                                                                                <table class="table">
                                                                                    <tbody id="text_form_list">
                                                                                        
                                                                                        @if(isset($textdata->phones) && $textdata->phones!= '')
                                                                                        @php $textlist = explode(',', $textdata->phones);@endphp
                                                                                        @foreach($textlist as $key=>$text)
                                                                                        <tr>
                                                                                            <td width="5%"><div class="h6">{{$key+1}}</div></td>
                                                                                            <td width="30%">
                                                                                                <input name="text[]" type="text" value="{{$text}}" id="text1" placeholder="Enter the phone number"/>
                                                                                            </td>
                                                                                            @if($key > 0)
                                                                                            <td width="30%" class="text-right"><a href="javascript:void(0)" class="remove-text-address"><i class="fas fa-minus-circle "></i> Remove Number</a></td>
                                                                                            @endif
                                                                                        </tr>

                                                                                        @endforeach
                                                                                        @endif
                                                                                    </tbody>
                                                                                </table>
                                                                                <a href="javascript:void(0)" class="add-text-address">
                                                                                    <button type="button" class="btn btn-primary">
                                                                                        <i class="fas fa-plus-circle"></i> Add More SMS </button>
                                                                                </a>
                                                                            </div>
                                                                        </div>
									<div class="editor-footer w-100 editor-duplicate-container-btn">                 
			                            <button type="submit" class="overbtnnone">
			                            <span class="fancy-btn-reverse float-right" href="#" title="">
			                                <svg class="fancy-btn-reverse-left" width="466" height="603" viewBox="0 0 100 100" preserveAspectRatio="none">
			                                    <path d="M0,0 L100,0 C15,50 50,100 0,100z"></path>
			                                </svg>
			                                Confirm Mobile Number
			                                <svg class="fancy-btn-reverse-right" width="466" height="603" viewBox="0 0 100 100" preserveAspectRatio="none">
			                                    <path d="M0,0 L100,0 C15,50 50,100 0,100z"></path>
			                                </svg>
			                            </span>
                                                    </button>
			                        </div>
                                                                        
                                                                </form>
								</div>
							</div>
                    <div id="setting-tab-4" class="tab-pane fade">

                            <div class="domain-code">
                                <form method="post" action="" id="formdatadomaction" >  
                                    {{csrf_field()}}
<!--                        <input type="hidden" name="project_id" value="{{$ProjectData->id}}"  />-->
                        <input type="hidden" name="action" value="settings" />
                                    <h3 class="">Domain Settings</h3>
                                    <div class="editor-duplicate-container">
                                        <div class="domain-wrap">
                                            <div class="domain-head">Domain</div>
                                            <select name="domain_id">
                                                <option value="">Choose a domain</option>
                                                @foreach($domains as $key => $dom)
                                                <option value="{!!$dom->id!!}">{!!$dom->name!!}</option>
                                                @endforeach
                                            </select>
                                            <div class="domain-head">Domain Status</div>
                                            <select name="domain_status">
                                                <option value="0">Deactive</option>
                                                <option value="1">Active</option>
                                            </select>
                                            <div class="domain-head">Assign Website</div>
                                            <select name="project_id">
                                                <option value="{{$ProjectData->id}}">{!!ucfirst($ProjectData->template)!!} | {!!$ProjectData->t_cat_name!!}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="editor-footer w-100 editor-duplicate-container-btn">
                                        <button type="button" class="overbtnnone">
                                        <span class="fancy-btn-reverse float-right" href="javascript:void(0)" id="clickdomsettings">
                                            <svg class="fancy-btn-reverse-left" width="466" height="603" viewBox="0 0 100 100" preserveAspectRatio="none">
                                            <path d="M0,0 L100,0 C15,50 50,100 0,100z"></path>
                                            </svg>
                                            Update
                                            <svg class="fancy-btn-reverse-right" width="466" height="603" viewBox="0 0 100 100" preserveAspectRatio="none">
                                            <path d="M0,0 L100,0 C15,50 50,100 0,100z"></path>
                                            </svg>
                                        </span>
                                        </button>
                                    </div>

                                </form>
                            </div>
                    </div>



<script>
  $(document).ready(function(){
     
$("#clickdomsettings").click(function () {
    //var connectdomproid = $("#connectdomproid").val();
    var form_data = $("#formdatadomaction").serializeArray();
    var _token = $('meta[name="csrf-token"]').attr('content');
     
    
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{ url('/admin/settings-domain')}}",
            type: 'post',
            //data: {domain_name:domain_name, project_id:connectdomproid, _token:_token},
            data: form_data,
            success: function (data) {
               if(data == "1"){
                    swal({
                        title: "Domain Setting Saved Successfully",
                        type: "success",
                        timer: 2000
                    });
                    setTimeout(function () { location.reload(true); }, 2000);
               }
            }
        });
    
}); 
  });
  
  </script>





  <div id="setting-tab-5" class="tab-pane fade">
      <form action="{{url('admin/panda-pages/save-trackingcode')}}" method="post">
          {{csrf_field()}}
          <div class="tracking-code">
              @foreach($paged as $page)    
              <h3 class="">Tracking Codes of {{ucfirst($page['name'])}} Page</h3>
              <div class="editor-duplicate-container">
<input type="hidden" name="page_name[]" value="{{$page['name']}}"  />
                  <div class="tracking-wrap">
                      <h2>Head Tracking Code</h2>
                      <div class="tracking-box">
                          <textarea name="header_code[]" class="form-control" rows="3">{!!@$page['tc']->header_code!!}</textarea>
                      </div>
                      <h6>Control Panda wide tracking code for the head tag</h6>
                      <h2>Footer Tracking Code</h2>
                      <div class="tracking-box">
                          <textarea name="footer_code[]" class="form-control" rows="3">{!!@$page['tc']->footer_code!!}</textarea>
                      </div>
                      <h6>Control Panda wide tracking codes for the body tag</h6>
                  </div>
              </div>
              @endforeach
              <div class="editor-footer w-100 editor-duplicate-container-btn">                 
                  <button type="button" class="overbtnnone">
                      <span class="fancy-btn-reverse float-right" href="javascript:void(0)" id="clickdomsettings">
                          <svg class="fancy-btn-reverse-left" width="466" height="603" viewBox="0 0 100 100" preserveAspectRatio="none">
                          <path d="M0,0 L100,0 C15,50 50,100 0,100z"></path>
                          </svg>
                          Save
                          <svg class="fancy-btn-reverse-right" width="466" height="603" viewBox="0 0 100 100" preserveAspectRatio="none">
                          <path d="M0,0 L100,0 C15,50 50,100 0,100z"></path>
                          </svg>
                      </span>
                  </button>
              </div>
          </div>
      </form>
  </div>
					</div>
					</div>
				</div>
			</div>
			<div id="menu6" class="tab-pane fade">
				<div class="right-content-space">
				<div class="editor-seo-container-heading')}}">
                                    <h3 class="">SEO </h3><p>(How you optimize your site so that it can be easily found and ranked by search engines like Google and more.)</p></div>
                    <div class="editor-seo-container">
                        <div class="seo-box">
                            <h4>SEO Status</h4>
                            <span>To get traffic from search engines to your site, this feature must be on.</span>
                            <div class="switch-buttons">
                             <div class="form-group">  
                                  <span class="switch switch-sm">
                                    <input type="checkbox" class="switch" id="switch-sm">
                                    <label for="switch-sm"></label>
                                    <p>Allow search engines to include your site in search results</p>
                                  </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="editor-seo-container-two">
                     <h4>Get Found on Google<br>
                        <span>Boost your site’s SEO with Wix SEO Wiz. Head to your step-by-step plan now.</span>
                        <a href="Website-dashboard-Seo-letsGo.html" target="_blank"><button type="button" class="btn btn-primary">Let's Go</button></a></h4>
                    </div>
                    <div class="editor-seo-container-heading')}}">
                    <h3 class="">Manage 301 Redirects</h3></div>
                    <div class="editor-seo-container-three">
                     <div class="jumbotron"><h3>A 301 redirect is how visitors and search engines automatically find your new websites and pages.</h3></div>
                        <div class="inner-box">
                         <div class="left-side">
                             <h3>What is 301 Redirect?</h3>
                                <span>A 301 redirect simply tells search engines, “we’ve moved to a new location.” Old site links get automatically forwarded to your new Panda pages, (e.g., the link to your old About page will lead to your new About page.</span>
                                <div style="margin-top:20px;">
                                 <h3>How does it work?</h3>
                                <span>When a visitor or search engine goes to your old address, they’ll automatically be sent to your new page. Visitors will find your new pages and search engines will keep your ranking.</span>
                                </div>
                            </div>
                            <div class="right-side text-center">
                            <img class="img-fluid" src="{{asset('frontend/images/save-icon-1.jpg')}}"></div>
                        </div>
                    </div>
                    <div class="editor-seo-container-four">
                     <h4>Redirect pages from your old site to your Panda site now
                        <button type="button" class="btn btn-primary dropdown-toggle">Let's Go</button>
                        <ul class="dropdown">
								<div class="jumbotron"><h4>Create a 301 redirect: Add the old page address on the left and choose your new control panda page on the right.</h4></div>
                            <div class="page-info">
                            	<div class="row">
                            		<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
                                	<h6>Old Page:</h6>
                                    <form action="#">
                                        <input type="text" name="firstname" class="btn-fild" placeholder="/about-us">
                                    </form>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
                                	<h6>Redirect to:</h6>
                                    <form action="#">
                                        <input type="text" name="firstname" class="btn-fild" placeholder="Select Page">
                                    </form>
                                </div>
	                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
	                                	<button type="button" class="btn btn-info">Save</button></div>
	                            	</div>
                                	<span>Don’t see your page?
                                	<strong><a href="#" data-toggle="modal" data-target=".myModal-pop">Add it manually</a>
                                		<div class="modal myModal-pop">
										    <div class="modal-dialog')}}">
										      <div class="modal-content">
										        <div class="modal-header">
										          <button type="button" class="close" data-dismiss="modal">&times;</button>
										          <div class="popup-top">Enter Page URL</div>
										          <div class="popup-bottom">
                                                    	<div class="jumbotron">
                                                        	<h3>Add the URL of your old site and redirect it to the URL of your new ControlPanda site</h3>
                                                            <div class="row">
                                                            	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <h6>Old Page:</h6>
	                                                                <form action="#">
	                                                                    <input type="text" name="firstname" class="btn-fild" placeholder="/about-us">
	                                                                </form>
	                                                            </div>
	                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
	                                                                <h6>Redirect to:</h6>
	                                                                <form action="#">
	                                                                    <input type="text" name="firstname" class="btn-fild" placeholder="Select Page">
	                                                                </form>
	                                                            </div>
                                                            </div>
                                                            <div class="btn-group">
                                                            	<button type="button" class="btn btn-primary">Save</button>
                                                            	<button type="button" class="btn btn-info">Cancel</button>
                                                            </div>
                                                        </div>
                                                    </div>
										        </div>
										      </div>
										    </div>
										  </div>
                                	</strong></span>
                                </div>
                                <div class="text-note">Note: Your 301 redirects will not work until you connect a domain.
                                <q>Connect Domain</q></div>
                            </div>
                        </ul>

                    </h4>
                    </div>
			</div>
		</div>
		


</section>
</div>


  <script>
  $(document).ready(function(){
     
$("#connectiondomain").click(function () {
    var connectdomproid = $("#connectdomproid").val();
    var form_data = $("#connectdomainfrmdata").serializeArray();
    var _token = $('meta[name="csrf-token"]').attr('content');
     
    if (domain_name != '') {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{ url('/admin/connect-domain')}}",
            type: 'post',
            data: {domain_name:domain_name, project_id:connectdomproid, _token:_token},
            //data: form_data,
            success: function (data) {
               if(data == "1"){
                    swal({
                        title: "Domain Connected Successfully",
                        type: "success",
                        timer: 2000
                    });
                    setTimeout(function () { location.reload(true); }, 2000);
               }
            }
        });
    }
}); 
  });
  
  </script>
  
  
  <script>
$(document).ready(function () {
    $(".filter_accordian, .start_date, .end_date").click(function (event) {
        $(".stats_content").slideToggle();
    });
    
    
     var email_count = <?php
if (isset($emailsdata->emails) && $emailsdata->emails != '') {
    $emaillist = explode(',', $emailsdata->emails);
    echo count($emaillist);
} else
    echo 3;
?>;
    var text_count = <?php
if (isset($textdata->phones) && $textdata->phones != '') {
    $textlist = explode(',', $textdata->phones);
    echo count($textlist);
} else
    echo 3;
?>;
    
     $(document).on('click', '.add-email-address', function () {
        email_count = email_count + 1;
            $("#email_form_list").append('<tr><td width="5%"><div class="h6">' + email_count + '</div></td><td width="30%"><input name="email[]" type="email" value="" id="email' + email_count + '" placeholder="Enter the email address"/></td><td width="30%" class="text-right"><a href="javascript:void(0)" class="remove-email-address"><i class="fas fa-minus-circle"></i> Remove Email</a></td></tr>');
            });
            
            $(document).on('click', '.remove-email-address', function(){
         email_count1 = 0;
            $(this).parent().parent().remove();
            email_count = $("#email_form_list tr td .h6").length;
            $("#email_form_list tr td .h6").each(function(){
                email_count1 = email_count1 + 1;
                $(this).text(email_count1);
                
            });
     });
            
            

    $(document).on('click', '.add-text-address', function () {
        text_count = text_count + 1;
        $("#text_form_list").append('<tr><td width="5%"><div class="h6">' + text_count + '</div></td><td width="30%"><input name="text[]" type="text" value="" id="text' + text_count + '" placeholder="Enter the phone number"/></td><td width="30%" class="text-right"><a href="javascript:void(0)" class="remove-text-address"><i class="fas fa-minus-circle"></i> Remove Number</a></td></tr>');
    });
            
            
    $(document).on('click', '.remove-text-address', function(){
         text_count1 = 0;
            $(this).parent().parent().remove();
            text_count = $("#text_form_list tr td .h6").length;
            $("#text_form_list tr td .h6").each(function(){
                text_count1 = text_count1 + 1;
                $(this).text(text_count1);
                
            });
     });
     
    
    
    
});
$("#stats_form").submit(function () {
    var form_data = $("#stats_form").serializeArray();

    $("#ui-block-loader").show();

    $.ajax({
        url: site_url + "/admin/panda-pages/{!!$ProjectData->id!!}/stats_ajax",
        type: 'POST',
        data: form_data,
        success: function (data) {
            $("#stats_data").html(data);
            $("#ui-block-loader").hide();
        }
    });
    return false;
});
</script>
  

@endsection