@extends('admin.layouts.app')
@section('content')











<section>
				
					<div class="left-panel-control" id="left-panel-open">
      <div id="site-menu">
					<div class="left-panel-top-head">Select Category</div>
                                        
                                        
                                        
                                        
                                        <div class="left-panel-scroll">
                                            <ul class="row left-nav-btn">
                                                @foreach($industries as $ind)
                                                <li class="">
                                                    <label for="industries{!!$ind->id!!}" class="btn btn-primary"><input type="checkbox" id="industries{!!$ind->id!!}" value="{!!$ind->id!!}" data-value="{!!$ind->title!!}" class="badgebox industries" name="industry"> {!!$ind->title!!}<span>10 Themes</span></label>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </div>
					<a class="fancy-btn btn-left-panel-btm" href="upgrade-packages.html" title="">
						<svg class="fancy-btn-left" width="466" height="603" viewbox="0 0 100 100" preserveAspectRatio="none">
							<path d="M0,0 L100,0 C15,50 50,100 0,100z"/>
						</svg>
						Upgrade
						<svg class="fancy-btn-right" width="466" height="603" viewbox="0 0 100 100" preserveAspectRatio="none">
							<path d="M0,0 L100,0 C15,50 50,100 0,100z"/>
						</svg>
					</a><!-- Fancy Button -->					
				</div>
				<a href="#" class="toggle-nav btn btn-purple" style="float: right" id="big-sexy"><i class="fas fa-angle-right"></i></a>
			</div>
				<div class="right-panel">
								<h2>Pick the website template you like</h2>

                                                                <div class="row" id="myaallwebsitetemplates">

                                                                </div>                                                                
                                                                
                                                                
<!--					<div class="row mt-40">
						<div class="col-lg-3 col-md-6 col-sm-12">
								<a href="#"><div class="home-img-as">
								<img src="{{asset('assets/images/promo-img-1.jpg')}}" alt="img-01" class="img-fluid">
								<div class="footer-img-home">
									<div class="img-mane">Club</div>
									<div class="page-number"><span>07</span>pages</div>
								</div>
								  <div class="overlay">
								  	<a href="manage-website.html">
									<div class="text"><span><img src="{{asset('assets/images/setting.svg')}}" class="svg-tool"></span> <i>Edit Site / Preview</i></div>
								</a>
								  </div>
						</div></a>
						</div>
						<div class="col-lg-3 col-md-6 col-sm-12">
							<a href="#"><div class="home-img-as">
								<img src="{{asset('assets/images/promo-img-2.jpg')}}" alt="img-02" class="img-fluid">
								<div class="footer-img-home">
									<div class="img-mane">Dance</div>
									<div class="page-number"><span>12</span>pages</div>
								</div>
								  <div class="overlay">
								  	<a href="manage-website.html">
									<div class="text"><span><img src="{{asset('assets/images/setting.svg')}}" class="svg-tool"></span> <i>Edit Site / Preview</i></div>
								</a>
								  </div>
							</div></a>
						</div>
						<div class="col-lg-3 col-md-6 col-sm-12">
							<a href="#"><div class="home-img-as">
								<img src="{{asset('assets/images/promo-img-3.jpg')}}" alt="img-03" class="img-fluid">
								<div class="footer-img-home">
									<div class="img-mane">Stand Out</div>
									<div class="page-number"><span>04</span>pages</div>
								</div>
								  <div class="overlay">
								  	<a href="manage-website.html">
									<div class="text"><span><img src="{{asset('assets/images/setting.svg')}}" class="svg-tool"></span> <i>Edit Site / Preview</i></div>
								</a>
								  </div>
							</div></a>
						</div>
						<div class="col-lg-3 col-md-6 col-sm-12">
							<a href="#"><div class="home-img-as">
								<img src="{{asset('assets/images/love-1.png')}}" alt="img-01" class="img-fluid">
								<div class="footer-img-home">
									<div class="img-mane">Love</div>
									<div class="pleft-panel left-panel-width-controlage-number"></div>
									<div class="page-number"><span>08</span>pages</div>
								</div>
								  <div class="overlay">
								  	<a href="manage-website.html">
									<div class="text"><span><img src="{{asset('assets/images/setting.svg')}}" class="svg-tool"></span> <i>Edit Site / Preview</i></div>
								</a>
								  </div>
							</div></a>
						</div>
						<div class="col-lg-3 col-md-6 col-sm-12">
							<a href="#"><div class="home-img-as">
								<div class="wrap-pic">
								<img src="{{asset('assets/images/tour.png')}}" alt="img-02" class="img-fluid">
								<div class="footer-img-home">
									<div class="img-mane">Tour</div>
									<div class="page-number"><span>08</span>pages</div>
								</div>
								  <div class="overlay">
								  	<a href="manage-website.html">
									<div class="text"><span><img src="{{asset('assets/images/setting.svg')}}" class="svg-tool"></span> <i>Edit Site / Preview</i></div>
								</a>
								  </div>
							</div>
						</div></a>
						</div>
						<div class="col-lg-3 col-md-6 col-sm-12">
							<a href="#"><div class="home-img-as">
								<img src="{{asset('assets/images/food.png')}}" alt="img-03" class="img-fluid">
								<div class="footer-img-home">
									<div class="img-mane">Food Party</div>
									<div class="page-number"><span>08</span>pages</div>
								</div>
								  <div class="overlay">
								  	<a href="manage-website.html">
									<div class="text"><span><img src="{{asset('assets/images/setting.svg')}}" class="svg-tool"></span> <i>Edit Site / Preview</i></div>
								</a>
								  </div>
							</div></a>
						</div>
						<div class="col-lg-3 col-md-6 col-sm-12">
							<a href="#"><div class="home-img-as">
								<img src="{{asset('assets/images/help.png')}}" alt="img-04" class="img-fluid">
								<div class="footer-img-home">
									<div class="img-mane">Help</div>
									<div class="page-number"><span>08</span>pages</div>
								</div>
								  <div class="overlay">
								  	<a href="manage-website.html">
									<div class="text"><span><img src="{{asset('assets/images/setting.svg')}}" class="svg-tool"></span> <i>Edit Site / Preview</i></div>
								</a>
								  </div>
							</div></a>
						</div>
						<div class="col-lg-3 col-md-6 col-sm-12">
							<a href="#"><div class="home-img-as">
								<img src="{{asset('assets/images/live-life.png')}}" alt="img-01" class="img-fluid">
								<div class="footer-img-home">
									<div class="img-mane">Live Life</div>
									<div class="page-number"><span>08</span>pages</div>
								</div>
								 <div class="overlay">
								  	<a href="manage-website.html">
									<div class="text"><span><img src="{{asset('assets/images/setting.svg')}}" class="svg-tool"></span> <i>Edit Site / Preview</i></div>
								</a>
								  </div>
							</div></a>
						</div>
					</div>-->
<!--					<div class="row">
					<a href="" class="more-btn text-center">See More</a>
				</div>-->
				</div>
		</section>















<script>
var industry_id = 0;
var category_id = 0;
var featured_value = 0;
var theme = '';
$(document).on('change', ".industries", function () {
    $("#step1_create_site").toggle('slide');
    $("#step2_create_site").delay(500).toggle('slide');
    //$(".selected_industry").html($(this).attr('data-value'));
    //$("#industry_selected").html($(this).attr('data-value'));
    industry_id = $(this).val();
    //$('.choose_template').click();
    
//});
//$(document).on('click', ".choose_template", function () {
//    $("#step2_create_site").toggle('slide');
//    $("#step3_create_site").delay(500).toggle('slide');

    $.ajax({
        url: site_url + "/admin/get_industry_templates",
        type: 'post',
        data: {'industry_id': industry_id, '_token': CSRF_TOKEN, 'start': 0},
        success: function (data) {
            //$("#templates_based_on_industries").html(data);
            $("#myaallwebsitetemplates").html(data);
            $("#mycontentarea").css('position', 'relative');
            $("#mycontentarea").css('top', 0);
            $(".clear80").hide();
        }
    });

});
$(document).on('click', ".lets_go", function () {
    $("#step2_create_site").toggle('slide');
    $("#step4_create_site").delay(500).toggle('slide');
    $("#mycontentarea").css('position', 'absolute');
    $("#mycontentarea").css('top', "70px");
    $(".clear80").show();
    $.ajax({
        url: site_url + "/admin/get_industry_categories",
        type: 'post',
        data: {'industry_id': industry_id, '_token': CSRF_TOKEN},
        success: function (data) {
            $("#categories_based_on_industries").html(data);
            setTimeout(function () {
                $("#category_id").select2();
            }, 500);
        }
    });
});
$(document).on("click", ".load_more_temp", function () {
    $.ajax({
        url: site_url + "/admin/get_industry_templates",
        type: 'post',
        data: {'industry_id': industry_id, '_token': CSRF_TOKEN, 'start': $(this).attr('data-start')},
        success: function (data) {
            $(".should_remove").remove();
            $("#templates_based_on_industries").append(data);
        }
    });
});
$(document).on("change", "#category_id", function () {
    $("#categories_based_on_industries").append('<div class="clear30 should_remove"></div><div class="col-md-12 text-center"><button type="button" class="btn btn-primary next_from_cat"> Next > </button></div>');
    category_id = $(this).val();
    $("#category_selected").html($("#category_id option:selected").text());
});
$(document).on('click', ".next_from_cat", function () {
    $("#step4_create_site").toggle('slide');
    $("#step5_create_site").delay(500).toggle('slide');
});
$(document).on('change', ".featurebox", function () {
    if ($(".featurebox:checked").length) {
        $(".next_from_feature").show();
        featured_value = $('.featurebox:checked').map(function () {
            return $(this).val();
        }).get().join();
    } else {
        $(".next_from_feature").hide();
    }
});
$(document).on("click", ".next_from_feature", function () {
    $("#step5_create_site").toggle('slide');
    $("#step6_create_site").delay(500).toggle('slide');
});
$(document).on('keyup', ".business_name", function () {
    if ($(".business_name").val() != '') {
        $("#business_name").html($(".business_name").val());
        $(".next_from_business_name").show();
    } else {
        $(".next_from_business_name").hide();
    }
});
$(document).on("click", ".next_from_business_name", function () {
    $("#step6_create_site").toggle('slide');
    $("#step7_create_site").delay(500).toggle('slide');
});
$(document).on('keyup', ".business_location", function () {
    if ($(".business_location").val() != '') {
        $("#business_location").html($(".business_location").val());
        $(".next_from_business_location").show();
    } else {
        $(".next_from_business_location").hide();
    }
});
$(document).on("click", ".next_from_business_location", function () {
    $("#step7_create_site").toggle('slide');
    $("#step8_create_site").delay(500).toggle('slide');
});
$(document).on("click", ".next_from_review", function () {
    $("#step8_create_site").toggle('slide');
    $("#step9_create_site").delay(500).toggle('slide');
});
$(document).on("click", ".next_from_clever", function () {
    $("#step9_create_site").toggle('slide');
    $("#step10_create_site").delay(500).toggle('slide');
    $.ajax({
        url: site_url + "/admin/get_themes",
        type: 'POST',
        data: {'_token': CSRF_TOKEN, 'start': 0},
        success: function (data) {
            $(".should_remove").remove();
            $("#themes_selection_area").append(data);
            $("#mycontentarea").css('position', 'relative');
            $("#mycontentarea").css('top', 0);
            $(".clear80").hide();
        }
    });
});
$(document).on("click", ".load_more_themes", function () {
    $.ajax({
        url: site_url + "/admin/get_themes",
        type: 'post',
        data: {'_token': CSRF_TOKEN, 'start': $(this).attr('data-start')},
        success: function (data) {
            $(".should_remove").remove();
            $("#themes_selection_area").append(data);
        }
    });
});
$(document).on("click", ".select-theme", function () {
    $("#step10_create_site").toggle('slide');
    $("#step11_create_site").delay(500).toggle('slide');
    $("#mycontentarea").css('position', 'absolute');
    $("#mycontentarea").css('top', "70px");
    $(".clear80").show();
    theme = $(this).attr('data-value');
});
$(document).on("click", ".next_from_create_my_site", function () {
    $("#step11_create_site").toggle('slide');
    $("#step12_create_site").delay(500).toggle('slide');
    setTimeout(function () {
        $(".progress-bar").css('width', "10%");
        $(".progress-bar").html("10% starting..");
        setTimeout(function () {
            $(".progress-bar").css('width', "20%");
            $(".progress-bar").html("20% searching by industry.");
        }, 300);
    }, 300);
    $.ajax({
        url: site_url + "/admin/build_site",
        type: 'post',
        data: {'industry_id': industry_id, '_token': CSRF_TOKEN, 'category_id': category_id, 'featured_value': featured_value, 'business_name': $(".business_name").val(), 'business_location': $(".business_location").val(), 'theme': theme},
        success: function (data) {
            if (data != '0') {
                setTimeout(function () {
                    $(".progress-bar").css('width', "30%");
                    $(".progress-bar").html("30% searching by business.");
                    setTimeout(function () {
                        $(".progress-bar").css('width', "50%");
                        $(".progress-bar").html("50% setting up features.");
                        setTimeout(function () {
                            $(".progress-bar").css('width', "70%");
                            $(".progress-bar").html("70% setting up theme.");
                            setTimeout(function () {
                                $(".progress-bar").css('width', "90%");
                                $(".progress-bar").html("90% customizing.");
                                setTimeout(function () {
                                    $(".progress-bar").css('width', "100%");
                                    $(".progress-bar").html("100% complete.");
                                    window.location.href = data;
                                }, 800);
                            }, 800);
                        }, 800);
                    }, 800);
                }, 800);
            } else {
                $("#step12_create_site").toggle('slide');
                $("#step1_create_site").delay(500).toggle('slide');
                $("#not_found_option").show();
            }
        }
    });
});
</script>
@endsection