@extends('admin.layouts.app')
@section('content')
<style>
    .contentmenu-filter{padding: 6px 12px;
                        background: #fafafa;
                        border: 1px solid #ccc;
                        border-radius: 4px;
                        margin-bottom: 10px;}
    .contentmenu-filter .contentmenu-filter-remove{float: right;}
</style>
<div class="clear40"></div>
<section class="contentarea">
    <div class="container-fluid">
        <div class="inner">
            @include('admin.pandamails.nav')
            <div class="right-contentarea">
                <div class="header2"> <i class="fa fa-list-alt"></i> Manage Email Lists
                    <div class="pull-right"><a class="btn btn-default header-btn" href="#">Panda mail walkthrough</a> </div>
                </div>
                <div class="clearfix"></div>
                <div class="clearfix"></div>
                <div class="inner-content">
                    <div class="well well-sm clearfix">
                       <form id="" method="get" action="{{url('/admin/contacts/lists')}}" class="">
                        <div class="row">
                            <div class="col-md-9 col-sm-7 col-xs-12">
                                <div id="imaginary_container"> 
                                    <div class="input-group stylish-input-group">
                                      
                                        <input type="text" class="form-control" placeholder="Search Contacts by name or email..." value="{{(isset($_GET['keyword']))? $_GET['keyword'] : ''}}" name="keyword" id="search_keyword" >
                                        <span class="input-group-addon">
                                            <button type="submit">
                                                <span class="glyphicon glyphicon-search"></span>
                                            </button>  
                                        </span>
                                      
                                    </div>
                                </div>
                            </div>
                            @if(have_premission(18))
                            <div class="col-md-3 col-sm-5 col-xs-12"> 
                                <a id="add_new_list" data-toggle="modal" data-target="#add_new_email_list" class="btn btn-default" title="Add new List" href="javascript:void(0)">
                                    <i class="fa fa-plus"></i>
                                    Add New List
                                </a> 
                                
                            </div>
                            @endif
                        </div>
                        </form>
                        
                        
                        <div class="clear10"></div>
                        <p class="contacts_total"> Viewing All Contacts <span class="badge">{!!$total!!} Contacts</span> Found </p>
                    </div>
                    <div class="table-responsive">
                        <table id="example" class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Email List Name</th>
                                    <th>Subscribe</th>
                                    <th>UnSubscribe</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($email_lists as $el)
                                <tr>
                                    <td>{!!$el->title!!}</td>
                                    <td>{!!$el->sub_count!!}</td>
                                    <td>{!!$el->unsub_count!!}</td>
                                    
                                    <td>
                                        @if(have_premission(19))
                                        <a class="btn btn-default btn-edit add-m-10" href="{!!url('admin/contacts/lists/'.$el->id.'/edit')!!}">Edit</a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <nav class="pull-right">{!! $email_lists->appends(request()->query())->render() !!}</nav>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
<div id="add_new_email_list" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add New Email List</h4>
            </div>
            <div class="modal-body">
                <form action="{{url('/admin/contacts/create-email-list')}}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name">List Name</label>
                        <input type="text" name="name" class="form-control" id="name" aria-describedby="nameHelp" placeholder="Enter List Name" required="required">
                        <small id="nameHelp" class="form-text text-muted">Name your email list.</small>
                    </div>
                    <div id="filters_div" style="display:none;"></div>
                    <div class="form-group">
                        <label>Set Filter</label>
                        <select id="keys_selecter" class="form-control" aria-describedby="keysHelp">
                            <option value="" disabled="" selected="" hidden="">Select filter parameter</option>
                            <option value="id" data-type="number">Id</option>
                            <option value="ip_address" data-type="text">IP Address</option>
                            <option value="created_at" data-type="date">Created At</option>
                            <option value="full_name" data-type="text">Full Name</option>
                            <option value="first_name" data-type="text">First Name</option>
                            <option value="last_name" data-type="text">Last Name</option>
                            <option value="company" data-type="text">Company</option>
                            <option value="designation" data-type="text">Designation</option>
                            <option value="email" data-type="text">Email</option>
                            <option value="phone" data-type="text">Phone</option>
                            <option value="fax" data-type="text">Fax</option>
                            <option value="city" data-type="text">City/District</option>
                            <option value="address" data-type="text">Address</option>
                            <option value="zip" data-type="text">Postal code</option>
                        </select>
                        <small id="keysHelp" class="form-text text-muted">Select filter parameter(key).</small>
                    </div>
                    <div class="form-group" id="comparison_div">

                    </div>
                    <div class="form-group" id="value_div">

                    </div>
                    <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <div class="clear5"></div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).on('change', '#keys_selecter', function () {
        var selected_value = $(this).val();
        if (selected_value != '') {
            $('#apply_custom_filter').removeClass('display_button');
        } else {
            $('#apply_custom_filter').addClass('display_button');
        }
        var text_comparisons = '<option value="is">is the value</option><option value="not">is not the value</option><option value="contains">contains the value</option><option value="notcontain">does not contain the value</option><option value="isempty">is empty</option><option value="isnotempty">is not empty</option>';
        var number_comparisons = '<option value="is">is the value</option><option value="not">is not the value</option><option value="greater">is greater than</option><option value="greatequal">is greater than or equal to</option><option value="lessequal">is less than or equal to</option><option value="less">is less than</option>';

        var date_comparisons = '<option value="is">is the value</option><option value="not">is not the value</option><option value="greater">is greater than</option><option value="greatequal">is greater than or equal to</option><option value="lessequal">is less than or equal to</option><option value="less">is less than</option>';

        var input_field = '<div class="input-group"><input type="text" class="form-control" id="search_value" placeholder="Enter Value"/><div class="input-group-btn"><button id="apply_custom_filter" class="btn btn-default" type="button">Add</button></div></div>';

        var data_type = $('#keys_selecter option[value="' + selected_value + '"]').attr('data-type');
        if (data_type == 'text') {
            $("#comparison_div").html('<select class="form-control" id="search_comparison">' + text_comparisons + '</select>');
            $("#value_div").html(input_field);
        } else if (data_type == 'date') {
            $("#comparison_div").html('<select class="form-control" id="search_comparison">' + date_comparisons + '</select>');
            $("#value_div").html(input_field);
        } else if (data_type == 'number') {
            $("#comparison_div").html('<select class="form-control" id="search_comparison">' + number_comparisons + '</select>');
            $("#value_div").html(input_field);
        }
    });
    $(document).on('click', '#apply_custom_filter', function () {
        if (($('#keys_selecter').val() != '' && $("#search_comparison").val() != '' && $("#search_value").val() != '') || ($('#keys_selecter').val() != '' && $("#search_comparison").val() == 'isempty') || ($('#keys_selecter').val() != '' && $("#search_comparison").val() == 'isnotempty')) {
            var keys_selecter = $('#keys_selecter').val();
            var keys_selecter_html = $('#keys_selecter option[value="' + keys_selecter + '"]').html();
            var compatison_operator = $("#search_comparison").val();
            var search_value = $("#search_value").val();
            var search_value_html = search_value;
            var search_key = keys_selecter;

            var hidden_fields = '<input type="hidden" class="search_key" name="search_key[]" value="' + search_key + '"><input type="hidden" name="comparison_op[]" class="comparison_op" value="' + compatison_operator + '"><input type="hidden" name="search_value[]" class="search_value" value="' + search_value + '">';

            var output_html = '<div class="contentmenu-filter">' + hidden_fields + '<a href="javascript:void(0)" class="contentmenu-filter-remove" title="Remove Filter"><i class="fa fa-times"></i></a><a href="javascript:void(0)" class="contentmenu-filter-edit" title="Edit Filter">' + keys_selecter_html + ' ' + compatison_operator + ' ' + search_value_html + '</a></div>';
            $("#filters_div").append(output_html);
            $('.editmode-open').remove();
            $("#filters_div").show();
            $("#comparison_div").html('');
            $("#value_div").html('');
            $('#keys_selecter').val('').trigger('change');
            $('.contentmenu-filter').removeClass('editmode-open');
        }
    });
    $("#add_new_email_list").on("hidden.bs.modal", function () {
        setTimeout(function () {
            $("#comparison_div").html('');
            $("#value_div").html('');
            $('#keys_selecter').val('').trigger('change');
            $('.contentmenu-filter').removeClass('editmode-open');
        }, 500);
    });
    $(document).on("click", ".contentmenu-filter-remove", function (e) {
        e.stopPropagation();
        $(this).parent('.contentmenu-filter').remove();
        if ($("#filters_div").html() == "") {
            $("#filters_div").hide();
        }
    });
    $(document).on("click", ".contentmenu-filter-edit", function () {
        var element = $(this).parent('.contentmenu-filter');
        var search_key = element.find('.search_key').val();
        var comparison_op = element.find('.comparison_op').val();
        var search_value = element.find('.search_value').val();
        setTimeout(function () {
            $('#keys_selecter').val(search_key).trigger('change');
            $("#search_comparison").val(comparison_op);
            $("#search_value").val(search_value);
            element.addClass('editmode-open');
        }, 200);
    });
</script>
@endsection