@extends('admin.layouts.app')

@section('content')

<div class="breadcrumbs contentarea">
    <div class="container-fluid">
    <ul>
        <li>
            <a href="{{url('/admin/dashboard')}}">Dashboard</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a>Blog</a>
        </li>
    </ul>
    <div class="close-bread">
        <a href="#"><i class="icon-remove"></i></a>
    </div>
</div>
</div>
<section class="contentarea">
    <div class="container-fluid">
        <div class="page-header"><h1>Blog <span class="badge">{{$total}}</span>
                @if(have_premission(84))
                <a href="{{url('/admin/blogs/create')}}" class="btn btn-info pull-right">Add New Post</a>
                @endif
                <div class="clearfix"></div>
            </h1></div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-content">
                        <div class="table-responsive">
                            <table class="table table-hover table-nomargin no-margin table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Image</th>
                                        <th>Category</th>
                                        <th>Blog Title</th>
                                        <th>Short Description</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($blog as $bg) 
                                    <tr>
                                        <td>{!!$bg->id!!}</td>
                                        <td>@if($bg->image!="") <img src="{{url("/uploads/blogs")}}/{{$bg->image}}" style="height: 50px; border-radius: 10px;"> @endif</td>
                                        <td>{!!$bg->name!!}</td>
                                        <td>{!!$bg->title!!}</td>
                                        <td>{!!$bg->short_description!!}</td>
                                        <td>@if($bg->is_active == 1) <label class="label label-success">Active</label> @else <label class="label label-danger">Inactive</label> @endif</td>
                                        <td>
                                            @if(have_premission(85))
                                            <a href="{{ url('/admin/blogs/'.$bg->id.'/edit')}}"><i class="fa fa-edit fa-fw"></i></a>
                                            @endif
                                            @if(have_premission(86))
                                            {!! Form::open([
                                            'method'=>'DELETE',
                                            'url' => ['admin/blogs', $bg->id],
                                            'style' => 'display:inline'
                                            ]) !!}
                                            {!! Form::button('<i class="fa fa-trash fa-fw" title="Delete Posts"></i>', ['class' => 'delete-form-btn']) !!}
                                            {!! Form::submit('Delete', ['class' => 'hidden deleteSubmit']) !!}
                                            {!! Form::close() !!}
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                    @if (count($blog) == 0)
                                    <tr><td colspan="7"><div class="no-record-found alert alert-warning">No blog post found!</div></td></tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <nav class="pull-right">{!! $blog->render() !!}</nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection