@extends('admin.layouts.app')

@section('content')
<!-- Validation -->
<script src="{{ asset('js/plugins/validation/jquery.validate.min.js')}}"></script>
<script src="{{ asset('js/plugins/jquery-ui/jquery.ui.spinner.js')}}"></script>
<script src="{{ asset('js/plugins/ckeditor/ckeditor.js')}}"></script>
<link rel="stylesheet" href="{{ asset('css/plugins/select2/select2.css') }}">
<script src="{{ asset('js/plugins/select2/select2.min.js')}}"></script>

<div class="breadcrumbs contentarea">
    <div class="container-fluid">
        <ul>
            <li>
                <a href="{{url('/admin/dashboard')}}">Dashboard</a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a href="{{url('/admin/blogs')}}">Blog</a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a>{!!$action!!} Post</a>
            </li>
        </ul>
        <div class="close-bread">
            <a href="#"><i class="icon-remove"></i></a>
        </div>
    </div>
</div>

<section class="contentarea">
    <div class="container-fluid">
        <div class="page-header"><h1>{!!$action!!} Blog</h1></div>
        <div class="box">
            <div class="box-content">
                <form id="lg-form" enctype="multipart/form-data" class="form-horizontal form-validate" action="{{url('/admin/blogs')}}" method="POST" novalidate="novalidate">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="name">Blog Category</label>
                        <div class="col-sm-5">
                            <select class="form-control" name="blog_category_id" id="blog_category_id" data-rule-required="true" aria-required="true">
                                <option value="">Select category</option>
                                @if($categories)
                                @foreach($categories as $cat)
                                <option @if(isset($Blog['blog_category_id']) && $Blog['blog_category_id'] == $cat->id) selected @endif value="{{$cat->id}}">{{$cat->name}}</option>
                                @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <input type="hidden" name="action" value="{!!$action!!}">
                    <input type="hidden" name="id" value="{!!@$Blog['id']!!}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="title">Title</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="title" id="title" placeholder="Enter Blog Title" data-rule-required="true" aria-required="true" value="{!!@$Blog['title']!!}"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="slug">Slug</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" readonly="readonly" name="slug" id="seo_url" value="{!!@$Blog['slug']!!}"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="name">Short Description</label>
                        <div class="col-sm-5">
                            <textarea rows="5"  placeholder="Enter Short description" type="text" class="form-control" name="short_description" id="short_description">{!!@$Blog['short_description']!!}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="name">Image</label>
                        <div class="col-sm-2">
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="fileupload-new thumbnail" style="max-width: 200px; max-height: 150px;">
                                    @if(isset($Blog['image']) && ($Blog['image'] != ''))
                                    <img class="image-display" src="{{URL::to('uploads/blogs/'.$Blog['image'])}}" />
                                    @else 
                                    <img class="image-display" src="{{URL::to('public/frontend/images/default.png')}}" />
                                    @endif 
                                </div>
                                <div>
                                    <div class="clear5"></div>
                                    <input accept="image/*" class="image-input" type="file" name='image'/>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="name">Description</label>
                        <div class="col-sm-9">
                            <textarea data-rule-required="true" aria-required="true" name="description" class='form-control ckeditor' rows="10">{!!@$Blog['description']!!}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Meta Title</label>
                        <div class="col-sm-4">
                            <input placeholder="Enter Meta Title"  type="text" class="form-control" id="meta_title" name="meta_title" value="{!!@$Blog['meta_title']!!}"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Meta Keywords</label>
                        <div class="col-sm-4">
                            <input placeholder="Enter Meta Keywords"  type="text" class="form-control" id="meta_keywords" name="meta_keywords" value="{!!@$Blog['meta_keywords']!!}"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Meta Description</label>
                        <div class="col-sm-4">
                            <textarea placeholder="Enter Meta Description"  class="form-control" style="height:100px;" id="meta_description" name="meta_description">{!!@$Blog['meta_description']!!}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="is_active">Status</label>
                        <div class="col-sm-4">
                            <input type="radio" name="is_active" value="1" @if(!isset($Blog['is_active']) || $Blog['is_active'] == 1) checked @endif /> Active &nbsp;&nbsp; <input type="radio" name="is_active" value="0"  @if(isset($Blog['is_active']) && $Blog['is_active'] == 0) checked @endif/> Inactive
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="col-sm-3 control-label"></div>
                        <a href="{{url('/admin/blog')}}" class="btn btn-default">Cancel</a>
                        <button type="submit" class="btn btn-success">Continue</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<script>
$("#title").keyup(function () {
    var title = $("#title").val();
    $("#seo_url").val(convertToSlug(title));
});
$("#title").blur(function () {
    var name = $("#name").val();
    $("#seo_url").val(convertToSlug(title));
});
function convertToSlug(Text)
{
    return Text.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');
}
</script>
@endsection
