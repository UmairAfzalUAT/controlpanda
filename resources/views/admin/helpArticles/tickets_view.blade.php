@extends('admin.layouts.app')

@section('content')
<!-- Validation -->
<script src="{{ asset('js/plugins/validation/jquery.validate.min.js')}}"></script>
<script src="{{ asset('js/plugins/jquery-ui/jquery.ui.spinner.js')}}"></script>
<script src="{{ asset('js/plugins/ckeditor/ckeditor.js')}}"></script>
<link rel="stylesheet" href="{{ asset('css/plugins/select2/select2.css') }}">
<script src="{{ asset('js/plugins/select2/select2.min.js')}}"></script>

<div class="breadcrumbs contentarea">
    <div class="container-fluid">
        <ul>
            <li>
                <a href="{{url('/admin/dashboard')}}">Dashboard</a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a href="{{url('/admin/tickets')}}">Tickets</a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a>Reply Ticket</a>
            </li>
        </ul>
        <div class="close-bread">
            <a href="#"><i class="icon-remove"></i></a>
        </div>
    </div>
</div>

<section class="contentarea">
    <div class="container-fluid">
        <div class="page-header"><h1>Reply Ticket</h1></div>
        <div class="box">
            <div class="box-content">
                <form id="lg-form" enctype="multipart/form-data" class="form-validate" action="{{url('/admin/tickets/replyticket')}}" method="POST" novalidate enctype="multipart/form-data"  >
                    <input type="hidden" name="ticket_id" value="{{$ticket_id}}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="col-sm-12 text-center"><h2>{{$replay[0]->question}}</h2></label>
                    </div>
                        <div class="clear20"></div>
                    <div class="form-group">
                            @foreach ($replay as $rep)
                            <div class=" @if( $rep->user_id == Auth::user()->id ) right_allign_msg @else left_allign_msg @endif">
                                <div class="img-responsive chat_img pull-left">
                                    <img src="{!!checkImage('users/'.$rep->profile_image)!!}">
                                </div>
                                <div class="chat-body clearfix">
                                    <b>{!! $rep->first_name.' '.$rep->last_name  !!}</b> &nbsp;&nbsp;
                                    <span><i class="fa fa-clock-o"></i> {!!display_date_time($rep->created_at)!!}</span>
                                    <div class="descrip">
                                        {!! $rep->replay !!}
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            @endforeach       
                            <br>                            
                            <br>                            
                            <br>                            
                            <textarea data-rule-required="true" aria-required="true" name="reply" class='form-control' rows="10"></textarea> 
                    </div>

                    <div class="form-actions">
                        <div class="row">
                            <div class="col-sm-offset-3 col-sm-9 text-right">
                                <a href="{{url('/admin/tickets')}}" class="btn btn-default">Cancel</a>
                                <button type="submit" class="btn btn-success">Continue</button>
                            </div>
                        </div>    
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

@endsection
