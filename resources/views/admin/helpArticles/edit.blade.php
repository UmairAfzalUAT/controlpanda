@extends('admin.layouts.app')

@section('content')
<!-- Validation -->
<script src="{{ asset('js/plugins/validation/jquery.validate.min.js')}}"></script>
<script src="{{ asset('js/plugins/jquery-ui/jquery.ui.spinner.js')}}"></script>
<script src="{{ asset('js/plugins/ckeditor/ckeditor.js')}}"></script>
<link rel="stylesheet" href="{{ asset('css/plugins/select2/select2.css') }}">
<script src="{{ asset('js/plugins/select2/select2.min.js')}}"></script>

<div class="breadcrumbs contentarea">
    <div class="container-fluid">
    <ul>
        <li>
            <a href="{{url('/admin/dashboard')}}">Dashboard</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="{{url('/admin/help-articles')}}">Help Articles</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a>{!!$action!!} Help Article</a>
        </li>
    </ul>
    <div class="close-bread">
        <a href="#"><i class="icon-remove"></i></a>
    </div>
</div>
</div>

<section class="contentarea">
    <div class="container-fluid">
        <div class="page-header"><h1>{!!$action!!} Help Articles</h1></div>
        <div class="box">
            <div class="box-content">
                <form id="lg-form" enctype="multipart/form-data" class="form-horizontal form-validate" action="{{url('/admin/help-articles')}}" method="POST" novalidate="novalidate" enctype="multipart/form-data"  >
                <div class="form_wrap">  
                    <input type="hidden" name="action" value="{!!$action!!}">
                    <input type="hidden" name="id" value="{!!@$category['id']!!}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="title">Question</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="question" id="title" placeholder="Enter question" data-rule-required="true" aria-required="true" value="{!!@$category['question']!!}"/>
                        </div>
                    </div>
					
					<div class="form-group">
                        <label class="col-sm-4 control-label" for="title">KeyWords</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="keywords" id="keywords" placeholder="Enter keywords " data-rule-required="true" aria-required="true" value="{!!@$category['keywords']!!}"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="slug">Slug</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" readonly="readonly" name="slug" id="seo_url" value="{!!@$category['slug']!!}"/>
                        </div>
                    </div>
                  
                    <div class="form-group">
                        <label class="col-sm-4 control-label pt_0" for="status">Status</label>
                        <div class="col-sm-8">
                            <input type="radio" name="status" value="1" @if(!isset($category['status']) || $category['status'] == 1) checked @endif /> Active &nbsp;&nbsp; <input type="radio" name="status" value="0"  @if(isset($category['status']) && $category['status'] == 0) checked @endif/> Inactive
                        </div>
                    </div>
				
				    <div class="form-group">
                        <label class="col-sm-4 control-label" for="type">Image</label>
                        <div class="fileupload fileupload-new col-md-8" data-provides="fileupload">
                            <div class="fileupload-new thumbnail" style="max-width: 200px; max-height: 150px;">
                              @if(!empty($category['images']))
                                <img class="image-display" src="{{URL::to('uploads/helparticles/'.$category['images'])}}" />
							 @else 
                                <img class="image-display" src="{{URL::to('public/frontend/images/default.png')}}" />
                               @endif
                            </div>
                            <div >
                                <span class="btn btn-file col-sm-12 select_logo"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span>
                                <input accept="image/*" class="image-input" type="file" name="images" id="image-upload"/></span>
                                <a href="javascipt:void(0);" class="btn fileupload-exists remove_btn" data-dismiss="fileupload">Remove</a>
                            </div>
                        </div>
                    </div>
                </div>     
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Answer</label>
                        <div class="col-sm-9">                          
                        <textarea data-rule-required="true" aria-required="true" name="answer" class='form-control ckeditor' rows="10">{!!@$category['answer']!!}</textarea>
                            
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-sm-offset-2 col-sm-9 text-right">
                                <a href="{{url('/admin/help-articles')}}" class="btn btn-default">Cancel</a>
                                <button type="submit" class="btn btn-success">Continue</button>
                            </div>
                        </div>    
                    </div>
                   
            </form>
            </div>
        </div>
    </div>
</section>
<script>
    $("#title").keyup(function () {
        var title = $("#title").val();
        $("#seo_url").val(convertToSlug(title));
    });
    $("#title").blur(function () {
        var name = $("#title").val();
        $("#seo_url").val(convertToSlug(title));
    });
	
	$('.remove_btn').click(function(){
	  
	  $("#image-upload").val('');
	
	});
	
    function convertToSlug(Text)
    {
        return Text.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');
    }
</script>
@endsection
