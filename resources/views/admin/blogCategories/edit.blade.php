@extends('admin.layouts.app')

@section('content')
<!-- Validation -->
<script src="{{ asset('js/plugins/validation/jquery.validate.min.js')}}"></script>
<script src="{{ asset('js/plugins/jquery-ui/jquery.ui.spinner.js')}}"></script>

<div class="breadcrumbs contentarea">
    <div class="container-fluid">
    <ul>
        <li>
            <a href="{{url('/admin/dashboard')}}">Dashboard</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="{{url('/admin/blog-category')}}">Blog Categories</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a>{!!$action!!} Blog Category</a>
        </li>
    </ul>
    <div class="close-bread">
        <a href="#"><i class="icon-remove"></i></a>
    </div>
	</div>
</div>
<section class="contentarea">
    <div class="container-fluid">
        <div class="page-header"><h1>{!!$action!!} Blog Category</h1></div>
        <div class="box">
            <div class="box-content">
                <form id="lg-form" class="form-horizontal form-validate" action="{{url('/admin/blog-category')}}" method="POST" novalidate="novalidate">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="name">Category Name</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="name" id="name" placeholder="Enter Category Name" data-rule-required="true" aria-required="true" value="{!!@$BlogCategories['name']!!}"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="slug">Slug</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" readonly="readonly" name="slug" id="seo_url" value="{!!@$BlogCategories['slug']!!}"/>
                        </div>
                    </div>
                    <input type="hidden" name="action" value="{!!$action!!}">
                    <input type="hidden" name="id" value="{!!@$BlogCategories['id']!!}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="is_active">Status</label>
                        <div class="col-sm-4">
                            <input type="radio" name="is_active" value="1" @if(!isset($BlogCategories['is_active']) || $BlogCategories['is_active'] == 1) checked @endif /> Active &nbsp;&nbsp; <input type="radio" name="is_active" value="0"  @if(isset($BlogCategories['is_active']) && $BlogCategories['is_active'] == 0) checked @endif/> Inactive
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="col-sm-3 control-label"></div>
                        <a href="{{url('/admin/blog-category')}}" class="btn btn-default">Cancel</a>
                        <button type="submit" class="btn btn-success">Continue</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<script>
$("#name").keyup(function(){
    var name = $("#name").val();
    $("#seo_url").val(convertToSlug(name));
});
$("#name").blur(function(){
    var name = $("#name").val();
    $("#seo_url").val(convertToSlug(name));
});
function convertToSlug(Text)
{
    return Text.toLowerCase().replace(/ /g,'-').replace(/[^\w-]+/g,'');
}
</script>
@endsection
