@extends('admin.layouts.app')
@section('content')
<?php
if (isset($cmsPage['is_static']) && $cmsPage['is_static'] == 1) {
    $url = $cmsPage['description'];
    $text = file_get_contents($url);
    $text = str_replace('textarea', 'textbox', $text);
}
?>
<!-- Validation -->
<script src="{{ asset('js/plugins/validation/jquery.validate.min.js')}}"></script>
<script src="{{ asset('js/plugins/validation/additional-methods.min.js')}}"></script>
<script src="{{ asset('js/plugins/ckeditor/ckeditor.js')}}"></script>

<div class="breadcrumbs contentarea">
    <div class="container-fluid">
        <ul>
            <li>
                <a href="{{url('/admin/dashboard')}}">Dashboard</a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a href="{{url('/admin/cms-pages')}}">CMS Pages</a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a>{!!$action!!} CMS Page</a>
            </li>
        </ul>
        <div class="close-bread">
            <a href="#"><i class="icon-remove"></i></a>
        </div>
    </div>
</div>
<section class="contentarea">
    <div class="container-fluid">
        <div class="page-header"><h1>{!!$action!!} CMS Page</h1></div>
        <div class="box">
            <div class="box-content">
                <form  id="cms-form" class="form-horizontal form-validate" action="{{url('/admin/cms-pages')}}" method="POST" novalidate="novalidate">
                    <div class="form_wrap">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Title</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="title" id="title" placeholder="Enter Title" data-rule-required="true" aria-required="true" value="{!!@$cmsPage['title']!!}"/>
                            </div>
                        </div>
                        <input type="hidden" name="action" value="{!!$action!!}">
                        <input type="hidden" name="id" value="{!!@$cmsPage['id']!!}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Slug</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="seo_url" id="seo_url" readonly="readonly" value="{!!@$cmsPage['seo_url']!!}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Meta Title</label>
                            <div class="col-sm-8">
                                <input placeholder="Enter Meta Title"  type="text" class="form-control" id="meta_title" name="meta_title" value="{!!@$cmsPage['meta_title']!!}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Meta Keywords</label>
                            <div class="col-sm-8">
                                <input placeholder="Enter Meta Keywords"  type="text" class="form-control" id="meta_keywords" name="meta_keywords" value="{!!@$cmsPage['meta_keywords']!!}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Meta Description</label>
                            <div class="col-sm-8">
                                <textarea placeholder="Enter Meta Description"  class="form-control" style="height:100px;" id="meta_title" name="meta_description">{!!@$cmsPage['meta_description']!!}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label pt_0">Show in Header</label>
                            <div class="col-sm-8">
                                <input type="radio" name="show_in_header" value="1" @if(!isset($cmsPage['show_in_header']) || $cmsPage['show_in_header'] == 1) checked @endif /> Yes &nbsp;&nbsp; <input type="radio" name="show_in_header" value="0"  @if(isset($cmsPage['show_in_header']) && $cmsPage['show_in_header'] == 0) checked @endif /> No
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label pt_0">Show in Footer</label>
                            <div class="col-sm-8">
                                <input type="radio" name="show_in_footer" value="1" @if(!isset($cmsPage['show_in_footer']) || $cmsPage['show_in_footer'] == 1) checked @endif /> Yes &nbsp;&nbsp; <input type="radio" name="show_in_footer" value="0"  @if(isset($cmsPage['show_in_footer']) && $cmsPage['show_in_footer'] == 0) checked @endif /> No
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label pt_0">Status</label>
                            <div class="col-sm-8">
                                <input type="radio" name="is_active" value="1" @if(!isset($cmsPage['is_active']) || $cmsPage['is_active'] == 1) checked @endif /> Active &nbsp;&nbsp; <input type="radio" name="is_active" value="0"  @if(isset($cmsPage['is_active']) && $cmsPage['is_active'] == 0) checked @endif /> Inactive
                            </div>
                        </div>
                    </div>    
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-9">
                            @if(isset($cmsPage['is_static']) && $cmsPage['is_static'])
                            <textarea data-rule-required="true" aria-required="true" name="file_content" class="form-control" style="min-height: 500px">{!!$text!!}</textarea>
                            <input type="hidden" name="description" value="{!!$cmsPage['description']!!}">
                            @else
                            <textarea data-rule-required="true" aria-required="true" name="description" class='form-control ckeditor' rows="10">{!!@$cmsPage['description']!!}</textarea>
                            @endif

                        </div>
                    </div>

                    <div class="form-actions">
                        <div class="row">
                            <div class="col-sm-offset-2 col-sm-9 text-right">
                                <a href="{{url('/admin/cms-pages')}}" class="btn btn-default">Cancel</a>
                                <button class="btn btn-success" type="submit">Save</button>
                            </div>
                        </div>    
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<script>
$("#title").keyup(function () {
    var title = $("#title").val();
    $("#seo_url").val(convertToSlug(title));
});
$("#title").blur(function () {
    var title = $("#title").val();
    $("#seo_url").val(convertToSlug(title));
});
function convertToSlug(Text)
{
    var text = Text.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');
    return text.replace('--', '-');
}
</script>

@endsection