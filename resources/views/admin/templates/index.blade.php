@extends('admin.layouts.app')

@section('content')

<div class="breadcrumbs contentarea">
    <div class="container-fluid">
        <ul>
            <li>
                <a href="{{url('/admin/dashboard')}}">Dashboard</a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a>Templates</a>
            </li>
        </ul>
        <div class="close-bread">
            <a href="#"><i class="icon-remove"></i></a>
        </div>
    </div>
</div>
<section class="contentarea">
    <div class="container-fluid">
        <div class="page-header"><h1>Templates <a href="{{url('/admin/templates/create')}}" class="btn btn-info pull-right">Add New Template</a></h1></div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-content">
                        <div class="table-responsive">
                            <table class="table table-hover table-nomargin no-margin table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Thumbnail</th>
                                        <th>Name</th>
                                        <th>Theme</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($templates as $bg)

                                    <tr>
                                        <td><img style="width: 100px;" src="{!! asset($bg['thumbnail'])!!}"></td>
                                        <td>{!!$bg['name']!!}</td>

                                        <td>{!!$bg['config']['theme']!!}</td>
                                        <td>
                                            @if(have_premission(59))
                                            <a href="{{ url('/admin/templates/'.$bg['name'].'/edit')}}"><i class="fa fa-edit fa-fw"></i></a>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                    @if (count($templates) == 0)
                                    <tr><td colspan="4"><div class="no-record-found alert alert-warning">No Templates found!</div></td></tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection