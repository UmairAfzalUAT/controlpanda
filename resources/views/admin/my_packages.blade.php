@extends('admin.layouts.app')
@section('content')
<link href="{{asset('frontend/css/integration.css')}}" rel="stylesheet" media="screen">
<style type="text/css">
    .copyright { padding:0}
    .btn-flow { background:#F00;}
    .panda-flow-pricing .pricing-div ul {min-height: 200px;}
</style>
<div class="clear20"></div>
<section class="contentarea">
    <div class="container-fluid">
        <section id="pandaflowpricing" class="panda-flow-pricing text-center">
            <div class="container">

                @if(!is_object($dial_package) && !is_object($crm_package) && !is_object($flow_package) && !is_object($sms_package))
                <h2 align="center">You have not subscribe any package yet.</h2>
                @endif

                @if(is_object($dial_package))
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="pricing-div">
                        <h2><b>Panda Dial Package</b></h2>
                        <h2>@if($dial_package->price > 0) £ {!!$dial_package->price!!} @else {{'Free'}}  @endif</h2>
                        <p>Per Month/Per User</p>
                        <b>{!!$dial_package->segment_title!!}</b>
                        <ul>
                            @if($dial_package->instant_deployment_24h == 1) <li><i class="fa fa-fw fa-check"></i>&nbsp;Instant Deployment-24/7 Support</li> @endif
                            @if($dial_package->auto_attend == 1) <li><i class="fa fa-fw fa-check"></i>&nbsp;Auto Attend<br></li>@endif
                            @if($dial_package->call_queues == 1) <li><i class="fa fa-fw fa-check"></i>&nbsp;Call Queues<br></li>@endif
                            @if($dial_package->call_recording == 1)<li><i class="fa fa-fw fa-check"></i>&nbsp;Call Recording<br></li>@endif
                            @if($dial_package->fully_integrated_with_control_panda == 1)<li><i class="fa fa-fw fa-check"></i>&nbsp;Fully Integrated with Control Panda<br></li>@endif
                            @if($dial_package->virtual_business_number == 1)<li><i class="fa fa-fw fa-check"></i>&nbsp;Virtual Business number<br></li>@endif

                            @if($dial_package->pay_as_you_go_features == 1)<li><i class="fa fa-fw fa-check"></i>&nbsp;All pay as you go features +</li> @endif

                            @if($dial_package->per_user_for_one_country == 1)<li><i class="fa fa-fw fa-check"></i>&nbsp;2000 Minute free outbound call package per user for one country<br></li> @endif

                            @if($dial_package->per_user_to_listed_countries == 1)  <li><i class="fa fa-fw fa-check"></i>&nbsp;2000 Minute free outbound call package per user to all listed countries plus mobile phone numbers in these countries.<br></li> @endif

                            @if($dial_package->geographical_number == 1)  <li><i class="fa fa-fw fa-check"></i>&nbsp;Inclusive geographical number for each user<br></li> @endif

                        </ul>
                        <a href="javascript:void(0);" class="btn btn-flow">Renew Date: {{date('d-M-Y', strtotime($dial_package->renew_date))}}</a>

                        <a href="{{url('/admin/payment-history/panda-dials')}}" class="">Transactions History</a>
                    </div>
                </div>

                @endif 


                @if(is_object($flow_package))

                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="pricing-div">
                        <h2><b>Panda Flow Package</b></h2>
                        <h2>@if($flow_package->price > 0) £ {!!$flow_package->price!!} @else {{'Free'}}  @endif</h2>
                        <p>Per Month</p>
                        <!--<b>{!!$flow_package->page_views_per_m!!} page views p/m</b>-->


                        @if($flow_package->page_views_per_m >= 1) <b>{!!$flow_package->page_views_per_m!!} page views p/m</b> @elseif ($flow_package->page_views_per_m == -1) <b>Unlimited page views p/m</b>  @endif

                        <ul>

                            @if($flow_package->snap_shots >= 1) <li><i class="fa fa-fw fa-check"></i>&nbsp;{!!$flow_package->snap_shots!!} Snap shots</li> @elseif ($flow_package->snap_shots == -1) <li><i class="fa fa-fw fa-check"></i>&nbsp; Unlimited Snap shots</li> @endif

                            @if($flow_package->screen_recordings >= 1) <li><i class="fa fa-fw fa-check"></i>&nbsp;{!!$flow_package->screen_recordings!!} Screen recordings<br></li> @elseif ($flow_package->screen_recordings == -1)  <li><i class="fa fa-fw fa-check"></i>&nbsp; Unlimited Screen recordings<br></li> @endif


                            @if($flow_package->months_of_recording ==1)<li><i class="fa fa-fw fa-check"></i>&nbsp;{!!$flow_package->months_of_recording!!} Month of recording storage data<br></li> @elseif($flow_package->months_of_recording >1)<li><i class="fa fa-fw fa-check"></i>&nbsp;{!!$flow_package->months_of_recording!!} Months of recording storage data<br></li> @endif


                            @if($flow_package->split_tests == -1) <li><i class="fa fa-fw fa-check"></i>&nbsp;  Unlimited split tests <br></li>@elseif($flow_package->split_tests >= 1)<li><i class="fa fa-fw fa-check"></i>&nbsp; {!!$flow_package->split_tests!!} split tests <br></li>  @endif



                            @if($flow_package->panda_sites == -1)<li><i class="fa fa-fw fa-check"></i>&nbsp;  Unlimited panda sites <br></li> @elseif($flow_package->panda_sites >= 1)<li><i class="fa fa-fw fa-check"></i>&nbsp; {!!$flow_package->panda_sites!!} panda sites <br></li> @endif
                        </ul>
                        <a href="javascript:void(0);" class="btn btn-flow">Renew Date: {{date('d-M-Y', strtotime($flow_package->renew_date))}}</a>
                        <a href="{{url('/admin/payment-history/panda-flows')}}" class="">Transactions History</a>
                    </div>
                </div>

                @endif



                @if(is_object($sms_package))

                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="pricing-div">
                        <h2><b>Panda Sms Package</b></h2>
                        <h2>@if($sms_package->price > 0) £ {!!$sms_package->price!!} @else {{'Free'}}  @endif</h2>
                        <p>Per Month</p>
                        @if($sms_package->messages_per_m >= 1)<b>{!!$sms_package->messages_per_m!!} SMS messages p/m</b> @elseif ($sms_package->messages_per_m == -1)<b> Unlimited SMS messages p/m</b>@endif
                        <ul>
                            @if($sms_package->messages_per_m >= 1)<li><i class="fa fa-fw fa-check"></i>&nbsp;{!!$sms_package->messages_per_m!!} SMS messages</li> @elseif ($sms_package->messages_per_m == -1) <li><i class="fa fa-fw fa-check"></i>&nbsp; Unlimited SMS messages</li> @endif

                            @if($sms_package->custom_link_integration == 1) <li><i class="fa fa-fw fa-check"></i>&nbsp;Custom link integration<br></li> @endif
                            @if($sms_package->custom_sms_reporting == 1)<li><i class="fa fa-fw fa-check"></i>&nbsp;Custom SMS reporting<br></li>@endif
                            @if($sms_package->individual_business_number == 1)<li><i class="fa fa-fw fa-check"></i>&nbsp;Individual business number<br></li>@endif

                            @if($sms_package->panda_sites == -1)  <li><i class="fa fa-fw fa-check"></i>&nbsp;  Unlimited panda sites<br></li> @elseif($sms_package->panda_sites >= 1)   <li><i class="fa fa-fw fa-check"></i>&nbsp;  {!!$sms_package->panda_sites!!} panda sites<br></li> @endif
                        </ul>
                        <a href="javascript:void(0);" class="btn btn-flow">Renew Date: {{date('d-M-Y', strtotime($sms_package->renew_date))}}</a>
                        <a href="{{url('/admin/payment-history/panda-sms')}}" class="">Transactions History</a>

                    </div>
                </div>
                @endif 


                @if(is_object($crm_package))

                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="pricing-div">
                        <h2><b>Panda Crm Package</b></h2>
                        <h2>@if($crm_package->price > 0) £ {!!$crm_package->price!!} @else {{'Free'}}  @endif</h2>
                        <p>Per Month</p>
                        <b>{!!$crm_package->segment_title!!}</b>
                        <ul>
                            @if($crm_package->instant_deployment_24h == 1) <li><i class="fa fa-fw fa-check"></i>&nbsp;Instant Deployment-24/7 Support</li> @endif
                            @if($crm_package->auto_attend == 1) <li><i class="fa fa-fw fa-check"></i>&nbsp;Auto Attend<br></li>@endif
                            @if($crm_package->call_queues == 1) <li><i class="fa fa-fw fa-check"></i>&nbsp;Call Queues<br></li>@endif
                            @if($crm_package->call_recording == 1)<li><i class="fa fa-fw fa-check"></i>&nbsp;Call Recording<br></li>@endif
                            @if($crm_package->fully_integrated_with_control_panda == 1)<li><i class="fa fa-fw fa-check"></i>&nbsp;Fully Integrated with Control Panda<br></li>@endif
                            @if($crm_package->virtual_business_number == 1)<li><i class="fa fa-fw fa-check"></i>&nbsp;Virtual Business number<br></li>@endif

                            @if($crm_package->pay_as_you_go_features == 1)<li><i class="fa fa-fw fa-check"></i>&nbsp;All pay as you go features +</li> @endif

                            @if($crm_package->per_user_for_one_country == 1)<li><i class="fa fa-fw fa-check"></i>&nbsp;2000 Minute free outbound call package per user for one country<br></li> @endif

                            @if($crm_package->per_user_to_listed_countries == 1)  <li><i class="fa fa-fw fa-check"></i>&nbsp;2000 Minute free outbound call package per user to all listed countries plus mobile phone numbers in these countries.<br></li> @endif

                            @if($crm_package->geographical_number == 1)  <li><i class="fa fa-fw fa-check"></i>&nbsp;Inclusive geographical number for each user<br></li> @endif


                        </ul>
                        <a href="javascript:void(0);" class="btn btn-flow">Renew Date: {{date('d-M-Y', strtotime($crm_package->renew_date))}}</a>
                        <a href="{{url('/admin/payment-history/panda-crms')}}" class="">Transactions History</a>
                    </div>
                </div>

                @endif    

            </div>
        </section>





    </div>




</section>





<script type="text/javascript">
    $(function () {

        $('body').addClass('dashboard-container');

    });
</script>

@endsection