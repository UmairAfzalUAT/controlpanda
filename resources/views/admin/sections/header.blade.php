@php
$segment2 = Request::segment(2);
$segment3 = Request::segment(3);
@endphp
<?php  //echo '<pre>';  print_r(Session::get('session_last_project')); ?>
<header>
    <div class="header-left left-panel-width-control">
        <div class="logo">
            <a href="{{url('admin/dashboard')}}" title=""><img src="{{asset('assets/images/logo.png')}}" alt="" /></a>
            <svg width="466" height="603" viewbox="0 0 100 100" preserveAspectRatio="none">
            <path d="M0,0 L100,0 C15,50 50,100 0,100z"/>
            </svg>
        </div>
        <!-- Logo -->
        <div class="header-dropdown">
            <div class="dropdown">
                <span class="selected-value dropbtn" id="dropdownMenu2" data-toggle="dropdown">My Sites <i class="fas fa-angle-down"></i> </span>
                <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                    <div class="top-head">My Site 
                        <a href="{{asset('admin/panda-pages/create')}}"><button type="button" class="btn btn-danger">Create a new site</button></a>
                    </div>
                    <div id="custom-search-input" style="display: none; ">
                        <div class="input-group col-md-12" >
                            <input type="text" class="form-control" placeholder="Search">
                            <span class="input-group-btn">
                                <button class="btn btn-danger" type="button">
                                    <span class=" fas fa-search"></span>
                                </button>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="selectsite">
                            @foreach(dashboard::getProjects() as $pro)
                            <article style="margin: 10px 10px;">
                                <div class="left-box"><img class="img-fluid" src="{!! asset('storage/projects/'.Auth::user()->id.'/'.$pro->uuid.'/thumbnail.png') !!}"></div>
                                <div class="right-box">
                                    <h1>{!!$pro->template!!} &nbsp;<span>| {!!$pro->t_cat_name!!}</span></h1>
                                    <p>last updated <span class="timeago" title="Fri, Jan 26 2018">{{{ $date = time_ago($pro->updated_at) }}}</span></p>
                                    <a href="{!!url('sites/'.$pro->name)!!}">
                                        <button type="button" class="btn btn-primary">Select Site</button>
                                    </a>
                                    <a href="{!!url('admin/panda-pages/'.$pro->id.'/edit')!!}">
                                        <button type="button" class="btn btn-info">Edit Site</button>
                                    </a>
                                </div>
                            </article>
                            @endforeach
                            
                            <div class="site-button">
                                <a href="{{asset('admin/panda-pages/create')}}">
                                    <button type="button" class="btn btn-danger"><i class="fas fa-plus-circle"></i> Create a new site</button></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header Dropdown -->
    </div>
    <div class="header-right">
        <nav class="navbar fixed-top fixed-top-custom-icon navbar-expand-sm navbar-light">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse">☰</button> 
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav ml-auto header-icons">
                    @if(have_premission(1))
                    <a class="icon tooltipp" href="{{url('/admin/panda-pages')}}" title="">
                        <span class="tooltiptext">Websites</span><img src="{{asset('assets/images/icon5.svg')}}" class="svg5"/></a>
                    @endif    
                    <a class="icon tooltipp" href="{{url('/admin/contacts/emails/'.@Session::get('session_last_project')[0]->id.'/lists')}}" title="">
                        <span class="tooltiptext">Email</span><img src="{{asset('assets/images/icon4.svg')}}" class="svg4" /></a>
                    <a class="icon tooltipp" href="{{url('/admin/external-integrations')}}" title="">
                        <span class="tooltiptext">Integration</span><img src="{{asset('assets/images/icon3.svg')}}" class="svg3" /></a>
                    <a class="icon tooltipp" href="index.html" title="">
                        <span class="tooltiptext">Templates</span><img src="{{asset('assets/images/icon2.svg')}}" class="svg2" /></a>
                    <a class="icon tooltipp" href="{{url('/support')}}" title="">
                        <span class="tooltiptext">Spport</span><img src="{{asset('assets/images/icon1.svg')}}" class="svg1" /> </a>
                    <a class="fancy-btn fancy-btn-mob" href="{{url('admin/upgrade-account')}}" title="">
                        <svg class="fancy-btn-left" width="466" height="603" viewbox="0 0 100 100" preserveAspectRatio="none">
                        <path d="M0,0 L100,0 C15,50 50,100 0,100z"/>
                        </svg>
                        Upgrade
                        <svg class="fancy-btn-right" width="466" height="603" viewbox="0 0 100 100" preserveAspectRatio="none">
                        <path d="M0,0 L100,0 C15,50 50,100 0,100z"/>
                        </svg>
                    </a>
                    <!-- Fancy Button -->								
                </ul>
            </div>
        </nav>

    </div>
</header>
<!-- Header -->