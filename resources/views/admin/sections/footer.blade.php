<div class="clear80"></div>
<section class="copyright">
    <div class="container text-center">
        Copyright © 2018 All rights reserved. <span>Creative work by ArhamSoft</span>
    </div>
</section>


<script>

    $(".nvccb, #cc_number").keypress(function (evt) {
        // between 0 and 9   
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;

    });


    $(document).ready(function () {
        $('#step-1').addClass("show");
        $('#step-2').removeClass("show");
        $('#step-3').removeClass("show");
        $('#step-4').removeClass("show");

    });

    $(document).on('click', '#gethelpmodal', function () {
        $('#step-1').addClass("show");
        $('#step-2').removeClass("show");
        $('#step-3').removeClass("show");
        $('#step-4').removeClass("show");
    });

    $(document).on('click', '.notsolvedbtn', function () {
        $('#stepwizard-3').removeClass("btn btn-circle light-blue");
        $('#stepwizard-3').addClass("btn btn-default btn-circle");
        $('#stepwizard-3').next("p").remove();
        $('#appendtodiv').append("<p>Finish</p>");
        $('#step-1').removeClass("show");
        $('#step-2').removeClass("show");
        $('#step-4').removeClass("show");
        $('#step-3').addClass("show");

    });



    $(document).on('click', '.solvedbtn', function () {

        $('#closemodaltab').remove();
        $('#innerstep-4').append('<button type="button" class="btn btn-primary nextBtn btn-lg btn-block" id="closemodaltab" data-dismiss="modal">Close Tab</button>');
        $('#stepwizard-3').removeClass("btn btn-default btn-circle");
        $('#stepwizard-3').addClass("btn btn-circle light-blue");
        $('#stepwizard-3').next("p").remove();
        $('#appendtodiv').append("<p>Complete</p>");
        $('#step-1').removeClass("show");
        $('#step-2').removeClass("show");
        $('#step-3').removeClass("show");
        $('#step-4').addClass("show");
        var timeleft = 10;
        var downloadTimer = setInterval(function () {
            timeleft--;
            document.getElementById("countdowntimer").textContent = timeleft;
            if (timeleft <= 0)
                clearInterval(downloadTimer);

        }, 1000);

        var timeleft1 = 10000;
        $(function () {  // document.ready function...
            setTimeout(function () {
                $('#closemodaltab').click();
            }, timeleft1);
        });

        //$('#closemodaltab').click();

    });

    $(document).on('click', '#backtoanswers', function () {
        $('#closemodaltab').remove();
        $('#stepwizard-3').removeClass("btn btn-circle light-blue");
        $('#stepwizard-3').addClass("btn btn-default btn-circle");
        $('#stepwizard-3').next("p").remove();
        $('#appendtodiv').append("<p>Finish</p>");
        $('#step-1').removeClass("show");
        $('#step-3').removeClass("show");
        $('#step-4').removeClass("show");
        $('#step-2').addClass("show");
    });



    function checkSolutions() {
        $('#step-svg').addClass("show");
        $("#step-1").removeClass("show");
        URL = "{{URL::to('supportreviews')}}";
        question = $("#question").val();
        toke = $("input[name=_token]").val();
        $.ajax({
            type: "POST",
            url: URL,
            data: {
                'q': question,
                '_token': toke
            },
            success: function (response) {

                $("#step-1").removeClass("show");
                $("#step-2").addClass("show");

                if (response === '') {
                    $('#step-svg').removeClass("show");
                    $('.solvedbtn').click();
                }
                if (response != '') {
                    $('#stepwizard-2').removeClass("btn btn-default btn-circle");
                    $('#stepwizard-2').addClass("btn btn-circle light-blue");
                    $('#step-svg').removeClass("show");
                    $("#innerstep-2").html(response);
                }

            },
            error: function (ajax_alert) {

                alert(ajax_alert);

            }

        });
    }



    function submitticket() {
        $('#step-svg').addClass("show");
        //$("#step-1").removeClass("show");
        URL = "{{URL::to('admin/tickets/createticket')}}";
        question = $("#question").val();
        toke = $("input[name=_token]").val();
        $.ajax({
            type: "POST",
            url: URL,
            data: {
                'q': question,
                '_token': toke
            },
            success: function (response) {
                response = JSON.parse(response);
                $("#step-1").removeClass("show");
                $("#step-2").removeClass("show");
                $("#step-3").removeClass("show");
                $("#step-4").addClass("show");


                if (response === 0) {
                    $("#ques-div").hide();
                    $("#solution-div").show();
                    $('#question').val(question);
                }
                if (response != 0) {
                    $('#stepwizard-3').removeClass("btn btn-default btn-circle");
                    $('#stepwizard-3').addClass("btn btn-circle light-blue");
                    $('#step-svg').removeClass("show");
                    $("#descriptions").html("Your Ticket has been submitted. We'll get back to you shortly");
                }

                var timeleft = 10;
                var downloadTimer = setInterval(function () {
                    timeleft--;
                    document.getElementById("countdowntimer").textContent = timeleft;
                    if (timeleft <= 0)
                        clearInterval(downloadTimer);

                }, 1000);

                var timeleft1 = 10000;
                $(function () {  // document.ready function...
                    setTimeout(function () {
                        $('#closemodaltab').click();
                    }, timeleft1);
                });



            },
            error: function (ajax_alert) {

                alert(ajax_alert);

            }

        });
    }



    //------------------ Balance Page Functions -------------------------//
    function validatePaybyCredit(creditCardForm)
    {
        alert('into the form');
        $('#stripe-form').validate({
            ignore: "input[type='text']:hidden",
            onfocusout: false,
            rules: {
                cc_name: {required: true},
                cc_number: {required: true, number: true, maxlength: 16, minlength: 16},
                cc_exp_month: {required: true},
                cc_exp_year: {required: true},
                cc_cvv: {required: true, maxlength: 4},
            },
            messages: {
                cc_name: {required: "Name on card is required"},
                cc_number: {required: "Valid Card Number is required", number: "Provide a valid card Number",
                    maxlength: "Length should be 16 characters", minlength: "Length should be 16 characters"},
                cc_exp_month: {required: "Expire Month is required"},
                cc_exp_year: {required: "Expire Year is required"},
                cc_cvv: {required: "CVV code is required", maxlength: "CVV code should be 4 characters"},
            },

            highlight: function (element) {
                $(element).parent().css("color", "red");
            },
            errorPlacement: function (error, element) {},
            invalidHandler: function (form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    $('#stripformerrorcontainer').fadeIn('slow');
                    $('#stripformerrors').html("<li class='alert-box-li' style='color:red; font-size:20px;'>" + validator.errorList[0].message + "</li>");
                    validator.errorList[0].element.focus(); //Set Focus
                }
            }

        });
    }

            




</script>


