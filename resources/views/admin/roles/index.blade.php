@extends('admin.layouts.app')

@section('content')

<div class="breadcrumbs contentarea">
    <div class="container-fluid">
    <ul>
        <li>
            <a href="{{url('/admin/dashboard')}}">Dashboard</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a>Roles</a>
        </li>
    </ul>
    <div class="close-bread">
        <a href="#"><i class="icon-remove"></i></a>
    </div>
</div>
</div>

<section class="contentarea">
    <div class="container-fluid">
        <div class="page-header"><h1>Roles <span class="badge">{{count($roles)}}</span>
                @if(have_premission(61))
                <a href="{{url('/admin/roles/create')}}" class="btn btn-info pull-right">Add New Role</a>
                @endif
            </h1></div>
        <div class="box">
            <div class="box-content">
                <div class="table-responsive">
                    <table class="table table-hover no-margin table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Last Modified</th>
                                <th>Status</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($roles as $role)
                            <tr>
                                <td>{!!$role->title!!}</td>
                                <td>
                                    {{date('M d, Y', strtotime($role->updated_at))}}
                                </td>
                                <td>@if($role->is_active == 1) <label class="label label-success">Active</label> @else <label class="label label-danger">Inactive</label> @endif</td>
                                <td class="text-center">@if(have_premission(58))<a href="{{url('/admin/roles/'.$role->id.'/edit')}}"><i class="fa fa-edit"></i></a>@endif
                               
                               
                                @if(have_premission(66))
                                {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['admin/roles/'.$role->id],
                                'style' => 'display:inline'
                                ]) !!}
                                {!! Form::button('<i class="fa fa-trash fa-fw" title="Delete Role"></i>', ['class' => 'delete-form-btn']) !!}
                                {!! Form::submit('Delete', ['class' => 'hidden deleteSubmit']) !!}
                                {!! Form::close() !!}
                                @endif
                               
                                
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection