@extends('admin.layouts.app')
@section('content')
<script src="{{ asset('js/plugins/validation/jquery.validate.min.js')}}"></script>
<script src="{{ asset('js/plugins/validation/additional-methods.min.js')}}"></script>
<div class="breadcrumbs contentarea">
    <div class="container-fluid">
    <ul>
        <li>
            <a href="{{url('/admin/dashboard')}}">Dashboard</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="{{url('/admin/roles')}}">Roles</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a>{!!$action!!} Role</a>
        </li>
    </ul>
</div>
</div>

<section class="contentarea">
    <div class="container-fluid">
        <div class="page-header"><h1>{!!$action!!} Role</h1></div>
        <div class="box">
            <div class="box-content">
                <form id="role-form" class="form-horizontal form-validate" action="{{url('/admin/roles')}}" method="POST">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Title</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" name="title" value="{!!@$role['title']!!}" aria-required="true" data-rule-required="true"/>
                        </div>
                    </div>
                    <input type="hidden" name="action" value="{!!$action!!}">
                    <input type="hidden" name="id" value="{!!@$role['id']!!}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="is_active">Status</label>
                        <div class="col-sm-4">
                            <input type="radio" name="is_active" value="1" @if(!isset($role['is_active']) || $role['is_active'] == 1) checked @endif /> Active &nbsp;&nbsp; <input type="radio" name="is_active" value="0"  @if(isset($role['is_active']) && $role['is_active'] == 0) checked @endif/> Inactive
                        </div>
                    </div>
                    <h4 class="sub_heading">Permissions</h4>
                    <?php
                    $previous_module = '';
                    $subcontainer = '';
                    ?>
                    @foreach($rights as $right)
                    <?php
                    $count = array();
                    if ($right->module_name != $previous_module) {
                        $all_checked = '';

                        if ($previous_module != '') {
                            echo '</div></div>';
                        }
                        ?>
                        <div class="form-group permissions-checkboxes">
                            <label class="col-sm-2 control-label">{!!$right->module_name!!}<br>
                                <input class="check_all" <?php echo $all_checked ?> type="checkbox"/> <small>Check All</small>
                            </label>
                            <div class="col-sm-9">
                                <?php
                            }
                            ?>

                            <div class="col-sm-4"><input class="permission-checkbox" type="checkbox" name="right_id[]" value="{!!$right->id!!}" @if(isset($right->is_selected) && $right->is_selected !='') checked @endif/> {!!$right->right_name!!}</div>
                            <?php
                            $previous_module = $right->module_name;
                            ?>

                            @endforeach
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="col-sm-3 control-label"></div>
                        <a href="{{url('/admin/roles')}}" class="btn btn-default">Cancel</a>
                        <button type="submit" class="btn btn-success">Continue</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<script>
    $(".check_all").change(function () {
        var element = $(this);
        element.parent('.col-sm-2').next('.col-sm-9').find('input[type="checkbox"]').prop('checked', element.is(":checked"));
    });
</script>
@endsection
