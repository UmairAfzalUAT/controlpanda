@extends('admin.layouts.app')

@section('content')
<!-- Validation -->
<script src="{{ asset('js/plugins/validation/jquery.validate.min.js')}}"></script>
<link rel="stylesheet" href="{{ asset('css/plugins/select2/select2.css') }}">
<script src="{{ asset('js/plugins/select2/select2.min.js')}}"></script>

<div class="breadcrumbs contentarea">
    <div class="container-fluid">
        <ul>
            <li>
                <a href="{{url('/admin/dashboard')}}">Dashboard</a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a>My Profile</a>
            </li>

        </ul>
        <div class="close-bread">
            <a href="#"><i class="icon-remove"></i></a>
        </div>
    </div>
</div>
<section class="contentarea">
    <div class="container-fluid">
        <div class="page-header">
            <h1>
                <i class="icon-user"></i>
                Edit user profile
            </h1>
        </div>
        <div class="box-content nopadding">
            <div class="form_wrap" style="max-width: 670px;">
                <ul class="tabs tabs-inline tabs-top">
                    <li class="@if((isset($_GET['action']) && $_GET['action'] == 'profile') || (!isset($_GET['action']))) active @endif">
                        <a href="#profile" data-toggle='tab'><i class="icon-user"></i> Profile</a>
                    </li>
                    <li class="@if(isset($_GET['action']) && $_GET['action'] == 'password') active @endif">
                        <a href="#passwords-tab" data-toggle='tab'><i class="icon-lock"></i> Password</a>
                    </li>
                    <!--media link-->
                    <li class="@if(isset($_GET['action']) && $_GET['action'] == 'media') active @endif">
                        <a href="#media" data-toggle='tab'><i class="icon-lock"></i>Social Media Links</a>
                    </li>
                    <li class="@if(isset($_GET['action']) && $_GET['action'] == 'media') active @endif">
                        <a href="#fbmessenger" data-toggle='tab'><i class="icon-lock"></i>Facebook Messenger Setup</a>
                    </li>
                    
                </ul>
                <div class="tab-content padding tab-content-inline tab-content-bottom">
                    <div class="tab-pane @if(isset($_GET['action']) && $_GET['action'] == 'profile'  || (!isset($_GET['action']))) active @endif" id="profile">
                        <form id="profile-form" method="POST" action="{{url("/admin/update-profile")}}" class="form-horizontal form-validate" novalidate="novalidate" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="col-sm-offset-4 col-sm-4">
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <div class="fileupload-new thumbnail">
                                        <img class="image-display" src="{{ checkImage('users/'.$user['profile_image']) }}" />
                                    </div>
                                    <div>
                                        <input accept="image/*" class="image-input" type="file" name='profile_image'/>
                                    </div>
                                </div>
                            </div> 
                            <div class="clear5"></div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="first_name" class="col-sm-4 control-label">First Name</label>
                                        <div class="col-sm-8">
                                            <input type="text" id="first_name" name="first_name" class='form-control' placeholder="Enter First Name" data-rule-required="true" aria-required="true" value="{!!@$user['first_name']!!}"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="last_name" class="col-sm-4 control-label">Last Name</label>
                                        <div class="col-sm-8">
                                            <input type="text" id="last_name" name="last_name" class='form-control' placeholder="Enter First Name" data-rule-required="true" aria-required="true" value="{!!@$user['last_name']!!}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="email" class="col-sm-4 control-label">Email</label>
                                        <div class="col-sm-8">
                                            <input type="text" readonly class='form-control' value="{!!@$user['email']!!}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="phone" class="col-sm-4 control-label">Phone</label>
                                        <div class="col-sm-8">
                                            <input type="text" id="phone" name="phone" class='form-control' placeholder="Enter Phone" data-rule-required="true" data-rule-minlength="10" aria-required="true" value="{!!@$user['phone']!!}"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="address" class="col-sm-4 control-label">Address</label>
                                        <div class="col-sm-8">
                                            <input type="text" id="address" name="address" class='form-control' placeholder="Enter Address" data-rule-required="true" data-rule-minlength="5" aria-required="true" value="{!!@$user['address']!!}"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="zipcode" class="col-sm-4 control-label">Zip</label>
                                        <div class="col-sm-8">
                                            <input type="text" id="address" name="zipcode" class='form-control' placeholder="Enter Zip" data-rule-required="true" data-rule-minlength="5" aria-required="true" value="{!!@$user['zipcode']!!}"/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="country" class="col-sm-4 control-label">Country</label>
                                        <div class="col-sm-8">
                                            <select name="country" id="country" class='select2-me form-control'>
                                                @foreach($countries as $country)
                                                <option @if($country->ccode == $user['country']) selected @endif value="{!!$country->ccode!!}">{!!$country->country!!}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="city" class="col-sm-4 control-label">City</label>
                                        <div class="col-sm-8">
                                            <input type="text" id="city" name="city" class='form-control' placeholder="Enter City" data-rule-required="true" aria-required="true" value="{!!@$user['city']!!}">
                                        </div>
                                    </div>
                                    <div class="form-actions text-right" style="padding: 0 0 20px;">
                                        <input type="submit" class='btn btn-primary' value="Save">
                                        <input type="reset" class='btn' value="Discard changes">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane @if(isset($_GET['action']) && $_GET['action'] == 'password') active @endif" id="passwords-tab">
                        <form id="password-form" method="POST" action="{{url("/admin/update-password")}}" class="form-horizontal form-validate" novalidate="novalidate">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="current_password" class="col-sm-4 control-label">Current Password</label>
                                <div class="col-sm-8">
                                    <input type="password" id="current_password" name="current_password" class='form-control' placeholder="Enter current password" data-rule-minlength="6" data-rule-required="true" aria-required="true" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="new_password" class="col-sm-4 control-label">New Password</label>
                                <div class="col-sm-8">
                                    <input type="password" id="new_password" name="new_password" class='form-control' placeholder="Enter new password" data-rule-required="true" data-rule-minlength="6" aria-required="true" value=""/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="confirm_password" class="col-sm-4 control-label">Confirm Password</label>
                                <div class="col-sm-8">
                                    <input type="password" id="confirm_password" name="confirm_password" class='form-control' placeholder="Retype new password" data-rule-equalto="#new_password" data-rule-minlength="6" data-rule-required="true" aria-required="true" value=""/>
                                </div>
                            </div>

                            <div class="form-actions text-right" style="padding: 0 0 20px;">
                                <input type="submit" class='btn btn-primary' value="Save">
                                <input type="reset" class='btn' value="Discard changes">
                            </div>
                        </form>
                    </div>
                    <!--new tabb-->
                    <div class="tab-pane @if(isset($_GET['action']) && $_GET['action'] == 'media') active @endif" id="media">
                        <form id="socialmedia" method="POST" action="{{url("/admin/update-profile")}}" class="form-horizontal form-validate" novalidate="novalidate">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="facebook" class="col-sm-4 control-label">Facebook</label>
                                <div class="col-sm-8">
                                    <input type="text" data-rule-url=”true” name="facebook" class='form-control' value="{!!@$user['facebook']!!}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="twitter" class="col-sm-4 control-label">Twitter</label>
                                <div class="col-sm-8">
                                    <input type="text" data-rule-url=”true” name="twitter" class='form-control' value="{!!@$user['twitter']!!}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="linkedin" class="col-sm-4 control-label">Linkedin</label>
                                <div class="col-sm-8">
                                    <input type="text" data-rule-url=”true” name="linkedin" class='form-control' value="{!!@$user['linkedin']!!}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="googleplus" class="col-sm-4 control-label">Google Plus</label>
                                <div class="col-sm-8">
                                    <input type="text" data-rule-url=”true” name="googleplus" class='form-control' value="{!!@$user['googleplus']!!}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="youtube" class="col-sm-4 control-label">You Tube</label>
                                <div class="col-sm-8">
                                    <input type="text" name="youtube" data-rule-url=”true” class='form-control' value="{!!@$user['youtube']!!}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="pinterest" class="col-sm-4 control-label">Pinterest</label>
                                <div class="col-sm-8">
                                    <input type="text" name="pinterest" data-rule-url=”true” class='form-control' value="{!!@$user['pinterest']!!}">
                                </div>
                            </div>



                            <div class="form-actions text-right" style="padding: 0 0 20px;">
                                <input type="submit" class='btn btn-primary' value="Save">
                                <input type="reset" class='btn' value="Discard changes">
                            </div>
                        </form>
                    </div>
                     <div class="tab-pane @if(isset($_GET['action']) && $_GET['action'] == 'fbmessenger') active @endif" id="fbmessenger">
                        <form id="fbmessenger" method="POST" action="{{url("/admin/update-profile")}}" class="form-horizontal form-validate" novalidate="novalidate">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Facebook page ID</label>
                                <div class="col-sm-8">
                                    <input type="text" data-rule-url="true" name="facebook_page_id" class='form-control' value="{!!@$user['facebook_page_id']!!}"/>
                                </div>
                            </div>
                            <div class="alert alert-info">Please provide facebook page id if you want to enable facebook messenger contact button on your all newly created web sites</div>
                            
                            <div class="form-actions text-right" style="padding: 0 0 20px;">
                                <input type="submit" class='btn btn-primary' value="Save">
                                <input type="reset" class='btn' value="Discard changes">
                            </div>
                        </form>
                    </div>
                </div>
            </div>    
        </div>
    </div>
</section>
<script>
$(document).on('click', '#generate_key', function () {
    var text = "";
    var possible = "ABCDEFGHIJKL-MNOPQRSTUVWXYZ-abcdefghij-klmnopqrstuv-wxyz0123456789";
    for (var i = 0; i < 31; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    $("#unique_code").val(text);
});
</script>
<script>
    $('#process-auth-key').click(function () {
        var submitBtn = $(this).next('.processSubmit');
        swal({
            title: "Are you sure?",
            text: "It will disabled the all existing intigrations!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: false,
            closeOnCancel: true
        },
                function (isConfirm) {
                    if (isConfirm) {
                        submitBtn.click();
                    }
                });
    });
</script>
@endsection

