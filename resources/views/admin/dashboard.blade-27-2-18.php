@extends('admin.layouts.app')
@section('content')
<link rel="stylesheet" href="{{asset('frontend/css/daterangepicker.css')}}">
<link rel="stylesheet" href="{{asset('frontend/css/select2.min.css')}}">
<div class="clear20"></div>
<section class="contentarea">
    <div class="container-fluid">
        <div class="col-md-3 col-sm-5 col-xs-12">
            <div class="cf-section">
                <div class="cf-section-header">
                    <i class="fa fa-user"></i>
                    Recent Activity
                </div>
                <div class="cf-section-body">
                    <div class="activity-stream-outer">
                        <div class="activity-stream">
                            <div class="stream" id="1516260053221">
                                <div class="stream-badge"><i class="fa-gg-circle"></i></div>
                                <div class="stream-panel">
                                    <div class="stream-info">
                                        <a href="#">
                                            <span class="name">Someone</span>
                                            <time class="date timeago" datetime="2018-01-18T07:20:53Z" title="Thu, Jan 18 at  7:20AM UTC">4 days ago</time>
                                        </a>
                                    </div>Lorem ipsum dolor sit amet <a class="target" href="#">CONSECTETUR adipiscing elit</a>
                                </div>
                            </div>
                            <div class="stream" id="1516260041614">
                                <div class="stream-badge">
                                    <i class="fa-gg-circle"></i>
                                </div>
                                <div class="stream-panel">
                                    <div class="stream-info">
                                        <a href="#">
                                            <span class="name">Someone</span>
                                            <time class="date timeago" datetime="2018-01-18T07:20:41Z" title="Thu, Jan 18 at  7:20AM UTC">4 days ago</time>
                                        </a>
                                    </div>Lorem ipsum dolor sit amet <a class="target" href="#">CONSECTETUR adipiscing elit</a>
                                </div>
                            </div>
                            <div class="stream" id="1516260032919">
                                <div class="stream-badge">
                                    <i class="fa fa-user-plus bg-warning"></i>
                                </div>
                                <div class="stream-panel">
                                    <div class="stream-info">
                                        <a href="#">
                                            <span class="name">Someone</span>
                                            <time class="date timeago" datetime="2018-01-18T07:20:32Z" title="Thu, Jan 18 at  7:20AM UTC">4 days ago</time>
                                        </a>
                                    </div>Lorem ipsum dolor sit amet <a class="target" href="#">CONSECTETUR adipiscing elit</a>
                                </div>
                            </div>
                            <div class="stream" id="1516259925261">
                                <div class="stream-badge">
                                    <i class="fa-gg-circle"></i>
                                </div>
                                <div class="stream-panel">
                                    <div class="stream-info">
                                        <a href="/contact_profiles/237584334">
                                            <span class="name">Someone</span>
                                            <time class="date timeago" datetime="2018-01-18T07:18:45Z" title="Thu, Jan 18 at  7:18AM UTC">4 days ago</time>
                                        </a>
                                    </div>Lorem ipsum dolor sit amet <a class="target" href="#">CONSECTETUR adipiscing elit</a>
                                </div>
                            </div>
                            <div class="stream" id="1516259292656">
                                <div class="stream-badge">
                                    <i class="fa fa-user-plus bg-warning"></i>
                                </div>
                                <div class="stream-panel">
                                    <div class="stream-info">
                                        <a href="/contact_profiles/237584334">
                                            <span class="name">Someone</span>
                                            <time class="date timeago" datetime="2018-01-18T07:08:12Z" title="Thu, Jan 18 at  7:08AM UTC">4 days ago</time>
                                        </a>
                                    </div>Lorem ipsum dolor sit amet <a class="target" href="#">CONSECTETUR adipiscing elit</a>
                                </div>
                            </div>
                            <div class="stream" id="1514891655504">
                                <div class="stream-badge">
                                    <i class="fa-gg-circle"></i>
                                </div>
                                <div class="stream-panel">
                                    <div class="stream-info">
                                        <a href="/contact_profiles/233243346">
                                            <span class="name">jason@comlinksmb.com</span>
                                            <time class="date timeago" datetime="2018-01-02T11:14:15Z" title="Tue, Jan  2 at 11:14AM UTC">20 days ago</time>
                                        </a>
                                    </div>Lorem ipsum dolor sit amet <a class="target" href="#">CONSECTETUR adipiscing elit</a>
                                </div>
                            </div>
                            <div class="stream" id="1514891645146">
                                <div class="stream-badge">
                                    <i class="fa-gg-circle"></i>
                                </div>
                                <div class="stream-panel">
                                    <div class="stream-info">
                                        <a href="/contact_profiles/233243346">
                                            <span class="name">jason@comlinksmb.com</span>
                                            <time class="date timeago" datetime="2018-01-02T11:14:04Z" title="Tue, Jan  2 at 11:14AM UTC">20 days ago</time>
                                        </a>
                                    </div>Lorem ipsum dolor sit amet <a class="target" href="#">CONSECTETUR adipiscing elit</a>
                                </div>
                            </div>
                            <div class="stream" id="1514891537925">
                                <div class="stream-badge">
                                    <i class="fa fa-user-plus bg-warning"></i>
                                </div>
                                <div class="stream-panel">
                                    <div class="stream-info">
                                        <a href="/contact_profiles/233243346">
                                            <span class="name">jason@comlinksmb.com</span>
                                            <time class="date timeago" datetime="2018-01-02T11:12:17Z" title="Tue, Jan  2 at 11:12AM UTC">20 days ago</time>
                                        </a>
                                    </div>Lorem ipsum dolor sit amet <a class="target" href="#">CONSECTETUR adipiscing elit</a>
                                </div>
                            </div>
                            <div class="cf-cta-small">
                                <h3>No activity yet</h3>
                                <div class="details">
                                    Lorem Ipsum is simply dummy text 
                                    <br>
                                    of the printing and typesetting industry.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9 col-sm-7 col-xs-12">
            <div class="form-group">
                <select class="select-multiple-basic" multiple="multiple" style="width: 100%;">
                    <option>Select1</option>
                    <option>Select2</option>
                    <option>Select3</option>
                    <option>Select4</option>
                    <option>Select5</option>
                    <option>Select6</option>
                    <option>Select7</option>
                    <option>Select8</option>
                </select>
            </div>
            <div class="dashboard-div">
                <div class="head-div head_div_2">
                    <h1><i class="fa fa-dashboard" aria-hidden="true"></i> Your Controlpanda Dashboard</h1>
                    <div class="date-div">
                        <form id="reservationForm" name="reservationform" class="reservation-form">
                            <div class="form-group">
                                <div id="reportrange" class="pull-right">
                                    <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                    <span></span>
                                  </div>
                                </div>
                                <div class="clearfix"></div>
<!--                                <div class="input-group-icon"><i class="fa fa-calander"></i></div>-->
                            </div><!-- end form-group -->
                        </form>
                    </div>
                    <div class="clearfix"></div>                            
                </div>                       
                <div class="dashboard-div-inner">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#visits">Site Visits</a></li>
                        <li><a data-toggle="tab" href="#leads">Leads</a></li>
                        <li><a data-toggle="tab" href="#leadrate">Lead Conversion Rate</a></li>
                        <li><a data-toggle="tab" href="#sales">Sales</a></li>
                        <li><a data-toggle="tab" href="#revenue">Sale Revenue</a></li>
                        <li><a data-toggle="tab" href="#conrates">Sale Conversion Rate</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="visits" class="tab-pane fade in active">

                            <div id="container"></div>

                        </div>
                        <div id="leads" class="tab-pane fade">
                            <h3>Menu 1</h3>
                            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        </div>
                        <div id="leadrate" class="tab-pane fade">
                            <h3>Menu 2</h3>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                        </div>
                        <div id="sales" class="tab-pane fade">
                            <h3>Menu 3</h3>
                            <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                        </div>
                        <div id="revenue" class="tab-pane fade">
                            <h3>Menu 3</h3>
                            <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                        </div>
                        <div id="conrates" class="tab-pane fade">
                            <h3>Menu 3</h3>
                            <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                        </div>
                    </div>

                </div> 
            </div>

        </div>

    </div>
</section>

<!-- Bootstrap daterangepicker -->
<script src="{{asset('frontend/js/moment.min.js')}}"></script>
<script src="{{asset('frontend/js/daterangepicker.js')}}"></script>
<!-- <?php

function x_week_range($date) {
    $ts = strtotime($date);
    $start = (date('w', $ts) == 0) ? $ts : strtotime('last sunday', $ts);
    return array(date('m/d/Y', $start),
        date('m/d/Y', strtotime('next saturday', $start)));
}

function this_quater_range() {
    $current_month = date('m');
    $current_year = date('Y');
    if ($current_month >= 1 && $current_month <= 3) {
        $start_date = strtotime('01-January-' . $current_year);
        $end_date = strtotime('01-April-' . $current_year);
    } else if ($current_month >= 4 && $current_month <= 6) {
        $start_date = strtotime('01-April-' . $current_year);
        $end_date = strtotime('01-July-' . $current_year);
    } else if ($current_month >= 7 && $current_month <= 9) {
        $start_date = strtotime('01-July-' . $current_year);
        $end_date = strtotime('01-October-' . $current_year);
    } else if ($current_month >= 10 && $current_month <= 12) {
        $start_date = strtotime('01-October-' . $current_year);
        $end_date = strtotime('01-January-' . ($current_year + 1));
    }
    return array(date('m/d/Y', $start_date), date('m/d/Y', $end_date));
}

function last_quater_range() {
    $current_month = date('m');
    $current_year = date('Y');

    if ($current_month >= 1 && $current_month <= 3) {
        $start_date = strtotime('01-October-' . ($current_year - 1));
        $end_date = strtotime('01-January-' . $current_year);
    } else if ($current_month >= 4 && $current_month <= 6) {
        $start_date = strtotime('01-January-' . $current_year);
        $end_date = strtotime('01-April-' . $current_year);
    } else if ($current_month >= 7 && $current_month <= 9) {
        $start_date = strtotime('01-April-' . $current_year);
        $end_date = strtotime('01-July-' . $current_year);
    } else if ($current_month >= 10 && $current_month <= 12) {
        $start_date = strtotime('01-July-' . $current_year);
        $end_date = strtotime('01-October-' . $current_year);
    }

    return array(date('m/d/Y', $start_date), date('m/d/Y', $end_date));
}

list($start_date_tw, $end_date_tw) = x_week_range(date('m/d/Y'));
list($start_date_lw, $end_date_lw) = x_week_range(date('m/d/Y', strtotime('-7 Days')));
list($start_date_tq, $end_date_tq) = this_quater_range();
list($start_date_lq, $end_date_lq) = last_quater_range();
?> -->
<!-- hight chart -->
<script src="{{asset('frontend/js/highcharts.js')}}"></script>
<script src="{{asset('frontend/js/exporting.js')}}"></script> 

 <!-- select2 -->
<script src="{{asset('frontend/js/select2.min.js')}}"></script>  

<script>
Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Monthly Average Rainfall'
    },
    subtitle: {
        text: 'Source: WorldClimate.com'
    },
    xAxis: {
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Rainfall (mm)'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
            name: 'Tokyo',
            data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
        }, {
            name: 'New York',
            data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]
        }, {
            name: 'London',
            data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2]
        }, {
            name: 'Berlin',
            data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]
        }]
});

</script>

<script type="text/javascript">
$(function() {

    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);
    
});
</script>
<script>
        $(function()
        {
            $('.select-basic').select2();
            $('.select-multiple-basic').select2();
            $('#select-placeholder-single').select2(
            {
                placeholder: 'Select a state',
                allowClear: true
            });
            $('#select-placeholder-multiple').select2(
            {
                placeholder: 'Select a state'
            });
            $('#select-tag').select2(
            {
                tags: true
            });
            $('#select-tag-token').select2(
            {
                tags: true,
                tokenSeparators: [',', ' ']
            });
        });
        </script>
@endsection