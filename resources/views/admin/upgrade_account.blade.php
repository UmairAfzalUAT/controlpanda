@extends('admin.layouts.app')
@section('content')




    <section>
      <div class="plan-bg ">
        <div class="container">
          <h2>Choose a Plan<br><span>Select the plan that fits your needs and help us make sure </span></h2>
        </div>
      </div>
    </section>
    <section>
      <div id="price-table">
        <div class="container">
          <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6">
              <div class="pricing-plans">
                 <div class="pricing-grids">
                    <div class="pricing-grid1">
                      <div class="price-value gray">
                        <h2><a href="#"> BASIC</a></h2>
                        <h5><span>$14</span><lable>month</lable></h5>
                      </div>
                      <div class="price-bg">
                        <ul>
                          <li class="whyt"><a href="#" class="gray-color"><i style="color: #ff0000;" class="fas fa-circle"></i> 100 MB Disk Space</a></li>
                          <li><a href="#"><i style="color: #7ab428;" class="fas fa-circle"></i> 2 Subdomains</a></li>
                          <li class="whyt"><a href="#" class="gray-color"> <i style="color: #ff0000;" class="fas fa-circle"></i> 5 Email Accounts </a></li>
                          <li><a href="#" class="gray-color"><i style="color: #ff0000;" class="fas fa-circle"></i> Webmail Support </a></li>
                          <li class="whyt"><a href="#"> <i style="color: #7ab428;" class="fas fa-circle"></i> Customer Support 24/7</a></li>
                        </ul>
                        <div class="cart1">
                          <a class="popup-with-zoom-anim" href="select-unlimited-plan.html">Select</a>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
              <div class="pricing-plans">
                 <div class="pricing-grids">
                    <div class="pricing-grid1">
                      <div class="price-value pruple prupleone">
                        <h2><a href="#"> STANDARD</a></h2>
                        <h5><span>$18</span><lable>month</lable></h5>
                      </div>
                      <div class="price-bg">
                        <ul>
                          <li class="whyt"><a href="#"><i style="color: #7ab428;" class="fas fa-circle"></i> 100 MB Disk Space</a></li>
                          <li><a href="#"><i style="color: #7ab428;" class="fas fa-circle"></i> 2 Subdomains</a></li>
                          <li class="whyt"><a href="#"> <i style="color: #7ab428;" class="fas fa-circle"></i> 5 Email Accounts </a></li>
                          <li><a href="#" class="gray-color"><i style="color: #ff0000;" class="fas fa-circle"></i> Webmail Support </a></li>
                          <li class="whyt"><a href="#" class="gray-color"> <i style="color: #ff0000;" class="fas fa-circle"></i> Customer Support 24/7</a></li>
                        </ul>
                        <div class="cart1">
                          <a class="popup-with-zoom-anim pruple" href="select-unlimited-plan.html">Select</a>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
              <div class="pricing-plans">
                 <div class="pricing-grids">
                    <div class="pricing-grid1">
                      <div class="price-value green greenone">
                        <h2><a href="#"> COMFORT</a></h2>
                        <h5><span>$24</span><lable>month</lable></h5>
                      </div>
                      <div class="price-bg">
                        <ul>
                          <li class="whyt"><a href="#"><i style="color: #ff0000;" class="fas fa-circle"></i> 100 MB Disk Space</a></li>
                          <li><a href="#"><i style="color: #ff0000;" class="fas fa-circle"></i> 2 Subdomains</a></li>
                          <li class="whyt"><a href="#"> <i style="color: #ff0000;" class="fas fa-circle"></i> 5 Email Accounts </a></li>
                          <li><a href="#" class="gray-color"><i style="color: #7ab428;" class="fas fa-circle"></i> Webmail Support </a></li>
                          <li class="whyt"><a href="#"> <i style="color: #ff0000;" class="fas fa-circle"></i> Customer Support 24/7</a></li>
                        </ul>
                        <div class="cart1">
                          <a class="popup-with-zoom-anim green" href="select-unlimited-plan.html">Select</a>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
              <div class="pricing-plans">
                 <div class="pricing-grids">
                    <div class="pricing-grid1">
                      <div class="price-value orange orangeone">
                        <h2><a href="#"> PREMIUM</a></h2>
                        <h5><span>$28</span><lable>month</lable></h5>
                      </div>
                      <div class="price-bg">
                        <ul>
                          <li class="whyt"><a href="#"><i style="color: #7ab428;" class="fas fa-circle"></i> 100 MB Disk Space</a></li>
                          <li><a href="#"><i style="color: #7ab428;" class="fas fa-circle"></i> 2 Subdomains</a></li>
                          <li class="whyt"><a href="#"> <i style="color: #7ab428;" class="fas fa-circle"></i> 5 Email Accounts </a></li>
                          <li><a href="#"><i style="color: #7ab428;" class="fas fa-circle"></i> Webmail Support </a></li>
                          <li class="whyt"><a href="#"> <i style="color: #7ab428;" class="fas fa-circle"></i> Customer Support 24/7</a></li>
                        </ul>
                        <div class="cart1">
                          <a class="popup-with-zoom-anim orange" href="select-unlimited-plan.html">Select</a>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section>
      <div id="Include">
        <div class="container">
          <h3>All Premium Plans Include</h3>
          <div class="row">
            <div class="col-lg-2 col-md-6 col-sm-6 offset-xs-10">
              <div class="iner-box"><div><a href="#"><img src="{{asset('assets/images/icon-9.jpg')}}"></a></div><h2>FREE Hosting</h2></div>
            </div>
            <div class="col-lg-2 col-md-6 col-sm-6 offset-xs-10">
              <div class="iner-box"><div><a href="#"><img src="{{asset('assets/images/icon-10.jpg')}}"></a></div><h2>Domain Connection</h2></div>
            </div>
            <div class="col-lg-2 col-md-6 col-sm-6 offset-xs-10">
              <div class="iner-box"><div><a href="#"><img src="{{asset('assets/images/icon-11.jpg')}}"></a></div><h2>500MB+ Storage</h2></div>
            </div>
            <div class="col-lg-2 col-md-6 col-sm-6 offset-xs-10">
              <div class="iner-box"><div><a href="#"><img src="{{asset('assets/images/icon-12.jpg')}}"></a></div><h2>Google Analytics</h2></div>
            </div>
            <div class="col-lg-2 col-md-6 col-sm-6 offset-xs-10">
              <div class="iner-box"><div><a href="#"><img src="{{asset('assets/images/icon-13.jpg')}}"></a></div><h2>Premium Support</h2></div>
            </div>
            <div class="col-lg-2 col-md-6 col-sm-6 offset-xs-10">
              <div class="iner-box"><div><a href="#"><img src="{{asset('assets/images/icon-14.jpg')}}"></a></div><h2>No Set-up Fee</h2></div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section>
      <div id="trusted">
        <div class="container">
          <h4>Trusted By Millions</h4>
          <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="trusted-box">
                <div><img src="{{asset('assets/images/icon-15.png')}}"></div>
                <h5>SSL SECURE PAYMENT<br><span>Your information is protected by 256-bit SSL encryption</span></h5>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="trusted-box">
                <div><img src="{{asset('assets/images/icon-16.png')}}"></div>
                <h5>14 Day Money Back Guarantee<br><span>On ALL Plans</span></h5>
              </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12">
              <div class="trusted-box" style="padding:38px 60px; height: 88% ">
                <h5>We accept all of the following credit cards<br><br></h5>
                <div><img src="{{asset('assets/images/cradit.png')}}"></div>
              </div>
            </div>
          </div>
          </div>
          </div>
      </div>
    </section>
    <section>
      <div id="question">
        <h4>Frequently Asked Questions</h4>
        <div class="container">
          <div>
            <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#demo1">What's a Premium Plan?</button>
              <div id="demo1" class="collapse collapse-box">
                <strong>What are the benefits of a Premium plan?</strong><br><br>
                <span>The Premium plan offers many benefits. Highlights include:</span><br><br>
                <b>The ability to connect your own domain</b><br>
                <span>Your own domain (e.g. www.MyStunningWebsite.com) gives your business credibility and professionalism, and makes it easier for your audience to find you.</span></b><br><br>
                <b>Remove Ads</b><br>
                <span>Websites display a small banner at the top and bottom of the page promoting. We do not place any other advertisements on your site. Most of our Premium plans let you remove these banners.</span></b><br><br>
                <b>Extra bandwidth and storage</b><br>
                <span>Most of our Premium plans come with extra bandwidth and storage, allowing for higher-res graphics and content on your site.</span>
              </div>
              <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#demo2">What are the benefits of purchasing a Yearly Savings Plan?</button>
              <div id="demo2" class="collapse collapse-box">
                <strong>What are the benefits of a Premium plan?</strong><br><br>
                <span>The Premium plan offers many benefits. Highlights include:</span><br><br>
                <b>The ability to connect your own domain</b><br>
                <span>Your own domain (e.g. www.MyStunningWebsite.com) gives your business credibility and professionalism, and makes it easier for your audience to find you.</span></b><br><br>
                <b>Remove Ads</b><br>
                <span>Websites display a small banner at the top and bottom of the page promoting. We do not place any other advertisements on your site. Most of our Premium plans let you remove these banners.</span></b><br><br>
                <b>Extra bandwidth and storage</b><br>
                <span>Most of our Premium plans come with extra bandwidth and storage, allowing for higher-res graphics and content on your site.</span>
              </div>
              <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#demo3">What does “connect your own domain“ mean?</button>
              <div id="demo3" class="collapse collapse-box">
                <strong>What are the benefits of a Premium plan?</strong><br><br>
                <span>The Premium plan offers many benefits. Highlights include:</span><br><br>
                <b>The ability to connect your own domain</b><br>
                <span>Your own domain (e.g. www.MyStunningWebsite.com) gives your business credibility and professionalism, and makes it easier for your audience to find you.</span></b><br><br>
                <b>Remove Ads</b><br>
                <span>Websites display a small banner at the top and bottom of the page promoting. We do not place any other advertisements on your site. Most of our Premium plans let you remove these banners.</span></b><br><br>
                <b>Extra bandwidth and storage</b><br>
                <span>Most of our Premium plans come with extra bandwidth and storage, allowing for higher-res graphics and content on your site.</span>
              </div>
              <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#demo4">How do I purchase my own domain?</button>
              <div id="demo4" class="collapse collapse-box">
                <strong>What are the benefits of a Premium plan?</strong><br><br>
                <span>The Premium plan offers many benefits. Highlights include:</span><br><br>
                <b>The ability to connect your own domain</b><br>
                <span>Your own domain (e.g. www.MyStunningWebsite.com) gives your business credibility and professionalism, and makes it easier for your audience to find you.</span></b><br><br>
                <b>Remove Ads</b><br>
                <span>Websites display a small banner at the top and bottom of the page promoting. We do not place any other advertisements on your site. Most of our Premium plans let you remove these banners.</span></b><br><br>
                <b>Extra bandwidth and storage</b><br>
                <span>Most of our Premium plans come with extra bandwidth and storage, allowing for higher-res graphics and content on your site.</span>
              </div>
          </div>
        </div>
      </div>
    </section>














<!--<section class="contentarea">
    <div class="container-fluid">
        
        <div class="col-sm-12">
        <div class="gap"></div>
        <div class="page-header"><h1>Upgrade Membership</h1></div>
                <div class="gap"></div>
          <form id="package-form" class="form-horizontal" action="{{url('/admin/update_membership')}}" method="POST" novalidate="novalidate">
          {{ csrf_field() }}
          <input type="hidden" name="old_package_id" value="{{$package_id->package_id}}" />
           
           <div class="col-sm-6 col-sm-offset-4">
              <p class="helpinfotextarea">
                <i class="fa fa-exclamation-triangle"></i>
                <strong>Important Note:</strong>
                Payment will be deduct from your default payment gateway.
              </p>
           </div>
            
           <div class="col-sm-6 col-sm-offset-4">
           <div class="form-group">
                <label class="col-sm-4 control-label" for="is_active">Packages:</label>
                <div class="col-sm-8">
                    
                    @foreach($packages as $key=>$package)
                    
                    <input type="radio" name="package" id="{{$package->id}}" class="package required"   price="{{$package->price}}" value="{{$package->id}}" {{($package->id == $package_id->package_id)? 'checked="checked"' : ''}} > <label for="{{$package->id}}">{{$package->title}} &nbsp;&nbsp;</label>
                    @endforeach
                </div>
            </div>
            
            </div>
            
            <div class="col-sm-6 col-sm-offset-4">
            <div class="form-group">
                <label class="col-sm-4 control-label" for="is_active">Package Price:</label>
                
                <div class="col-sm-8 price">
                    
                </div>
            </div>
            </div>
            
            <div class="col-sm-6 col-sm-offset-4">
           <div class="form-group">
                <label class="col-sm-4 control-label" for="payment_type">Payment Method:</label>
                <div class="col-sm-8">
                    <input type="radio" name="payment_type" id="Stripe" class=" required" checked="checked"  value="Stripe"  > <label for="Stripe">Stripe &nbsp;&nbsp;</label>
                    <input type="radio" name="payment_type" id="Paypal" class=" required"   value="Paypal"  > <label for="Paypal">Paypal &nbsp;&nbsp;</label>
                    
                </div>
            </div>
            
            </div>
            
            
         <div class="col-sm-6 col-sm-offset-4">
          <div class="form-actions">
                <div class="row">
                    <div class=" col-sm-8 text-right">
                        <a href="{{url('/admin/dashboard')}}" class="btn btn-default">Cancel</a>
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>    
            </div>
                        
           </div>
            
         </form> 
           
        </div>
        
        

    </div>
    
   
    
</section>-->

<style type="text/css">
.error { color:red;}
</style>



<script type="text/javascript">
$(function() {
	
   $('#package-form').validate();
   
   $('.price').text('$'+$('.package:checked').attr("price")+' Per Month');

   $('.package').click(function(){
   
         $('.price').text('$'+$(this).attr('price')+' Per Month');
   });
    
});
</script>

@endsection