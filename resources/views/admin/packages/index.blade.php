@extends('admin.layouts.app')

@section('content')

<div class="breadcrumbs contentarea">
    <div class="container-fluid">
        <ul>
            <li>
                <a href="{{url('/admin/dashboard')}}">Dashboard</a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a>Packages</a>
            </li>
        </ul>
    </div>    
    <div class="close-bread">
        <a href="#"><i class="icon-remove"></i></a>
    </div>
</div>
<div class="main_panel contentarea clearfix">
    <div class="white_box container-fluid clearfix">
        <div class="page-header"><h1>Packages 
                @if(have_premission(104))
                <a href="{{url('/admin/packages/create')}}" class="btn btn-info pull-right">Add New Package</a>
                @endif
            </h1></div>
        <div class="row" >
            <div class="col-md-12">
                <div class="box">
                    <div class="box-content">
                        <table class="table table-hover table-nomargin no-margin table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th># Of Funnels</th>
                                    <th># Of Pages</th>
                                    <th># Of Visitors</th>
                                    <th># Of Contacts</th>
                                    <th>Custom Domains</th>
                                    <th>Priority Template Requests</th>
                                    <th>Div Class</th>
                                    <th>Price</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($packages as $package) 
                                <tr>
                                    <td>{!!$package->title!!}</td>
                                    <td>{!!$package->no_of_funnels!!}</td>
                                    <td>{!!$package->no_of_pages!!}</td>
                                    <td>{!!$package->no_of_visitors!!}</td>
                                    <td>{!!$package->no_of_contacts!!}</td>
                                    <td>{!!$package->custom_domains!!}</td>
                                    <td>{!!$package->priority_template_requests!!}</td>
                                    <td>{!!$package->div_class!!}</td>
                                    <td>${!!$package->price!!}</td>
                                    
                                    <td>@if($package->is_active == 1) <label class="label label-success">Active</label> @else <label class="label label-danger">Inactive</label> @endif</td>
                                    <td>
                                        @if(have_premission(105))
                                        <a href="{{ url('/admin/packages/'.$package->id.'/edit')}}"><i class="fa fa-edit fa-fw"></i></a>
                                        @endif
                                        @if(have_premission(106))
                                        {!! Form::open([
                                        'method'=>'DELETE',
                                        'url' => ['admin/packages', $package->id],
                                        'style' => 'display:inline'
                                        ]) !!}
                                        {!! Form::button('<i class="fa fa-trash fa-fw" title="Delete Package"></i>', ['class' => 'delete-form-btn']) !!}
                                        {!! Form::submit('Delete', ['class' => 'hidden deleteSubmit']) !!}
                                        {!! Form::close() !!}
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <nav class="pull-right">{!! $packages->render() !!}</nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection