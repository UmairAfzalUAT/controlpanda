@extends('admin.layouts.app')

@section('content')
<!-- Validation -->
<script src="{{ asset('js/plugins/validation/jquery.validate.min.js')}}"></script>
<script src="{{ asset('js/plugins/jquery-ui/jquery.ui.spinner.js')}}"></script>

<div class="breadcrumbs contentarea">
    <div class="container-fluid">
        <ul>
            <li>
                <a href="{{url('/admin/dashboard')}}">Dashboard</a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a href="{{url('/admin/packages')}}">Packages</a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a>{!!$action!!} Package</a>
            </li>
        </ul>
    </div>    
    <div class="close-bread">
        <a href="#"><i class="icon-remove"></i></a>
    </div>
</div>

<div class="main_panel contentarea clearfix">
    <div class="white_box container-fluid clearfix">
        <div class="page-header"><h1>{!!$action!!} Package</h1></div>
        <div class="box">
            <div class="box-content">
                <form id="packages-form" class="form-horizontal form-validate" action="{{url('/admin/packages')}}" method="POST" novalidate="novalidate">
                    <div class="form_wrap">
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="title">Title</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="title" id="title" placeholder="Enter Package Name" data-rule-required="true" data-rule-minlength="5" aria-required="true" value="{!!@$package['title']!!}" required=""/>
                            </div>
                        </div>
                        <input type="hidden" name="action" value="{!!$action!!}">
                        <input type="hidden" name="id" value="{!!@$package['id']!!}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <div class="control-group">
                                <label class="col-sm-4 control-label" for="no_of_funnels"># Of Funnels</label>
                                <div class="col-sm-8 error-container-custom">
                                    <input type="text" name="no_of_funnels" id="no_of_funnels" value="@if (isset($package['no_of_funnels'])) {!!$package['no_of_funnels']!!} @else 0 @endif" class="spinner input-mini form-control" data-rule-number="true" data-rule-required="true" aria-required="true" aria-invalid="true" aria-describedby="numberfield-error" required="">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="no_of_pages"># Of Pages</label>
                            <div class="col-sm-8">
                                <input type="text" name="no_of_pages" id="no_of_pages" placeholder="# Of Pages" class="form-control" data-rule-required="true" data-rule-minlength="5" aria-required="true" value="@if (isset($package['no_of_pages'])) {!!$package['no_of_pages']!!} @else 0 @endif" required=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="control-group">
                                <label class="col-sm-4 control-label" for="no_of_visitors"># Of Visitors</label>            
                                <div class="col-sm-8 error-container-custom">
                                    <input type="text" name="no_of_visitors" id="no_of_visitors" value="@if (isset($package['no_of_visitors'])) {!!$package['no_of_visitors']!!} @else 0 @endif" class="spinner input-mini form-control" data-rule-number="true" data-rule-required="true" aria-required="true" aria-invalid="true" aria-describedby="numberfield-error" required="">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="control-group">
                                <label class="col-sm-4 control-label" for="no_of_contacts"># Of Contacts</label>
                                <div class="col-sm-8 error-container-custom">
                                    <input type="text" name="no_of_contacts" id="no_of_contacts" value="@if (isset($package['no_of_contacts'])) {!!$package['no_of_contacts']!!} @else 0 @endif" class="spinner input-mini form-control" data-rule-number="true" data-rule-required="true" aria-required="true" aria-invalid="true" aria-describedby="numberfield-error" required="">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="control-group">
                                <label class="col-sm-4 control-label" for="custom_domains">Custom Domains</label>
                                <div class="col-sm-8 error-container-custom">
                                    <input type="text" name="custom_domains" id="custom_domains" value="@if (isset($package['custom_domains'])) {!!$package['custom_domains']!!} @else 0 @endif" class="spinner input-mini form-control" data-rule-number="true" data-rule-required="true" aria-required="true" aria-invalid="true" aria-describedby="numberfield-error">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="control-group">
                                <label class="col-sm-4 control-label" for="a_b_split_tests">A/B Split Tests</label>
                                <div class="col-sm-8 error-container-custom">
                                    <input type="text" name="a_b_split_tests" id="a_b_split_tests" value="@if (isset($package['a_b_split_tests'])) {!!$package['a_b_split_tests']!!} @else 0 @endif" class="spinner input-mini form-control" data-rule-number="true" data-rule-required="true" aria-required="true" aria-invalid="true" aria-describedby="numberfield-error">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="control-group">
                                <label class="col-sm-4 control-label" for="email_integrations">Email Integrations</label>
                                <div class="col-sm-8 error-container-custom">
                                    <input type="text" name="email_integrations" id="email_integrations" value="@if (isset($package['email_integrations'])) {!!$package['email_integrations']!!} @else 0 @endif" class="spinner input-mini form-control" data-rule-number="true" data-rule-required="true" aria-required="true" aria-invalid="true" aria-describedby="numberfield-error">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="control-group">
                                <label class="col-sm-4 control-label" for="optin_funnels">Optin Funnels</label>
                                <div class="col-sm-8 error-container-custom">
                                    <input type="text" name="optin_funnels" id="optin_funnels" value="@if (isset($package['optin_funnels'])) {!!$package['optin_funnels']!!} @else 0 @endif" class="spinner input-mini form-control" data-rule-number="true" data-rule-required="true" aria-required="true" aria-invalid="true" aria-describedby="numberfield-error">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="control-group">
                                <label class="col-sm-4 control-label" for="clickpops">ClickPops</label>
                                <div class="col-sm-8 error-container-custom">
                                    <input type="text" name="clickpops" id="clickpops" value="@if (isset($package['clickpops'])) {!!$package['clickpops']!!} @else 0 @endif" class="spinner input-mini form-control" data-rule-number="true" data-rule-required="true" aria-required="true" aria-invalid="true" aria-describedby="numberfield-error">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="control-group">
                                <label class="col-sm-4 control-label" for="clickoptin">ClickOptin</label>
                                <div class="col-sm-8 error-container-custom">
                                    <input type="text" name="clickoptin" id="clickoptin" value="@if (isset($package['clickoptin'])) {!!$package['clickoptin']!!} @else 0 @endif" class="spinner input-mini form-control" data-rule-number="true" data-rule-required="true" aria-required="true" aria-invalid="true" aria-describedby="numberfield-error">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="control-group">
                                <label class="col-sm-4 control-label" for="all_advanced_funnels">All Advanced Funnels</label>
                                <div class="col-sm-8 error-container-custom">
                                    <input type="text" name="all_advanced_funnels" id="all_advanced_funnels" value="@if (isset($package['all_advanced_funnels'])) {!!$package['all_advanced_funnels']!!} @else 0 @endif" class="spinner input-mini form-control" data-rule-number="true" data-rule-required="true" aria-required="true" aria-invalid="true" aria-describedby="numberfield-error">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="control-group">
                                <label class="col-sm-4 control-label" for="sales_funnels">Sales Funnels</label>
                                <div class="col-sm-8 error-container-custom">
                                    <input type="text" name="sales_funnels" id="sales_funnels" value="@if (isset($package['sales_funnels'])) {!!$package['sales_funnels']!!} @else 0 @endif" class="spinner input-mini form-control" data-rule-number="true" data-rule-required="true" aria-required="true" aria-invalid="true" aria-describedby="numberfield-error">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="control-group">
                                <label class="col-sm-4 control-label" for="membership_funnels">Membership Funnels</label>
                                <div class="col-sm-8 error-container-custom">
                                    <input type="text" name="membership_funnels" id="membership_funnels" value="@if (isset($package['membership_funnels'])) {!!$package['membership_funnels']!!} @else 0 @endif" class="spinner input-mini form-control" data-rule-number="true" data-rule-required="true" aria-required="true" aria-invalid="true" aria-describedby="numberfield-error">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="control-group">
                                <label class="col-sm-4 control-label" for="no_of_members"># Of Members</label>
                                <div class="col-sm-8 error-container-custom">
                                    <input type="text" name="no_of_members" id="no_of_members" value="@if (isset($package['no_of_members'])) {!!$package['no_of_members']!!} @else 0 @endif" class="spinner input-mini form-control" data-rule-number="true" data-rule-required="true" aria-required="true" aria-invalid="true" aria-describedby="numberfield-error">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="control-group">
                                <label class="col-sm-4 control-label" for="auto_webinar_funnels">Auto Webinar Funnels</label>
                                <div class="col-sm-8 error-container-custom">
                                    <input type="text" name="auto_webinar_funnels" id="auto_webinar_funnels" value="@if (isset($package['auto_webinar_funnels'])) {!!$package['auto_webinar_funnels']!!} @else 0 @endif" class="spinner input-mini form-control" data-rule-number="true" data-rule-required="true" aria-required="true" aria-invalid="true" aria-describedby="numberfield-error">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="control-group">
                                <label class="col-sm-4 control-label" for="webinar_funnels">Webinar Funnels</label>
                                <div class="col-sm-8 error-container-custom">
                                    <input type="text" name="webinar_funnels" id="webinar_funnels" value="@if (isset($package['webinar_funnels'])) {!!$package['webinar_funnels']!!} @else 0 @endif" class="spinner input-mini form-control" data-rule-number="true" data-rule-required="true" aria-required="true" aria-invalid="true" aria-describedby="numberfield-error">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="control-group">
                                <label class="col-sm-4 control-label" for="hangout_funnels">Hangout Funnels</label>
                                <div class="col-sm-8 error-container-custom">
                                    <input type="text" name="hangout_funnels" id="hangout_funnels" value="@if (isset($package['hangout_funnels'])) {!!$package['hangout_funnels']!!} @else 0 @endif" class="spinner input-mini form-control" data-rule-number="true" data-rule-required="true" aria-required="true" aria-invalid="true" aria-describedby="numberfield-error">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="control-group">
                                <label class="col-sm-4 control-label" for="order_pages">Order Pages</label>
                                <div class="col-sm-8 error-container-custom">
                                    <input type="text" name="order_pages" id="order_pages" value="@if (isset($package['order_pages'])) {!!$package['order_pages']!!} @else 0 @endif" class="spinner input-mini form-control" data-rule-number="true" data-rule-required="true" aria-required="true" aria-invalid="true" aria-describedby="numberfield-error">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="control-group">
                                <label class="col-sm-4 control-label" for="upsell_pages">Upsell Pages</label>
                                <div class="col-sm-8 error-container-custom">
                                    <input type="text" name="upsell_pages" id="upsell_pages" value="@if (isset($package['upsell_pages'])) {!!$package['upsell_pages']!!} @else 0 @endif" class="spinner input-mini form-control" data-rule-number="true" data-rule-required="true" aria-required="true" aria-invalid="true" aria-describedby="numberfield-error">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="control-group">
                                <label class="col-sm-4 control-label" for="downsale_pages">Downsale Pages</label>
                                <div class="col-sm-8 error-container-custom">
                                    <input type="text" name="downsale_pages" id="downsale_pages" value="@if (isset($package['downsale_pages'])) {!!$package['downsale_pages']!!} @else 0 @endif" class="spinner input-mini form-control" data-rule-number="true" data-rule-required="true" aria-required="true" aria-invalid="true" aria-describedby="numberfield-error">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="control-group">
                                <label class="col-sm-4 control-label" for="share_your_funnels">Share Your Funnels</label>
                                <div class="col-sm-8 error-container-custom">
                                    <input type="text" name="share_your_funnels" id="share_your_funnels" value="@if (isset($package['share_your_funnels'])) {!!$package['share_your_funnels']!!} @else 0 @endif" class="spinner input-mini form-control" data-rule-number="true" data-rule-required="true" aria-required="true" aria-invalid="true" aria-describedby="numberfield-error">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="control-group">
                                <label class="col-sm-4 control-label" for="priority_support">Priority Support</label>
                                <div class="col-sm-8 error-container-custom">
                                    <input type="text" name="priority_support" id="priority_support" value="@if (isset($package['priority_support'])) {!!$package['priority_support']!!} @else 0 @endif" class="spinner input-mini form-control" data-rule-number="true" data-rule-required="true" aria-required="true" aria-invalid="true" aria-describedby="numberfield-error">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="control-group">
                                <label class="col-sm-4 control-label" for="priority_template_requests">Priority Template Requests</label>
                                <div class="col-sm-8 error-container-custom">
                                    <input type="text" name="priority_template_requests" id="priority_template_requests" value="@if (isset($package['priority_template_requests'])) {!!$package['priority_template_requests']!!} @else 0 @endif" class="spinner input-mini form-control" data-rule-number="true" data-rule-required="true" aria-required="true" aria-invalid="true" aria-describedby="numberfield-error">
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="control-group">
                                <label class="col-sm-4 control-label" for="div_class">Div Class</label>
                                <div class="col-sm-8 error-container-custom">
                                    <input type="text" name="div_class" id="div_class" value="@if (isset($package['div_class'])) {!!$package['div_class']!!} @else 0 @endif" class="spinner input-mini form-control" data-rule-number="true" data-rule-required="true" aria-required="true" aria-invalid="true" aria-describedby="numberfield-error">
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="control-group">
                                <label class="col-sm-4 control-label" for="price">Price</label>
                                <div class="col-sm-8 error-container-custom">
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input class="form-control ui-wizard-content" name="price" id="price" placeholder="00.00"  data-rule-number="true" data-rule-required="true" aria-required="true" aria-invalid="true" aria-describedby="numberfield-error"  value="@if (isset($package['price'])) {!!$package['price']!!} @else 0.00 @endif">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--<div class="form-group">
                            <label class="col-sm-4 control-label" for="rating">Rate</label>
                            <div class="col-sm-4 error-container-custom">
                                <select class="form-control" name="rating" id="rating">
                                    <option value="">Select Rating</option>
                                    @for($i=1; $i<=5; $i++)
                                    <option @if(isset($package['rating']) && $package['rating'] == $i) selected @endif value="{{$i}}">{{$i}}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>-->

                        <div class="form-group">
                            <label class="col-sm-4 control-label pt_0" for="is_active">Status</label>
                            <div class="col-sm-8">
                                <input type="radio" name="is_active" value="1" @if(!isset($package['is_active']) || $package['is_active'] == 1) checked @endif /> Active &nbsp;&nbsp; <input type="radio" name="is_active" value="0"  @if(isset($package['is_active']) && $package['is_active'] == 0) checked @endif/> Inactive
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-sm-offset-4 col-sm-8 text-right">
                                    <a href="{{url('/admin/packages')}}" class="btn btn-default">Cancel</a>
                                    <button type="submit" class="btn btn-success">Save</button>
                                </div>
                            </div>    
                        </div>
                    </div>    
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
