@extends('admin.layouts.app')

@section('content')

@include('admin.sections.subheader')
<div class="container-fluid">
<section class="inner-full-width-panel pr-30">


		
					<div class="tab-content">
  <div id="menu4" class="tab-pane in">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-4">
							<div class="left-menu">
								<div class="heading-title">Website Statistics & opt in</div>
								<ul class="nav nav-tabs left-panel-menu">
									<li><a data-toggle="tab" href="#hname">Index</a></li>
									<li><a data-toggle="tab" href="#aname" class="active">About Comapny</a></li>
									<li><a data-toggle="tab" href="#pname">Our Products</a></li>
									<li><a data-toggle="tab" href="#bname">Blog</a></li>
									<li><a data-toggle="tab" href="#hiname">History</a></li>
									<li><a data-toggle="tab" href="#cname">Contact Us</a></li>
								</ul>
							</div>
						</div>
						<div class="col-lg-9 col-md-9 col-sm-8">
							<div class="row right-content pl-20">
								<div class="top-header">
									<ul>
										<li>Sorting</li>
										<li><div class="form-group">
						                <div class="input-group date form_date col-md-5" data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
						                    <input class="form-control badge-custom"  type="text" value="From" readonly>
						                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
											<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
						                </div>
										<input type="hidden" id="dtp_input2" value="" /><br/>
						            </div></li>
						            <li><div class="form-group">
						                <div class="input-group date form_date col-md-5" data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
						                    <input class="form-control badge-custom"  type="text" value="To" readonly>
						                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
											<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
						                </div>
										<input type="hidden" id="dtp_input2" value="" /><br/>
						            </div></li>
									</ul>
 
								</div>
							</div>
							<div class="tab-content">
								<div id="hname" class="tab-pane fade active show">
									<div class="row">
										<div class="col-xl-6 col-lg-12">
											<div class="single-panel">
												<div class="left">
													<div class="padding-panel">
													<div class="viewQuote">
													1455
													Views
													</div>
													</div>												
													<div class="separator-border-panel"></div>
													<div class="padding-panel float-left w-100">
													<ul class="single-panel-list">
														<li><a href="#">All</a></li>
														<li><a href="#">Unique</a></li>
													</ul>
													</div>
												</div>
												<div class="right">
													<div class="header">Weekly Page views</div>
													<div class="body">
									<img src="{{asset('frontend/images/graph1.jpg')}}">
													</div>
													<div class="footer">
													<img src="{{asset('frontend/images/day-graph.png')}}">
													</div>
												</div>											
											</div>
										</div>
										<div class="col-xl-6 col-lg-12">
											<div class="single-panel">
												<div class="left op2">
													<div class="padding-panel">
														<div class="viewQuote">
														1455
														Views
														</div>
													</div>												
													<div class="separator-border-panel"></div>
													<div class="padding-panel float-left w-100">
														<ul class="single-panel-list">
															<li><a href="#">All</a></li>
															<li><a href="#">Unique</a></li>
														</ul>
													</div>
												</div>
												<div class="right">
													<div class="header">Weekly Opt-ins</div>
													<div class="body">
													<img src="{{asset('frontend/images/graph2.jpg')}}">
													</div>
													<div class="footer">
													<img src="{{asset('frontend/images/day-graph.png')}}">
													</div>	
												</div>											
											</div>
										</div>									
									</div>
								</div>
								<div id="aname" class="tab-pane in">
									<div class="row">	
										<div class="col-xl-6 col-lg-12">
											<div class="single-panel">
												<div class="left">
													<div class="padding-panel">
														<div class="viewQuote">
														2055
														Views
														</div>
													</div>												
													<div class="separator-border-panel"></div>
													<div class="padding-panel float-left w-100">
														<ul class="single-panel-list">
															<li><a href="#">All</a></li>
															<li><a href="#">Unique</a></li>
														</ul>
													</div>
												</div>
												<div class="right">
													<div class="header">Weekly Page views</div>
													<div class="body">
													<img src="{{asset('frontend/images/graph1.jpg')}}">
													</div>
													<div class="footer">
													<img src="{{asset('frontend/images/day-graph.png')}}">
													</div>
												</div>											
											</div>
										</div>
										<div class="col-xl-6 col-lg-12">
											<div class="single-panel">
												<div class="left op2">
													<div class="padding-panel">
														<div class="viewQuote">
														2055
														Views
														</div>
													</div>												
													<div class="separator-border-panel"></div>
													<div class="padding-panel float-left w-100">
														<ul class="single-panel-list">
															<li><a href="#">All</a></li>
															<li><a href="#">Unique</a></li>
														</ul>
													</div>
												</div>
												<div class="right">
													<div class="header">Weekly Opt-ins</div>
													<div class="body">
													<img src="{{asset('frontend/images/graph2.jpg')}}">
													</div>
													<div class="footer">
													<img src="{{asset('frontend/images/day-graph.png')}}">
													</div>	
												</div>											
											</div>
										</div>									
									</div>
								</div>
								<div id="pname" class="tab-pane fade">
									<div class="row">	
										<div class="col-xl-6 col-lg-12">
											<div class="single-panel">
												<div class="left">
													<div class="padding-panel">
														<div class="viewQuote">
														3055
														Views
														</div>
													</div>												
													<div class="separator-border-panel"></div>
													<div class="padding-panel float-left w-100">
														<ul class="single-panel-list">
															<li><a href="#">All</a></li>
															<li><a href="#">Unique</a></li>
														</ul>
													</div>
												</div>
												<div class="right">
													<div class="header">Weekly Page views</div>
													<div class="body">
														<img src="{{asset('frontend/images/graph1.jpg')}}">
													</div>
													<div class="footer">
														<img src="{{asset('frontend/images/day-graph.png')}}">
													</div>
												</div>											
											</div>
										</div>
										<div class="col-xl-6 col-lg-12">
											<div class="single-panel">
												<div class="left op2">
													<div class="padding-panel">
														<div class="viewQuote">
														3055
														Views
														</div>
													</div>												
													<div class="separator-border-panel"></div>
													<div class="padding-panel float-left w-100">
														<ul class="single-panel-list">
															<li><a href="#">All</a></li>
															<li><a href="#">Unique</a></li>
														</ul>
													</div>
												</div>
												<div class="right">
													<div class="header">Weekly Opt-ins</div>
													<div class="body">
													<img src="{{asset('frontend/images/graph2.jpg')}}">
													</div>
													<div class="footer">
													<img src="{{asset('frontend/images/day-graph.png')}}">
													</div>	
												</div>											
											</div>
										</div>									
									</div>
								</div>
								<div id="bname" class="tab-pane fade">
									<div class="row">	
										<div class="col-xl-6 col-lg-12">
											<div class="single-panel">
												<div class="left">
													<div class="padding-panel">
														<div class="viewQuote">
														4055
														Views
														</div>
													</div>												
													<div class="separator-border-panel"></div>
													<div class="padding-panel float-left w-100">
														<ul class="single-panel-list">
															<li><a href="#">All</a></li>
															<li><a href="#">Unique</a></li>
														</ul>
													</div>
												</div>
												<div class="right">
													<div class="header">Weekly Page views</div>
													<div class="body">
														<img src="{{asset('frontend/images/graph1.jpg')}}">
													</div>
													<div class="footer">
														<img src="{{asset('frontend/images/day-graph.png')}}">
													</div>
												</div>											
											</div>
										</div>
										<div class="col-xl-6 col-lg-12">
											<div class="single-panel">
												<div class="left op2">
													<div class="padding-panel">
														<div class="viewQuote">
														4055
														Views
														</div>
													</div>												
													<div class="separator-border-panel"></div>
													<div class="padding-panel float-left w-100">
														<ul class="single-panel-list">
															<li><a href="#">All</a></li>
															<li><a href="#">Unique</a></li>
														</ul>
													</div>
												</div>
												<div class="right">
													<div class="header">Weekly Opt-ins</div>
													<div class="body">
													<img src="{{asset('frontend/images/graph2.jpg')}}">
													</div>
													<div class="footer">
													<img src="{{asset('frontend/images/day-graph.png')}}">
													</div>	
												</div>											
											</div>
										</div>									
									</div>
								</div>
								<div id="hiname" class="tab-pane fade">
									<div class="row">	
										<div class="col-xl-6 col-lg-12">
											<div class="single-panel">
												<div class="left">
													<div class="padding-panel">
														<div class="viewQuote">
														5055
														Views
														</div>
													</div>												
													<div class="separator-border-panel"></div>
													<div class="padding-panel float-left w-100">
														<ul class="single-panel-list">
															<li><a href="#">All</a></li>
															<li><a href="#">Unique</a></li>
														</ul>
													</div>
												</div>
												<div class="right">
													<div class="header">Weekly Page views</div>
													<div class="body">
														<img src="{{asset('frontend/images/graph1.jpg')}}">
													</div>
													<div class="footer">
														<img src="{{asset('frontend/images/day-graph.png')}}">
													</div>
												</div>											
											</div>
										</div>
										<div class="col-xl-6 col-lg-12">
											<div class="single-panel">
												<div class="left op2">
													<div class="padding-panel">
														<div class="viewQuote">
														5055
														Views
														</div>
													</div>												
													<div class="separator-border-panel"></div>
													<div class="padding-panel float-left w-100">
														<ul class="single-panel-list">
															<li><a href="#">All</a></li>
															<li><a href="#">Unique</a></li>
														</ul>
													</div>
												</div>
												<div class="right">
													<div class="header">Weekly Opt-ins</div>
													<div class="body">
													<img src="{{asset('frontend/images/graph2.jpg')}}">
													</div>
													<div class="footer">
													<img src="{{asset('frontend/images/day-graph.png')}}">
													</div>	
												</div>											
											</div>
										</div>									
									</div>
								</div>
								<div id="cname" class="tab-pane fade">
									<div class="row">	
										<div class="col-xl-6 col-lg-12">
											<div class="single-panel">
												<div class="left">
													<div class="padding-panel">
														<div class="viewQuote">
														6055
														Views
														</div>
													</div>												
													<div class="separator-border-panel"></div>
													<div class="padding-panel float-left w-100">
														<ul class="single-panel-list">
															<li><a href="#">All</a></li>
															<li><a href="#">Unique</a></li>
														</ul>
													</div>
												</div>
												<div class="right">
													<div class="header">Weekly Page views</div>
													<div class="body">
														<img src="{{asset('frontend/images/graph1.jpg')}}">
													</div>
													<div class="footer">
														<img src="{{asset('frontend/images/day-graph.png')}}">
													</div>
												</div>											
											</div>
										</div>
										<div class="col-xl-6 col-lg-12">
											<div class="single-panel">
												<div class="left op2">
													<div class="padding-panel">
														<div class="viewQuote">
														6055
														Views
														</div>
													</div>												
													<div class="separator-border-panel"></div>
													<div class="padding-panel float-left w-100">
														<ul class="single-panel-list">
															<li><a href="#">All</a></li>
															<li><a href="#">Unique</a></li>
														</ul>
													</div>
												</div>
												<div class="right">
													<div class="header">Weekly Opt-ins</div>
													<div class="body">
													<img src="{{asset('frontend/images/graph2.jpg')}}">
													</div>
													<div class="footer">
													<img src="{{asset('frontend/images/day-graph.png')}}">
													</div>	
												</div>											
											</div>
										</div>									
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="menu9" class="tab-pane">
					<div class="right-content right-content-space fixed-width">
                                <div class="header-title">
                                  <h3 class="warning left-part">Contacts</h3>
                                  <div class="right-part">
                                   <span>Export Contacts</span>
                                   <a href="#" class="icon-green">
                                    <img src="{{asset('frontend/images/msword.svg')}}">
                                   </a>
                                   <a href="#" class="icon-green">
                                    <img src="{{asset('frontend/images/print.svg')}}">
                                   </a>
                                  </div>
                                </div>
                            <div class="editor-dashboard-container">
                                <div class="dashboard-Contact-box">
                                	<div class="table-responsive">
                                        <table class="table min-width">
                                            <tr>
                                              <td><div class="h6">1</div></td>
                                              <td>Tristan Mike</td>
                                              <td>
                                              <img class="img-fluid" src="{{asset('frontend/images/icon-4.jpg')}}">
                                              <span> electromine@controlpanda.com</span></td>
                                              <td> <img class="img-fluid" src="{{asset('frontend/images/icon-5.jpg')}}"> <span>31055511660</span></td>
                                              <td> <img class="img-fluid" src="{{asset('frontend/images/icon-6.jpg')}}"> <span>192.168.0.61</span></td>
                                              <td> <img class="img-fluid" src="{{asset('frontend/images/icon-7.jpg')}}"> <span>indexpage</span></td>
                                              <td><img class="img-fluid" src="{{asset('frontend/images/icon-1.jpg')}}"></td>
                                              <td class="text-center" width="12%">
                                              <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".myModal">View Detail</button>
												<div class="modal myModal">
													<div class="modal-dialog')}}">
														<div class="modal-content">
															<div class="modal-header">
															<button type="button" class="close" data-dismiss="modal">&times;</button>
																<div class="popup-top-two">Tristan Mike</div>
			                                                    <div class="table-box">
			                                                    	<table class="table table-striped table-nomargin no-margin">
			                                                    		<thead>
																		    <tr>
																		      <th>Key</th>
																		      <th>Value</th>
																		    </tr>
																		  </thead>
				                                                    	<tbody>
				                                                            <tr>
				                                                                <td width="50%" height="30">Title</td>
				                                                                <td width="50%" height="30">MasterPillow</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Full Name</td>
				                                                                <td width="50%" height="30">TristanMike</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Full Name</td>
				                                                                <td width="50%" height="30">Trisan</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Last Name</td>
				                                                                <td width="50%" height="30">Mike</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Email</td>
				                                                                <td width="50%" height="30">masterpillow@gmail.com</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Phone</td>
				                                                                <td width="50%" height="30">+44 9784 847 8</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Fax</td>
				                                                                <td width="50%" height="30">+44 9784 878 1</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Zip</td>
				                                                                <td width="50%" height="30">STD5</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Address</td>
				                                                                <td width="50%" height="30">82 Grand Ave, Surbiton KT5 9HX, UK</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">City</td>
				                                                                <td width="50%" height="30">Bistin</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Company</td>
				                                                                <td width="50%" height="30">MasterPillow</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Designation</td>
				                                                                <td width="50%" height="30">HR Manager</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Date oF Birth</td>
				                                                                <td width="50%" height="30">May 10, 1984</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Ip Address</td>
				                                                                <td width="50%" height="30">34.145.11.14</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Lead Status</td>
				                                                                <td width="50%" height="30">5</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Created At</td>
				                                                                <td width="50%" height="30">May 31, 2018 02:44 pm
		</td>
				                                                            </tr>
				                                                        </tbody>
				                                                    </table>
			                                                    </div>
															</div>
														</div>
													</div>
												</div>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td><div class="h6">2</div></td>
                                              <td>Tristan Mike</td>
                                              <td>
                                              <img class="img-fluid" src="{{asset('frontend/images/icon-4.jpg')}}">
                                              <span> electromine@controlpanda.com</span></td>
                                              <td> <img class="img-fluid" src="{{asset('frontend/images/icon-5.jpg')}}"> <span>31055511660</span></td>
                                              <td> <img class="img-fluid" src="{{asset('frontend/images/icon-6.jpg')}}"> <span>192.168.0.61</span></td>
                                              <td> <img class="img-fluid" src="{{asset('frontend/images/icon-7.jpg')}}"> <span>indexpage</span></td>
                                              <td><img class="img-fluid" src="{{asset('frontend/images/icon-1.jpg')}}"></td>
                                              <td class="text-center" width="12%">
                                              <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".myModal">View Detail</button>
												<div class="modal myModal">
													<div class="modal-dialog')}}">
														<div class="modal-content">
															<div class="modal-header">
															<button type="button" class="close" data-dismiss="modal">&times;</button>
																<div class="popup-top-two">Tristan Mike</div>
			                                                    <div class="table-box">
			                                                    	<table class="table table-striped table-nomargin no-margin">
			                                                    		<thead>
																		    <tr>
																		      <th>Key</th>
																		      <th>Value</th>
																		    </tr>
																		  </thead>
				                                                    	<tbody>
				                                                            <tr>
				                                                                <td width="50%" height="30">Title</td>
				                                                                <td width="50%" height="30">MasterPillow</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Full Name</td>
				                                                                <td width="50%" height="30">TristanMike</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Full Name</td>
				                                                                <td width="50%" height="30">Trisan</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Last Name</td>
				                                                                <td width="50%" height="30">Mike</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Email</td>
				                                                                <td width="50%" height="30">masterpillow@gmail.com</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Phone</td>
				                                                                <td width="50%" height="30">+44 9784 847 8</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Fax</td>
				                                                                <td width="50%" height="30">+44 9784 878 1</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Zip</td>
				                                                                <td width="50%" height="30">STD5</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Address</td>
				                                                                <td width="50%" height="30">82 Grand Ave, Surbiton KT5 9HX, UK</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">City</td>
				                                                                <td width="50%" height="30">Bistin</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Company</td>
				                                                                <td width="50%" height="30">MasterPillow</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Designation</td>
				                                                                <td width="50%" height="30">HR Manager</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Date oF Birth</td>
				                                                                <td width="50%" height="30">May 10, 1984</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Ip Address</td>
				                                                                <td width="50%" height="30">34.145.11.14</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Lead Status</td>
				                                                                <td width="50%" height="30">5</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Created At</td>
				                                                                <td width="50%" height="30">May 31, 2018 02:44 pm
		</td>
				                                                            </tr>
				                                                        </tbody>
				                                                    </table>
			                                                    </div>
															</div>
														</div>
													</div>
												</div>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td><div class="h6">3</div></td>
                                              <td>Tristan Mike</td>
                                              <td>
                                              <img class="img-fluid" src="{{asset('frontend/images/icon-4.jpg')}}">
                                              <span> electromine@controlpanda.com</span></td>
                                              <td> <img class="img-fluid" src="{{asset('frontend/images/icon-5.jpg')}}"> <span>31055511660</span></td>
                                              <td> <img class="img-fluid" src="{{asset('frontend/images/icon-6.jpg')}}"> <span>192.168.0.61</span></td>
                                              <td> <img class="img-fluid" src="{{asset('frontend/images/icon-7.jpg')}}"> <span>indexpage</span></td>
                                              <td><img class="img-fluid" src="{{asset('frontend/images/icon-1.jpg')}}"></td>
                                              <td class="text-center" width="12%">
                                              <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".myModal">View Detail</button>
												<div class="modal myModal">
													<div class="modal-dialog')}}">
														<div class="modal-content">
															<div class="modal-header">
															<button type="button" class="close" data-dismiss="modal">&times;</button>
																<div class="popup-top-two">Tristan Mike</div>
			                                                    <div class="table-box">
			                                                    	<table class="table table-striped table-nomargin no-margin">
			                                                    		<thead>
																		    <tr>
																		      <th>Key</th>
																		      <th>Value</th>
																		    </tr>
																		  </thead>
				                                                    	<tbody>
				                                                            <tr>
				                                                                <td width="50%" height="30">Title</td>
				                                                                <td width="50%" height="30">MasterPillow</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Full Name</td>
				                                                                <td width="50%" height="30">TristanMike</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Full Name</td>
				                                                                <td width="50%" height="30">Trisan</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Last Name</td>
				                                                                <td width="50%" height="30">Mike</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Email</td>
				                                                                <td width="50%" height="30">masterpillow@gmail.com</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Phone</td>
				                                                                <td width="50%" height="30">+44 9784 847 8</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Fax</td>
				                                                                <td width="50%" height="30">+44 9784 878 1</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Zip</td>
				                                                                <td width="50%" height="30">STD5</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Address</td>
				                                                                <td width="50%" height="30">82 Grand Ave, Surbiton KT5 9HX, UK</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">City</td>
				                                                                <td width="50%" height="30">Bistin</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Company</td>
				                                                                <td width="50%" height="30">MasterPillow</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Designation</td>
				                                                                <td width="50%" height="30">HR Manager</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Date oF Birth</td>
				                                                                <td width="50%" height="30">May 10, 1984</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Ip Address</td>
				                                                                <td width="50%" height="30">34.145.11.14</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Lead Status</td>
				                                                                <td width="50%" height="30">5</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Created At</td>
				                                                                <td width="50%" height="30">May 31, 2018 02:44 pm
		</td>
				                                                            </tr>
				                                                        </tbody>
				                                                    </table>
			                                                    </div>
															</div>
														</div>
													</div>
												</div>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td><div class="h6">4</div></td>
                                              <td>Tristan Mike</td>
                                              <td>
                                              <img class="img-fluid" src="{{asset('frontend/images/icon-4.jpg')}}">
                                              <span> electromine@controlpanda.com</span></td>
                                              <td> <img class="img-fluid" src="{{asset('frontend/images/icon-5.jpg')}}"> <span>31055511660</span></td>
                                              <td> <img class="img-fluid" src="{{asset('frontend/images/icon-6.jpg')}}"> <span>192.168.0.61</span></td>
                                              <td> <img class="img-fluid" src="{{asset('frontend/images/icon-7.jpg')}}"> <span>indexpage</span></td>
                                              <td><img class="img-fluid" src="{{asset('frontend/images/icon-1.jpg')}}"></td>
                                              <td class="text-center" width="12%">
                                              <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".myModal">View Detail</button>
												<div class="modal myModal">
													<div class="modal-dialog')}}">
														<div class="modal-content">
															<div class="modal-header">
															<button type="button" class="close" data-dismiss="modal">&times;</button>
																<div class="popup-top-two">Tristan Mike</div>
			                                                    <div class="table-box">
			                                                    	<table class="table table-striped table-nomargin no-margin">
			                                                    		<thead>
																		    <tr>
																		      <th>Key</th>
																		      <th>Value</th>
																		    </tr>
																		  </thead>
				                                                    	<tbody>
				                                                            <tr>
				                                                                <td width="50%" height="30">Title</td>
				                                                                <td width="50%" height="30">MasterPillow</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Full Name</td>
				                                                                <td width="50%" height="30">TristanMike</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Full Name</td>
				                                                                <td width="50%" height="30">Trisan</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Last Name</td>
				                                                                <td width="50%" height="30">Mike</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Email</td>
				                                                                <td width="50%" height="30">masterpillow@gmail.com</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Phone</td>
				                                                                <td width="50%" height="30">+44 9784 847 8</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Fax</td>
				                                                                <td width="50%" height="30">+44 9784 878 1</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Zip</td>
				                                                                <td width="50%" height="30">STD5</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Address</td>
				                                                                <td width="50%" height="30">82 Grand Ave, Surbiton KT5 9HX, UK</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">City</td>
				                                                                <td width="50%" height="30">Bistin</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Company</td>
				                                                                <td width="50%" height="30">MasterPillow</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Designation</td>
				                                                                <td width="50%" height="30">HR Manager</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Date oF Birth</td>
				                                                                <td width="50%" height="30">May 10, 1984</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Ip Address</td>
				                                                                <td width="50%" height="30">34.145.11.14</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Lead Status</td>
				                                                                <td width="50%" height="30">5</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Created At</td>
				                                                                <td width="50%" height="30">May 31, 2018 02:44 pm
		</td>
				                                                            </tr>
				                                                        </tbody>
				                                                    </table>
			                                                    </div>
															</div>
														</div>
													</div>
												</div>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td><div class="h6">5</div></td>
                                              <td>Tristan Mike</td>
                                              <td>
                                              <img class="img-fluid" src="{{asset('frontend/images/icon-4.jpg')}}">
                                              <span> electromine@controlpanda.com</span></td>
                                              <td> <img class="img-fluid" src="{{asset('frontend/images/icon-5.jpg')}}"> <span>31055511660</span></td>
                                              <td> <img class="img-fluid" src="{{asset('frontend/images/icon-6.jpg')}}"> <span>192.168.0.61</span></td>
                                              <td> <img class="img-fluid" src="{{asset('frontend/images/icon-7.jpg')}}"> <span>indexpage</span></td>
                                              <td><img class="img-fluid" src="{{asset('frontend/images/icon-1.jpg')}}"></td>
                                              <td class="text-center" width="12%">
                                              <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".myModal">View Detail</button>
												<div class="modal myModal">
													<div class="modal-dialog')}}">
														<div class="modal-content">
															<div class="modal-header">
															<button type="button" class="close" data-dismiss="modal">&times;</button>
																<div class="popup-top-two">Tristan Mike</div>
			                                                    <div class="table-box">
			                                                    	<table class="table table-striped table-nomargin no-margin">
			                                                    		<thead>
																		    <tr>
																		      <th>Key</th>
																		      <th>Value</th>
																		    </tr>
																		  </thead>
				                                                    	<tbody>
				                                                            <tr>
				                                                                <td width="50%" height="30">Title</td>
				                                                                <td width="50%" height="30">MasterPillow</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Full Name</td>
				                                                                <td width="50%" height="30">TristanMike</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Full Name</td>
				                                                                <td width="50%" height="30">Trisan</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Last Name</td>
				                                                                <td width="50%" height="30">Mike</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Email</td>
				                                                                <td width="50%" height="30">masterpillow@gmail.com</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Phone</td>
				                                                                <td width="50%" height="30">+44 9784 847 8</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Fax</td>
				                                                                <td width="50%" height="30">+44 9784 878 1</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Zip</td>
				                                                                <td width="50%" height="30">STD5</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Address</td>
				                                                                <td width="50%" height="30">82 Grand Ave, Surbiton KT5 9HX, UK</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">City</td>
				                                                                <td width="50%" height="30">Bistin</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Company</td>
				                                                                <td width="50%" height="30">MasterPillow</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Designation</td>
				                                                                <td width="50%" height="30">HR Manager</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Date oF Birth</td>
				                                                                <td width="50%" height="30">May 10, 1984</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Ip Address</td>
				                                                                <td width="50%" height="30">34.145.11.14</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Lead Status</td>
				                                                                <td width="50%" height="30">5</td>
				                                                            </tr>
				                                                            <tr>
				                                                                <td width="50%" height="30">Created At</td>
				                                                                <td width="50%" height="30">May 31, 2018 02:44 pm
		</td>
				                                                            </tr>
				                                                        </tbody>
				                                                    </table>
			                                                    </div>
															</div>
														</div>
													</div>
												</div>
                                              </td>
                                            </tr>
                                          </table>
                                      </div>
                                </div>
                            </div>
                            <div class="selected-item">
                                <span>Show </span>
                                <select>
                                  <option value="volvo">1</option>
                                  <option value="saab">2</option>
                                  <option value="vw">3</option>
                                  <option value="volvo">4</option>
                                  <option value="saab">5</option>
                                  <option value="volvo">6</option>
                                  <option value="saab">7</option>
                                  <option value="vw">8</option>
                                  <option value="volvo">9</option>
                                  <option value="saab">10</option>
                                  <option value="audi" selected>10</option>
                                </select>
                                <strong>user per page</strong>
                            </div>
              </div>
				</div>
				<div id="menu1" class="tab-pane active">
					<div class="right-content-space fix-width">
					<div class="editor-domain-container-heading')}}">
                                    <h3 class="Duplicate">Connect Domain</h3>
                                </div>
                    <div class="editor-domain-container">
                           <div class="domain-box">
                                <div class="table-responsive">
                                    <table class="table">
                                     <tbody><tr>
                                          <td><div class="h6">1</div></td>
                                          <td style="width:80%;">controlpanda.com</td>
                                          <td class="text-center" style="width:15%;">
                                          <img class="img-fluid" src="{{asset('frontend/images/icon-3.jpg')}}"></td>
                                        </tr>
                                        <tr>
                                          <td><div class="h6">2</div></td>
                                          <td style="width:80%;">controlpanda.com</td>
                                          <td class="text-center" style="width:15%;">
                                          <img class="img-fluid" src="{{asset('frontend/images/icon-3.jpg')}}"></td>
                                        </tr>
                                        <tr>
                                          <td><div class="h6">3</div></td>
                                          <td style="width:80%; color:#7ab428;">controlpanda.com</td>
                                          <td class="text-center" style="width:15%;">
                                          <img class="img-fluid"  src="{{asset('frontend/images/icon-1.jpg')}}"></td>
                                        </tr>
                                        <tr>
                                          <td><div class="h6">4</div></td>
                                          <td style="width:80%;">controlpanda.com</td>
                                          <td class="text-center" style="width:15%;">
                                          <img class="img-fluid"  src="{{asset('frontend/images/icon-3.jpg')}}"></td>
                                        </tr>
                                    </tbody></table>
                                </div>
                            </div>
                        </div>
                        <div class="editor-footer w-100 editor-domain-container-btn">                 
                            <a class="fancy-btn-reverse float-right" href="#" title="">
                                <svg class="fancy-btn-reverse-left" width="466" height="603" viewBox="0 0 100 100" preserveAspectRatio="none">
                                    <path d="M0,0 L100,0 C15,50 50,100 0,100z"></path>
                                </svg>
                                Use Domain
                                <svg class="fancy-btn-reverse-right" width="466" height="603" viewBox="0 0 100 100" preserveAspectRatio="none">
                                    <path d="M0,0 L100,0 C15,50 50,100 0,100z"></path>
                                </svg>
                            </a>
                            <a href="{{url('admin/dashboard')}}" class="btn-preview float-right">Preview</a>
                        </div> 
                        </div>           
				</div>
				<div id="menu2" class="tab-pane fade">
					<div class="right-content-space fix-width">
					<div class="editor-rename-container-heading')}}">
                        <h3 class="Duplicate">Rename Site</h3></div>
                        <div class="editor-rename-container">
                            <div class="objective-wrap">
                             <div class="Rename-box">
                                 <form action="#"> Current Name<br>
           <input type="text" name="firstname" class="bg-primary" placeholder="Happy Life Hacks"><br>
                                     New Site Name
           <input type="text" name="firstname" placeholder="Life Tricks">
                                </form></div>
                            </div>
                        </div>
                        <div class="editor-footer w-100 editor-rename-container-btn">                 
                            <a class="fancy-btn-reverse float-right" href="#" title="">
                                <svg class="fancy-btn-reverse-left" width="466" height="603" viewBox="0 0 100 100" preserveAspectRatio="none">
                                    <path d="M0,0 L100,0 C15,50 50,100 0,100z"></path>
                                </svg>
                                Save
                                <svg class="fancy-btn-reverse-right" width="466" height="603" viewBox="0 0 100 100" preserveAspectRatio="none">
                                    <path d="M0,0 L100,0 C15,50 50,100 0,100z"></path>
                                </svg>
                            </a>
                        </div>
                    </div>
				</div>
				<div id="menu3" class="tab-pane fade">
					<div class="right-content-space fix-width">
					<div class="editor-duplicate-container-heading')}}"><h3 class="Duplicate">Duplicate Site</h3></div>
					<div class="editor-duplicate-container">
                            <div class="objective-wrap">
                             <div class="duplicate-box">
                                 <form action="#"> Enter Duplicate Site Name<br>
           <input type="text" name="firstname" placeholder="Happy Life Hacks">
                                </form></div>
                            </div>
                        </div>
                        <div class="editor-footer w-100 editor-duplicate-container-btn">                 
                            <a class="fancy-btn-reverse float-right" href="#" title="">
                                <svg class="fancy-btn-reverse-left" width="466" height="603" viewBox="0 0 100 100" preserveAspectRatio="none">
                                    <path d="M0,0 L100,0 C15,50 50,100 0,100z"></path>
                                </svg>
                                Confirm
                                <svg class="fancy-btn-reverse-right" width="466" height="603" viewBox="0 0 100 100" preserveAspectRatio="none">
                                    <path d="M0,0 L100,0 C15,50 50,100 0,100z"></path>
                                </svg>
                            </a>
                        </div>
                    </div>
				</div>
				<div class="tab-pane">
					
				</div>
				<div id="menu5" class="tab-pane fade">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-4">
							<div class="left-menu">
								<div class="heading-title">Settings</div>
								<ul class="nav nav-tabs left-panel-menu">
									<li><a data-toggle="tab" href="#setting-tab-1" class="active show">Email Settings</a></li>
									<li><a data-toggle="tab" href="#setting-tab-2" class="">Email List Integrations</a></li>
									<li><a data-toggle="tab" href="#setting-tab-3" class="">SMS Settings</a></li>
									<li><a data-toggle="tab" href="#setting-tab-4" class="">Domain Settings</a></li>
									<li><a data-toggle="tab" href="#setting-tab-5" class="">Tracking Codes</a></li>
								</ul>
							</div>
						</div>
						<div class="col-lg-9 col-md-9 col-sm-8">
						<div class="tab-content">
							<div id="setting-tab-1" class="tab-pane active in">
								<div class="email-setting')}}">
									<div class="email-setting-head">Email Settings <p>(Enter the email address of all that will receive the enquiriess contact details.)</p></div>
									<div class="editor-dashboard-container dashboard-Contact-box">
										<div class="table-responsive">
											<table class="table">
												<tbody>
													<tr>
		                                              <td width="5%"><div class="h6">1</div></td>
		                                              <td width="30%">
		                                              	<form action="#"><input type="text" name="firstname" placeholder="helloworldlead1445@qqmail.com"></form>
		                                              </td>
		                                              <td width="30%" class="text-right"><a href="#"><i class="fas fa-minus-circle"></i> Remove Email</a></td>
		                                            </tr>
		                                            <tr>
		                                              <td width="5%"><div class="h6">2</div></td>
		                                              <td width="30%">
		                                              	<form action="#"><input type="text" name="firstname" placeholder="mastermike11@gmail.com"></form>
		                                              </td>
		                                              <td width="30%" class="text-right"><a href="#"><i class="fas fa-minus-circle"></i> Remove Email</a></td>
		                                            </tr>
		                                            <tr>
		                                              <td width="5%"><div class="h6">3</div></td>
		                                              <td width="30%">
		                                              	<form action="#"><input type="text" name="firstname" placeholder="slaem_busines_marchi@gmail.com"></form>
		                                              </td>
		                                              <td width="30%" class="text-right"><a href="#"><i class="fas fa-minus-circle"></i> Remove Email</a></td>
		                                            </tr>
		                                            <tr>
		                                              <td width="5%"><div class="h6">4</div></td>
		                                              <td width="30%">
		                                              	<form action="#"><input type="text" name="firstname" placeholder="guliz_readydo447@gmail.com"></form>
		                                              </td>
		                                              <td width="30%" class="text-right"><a href="#"><i class="fas fa-minus-circle"></i> Remove Email</a></td>
		                                            </tr>
		                                            <tr>
		                                            	<td width="8%"></td>
		                                            	<td><a href="#"><button type="button" class="btn btn-primary">
		                                            	<i class="fas fa-plus-circle"></i> Add More Email </button></a></td>
		                                            </tr>
												</tbody>
											</table>
										</div>
									</div>
									<div class="editor-footer w-100 editor-duplicate-container-btn">                 
			                            <a class="fancy-btn-reverse float-right" href="#" title="">
			                                <svg class="fancy-btn-reverse-left" width="466" height="603" viewBox="0 0 100 100" preserveAspectRatio="none">
			                                    <path d="M0,0 L100,0 C15,50 50,100 0,100z"></path>
			                                </svg>
			                                Confirm Email Address
			                                <svg class="fancy-btn-reverse-right" width="466" height="603" viewBox="0 0 100 100" preserveAspectRatio="none">
			                                    <path d="M0,0 L100,0 C15,50 50,100 0,100z"></path>
			                                </svg>
			                            </a>
			                        </div>
								</div>
							</div>
							<div id="setting-tab-2" class="tab-pane fade">
								<div class="integrations-code">
									<h3 class="">Email List Integrations</h3>
									<div class="editor-duplicate-container">
			                            <div class="integrations-wrap">
			                            	<div class="integrations-head">Action</div>
			                            	<select>
			                            		<option value="Volvo">Add to List</option>
			                            		<option value="Volvo">Add to List</option>
			                            	</select>
			                            	<div class="integrations-head">Select List</div>
			                            	<select>
			                            		<option value="Volvo">Select</option>
			                            		<option value="Volvo">Select</option>
			                            	</select>
			                            	<div class="integrations-head">Pages</div>
			                            	<select>
			                            		<option value="Volvo">Gallery</option>
			                            		<option value="Volvo">Gallery</option>
			                            	</select>
			                            </div>
									</div>
									<div class="editor-footer w-100 editor-duplicate-container-btn">                 
			                            <a class="fancy-btn-reverse float-right" href="#" title="">
			                                <svg class="fancy-btn-reverse-left" width="466" height="603" viewBox="0 0 100 100" preserveAspectRatio="none">
			                                    <path d="M0,0 L100,0 C15,50 50,100 0,100z"></path>
			                                </svg>
			                                Save
			                                <svg class="fancy-btn-reverse-right" width="466" height="603" viewBox="0 0 100 100" preserveAspectRatio="none">
			                                    <path d="M0,0 L100,0 C15,50 50,100 0,100z"></path>
			                                </svg>
			                            </a>
			                        </div>
								</div>
							</div>
							<div id="setting-tab-3" class="tab-pane fade">
								<div class="sms-settings">
									<div class="email-setting-head">SMS Settings <p>(Enter the mobile phone number of all that will receive the enquiries contact details.)</p></div>
									<div class="editor-dashboard-container dashboard-Contact-box">
										<div class="table-responsive">
											<table class="table">
												<tbody>
													<tr>
		                                              <td width="5%"><div class="h6">1</div></td>
		                                              <td width="30%">
		                                              	<form action="#"><input type="text" name="firstname" placeholder="+92 123 4567 86"></form>
		                                              </td>
		                                              <td width="30%" class="text-right"><a href="#"><i class="fas fa-minus-circle"></i> Remove Number</a></td>
		                                            </tr>
		                                            <tr>
		                                              <td width="5%"><div class="h6">2</div></td>
		                                              <td width="30%">
		                                              	<form action="#"><input type="text" name="firstname" placeholder="+94 123 4567 98"></form>
		                                              </td>
		                                              <td width="30%" class="text-right"><a href="#"><i class="fas fa-minus-circle"></i> Remove Number</a></td>
		                                            </tr>
		                                            <tr>
		                                              <td width="5%"><div class="h6">3</div></td>
		                                              <td width="30%">
		                                              	<form action="#"><input type="text" name="firstname" placeholder="+96 123 4567 00"></form>
		                                              </td>
		                                              <td width="30%" class="text-right"><a href="#"><i class="fas fa-minus-circle"></i> Remove Number</a></td>
		                                            </tr>
		                                            <tr>
		                                              <td width="5%"><div class="h6">4</div></td>
		                                              <td width="30%">
		                                              	<form action="#"><input type="text" name="firstname" placeholder="+98 123 4567 11"></form>
		                                              </td>
		                                              <td width="30%" class="text-right"><a href="#"><i class="fas fa-minus-circle"></i> Remove Number</a></td>
		                                            </tr>
		                                            <tr>
		                                              <td width="5%"><div class="h6">5</div></td>
		                                              <td width="30%">
		                                              	<form action="#"><input type="text" name="firstname" placeholder="+99 123 4567 22"></form>
		                                              </td>
		                                              <td width="30%" class="text-right"><a href="#"><i class="fas fa-minus-circle"></i> Remove Number</a></td>
		                                            </tr>
		                                            <tr>
		                                              <td width="5%"><div class="h6">6</div></td>
		                                              <td width="30%">
		                                              	<form action="#"><input type="text" name="firstname" placeholder="+99 123 4567 22"></form>
		                                              </td>
		                                              <td width="30%" class="text-right"><a href="#"><i class="fas fa-minus-circle"></i> Remove Number</a></td>
		                                            </tr>
		                                            <tr>
		                                            	<td width="8%"></td>
		                                            	<td><a href="#"><button type="button" class="btn btn-primary">
		                                            	<i class="fas fa-plus-circle"></i> Add More Email </button></a></td>
		                                            </tr>
												</tbody>
											</table>
										</div>
									</div>
									<div class="editor-footer w-100 editor-duplicate-container-btn">                 
			                            <a class="fancy-btn-reverse float-right" href="#" title="">
			                                <svg class="fancy-btn-reverse-left" width="466" height="603" viewBox="0 0 100 100" preserveAspectRatio="none">
			                                    <path d="M0,0 L100,0 C15,50 50,100 0,100z"></path>
			                                </svg>
			                                Confirm Mobile Number
			                                <svg class="fancy-btn-reverse-right" width="466" height="603" viewBox="0 0 100 100" preserveAspectRatio="none">
			                                    <path d="M0,0 L100,0 C15,50 50,100 0,100z"></path>
			                                </svg>
			                            </a>
			                        </div>
								</div>
							</div>
							<div id="setting-tab-4" class="tab-pane fade">
								<div class="domain-code">
									<h3 class="">Domain Settings</h3>
									<div class="editor-duplicate-container">
			                            <div class="domain-wrap">
			                            	<div class="domain-head">Domain</div>
			                            	<select>
			                            		<option value="Volvo">Choose a domain</option>
			                            	</select>
			                            	<div class="domain-head">Domain Status</div>
			                            	<select>
			                            		<option value="Volvo">Deactive</option>
			                            	</select>
			                            	<div class="domain-head">Assign Website</div>
			                            	<select>
			                            		<option value="Volvo">Select</option>
			                            	</select>
			                            </div>
									</div>
									<div class="editor-footer w-100 editor-duplicate-container-btn">
			                            <a class="fancy-btn-reverse float-right" href="#" title="">
			                                <svg class="fancy-btn-reverse-left" width="466" height="603" viewBox="0 0 100 100" preserveAspectRatio="none">
			                                    <path d="M0,0 L100,0 C15,50 50,100 0,100z"></path>
			                                </svg>
			                                Update
			                                <svg class="fancy-btn-reverse-right" width="466" height="603" viewBox="0 0 100 100" preserveAspectRatio="none">
			                                    <path d="M0,0 L100,0 C15,50 50,100 0,100z"></path>
			                                </svg>
			                            </a>
			                        </div>
								</div>
							</div>
							<div id="setting-tab-5" class="tab-pane fade">
								<div class="tracking-code">
								<h3 class="">Tracking Codes</h3>
								<div class="editor-duplicate-container">
		                            <div class="tracking-wrap">
		                             	<h2>Head Tracking Code</h2>
		                             	<div class="tracking-box">
		                             		<p>var pageTracker = _gat._geacker._trackPtT sd rac-<br> ker('UA-XXXXX-X');<br> pageTracker._trackPageview();</p>
		                             	</div>
		                             	<h6>Control Panda wide tracking code for the head tag</h6>
		                             	<h2>Footer Tracking Code</h2>
		                             	<div class="tracking-box">
		                             		<p>var pageTracker = _gat._geacker._trackPtT sd rac-<br> ker('UA-XXXXX-X');<br> pageTracker._trackPageview();</p>
		                             	</div>
		                             	<h6>Control Panda wide tracking codes for the body tag</h6>
		                            </div>
								</div>
								<div class="editor-footer w-100 editor-duplicate-container-btn">                 
		                            <a class="fancy-btn-reverse float-right" href="#" title="">
		                                <svg class="fancy-btn-reverse-left" width="466" height="603" viewBox="0 0 100 100" preserveAspectRatio="none">
		                                    <path d="M0,0 L100,0 C15,50 50,100 0,100z"></path>
		                                </svg>
		                                Save
		                                <svg class="fancy-btn-reverse-right" width="466" height="603" viewBox="0 0 100 100" preserveAspectRatio="none">
		                                    <path d="M0,0 L100,0 C15,50 50,100 0,100z"></path>
		                                </svg>
		                            </a>
		                        </div>
							</div>
						</div>
					</div>
					</div>
				</div>
			</div>
			<div id="menu6" class="tab-pane fade">
				<div class="right-content-space">
				<div class="editor-seo-container-heading')}}">
                                    <h3 class="">SEO </h3><p>(How you optimize your site so that it can be easily found and ranked by search engines like Google and more.)</p></div>
                    <div class="editor-seo-container">
                        <div class="seo-box">
                            <h4>SEO Status</h4>
                            <span>To get traffic from search engines to your site, this feature must be on.</span>
                            <div class="switch-buttons">
                             <div class="form-group">  
                                  <span class="switch switch-sm">
                                    <input type="checkbox" class="switch" id="switch-sm">
                                    <label for="switch-sm"></label>
                                    <p>Allow search engines to include your site in search results</p>
                                  </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="editor-seo-container-two">
                     <h4>Get Found on Google<br>
                        <span>Boost your site’s SEO with Wix SEO Wiz. Head to your step-by-step plan now.</span>
                        <a href="Website-dashboard-Seo-letsGo.html" target="_blank"><button type="button" class="btn btn-primary">Let's Go</button></a></h4>
                    </div>
                    <div class="editor-seo-container-heading')}}">
                    <h3 class="">Manage 301 Redirects</h3></div>
                    <div class="editor-seo-container-three">
                     <div class="jumbotron"><h3>A 301 redirect is how visitors and search engines automatically find your new websites and pages.</h3></div>
                        <div class="inner-box">
                         <div class="left-side">
                             <h3>What is 301 Redirect?</h3>
                                <span>A 301 redirect simply tells search engines, “we’ve moved to a new location.” Old site links get automatically forwarded to your new Panda pages, (e.g., the link to your old About page will lead to your new About page.</span>
                                <div style="margin-top:20px;">
                                 <h3>How does it work?</h3>
                                <span>When a visitor or search engine goes to your old address, they’ll automatically be sent to your new page. Visitors will find your new pages and search engines will keep your ranking.</span>
                                </div>
                            </div>
                            <div class="right-side text-center">
                            <img class="img-fluid" src="{{asset('frontend/images/save-icon-1.jpg')}}"></div>
                        </div>
                    </div>
                    <div class="editor-seo-container-four">
                     <h4>Redirect pages from your old site to your Panda site now
                        <button type="button" class="btn btn-primary dropdown-toggle">Let's Go</button>
                        <ul class="dropdown">
								<div class="jumbotron"><h4>Create a 301 redirect: Add the old page address on the left and choose your new control panda page on the right.</h4></div>
                            <div class="page-info">
                            	<div class="row">
                            		<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
                                	<h6>Old Page:</h6>
                                    <form action="#">
                                        <input type="text" name="firstname" class="btn-fild" placeholder="/about-us">
                                    </form>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
                                	<h6>Redirect to:</h6>
                                    <form action="#">
                                        <input type="text" name="firstname" class="btn-fild" placeholder="Select Page">
                                    </form>
                                </div>
	                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
	                                	<button type="button" class="btn btn-info">Save</button></div>
	                            	</div>
                                	<span>Don’t see your page?
                                	<strong><a href="#" data-toggle="modal" data-target=".myModal-pop">Add it manually</a>
                                		<div class="modal myModal-pop">
										    <div class="modal-dialog')}}">
										      <div class="modal-content">
										        <div class="modal-header">
										          <button type="button" class="close" data-dismiss="modal">&times;</button>
										          <div class="popup-top">Enter Page URL</div>
										          <div class="popup-bottom">
                                                    	<div class="jumbotron">
                                                        	<h3>Add the URL of your old site and redirect it to the URL of your new ControlPanda site</h3>
                                                            <div class="row">
                                                            	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <h6>Old Page:</h6>
	                                                                <form action="#">
	                                                                    <input type="text" name="firstname" class="btn-fild" placeholder="/about-us">
	                                                                </form>
	                                                            </div>
	                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
	                                                                <h6>Redirect to:</h6>
	                                                                <form action="#">
	                                                                    <input type="text" name="firstname" class="btn-fild" placeholder="Select Page">
	                                                                </form>
	                                                            </div>
                                                            </div>
                                                            <div class="btn-group">
                                                            	<button type="button" class="btn btn-primary">Save</button>
                                                            	<button type="button" class="btn btn-info">Cancel</button>
                                                            </div>
                                                        </div>
                                                    </div>
										        </div>
										      </div>
										    </div>
										  </div>
                                	</strong></span>
                                </div>
                                <div class="text-note">Note: Your 301 redirects will not work until you connect a domain.
                                <q>Connect Domain</q></div>
                            </div>
                        </ul>

                    </h4>
                    </div>
			</div>
		</div>
		


</section>
</div>



@endsection