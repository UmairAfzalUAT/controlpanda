@extends('admin.layouts.app')
@section('content')
<section>
<?php //echo '<pre>'; print_r($last_project[0]);  die();
global $ids;
$ids = $last_project[0]->id;
?>
    <div class="left-panel-control" id="left-panel-open">
        <div id="site-menu">
            <div class="left-panel-heading float-left w-100 left-panel-inner-space mb-10">
                <div class="pt-15 ft-16">Site Name: 
                    <small>
                        @if(!empty($last_project[0]->site_name))
                            {{$last_project[0]->site_name}} 
                        @else 
                            Not available 
                        @endif 
                    </small>
                </div>
                @if($last_project[0]->published == 1)
                    <span class="badge badge-success float-right">Published</span>
                @else 
                    <span class="badge badge-success float-right">Not Published</span>
                @endif 
                
            </div>
            <div class="left-panel-inner-space">
                
                <div class="avatar-web">
                    <img src="{!! asset('storage/projects/'.Auth::user()->id.'/'.$last_project[0]->uuid.'/thumbnail.png') !!}" />
                </div>
            </div>          
            <div class="left-panel-btn-control">
                <a href="{!!url('sites/'.$last_project[0]->name)!!}" class="left">             
                    <span class="icon float-right">
                        <img src="{{asset('assets/images/access.svg')}}" />
                    </span>
                    <span class="float-right">
                        Access
                    </span>
                </a>
                <a href="{!!url('admin/panda-pages/'.$last_project[0]->id.'/edit')!!}" class="right">                
                    <span class="icon float-left">
                        <img src="{{asset('assets/images/setting.svg')}}" />
                    </span>
                    <span class="float-left">Edit Site</span>
                </a>
            </div>
            <div class="left-panel-heading mt-40 float-left w-100 left-panel-inner-space">
                <div><span class="color-red ft-16">Plan:</span> <small>{{$last_project[0]->package_title}}</small></div>
                <a href="{{url('admin/upgrade-account')}}" class="badge float-right color-purple ft-12">Compare</a>              
            </div>
            <div class="left-panel-heading mt-15  float-left w-100 left-panel-inner-space">             
                <div><span class="color-red ft-16">Domain:</span> 
					@if(!empty($last_project[0]->domain_name))
						<small>Connected: {{$last_project[0]->domain_name}}</small>
					@else 
						<small>Not Connected</small>
					@endif 
				</div>
				@if(empty($last_project[0]->domain_name))
					<a  href="#" class="badge float-right color-purple ft-12" data-toggle="modal" data-target="#connect_domains">Connect Domain</a>
				@endif 
                
            </div>
            <a class="fancy-btn btn-left-panel-btm" href="{{url('admin/upgrade-account')}}" title="">
                <svg class="fancy-btn-left" width="466" height="603" viewbox="0 0 100 100" preserveAspectRatio="none">
                <path d="M0,0 L100,0 C15,50 50,100 0,100z"/>
                </svg>
                Upgrade
                <svg class="fancy-btn-right" width="466" height="603" viewbox="0 0 100 100" preserveAspectRatio="none">
                <path d="M0,0 L100,0 C15,50 50,100 0,100z"/>
                </svg>
            </a>
        </div>

        <a href="#" class="toggle-nav btn btn-purple" style="float: right" id="big-sexy"><i class="fas fa-angle-right"></i></a>
    </div>

    <div class="right-panel">
        <h2>Account Dashboard</h2>
        <div class="row">
            @if(have_premission(1))
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6">
                <a href="{{url('/admin/panda-pages')}}" class="dashboard-component-controll">
                    <div class="dashboard-component">
                        <img src="{{asset('assets/images/website-icon.png')}}" />
                        <h4>Websites</h4>
                        <p>Manage your website</p>
                    </div>
                </a>
            </div>
            @endif
            @if(have_premission(50))
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6">
                <a href="{{url('admin/domains')}}" class="dashboard-component-controll">
                    <div class="dashboard-component">
                        <img src="{{asset('assets/images/domain-icon.png')}}" />
                        <h4>Domains</h4>
                        <p>Manage your domains</p>
                    </div>
                </a>
            </div>
            @endif
            @if(have_premission(12))
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6">
                <a href="{{url('/admin/contacts')}}" class="dashboard-component-controll">
                    <div class="dashboard-component">
                        <img src="{{asset('assets/images/contact-icon.png')}}" />
                        <h4>Contacts</h4>
                        <p>Manage your contacts</p>
                    </div>
                </a>
            </div>
            @endif
            @if(have_premission(20))
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6">
                <a href="{{url('/admin/contacts/emails/'.$ids.'/lists')}}" class="dashboard-component-controll">
                    <div class="dashboard-component">
                        <img src="{{asset('assets/images/email-icon.png')}}" />
                        <h4>Email</h4>
                        <p>Manage your customers</p>
                    </div>
                </a>
            </div>
            @endif
            @if(have_premission(24))
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6">
                <a href="{{url('/admin/panda-sms')}}" class="dashboard-component-controll">
                    <div class="dashboard-component">
                        <img src="{{asset('assets/images/sms-icon.png')}}" />
                        <h4>SMS</h4>
                        <p>Text your customers</p>
                    </div>
                </a>
            </div>
            @endif
            @if(have_premission(28))
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6">
                <a href="{{url('/admin/external-integrations')}}" class="dashboard-component-controll">
                    <div class="dashboard-component">
                        <img src="{{asset('assets/images/integeration-icon.png')}}" />
                        <h4>Integerations</h4>
                        <p>Set up custom integrations</p>
                    </div>
                </a>
            </div>
            @endif
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6">
                <a href="{{url('support')}}" class="dashboard-component-controll">
                    <div class="dashboard-component">
                        <img src="{{asset('assets/images/support-icon.png')}}" />
                        <h4>Support</h4>
                        <p>Contact our support team</p>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6">
                <a href="{{url('admin/upgrade-account')}}" class="dashboard-component-controll">
                    <div class="dashboard-component">
                        <img src="{{asset('assets/images/Upgrade-icon.png')}}" />
                        <h4>Upgrade</h4>
                        <p>Upgrade & compare plans</p>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div id="connect_domains" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Choose domain</h4>
      </div>
      <div class="modal-body">
        <p>
			<form method="" action="" class="">
				<div class="form-group">
					<select class="form-control" name="" required>
						<option value="" >Select Domain</option>
						@foreach($domains as $domain)
							<option value="{{$domain->id}}" >{{$domain->name}}</option>
						@endforeach
					</select>
				</div>
			</form>
		</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
@endsection