<?php 

return array(
	'name' => 'Yogafit',
	'color' => 'Orange',
	'category' => 'Gym - Yoga',
	'theme'  => 'Gym/Fitness',
	'libraries' => array('Classie', 'Animated Header', 'Bootstrap Validation', 'Jquery Easing'),
);