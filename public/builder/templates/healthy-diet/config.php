<?php 

return array(
	'name' => 'Healthy Diet',
	'color' => 'Blue',
	'category' => 'Weightloss ',
	'theme'  => 'Dietician',
	'libraries' => array('Classie', 'Animated Header', 'Bootstrap Validation', 'Jquery Easing'),
);