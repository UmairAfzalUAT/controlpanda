<?php 

return array(
	'name' => 'Handyman & services',
	'color' => 'yellow',
	'category' => 'Tradesman',
	'theme'  => 'Handyman',
	'libraries' => array('Classie', 'Animated Header', 'Bootstrap Validation', 'Jquery Easing'),
);