<?php 

return array(
	'name' => 'Primetime Newspaper',
	'color' => 'Black',
	'category' => 'Newspaper Blog',
	'theme'  => 'Primetime Newspaper',
	'libraries' => array('Classie', 'Animated Header', 'Bootstrap Validation', 'Jquery Easing'),
);