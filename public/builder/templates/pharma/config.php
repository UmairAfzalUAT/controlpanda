<?php 

return array(
	'name' => 'Health Services',
	'color' => 'Blue',
	'category' => 'Pharma',
	'theme'  => 'Health Services',
	'libraries' => array('Classie', 'Animated Header', 'Bootstrap Validation', 'Jquery Easing'),
);