<?php 

return array(
	'name' => 'Plumbing Expert   ',
	'color' => 'Yellow',
	'category' => 'Tradesman ',
	'theme'  => 'Plumbing Expert',
	'libraries' => array('Classie', 'Animated Header', 'Bootstrap Validation', 'Jquery Easing'),
);