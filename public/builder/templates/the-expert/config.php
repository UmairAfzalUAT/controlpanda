<?php 

return array(
	'name' => 'The Expert',
	'color' => 'yellow',
	'category' => 'Digital Agency',
	'theme'  => 'Agency One Page',
	'libraries' => array('Classie', 'Animated Header', 'Bootstrap Validation', 'Jquery Easing'),
);