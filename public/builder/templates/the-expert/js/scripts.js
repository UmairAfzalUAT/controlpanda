(function () {
	function isTouchDevice() {
		return (('ontouchstart' in window) || (navigator.MaxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0));
	}
	window.gemSettings.isTouch = isTouchDevice();

	function userAgentDetection() {
		var ua = navigator.userAgent.toLowerCase(),
			platform = navigator.platform.toLowerCase(),
			UA = ua.match(/(opera|ie|firefox|chrome|version)[\s\/:]([\w\d\.]+)?.*?(safari|version[\s\/:]([\w\d\.]+)|$)/) || [null, 'unknown', 0],
			mode = UA[1] == 'ie' && document.documentMode;
		window.gemBrowser = {
			name: (UA[1] == 'version') ? UA[3] : UA[1],
			version: UA[2],
			platform: {
				name: ua.match(/ip(?:ad|od|hone)/) ? 'ios' : (ua.match(/(?:webos|android)/) || platform.match(/mac|win|linux/) || ['other'])[0]
			}
		};
	}
	window.updateGemClientSize = function () {
		if (window.gemOptions == null || window.gemOptions == undefined) {
			window.gemOptions = {
				first: false,
				clientWidth: 0,
				clientHeight: 0,
				innerWidth: -1
			};
		}
		window.gemOptions.clientWidth = window.innerWidth || document.documentElement.clientWidth;
		if (document.body != null && !window.gemOptions.clientWidth) {
			window.gemOptions.clientWidth = document.body.clientWidth;
		}
		window.gemOptions.clientHeight = window.innerHeight || document.documentElement.clientHeight;
		if (document.body != null && !window.gemOptions.clientHeight) {
			window.gemOptions.clientHeight = document.body.clientHeight;
		}
	};
	window.updateGemInnerSize = function (width) {
		window.gemOptions.innerWidth = width != undefined ? width : (document.body != null ? document.body.clientWidth : 0);
	};
	userAgentDetection();
	window.updateGemClientSize(true);
	window.gemSettings.lasyDisabled = window.gemSettings.forcedLasyDisabled || (!window.gemSettings.mobileEffectsEnabled && (window.gemSettings.isTouch || window.gemOptions.clientWidth <= 800));
})();;
(function () {
	var fullwithData = {
		page: null,
		pageWidth: 0,
		pageOffset: {},
		fixVcRow: true,
		pagePaddingLeft: 0
	};

	function updateFullwidthData() {
		fullwithData.pageOffset = fullwithData.page.getBoundingClientRect();
		fullwithData.pageWidth = parseFloat(fullwithData.pageOffset.width);
		fullwithData.pagePaddingLeft = 0;
		if (fullwithData.page.className.indexOf('vertical-header') != -1) {
			fullwithData.pagePaddingLeft = 45;
			if (fullwithData.pageWidth >= 1600) {
				fullwithData.pagePaddingLeft = 360;
			}
			if (fullwithData.pageWidth < 980) {
				fullwithData.pagePaddingLeft = 0;
			}
		}
	}

	function gem_fix_fullwidth_position(element) {
		if (element == null) {
			return false;
		}
		if (fullwithData.page == null) {
			fullwithData.page = document.getElementById('page');
			updateFullwidthData();
		}
		if (fullwithData.pageWidth < 1170) {
			return false;
		}
		if (!fullwithData.fixVcRow) {
			return false;
		}
		if (element.previousElementSibling != null && element.previousElementSibling != undefined && element.previousElementSibling.className.indexOf('fullwidth-block') == -1) {
			var elementParentViewportOffset = element.previousElementSibling.getBoundingClientRect();
		} else {
			var elementParentViewportOffset = element.parentNode.getBoundingClientRect();
		}
		if (elementParentViewportOffset.top > window.gemOptions.clientHeight) {
			fullwithData.fixVcRow = false;
			return false;
		}
		if (element.className.indexOf('vc_row') != -1) {
			var elementMarginLeft = -21;
			var elementMarginRight = -21;
		} else {
			var elementMarginLeft = 0;
			var elementMarginRight = 0;
		}
		var offset = parseInt(fullwithData.pageOffset.left + 0.5) - parseInt((elementParentViewportOffset.left < 0 ? 0 : elementParentViewportOffset.left) + 0.5) - elementMarginLeft + fullwithData.pagePaddingLeft;
		var offsetKey = window.gemSettings.isRTL ? 'right' : 'left';
		element.style.position = 'relative';
		element.style[offsetKey] = offset + 'px';
		element.style.width = fullwithData.pageWidth - fullwithData.pagePaddingLeft + 'px';
		if (element.className.indexOf('vc_row') == -1) {
			element.setAttribute('data-fullwidth-updated', 1);
		}
		if (element.className.indexOf('vc_row') != -1 && !element.hasAttribute('data-vc-stretch-content')) {
			var el_full = element.parentNode.querySelector('.vc_row-full-width-before');
			var padding = -1 * offset;
			0 > padding && (padding = 0);
			var paddingRight = fullwithData.pageWidth - padding - el_full.offsetWidth + elementMarginLeft + elementMarginRight;
			0 > paddingRight && (paddingRight = 0);
			element.style.paddingLeft = padding + 'px';
			element.style.paddingRight = paddingRight + 'px';
		}
	}
	window.gem_fix_fullwidth_position = gem_fix_fullwidth_position;
	if (window.gemSettings.isTouch) {
		setTimeout(function () {
			var head = document.getElementsByTagName('head')[0],
				link = document.createElement('link');
			link.rel = 'stylesheet';
			link.type = 'text/css';
			link.href = window.gemSettings.themePath + '/css/thegem-touch.css';
			head.appendChild(link);
		}, 1000);
	}
	if (window.gemSettings.lasyDisabled && !window.gemSettings.forcedLasyDisabled) {
		setTimeout(function () {
			var head = document.getElementsByTagName('head')[0],
				link = document.createElement('link');
			link.rel = 'stylesheet';
			link.type = 'text/css';
			link.href = window.gemSettings.themePath + '/css/thegem-effects-disabled.css';
			head.appendChild(link);
		}, 1000);
	}
	if (window.gemSettings.parallaxDisabled) {
		var head = document.getElementsByTagName('head')[0],
			link = document.createElement('style');
		link.rel = 'stylesheet';
		link.type = 'text/css';
		link.innerHTML = ".fullwidth-block.fullwidth-block-parallax-vertical .fullwidth-block-background, .fullwidth-block.fullwidth-block-parallax-fixed .fullwidth-block-background { background-attachment: scroll !important; }";
		head.appendChild(link);
	}
})();
(function () {
	setTimeout(function () {
		var preloader = document.getElementById('page-preloader');
		if (preloader != null && preloader != undefined) {
			preloader.className += ' preloader-loaded';
		}
	}, window.pagePreloaderHideTime || 1000);
})();;
/*! jQuery v1.12.4 | (c) jQuery Foundation | jquery.org/license */
! function (a, b) {
	"object" == typeof module && "object" == typeof module.exports ? module.exports = a.document ? b(a, !0) : function (a) {
		if (!a.document) throw new Error("jQuery requires a window with a document");
		return b(a)
	} : b(a)
}("undefined" != typeof window ? window : this, function (a, b) {
	var c = [],
		d = a.document,
		e = c.slice,
		f = c.concat,
		g = c.push,
		h = c.indexOf,
		i = {},
		j = i.toString,
		k = i.hasOwnProperty,
		l = {},
		m = "1.12.4",
		n = function (a, b) {
			return new n.fn.init(a, b)
		},
		o = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
		p = /^-ms-/,
		q = /-([\da-z])/gi,
		r = function (a, b) {
			return b.toUpperCase()
		};
	n.fn = n.prototype = {
		jquery: m,
		constructor: n,
		selector: "",
		length: 0,
		toArray: function () {
			return e.call(this)
		},
		get: function (a) {
			return null != a ? 0 > a ? this[a + this.length] : this[a] : e.call(this)
		},
		pushStack: function (a) {
			var b = n.merge(this.constructor(), a);
			return b.prevObject = this, b.context = this.context, b
		},
		each: function (a) {
			return n.each(this, a)
		},
		map: function (a) {
			return this.pushStack(n.map(this, function (b, c) {
				return a.call(b, c, b)
			}))
		},
		slice: function () {
			return this.pushStack(e.apply(this, arguments))
		},
		first: function () {
			return this.eq(0)
		},
		last: function () {
			return this.eq(-1)
		},
		eq: function (a) {
			var b = this.length,
				c = +a + (0 > a ? b : 0);
			return this.pushStack(c >= 0 && b > c ? [this[c]] : [])
		},
		end: function () {
			return this.prevObject || this.constructor()
		},
		push: g,
		sort: c.sort,
		splice: c.splice
	}, n.extend = n.fn.extend = function () {
		var a, b, c, d, e, f, g = arguments[0] || {},
			h = 1,
			i = arguments.length,
			j = !1;
		for ("boolean" == typeof g && (j = g, g = arguments[h] || {}, h++), "object" == typeof g || n.isFunction(g) || (g = {}), h === i && (g = this, h--); i > h; h++)
			if (null != (e = arguments[h]))
				for (d in e) a = g[d], c = e[d], g !== c && (j && c && (n.isPlainObject(c) || (b = n.isArray(c))) ? (b ? (b = !1, f = a && n.isArray(a) ? a : []) : f = a && n.isPlainObject(a) ? a : {}, g[d] = n.extend(j, f, c)) : void 0 !== c && (g[d] = c));
		return g
	}, n.extend({
		expando: "jQuery" + (m + Math.random()).replace(/\D/g, ""),
		isReady: !0,
		error: function (a) {
			throw new Error(a)
		},
		noop: function () {},
		isFunction: function (a) {
			return "function" === n.type(a)
		},
		isArray: Array.isArray || function (a) {
			return "array" === n.type(a)
		},
		isWindow: function (a) {
			return null != a && a == a.window
		},
		isNumeric: function (a) {
			var b = a && a.toString();
			return !n.isArray(a) && b - parseFloat(b) + 1 >= 0
		},
		isEmptyObject: function (a) {
			var b;
			for (b in a) return !1;
			return !0
		},
		isPlainObject: function (a) {
			var b;
			if (!a || "object" !== n.type(a) || a.nodeType || n.isWindow(a)) return !1;
			try {
				if (a.constructor && !k.call(a, "constructor") && !k.call(a.constructor.prototype, "isPrototypeOf")) return !1
			} catch (c) {
				return !1
			}
			if (!l.ownFirst)
				for (b in a) return k.call(a, b);
			for (b in a);
			return void 0 === b || k.call(a, b)
		},
		type: function (a) {
			return null == a ? a + "" : "object" == typeof a || "function" == typeof a ? i[j.call(a)] || "object" : typeof a
		},
		globalEval: function (b) {
			b && n.trim(b) && (a.execScript || function (b) {
				a.eval.call(a, b)
			})(b)
		},
		camelCase: function (a) {
			return a.replace(p, "ms-").replace(q, r)
		},
		nodeName: function (a, b) {
			return a.nodeName && a.nodeName.toLowerCase() === b.toLowerCase()
		},
		each: function (a, b) {
			var c, d = 0;
			if (s(a)) {
				for (c = a.length; c > d; d++)
					if (b.call(a[d], d, a[d]) === !1) break
			} else
				for (d in a)
					if (b.call(a[d], d, a[d]) === !1) break;
			return a
		},
		trim: function (a) {
			return null == a ? "" : (a + "").replace(o, "")
		},
		makeArray: function (a, b) {
			var c = b || [];
			return null != a && (s(Object(a)) ? n.merge(c, "string" == typeof a ? [a] : a) : g.call(c, a)), c
		},
		inArray: function (a, b, c) {
			var d;
			if (b) {
				if (h) return h.call(b, a, c);
				for (d = b.length, c = c ? 0 > c ? Math.max(0, d + c) : c : 0; d > c; c++)
					if (c in b && b[c] === a) return c
			}
			return -1
		},
		merge: function (a, b) {
			var c = +b.length,
				d = 0,
				e = a.length;
			while (c > d) a[e++] = b[d++];
			if (c !== c)
				while (void 0 !== b[d]) a[e++] = b[d++];
			return a.length = e, a
		},
		grep: function (a, b, c) {
			for (var d, e = [], f = 0, g = a.length, h = !c; g > f; f++) d = !b(a[f], f), d !== h && e.push(a[f]);
			return e
		},
		map: function (a, b, c) {
			var d, e, g = 0,
				h = [];
			if (s(a))
				for (d = a.length; d > g; g++) e = b(a[g], g, c), null != e && h.push(e);
			else
				for (g in a) e = b(a[g], g, c), null != e && h.push(e);
			return f.apply([], h)
		},
		guid: 1,
		proxy: function (a, b) {
			var c, d, f;
			return "string" == typeof b && (f = a[b], b = a, a = f), n.isFunction(a) ? (c = e.call(arguments, 2), d = function () {
				return a.apply(b || this, c.concat(e.call(arguments)))
			}, d.guid = a.guid = a.guid || n.guid++, d) : void 0
		},
		now: function () {
			return +new Date
		},
		support: l
	}), "function" == typeof Symbol && (n.fn[Symbol.iterator] = c[Symbol.iterator]), n.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function (a, b) {
		i["[object " + b + "]"] = b.toLowerCase()
	});

	function s(a) {
		var b = !!a && "length" in a && a.length,
			c = n.type(a);
		return "function" === c || n.isWindow(a) ? !1 : "array" === c || 0 === b || "number" == typeof b && b > 0 && b - 1 in a
	}
	var t = function (a) {
		var b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u = "sizzle" + 1 * new Date,
			v = a.document,
			w = 0,
			x = 0,
			y = ga(),
			z = ga(),
			A = ga(),
			B = function (a, b) {
				return a === b && (l = !0), 0
			},
			C = 1 << 31,
			D = {}.hasOwnProperty,
			E = [],
			F = E.pop,
			G = E.push,
			H = E.push,
			I = E.slice,
			J = function (a, b) {
				for (var c = 0, d = a.length; d > c; c++)
					if (a[c] === b) return c;
				return -1
			},
			K = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
			L = "[\\x20\\t\\r\\n\\f]",
			M = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",
			N = "\\[" + L + "*(" + M + ")(?:" + L + "*([*^$|!~]?=)" + L + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + M + "))|)" + L + "*\\]",
			O = ":(" + M + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + N + ")*)|.*)\\)|)",
			P = new RegExp(L + "+", "g"),
			Q = new RegExp("^" + L + "+|((?:^|[^\\\\])(?:\\\\.)*)" + L + "+$", "g"),
			R = new RegExp("^" + L + "*," + L + "*"),
			S = new RegExp("^" + L + "*([>+~]|" + L + ")" + L + "*"),
			T = new RegExp("=" + L + "*([^\\]'\"]*?)" + L + "*\\]", "g"),
			U = new RegExp(O),
			V = new RegExp("^" + M + "$"),
			W = {
				ID: new RegExp("^#(" + M + ")"),
				CLASS: new RegExp("^\\.(" + M + ")"),
				TAG: new RegExp("^(" + M + "|[*])"),
				ATTR: new RegExp("^" + N),
				PSEUDO: new RegExp("^" + O),
				CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + L + "*(even|odd|(([+-]|)(\\d*)n|)" + L + "*(?:([+-]|)" + L + "*(\\d+)|))" + L + "*\\)|)", "i"),
				bool: new RegExp("^(?:" + K + ")$", "i"),
				needsContext: new RegExp("^" + L + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + L + "*((?:-\\d)?\\d*)" + L + "*\\)|)(?=[^-]|$)", "i")
			},
			X = /^(?:input|select|textarea|button)$/i,
			Y = /^h\d$/i,
			Z = /^[^{]+\{\s*\[native \w/,
			$ = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
			_ = /[+~]/,
			aa = /'|\\/g,
			ba = new RegExp("\\\\([\\da-f]{1,6}" + L + "?|(" + L + ")|.)", "ig"),
			ca = function (a, b, c) {
				var d = "0x" + b - 65536;
				return d !== d || c ? b : 0 > d ? String.fromCharCode(d + 65536) : String.fromCharCode(d >> 10 | 55296, 1023 & d | 56320)
			},
			da = function () {
				m()
			};
		try {
			H.apply(E = I.call(v.childNodes), v.childNodes), E[v.childNodes.length].nodeType
		} catch (ea) {
			H = {
				apply: E.length ? function (a, b) {
					G.apply(a, I.call(b))
				} : function (a, b) {
					var c = a.length,
						d = 0;
					while (a[c++] = b[d++]);
					a.length = c - 1
				}
			}
		}

		function fa(a, b, d, e) {
			var f, h, j, k, l, o, r, s, w = b && b.ownerDocument,
				x = b ? b.nodeType : 9;
			if (d = d || [], "string" != typeof a || !a || 1 !== x && 9 !== x && 11 !== x) return d;
			if (!e && ((b ? b.ownerDocument || b : v) !== n && m(b), b = b || n, p)) {
				if (11 !== x && (o = $.exec(a)))
					if (f = o[1]) {
						if (9 === x) {
							if (!(j = b.getElementById(f))) return d;
							if (j.id === f) return d.push(j), d
						} else if (w && (j = w.getElementById(f)) && t(b, j) && j.id === f) return d.push(j), d
					} else {
						if (o[2]) return H.apply(d, b.getElementsByTagName(a)), d;
						if ((f = o[3]) && c.getElementsByClassName && b.getElementsByClassName) return H.apply(d, b.getElementsByClassName(f)), d
					}
				if (c.qsa && !A[a + " "] && (!q || !q.test(a))) {
					if (1 !== x) w = b, s = a;
					else if ("object" !== b.nodeName.toLowerCase()) {
						(k = b.getAttribute("id")) ? k = k.replace(aa, "\\$&"): b.setAttribute("id", k = u), r = g(a), h = r.length, l = V.test(k) ? "#" + k : "[id='" + k + "']";
						while (h--) r[h] = l + " " + qa(r[h]);
						s = r.join(","), w = _.test(a) && oa(b.parentNode) || b
					}
					if (s) try {
						return H.apply(d, w.querySelectorAll(s)), d
					} catch (y) {} finally {
						k === u && b.removeAttribute("id")
					}
				}
			}
			return i(a.replace(Q, "$1"), b, d, e)
		}

		function ga() {
			var a = [];

			function b(c, e) {
				return a.push(c + " ") > d.cacheLength && delete b[a.shift()], b[c + " "] = e
			}
			return b
		}

		function ha(a) {
			return a[u] = !0, a
		}

		function ia(a) {
			var b = n.createElement("div");
			try {
				return !!a(b)
			} catch (c) {
				return !1
			} finally {
				b.parentNode && b.parentNode.removeChild(b), b = null
			}
		}

		function ja(a, b) {
			var c = a.split("|"),
				e = c.length;
			while (e--) d.attrHandle[c[e]] = b
		}

		function ka(a, b) {
			var c = b && a,
				d = c && 1 === a.nodeType && 1 === b.nodeType && (~b.sourceIndex || C) - (~a.sourceIndex || C);
			if (d) return d;
			if (c)
				while (c = c.nextSibling)
					if (c === b) return -1;
			return a ? 1 : -1
		}

		function la(a) {
			return function (b) {
				var c = b.nodeName.toLowerCase();
				return "input" === c && b.type === a
			}
		}

		function ma(a) {
			return function (b) {
				var c = b.nodeName.toLowerCase();
				return ("input" === c || "button" === c) && b.type === a
			}
		}

		function na(a) {
			return ha(function (b) {
				return b = +b, ha(function (c, d) {
					var e, f = a([], c.length, b),
						g = f.length;
					while (g--) c[e = f[g]] && (c[e] = !(d[e] = c[e]))
				})
			})
		}

		function oa(a) {
			return a && "undefined" != typeof a.getElementsByTagName && a
		}
		c = fa.support = {}, f = fa.isXML = function (a) {
			var b = a && (a.ownerDocument || a).documentElement;
			return b ? "HTML" !== b.nodeName : !1
		}, m = fa.setDocument = function (a) {
			var b, e, g = a ? a.ownerDocument || a : v;
			return g !== n && 9 === g.nodeType && g.documentElement ? (n = g, o = n.documentElement, p = !f(n), (e = n.defaultView) && e.top !== e && (e.addEventListener ? e.addEventListener("unload", da, !1) : e.attachEvent && e.attachEvent("onunload", da)), c.attributes = ia(function (a) {
				return a.className = "i", !a.getAttribute("className")
			}), c.getElementsByTagName = ia(function (a) {
				return a.appendChild(n.createComment("")), !a.getElementsByTagName("*").length
			}), c.getElementsByClassName = Z.test(n.getElementsByClassName), c.getById = ia(function (a) {
				return o.appendChild(a).id = u, !n.getElementsByName || !n.getElementsByName(u).length
			}), c.getById ? (d.find.ID = function (a, b) {
				if ("undefined" != typeof b.getElementById && p) {
					var c = b.getElementById(a);
					return c ? [c] : []
				}
			}, d.filter.ID = function (a) {
				var b = a.replace(ba, ca);
				return function (a) {
					return a.getAttribute("id") === b
				}
			}) : (delete d.find.ID, d.filter.ID = function (a) {
				var b = a.replace(ba, ca);
				return function (a) {
					var c = "undefined" != typeof a.getAttributeNode && a.getAttributeNode("id");
					return c && c.value === b
				}
			}), d.find.TAG = c.getElementsByTagName ? function (a, b) {
				return "undefined" != typeof b.getElementsByTagName ? b.getElementsByTagName(a) : c.qsa ? b.querySelectorAll(a) : void 0
			} : function (a, b) {
				var c, d = [],
					e = 0,
					f = b.getElementsByTagName(a);
				if ("*" === a) {
					while (c = f[e++]) 1 === c.nodeType && d.push(c);
					return d
				}
				return f
			}, d.find.CLASS = c.getElementsByClassName && function (a, b) {
				return "undefined" != typeof b.getElementsByClassName && p ? b.getElementsByClassName(a) : void 0
			}, r = [], q = [], (c.qsa = Z.test(n.querySelectorAll)) && (ia(function (a) {
				o.appendChild(a).innerHTML = "<a id='" + u + "'></a><select id='" + u + "-\r\\' msallowcapture=''><option selected=''></option></select>", a.querySelectorAll("[msallowcapture^='']").length && q.push("[*^$]=" + L + "*(?:''|\"\")"), a.querySelectorAll("[selected]").length || q.push("\\[" + L + "*(?:value|" + K + ")"), a.querySelectorAll("[id~=" + u + "-]").length || q.push("~="), a.querySelectorAll(":checked").length || q.push(":checked"), a.querySelectorAll("a#" + u + "+*").length || q.push(".#.+[+~]")
			}), ia(function (a) {
				var b = n.createElement("input");
				b.setAttribute("type", "hidden"), a.appendChild(b).setAttribute("name", "D"), a.querySelectorAll("[name=d]").length && q.push("name" + L + "*[*^$|!~]?="), a.querySelectorAll(":enabled").length || q.push(":enabled", ":disabled"), a.querySelectorAll("*,:x"), q.push(",.*:")
			})), (c.matchesSelector = Z.test(s = o.matches || o.webkitMatchesSelector || o.mozMatchesSelector || o.oMatchesSelector || o.msMatchesSelector)) && ia(function (a) {
				c.disconnectedMatch = s.call(a, "div"), s.call(a, "[s!='']:x"), r.push("!=", O)
			}), q = q.length && new RegExp(q.join("|")), r = r.length && new RegExp(r.join("|")), b = Z.test(o.compareDocumentPosition), t = b || Z.test(o.contains) ? function (a, b) {
				var c = 9 === a.nodeType ? a.documentElement : a,
					d = b && b.parentNode;
				return a === d || !(!d || 1 !== d.nodeType || !(c.contains ? c.contains(d) : a.compareDocumentPosition && 16 & a.compareDocumentPosition(d)))
			} : function (a, b) {
				if (b)
					while (b = b.parentNode)
						if (b === a) return !0;
				return !1
			}, B = b ? function (a, b) {
				if (a === b) return l = !0, 0;
				var d = !a.compareDocumentPosition - !b.compareDocumentPosition;
				return d ? d : (d = (a.ownerDocument || a) === (b.ownerDocument || b) ? a.compareDocumentPosition(b) : 1, 1 & d || !c.sortDetached && b.compareDocumentPosition(a) === d ? a === n || a.ownerDocument === v && t(v, a) ? -1 : b === n || b.ownerDocument === v && t(v, b) ? 1 : k ? J(k, a) - J(k, b) : 0 : 4 & d ? -1 : 1)
			} : function (a, b) {
				if (a === b) return l = !0, 0;
				var c, d = 0,
					e = a.parentNode,
					f = b.parentNode,
					g = [a],
					h = [b];
				if (!e || !f) return a === n ? -1 : b === n ? 1 : e ? -1 : f ? 1 : k ? J(k, a) - J(k, b) : 0;
				if (e === f) return ka(a, b);
				c = a;
				while (c = c.parentNode) g.unshift(c);
				c = b;
				while (c = c.parentNode) h.unshift(c);
				while (g[d] === h[d]) d++;
				return d ? ka(g[d], h[d]) : g[d] === v ? -1 : h[d] === v ? 1 : 0
			}, n) : n
		}, fa.matches = function (a, b) {
			return fa(a, null, null, b)
		}, fa.matchesSelector = function (a, b) {
			if ((a.ownerDocument || a) !== n && m(a), b = b.replace(T, "='$1']"), c.matchesSelector && p && !A[b + " "] && (!r || !r.test(b)) && (!q || !q.test(b))) try {
				var d = s.call(a, b);
				if (d || c.disconnectedMatch || a.document && 11 !== a.document.nodeType) return d
			} catch (e) {}
			return fa(b, n, null, [a]).length > 0
		}, fa.contains = function (a, b) {
			return (a.ownerDocument || a) !== n && m(a), t(a, b)
		}, fa.attr = function (a, b) {
			(a.ownerDocument || a) !== n && m(a);
			var e = d.attrHandle[b.toLowerCase()],
				f = e && D.call(d.attrHandle, b.toLowerCase()) ? e(a, b, !p) : void 0;
			return void 0 !== f ? f : c.attributes || !p ? a.getAttribute(b) : (f = a.getAttributeNode(b)) && f.specified ? f.value : null
		}, fa.error = function (a) {
			throw new Error("Syntax error, unrecognized expression: " + a)
		}, fa.uniqueSort = function (a) {
			var b, d = [],
				e = 0,
				f = 0;
			if (l = !c.detectDuplicates, k = !c.sortStable && a.slice(0), a.sort(B), l) {
				while (b = a[f++]) b === a[f] && (e = d.push(f));
				while (e--) a.splice(d[e], 1)
			}
			return k = null, a
		}, e = fa.getText = function (a) {
			var b, c = "",
				d = 0,
				f = a.nodeType;
			if (f) {
				if (1 === f || 9 === f || 11 === f) {
					if ("string" == typeof a.textContent) return a.textContent;
					for (a = a.firstChild; a; a = a.nextSibling) c += e(a)
				} else if (3 === f || 4 === f) return a.nodeValue
			} else
				while (b = a[d++]) c += e(b);
			return c
		}, d = fa.selectors = {
			cacheLength: 50,
			createPseudo: ha,
			match: W,
			attrHandle: {},
			find: {},
			relative: {
				">": {
					dir: "parentNode",
					first: !0
				},
				" ": {
					dir: "parentNode"
				},
				"+": {
					dir: "previousSibling",
					first: !0
				},
				"~": {
					dir: "previousSibling"
				}
			},
			preFilter: {
				ATTR: function (a) {
					return a[1] = a[1].replace(ba, ca), a[3] = (a[3] || a[4] || a[5] || "").replace(ba, ca), "~=" === a[2] && (a[3] = " " + a[3] + " "), a.slice(0, 4)
				},
				CHILD: function (a) {
					return a[1] = a[1].toLowerCase(), "nth" === a[1].slice(0, 3) ? (a[3] || fa.error(a[0]), a[4] = +(a[4] ? a[5] + (a[6] || 1) : 2 * ("even" === a[3] || "odd" === a[3])), a[5] = +(a[7] + a[8] || "odd" === a[3])) : a[3] && fa.error(a[0]), a
				},
				PSEUDO: function (a) {
					var b, c = !a[6] && a[2];
					return W.CHILD.test(a[0]) ? null : (a[3] ? a[2] = a[4] || a[5] || "" : c && U.test(c) && (b = g(c, !0)) && (b = c.indexOf(")", c.length - b) - c.length) && (a[0] = a[0].slice(0, b), a[2] = c.slice(0, b)), a.slice(0, 3))
				}
			},
			filter: {
				TAG: function (a) {
					var b = a.replace(ba, ca).toLowerCase();
					return "*" === a ? function () {
						return !0
					} : function (a) {
						return a.nodeName && a.nodeName.toLowerCase() === b
					}
				},
				CLASS: function (a) {
					var b = y[a + " "];
					return b || (b = new RegExp("(^|" + L + ")" + a + "(" + L + "|$)")) && y(a, function (a) {
						return b.test("string" == typeof a.className && a.className || "undefined" != typeof a.getAttribute && a.getAttribute("class") || "")
					})
				},
				ATTR: function (a, b, c) {
					return function (d) {
						var e = fa.attr(d, a);
						return null == e ? "!=" === b : b ? (e += "", "=" === b ? e === c : "!=" === b ? e !== c : "^=" === b ? c && 0 === e.indexOf(c) : "*=" === b ? c && e.indexOf(c) > -1 : "$=" === b ? c && e.slice(-c.length) === c : "~=" === b ? (" " + e.replace(P, " ") + " ").indexOf(c) > -1 : "|=" === b ? e === c || e.slice(0, c.length + 1) === c + "-" : !1) : !0
					}
				},
				CHILD: function (a, b, c, d, e) {
					var f = "nth" !== a.slice(0, 3),
						g = "last" !== a.slice(-4),
						h = "of-type" === b;
					return 1 === d && 0 === e ? function (a) {
						return !!a.parentNode
					} : function (b, c, i) {
						var j, k, l, m, n, o, p = f !== g ? "nextSibling" : "previousSibling",
							q = b.parentNode,
							r = h && b.nodeName.toLowerCase(),
							s = !i && !h,
							t = !1;
						if (q) {
							if (f) {
								while (p) {
									m = b;
									while (m = m[p])
										if (h ? m.nodeName.toLowerCase() === r : 1 === m.nodeType) return !1;
									o = p = "only" === a && !o && "nextSibling"
								}
								return !0
							}
							if (o = [g ? q.firstChild : q.lastChild], g && s) {
								m = q, l = m[u] || (m[u] = {}), k = l[m.uniqueID] || (l[m.uniqueID] = {}), j = k[a] || [], n = j[0] === w && j[1], t = n && j[2], m = n && q.childNodes[n];
								while (m = ++n && m && m[p] || (t = n = 0) || o.pop())
									if (1 === m.nodeType && ++t && m === b) {
										k[a] = [w, n, t];
										break
									}
							} else if (s && (m = b, l = m[u] || (m[u] = {}), k = l[m.uniqueID] || (l[m.uniqueID] = {}), j = k[a] || [], n = j[0] === w && j[1], t = n), t === !1)
								while (m = ++n && m && m[p] || (t = n = 0) || o.pop())
									if ((h ? m.nodeName.toLowerCase() === r : 1 === m.nodeType) && ++t && (s && (l = m[u] || (m[u] = {}), k = l[m.uniqueID] || (l[m.uniqueID] = {}), k[a] = [w, t]), m === b)) break;
							return t -= e, t === d || t % d === 0 && t / d >= 0
						}
					}
				},
				PSEUDO: function (a, b) {
					var c, e = d.pseudos[a] || d.setFilters[a.toLowerCase()] || fa.error("unsupported pseudo: " + a);
					return e[u] ? e(b) : e.length > 1 ? (c = [a, a, "", b], d.setFilters.hasOwnProperty(a.toLowerCase()) ? ha(function (a, c) {
						var d, f = e(a, b),
							g = f.length;
						while (g--) d = J(a, f[g]), a[d] = !(c[d] = f[g])
					}) : function (a) {
						return e(a, 0, c)
					}) : e
				}
			},
			pseudos: {
				not: ha(function (a) {
					var b = [],
						c = [],
						d = h(a.replace(Q, "$1"));
					return d[u] ? ha(function (a, b, c, e) {
						var f, g = d(a, null, e, []),
							h = a.length;
						while (h--)(f = g[h]) && (a[h] = !(b[h] = f))
					}) : function (a, e, f) {
						return b[0] = a, d(b, null, f, c), b[0] = null, !c.pop()
					}
				}),
				has: ha(function (a) {
					return function (b) {
						return fa(a, b).length > 0
					}
				}),
				contains: ha(function (a) {
					return a = a.replace(ba, ca),
						function (b) {
							return (b.textContent || b.innerText || e(b)).indexOf(a) > -1
						}
				}),
				lang: ha(function (a) {
					return V.test(a || "") || fa.error("unsupported lang: " + a), a = a.replace(ba, ca).toLowerCase(),
						function (b) {
							var c;
							do
								if (c = p ? b.lang : b.getAttribute("xml:lang") || b.getAttribute("lang")) return c = c.toLowerCase(), c === a || 0 === c.indexOf(a + "-"); while ((b = b.parentNode) && 1 === b.nodeType);
							return !1
						}
				}),
				target: function (b) {
					var c = a.location && a.location.hash;
					return c && c.slice(1) === b.id
				},
				root: function (a) {
					return a === o
				},
				focus: function (a) {
					return a === n.activeElement && (!n.hasFocus || n.hasFocus()) && !!(a.type || a.href || ~a.tabIndex)
				},
				enabled: function (a) {
					return a.disabled === !1
				},
				disabled: function (a) {
					return a.disabled === !0
				},
				checked: function (a) {
					var b = a.nodeName.toLowerCase();
					return "input" === b && !!a.checked || "option" === b && !!a.selected
				},
				selected: function (a) {
					return a.parentNode && a.parentNode.selectedIndex, a.selected === !0
				},
				empty: function (a) {
					for (a = a.firstChild; a; a = a.nextSibling)
						if (a.nodeType < 6) return !1;
					return !0
				},
				parent: function (a) {
					return !d.pseudos.empty(a)
				},
				header: function (a) {
					return Y.test(a.nodeName)
				},
				input: function (a) {
					return X.test(a.nodeName)
				},
				button: function (a) {
					var b = a.nodeName.toLowerCase();
					return "input" === b && "button" === a.type || "button" === b
				},
				text: function (a) {
					var b;
					return "input" === a.nodeName.toLowerCase() && "text" === a.type && (null == (b = a.getAttribute("type")) || "text" === b.toLowerCase())
				},
				first: na(function () {
					return [0]
				}),
				last: na(function (a, b) {
					return [b - 1]
				}),
				eq: na(function (a, b, c) {
					return [0 > c ? c + b : c]
				}),
				even: na(function (a, b) {
					for (var c = 0; b > c; c += 2) a.push(c);
					return a
				}),
				odd: na(function (a, b) {
					for (var c = 1; b > c; c += 2) a.push(c);
					return a
				}),
				lt: na(function (a, b, c) {
					for (var d = 0 > c ? c + b : c; --d >= 0;) a.push(d);
					return a
				}),
				gt: na(function (a, b, c) {
					for (var d = 0 > c ? c + b : c; ++d < b;) a.push(d);
					return a
				})
			}
		}, d.pseudos.nth = d.pseudos.eq;
		for (b in {
				radio: !0,
				checkbox: !0,
				file: !0,
				password: !0,
				image: !0
			}) d.pseudos[b] = la(b);
		for (b in {
				submit: !0,
				reset: !0
			}) d.pseudos[b] = ma(b);

		function pa() {}
		pa.prototype = d.filters = d.pseudos, d.setFilters = new pa, g = fa.tokenize = function (a, b) {
			var c, e, f, g, h, i, j, k = z[a + " "];
			if (k) return b ? 0 : k.slice(0);
			h = a, i = [], j = d.preFilter;
			while (h) {
				c && !(e = R.exec(h)) || (e && (h = h.slice(e[0].length) || h), i.push(f = [])), c = !1, (e = S.exec(h)) && (c = e.shift(), f.push({
					value: c,
					type: e[0].replace(Q, " ")
				}), h = h.slice(c.length));
				for (g in d.filter) !(e = W[g].exec(h)) || j[g] && !(e = j[g](e)) || (c = e.shift(), f.push({
					value: c,
					type: g,
					matches: e
				}), h = h.slice(c.length));
				if (!c) break
			}
			return b ? h.length : h ? fa.error(a) : z(a, i).slice(0)
		};

		function qa(a) {
			for (var b = 0, c = a.length, d = ""; c > b; b++) d += a[b].value;
			return d
		}

		function ra(a, b, c) {
			var d = b.dir,
				e = c && "parentNode" === d,
				f = x++;
			return b.first ? function (b, c, f) {
				while (b = b[d])
					if (1 === b.nodeType || e) return a(b, c, f)
			} : function (b, c, g) {
				var h, i, j, k = [w, f];
				if (g) {
					while (b = b[d])
						if ((1 === b.nodeType || e) && a(b, c, g)) return !0
				} else
					while (b = b[d])
						if (1 === b.nodeType || e) {
							if (j = b[u] || (b[u] = {}), i = j[b.uniqueID] || (j[b.uniqueID] = {}), (h = i[d]) && h[0] === w && h[1] === f) return k[2] = h[2];
							if (i[d] = k, k[2] = a(b, c, g)) return !0
						}
			}
		}

		function sa(a) {
			return a.length > 1 ? function (b, c, d) {
				var e = a.length;
				while (e--)
					if (!a[e](b, c, d)) return !1;
				return !0
			} : a[0]
		}

		function ta(a, b, c) {
			for (var d = 0, e = b.length; e > d; d++) fa(a, b[d], c);
			return c
		}

		function ua(a, b, c, d, e) {
			for (var f, g = [], h = 0, i = a.length, j = null != b; i > h; h++)(f = a[h]) && (c && !c(f, d, e) || (g.push(f), j && b.push(h)));
			return g
		}

		function va(a, b, c, d, e, f) {
			return d && !d[u] && (d = va(d)), e && !e[u] && (e = va(e, f)), ha(function (f, g, h, i) {
				var j, k, l, m = [],
					n = [],
					o = g.length,
					p = f || ta(b || "*", h.nodeType ? [h] : h, []),
					q = !a || !f && b ? p : ua(p, m, a, h, i),
					r = c ? e || (f ? a : o || d) ? [] : g : q;
				if (c && c(q, r, h, i), d) {
					j = ua(r, n), d(j, [], h, i), k = j.length;
					while (k--)(l = j[k]) && (r[n[k]] = !(q[n[k]] = l))
				}
				if (f) {
					if (e || a) {
						if (e) {
							j = [], k = r.length;
							while (k--)(l = r[k]) && j.push(q[k] = l);
							e(null, r = [], j, i)
						}
						k = r.length;
						while (k--)(l = r[k]) && (j = e ? J(f, l) : m[k]) > -1 && (f[j] = !(g[j] = l))
					}
				} else r = ua(r === g ? r.splice(o, r.length) : r), e ? e(null, g, r, i) : H.apply(g, r)
			})
		}

		function wa(a) {
			for (var b, c, e, f = a.length, g = d.relative[a[0].type], h = g || d.relative[" "], i = g ? 1 : 0, k = ra(function (a) {
					return a === b
				}, h, !0), l = ra(function (a) {
					return J(b, a) > -1
				}, h, !0), m = [function (a, c, d) {
					var e = !g && (d || c !== j) || ((b = c).nodeType ? k(a, c, d) : l(a, c, d));
					return b = null, e
				}]; f > i; i++)
				if (c = d.relative[a[i].type]) m = [ra(sa(m), c)];
				else {
					if (c = d.filter[a[i].type].apply(null, a[i].matches), c[u]) {
						for (e = ++i; f > e; e++)
							if (d.relative[a[e].type]) break;
						return va(i > 1 && sa(m), i > 1 && qa(a.slice(0, i - 1).concat({
							value: " " === a[i - 2].type ? "*" : ""
						})).replace(Q, "$1"), c, e > i && wa(a.slice(i, e)), f > e && wa(a = a.slice(e)), f > e && qa(a))
					}
					m.push(c)
				}
			return sa(m)
		}

		function xa(a, b) {
			var c = b.length > 0,
				e = a.length > 0,
				f = function (f, g, h, i, k) {
					var l, o, q, r = 0,
						s = "0",
						t = f && [],
						u = [],
						v = j,
						x = f || e && d.find.TAG("*", k),
						y = w += null == v ? 1 : Math.random() || .1,
						z = x.length;
					for (k && (j = g === n || g || k); s !== z && null != (l = x[s]); s++) {
						if (e && l) {
							o = 0, g || l.ownerDocument === n || (m(l), h = !p);
							while (q = a[o++])
								if (q(l, g || n, h)) {
									i.push(l);
									break
								}
							k && (w = y)
						}
						c && ((l = !q && l) && r--, f && t.push(l))
					}
					if (r += s, c && s !== r) {
						o = 0;
						while (q = b[o++]) q(t, u, g, h);
						if (f) {
							if (r > 0)
								while (s--) t[s] || u[s] || (u[s] = F.call(i));
							u = ua(u)
						}
						H.apply(i, u), k && !f && u.length > 0 && r + b.length > 1 && fa.uniqueSort(i)
					}
					return k && (w = y, j = v), t
				};
			return c ? ha(f) : f
		}
		return h = fa.compile = function (a, b) {
			var c, d = [],
				e = [],
				f = A[a + " "];
			if (!f) {
				b || (b = g(a)), c = b.length;
				while (c--) f = wa(b[c]), f[u] ? d.push(f) : e.push(f);
				f = A(a, xa(e, d)), f.selector = a
			}
			return f
		}, i = fa.select = function (a, b, e, f) {
			var i, j, k, l, m, n = "function" == typeof a && a,
				o = !f && g(a = n.selector || a);
			if (e = e || [], 1 === o.length) {
				if (j = o[0] = o[0].slice(0), j.length > 2 && "ID" === (k = j[0]).type && c.getById && 9 === b.nodeType && p && d.relative[j[1].type]) {
					if (b = (d.find.ID(k.matches[0].replace(ba, ca), b) || [])[0], !b) return e;
					n && (b = b.parentNode), a = a.slice(j.shift().value.length)
				}
				i = W.needsContext.test(a) ? 0 : j.length;
				while (i--) {
					if (k = j[i], d.relative[l = k.type]) break;
					if ((m = d.find[l]) && (f = m(k.matches[0].replace(ba, ca), _.test(j[0].type) && oa(b.parentNode) || b))) {
						if (j.splice(i, 1), a = f.length && qa(j), !a) return H.apply(e, f), e;
						break
					}
				}
			}
			return (n || h(a, o))(f, b, !p, e, !b || _.test(a) && oa(b.parentNode) || b), e
		}, c.sortStable = u.split("").sort(B).join("") === u, c.detectDuplicates = !!l, m(), c.sortDetached = ia(function (a) {
			return 1 & a.compareDocumentPosition(n.createElement("div"))
		}), ia(function (a) {
			return a.innerHTML = "<a href='#'></a>", "#" === a.firstChild.getAttribute("href")
		}) || ja("type|href|height|width", function (a, b, c) {
			return c ? void 0 : a.getAttribute(b, "type" === b.toLowerCase() ? 1 : 2)
		}), c.attributes && ia(function (a) {
			return a.innerHTML = "<input/>", a.firstChild.setAttribute("value", ""), "" === a.firstChild.getAttribute("value")
		}) || ja("value", function (a, b, c) {
			return c || "input" !== a.nodeName.toLowerCase() ? void 0 : a.defaultValue
		}), ia(function (a) {
			return null == a.getAttribute("disabled")
		}) || ja(K, function (a, b, c) {
			var d;
			return c ? void 0 : a[b] === !0 ? b.toLowerCase() : (d = a.getAttributeNode(b)) && d.specified ? d.value : null
		}), fa
	}(a);
	n.find = t, n.expr = t.selectors, n.expr[":"] = n.expr.pseudos, n.uniqueSort = n.unique = t.uniqueSort, n.text = t.getText, n.isXMLDoc = t.isXML, n.contains = t.contains;
	var u = function (a, b, c) {
			var d = [],
				e = void 0 !== c;
			while ((a = a[b]) && 9 !== a.nodeType)
				if (1 === a.nodeType) {
					if (e && n(a).is(c)) break;
					d.push(a)
				}
			return d
		},
		v = function (a, b) {
			for (var c = []; a; a = a.nextSibling) 1 === a.nodeType && a !== b && c.push(a);
			return c
		},
		w = n.expr.match.needsContext,
		x = /^<([\w-]+)\s*\/?>(?:<\/\1>|)$/,
		y = /^.[^:#\[\.,]*$/;

	function z(a, b, c) {
		if (n.isFunction(b)) return n.grep(a, function (a, d) {
			return !!b.call(a, d, a) !== c
		});
		if (b.nodeType) return n.grep(a, function (a) {
			return a === b !== c
		});
		if ("string" == typeof b) {
			if (y.test(b)) return n.filter(b, a, c);
			b = n.filter(b, a)
		}
		return n.grep(a, function (a) {
			return n.inArray(a, b) > -1 !== c
		})
	}
	n.filter = function (a, b, c) {
		var d = b[0];
		return c && (a = ":not(" + a + ")"), 1 === b.length && 1 === d.nodeType ? n.find.matchesSelector(d, a) ? [d] : [] : n.find.matches(a, n.grep(b, function (a) {
			return 1 === a.nodeType
		}))
	}, n.fn.extend({
		find: function (a) {
			var b, c = [],
				d = this,
				e = d.length;
			if ("string" != typeof a) return this.pushStack(n(a).filter(function () {
				for (b = 0; e > b; b++)
					if (n.contains(d[b], this)) return !0
			}));
			for (b = 0; e > b; b++) n.find(a, d[b], c);
			return c = this.pushStack(e > 1 ? n.unique(c) : c), c.selector = this.selector ? this.selector + " " + a : a, c
		},
		filter: function (a) {
			return this.pushStack(z(this, a || [], !1))
		},
		not: function (a) {
			return this.pushStack(z(this, a || [], !0))
		},
		is: function (a) {
			return !!z(this, "string" == typeof a && w.test(a) ? n(a) : a || [], !1).length
		}
	});
	var A, B = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,
		C = n.fn.init = function (a, b, c) {
			var e, f;
			if (!a) return this;
			if (c = c || A, "string" == typeof a) {
				if (e = "<" === a.charAt(0) && ">" === a.charAt(a.length - 1) && a.length >= 3 ? [null, a, null] : B.exec(a), !e || !e[1] && b) return !b || b.jquery ? (b || c).find(a) : this.constructor(b).find(a);
				if (e[1]) {
					if (b = b instanceof n ? b[0] : b, n.merge(this, n.parseHTML(e[1], b && b.nodeType ? b.ownerDocument || b : d, !0)), x.test(e[1]) && n.isPlainObject(b))
						for (e in b) n.isFunction(this[e]) ? this[e](b[e]) : this.attr(e, b[e]);
					return this
				}
				if (f = d.getElementById(e[2]), f && f.parentNode) {
					if (f.id !== e[2]) return A.find(a);
					this.length = 1, this[0] = f
				}
				return this.context = d, this.selector = a, this
			}
			return a.nodeType ? (this.context = this[0] = a, this.length = 1, this) : n.isFunction(a) ? "undefined" != typeof c.ready ? c.ready(a) : a(n) : (void 0 !== a.selector && (this.selector = a.selector, this.context = a.context), n.makeArray(a, this))
		};
	C.prototype = n.fn, A = n(d);
	var D = /^(?:parents|prev(?:Until|All))/,
		E = {
			children: !0,
			contents: !0,
			next: !0,
			prev: !0
		};
	n.fn.extend({
		has: function (a) {
			var b, c = n(a, this),
				d = c.length;
			return this.filter(function () {
				for (b = 0; d > b; b++)
					if (n.contains(this, c[b])) return !0
			})
		},
		closest: function (a, b) {
			for (var c, d = 0, e = this.length, f = [], g = w.test(a) || "string" != typeof a ? n(a, b || this.context) : 0; e > d; d++)
				for (c = this[d]; c && c !== b; c = c.parentNode)
					if (c.nodeType < 11 && (g ? g.index(c) > -1 : 1 === c.nodeType && n.find.matchesSelector(c, a))) {
						f.push(c);
						break
					}
			return this.pushStack(f.length > 1 ? n.uniqueSort(f) : f)
		},
		index: function (a) {
			return a ? "string" == typeof a ? n.inArray(this[0], n(a)) : n.inArray(a.jquery ? a[0] : a, this) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
		},
		add: function (a, b) {
			return this.pushStack(n.uniqueSort(n.merge(this.get(), n(a, b))))
		},
		addBack: function (a) {
			return this.add(null == a ? this.prevObject : this.prevObject.filter(a))
		}
	});

	function F(a, b) {
		do a = a[b]; while (a && 1 !== a.nodeType);
		return a
	}
	n.each({
		parent: function (a) {
			var b = a.parentNode;
			return b && 11 !== b.nodeType ? b : null
		},
		parents: function (a) {
			return u(a, "parentNode")
		},
		parentsUntil: function (a, b, c) {
			return u(a, "parentNode", c)
		},
		next: function (a) {
			return F(a, "nextSibling")
		},
		prev: function (a) {
			return F(a, "previousSibling")
		},
		nextAll: function (a) {
			return u(a, "nextSibling")
		},
		prevAll: function (a) {
			return u(a, "previousSibling")
		},
		nextUntil: function (a, b, c) {
			return u(a, "nextSibling", c)
		},
		prevUntil: function (a, b, c) {
			return u(a, "previousSibling", c)
		},
		siblings: function (a) {
			return v((a.parentNode || {}).firstChild, a)
		},
		children: function (a) {
			return v(a.firstChild)
		},
		contents: function (a) {
			return n.nodeName(a, "iframe") ? a.contentDocument || a.contentWindow.document : n.merge([], a.childNodes)
		}
	}, function (a, b) {
		n.fn[a] = function (c, d) {
			var e = n.map(this, b, c);
			return "Until" !== a.slice(-5) && (d = c), d && "string" == typeof d && (e = n.filter(d, e)), this.length > 1 && (E[a] || (e = n.uniqueSort(e)), D.test(a) && (e = e.reverse())), this.pushStack(e)
		}
	});
	var G = /\S+/g;

	function H(a) {
		var b = {};
		return n.each(a.match(G) || [], function (a, c) {
			b[c] = !0
		}), b
	}
	n.Callbacks = function (a) {
		a = "string" == typeof a ? H(a) : n.extend({}, a);
		var b, c, d, e, f = [],
			g = [],
			h = -1,
			i = function () {
				for (e = a.once, d = b = !0; g.length; h = -1) {
					c = g.shift();
					while (++h < f.length) f[h].apply(c[0], c[1]) === !1 && a.stopOnFalse && (h = f.length, c = !1)
				}
				a.memory || (c = !1), b = !1, e && (f = c ? [] : "")
			},
			j = {
				add: function () {
					return f && (c && !b && (h = f.length - 1, g.push(c)), function d(b) {
						n.each(b, function (b, c) {
							n.isFunction(c) ? a.unique && j.has(c) || f.push(c) : c && c.length && "string" !== n.type(c) && d(c)
						})
					}(arguments), c && !b && i()), this
				},
				remove: function () {
					return n.each(arguments, function (a, b) {
						var c;
						while ((c = n.inArray(b, f, c)) > -1) f.splice(c, 1), h >= c && h--
					}), this
				},
				has: function (a) {
					return a ? n.inArray(a, f) > -1 : f.length > 0
				},
				empty: function () {
					return f && (f = []), this
				},
				disable: function () {
					return e = g = [], f = c = "", this
				},
				disabled: function () {
					return !f
				},
				lock: function () {
					return e = !0, c || j.disable(), this
				},
				locked: function () {
					return !!e
				},
				fireWith: function (a, c) {
					return e || (c = c || [], c = [a, c.slice ? c.slice() : c], g.push(c), b || i()), this
				},
				fire: function () {
					return j.fireWith(this, arguments), this
				},
				fired: function () {
					return !!d
				}
			};
		return j
	}, n.extend({
		Deferred: function (a) {
			var b = [["resolve", "done", n.Callbacks("once memory"), "resolved"], ["reject", "fail", n.Callbacks("once memory"), "rejected"], ["notify", "progress", n.Callbacks("memory")]],
				c = "pending",
				d = {
					state: function () {
						return c
					},
					always: function () {
						return e.done(arguments).fail(arguments), this
					},
					then: function () {
						var a = arguments;
						return n.Deferred(function (c) {
							n.each(b, function (b, f) {
								var g = n.isFunction(a[b]) && a[b];
								e[f[1]](function () {
									var a = g && g.apply(this, arguments);
									a && n.isFunction(a.promise) ? a.promise().progress(c.notify).done(c.resolve).fail(c.reject) : c[f[0] + "With"](this === d ? c.promise() : this, g ? [a] : arguments)
								})
							}), a = null
						}).promise()
					},
					promise: function (a) {
						return null != a ? n.extend(a, d) : d
					}
				},
				e = {};
			return d.pipe = d.then, n.each(b, function (a, f) {
				var g = f[2],
					h = f[3];
				d[f[1]] = g.add, h && g.add(function () {
					c = h
				}, b[1 ^ a][2].disable, b[2][2].lock), e[f[0]] = function () {
					return e[f[0] + "With"](this === e ? d : this, arguments), this
				}, e[f[0] + "With"] = g.fireWith
			}), d.promise(e), a && a.call(e, e), e
		},
		when: function (a) {
			var b = 0,
				c = e.call(arguments),
				d = c.length,
				f = 1 !== d || a && n.isFunction(a.promise) ? d : 0,
				g = 1 === f ? a : n.Deferred(),
				h = function (a, b, c) {
					return function (d) {
						b[a] = this, c[a] = arguments.length > 1 ? e.call(arguments) : d, c === i ? g.notifyWith(b, c) : --f || g.resolveWith(b, c)
					}
				},
				i, j, k;
			if (d > 1)
				for (i = new Array(d), j = new Array(d), k = new Array(d); d > b; b++) c[b] && n.isFunction(c[b].promise) ? c[b].promise().progress(h(b, j, i)).done(h(b, k, c)).fail(g.reject) : --f;
			return f || g.resolveWith(k, c), g.promise()
		}
	});
	var I;
	n.fn.ready = function (a) {
		return n.ready.promise().done(a), this
	}, n.extend({
		isReady: !1,
		readyWait: 1,
		holdReady: function (a) {
			a ? n.readyWait++ : n.ready(!0)
		},
		ready: function (a) {
			(a === !0 ? --n.readyWait : n.isReady) || (n.isReady = !0, a !== !0 && --n.readyWait > 0 || (I.resolveWith(d, [n]), n.fn.triggerHandler && (n(d).triggerHandler("ready"), n(d).off("ready"))))
		}
	});

	function J() {
		d.addEventListener ? (d.removeEventListener("DOMContentLoaded", K), a.removeEventListener("load", K)) : (d.detachEvent("onreadystatechange", K), a.detachEvent("onload", K))
	}

	function K() {
		(d.addEventListener || "load" === a.event.type || "complete" === d.readyState) && (J(), n.ready())
	}
	n.ready.promise = function (b) {
		if (!I)
			if (I = n.Deferred(), "complete" === d.readyState || "loading" !== d.readyState && !d.documentElement.doScroll) a.setTimeout(n.ready);
			else if (d.addEventListener) d.addEventListener("DOMContentLoaded", K), a.addEventListener("load", K);
		else {
			d.attachEvent("onreadystatechange", K), a.attachEvent("onload", K);
			var c = !1;
			try {
				c = null == a.frameElement && d.documentElement
			} catch (e) {}
			c && c.doScroll && ! function f() {
				if (!n.isReady) {
					try {
						c.doScroll("left")
					} catch (b) {
						return a.setTimeout(f, 50)
					}
					J(), n.ready()
				}
			}()
		}
		return I.promise(b)
	}, n.ready.promise();
	var L;
	for (L in n(l)) break;
	l.ownFirst = "0" === L, l.inlineBlockNeedsLayout = !1, n(function () {
			var a, b, c, e;
			c = d.getElementsByTagName("body")[0], c && c.style && (b = d.createElement("div"), e = d.createElement("div"), e.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", c.appendChild(e).appendChild(b), "undefined" != typeof b.style.zoom && (b.style.cssText = "display:inline;margin:0;border:0;padding:1px;width:1px;zoom:1", l.inlineBlockNeedsLayout = a = 3 === b.offsetWidth, a && (c.style.zoom = 1)), c.removeChild(e))
		}),
		function () {
			var a = d.createElement("div");
			l.deleteExpando = !0;
			try {
				delete a.test
			} catch (b) {
				l.deleteExpando = !1
			}
			a = null
		}();
	var M = function (a) {
			var b = n.noData[(a.nodeName + " ").toLowerCase()],
				c = +a.nodeType || 1;
			return 1 !== c && 9 !== c ? !1 : !b || b !== !0 && a.getAttribute("classid") === b
		},
		N = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
		O = /([A-Z])/g;

	function P(a, b, c) {
		if (void 0 === c && 1 === a.nodeType) {
			var d = "data-" + b.replace(O, "-$1").toLowerCase();
			if (c = a.getAttribute(d), "string" == typeof c) {
				try {
					c = "true" === c ? !0 : "false" === c ? !1 : "null" === c ? null : +c + "" === c ? +c : N.test(c) ? n.parseJSON(c) : c
				} catch (e) {}
				n.data(a, b, c)
			} else c = void 0;
		}
		return c
	}

	function Q(a) {
		var b;
		for (b in a)
			if (("data" !== b || !n.isEmptyObject(a[b])) && "toJSON" !== b) return !1;
		return !0
	}

	function R(a, b, d, e) {
		if (M(a)) {
			var f, g, h = n.expando,
				i = a.nodeType,
				j = i ? n.cache : a,
				k = i ? a[h] : a[h] && h;
			if (k && j[k] && (e || j[k].data) || void 0 !== d || "string" != typeof b) return k || (k = i ? a[h] = c.pop() || n.guid++ : h), j[k] || (j[k] = i ? {} : {
				toJSON: n.noop
			}), "object" != typeof b && "function" != typeof b || (e ? j[k] = n.extend(j[k], b) : j[k].data = n.extend(j[k].data, b)), g = j[k], e || (g.data || (g.data = {}), g = g.data), void 0 !== d && (g[n.camelCase(b)] = d), "string" == typeof b ? (f = g[b], null == f && (f = g[n.camelCase(b)])) : f = g, f
		}
	}

	function S(a, b, c) {
		if (M(a)) {
			var d, e, f = a.nodeType,
				g = f ? n.cache : a,
				h = f ? a[n.expando] : n.expando;
			if (g[h]) {
				if (b && (d = c ? g[h] : g[h].data)) {
					n.isArray(b) ? b = b.concat(n.map(b, n.camelCase)) : b in d ? b = [b] : (b = n.camelCase(b), b = b in d ? [b] : b.split(" ")), e = b.length;
					while (e--) delete d[b[e]];
					if (c ? !Q(d) : !n.isEmptyObject(d)) return
				}(c || (delete g[h].data, Q(g[h]))) && (f ? n.cleanData([a], !0) : l.deleteExpando || g != g.window ? delete g[h] : g[h] = void 0)
			}
		}
	}
	n.extend({
			cache: {},
			noData: {
				"applet ": !0,
				"embed ": !0,
				"object ": "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
			},
			hasData: function (a) {
				return a = a.nodeType ? n.cache[a[n.expando]] : a[n.expando], !!a && !Q(a)
			},
			data: function (a, b, c) {
				return R(a, b, c)
			},
			removeData: function (a, b) {
				return S(a, b)
			},
			_data: function (a, b, c) {
				return R(a, b, c, !0)
			},
			_removeData: function (a, b) {
				return S(a, b, !0)
			}
		}), n.fn.extend({
			data: function (a, b) {
				var c, d, e, f = this[0],
					g = f && f.attributes;
				if (void 0 === a) {
					if (this.length && (e = n.data(f), 1 === f.nodeType && !n._data(f, "parsedAttrs"))) {
						c = g.length;
						while (c--) g[c] && (d = g[c].name, 0 === d.indexOf("data-") && (d = n.camelCase(d.slice(5)), P(f, d, e[d])));
						n._data(f, "parsedAttrs", !0)
					}
					return e
				}
				return "object" == typeof a ? this.each(function () {
					n.data(this, a)
				}) : arguments.length > 1 ? this.each(function () {
					n.data(this, a, b)
				}) : f ? P(f, a, n.data(f, a)) : void 0
			},
			removeData: function (a) {
				return this.each(function () {
					n.removeData(this, a)
				})
			}
		}), n.extend({
			queue: function (a, b, c) {
				var d;
				return a ? (b = (b || "fx") + "queue", d = n._data(a, b), c && (!d || n.isArray(c) ? d = n._data(a, b, n.makeArray(c)) : d.push(c)), d || []) : void 0
			},
			dequeue: function (a, b) {
				b = b || "fx";
				var c = n.queue(a, b),
					d = c.length,
					e = c.shift(),
					f = n._queueHooks(a, b),
					g = function () {
						n.dequeue(a, b)
					};
				"inprogress" === e && (e = c.shift(), d--), e && ("fx" === b && c.unshift("inprogress"), delete f.stop, e.call(a, g, f)), !d && f && f.empty.fire()
			},
			_queueHooks: function (a, b) {
				var c = b + "queueHooks";
				return n._data(a, c) || n._data(a, c, {
					empty: n.Callbacks("once memory").add(function () {
						n._removeData(a, b + "queue"), n._removeData(a, c)
					})
				})
			}
		}), n.fn.extend({
			queue: function (a, b) {
				var c = 2;
				return "string" != typeof a && (b = a, a = "fx", c--), arguments.length < c ? n.queue(this[0], a) : void 0 === b ? this : this.each(function () {
					var c = n.queue(this, a, b);
					n._queueHooks(this, a), "fx" === a && "inprogress" !== c[0] && n.dequeue(this, a)
				})
			},
			dequeue: function (a) {
				return this.each(function () {
					n.dequeue(this, a)
				})
			},
			clearQueue: function (a) {
				return this.queue(a || "fx", [])
			},
			promise: function (a, b) {
				var c, d = 1,
					e = n.Deferred(),
					f = this,
					g = this.length,
					h = function () {
						--d || e.resolveWith(f, [f])
					};
				"string" != typeof a && (b = a, a = void 0), a = a || "fx";
				while (g--) c = n._data(f[g], a + "queueHooks"), c && c.empty && (d++, c.empty.add(h));
				return h(), e.promise(b)
			}
		}),
		function () {
			var a;
			l.shrinkWrapBlocks = function () {
				if (null != a) return a;
				a = !1;
				var b, c, e;
				return c = d.getElementsByTagName("body")[0], c && c.style ? (b = d.createElement("div"), e = d.createElement("div"), e.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", c.appendChild(e).appendChild(b), "undefined" != typeof b.style.zoom && (b.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:1px;width:1px;zoom:1", b.appendChild(d.createElement("div")).style.width = "5px", a = 3 !== b.offsetWidth), c.removeChild(e), a) : void 0
			}
		}();
	var T = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
		U = new RegExp("^(?:([+-])=|)(" + T + ")([a-z%]*)$", "i"),
		V = ["Top", "Right", "Bottom", "Left"],
		W = function (a, b) {
			return a = b || a, "none" === n.css(a, "display") || !n.contains(a.ownerDocument, a)
		};

	function X(a, b, c, d) {
		var e, f = 1,
			g = 20,
			h = d ? function () {
				return d.cur()
			} : function () {
				return n.css(a, b, "")
			},
			i = h(),
			j = c && c[3] || (n.cssNumber[b] ? "" : "px"),
			k = (n.cssNumber[b] || "px" !== j && +i) && U.exec(n.css(a, b));
		if (k && k[3] !== j) {
			j = j || k[3], c = c || [], k = +i || 1;
			do f = f || ".5", k /= f, n.style(a, b, k + j); while (f !== (f = h() / i) && 1 !== f && --g)
		}
		return c && (k = +k || +i || 0, e = c[1] ? k + (c[1] + 1) * c[2] : +c[2], d && (d.unit = j, d.start = k, d.end = e)), e
	}
	var Y = function (a, b, c, d, e, f, g) {
			var h = 0,
				i = a.length,
				j = null == c;
			if ("object" === n.type(c)) {
				e = !0;
				for (h in c) Y(a, b, h, c[h], !0, f, g)
			} else if (void 0 !== d && (e = !0, n.isFunction(d) || (g = !0), j && (g ? (b.call(a, d), b = null) : (j = b, b = function (a, b, c) {
					return j.call(n(a), c)
				})), b))
				for (; i > h; h++) b(a[h], c, g ? d : d.call(a[h], h, b(a[h], c)));
			return e ? a : j ? b.call(a) : i ? b(a[0], c) : f
		},
		Z = /^(?:checkbox|radio)$/i,
		$ = /<([\w:-]+)/,
		_ = /^$|\/(?:java|ecma)script/i,
		aa = /^\s+/,
		ba = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|dialog|figcaption|figure|footer|header|hgroup|main|mark|meter|nav|output|picture|progress|section|summary|template|time|video";

	function ca(a) {
		var b = ba.split("|"),
			c = a.createDocumentFragment();
		if (c.createElement)
			while (b.length) c.createElement(b.pop());
		return c
	}! function () {
		var a = d.createElement("div"),
			b = d.createDocumentFragment(),
			c = d.createElement("input");
		a.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", l.leadingWhitespace = 3 === a.firstChild.nodeType, l.tbody = !a.getElementsByTagName("tbody").length, l.htmlSerialize = !!a.getElementsByTagName("link").length, l.html5Clone = "<:nav></:nav>" !== d.createElement("nav").cloneNode(!0).outerHTML, c.type = "checkbox", c.checked = !0, b.appendChild(c), l.appendChecked = c.checked, a.innerHTML = "<textarea>x</textarea>", l.noCloneChecked = !!a.cloneNode(!0).lastChild.defaultValue, b.appendChild(a), c = d.createElement("input"), c.setAttribute("type", "radio"), c.setAttribute("checked", "checked"), c.setAttribute("name", "t"), a.appendChild(c), l.checkClone = a.cloneNode(!0).cloneNode(!0).lastChild.checked, l.noCloneEvent = !!a.addEventListener, a[n.expando] = 1, l.attributes = !a.getAttribute(n.expando)
	}();
	var da = {
		option: [1, "<select multiple='multiple'>", "</select>"],
		legend: [1, "<fieldset>", "</fieldset>"],
		area: [1, "<map>", "</map>"],
		param: [1, "<object>", "</object>"],
		thead: [1, "<table>", "</table>"],
		tr: [2, "<table><tbody>", "</tbody></table>"],
		col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"],
		td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
		_default: l.htmlSerialize ? [0, "", ""] : [1, "X<div>", "</div>"]
	};
	da.optgroup = da.option, da.tbody = da.tfoot = da.colgroup = da.caption = da.thead, da.th = da.td;

	function ea(a, b) {
		var c, d, e = 0,
			f = "undefined" != typeof a.getElementsByTagName ? a.getElementsByTagName(b || "*") : "undefined" != typeof a.querySelectorAll ? a.querySelectorAll(b || "*") : void 0;
		if (!f)
			for (f = [], c = a.childNodes || a; null != (d = c[e]); e++) !b || n.nodeName(d, b) ? f.push(d) : n.merge(f, ea(d, b));
		return void 0 === b || b && n.nodeName(a, b) ? n.merge([a], f) : f
	}

	function fa(a, b) {
		for (var c, d = 0; null != (c = a[d]); d++) n._data(c, "globalEval", !b || n._data(b[d], "globalEval"))
	}
	var ga = /<|&#?\w+;/,
		ha = /<tbody/i;

	function ia(a) {
		Z.test(a.type) && (a.defaultChecked = a.checked)
	}

	function ja(a, b, c, d, e) {
		for (var f, g, h, i, j, k, m, o = a.length, p = ca(b), q = [], r = 0; o > r; r++)
			if (g = a[r], g || 0 === g)
				if ("object" === n.type(g)) n.merge(q, g.nodeType ? [g] : g);
				else if (ga.test(g)) {
			i = i || p.appendChild(b.createElement("div")), j = ($.exec(g) || ["", ""])[1].toLowerCase(), m = da[j] || da._default, i.innerHTML = m[1] + n.htmlPrefilter(g) + m[2], f = m[0];
			while (f--) i = i.lastChild;
			if (!l.leadingWhitespace && aa.test(g) && q.push(b.createTextNode(aa.exec(g)[0])), !l.tbody) {
				g = "table" !== j || ha.test(g) ? "<table>" !== m[1] || ha.test(g) ? 0 : i : i.firstChild, f = g && g.childNodes.length;
				while (f--) n.nodeName(k = g.childNodes[f], "tbody") && !k.childNodes.length && g.removeChild(k)
			}
			n.merge(q, i.childNodes), i.textContent = "";
			while (i.firstChild) i.removeChild(i.firstChild);
			i = p.lastChild
		} else q.push(b.createTextNode(g));
		i && p.removeChild(i), l.appendChecked || n.grep(ea(q, "input"), ia), r = 0;
		while (g = q[r++])
			if (d && n.inArray(g, d) > -1) e && e.push(g);
			else if (h = n.contains(g.ownerDocument, g), i = ea(p.appendChild(g), "script"), h && fa(i), c) {
			f = 0;
			while (g = i[f++]) _.test(g.type || "") && c.push(g)
		}
		return i = null, p
	}! function () {
		var b, c, e = d.createElement("div");
		for (b in {
				submit: !0,
				change: !0,
				focusin: !0
			}) c = "on" + b, (l[b] = c in a) || (e.setAttribute(c, "t"), l[b] = e.attributes[c].expando === !1);
		e = null
	}();
	var ka = /^(?:input|select|textarea)$/i,
		la = /^key/,
		ma = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
		na = /^(?:focusinfocus|focusoutblur)$/,
		oa = /^([^.]*)(?:\.(.+)|)/;

	function pa() {
		return !0
	}

	function qa() {
		return !1
	}

	function ra() {
		try {
			return d.activeElement
		} catch (a) {}
	}

	function sa(a, b, c, d, e, f) {
		var g, h;
		if ("object" == typeof b) {
			"string" != typeof c && (d = d || c, c = void 0);
			for (h in b) sa(a, h, c, d, b[h], f);
			return a
		}
		if (null == d && null == e ? (e = c, d = c = void 0) : null == e && ("string" == typeof c ? (e = d, d = void 0) : (e = d, d = c, c = void 0)), e === !1) e = qa;
		else if (!e) return a;
		return 1 === f && (g = e, e = function (a) {
			return n().off(a), g.apply(this, arguments)
		}, e.guid = g.guid || (g.guid = n.guid++)), a.each(function () {
			n.event.add(this, b, e, d, c)
		})
	}
	n.event = {
		global: {},
		add: function (a, b, c, d, e) {
			var f, g, h, i, j, k, l, m, o, p, q, r = n._data(a);
			if (r) {
				c.handler && (i = c, c = i.handler, e = i.selector), c.guid || (c.guid = n.guid++), (g = r.events) || (g = r.events = {}), (k = r.handle) || (k = r.handle = function (a) {
					return "undefined" == typeof n || a && n.event.triggered === a.type ? void 0 : n.event.dispatch.apply(k.elem, arguments)
				}, k.elem = a), b = (b || "").match(G) || [""], h = b.length;
				while (h--) f = oa.exec(b[h]) || [], o = q = f[1], p = (f[2] || "").split(".").sort(), o && (j = n.event.special[o] || {}, o = (e ? j.delegateType : j.bindType) || o, j = n.event.special[o] || {}, l = n.extend({
					type: o,
					origType: q,
					data: d,
					handler: c,
					guid: c.guid,
					selector: e,
					needsContext: e && n.expr.match.needsContext.test(e),
					namespace: p.join(".")
				}, i), (m = g[o]) || (m = g[o] = [], m.delegateCount = 0, j.setup && j.setup.call(a, d, p, k) !== !1 || (a.addEventListener ? a.addEventListener(o, k, !1) : a.attachEvent && a.attachEvent("on" + o, k))), j.add && (j.add.call(a, l), l.handler.guid || (l.handler.guid = c.guid)), e ? m.splice(m.delegateCount++, 0, l) : m.push(l), n.event.global[o] = !0);
				a = null
			}
		},
		remove: function (a, b, c, d, e) {
			var f, g, h, i, j, k, l, m, o, p, q, r = n.hasData(a) && n._data(a);
			if (r && (k = r.events)) {
				b = (b || "").match(G) || [""], j = b.length;
				while (j--)
					if (h = oa.exec(b[j]) || [], o = q = h[1], p = (h[2] || "").split(".").sort(), o) {
						l = n.event.special[o] || {}, o = (d ? l.delegateType : l.bindType) || o, m = k[o] || [], h = h[2] && new RegExp("(^|\\.)" + p.join("\\.(?:.*\\.|)") + "(\\.|$)"), i = f = m.length;
						while (f--) g = m[f], !e && q !== g.origType || c && c.guid !== g.guid || h && !h.test(g.namespace) || d && d !== g.selector && ("**" !== d || !g.selector) || (m.splice(f, 1), g.selector && m.delegateCount--, l.remove && l.remove.call(a, g));
						i && !m.length && (l.teardown && l.teardown.call(a, p, r.handle) !== !1 || n.removeEvent(a, o, r.handle), delete k[o])
					} else
						for (o in k) n.event.remove(a, o + b[j], c, d, !0);
				n.isEmptyObject(k) && (delete r.handle, n._removeData(a, "events"))
			}
		},
		trigger: function (b, c, e, f) {
			var g, h, i, j, l, m, o, p = [e || d],
				q = k.call(b, "type") ? b.type : b,
				r = k.call(b, "namespace") ? b.namespace.split(".") : [];
			if (i = m = e = e || d, 3 !== e.nodeType && 8 !== e.nodeType && !na.test(q + n.event.triggered) && (q.indexOf(".") > -1 && (r = q.split("."), q = r.shift(), r.sort()), h = q.indexOf(":") < 0 && "on" + q, b = b[n.expando] ? b : new n.Event(q, "object" == typeof b && b), b.isTrigger = f ? 2 : 3, b.namespace = r.join("."), b.rnamespace = b.namespace ? new RegExp("(^|\\.)" + r.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, b.result = void 0, b.target || (b.target = e), c = null == c ? [b] : n.makeArray(c, [b]), l = n.event.special[q] || {}, f || !l.trigger || l.trigger.apply(e, c) !== !1)) {
				if (!f && !l.noBubble && !n.isWindow(e)) {
					for (j = l.delegateType || q, na.test(j + q) || (i = i.parentNode); i; i = i.parentNode) p.push(i), m = i;
					m === (e.ownerDocument || d) && p.push(m.defaultView || m.parentWindow || a)
				}
				o = 0;
				while ((i = p[o++]) && !b.isPropagationStopped()) b.type = o > 1 ? j : l.bindType || q, g = (n._data(i, "events") || {})[b.type] && n._data(i, "handle"), g && g.apply(i, c), g = h && i[h], g && g.apply && M(i) && (b.result = g.apply(i, c), b.result === !1 && b.preventDefault());
				if (b.type = q, !f && !b.isDefaultPrevented() && (!l._default || l._default.apply(p.pop(), c) === !1) && M(e) && h && e[q] && !n.isWindow(e)) {
					m = e[h], m && (e[h] = null), n.event.triggered = q;
					try {
						e[q]()
					} catch (s) {}
					n.event.triggered = void 0, m && (e[h] = m)
				}
				return b.result
			}
		},
		dispatch: function (a) {
			a = n.event.fix(a);
			var b, c, d, f, g, h = [],
				i = e.call(arguments),
				j = (n._data(this, "events") || {})[a.type] || [],
				k = n.event.special[a.type] || {};
			if (i[0] = a, a.delegateTarget = this, !k.preDispatch || k.preDispatch.call(this, a) !== !1) {
				h = n.event.handlers.call(this, a, j), b = 0;
				while ((f = h[b++]) && !a.isPropagationStopped()) {
					a.currentTarget = f.elem, c = 0;
					while ((g = f.handlers[c++]) && !a.isImmediatePropagationStopped()) a.rnamespace && !a.rnamespace.test(g.namespace) || (a.handleObj = g, a.data = g.data, d = ((n.event.special[g.origType] || {}).handle || g.handler).apply(f.elem, i), void 0 !== d && (a.result = d) === !1 && (a.preventDefault(), a.stopPropagation()))
				}
				return k.postDispatch && k.postDispatch.call(this, a), a.result
			}
		},
		handlers: function (a, b) {
			var c, d, e, f, g = [],
				h = b.delegateCount,
				i = a.target;
			if (h && i.nodeType && ("click" !== a.type || isNaN(a.button) || a.button < 1))
				for (; i != this; i = i.parentNode || this)
					if (1 === i.nodeType && (i.disabled !== !0 || "click" !== a.type)) {
						for (d = [], c = 0; h > c; c++) f = b[c], e = f.selector + " ", void 0 === d[e] && (d[e] = f.needsContext ? n(e, this).index(i) > -1 : n.find(e, this, null, [i]).length), d[e] && d.push(f);
						d.length && g.push({
							elem: i,
							handlers: d
						})
					}
			return h < b.length && g.push({
				elem: this,
				handlers: b.slice(h)
			}), g
		},
		fix: function (a) {
			if (a[n.expando]) return a;
			var b, c, e, f = a.type,
				g = a,
				h = this.fixHooks[f];
			h || (this.fixHooks[f] = h = ma.test(f) ? this.mouseHooks : la.test(f) ? this.keyHooks : {}), e = h.props ? this.props.concat(h.props) : this.props, a = new n.Event(g), b = e.length;
			while (b--) c = e[b], a[c] = g[c];
			return a.target || (a.target = g.srcElement || d), 3 === a.target.nodeType && (a.target = a.target.parentNode), a.metaKey = !!a.metaKey, h.filter ? h.filter(a, g) : a
		},
		props: "altKey bubbles cancelable ctrlKey currentTarget detail eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
		fixHooks: {},
		keyHooks: {
			props: "char charCode key keyCode".split(" "),
			filter: function (a, b) {
				return null == a.which && (a.which = null != b.charCode ? b.charCode : b.keyCode), a
			}
		},
		mouseHooks: {
			props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
			filter: function (a, b) {
				var c, e, f, g = b.button,
					h = b.fromElement;
				return null == a.pageX && null != b.clientX && (e = a.target.ownerDocument || d, f = e.documentElement, c = e.body, a.pageX = b.clientX + (f && f.scrollLeft || c && c.scrollLeft || 0) - (f && f.clientLeft || c && c.clientLeft || 0), a.pageY = b.clientY + (f && f.scrollTop || c && c.scrollTop || 0) - (f && f.clientTop || c && c.clientTop || 0)), !a.relatedTarget && h && (a.relatedTarget = h === a.target ? b.toElement : h), a.which || void 0 === g || (a.which = 1 & g ? 1 : 2 & g ? 3 : 4 & g ? 2 : 0), a
			}
		},
		special: {
			load: {
				noBubble: !0
			},
			focus: {
				trigger: function () {
					if (this !== ra() && this.focus) try {
						return this.focus(), !1
					} catch (a) {}
				},
				delegateType: "focusin"
			},
			blur: {
				trigger: function () {
					return this === ra() && this.blur ? (this.blur(), !1) : void 0
				},
				delegateType: "focusout"
			},
			click: {
				trigger: function () {
					return n.nodeName(this, "input") && "checkbox" === this.type && this.click ? (this.click(), !1) : void 0
				},
				_default: function (a) {
					return n.nodeName(a.target, "a")
				}
			},
			beforeunload: {
				postDispatch: function (a) {
					void 0 !== a.result && a.originalEvent && (a.originalEvent.returnValue = a.result)
				}
			}
		},
		simulate: function (a, b, c) {
			var d = n.extend(new n.Event, c, {
				type: a,
				isSimulated: !0
			});
			n.event.trigger(d, null, b), d.isDefaultPrevented() && c.preventDefault()
		}
	}, n.removeEvent = d.removeEventListener ? function (a, b, c) {
		a.removeEventListener && a.removeEventListener(b, c)
	} : function (a, b, c) {
		var d = "on" + b;
		a.detachEvent && ("undefined" == typeof a[d] && (a[d] = null), a.detachEvent(d, c))
	}, n.Event = function (a, b) {
		return this instanceof n.Event ? (a && a.type ? (this.originalEvent = a, this.type = a.type, this.isDefaultPrevented = a.defaultPrevented || void 0 === a.defaultPrevented && a.returnValue === !1 ? pa : qa) : this.type = a, b && n.extend(this, b), this.timeStamp = a && a.timeStamp || n.now(), void(this[n.expando] = !0)) : new n.Event(a, b)
	}, n.Event.prototype = {
		constructor: n.Event,
		isDefaultPrevented: qa,
		isPropagationStopped: qa,
		isImmediatePropagationStopped: qa,
		preventDefault: function () {
			var a = this.originalEvent;
			this.isDefaultPrevented = pa, a && (a.preventDefault ? a.preventDefault() : a.returnValue = !1)
		},
		stopPropagation: function () {
			var a = this.originalEvent;
			this.isPropagationStopped = pa, a && !this.isSimulated && (a.stopPropagation && a.stopPropagation(), a.cancelBubble = !0)
		},
		stopImmediatePropagation: function () {
			var a = this.originalEvent;
			this.isImmediatePropagationStopped = pa, a && a.stopImmediatePropagation && a.stopImmediatePropagation(), this.stopPropagation()
		}
	}, n.each({
		mouseenter: "mouseover",
		mouseleave: "mouseout",
		pointerenter: "pointerover",
		pointerleave: "pointerout"
	}, function (a, b) {
		n.event.special[a] = {
			delegateType: b,
			bindType: b,
			handle: function (a) {
				var c, d = this,
					e = a.relatedTarget,
					f = a.handleObj;
				return e && (e === d || n.contains(d, e)) || (a.type = f.origType, c = f.handler.apply(this, arguments), a.type = b), c
			}
		}
	}), l.submit || (n.event.special.submit = {
		setup: function () {
			return n.nodeName(this, "form") ? !1 : void n.event.add(this, "click._submit keypress._submit", function (a) {
				var b = a.target,
					c = n.nodeName(b, "input") || n.nodeName(b, "button") ? n.prop(b, "form") : void 0;
				c && !n._data(c, "submit") && (n.event.add(c, "submit._submit", function (a) {
					a._submitBubble = !0
				}), n._data(c, "submit", !0))
			})
		},
		postDispatch: function (a) {
			a._submitBubble && (delete a._submitBubble, this.parentNode && !a.isTrigger && n.event.simulate("submit", this.parentNode, a))
		},
		teardown: function () {
			return n.nodeName(this, "form") ? !1 : void n.event.remove(this, "._submit")
		}
	}), l.change || (n.event.special.change = {
		setup: function () {
			return ka.test(this.nodeName) ? ("checkbox" !== this.type && "radio" !== this.type || (n.event.add(this, "propertychange._change", function (a) {
				"checked" === a.originalEvent.propertyName && (this._justChanged = !0)
			}), n.event.add(this, "click._change", function (a) {
				this._justChanged && !a.isTrigger && (this._justChanged = !1), n.event.simulate("change", this, a)
			})), !1) : void n.event.add(this, "beforeactivate._change", function (a) {
				var b = a.target;
				ka.test(b.nodeName) && !n._data(b, "change") && (n.event.add(b, "change._change", function (a) {
					!this.parentNode || a.isSimulated || a.isTrigger || n.event.simulate("change", this.parentNode, a)
				}), n._data(b, "change", !0))
			})
		},
		handle: function (a) {
			var b = a.target;
			return this !== b || a.isSimulated || a.isTrigger || "radio" !== b.type && "checkbox" !== b.type ? a.handleObj.handler.apply(this, arguments) : void 0
		},
		teardown: function () {
			return n.event.remove(this, "._change"), !ka.test(this.nodeName)
		}
	}), l.focusin || n.each({
		focus: "focusin",
		blur: "focusout"
	}, function (a, b) {
		var c = function (a) {
			n.event.simulate(b, a.target, n.event.fix(a))
		};
		n.event.special[b] = {
			setup: function () {
				var d = this.ownerDocument || this,
					e = n._data(d, b);
				e || d.addEventListener(a, c, !0), n._data(d, b, (e || 0) + 1)
			},
			teardown: function () {
				var d = this.ownerDocument || this,
					e = n._data(d, b) - 1;
				e ? n._data(d, b, e) : (d.removeEventListener(a, c, !0), n._removeData(d, b))
			}
		}
	}), n.fn.extend({
		on: function (a, b, c, d) {
			return sa(this, a, b, c, d)
		},
		one: function (a, b, c, d) {
			return sa(this, a, b, c, d, 1)
		},
		off: function (a, b, c) {
			var d, e;
			if (a && a.preventDefault && a.handleObj) return d = a.handleObj, n(a.delegateTarget).off(d.namespace ? d.origType + "." + d.namespace : d.origType, d.selector, d.handler), this;
			if ("object" == typeof a) {
				for (e in a) this.off(e, b, a[e]);
				return this
			}
			return b !== !1 && "function" != typeof b || (c = b, b = void 0), c === !1 && (c = qa), this.each(function () {
				n.event.remove(this, a, c, b)
			})
		},
		trigger: function (a, b) {
			return this.each(function () {
				n.event.trigger(a, b, this)
			})
		},
		triggerHandler: function (a, b) {
			var c = this[0];
			return c ? n.event.trigger(a, b, c, !0) : void 0
		}
	});
	var ta = / jQuery\d+="(?:null|\d+)"/g,
		ua = new RegExp("<(?:" + ba + ")[\\s/>]", "i"),
		va = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:-]+)[^>]*)\/>/gi,
		wa = /<script|<style|<link/i,
		xa = /checked\s*(?:[^=]|=\s*.checked.)/i,
		ya = /^true\/(.*)/,
		za = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,
		Aa = ca(d),
		Ba = Aa.appendChild(d.createElement("div"));

	function Ca(a, b) {
		return n.nodeName(a, "table") && n.nodeName(11 !== b.nodeType ? b : b.firstChild, "tr") ? a.getElementsByTagName("tbody")[0] || a.appendChild(a.ownerDocument.createElement("tbody")) : a
	}

	function Da(a) {
		return a.type = (null !== n.find.attr(a, "type")) + "/" + a.type, a
	}

	function Ea(a) {
		var b = ya.exec(a.type);
		return b ? a.type = b[1] : a.removeAttribute("type"), a
	}

	function Fa(a, b) {
		if (1 === b.nodeType && n.hasData(a)) {
			var c, d, e, f = n._data(a),
				g = n._data(b, f),
				h = f.events;
			if (h) {
				delete g.handle, g.events = {};
				for (c in h)
					for (d = 0, e = h[c].length; e > d; d++) n.event.add(b, c, h[c][d])
			}
			g.data && (g.data = n.extend({}, g.data))
		}
	}

	function Ga(a, b) {
		var c, d, e;
		if (1 === b.nodeType) {
			if (c = b.nodeName.toLowerCase(), !l.noCloneEvent && b[n.expando]) {
				e = n._data(b);
				for (d in e.events) n.removeEvent(b, d, e.handle);
				b.removeAttribute(n.expando)
			}
			"script" === c && b.text !== a.text ? (Da(b).text = a.text, Ea(b)) : "object" === c ? (b.parentNode && (b.outerHTML = a.outerHTML), l.html5Clone && a.innerHTML && !n.trim(b.innerHTML) && (b.innerHTML = a.innerHTML)) : "input" === c && Z.test(a.type) ? (b.defaultChecked = b.checked = a.checked, b.value !== a.value && (b.value = a.value)) : "option" === c ? b.defaultSelected = b.selected = a.defaultSelected : "input" !== c && "textarea" !== c || (b.defaultValue = a.defaultValue)
		}
	}

	function Ha(a, b, c, d) {
		b = f.apply([], b);
		var e, g, h, i, j, k, m = 0,
			o = a.length,
			p = o - 1,
			q = b[0],
			r = n.isFunction(q);
		if (r || o > 1 && "string" == typeof q && !l.checkClone && xa.test(q)) return a.each(function (e) {
			var f = a.eq(e);
			r && (b[0] = q.call(this, e, f.html())), Ha(f, b, c, d)
		});
		if (o && (k = ja(b, a[0].ownerDocument, !1, a, d), e = k.firstChild, 1 === k.childNodes.length && (k = e), e || d)) {
			for (i = n.map(ea(k, "script"), Da), h = i.length; o > m; m++) g = k, m !== p && (g = n.clone(g, !0, !0), h && n.merge(i, ea(g, "script"))), c.call(a[m], g, m);
			if (h)
				for (j = i[i.length - 1].ownerDocument, n.map(i, Ea), m = 0; h > m; m++) g = i[m], _.test(g.type || "") && !n._data(g, "globalEval") && n.contains(j, g) && (g.src ? n._evalUrl && n._evalUrl(g.src) : n.globalEval((g.text || g.textContent || g.innerHTML || "").replace(za, "")));
			k = e = null
		}
		return a
	}

	function Ia(a, b, c) {
		for (var d, e = b ? n.filter(b, a) : a, f = 0; null != (d = e[f]); f++) c || 1 !== d.nodeType || n.cleanData(ea(d)), d.parentNode && (c && n.contains(d.ownerDocument, d) && fa(ea(d, "script")), d.parentNode.removeChild(d));
		return a
	}
	n.extend({
		htmlPrefilter: function (a) {
			return a.replace(va, "<$1></$2>")
		},
		clone: function (a, b, c) {
			var d, e, f, g, h, i = n.contains(a.ownerDocument, a);
			if (l.html5Clone || n.isXMLDoc(a) || !ua.test("<" + a.nodeName + ">") ? f = a.cloneNode(!0) : (Ba.innerHTML = a.outerHTML, Ba.removeChild(f = Ba.firstChild)), !(l.noCloneEvent && l.noCloneChecked || 1 !== a.nodeType && 11 !== a.nodeType || n.isXMLDoc(a)))
				for (d = ea(f), h = ea(a), g = 0; null != (e = h[g]); ++g) d[g] && Ga(e, d[g]);
			if (b)
				if (c)
					for (h = h || ea(a), d = d || ea(f), g = 0; null != (e = h[g]); g++) Fa(e, d[g]);
				else Fa(a, f);
			return d = ea(f, "script"), d.length > 0 && fa(d, !i && ea(a, "script")), d = h = e = null, f
		},
		cleanData: function (a, b) {
			for (var d, e, f, g, h = 0, i = n.expando, j = n.cache, k = l.attributes, m = n.event.special; null != (d = a[h]); h++)
				if ((b || M(d)) && (f = d[i], g = f && j[f])) {
					if (g.events)
						for (e in g.events) m[e] ? n.event.remove(d, e) : n.removeEvent(d, e, g.handle);
					j[f] && (delete j[f], k || "undefined" == typeof d.removeAttribute ? d[i] = void 0 : d.removeAttribute(i), c.push(f))
				}
		}
	}), n.fn.extend({
		domManip: Ha,
		detach: function (a) {
			return Ia(this, a, !0)
		},
		remove: function (a) {
			return Ia(this, a)
		},
		text: function (a) {
			return Y(this, function (a) {
				return void 0 === a ? n.text(this) : this.empty().append((this[0] && this[0].ownerDocument || d).createTextNode(a))
			}, null, a, arguments.length)
		},
		append: function () {
			return Ha(this, arguments, function (a) {
				if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
					var b = Ca(this, a);
					b.appendChild(a)
				}
			})
		},
		prepend: function () {
			return Ha(this, arguments, function (a) {
				if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
					var b = Ca(this, a);
					b.insertBefore(a, b.firstChild)
				}
			})
		},
		before: function () {
			return Ha(this, arguments, function (a) {
				this.parentNode && this.parentNode.insertBefore(a, this)
			})
		},
		after: function () {
			return Ha(this, arguments, function (a) {
				this.parentNode && this.parentNode.insertBefore(a, this.nextSibling)
			})
		},
		empty: function () {
			for (var a, b = 0; null != (a = this[b]); b++) {
				1 === a.nodeType && n.cleanData(ea(a, !1));
				while (a.firstChild) a.removeChild(a.firstChild);
				a.options && n.nodeName(a, "select") && (a.options.length = 0)
			}
			return this
		},
		clone: function (a, b) {
			return a = null == a ? !1 : a, b = null == b ? a : b, this.map(function () {
				return n.clone(this, a, b)
			})
		},
		html: function (a) {
			return Y(this, function (a) {
				var b = this[0] || {},
					c = 0,
					d = this.length;
				if (void 0 === a) return 1 === b.nodeType ? b.innerHTML.replace(ta, "") : void 0;
				if ("string" == typeof a && !wa.test(a) && (l.htmlSerialize || !ua.test(a)) && (l.leadingWhitespace || !aa.test(a)) && !da[($.exec(a) || ["", ""])[1].toLowerCase()]) {
					a = n.htmlPrefilter(a);
					try {
						for (; d > c; c++) b = this[c] || {}, 1 === b.nodeType && (n.cleanData(ea(b, !1)), b.innerHTML = a);
						b = 0
					} catch (e) {}
				}
				b && this.empty().append(a)
			}, null, a, arguments.length)
		},
		replaceWith: function () {
			var a = [];
			return Ha(this, arguments, function (b) {
				var c = this.parentNode;
				n.inArray(this, a) < 0 && (n.cleanData(ea(this)), c && c.replaceChild(b, this))
			}, a)
		}
	}), n.each({
		appendTo: "append",
		prependTo: "prepend",
		insertBefore: "before",
		insertAfter: "after",
		replaceAll: "replaceWith"
	}, function (a, b) {
		n.fn[a] = function (a) {
			for (var c, d = 0, e = [], f = n(a), h = f.length - 1; h >= d; d++) c = d === h ? this : this.clone(!0), n(f[d])[b](c), g.apply(e, c.get());
			return this.pushStack(e)
		}
	});
	var Ja, Ka = {
		HTML: "block",
		BODY: "block"
	};

	function La(a, b) {
		var c = n(b.createElement(a)).appendTo(b.body),
			d = n.css(c[0], "display");
		return c.detach(), d
	}

	function Ma(a) {
		var b = d,
			c = Ka[a];
		return c || (c = La(a, b), "none" !== c && c || (Ja = (Ja || n("<iframe frameborder='0' width='0' height='0'/>")).appendTo(b.documentElement), b = (Ja[0].contentWindow || Ja[0].contentDocument).document, b.write(), b.close(), c = La(a, b), Ja.detach()), Ka[a] = c), c
	}
	var Na = /^margin/,
		Oa = new RegExp("^(" + T + ")(?!px)[a-z%]+$", "i"),
		Pa = function (a, b, c, d) {
			var e, f, g = {};
			for (f in b) g[f] = a.style[f], a.style[f] = b[f];
			e = c.apply(a, d || []);
			for (f in b) a.style[f] = g[f];
			return e
		},
		Qa = d.documentElement;
	! function () {
		var b, c, e, f, g, h, i = d.createElement("div"),
			j = d.createElement("div");
		if (j.style) {
			j.style.cssText = "float:left;opacity:.5", l.opacity = "0.5" === j.style.opacity, l.cssFloat = !!j.style.cssFloat, j.style.backgroundClip = "content-box", j.cloneNode(!0).style.backgroundClip = "", l.clearCloneStyle = "content-box" === j.style.backgroundClip, i = d.createElement("div"), i.style.cssText = "border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute", j.innerHTML = "", i.appendChild(j), l.boxSizing = "" === j.style.boxSizing || "" === j.style.MozBoxSizing || "" === j.style.WebkitBoxSizing, n.extend(l, {
				reliableHiddenOffsets: function () {
					return null == b && k(), f
				},
				boxSizingReliable: function () {
					return null == b && k(), e
				},
				pixelMarginRight: function () {
					return null == b && k(), c
				},
				pixelPosition: function () {
					return null == b && k(), b
				},
				reliableMarginRight: function () {
					return null == b && k(), g
				},
				reliableMarginLeft: function () {
					return null == b && k(), h
				}
			});

			function k() {
				var k, l, m = d.documentElement;
				m.appendChild(i), j.style.cssText = "-webkit-box-sizing:border-box;box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%", b = e = h = !1, c = g = !0, a.getComputedStyle && (l = a.getComputedStyle(j), b = "1%" !== (l || {}).top, h = "2px" === (l || {}).marginLeft, e = "4px" === (l || {
					width: "4px"
				}).width, j.style.marginRight = "50%", c = "4px" === (l || {
					marginRight: "4px"
				}).marginRight, k = j.appendChild(d.createElement("div")), k.style.cssText = j.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0", k.style.marginRight = k.style.width = "0", j.style.width = "1px", g = !parseFloat((a.getComputedStyle(k) || {}).marginRight), j.removeChild(k)), j.style.display = "none", f = 0 === j.getClientRects().length, f && (j.style.display = "", j.innerHTML = "<table><tr><td></td><td>t</td></tr></table>", j.childNodes[0].style.borderCollapse = "separate", k = j.getElementsByTagName("td"), k[0].style.cssText = "margin:0;border:0;padding:0;display:none", f = 0 === k[0].offsetHeight, f && (k[0].style.display = "", k[1].style.display = "none", f = 0 === k[0].offsetHeight)), m.removeChild(i)
			}
		}
	}();
	var Ra, Sa, Ta = /^(top|right|bottom|left)$/;
	a.getComputedStyle ? (Ra = function (b) {
		var c = b.ownerDocument.defaultView;
		return c && c.opener || (c = a), c.getComputedStyle(b)
	}, Sa = function (a, b, c) {
		var d, e, f, g, h = a.style;
		return c = c || Ra(a), g = c ? c.getPropertyValue(b) || c[b] : void 0, "" !== g && void 0 !== g || n.contains(a.ownerDocument, a) || (g = n.style(a, b)), c && !l.pixelMarginRight() && Oa.test(g) && Na.test(b) && (d = h.width, e = h.minWidth, f = h.maxWidth, h.minWidth = h.maxWidth = h.width = g, g = c.width, h.width = d, h.minWidth = e, h.maxWidth = f), void 0 === g ? g : g + ""
	}) : Qa.currentStyle && (Ra = function (a) {
		return a.currentStyle
	}, Sa = function (a, b, c) {
		var d, e, f, g, h = a.style;
		return c = c || Ra(a), g = c ? c[b] : void 0, null == g && h && h[b] && (g = h[b]), Oa.test(g) && !Ta.test(b) && (d = h.left, e = a.runtimeStyle, f = e && e.left, f && (e.left = a.currentStyle.left), h.left = "fontSize" === b ? "1em" : g, g = h.pixelLeft + "px", h.left = d, f && (e.left = f)), void 0 === g ? g : g + "" || "auto"
	});

	function Ua(a, b) {
		return {
			get: function () {
				return a() ? void delete this.get : (this.get = b).apply(this, arguments)
			}
		}
	}
	var Va = /alpha\([^)]*\)/i,
		Wa = /opacity\s*=\s*([^)]*)/i,
		Xa = /^(none|table(?!-c[ea]).+)/,
		Ya = new RegExp("^(" + T + ")(.*)$", "i"),
		Za = {
			position: "absolute",
			visibility: "hidden",
			display: "block"
		},
		$a = {
			letterSpacing: "0",
			fontWeight: "400"
		},
		_a = ["Webkit", "O", "Moz", "ms"],
		ab = d.createElement("div").style;

	function bb(a) {
		if (a in ab) return a;
		var b = a.charAt(0).toUpperCase() + a.slice(1),
			c = _a.length;
		while (c--)
			if (a = _a[c] + b, a in ab) return a
	}

	function cb(a, b) {
		for (var c, d, e, f = [], g = 0, h = a.length; h > g; g++) d = a[g], d.style && (f[g] = n._data(d, "olddisplay"), c = d.style.display, b ? (f[g] || "none" !== c || (d.style.display = ""), "" === d.style.display && W(d) && (f[g] = n._data(d, "olddisplay", Ma(d.nodeName)))) : (e = W(d), (c && "none" !== c || !e) && n._data(d, "olddisplay", e ? c : n.css(d, "display"))));
		for (g = 0; h > g; g++) d = a[g], d.style && (b && "none" !== d.style.display && "" !== d.style.display || (d.style.display = b ? f[g] || "" : "none"));
		return a
	}

	function db(a, b, c) {
		var d = Ya.exec(b);
		return d ? Math.max(0, d[1] - (c || 0)) + (d[2] || "px") : b
	}

	function eb(a, b, c, d, e) {
		for (var f = c === (d ? "border" : "content") ? 4 : "width" === b ? 1 : 0, g = 0; 4 > f; f += 2) "margin" === c && (g += n.css(a, c + V[f], !0, e)), d ? ("content" === c && (g -= n.css(a, "padding" + V[f], !0, e)), "margin" !== c && (g -= n.css(a, "border" + V[f] + "Width", !0, e))) : (g += n.css(a, "padding" + V[f], !0, e), "padding" !== c && (g += n.css(a, "border" + V[f] + "Width", !0, e)));
		return g
	}

	function fb(a, b, c) {
		var d = !0,
			e = "width" === b ? a.offsetWidth : a.offsetHeight,
			f = Ra(a),
			g = l.boxSizing && "border-box" === n.css(a, "boxSizing", !1, f);
		if (0 >= e || null == e) {
			if (e = Sa(a, b, f), (0 > e || null == e) && (e = a.style[b]), Oa.test(e)) return e;
			d = g && (l.boxSizingReliable() || e === a.style[b]), e = parseFloat(e) || 0
		}
		return e + eb(a, b, c || (g ? "border" : "content"), d, f) + "px"
	}
	n.extend({
		cssHooks: {
			opacity: {
				get: function (a, b) {
					if (b) {
						var c = Sa(a, "opacity");
						return "" === c ? "1" : c
					}
				}
			}
		},
		cssNumber: {
			animationIterationCount: !0,
			columnCount: !0,
			fillOpacity: !0,
			flexGrow: !0,
			flexShrink: !0,
			fontWeight: !0,
			lineHeight: !0,
			opacity: !0,
			order: !0,
			orphans: !0,
			widows: !0,
			zIndex: !0,
			zoom: !0
		},
		cssProps: {
			"float": l.cssFloat ? "cssFloat" : "styleFloat"
		},
		style: function (a, b, c, d) {
			if (a && 3 !== a.nodeType && 8 !== a.nodeType && a.style) {
				var e, f, g, h = n.camelCase(b),
					i = a.style;
				if (b = n.cssProps[h] || (n.cssProps[h] = bb(h) || h), g = n.cssHooks[b] || n.cssHooks[h], void 0 === c) return g && "get" in g && void 0 !== (e = g.get(a, !1, d)) ? e : i[b];
				if (f = typeof c, "string" === f && (e = U.exec(c)) && e[1] && (c = X(a, b, e), f = "number"), null != c && c === c && ("number" === f && (c += e && e[3] || (n.cssNumber[h] ? "" : "px")), l.clearCloneStyle || "" !== c || 0 !== b.indexOf("background") || (i[b] = "inherit"), !(g && "set" in g && void 0 === (c = g.set(a, c, d))))) try {
					i[b] = c
				} catch (j) {}
			}
		},
		css: function (a, b, c, d) {
			var e, f, g, h = n.camelCase(b);
			return b = n.cssProps[h] || (n.cssProps[h] = bb(h) || h), g = n.cssHooks[b] || n.cssHooks[h], g && "get" in g && (f = g.get(a, !0, c)), void 0 === f && (f = Sa(a, b, d)), "normal" === f && b in $a && (f = $a[b]), "" === c || c ? (e = parseFloat(f), c === !0 || isFinite(e) ? e || 0 : f) : f
		}
	}), n.each(["height", "width"], function (a, b) {
		n.cssHooks[b] = {
			get: function (a, c, d) {
				return c ? Xa.test(n.css(a, "display")) && 0 === a.offsetWidth ? Pa(a, Za, function () {
					return fb(a, b, d)
				}) : fb(a, b, d) : void 0
			},
			set: function (a, c, d) {
				var e = d && Ra(a);
				return db(a, c, d ? eb(a, b, d, l.boxSizing && "border-box" === n.css(a, "boxSizing", !1, e), e) : 0)
			}
		}
	}), l.opacity || (n.cssHooks.opacity = {
		get: function (a, b) {
			return Wa.test((b && a.currentStyle ? a.currentStyle.filter : a.style.filter) || "") ? .01 * parseFloat(RegExp.$1) + "" : b ? "1" : ""
		},
		set: function (a, b) {
			var c = a.style,
				d = a.currentStyle,
				e = n.isNumeric(b) ? "alpha(opacity=" + 100 * b + ")" : "",
				f = d && d.filter || c.filter || "";
			c.zoom = 1, (b >= 1 || "" === b) && "" === n.trim(f.replace(Va, "")) && c.removeAttribute && (c.removeAttribute("filter"), "" === b || d && !d.filter) || (c.filter = Va.test(f) ? f.replace(Va, e) : f + " " + e)
		}
	}), n.cssHooks.marginRight = Ua(l.reliableMarginRight, function (a, b) {
		return b ? Pa(a, {
			display: "inline-block"
		}, Sa, [a, "marginRight"]) : void 0
	}), n.cssHooks.marginLeft = Ua(l.reliableMarginLeft, function (a, b) {
		return b ? (parseFloat(Sa(a, "marginLeft")) || (n.contains(a.ownerDocument, a) ? a.getBoundingClientRect().left - Pa(a, {
			marginLeft: 0
		}, function () {
			return a.getBoundingClientRect().left
		}) : 0)) + "px" : void 0
	}), n.each({
		margin: "",
		padding: "",
		border: "Width"
	}, function (a, b) {
		n.cssHooks[a + b] = {
			expand: function (c) {
				for (var d = 0, e = {}, f = "string" == typeof c ? c.split(" ") : [c]; 4 > d; d++) e[a + V[d] + b] = f[d] || f[d - 2] || f[0];
				return e
			}
		}, Na.test(a) || (n.cssHooks[a + b].set = db)
	}), n.fn.extend({
		css: function (a, b) {
			return Y(this, function (a, b, c) {
				var d, e, f = {},
					g = 0;
				if (n.isArray(b)) {
					for (d = Ra(a), e = b.length; e > g; g++) f[b[g]] = n.css(a, b[g], !1, d);
					return f
				}
				return void 0 !== c ? n.style(a, b, c) : n.css(a, b)
			}, a, b, arguments.length > 1)
		},
		show: function () {
			return cb(this, !0)
		},
		hide: function () {
			return cb(this)
		},
		toggle: function (a) {
			return "boolean" == typeof a ? a ? this.show() : this.hide() : this.each(function () {
				W(this) ? n(this).show() : n(this).hide()
			})
		}
	});

	function gb(a, b, c, d, e) {
		return new gb.prototype.init(a, b, c, d, e)
	}
	n.Tween = gb, gb.prototype = {
		constructor: gb,
		init: function (a, b, c, d, e, f) {
			this.elem = a, this.prop = c, this.easing = e || n.easing._default, this.options = b, this.start = this.now = this.cur(), this.end = d, this.unit = f || (n.cssNumber[c] ? "" : "px")
		},
		cur: function () {
			var a = gb.propHooks[this.prop];
			return a && a.get ? a.get(this) : gb.propHooks._default.get(this)
		},
		run: function (a) {
			var b, c = gb.propHooks[this.prop];
			return this.options.duration ? this.pos = b = n.easing[this.easing](a, this.options.duration * a, 0, 1, this.options.duration) : this.pos = b = a, this.now = (this.end - this.start) * b + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), c && c.set ? c.set(this) : gb.propHooks._default.set(this), this
		}
	}, gb.prototype.init.prototype = gb.prototype, gb.propHooks = {
		_default: {
			get: function (a) {
				var b;
				return 1 !== a.elem.nodeType || null != a.elem[a.prop] && null == a.elem.style[a.prop] ? a.elem[a.prop] : (b = n.css(a.elem, a.prop, ""), b && "auto" !== b ? b : 0)
			},
			set: function (a) {
				n.fx.step[a.prop] ? n.fx.step[a.prop](a) : 1 !== a.elem.nodeType || null == a.elem.style[n.cssProps[a.prop]] && !n.cssHooks[a.prop] ? a.elem[a.prop] = a.now : n.style(a.elem, a.prop, a.now + a.unit)
			}
		}
	}, gb.propHooks.scrollTop = gb.propHooks.scrollLeft = {
		set: function (a) {
			a.elem.nodeType && a.elem.parentNode && (a.elem[a.prop] = a.now)
		}
	}, n.easing = {
		linear: function (a) {
			return a
		},
		swing: function (a) {
			return .5 - Math.cos(a * Math.PI) / 2
		},
		_default: "swing"
	}, n.fx = gb.prototype.init, n.fx.step = {};
	var hb, ib, jb = /^(?:toggle|show|hide)$/,
		kb = /queueHooks$/;

	function lb() {
		return a.setTimeout(function () {
			hb = void 0
		}), hb = n.now()
	}

	function mb(a, b) {
		var c, d = {
				height: a
			},
			e = 0;
		for (b = b ? 1 : 0; 4 > e; e += 2 - b) c = V[e], d["margin" + c] = d["padding" + c] = a;
		return b && (d.opacity = d.width = a), d
	}

	function nb(a, b, c) {
		for (var d, e = (qb.tweeners[b] || []).concat(qb.tweeners["*"]), f = 0, g = e.length; g > f; f++)
			if (d = e[f].call(c, b, a)) return d
	}

	function ob(a, b, c) {
		var d, e, f, g, h, i, j, k, m = this,
			o = {},
			p = a.style,
			q = a.nodeType && W(a),
			r = n._data(a, "fxshow");
		c.queue || (h = n._queueHooks(a, "fx"), null == h.unqueued && (h.unqueued = 0, i = h.empty.fire, h.empty.fire = function () {
			h.unqueued || i()
		}), h.unqueued++, m.always(function () {
			m.always(function () {
				h.unqueued--, n.queue(a, "fx").length || h.empty.fire()
			})
		})), 1 === a.nodeType && ("height" in b || "width" in b) && (c.overflow = [p.overflow, p.overflowX, p.overflowY], j = n.css(a, "display"), k = "none" === j ? n._data(a, "olddisplay") || Ma(a.nodeName) : j, "inline" === k && "none" === n.css(a, "float") && (l.inlineBlockNeedsLayout && "inline" !== Ma(a.nodeName) ? p.zoom = 1 : p.display = "inline-block")), c.overflow && (p.overflow = "hidden", l.shrinkWrapBlocks() || m.always(function () {
			p.overflow = c.overflow[0], p.overflowX = c.overflow[1], p.overflowY = c.overflow[2]
		}));
		for (d in b)
			if (e = b[d], jb.exec(e)) {
				if (delete b[d], f = f || "toggle" === e, e === (q ? "hide" : "show")) {
					if ("show" !== e || !r || void 0 === r[d]) continue;
					q = !0
				}
				o[d] = r && r[d] || n.style(a, d)
			} else j = void 0;
		if (n.isEmptyObject(o)) "inline" === ("none" === j ? Ma(a.nodeName) : j) && (p.display = j);
		else {
			r ? "hidden" in r && (q = r.hidden) : r = n._data(a, "fxshow", {}), f && (r.hidden = !q), q ? n(a).show() : m.done(function () {
				n(a).hide()
			}), m.done(function () {
				var b;
				n._removeData(a, "fxshow");
				for (b in o) n.style(a, b, o[b])
			});
			for (d in o) g = nb(q ? r[d] : 0, d, m), d in r || (r[d] = g.start, q && (g.end = g.start, g.start = "width" === d || "height" === d ? 1 : 0))
		}
	}

	function pb(a, b) {
		var c, d, e, f, g;
		for (c in a)
			if (d = n.camelCase(c), e = b[d], f = a[c], n.isArray(f) && (e = f[1], f = a[c] = f[0]), c !== d && (a[d] = f, delete a[c]), g = n.cssHooks[d], g && "expand" in g) {
				f = g.expand(f), delete a[d];
				for (c in f) c in a || (a[c] = f[c], b[c] = e)
			} else b[d] = e
	}

	function qb(a, b, c) {
		var d, e, f = 0,
			g = qb.prefilters.length,
			h = n.Deferred().always(function () {
				delete i.elem
			}),
			i = function () {
				if (e) return !1;
				for (var b = hb || lb(), c = Math.max(0, j.startTime + j.duration - b), d = c / j.duration || 0, f = 1 - d, g = 0, i = j.tweens.length; i > g; g++) j.tweens[g].run(f);
				return h.notifyWith(a, [j, f, c]), 1 > f && i ? c : (h.resolveWith(a, [j]), !1)
			},
			j = h.promise({
				elem: a,
				props: n.extend({}, b),
				opts: n.extend(!0, {
					specialEasing: {},
					easing: n.easing._default
				}, c),
				originalProperties: b,
				originalOptions: c,
				startTime: hb || lb(),
				duration: c.duration,
				tweens: [],
				createTween: function (b, c) {
					var d = n.Tween(a, j.opts, b, c, j.opts.specialEasing[b] || j.opts.easing);
					return j.tweens.push(d), d
				},
				stop: function (b) {
					var c = 0,
						d = b ? j.tweens.length : 0;
					if (e) return this;
					for (e = !0; d > c; c++) j.tweens[c].run(1);
					return b ? (h.notifyWith(a, [j, 1, 0]), h.resolveWith(a, [j, b])) : h.rejectWith(a, [j, b]), this
				}
			}),
			k = j.props;
		for (pb(k, j.opts.specialEasing); g > f; f++)
			if (d = qb.prefilters[f].call(j, a, k, j.opts)) return n.isFunction(d.stop) && (n._queueHooks(j.elem, j.opts.queue).stop = n.proxy(d.stop, d)), d;
		return n.map(k, nb, j), n.isFunction(j.opts.start) && j.opts.start.call(a, j), n.fx.timer(n.extend(i, {
			elem: a,
			anim: j,
			queue: j.opts.queue
		})), j.progress(j.opts.progress).done(j.opts.done, j.opts.complete).fail(j.opts.fail).always(j.opts.always)
	}
	n.Animation = n.extend(qb, {
			tweeners: {
				"*": [function (a, b) {
					var c = this.createTween(a, b);
					return X(c.elem, a, U.exec(b), c), c
				}]
			},
			tweener: function (a, b) {
				n.isFunction(a) ? (b = a, a = ["*"]) : a = a.match(G);
				for (var c, d = 0, e = a.length; e > d; d++) c = a[d], qb.tweeners[c] = qb.tweeners[c] || [], qb.tweeners[c].unshift(b)
			},
			prefilters: [ob],
			prefilter: function (a, b) {
				b ? qb.prefilters.unshift(a) : qb.prefilters.push(a)
			}
		}), n.speed = function (a, b, c) {
			var d = a && "object" == typeof a ? n.extend({}, a) : {
				complete: c || !c && b || n.isFunction(a) && a,
				duration: a,
				easing: c && b || b && !n.isFunction(b) && b
			};
			return d.duration = n.fx.off ? 0 : "number" == typeof d.duration ? d.duration : d.duration in n.fx.speeds ? n.fx.speeds[d.duration] : n.fx.speeds._default, null != d.queue && d.queue !== !0 || (d.queue = "fx"), d.old = d.complete, d.complete = function () {
				n.isFunction(d.old) && d.old.call(this), d.queue && n.dequeue(this, d.queue)
			}, d
		}, n.fn.extend({
			fadeTo: function (a, b, c, d) {
				return this.filter(W).css("opacity", 0).show().end().animate({
					opacity: b
				}, a, c, d)
			},
			animate: function (a, b, c, d) {
				var e = n.isEmptyObject(a),
					f = n.speed(b, c, d),
					g = function () {
						var b = qb(this, n.extend({}, a), f);
						(e || n._data(this, "finish")) && b.stop(!0)
					};
				return g.finish = g, e || f.queue === !1 ? this.each(g) : this.queue(f.queue, g)
			},
			stop: function (a, b, c) {
				var d = function (a) {
					var b = a.stop;
					delete a.stop, b(c)
				};
				return "string" != typeof a && (c = b, b = a, a = void 0), b && a !== !1 && this.queue(a || "fx", []), this.each(function () {
					var b = !0,
						e = null != a && a + "queueHooks",
						f = n.timers,
						g = n._data(this);
					if (e) g[e] && g[e].stop && d(g[e]);
					else
						for (e in g) g[e] && g[e].stop && kb.test(e) && d(g[e]);
					for (e = f.length; e--;) f[e].elem !== this || null != a && f[e].queue !== a || (f[e].anim.stop(c), b = !1, f.splice(e, 1));
					!b && c || n.dequeue(this, a)
				})
			},
			finish: function (a) {
				return a !== !1 && (a = a || "fx"), this.each(function () {
					var b, c = n._data(this),
						d = c[a + "queue"],
						e = c[a + "queueHooks"],
						f = n.timers,
						g = d ? d.length : 0;
					for (c.finish = !0, n.queue(this, a, []), e && e.stop && e.stop.call(this, !0), b = f.length; b--;) f[b].elem === this && f[b].queue === a && (f[b].anim.stop(!0), f.splice(b, 1));
					for (b = 0; g > b; b++) d[b] && d[b].finish && d[b].finish.call(this);
					delete c.finish
				})
			}
		}), n.each(["toggle", "show", "hide"], function (a, b) {
			var c = n.fn[b];
			n.fn[b] = function (a, d, e) {
				return null == a || "boolean" == typeof a ? c.apply(this, arguments) : this.animate(mb(b, !0), a, d, e)
			}
		}), n.each({
			slideDown: mb("show"),
			slideUp: mb("hide"),
			slideToggle: mb("toggle"),
			fadeIn: {
				opacity: "show"
			},
			fadeOut: {
				opacity: "hide"
			},
			fadeToggle: {
				opacity: "toggle"
			}
		}, function (a, b) {
			n.fn[a] = function (a, c, d) {
				return this.animate(b, a, c, d)
			}
		}), n.timers = [], n.fx.tick = function () {
			var a, b = n.timers,
				c = 0;
			for (hb = n.now(); c < b.length; c++) a = b[c], a() || b[c] !== a || b.splice(c--, 1);
			b.length || n.fx.stop(), hb = void 0
		}, n.fx.timer = function (a) {
			n.timers.push(a), a() ? n.fx.start() : n.timers.pop()
		}, n.fx.interval = 13, n.fx.start = function () {
			ib || (ib = a.setInterval(n.fx.tick, n.fx.interval))
		}, n.fx.stop = function () {
			a.clearInterval(ib), ib = null
		}, n.fx.speeds = {
			slow: 600,
			fast: 200,
			_default: 400
		}, n.fn.delay = function (b, c) {
			return b = n.fx ? n.fx.speeds[b] || b : b, c = c || "fx", this.queue(c, function (c, d) {
				var e = a.setTimeout(c, b);
				d.stop = function () {
					a.clearTimeout(e)
				}
			})
		},
		function () {
			var a, b = d.createElement("input"),
				c = d.createElement("div"),
				e = d.createElement("select"),
				f = e.appendChild(d.createElement("option"));
			c = d.createElement("div"), c.setAttribute("className", "t"), c.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", a = c.getElementsByTagName("a")[0], b.setAttribute("type", "checkbox"), c.appendChild(b), a = c.getElementsByTagName("a")[0], a.style.cssText = "top:1px", l.getSetAttribute = "t" !== c.className, l.style = /top/.test(a.getAttribute("style")), l.hrefNormalized = "/a" === a.getAttribute("href"), l.checkOn = !!b.value, l.optSelected = f.selected, l.enctype = !!d.createElement("form").enctype, e.disabled = !0, l.optDisabled = !f.disabled, b = d.createElement("input"), b.setAttribute("value", ""), l.input = "" === b.getAttribute("value"), b.value = "t", b.setAttribute("type", "radio"), l.radioValue = "t" === b.value
		}();
	var rb = /\r/g,
		sb = /[\x20\t\r\n\f]+/g;
	n.fn.extend({
		val: function (a) {
			var b, c, d, e = this[0]; {
				if (arguments.length) return d = n.isFunction(a), this.each(function (c) {
					var e;
					1 === this.nodeType && (e = d ? a.call(this, c, n(this).val()) : a, null == e ? e = "" : "number" == typeof e ? e += "" : n.isArray(e) && (e = n.map(e, function (a) {
						return null == a ? "" : a + ""
					})), b = n.valHooks[this.type] || n.valHooks[this.nodeName.toLowerCase()], b && "set" in b && void 0 !== b.set(this, e, "value") || (this.value = e))
				});
				if (e) return b = n.valHooks[e.type] || n.valHooks[e.nodeName.toLowerCase()], b && "get" in b && void 0 !== (c = b.get(e, "value")) ? c : (c = e.value, "string" == typeof c ? c.replace(rb, "") : null == c ? "" : c)
			}
		}
	}), n.extend({
		valHooks: {
			option: {
				get: function (a) {
					var b = n.find.attr(a, "value");
					return null != b ? b : n.trim(n.text(a)).replace(sb, " ")
				}
			},
			select: {
				get: function (a) {
					for (var b, c, d = a.options, e = a.selectedIndex, f = "select-one" === a.type || 0 > e, g = f ? null : [], h = f ? e + 1 : d.length, i = 0 > e ? h : f ? e : 0; h > i; i++)
						if (c = d[i], (c.selected || i === e) && (l.optDisabled ? !c.disabled : null === c.getAttribute("disabled")) && (!c.parentNode.disabled || !n.nodeName(c.parentNode, "optgroup"))) {
							if (b = n(c).val(), f) return b;
							g.push(b)
						}
					return g
				},
				set: function (a, b) {
					var c, d, e = a.options,
						f = n.makeArray(b),
						g = e.length;
					while (g--)
						if (d = e[g], n.inArray(n.valHooks.option.get(d), f) > -1) try {
							d.selected = c = !0
						} catch (h) {
							d.scrollHeight
						} else d.selected = !1;
					return c || (a.selectedIndex = -1), e
				}
			}
		}
	}), n.each(["radio", "checkbox"], function () {
		n.valHooks[this] = {
			set: function (a, b) {
				return n.isArray(b) ? a.checked = n.inArray(n(a).val(), b) > -1 : void 0
			}
		}, l.checkOn || (n.valHooks[this].get = function (a) {
			return null === a.getAttribute("value") ? "on" : a.value
		})
	});
	var tb, ub, vb = n.expr.attrHandle,
		wb = /^(?:checked|selected)$/i,
		xb = l.getSetAttribute,
		yb = l.input;
	n.fn.extend({
		attr: function (a, b) {
			return Y(this, n.attr, a, b, arguments.length > 1)
		},
		removeAttr: function (a) {
			return this.each(function () {
				n.removeAttr(this, a)
			})
		}
	}), n.extend({
		attr: function (a, b, c) {
			var d, e, f = a.nodeType;
			if (3 !== f && 8 !== f && 2 !== f) return "undefined" == typeof a.getAttribute ? n.prop(a, b, c) : (1 === f && n.isXMLDoc(a) || (b = b.toLowerCase(), e = n.attrHooks[b] || (n.expr.match.bool.test(b) ? ub : tb)), void 0 !== c ? null === c ? void n.removeAttr(a, b) : e && "set" in e && void 0 !== (d = e.set(a, c, b)) ? d : (a.setAttribute(b, c + ""), c) : e && "get" in e && null !== (d = e.get(a, b)) ? d : (d = n.find.attr(a, b), null == d ? void 0 : d))
		},
		attrHooks: {
			type: {
				set: function (a, b) {
					if (!l.radioValue && "radio" === b && n.nodeName(a, "input")) {
						var c = a.value;
						return a.setAttribute("type", b), c && (a.value = c), b
					}
				}
			}
		},
		removeAttr: function (a, b) {
			var c, d, e = 0,
				f = b && b.match(G);
			if (f && 1 === a.nodeType)
				while (c = f[e++]) d = n.propFix[c] || c, n.expr.match.bool.test(c) ? yb && xb || !wb.test(c) ? a[d] = !1 : a[n.camelCase("default-" + c)] = a[d] = !1 : n.attr(a, c, ""), a.removeAttribute(xb ? c : d)
		}
	}), ub = {
		set: function (a, b, c) {
			return b === !1 ? n.removeAttr(a, c) : yb && xb || !wb.test(c) ? a.setAttribute(!xb && n.propFix[c] || c, c) : a[n.camelCase("default-" + c)] = a[c] = !0, c
		}
	}, n.each(n.expr.match.bool.source.match(/\w+/g), function (a, b) {
		var c = vb[b] || n.find.attr;
		yb && xb || !wb.test(b) ? vb[b] = function (a, b, d) {
			var e, f;
			return d || (f = vb[b], vb[b] = e, e = null != c(a, b, d) ? b.toLowerCase() : null, vb[b] = f), e
		} : vb[b] = function (a, b, c) {
			return c ? void 0 : a[n.camelCase("default-" + b)] ? b.toLowerCase() : null
		}
	}), yb && xb || (n.attrHooks.value = {
		set: function (a, b, c) {
			return n.nodeName(a, "input") ? void(a.defaultValue = b) : tb && tb.set(a, b, c)
		}
	}), xb || (tb = {
		set: function (a, b, c) {
			var d = a.getAttributeNode(c);
			return d || a.setAttributeNode(d = a.ownerDocument.createAttribute(c)), d.value = b += "", "value" === c || b === a.getAttribute(c) ? b : void 0
		}
	}, vb.id = vb.name = vb.coords = function (a, b, c) {
		var d;
		return c ? void 0 : (d = a.getAttributeNode(b)) && "" !== d.value ? d.value : null
	}, n.valHooks.button = {
		get: function (a, b) {
			var c = a.getAttributeNode(b);
			return c && c.specified ? c.value : void 0
		},
		set: tb.set
	}, n.attrHooks.contenteditable = {
		set: function (a, b, c) {
			tb.set(a, "" === b ? !1 : b, c)
		}
	}, n.each(["width", "height"], function (a, b) {
		n.attrHooks[b] = {
			set: function (a, c) {
				return "" === c ? (a.setAttribute(b, "auto"), c) : void 0
			}
		}
	})), l.style || (n.attrHooks.style = {
		get: function (a) {
			return a.style.cssText || void 0
		},
		set: function (a, b) {
			return a.style.cssText = b + ""
		}
	});
	var zb = /^(?:input|select|textarea|button|object)$/i,
		Ab = /^(?:a|area)$/i;
	n.fn.extend({
		prop: function (a, b) {
			return Y(this, n.prop, a, b, arguments.length > 1)
		},
		removeProp: function (a) {
			return a = n.propFix[a] || a, this.each(function () {
				try {
					this[a] = void 0, delete this[a]
				} catch (b) {}
			})
		}
	}), n.extend({
		prop: function (a, b, c) {
			var d, e, f = a.nodeType;
			if (3 !== f && 8 !== f && 2 !== f) return 1 === f && n.isXMLDoc(a) || (b = n.propFix[b] || b, e = n.propHooks[b]), void 0 !== c ? e && "set" in e && void 0 !== (d = e.set(a, c, b)) ? d : a[b] = c : e && "get" in e && null !== (d = e.get(a, b)) ? d : a[b]
		},
		propHooks: {
			tabIndex: {
				get: function (a) {
					var b = n.find.attr(a, "tabindex");
					return b ? parseInt(b, 10) : zb.test(a.nodeName) || Ab.test(a.nodeName) && a.href ? 0 : -1
				}
			}
		},
		propFix: {
			"for": "htmlFor",
			"class": "className"
		}
	}), l.hrefNormalized || n.each(["href", "src"], function (a, b) {
		n.propHooks[b] = {
			get: function (a) {
				return a.getAttribute(b, 4)
			}
		}
	}), l.optSelected || (n.propHooks.selected = {
		get: function (a) {
			var b = a.parentNode;
			return b && (b.selectedIndex, b.parentNode && b.parentNode.selectedIndex), null
		},
		set: function (a) {
			var b = a.parentNode;
			b && (b.selectedIndex, b.parentNode && b.parentNode.selectedIndex)
		}
	}), n.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function () {
		n.propFix[this.toLowerCase()] = this
	}), l.enctype || (n.propFix.enctype = "encoding");
	var Bb = /[\t\r\n\f]/g;

	function Cb(a) {
		return n.attr(a, "class") || ""
	}
	n.fn.extend({
		addClass: function (a) {
			var b, c, d, e, f, g, h, i = 0;
			if (n.isFunction(a)) return this.each(function (b) {
				n(this).addClass(a.call(this, b, Cb(this)))
			});
			if ("string" == typeof a && a) {
				b = a.match(G) || [];
				while (c = this[i++])
					if (e = Cb(c), d = 1 === c.nodeType && (" " + e + " ").replace(Bb, " ")) {
						g = 0;
						while (f = b[g++]) d.indexOf(" " + f + " ") < 0 && (d += f + " ");
						h = n.trim(d), e !== h && n.attr(c, "class", h)
					}
			}
			return this
		},
		removeClass: function (a) {
			var b, c, d, e, f, g, h, i = 0;
			if (n.isFunction(a)) return this.each(function (b) {
				n(this).removeClass(a.call(this, b, Cb(this)))
			});
			if (!arguments.length) return this.attr("class", "");
			if ("string" == typeof a && a) {
				b = a.match(G) || [];
				while (c = this[i++])
					if (e = Cb(c), d = 1 === c.nodeType && (" " + e + " ").replace(Bb, " ")) {
						g = 0;
						while (f = b[g++])
							while (d.indexOf(" " + f + " ") > -1) d = d.replace(" " + f + " ", " ");
						h = n.trim(d), e !== h && n.attr(c, "class", h)
					}
			}
			return this
		},
		toggleClass: function (a, b) {
			var c = typeof a;
			return "boolean" == typeof b && "string" === c ? b ? this.addClass(a) : this.removeClass(a) : n.isFunction(a) ? this.each(function (c) {
				n(this).toggleClass(a.call(this, c, Cb(this), b), b)
			}) : this.each(function () {
				var b, d, e, f;
				if ("string" === c) {
					d = 0, e = n(this), f = a.match(G) || [];
					while (b = f[d++]) e.hasClass(b) ? e.removeClass(b) : e.addClass(b)
				} else void 0 !== a && "boolean" !== c || (b = Cb(this), b && n._data(this, "__className__", b), n.attr(this, "class", b || a === !1 ? "" : n._data(this, "__className__") || ""))
			})
		},
		hasClass: function (a) {
			var b, c, d = 0;
			b = " " + a + " ";
			while (c = this[d++])
				if (1 === c.nodeType && (" " + Cb(c) + " ").replace(Bb, " ").indexOf(b) > -1) return !0;
			return !1
		}
	}), n.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function (a, b) {
		n.fn[b] = function (a, c) {
			return arguments.length > 0 ? this.on(b, null, a, c) : this.trigger(b)
		}
	}), n.fn.extend({
		hover: function (a, b) {
			return this.mouseenter(a).mouseleave(b || a)
		}
	});
	var Db = a.location,
		Eb = n.now(),
		Fb = /\?/,
		Gb = /(,)|(\[|{)|(}|])|"(?:[^"\\\r\n]|\\["\\\/bfnrt]|\\u[\da-fA-F]{4})*"\s*:?|true|false|null|-?(?!0\d)\d+(?:\.\d+|)(?:[eE][+-]?\d+|)/g;
	n.parseJSON = function (b) {
		if (a.JSON && a.JSON.parse) return a.JSON.parse(b + "");
		var c, d = null,
			e = n.trim(b + "");
		return e && !n.trim(e.replace(Gb, function (a, b, e, f) {
			return c && b && (d = 0), 0 === d ? a : (c = e || b, d += !f - !e, "")
		})) ? Function("return " + e)() : n.error("Invalid JSON: " + b)
	}, n.parseXML = function (b) {
		var c, d;
		if (!b || "string" != typeof b) return null;
		try {
			a.DOMParser ? (d = new a.DOMParser, c = d.parseFromString(b, "text/xml")) : (c = new a.ActiveXObject("Microsoft.XMLDOM"), c.async = "false", c.loadXML(b))
		} catch (e) {
			c = void 0
		}
		return c && c.documentElement && !c.getElementsByTagName("parsererror").length || n.error("Invalid XML: " + b), c
	};
	var Hb = /#.*$/,
		Ib = /([?&])_=[^&]*/,
		Jb = /^(.*?):[ \t]*([^\r\n]*)\r?$/gm,
		Kb = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
		Lb = /^(?:GET|HEAD)$/,
		Mb = /^\/\//,
		Nb = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,
		Ob = {},
		Pb = {},
		Qb = "*/".concat("*"),
		Rb = Db.href,
		Sb = Nb.exec(Rb.toLowerCase()) || [];

	function Tb(a) {
		return function (b, c) {
			"string" != typeof b && (c = b, b = "*");
			var d, e = 0,
				f = b.toLowerCase().match(G) || [];
			if (n.isFunction(c))
				while (d = f[e++]) "+" === d.charAt(0) ? (d = d.slice(1) || "*", (a[d] = a[d] || []).unshift(c)) : (a[d] = a[d] || []).push(c)
		}
	}

	function Ub(a, b, c, d) {
		var e = {},
			f = a === Pb;

		function g(h) {
			var i;
			return e[h] = !0, n.each(a[h] || [], function (a, h) {
				var j = h(b, c, d);
				return "string" != typeof j || f || e[j] ? f ? !(i = j) : void 0 : (b.dataTypes.unshift(j), g(j), !1)
			}), i
		}
		return g(b.dataTypes[0]) || !e["*"] && g("*")
	}

	function Vb(a, b) {
		var c, d, e = n.ajaxSettings.flatOptions || {};
		for (d in b) void 0 !== b[d] && ((e[d] ? a : c || (c = {}))[d] = b[d]);
		return c && n.extend(!0, a, c), a
	}

	function Wb(a, b, c) {
		var d, e, f, g, h = a.contents,
			i = a.dataTypes;
		while ("*" === i[0]) i.shift(), void 0 === e && (e = a.mimeType || b.getResponseHeader("Content-Type"));
		if (e)
			for (g in h)
				if (h[g] && h[g].test(e)) {
					i.unshift(g);
					break
				}
		if (i[0] in c) f = i[0];
		else {
			for (g in c) {
				if (!i[0] || a.converters[g + " " + i[0]]) {
					f = g;
					break
				}
				d || (d = g)
			}
			f = f || d
		}
		return f ? (f !== i[0] && i.unshift(f), c[f]) : void 0
	}

	function Xb(a, b, c, d) {
		var e, f, g, h, i, j = {},
			k = a.dataTypes.slice();
		if (k[1])
			for (g in a.converters) j[g.toLowerCase()] = a.converters[g];
		f = k.shift();
		while (f)
			if (a.responseFields[f] && (c[a.responseFields[f]] = b), !i && d && a.dataFilter && (b = a.dataFilter(b, a.dataType)), i = f, f = k.shift())
				if ("*" === f) f = i;
				else if ("*" !== i && i !== f) {
			if (g = j[i + " " + f] || j["* " + f], !g)
				for (e in j)
					if (h = e.split(" "), h[1] === f && (g = j[i + " " + h[0]] || j["* " + h[0]])) {
						g === !0 ? g = j[e] : j[e] !== !0 && (f = h[0], k.unshift(h[1]));
						break
					}
			if (g !== !0)
				if (g && a["throws"]) b = g(b);
				else try {
					b = g(b)
				} catch (l) {
					return {
						state: "parsererror",
						error: g ? l : "No conversion from " + i + " to " + f
					}
				}
		}
		return {
			state: "success",
			data: b
		}
	}
	n.extend({
		active: 0,
		lastModified: {},
		etag: {},
		ajaxSettings: {
			url: Rb,
			type: "GET",
			isLocal: Kb.test(Sb[1]),
			global: !0,
			processData: !0,
			async: !0,
			contentType: "application/x-www-form-urlencoded; charset=UTF-8",
			accepts: {
				"*": Qb,
				text: "text/plain",
				html: "text/html",
				xml: "application/xml, text/xml",
				json: "application/json, text/javascript"
			},
			contents: {
				xml: /\bxml\b/,
				html: /\bhtml/,
				json: /\bjson\b/
			},
			responseFields: {
				xml: "responseXML",
				text: "responseText",
				json: "responseJSON"
			},
			converters: {
				"* text": String,
				"text html": !0,
				"text json": n.parseJSON,
				"text xml": n.parseXML
			},
			flatOptions: {
				url: !0,
				context: !0
			}
		},
		ajaxSetup: function (a, b) {
			return b ? Vb(Vb(a, n.ajaxSettings), b) : Vb(n.ajaxSettings, a)
		},
		ajaxPrefilter: Tb(Ob),
		ajaxTransport: Tb(Pb),
		ajax: function (b, c) {
			"object" == typeof b && (c = b, b = void 0), c = c || {};
			var d, e, f, g, h, i, j, k, l = n.ajaxSetup({}, c),
				m = l.context || l,
				o = l.context && (m.nodeType || m.jquery) ? n(m) : n.event,
				p = n.Deferred(),
				q = n.Callbacks("once memory"),
				r = l.statusCode || {},
				s = {},
				t = {},
				u = 0,
				v = "canceled",
				w = {
					readyState: 0,
					getResponseHeader: function (a) {
						var b;
						if (2 === u) {
							if (!k) {
								k = {};
								while (b = Jb.exec(g)) k[b[1].toLowerCase()] = b[2]
							}
							b = k[a.toLowerCase()]
						}
						return null == b ? null : b
					},
					getAllResponseHeaders: function () {
						return 2 === u ? g : null
					},
					setRequestHeader: function (a, b) {
						var c = a.toLowerCase();
						return u || (a = t[c] = t[c] || a, s[a] = b), this
					},
					overrideMimeType: function (a) {
						return u || (l.mimeType = a), this
					},
					statusCode: function (a) {
						var b;
						if (a)
							if (2 > u)
								for (b in a) r[b] = [r[b], a[b]];
							else w.always(a[w.status]);
						return this
					},
					abort: function (a) {
						var b = a || v;
						return j && j.abort(b), y(0, b), this
					}
				};
			if (p.promise(w).complete = q.add, w.success = w.done, w.error = w.fail, l.url = ((b || l.url || Rb) + "").replace(Hb, "").replace(Mb, Sb[1] + "//"), l.type = c.method || c.type || l.method || l.type, l.dataTypes = n.trim(l.dataType || "*").toLowerCase().match(G) || [""], null == l.crossDomain && (d = Nb.exec(l.url.toLowerCase()), l.crossDomain = !(!d || d[1] === Sb[1] && d[2] === Sb[2] && (d[3] || ("http:" === d[1] ? "80" : "443")) === (Sb[3] || ("http:" === Sb[1] ? "80" : "443")))), l.data && l.processData && "string" != typeof l.data && (l.data = n.param(l.data, l.traditional)), Ub(Ob, l, c, w), 2 === u) return w;
			i = n.event && l.global, i && 0 === n.active++ && n.event.trigger("ajaxStart"), l.type = l.type.toUpperCase(), l.hasContent = !Lb.test(l.type), f = l.url, l.hasContent || (l.data && (f = l.url += (Fb.test(f) ? "&" : "?") + l.data, delete l.data), l.cache === !1 && (l.url = Ib.test(f) ? f.replace(Ib, "$1_=" + Eb++) : f + (Fb.test(f) ? "&" : "?") + "_=" + Eb++)), l.ifModified && (n.lastModified[f] && w.setRequestHeader("If-Modified-Since", n.lastModified[f]), n.etag[f] && w.setRequestHeader("If-None-Match", n.etag[f])), (l.data && l.hasContent && l.contentType !== !1 || c.contentType) && w.setRequestHeader("Content-Type", l.contentType), w.setRequestHeader("Accept", l.dataTypes[0] && l.accepts[l.dataTypes[0]] ? l.accepts[l.dataTypes[0]] + ("*" !== l.dataTypes[0] ? ", " + Qb + "; q=0.01" : "") : l.accepts["*"]);
			for (e in l.headers) w.setRequestHeader(e, l.headers[e]);
			if (l.beforeSend && (l.beforeSend.call(m, w, l) === !1 || 2 === u)) return w.abort();
			v = "abort";
			for (e in {
					success: 1,
					error: 1,
					complete: 1
				}) w[e](l[e]);
			if (j = Ub(Pb, l, c, w)) {
				if (w.readyState = 1, i && o.trigger("ajaxSend", [w, l]), 2 === u) return w;
				l.async && l.timeout > 0 && (h = a.setTimeout(function () {
					w.abort("timeout")
				}, l.timeout));
				try {
					u = 1, j.send(s, y)
				} catch (x) {
					if (!(2 > u)) throw x;
					y(-1, x)
				}
			} else y(-1, "No Transport");

			function y(b, c, d, e) {
				var k, s, t, v, x, y = c;
				2 !== u && (u = 2, h && a.clearTimeout(h), j = void 0, g = e || "", w.readyState = b > 0 ? 4 : 0, k = b >= 200 && 300 > b || 304 === b, d && (v = Wb(l, w, d)), v = Xb(l, v, w, k), k ? (l.ifModified && (x = w.getResponseHeader("Last-Modified"), x && (n.lastModified[f] = x), x = w.getResponseHeader("etag"), x && (n.etag[f] = x)), 204 === b || "HEAD" === l.type ? y = "nocontent" : 304 === b ? y = "notmodified" : (y = v.state, s = v.data, t = v.error, k = !t)) : (t = y, !b && y || (y = "error", 0 > b && (b = 0))), w.status = b, w.statusText = (c || y) + "", k ? p.resolveWith(m, [s, y, w]) : p.rejectWith(m, [w, y, t]), w.statusCode(r), r = void 0, i && o.trigger(k ? "ajaxSuccess" : "ajaxError", [w, l, k ? s : t]), q.fireWith(m, [w, y]), i && (o.trigger("ajaxComplete", [w, l]), --n.active || n.event.trigger("ajaxStop")))
			}
			return w
		},
		getJSON: function (a, b, c) {
			return n.get(a, b, c, "json")
		},
		getScript: function (a, b) {
			return n.get(a, void 0, b, "script")
		}
	}), n.each(["get", "post"], function (a, b) {
		n[b] = function (a, c, d, e) {
			return n.isFunction(c) && (e = e || d, d = c, c = void 0), n.ajax(n.extend({
				url: a,
				type: b,
				dataType: e,
				data: c,
				success: d
			}, n.isPlainObject(a) && a))
		}
	}), n._evalUrl = function (a) {
		return n.ajax({
			url: a,
			type: "GET",
			dataType: "script",
			cache: !0,
			async: !1,
			global: !1,
			"throws": !0
		})
	}, n.fn.extend({
		wrapAll: function (a) {
			if (n.isFunction(a)) return this.each(function (b) {
				n(this).wrapAll(a.call(this, b))
			});
			if (this[0]) {
				var b = n(a, this[0].ownerDocument).eq(0).clone(!0);
				this[0].parentNode && b.insertBefore(this[0]), b.map(function () {
					var a = this;
					while (a.firstChild && 1 === a.firstChild.nodeType) a = a.firstChild;
					return a
				}).append(this)
			}
			return this
		},
		wrapInner: function (a) {
			return n.isFunction(a) ? this.each(function (b) {
				n(this).wrapInner(a.call(this, b))
			}) : this.each(function () {
				var b = n(this),
					c = b.contents();
				c.length ? c.wrapAll(a) : b.append(a)
			})
		},
		wrap: function (a) {
			var b = n.isFunction(a);
			return this.each(function (c) {
				n(this).wrapAll(b ? a.call(this, c) : a)
			})
		},
		unwrap: function () {
			return this.parent().each(function () {
				n.nodeName(this, "body") || n(this).replaceWith(this.childNodes)
			}).end()
		}
	});

	function Yb(a) {
		return a.style && a.style.display || n.css(a, "display")
	}

	function Zb(a) {
		if (!n.contains(a.ownerDocument || d, a)) return !0;
		while (a && 1 === a.nodeType) {
			if ("none" === Yb(a) || "hidden" === a.type) return !0;
			a = a.parentNode
		}
		return !1
	}
	n.expr.filters.hidden = function (a) {
		return l.reliableHiddenOffsets() ? a.offsetWidth <= 0 && a.offsetHeight <= 0 && !a.getClientRects().length : Zb(a)
	}, n.expr.filters.visible = function (a) {
		return !n.expr.filters.hidden(a)
	};
	var $b = /%20/g,
		_b = /\[\]$/,
		ac = /\r?\n/g,
		bc = /^(?:submit|button|image|reset|file)$/i,
		cc = /^(?:input|select|textarea|keygen)/i;

	function dc(a, b, c, d) {
		var e;
		if (n.isArray(b)) n.each(b, function (b, e) {
			c || _b.test(a) ? d(a, e) : dc(a + "[" + ("object" == typeof e && null != e ? b : "") + "]", e, c, d)
		});
		else if (c || "object" !== n.type(b)) d(a, b);
		else
			for (e in b) dc(a + "[" + e + "]", b[e], c, d)
	}
	n.param = function (a, b) {
		var c, d = [],
			e = function (a, b) {
				b = n.isFunction(b) ? b() : null == b ? "" : b, d[d.length] = encodeURIComponent(a) + "=" + encodeURIComponent(b)
			};
		if (void 0 === b && (b = n.ajaxSettings && n.ajaxSettings.traditional), n.isArray(a) || a.jquery && !n.isPlainObject(a)) n.each(a, function () {
			e(this.name, this.value)
		});
		else
			for (c in a) dc(c, a[c], b, e);
		return d.join("&").replace($b, "+")
	}, n.fn.extend({
		serialize: function () {
			return n.param(this.serializeArray())
		},
		serializeArray: function () {
			return this.map(function () {
				var a = n.prop(this, "elements");
				return a ? n.makeArray(a) : this
			}).filter(function () {
				var a = this.type;
				return this.name && !n(this).is(":disabled") && cc.test(this.nodeName) && !bc.test(a) && (this.checked || !Z.test(a))
			}).map(function (a, b) {
				var c = n(this).val();
				return null == c ? null : n.isArray(c) ? n.map(c, function (a) {
					return {
						name: b.name,
						value: a.replace(ac, "\r\n")
					}
				}) : {
					name: b.name,
					value: c.replace(ac, "\r\n")
				}
			}).get()
		}
	}), n.ajaxSettings.xhr = void 0 !== a.ActiveXObject ? function () {
		return this.isLocal ? ic() : d.documentMode > 8 ? hc() : /^(get|post|head|put|delete|options)$/i.test(this.type) && hc() || ic()
	} : hc;
	var ec = 0,
		fc = {},
		gc = n.ajaxSettings.xhr();
	a.attachEvent && a.attachEvent("onunload", function () {
		for (var a in fc) fc[a](void 0, !0)
	}), l.cors = !!gc && "withCredentials" in gc, gc = l.ajax = !!gc, gc && n.ajaxTransport(function (b) {
		if (!b.crossDomain || l.cors) {
			var c;
			return {
				send: function (d, e) {
					var f, g = b.xhr(),
						h = ++ec;
					if (g.open(b.type, b.url, b.async, b.username, b.password), b.xhrFields)
						for (f in b.xhrFields) g[f] = b.xhrFields[f];
					b.mimeType && g.overrideMimeType && g.overrideMimeType(b.mimeType), b.crossDomain || d["X-Requested-With"] || (d["X-Requested-With"] = "XMLHttpRequest");
					for (f in d) void 0 !== d[f] && g.setRequestHeader(f, d[f] + "");
					g.send(b.hasContent && b.data || null), c = function (a, d) {
						var f, i, j;
						if (c && (d || 4 === g.readyState))
							if (delete fc[h], c = void 0, g.onreadystatechange = n.noop, d) 4 !== g.readyState && g.abort();
							else {
								j = {}, f = g.status, "string" == typeof g.responseText && (j.text = g.responseText);
								try {
									i = g.statusText
								} catch (k) {
									i = ""
								}
								f || !b.isLocal || b.crossDomain ? 1223 === f && (f = 204) : f = j.text ? 200 : 404
							}
						j && e(f, i, j, g.getAllResponseHeaders())
					}, b.async ? 4 === g.readyState ? a.setTimeout(c) : g.onreadystatechange = fc[h] = c : c()
				},
				abort: function () {
					c && c(void 0, !0)
				}
			}
		}
	});

	function hc() {
		try {
			return new a.XMLHttpRequest
		} catch (b) {}
	}

	function ic() {
		try {
			return new a.ActiveXObject("Microsoft.XMLHTTP")
		} catch (b) {}
	}
	n.ajaxSetup({
		accepts: {
			script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
		},
		contents: {
			script: /\b(?:java|ecma)script\b/
		},
		converters: {
			"text script": function (a) {
				return n.globalEval(a), a
			}
		}
	}), n.ajaxPrefilter("script", function (a) {
		void 0 === a.cache && (a.cache = !1), a.crossDomain && (a.type = "GET", a.global = !1)
	}), n.ajaxTransport("script", function (a) {
		if (a.crossDomain) {
			var b, c = d.head || n("head")[0] || d.documentElement;
			return {
				send: function (e, f) {
					b = d.createElement("script"), b.async = !0, a.scriptCharset && (b.charset = a.scriptCharset), b.src = a.url, b.onload = b.onreadystatechange = function (a, c) {
						(c || !b.readyState || /loaded|complete/.test(b.readyState)) && (b.onload = b.onreadystatechange = null, b.parentNode && b.parentNode.removeChild(b), b = null, c || f(200, "success"))
					}, c.insertBefore(b, c.firstChild)
				},
				abort: function () {
					b && b.onload(void 0, !0)
				}
			}
		}
	});
	var jc = [],
		kc = /(=)\?(?=&|$)|\?\?/;
	n.ajaxSetup({
		jsonp: "callback",
		jsonpCallback: function () {
			var a = jc.pop() || n.expando + "_" + Eb++;
			return this[a] = !0, a
		}
	}), n.ajaxPrefilter("json jsonp", function (b, c, d) {
		var e, f, g, h = b.jsonp !== !1 && (kc.test(b.url) ? "url" : "string" == typeof b.data && 0 === (b.contentType || "").indexOf("application/x-www-form-urlencoded") && kc.test(b.data) && "data");
		return h || "jsonp" === b.dataTypes[0] ? (e = b.jsonpCallback = n.isFunction(b.jsonpCallback) ? b.jsonpCallback() : b.jsonpCallback, h ? b[h] = b[h].replace(kc, "$1" + e) : b.jsonp !== !1 && (b.url += (Fb.test(b.url) ? "&" : "?") + b.jsonp + "=" + e), b.converters["script json"] = function () {
			return g || n.error(e + " was not called"), g[0]
		}, b.dataTypes[0] = "json", f = a[e], a[e] = function () {
			g = arguments
		}, d.always(function () {
			void 0 === f ? n(a).removeProp(e) : a[e] = f, b[e] && (b.jsonpCallback = c.jsonpCallback, jc.push(e)), g && n.isFunction(f) && f(g[0]), g = f = void 0
		}), "script") : void 0
	}), n.parseHTML = function (a, b, c) {
		if (!a || "string" != typeof a) return null;
		"boolean" == typeof b && (c = b, b = !1), b = b || d;
		var e = x.exec(a),
			f = !c && [];
		return e ? [b.createElement(e[1])] : (e = ja([a], b, f), f && f.length && n(f).remove(), n.merge([], e.childNodes))
	};
	var lc = n.fn.load;
	n.fn.load = function (a, b, c) {
		if ("string" != typeof a && lc) return lc.apply(this, arguments);
		var d, e, f, g = this,
			h = a.indexOf(" ");
		return h > -1 && (d = n.trim(a.slice(h, a.length)), a = a.slice(0, h)), n.isFunction(b) ? (c = b, b = void 0) : b && "object" == typeof b && (e = "POST"), g.length > 0 && n.ajax({
			url: a,
			type: e || "GET",
			dataType: "html",
			data: b
		}).done(function (a) {
			f = arguments, g.html(d ? n("<div>").append(n.parseHTML(a)).find(d) : a)
		}).always(c && function (a, b) {
			g.each(function () {
				c.apply(this, f || [a.responseText, b, a])
			})
		}), this
	}, n.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function (a, b) {
		n.fn[b] = function (a) {
			return this.on(b, a)
		}
	}), n.expr.filters.animated = function (a) {
		return n.grep(n.timers, function (b) {
			return a === b.elem
		}).length
	};

	function mc(a) {
		return n.isWindow(a) ? a : 9 === a.nodeType ? a.defaultView || a.parentWindow : !1
	}
	n.offset = {
		setOffset: function (a, b, c) {
			var d, e, f, g, h, i, j, k = n.css(a, "position"),
				l = n(a),
				m = {};
			"static" === k && (a.style.position = "relative"), h = l.offset(), f = n.css(a, "top"), i = n.css(a, "left"), j = ("absolute" === k || "fixed" === k) && n.inArray("auto", [f, i]) > -1, j ? (d = l.position(), g = d.top, e = d.left) : (g = parseFloat(f) || 0, e = parseFloat(i) || 0), n.isFunction(b) && (b = b.call(a, c, n.extend({}, h))), null != b.top && (m.top = b.top - h.top + g), null != b.left && (m.left = b.left - h.left + e), "using" in b ? b.using.call(a, m) : l.css(m)
		}
	}, n.fn.extend({
		offset: function (a) {
			if (arguments.length) return void 0 === a ? this : this.each(function (b) {
				n.offset.setOffset(this, a, b)
			});
			var b, c, d = {
					top: 0,
					left: 0
				},
				e = this[0],
				f = e && e.ownerDocument;
			if (f) return b = f.documentElement, n.contains(b, e) ? ("undefined" != typeof e.getBoundingClientRect && (d = e.getBoundingClientRect()), c = mc(f), {
				top: d.top + (c.pageYOffset || b.scrollTop) - (b.clientTop || 0),
				left: d.left + (c.pageXOffset || b.scrollLeft) - (b.clientLeft || 0)
			}) : d
		},
		position: function () {
			if (this[0]) {
				var a, b, c = {
						top: 0,
						left: 0
					},
					d = this[0];
				return "fixed" === n.css(d, "position") ? b = d.getBoundingClientRect() : (a = this.offsetParent(), b = this.offset(), n.nodeName(a[0], "html") || (c = a.offset()), c.top += n.css(a[0], "borderTopWidth", !0), c.left += n.css(a[0], "borderLeftWidth", !0)), {
					top: b.top - c.top - n.css(d, "marginTop", !0),
					left: b.left - c.left - n.css(d, "marginLeft", !0)
				}
			}
		},
		offsetParent: function () {
			return this.map(function () {
				var a = this.offsetParent;
				while (a && !n.nodeName(a, "html") && "static" === n.css(a, "position")) a = a.offsetParent;
				return a || Qa
			})
		}
	}), n.each({
		scrollLeft: "pageXOffset",
		scrollTop: "pageYOffset"
	}, function (a, b) {
		var c = /Y/.test(b);
		n.fn[a] = function (d) {
			return Y(this, function (a, d, e) {
				var f = mc(a);
				return void 0 === e ? f ? b in f ? f[b] : f.document.documentElement[d] : a[d] : void(f ? f.scrollTo(c ? n(f).scrollLeft() : e, c ? e : n(f).scrollTop()) : a[d] = e)
			}, a, d, arguments.length, null)
		}
	}), n.each(["top", "left"], function (a, b) {
		n.cssHooks[b] = Ua(l.pixelPosition, function (a, c) {
			return c ? (c = Sa(a, b), Oa.test(c) ? n(a).position()[b] + "px" : c) : void 0
		})
	}), n.each({
		Height: "height",
		Width: "width"
	}, function (a, b) {
		n.each({
			padding: "inner" + a,
			content: b,
			"": "outer" + a
		}, function (c, d) {
			n.fn[d] = function (d, e) {
				var f = arguments.length && (c || "boolean" != typeof d),
					g = c || (d === !0 || e === !0 ? "margin" : "border");
				return Y(this, function (b, c, d) {
					var e;
					return n.isWindow(b) ? b.document.documentElement["client" + a] : 9 === b.nodeType ? (e = b.documentElement, Math.max(b.body["scroll" + a], e["scroll" + a], b.body["offset" + a], e["offset" + a], e["client" + a])) : void 0 === d ? n.css(b, c, g) : n.style(b, c, d, g)
				}, b, f ? d : void 0, f, null)
			}
		})
	}), n.fn.extend({
		bind: function (a, b, c) {
			return this.on(a, null, b, c)
		},
		unbind: function (a, b) {
			return this.off(a, null, b)
		},
		delegate: function (a, b, c, d) {
			return this.on(b, a, c, d)
		},
		undelegate: function (a, b, c) {
			return 1 === arguments.length ? this.off(a, "**") : this.off(b, a || "**", c)
		}
	}), n.fn.size = function () {
		return this.length
	}, n.fn.andSelf = n.fn.addBack, "function" == typeof define && define.amd && define("jquery", [], function () {
		return n
	});
	var nc = a.jQuery,
		oc = a.$;
	return n.noConflict = function (b) {
		return a.$ === n && (a.$ = oc), b && a.jQuery === n && (a.jQuery = nc), n
	}, b || (a.jQuery = a.$ = n), n
});
jQuery.noConflict();; /*! jQuery Migrate v1.4.1 | (c) jQuery Foundation and other contributors | jquery.org/license */
"undefined" == typeof jQuery.migrateMute && (jQuery.migrateMute = !0),
	function (a, b, c) {
		function d(c) {
			var d = b.console;
			f[c] || (f[c] = !0, a.migrateWarnings.push(c), d && d.warn && !a.migrateMute && (d.warn("JQMIGRATE: " + c), a.migrateTrace && d.trace && d.trace()))
		}

		function e(b, c, e, f) {
			if (Object.defineProperty) try {
				return void Object.defineProperty(b, c, {
					configurable: !0,
					enumerable: !0,
					get: function () {
						return d(f), e
					},
					set: function (a) {
						d(f), e = a
					}
				})
			} catch (g) {}
			a._definePropertyBroken = !0, b[c] = e
		}
		a.migrateVersion = "1.4.1";
		var f = {};
		a.migrateWarnings = [], b.console && b.console.log && b.console.log("JQMIGRATE: Migrate is installed" + (a.migrateMute ? "" : " with logging active") + ", version " + a.migrateVersion), a.migrateTrace === c && (a.migrateTrace = !0), a.migrateReset = function () {
			f = {}, a.migrateWarnings.length = 0
		}, "BackCompat" === document.compatMode && d("jQuery is not compatible with Quirks Mode");
		var g = a("<input/>", {
				size: 1
			}).attr("size") && a.attrFn,
			h = a.attr,
			i = a.attrHooks.value && a.attrHooks.value.get || function () {
				return null
			},
			j = a.attrHooks.value && a.attrHooks.value.set || function () {
				return c
			},
			k = /^(?:input|button)$/i,
			l = /^[238]$/,
			m = /^(?:autofocus|autoplay|async|checked|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped|selected)$/i,
			n = /^(?:checked|selected)$/i;
		e(a, "attrFn", g || {}, "jQuery.attrFn is deprecated"), a.attr = function (b, e, f, i) {
			var j = e.toLowerCase(),
				o = b && b.nodeType;
			return i && (h.length < 4 && d("jQuery.fn.attr( props, pass ) is deprecated"), b && !l.test(o) && (g ? e in g : a.isFunction(a.fn[e]))) ? a(b)[e](f) : ("type" === e && f !== c && k.test(b.nodeName) && b.parentNode && d("Can't change the 'type' of an input or button in IE 6/7/8"), !a.attrHooks[j] && m.test(j) && (a.attrHooks[j] = {
				get: function (b, d) {
					var e, f = a.prop(b, d);
					return f === !0 || "boolean" != typeof f && (e = b.getAttributeNode(d)) && e.nodeValue !== !1 ? d.toLowerCase() : c
				},
				set: function (b, c, d) {
					var e;
					return c === !1 ? a.removeAttr(b, d) : (e = a.propFix[d] || d, e in b && (b[e] = !0), b.setAttribute(d, d.toLowerCase())), d
				}
			}, n.test(j) && d("jQuery.fn.attr('" + j + "') might use property instead of attribute")), h.call(a, b, e, f))
		}, a.attrHooks.value = {
			get: function (a, b) {
				var c = (a.nodeName || "").toLowerCase();
				return "button" === c ? i.apply(this, arguments) : ("input" !== c && "option" !== c && d("jQuery.fn.attr('value') no longer gets properties"), b in a ? a.value : null)
			},
			set: function (a, b) {
				var c = (a.nodeName || "").toLowerCase();
				return "button" === c ? j.apply(this, arguments) : ("input" !== c && "option" !== c && d("jQuery.fn.attr('value', val) no longer sets properties"), void(a.value = b))
			}
		};
		var o, p, q = a.fn.init,
			r = a.find,
			s = a.parseJSON,
			t = /^\s*</,
			u = /\[(\s*[-\w]+\s*)([~|^$*]?=)\s*([-\w#]*?#[-\w#]*)\s*\]/,
			v = /\[(\s*[-\w]+\s*)([~|^$*]?=)\s*([-\w#]*?#[-\w#]*)\s*\]/g,
			w = /^([^<]*)(<[\w\W]+>)([^>]*)$/;
		a.fn.init = function (b, e, f) {
			var g, h;
			return b && "string" == typeof b && !a.isPlainObject(e) && (g = w.exec(a.trim(b))) && g[0] && (t.test(b) || d("$(html) HTML strings must start with '<' character"), g[3] && d("$(html) HTML text after last tag is ignored"), "#" === g[0].charAt(0) && (d("HTML string cannot start with a '#' character"), a.error("JQMIGRATE: Invalid selector string (XSS)")), e && e.context && e.context.nodeType && (e = e.context), a.parseHTML) ? q.call(this, a.parseHTML(g[2], e && e.ownerDocument || e || document, !0), e, f) : (h = q.apply(this, arguments), b && b.selector !== c ? (h.selector = b.selector, h.context = b.context) : (h.selector = "string" == typeof b ? b : "", b && (h.context = b.nodeType ? b : e || document)), h)
		}, a.fn.init.prototype = a.fn, a.find = function (a) {
			var b = Array.prototype.slice.call(arguments);
			if ("string" == typeof a && u.test(a)) try {
				document.querySelector(a)
			} catch (c) {
				a = a.replace(v, function (a, b, c, d) {
					return "[" + b + c + '"' + d + '"]'
				});
				try {
					document.querySelector(a), d("Attribute selector with '#' must be quoted: " + b[0]), b[0] = a
				} catch (e) {
					d("Attribute selector with '#' was not fixed: " + b[0])
				}
			}
			return r.apply(this, b)
		};
		var x;
		for (x in r) Object.prototype.hasOwnProperty.call(r, x) && (a.find[x] = r[x]);
		a.parseJSON = function (a) {
			return a ? s.apply(this, arguments) : (d("jQuery.parseJSON requires a valid JSON string"), null)
		}, a.uaMatch = function (a) {
			a = a.toLowerCase();
			var b = /(chrome)[ \/]([\w.]+)/.exec(a) || /(webkit)[ \/]([\w.]+)/.exec(a) || /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(a) || /(msie) ([\w.]+)/.exec(a) || a.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(a) || [];
			return {
				browser: b[1] || "",
				version: b[2] || "0"
			}
		}, a.browser || (o = a.uaMatch(navigator.userAgent), p = {}, o.browser && (p[o.browser] = !0, p.version = o.version), p.chrome ? p.webkit = !0 : p.webkit && (p.safari = !0), a.browser = p), e(a, "browser", a.browser, "jQuery.browser is deprecated"), a.boxModel = a.support.boxModel = "CSS1Compat" === document.compatMode, e(a, "boxModel", a.boxModel, "jQuery.boxModel is deprecated"), e(a.support, "boxModel", a.support.boxModel, "jQuery.support.boxModel is deprecated"), a.sub = function () {
			function b(a, c) {
				return new b.fn.init(a, c)
			}
			a.extend(!0, b, this), b.superclass = this, b.fn = b.prototype = this(), b.fn.constructor = b, b.sub = this.sub, b.fn.init = function (d, e) {
				var f = a.fn.init.call(this, d, e, c);
				return f instanceof b ? f : b(f)
			}, b.fn.init.prototype = b.fn;
			var c = b(document);
			return d("jQuery.sub() is deprecated"), b
		}, a.fn.size = function () {
			return d("jQuery.fn.size() is deprecated; use the .length property"), this.length
		};
		var y = !1;
		a.swap && a.each(["height", "width", "reliableMarginRight"], function (b, c) {
			var d = a.cssHooks[c] && a.cssHooks[c].get;
			d && (a.cssHooks[c].get = function () {
				var a;
				return y = !0, a = d.apply(this, arguments), y = !1, a
			})
		}), a.swap = function (a, b, c, e) {
			var f, g, h = {};
			y || d("jQuery.swap() is undocumented and deprecated");
			for (g in b) h[g] = a.style[g], a.style[g] = b[g];
			f = c.apply(a, e || []);
			for (g in b) a.style[g] = h[g];
			return f
		}, a.ajaxSetup({
			converters: {
				"text json": a.parseJSON
			}
		});
		var z = a.fn.data;
		a.fn.data = function (b) {
			var e, f, g = this[0];
			return !g || "events" !== b || 1 !== arguments.length || (e = a.data(g, b), f = a._data(g, b), e !== c && e !== f || f === c) ? z.apply(this, arguments) : (d("Use of jQuery.fn.data('events') is deprecated"), f)
		};
		var A = /\/(java|ecma)script/i;
		a.clean || (a.clean = function (b, c, e, f) {
			c = c || document, c = !c.nodeType && c[0] || c, c = c.ownerDocument || c, d("jQuery.clean() is deprecated");
			var g, h, i, j, k = [];
			if (a.merge(k, a.buildFragment(b, c).childNodes), e)
				for (i = function (a) {
						return !a.type || A.test(a.type) ? f ? f.push(a.parentNode ? a.parentNode.removeChild(a) : a) : e.appendChild(a) : void 0
					}, g = 0; null != (h = k[g]); g++) a.nodeName(h, "script") && i(h) || (e.appendChild(h), "undefined" != typeof h.getElementsByTagName && (j = a.grep(a.merge([], h.getElementsByTagName("script")), i), k.splice.apply(k, [g + 1, 0].concat(j)), g += j.length));
			return k
		});
		var B = a.event.add,
			C = a.event.remove,
			D = a.event.trigger,
			E = a.fn.toggle,
			F = a.fn.live,
			G = a.fn.die,
			H = a.fn.load,
			I = "ajaxStart|ajaxStop|ajaxSend|ajaxComplete|ajaxError|ajaxSuccess",
			J = new RegExp("\\b(?:" + I + ")\\b"),
			K = /(?:^|\s)hover(\.\S+|)\b/,
			L = function (b) {
				return "string" != typeof b || a.event.special.hover ? b : (K.test(b) && d("'hover' pseudo-event is deprecated, use 'mouseenter mouseleave'"), b && b.replace(K, "mouseenter$1 mouseleave$1"))
			};
		a.event.props && "attrChange" !== a.event.props[0] && a.event.props.unshift("attrChange", "attrName", "relatedNode", "srcElement"), a.event.dispatch && e(a.event, "handle", a.event.dispatch, "jQuery.event.handle is undocumented and deprecated"), a.event.add = function (a, b, c, e, f) {
			a !== document && J.test(b) && d("AJAX events should be attached to document: " + b), B.call(this, a, L(b || ""), c, e, f)
		}, a.event.remove = function (a, b, c, d, e) {
			C.call(this, a, L(b) || "", c, d, e)
		}, a.each(["load", "unload", "error"], function (b, c) {
			a.fn[c] = function () {
				var a = Array.prototype.slice.call(arguments, 0);
				return "load" === c && "string" == typeof a[0] ? H.apply(this, a) : (d("jQuery.fn." + c + "() is deprecated"), a.splice(0, 0, c), arguments.length ? this.bind.apply(this, a) : (this.triggerHandler.apply(this, a), this))
			}
		}), a.fn.toggle = function (b, c) {
			if (!a.isFunction(b) || !a.isFunction(c)) return E.apply(this, arguments);
			d("jQuery.fn.toggle(handler, handler...) is deprecated");
			var e = arguments,
				f = b.guid || a.guid++,
				g = 0,
				h = function (c) {
					var d = (a._data(this, "lastToggle" + b.guid) || 0) % g;
					return a._data(this, "lastToggle" + b.guid, d + 1), c.preventDefault(), e[d].apply(this, arguments) || !1
				};
			for (h.guid = f; g < e.length;) e[g++].guid = f;
			return this.click(h)
		}, a.fn.live = function (b, c, e) {
			return d("jQuery.fn.live() is deprecated"), F ? F.apply(this, arguments) : (a(this.context).on(b, this.selector, c, e), this)
		}, a.fn.die = function (b, c) {
			return d("jQuery.fn.die() is deprecated"), G ? G.apply(this, arguments) : (a(this.context).off(b, this.selector || "**", c), this)
		}, a.event.trigger = function (a, b, c, e) {
			return c || J.test(a) || d("Global events are undocumented and deprecated"), D.call(this, a, b, c || document, e)
		}, a.each(I.split("|"), function (b, c) {
			a.event.special[c] = {
				setup: function () {
					var b = this;
					return b !== document && (a.event.add(document, c + "." + a.guid, function () {
						a.event.trigger(c, Array.prototype.slice.call(arguments, 1), b, !0)
					}), a._data(this, c, a.guid++)), !1
				},
				teardown: function () {
					return this !== document && a.event.remove(document, c + "." + a._data(this, c)), !1
				}
			}
		}), a.event.special.ready = {
			setup: function () {
				this === document && d("'ready' event is deprecated")
			}
		};
		var M = a.fn.andSelf || a.fn.addBack,
			N = a.fn.find;
		if (a.fn.andSelf = function () {
				return d("jQuery.fn.andSelf() replaced by jQuery.fn.addBack()"), M.apply(this, arguments)
			}, a.fn.find = function (a) {
				var b = N.apply(this, arguments);
				return b.context = this.context, b.selector = this.selector ? this.selector + " " + a : a, b
			}, a.Callbacks) {
			var O = a.Deferred,
				P = [["resolve", "done", a.Callbacks("once memory"), a.Callbacks("once memory"), "resolved"], ["reject", "fail", a.Callbacks("once memory"), a.Callbacks("once memory"), "rejected"], ["notify", "progress", a.Callbacks("memory"), a.Callbacks("memory")]];
			a.Deferred = function (b) {
				var c = O(),
					e = c.promise();
				return c.pipe = e.pipe = function () {
					var b = arguments;
					return d("deferred.pipe() is deprecated"), a.Deferred(function (d) {
						a.each(P, function (f, g) {
							var h = a.isFunction(b[f]) && b[f];
							c[g[1]](function () {
								var b = h && h.apply(this, arguments);
								b && a.isFunction(b.promise) ? b.promise().done(d.resolve).fail(d.reject).progress(d.notify) : d[g[0] + "With"](this === e ? d.promise() : this, h ? [b] : arguments)
							})
						}), b = null
					}).promise()
				}, c.isResolved = function () {
					return d("deferred.isResolved is deprecated"), "resolved" === c.state()
				}, c.isRejected = function () {
					return d("deferred.isRejected is deprecated"), "rejected" === c.state()
				}, b && b.call(c, c), c
			}
		}
	}(jQuery, window);;
/********************************************
	-	THEMEPUNCH TOOLS Ver. 1.0     -
	 Last Update of Tools 27.02.2015
*********************************************/


/*
 * @fileOverview TouchSwipe - jQuery Plugin
 * @version 1.6.9
 *
 * @author Matt Bryson http://www.github.com/mattbryson
 * @see https://github.com/mattbryson/TouchSwipe-Jquery-Plugin
 * @see http://labs.skinkers.com/touchSwipe/
 * @see http://plugins.jquery.com/project/touchSwipe
 *
 * Copyright (c) 2010 Matt Bryson
 * Dual licensed under the MIT or GPL Version 2 licenses.
 *
 */



(function (a) {
	if (typeof define === "function" && define.amd && define.amd.jQuery) {
		define(["jquery"], a)
	} else {
		a(jQuery)
	}
}(function (f) {
	var y = "1.6.9",
		p = "left",
		o = "right",
		e = "up",
		x = "down",
		c = "in",
		A = "out",
		m = "none",
		s = "auto",
		l = "swipe",
		t = "pinch",
		B = "tap",
		j = "doubletap",
		b = "longtap",
		z = "hold",
		E = "horizontal",
		u = "vertical",
		i = "all",
		r = 10,
		g = "start",
		k = "move",
		h = "end",
		q = "cancel",
		a = "ontouchstart" in window,
		v = window.navigator.msPointerEnabled && !window.navigator.pointerEnabled,
		d = window.navigator.pointerEnabled || window.navigator.msPointerEnabled,
		C = "TouchSwipe";
	var n = {
		fingers: 1,
		threshold: 75,
		cancelThreshold: null,
		pinchThreshold: 20,
		maxTimeThreshold: null,
		fingerReleaseThreshold: 250,
		longTapThreshold: 500,
		doubleTapThreshold: 200,
		swipe: null,
		swipeLeft: null,
		swipeRight: null,
		swipeUp: null,
		swipeDown: null,
		swipeStatus: null,
		pinchIn: null,
		pinchOut: null,
		pinchStatus: null,
		click: null,
		tap: null,
		doubleTap: null,
		longTap: null,
		hold: null,
		triggerOnTouchEnd: true,
		triggerOnTouchLeave: false,
		allowPageScroll: "auto",
		fallbackToMouseEvents: true,
		excludedElements: "label, button, input, select, textarea, a, .noSwipe",
		preventDefaultEvents: true
	};
	f.fn.swipetp = function (H) {
		var G = f(this),
			F = G.data(C);
		if (F && typeof H === "string") {
			if (F[H]) {
				return F[H].apply(this, Array.prototype.slice.call(arguments, 1))
			} else {
				f.error("Method " + H + " does not exist on jQuery.swipetp")
			}
		} else {
			if (!F && (typeof H === "object" || !H)) {
				return w.apply(this, arguments)
			}
		}
		return G
	};
	f.fn.swipetp.version = y;
	f.fn.swipetp.defaults = n;
	f.fn.swipetp.phases = {
		PHASE_START: g,
		PHASE_MOVE: k,
		PHASE_END: h,
		PHASE_CANCEL: q
	};
	f.fn.swipetp.directions = {
		LEFT: p,
		RIGHT: o,
		UP: e,
		DOWN: x,
		IN: c,
		OUT: A
	};
	f.fn.swipetp.pageScroll = {
		NONE: m,
		HORIZONTAL: E,
		VERTICAL: u,
		AUTO: s
	};
	f.fn.swipetp.fingers = {
		ONE: 1,
		TWO: 2,
		THREE: 3,
		ALL: i
	};

	function w(F) {
		if (F && (F.allowPageScroll === undefined && (F.swipe !== undefined || F.swipeStatus !== undefined))) {
			F.allowPageScroll = m
		}
		if (F.click !== undefined && F.tap === undefined) {
			F.tap = F.click
		}
		if (!F) {
			F = {}
		}
		F = f.extend({}, f.fn.swipetp.defaults, F);
		return this.each(function () {
			var H = f(this);
			var G = H.data(C);
			if (!G) {
				G = new D(this, F);
				H.data(C, G)
			}
		})
	}

	function D(a5, aw) {
		var aA = (a || d || !aw.fallbackToMouseEvents),
			K = aA ? (d ? (v ? "MSPointerDown" : "pointerdown") : "touchstart") : "mousedown",
			az = aA ? (d ? (v ? "MSPointerMove" : "pointermove") : "touchmove") : "mousemove",
			V = aA ? (d ? (v ? "MSPointerUp" : "pointerup") : "touchend") : "mouseup",
			T = aA ? null : "mouseleave",
			aE = (d ? (v ? "MSPointerCancel" : "pointercancel") : "touchcancel");
		var ah = 0,
			aQ = null,
			ac = 0,
			a2 = 0,
			a0 = 0,
			H = 1,
			ar = 0,
			aK = 0,
			N = null;
		var aS = f(a5);
		var aa = "start";
		var X = 0;
		var aR = null;
		var U = 0,
			a3 = 0,
			a6 = 0,
			ae = 0,
			O = 0;
		var aX = null,
			ag = null;
		try {
			aS.bind(K, aO);
			aS.bind(aE, ba)
		} catch (al) {
			f.error("events not supported " + K + "," + aE + " on jQuery.swipetp")
		}
		this.enable = function () {
			aS.bind(K, aO);
			aS.bind(aE, ba);
			return aS
		};
		this.disable = function () {
			aL();
			return aS
		};
		this.destroy = function () {
			aL();
			aS.data(C, null);
			aS = null
		};
		this.option = function (bd, bc) {
			if (aw[bd] !== undefined) {
				if (bc === undefined) {
					return aw[bd]
				} else {
					aw[bd] = bc
				}
			} else {
				f.error("Option " + bd + " does not exist on jQuery.swipetp.options")
			}
			return null
		};

		function aO(be) {
			if (aC()) {
				return
			}
			if (f(be.target).closest(aw.excludedElements, aS).length > 0) {
				return
			}
			var bf = be.originalEvent ? be.originalEvent : be;
			var bd, bg = bf.touches,
				bc = bg ? bg[0] : bf;
			aa = g;
			if (bg) {
				X = bg.length
			} else {
				be.preventDefault()
			}
			ah = 0;
			aQ = null;
			aK = null;
			ac = 0;
			a2 = 0;
			a0 = 0;
			H = 1;
			ar = 0;
			aR = ak();
			N = ab();
			S();
			if (!bg || (X === aw.fingers || aw.fingers === i) || aY()) {
				aj(0, bc);
				U = au();
				if (X == 2) {
					aj(1, bg[1]);
					a2 = a0 = av(aR[0].start, aR[1].start)
				}
				if (aw.swipeStatus || aw.pinchStatus) {
					bd = P(bf, aa)
				}
			} else {
				bd = false
			}
			if (bd === false) {
				aa = q;
				P(bf, aa);
				return bd
			} else {
				if (aw.hold) {
					ag = setTimeout(f.proxy(function () {
						aS.trigger("hold", [bf.target]);
						if (aw.hold) {
							bd = aw.hold.call(aS, bf, bf.target)
						}
					}, this), aw.longTapThreshold)
				}
				ap(true)
			}
			return null
		}

		function a4(bf) {
			var bi = bf.originalEvent ? bf.originalEvent : bf;
			if (aa === h || aa === q || an()) {
				return
			}
			var be, bj = bi.touches,
				bd = bj ? bj[0] : bi;
			var bg = aI(bd);
			a3 = au();
			if (bj) {
				X = bj.length
			}
			if (aw.hold) {
				clearTimeout(ag)
			}
			aa = k;
			if (X == 2) {
				if (a2 == 0) {
					aj(1, bj[1]);
					a2 = a0 = av(aR[0].start, aR[1].start)
				} else {
					aI(bj[1]);
					a0 = av(aR[0].end, aR[1].end);
					aK = at(aR[0].end, aR[1].end)
				}
				H = a8(a2, a0);
				ar = Math.abs(a2 - a0)
			}
			if ((X === aw.fingers || aw.fingers === i) || !bj || aY()) {
				aQ = aM(bg.start, bg.end);
				am(bf, aQ);
				ah = aT(bg.start, bg.end);
				ac = aN();
				aJ(aQ, ah);
				if (aw.swipeStatus || aw.pinchStatus) {
					be = P(bi, aa)
				}
				if (!aw.triggerOnTouchEnd || aw.triggerOnTouchLeave) {
					var bc = true;
					if (aw.triggerOnTouchLeave) {
						var bh = aZ(this);
						bc = F(bg.end, bh)
					}
					if (!aw.triggerOnTouchEnd && bc) {
						aa = aD(k)
					} else {
						if (aw.triggerOnTouchLeave && !bc) {
							aa = aD(h)
						}
					}
					if (aa == q || aa == h) {
						P(bi, aa)
					}
				}
			} else {
				aa = q;
				P(bi, aa)
			}
			if (be === false) {
				aa = q;
				P(bi, aa)
			}
		}

		function M(bc) {
			var bd = bc.originalEvent ? bc.originalEvent : bc,
				be = bd.touches;
			if (be) {
				if (be.length) {
					G();
					return true
				}
			}
			if (an()) {
				X = ae
			}
			a3 = au();
			ac = aN();
			if (bb() || !ao()) {
				aa = q;
				P(bd, aa)
			} else {
				if (aw.triggerOnTouchEnd || (aw.triggerOnTouchEnd == false && aa === k)) {
					bc.preventDefault();
					aa = h;
					P(bd, aa)
				} else {
					if (!aw.triggerOnTouchEnd && a7()) {
						aa = h;
						aG(bd, aa, B)
					} else {
						if (aa === k) {
							aa = q;
							P(bd, aa)
						}
					}
				}
			}
			ap(false);
			return null
		}

		function ba() {
			X = 0;
			a3 = 0;
			U = 0;
			a2 = 0;
			a0 = 0;
			H = 1;
			S();
			ap(false)
		}

		function L(bc) {
			var bd = bc.originalEvent ? bc.originalEvent : bc;
			if (aw.triggerOnTouchLeave) {
				aa = aD(h);
				P(bd, aa)
			}
		}

		function aL() {
			aS.unbind(K, aO);
			aS.unbind(aE, ba);
			aS.unbind(az, a4);
			aS.unbind(V, M);
			if (T) {
				aS.unbind(T, L)
			}
			ap(false)
		}

		function aD(bg) {
			var bf = bg;
			var be = aB();
			var bd = ao();
			var bc = bb();
			if (!be || bc) {
				bf = q
			} else {
				if (bd && bg == k && (!aw.triggerOnTouchEnd || aw.triggerOnTouchLeave)) {
					bf = h
				} else {
					if (!bd && bg == h && aw.triggerOnTouchLeave) {
						bf = q
					}
				}
			}
			return bf
		}

		function P(be, bc) {
			var bd, bf = be.touches;
			if ((J() || W()) || (Q() || aY())) {
				if (J() || W()) {
					bd = aG(be, bc, l)
				}
				if ((Q() || aY()) && bd !== false) {
					bd = aG(be, bc, t)
				}
			} else {
				if (aH() && bd !== false) {
					bd = aG(be, bc, j)
				} else {
					if (aq() && bd !== false) {
						bd = aG(be, bc, b)
					} else {
						if (ai() && bd !== false) {
							bd = aG(be, bc, B)
						}
					}
				}
			}
			if (bc === q) {
				ba(be)
			}
			if (bc === h) {
				if (bf) {
					if (!bf.length) {
						ba(be)
					}
				} else {
					ba(be)
				}
			}
			return bd
		}

		function aG(bf, bc, be) {
			var bd;
			if (be == l) {
				aS.trigger("swipeStatus", [bc, aQ || null, ah || 0, ac || 0, X, aR]);
				if (aw.swipeStatus) {
					bd = aw.swipeStatus.call(aS, bf, bc, aQ || null, ah || 0, ac || 0, X, aR);
					if (bd === false) {
						return false
					}
				}
				if (bc == h && aW()) {
					aS.trigger("swipe", [aQ, ah, ac, X, aR]);
					if (aw.swipe) {
						bd = aw.swipe.call(aS, bf, aQ, ah, ac, X, aR);
						if (bd === false) {
							return false
						}
					}
					switch (aQ) {
						case p:
							aS.trigger("swipeLeft", [aQ, ah, ac, X, aR]);
							if (aw.swipeLeft) {
								bd = aw.swipeLeft.call(aS, bf, aQ, ah, ac, X, aR)
							}
							break;
						case o:
							aS.trigger("swipeRight", [aQ, ah, ac, X, aR]);
							if (aw.swipeRight) {
								bd = aw.swipeRight.call(aS, bf, aQ, ah, ac, X, aR)
							}
							break;
						case e:
							aS.trigger("swipeUp", [aQ, ah, ac, X, aR]);
							if (aw.swipeUp) {
								bd = aw.swipeUp.call(aS, bf, aQ, ah, ac, X, aR)
							}
							break;
						case x:
							aS.trigger("swipeDown", [aQ, ah, ac, X, aR]);
							if (aw.swipeDown) {
								bd = aw.swipeDown.call(aS, bf, aQ, ah, ac, X, aR)
							}
							break
					}
				}
			}
			if (be == t) {
				aS.trigger("pinchStatus", [bc, aK || null, ar || 0, ac || 0, X, H, aR]);
				if (aw.pinchStatus) {
					bd = aw.pinchStatus.call(aS, bf, bc, aK || null, ar || 0, ac || 0, X, H, aR);
					if (bd === false) {
						return false
					}
				}
				if (bc == h && a9()) {
					switch (aK) {
						case c:
							aS.trigger("pinchIn", [aK || null, ar || 0, ac || 0, X, H, aR]);
							if (aw.pinchIn) {
								bd = aw.pinchIn.call(aS, bf, aK || null, ar || 0, ac || 0, X, H, aR)
							}
							break;
						case A:
							aS.trigger("pinchOut", [aK || null, ar || 0, ac || 0, X, H, aR]);
							if (aw.pinchOut) {
								bd = aw.pinchOut.call(aS, bf, aK || null, ar || 0, ac || 0, X, H, aR)
							}
							break
					}
				}
			}
			if (be == B) {
				if (bc === q || bc === h) {
					clearTimeout(aX);
					clearTimeout(ag);
					if (Z() && !I()) {
						O = au();
						aX = setTimeout(f.proxy(function () {
							O = null;
							aS.trigger("tap", [bf.target]);
							if (aw.tap) {
								bd = aw.tap.call(aS, bf, bf.target)
							}
						}, this), aw.doubleTapThreshold)
					} else {
						O = null;
						aS.trigger("tap", [bf.target]);
						if (aw.tap) {
							bd = aw.tap.call(aS, bf, bf.target)
						}
					}
				}
			} else {
				if (be == j) {
					if (bc === q || bc === h) {
						clearTimeout(aX);
						O = null;
						aS.trigger("doubletap", [bf.target]);
						if (aw.doubleTap) {
							bd = aw.doubleTap.call(aS, bf, bf.target)
						}
					}
				} else {
					if (be == b) {
						if (bc === q || bc === h) {
							clearTimeout(aX);
							O = null;
							aS.trigger("longtap", [bf.target]);
							if (aw.longTap) {
								bd = aw.longTap.call(aS, bf, bf.target)
							}
						}
					}
				}
			}
			return bd
		}

		function ao() {
			var bc = true;
			if (aw.threshold !== null) {
				bc = ah >= aw.threshold
			}
			return bc
		}

		function bb() {
			var bc = false;
			if (aw.cancelThreshold !== null && aQ !== null) {
				bc = (aU(aQ) - ah) >= aw.cancelThreshold
			}
			return bc
		}

		function af() {
			if (aw.pinchThreshold !== null) {
				return ar >= aw.pinchThreshold
			}
			return true
		}

		function aB() {
			var bc;
			if (aw.maxTimeThreshold) {
				if (ac >= aw.maxTimeThreshold) {
					bc = false
				} else {
					bc = true
				}
			} else {
				bc = true
			}
			return bc
		}

		function am(bc, bd) {
			if (aw.preventDefaultEvents === false) {
				return
			}
			if (aw.allowPageScroll === m) {
				bc.preventDefault()
			} else {
				var be = aw.allowPageScroll === s;
				switch (bd) {
					case p:
						if ((aw.swipeLeft && be) || (!be && aw.allowPageScroll != E)) {
							bc.preventDefault()
						}
						break;
					case o:
						if ((aw.swipeRight && be) || (!be && aw.allowPageScroll != E)) {
							bc.preventDefault()
						}
						break;
					case e:
						if ((aw.swipeUp && be) || (!be && aw.allowPageScroll != u)) {
							bc.preventDefault()
						}
						break;
					case x:
						if ((aw.swipeDown && be) || (!be && aw.allowPageScroll != u)) {
							bc.preventDefault()
						}
						break
				}
			}
		}

		function a9() {
			var bd = aP();
			var bc = Y();
			var be = af();
			return bd && bc && be
		}

		function aY() {
			return !!(aw.pinchStatus || aw.pinchIn || aw.pinchOut)
		}

		function Q() {
			return !!(a9() && aY())
		}

		function aW() {
			var bf = aB();
			var bh = ao();
			var be = aP();
			var bc = Y();
			var bd = bb();
			var bg = !bd && bc && be && bh && bf;
			return bg
		}

		function W() {
			return !!(aw.swipe || aw.swipeStatus || aw.swipeLeft || aw.swipeRight || aw.swipeUp || aw.swipeDown)
		}

		function J() {
			return !!(aW() && W())
		}

		function aP() {
			return ((X === aw.fingers || aw.fingers === i) || !a)
		}

		function Y() {
			return aR[0].end.x !== 0
		}

		function a7() {
			return !!(aw.tap)
		}

		function Z() {
			return !!(aw.doubleTap)
		}

		function aV() {
			return !!(aw.longTap)
		}

		function R() {
			if (O == null) {
				return false
			}
			var bc = au();
			return (Z() && ((bc - O) <= aw.doubleTapThreshold))
		}

		function I() {
			return R()
		}

		function ay() {
			return ((X === 1 || !a) && (isNaN(ah) || ah < aw.threshold))
		}

		function a1() {
			return ((ac > aw.longTapThreshold) && (ah < r))
		}

		function ai() {
			return !!(ay() && a7())
		}

		function aH() {
			return !!(R() && Z())
		}

		function aq() {
			return !!(a1() && aV())
		}

		function G() {
			a6 = au();
			ae = event.touches.length + 1
		}

		function S() {
			a6 = 0;
			ae = 0
		}

		function an() {
			var bc = false;
			if (a6) {
				var bd = au() - a6;
				if (bd <= aw.fingerReleaseThreshold) {
					bc = true
				}
			}
			return bc
		}

		function aC() {
			return !!(aS.data(C + "_intouch") === true)
		}

		function ap(bc) {
			if (bc === true) {
				aS.bind(az, a4);
				aS.bind(V, M);
				if (T) {
					aS.bind(T, L)
				}
			} else {
				aS.unbind(az, a4, false);
				aS.unbind(V, M, false);
				if (T) {
					aS.unbind(T, L, false)
				}
			}
			aS.data(C + "_intouch", bc === true)
		}

		function aj(bd, bc) {
			var be = bc.identifier !== undefined ? bc.identifier : 0;
			aR[bd].identifier = be;
			aR[bd].start.x = aR[bd].end.x = bc.pageX || bc.clientX;
			aR[bd].start.y = aR[bd].end.y = bc.pageY || bc.clientY;
			return aR[bd]
		}

		function aI(bc) {
			var be = bc.identifier !== undefined ? bc.identifier : 0;
			var bd = ad(be);
			bd.end.x = bc.pageX || bc.clientX;
			bd.end.y = bc.pageY || bc.clientY;
			return bd
		}

		function ad(bd) {
			for (var bc = 0; bc < aR.length; bc++) {
				if (aR[bc].identifier == bd) {
					return aR[bc]
				}
			}
		}

		function ak() {
			var bc = [];
			for (var bd = 0; bd <= 5; bd++) {
				bc.push({
					start: {
						x: 0,
						y: 0
					},
					end: {
						x: 0,
						y: 0
					},
					identifier: 0
				})
			}
			return bc
		}

		function aJ(bc, bd) {
			bd = Math.max(bd, aU(bc));
			N[bc].distance = bd
		}

		function aU(bc) {
			if (N[bc]) {
				return N[bc].distance
			}
			return undefined
		}

		function ab() {
			var bc = {};
			bc[p] = ax(p);
			bc[o] = ax(o);
			bc[e] = ax(e);
			bc[x] = ax(x);
			return bc
		}

		function ax(bc) {
			return {
				direction: bc,
				distance: 0
			}
		}

		function aN() {
			return a3 - U
		}

		function av(bf, be) {
			var bd = Math.abs(bf.x - be.x);
			var bc = Math.abs(bf.y - be.y);
			return Math.round(Math.sqrt(bd * bd + bc * bc))
		}

		function a8(bc, bd) {
			var be = (bd / bc) * 1;
			return be.toFixed(2)
		}

		function at() {
			if (H < 1) {
				return A
			} else {
				return c
			}
		}

		function aT(bd, bc) {
			return Math.round(Math.sqrt(Math.pow(bc.x - bd.x, 2) + Math.pow(bc.y - bd.y, 2)))
		}

		function aF(bf, bd) {
			var bc = bf.x - bd.x;
			var bh = bd.y - bf.y;
			var be = Math.atan2(bh, bc);
			var bg = Math.round(be * 180 / Math.PI);
			if (bg < 0) {
				bg = 360 - Math.abs(bg)
			}
			return bg
		}

		function aM(bd, bc) {
			var be = aF(bd, bc);
			if ((be <= 45) && (be >= 0)) {
				return p
			} else {
				if ((be <= 360) && (be >= 315)) {
					return p
				} else {
					if ((be >= 135) && (be <= 225)) {
						return o
					} else {
						if ((be > 45) && (be < 135)) {
							return x
						} else {
							return e
						}
					}
				}
			}
		}

		function au() {
			var bc = new Date();
			return bc.getTime()
		}

		function aZ(bc) {
			bc = f(bc);
			var be = bc.offset();
			var bd = {
				left: be.left,
				right: be.left + bc.outerWidth(),
				top: be.top,
				bottom: be.top + bc.outerHeight()
			};
			return bd
		}

		function F(bc, bd) {
			return (bc.x > bd.left && bc.x < bd.right && bc.y > bd.top && bc.y < bd.bottom)
		}
	}
}));

if (typeof (console) === 'undefined') {
	var console = {};
	console.log = console.error = console.info = console.debug = console.warn = console.trace = console.dir = console.dirxml = console.group = console.groupEnd = console.time = console.timeEnd = console.assert = console.profile = console.groupCollapsed = function () {};
}

if (window.tplogs == true)
	try {
		console.groupCollapsed("ThemePunch GreenSocks Logs");
	} catch (e) {}


var oldgs = window.GreenSockGlobals;
oldgs_queue = window._gsQueue;

var punchgs = window.GreenSockGlobals = {};

if (window.tplogs == true)
	try {
		console.info("Build GreenSock SandBox for ThemePunch Plugins");
		console.info("GreenSock TweenLite Engine Initalised by ThemePunch Plugin");
	} catch (e) {}


	/* TWEEN LITE */
	/*!
	 * VERSION: 1.19.1
	 * DATE: 2017-01-17
	 * UPDATES AND DOCS AT: http://greensock.com
	 *
	 * @license Copyright (c) 2008-2017, GreenSock. All rights reserved.
	 * This work is subject to the terms at http://greensock.com/standard-license or for
	 * Club GreenSock members, the software agreement that was issued with your membership.
	 * 
	 * @author: Jack Doyle, jack@greensock.com
	 */
	! function (a, b) {
		"use strict";
		var c = {},
			d = a.document,
			e = a.GreenSockGlobals = a.GreenSockGlobals || a;
		if (!e.TweenLite) {
			var f, g, h, i, j, k = function (a) {
					var b, c = a.split("."),
						d = e;
					for (b = 0; b < c.length; b++) d[c[b]] = d = d[c[b]] || {};
					return d
				},
				l = k("com.greensock"),
				m = 1e-10,
				n = function (a) {
					var b, c = [],
						d = a.length;
					for (b = 0; b !== d; c.push(a[b++]));
					return c
				},
				o = function () {},
				p = function () {
					var a = Object.prototype.toString,
						b = a.call([]);
					return function (c) {
						return null != c && (c instanceof Array || "object" == typeof c && !!c.push && a.call(c) === b)
					}
				}(),
				q = {},
				r = function (d, f, g, h) {
					this.sc = q[d] ? q[d].sc : [], q[d] = this, this.gsClass = null, this.func = g;
					var i = [];
					this.check = function (j) {
						for (var l, m, n, o, p, s = f.length, t = s; --s > -1;)(l = q[f[s]] || new r(f[s], [])).gsClass ? (i[s] = l.gsClass, t--) : j && l.sc.push(this);
						if (0 === t && g) {
							if (m = ("com.greensock." + d).split("."), n = m.pop(), o = k(m.join("."))[n] = this.gsClass = g.apply(g, i), h)
								if (e[n] = c[n] = o, p = "undefined" != typeof module && module.exports, !p && "function" == typeof define && define.amd) define((a.GreenSockAMDPath ? a.GreenSockAMDPath + "/" : "") + d.split(".").pop(), [], function () {
									return o
								});
								else if (p)
								if (d === b) {
									module.exports = c[b] = o;
									for (s in c) o[s] = c[s]
								} else c[b] && (c[b][n] = o);
							for (s = 0; s < this.sc.length; s++) this.sc[s].check()
						}
					}, this.check(!0)
				},
				s = a._gsDefine = function (a, b, c, d) {
					return new r(a, b, c, d)
				},
				t = l._class = function (a, b, c) {
					return b = b || function () {}, s(a, [], function () {
						return b
					}, c), b
				};
			s.globals = e;
			var u = [0, 0, 1, 1],
				v = t("easing.Ease", function (a, b, c, d) {
					this._func = a, this._type = c || 0, this._power = d || 0, this._params = b ? u.concat(b) : u
				}, !0),
				w = v.map = {},
				x = v.register = function (a, b, c, d) {
					for (var e, f, g, h, i = b.split(","), j = i.length, k = (c || "easeIn,easeOut,easeInOut").split(","); --j > -1;)
						for (f = i[j], e = d ? t("easing." + f, null, !0) : l.easing[f] || {}, g = k.length; --g > -1;) h = k[g], w[f + "." + h] = w[h + f] = e[h] = a.getRatio ? a : a[h] || new a
				};
			for (h = v.prototype, h._calcEnd = !1, h.getRatio = function (a) {
					if (this._func) return this._params[0] = a, this._func.apply(null, this._params);
					var b = this._type,
						c = this._power,
						d = 1 === b ? 1 - a : 2 === b ? a : .5 > a ? 2 * a : 2 * (1 - a);
					return 1 === c ? d *= d : 2 === c ? d *= d * d : 3 === c ? d *= d * d * d : 4 === c && (d *= d * d * d * d), 1 === b ? 1 - d : 2 === b ? d : .5 > a ? d / 2 : 1 - d / 2
				}, f = ["Linear", "Quad", "Cubic", "Quart", "Quint,Strong"], g = f.length; --g > -1;) h = f[g] + ",Power" + g, x(new v(null, null, 1, g), h, "easeOut", !0), x(new v(null, null, 2, g), h, "easeIn" + (0 === g ? ",easeNone" : "")), x(new v(null, null, 3, g), h, "easeInOut");
			w.linear = l.easing.Linear.easeIn, w.swing = l.easing.Quad.easeInOut;
			var y = t("events.EventDispatcher", function (a) {
				this._listeners = {}, this._eventTarget = a || this
			});
			h = y.prototype, h.addEventListener = function (a, b, c, d, e) {
				e = e || 0;
				var f, g, h = this._listeners[a],
					k = 0;
				for (this !== i || j || i.wake(), null == h && (this._listeners[a] = h = []), g = h.length; --g > -1;) f = h[g], f.c === b && f.s === c ? h.splice(g, 1) : 0 === k && f.pr < e && (k = g + 1);
				h.splice(k, 0, {
					c: b,
					s: c,
					up: d,
					pr: e
				})
			}, h.removeEventListener = function (a, b) {
				var c, d = this._listeners[a];
				if (d)
					for (c = d.length; --c > -1;)
						if (d[c].c === b) return void d.splice(c, 1)
			}, h.dispatchEvent = function (a) {
				var b, c, d, e = this._listeners[a];
				if (e)
					for (b = e.length, b > 1 && (e = e.slice(0)), c = this._eventTarget; --b > -1;) d = e[b], d && (d.up ? d.c.call(d.s || c, {
						type: a,
						target: c
					}) : d.c.call(d.s || c))
			};
			var z = a.requestAnimationFrame,
				A = a.cancelAnimationFrame,
				B = Date.now || function () {
					return (new Date).getTime()
				},
				C = B();
			for (f = ["ms", "moz", "webkit", "o"], g = f.length; --g > -1 && !z;) z = a[f[g] + "RequestAnimationFrame"], A = a[f[g] + "CancelAnimationFrame"] || a[f[g] + "CancelRequestAnimationFrame"];
			t("Ticker", function (a, b) {
				var c, e, f, g, h, k = this,
					l = B(),
					n = b !== !1 && z ? "auto" : !1,
					p = 500,
					q = 33,
					r = "tick",
					s = function (a) {
						var b, d, i = B() - C;
						i > p && (l += i - q), C += i, k.time = (C - l) / 1e3, b = k.time - h, (!c || b > 0 || a === !0) && (k.frame++, h += b + (b >= g ? .004 : g - b), d = !0), a !== !0 && (f = e(s)), d && k.dispatchEvent(r)
					};
				y.call(k), k.time = k.frame = 0, k.tick = function () {
					s(!0)
				}, k.lagSmoothing = function (a, b) {
					p = a || 1 / m, q = Math.min(b, p, 0)
				}, k.sleep = function () {
					null != f && (n && A ? A(f) : clearTimeout(f), e = o, f = null, k === i && (j = !1))
				}, k.wake = function (a) {
					null !== f ? k.sleep() : a ? l += -C + (C = B()) : k.frame > 10 && (C = B() - p + 5), e = 0 === c ? o : n && z ? z : function (a) {
						return setTimeout(a, 1e3 * (h - k.time) + 1 | 0)
					}, k === i && (j = !0), s(2)
				}, k.fps = function (a) {
					return arguments.length ? (c = a, g = 1 / (c || 60), h = this.time + g, void k.wake()) : c
				}, k.useRAF = function (a) {
					return arguments.length ? (k.sleep(), n = a, void k.fps(c)) : n
				}, k.fps(a), setTimeout(function () {
					"auto" === n && k.frame < 5 && "hidden" !== d.visibilityState && k.useRAF(!1)
				}, 1500)
			}), h = l.Ticker.prototype = new l.events.EventDispatcher, h.constructor = l.Ticker;
			var D = t("core.Animation", function (a, b) {
				if (this.vars = b = b || {}, this._duration = this._totalDuration = a || 0, this._delay = Number(b.delay) || 0, this._timeScale = 1, this._active = b.immediateRender === !0, this.data = b.data, this._reversed = b.reversed === !0, W) {
					j || i.wake();
					var c = this.vars.useFrames ? V : W;
					c.add(this, c._time), this.vars.paused && this.paused(!0)
				}
			});
			i = D.ticker = new l.Ticker, h = D.prototype, h._dirty = h._gc = h._initted = h._paused = !1, h._totalTime = h._time = 0, h._rawPrevTime = -1, h._next = h._last = h._onUpdate = h._timeline = h.timeline = null, h._paused = !1;
			var E = function () {
				j && B() - C > 2e3 && i.wake(), setTimeout(E, 2e3)
			};
			E(), h.play = function (a, b) {
				return null != a && this.seek(a, b), this.reversed(!1).paused(!1)
			}, h.pause = function (a, b) {
				return null != a && this.seek(a, b), this.paused(!0)
			}, h.resume = function (a, b) {
				return null != a && this.seek(a, b), this.paused(!1)
			}, h.seek = function (a, b) {
				return this.totalTime(Number(a), b !== !1)
			}, h.restart = function (a, b) {
				return this.reversed(!1).paused(!1).totalTime(a ? -this._delay : 0, b !== !1, !0)
			}, h.reverse = function (a, b) {
				return null != a && this.seek(a || this.totalDuration(), b), this.reversed(!0).paused(!1)
			}, h.render = function (a, b, c) {}, h.invalidate = function () {
				return this._time = this._totalTime = 0, this._initted = this._gc = !1, this._rawPrevTime = -1, (this._gc || !this.timeline) && this._enabled(!0), this
			}, h.isActive = function () {
				var a, b = this._timeline,
					c = this._startTime;
				return !b || !this._gc && !this._paused && b.isActive() && (a = b.rawTime(!0)) >= c && a < c + this.totalDuration() / this._timeScale
			}, h._enabled = function (a, b) {
				return j || i.wake(), this._gc = !a, this._active = this.isActive(), b !== !0 && (a && !this.timeline ? this._timeline.add(this, this._startTime - this._delay) : !a && this.timeline && this._timeline._remove(this, !0)), !1
			}, h._kill = function (a, b) {
				return this._enabled(!1, !1)
			}, h.kill = function (a, b) {
				return this._kill(a, b), this
			}, h._uncache = function (a) {
				for (var b = a ? this : this.timeline; b;) b._dirty = !0, b = b.timeline;
				return this
			}, h._swapSelfInParams = function (a) {
				for (var b = a.length, c = a.concat(); --b > -1;) "{self}" === a[b] && (c[b] = this);
				return c
			}, h._callback = function (a) {
				var b = this.vars,
					c = b[a],
					d = b[a + "Params"],
					e = b[a + "Scope"] || b.callbackScope || this,
					f = d ? d.length : 0;
				switch (f) {
					case 0:
						c.call(e);
						break;
					case 1:
						c.call(e, d[0]);
						break;
					case 2:
						c.call(e, d[0], d[1]);
						break;
					default:
						c.apply(e, d)
				}
			}, h.eventCallback = function (a, b, c, d) {
				if ("on" === (a || "").substr(0, 2)) {
					var e = this.vars;
					if (1 === arguments.length) return e[a];
					null == b ? delete e[a] : (e[a] = b, e[a + "Params"] = p(c) && -1 !== c.join("").indexOf("{self}") ? this._swapSelfInParams(c) : c, e[a + "Scope"] = d), "onUpdate" === a && (this._onUpdate = b)
				}
				return this
			}, h.delay = function (a) {
				return arguments.length ? (this._timeline.smoothChildTiming && this.startTime(this._startTime + a - this._delay), this._delay = a, this) : this._delay
			}, h.duration = function (a) {
				return arguments.length ? (this._duration = this._totalDuration = a, this._uncache(!0), this._timeline.smoothChildTiming && this._time > 0 && this._time < this._duration && 0 !== a && this.totalTime(this._totalTime * (a / this._duration), !0), this) : (this._dirty = !1, this._duration)
			}, h.totalDuration = function (a) {
				return this._dirty = !1, arguments.length ? this.duration(a) : this._totalDuration
			}, h.time = function (a, b) {
				return arguments.length ? (this._dirty && this.totalDuration(), this.totalTime(a > this._duration ? this._duration : a, b)) : this._time
			}, h.totalTime = function (a, b, c) {
				if (j || i.wake(), !arguments.length) return this._totalTime;
				if (this._timeline) {
					if (0 > a && !c && (a += this.totalDuration()), this._timeline.smoothChildTiming) {
						this._dirty && this.totalDuration();
						var d = this._totalDuration,
							e = this._timeline;
						if (a > d && !c && (a = d), this._startTime = (this._paused ? this._pauseTime : e._time) - (this._reversed ? d - a : a) / this._timeScale, e._dirty || this._uncache(!1), e._timeline)
							for (; e._timeline;) e._timeline._time !== (e._startTime + e._totalTime) / e._timeScale && e.totalTime(e._totalTime, !0), e = e._timeline
					}
					this._gc && this._enabled(!0, !1), (this._totalTime !== a || 0 === this._duration) && (J.length && Y(), this.render(a, b, !1), J.length && Y())
				}
				return this
			}, h.progress = h.totalProgress = function (a, b) {
				var c = this.duration();
				return arguments.length ? this.totalTime(c * a, b) : c ? this._time / c : this.ratio
			}, h.startTime = function (a) {
				return arguments.length ? (a !== this._startTime && (this._startTime = a, this.timeline && this.timeline._sortChildren && this.timeline.add(this, a - this._delay)), this) : this._startTime
			}, h.endTime = function (a) {
				return this._startTime + (0 != a ? this.totalDuration() : this.duration()) / this._timeScale
			}, h.timeScale = function (a) {
				if (!arguments.length) return this._timeScale;
				if (a = a || m, this._timeline && this._timeline.smoothChildTiming) {
					var b = this._pauseTime,
						c = b || 0 === b ? b : this._timeline.totalTime();
					this._startTime = c - (c - this._startTime) * this._timeScale / a
				}
				return this._timeScale = a, this._uncache(!1)
			}, h.reversed = function (a) {
				return arguments.length ? (a != this._reversed && (this._reversed = a, this.totalTime(this._timeline && !this._timeline.smoothChildTiming ? this.totalDuration() - this._totalTime : this._totalTime, !0)), this) : this._reversed
			}, h.paused = function (a) {
				if (!arguments.length) return this._paused;
				var b, c, d = this._timeline;
				return a != this._paused && d && (j || a || i.wake(), b = d.rawTime(), c = b - this._pauseTime, !a && d.smoothChildTiming && (this._startTime += c, this._uncache(!1)), this._pauseTime = a ? b : null, this._paused = a, this._active = this.isActive(), !a && 0 !== c && this._initted && this.duration() && (b = d.smoothChildTiming ? this._totalTime : (b - this._startTime) / this._timeScale, this.render(b, b === this._totalTime, !0))), this._gc && !a && this._enabled(!0, !1), this
			};
			var F = t("core.SimpleTimeline", function (a) {
				D.call(this, 0, a), this.autoRemoveChildren = this.smoothChildTiming = !0
			});
			h = F.prototype = new D, h.constructor = F, h.kill()._gc = !1, h._first = h._last = h._recent = null, h._sortChildren = !1, h.add = h.insert = function (a, b, c, d) {
				var e, f;
				if (a._startTime = Number(b || 0) + a._delay, a._paused && this !== a._timeline && (a._pauseTime = a._startTime + (this.rawTime() - a._startTime) / a._timeScale), a.timeline && a.timeline._remove(a, !0), a.timeline = a._timeline = this, a._gc && a._enabled(!0, !0), e = this._last, this._sortChildren)
					for (f = a._startTime; e && e._startTime > f;) e = e._prev;
				return e ? (a._next = e._next, e._next = a) : (a._next = this._first, this._first = a), a._next ? a._next._prev = a : this._last = a, a._prev = e, this._recent = a, this._timeline && this._uncache(!0), this
			}, h._remove = function (a, b) {
				return a.timeline === this && (b || a._enabled(!1, !0), a._prev ? a._prev._next = a._next : this._first === a && (this._first = a._next), a._next ? a._next._prev = a._prev : this._last === a && (this._last = a._prev), a._next = a._prev = a.timeline = null, a === this._recent && (this._recent = this._last), this._timeline && this._uncache(!0)), this
			}, h.render = function (a, b, c) {
				var d, e = this._first;
				for (this._totalTime = this._time = this._rawPrevTime = a; e;) d = e._next, (e._active || a >= e._startTime && !e._paused) && (e._reversed ? e.render((e._dirty ? e.totalDuration() : e._totalDuration) - (a - e._startTime) * e._timeScale, b, c) : e.render((a - e._startTime) * e._timeScale, b, c)), e = d
			}, h.rawTime = function () {
				return j || i.wake(), this._totalTime
			};
			var G = t("TweenLite", function (b, c, d) {
					if (D.call(this, c, d), this.render = G.prototype.render, null == b) throw "Cannot tween a null target.";
					this.target = b = "string" != typeof b ? b : G.selector(b) || b;
					var e, f, g, h = b.jquery || b.length && b !== a && b[0] && (b[0] === a || b[0].nodeType && b[0].style && !b.nodeType),
						i = this.vars.overwrite;
					if (this._overwrite = i = null == i ? U[G.defaultOverwrite] : "number" == typeof i ? i >> 0 : U[i], (h || b instanceof Array || b.push && p(b)) && "number" != typeof b[0])
						for (this._targets = g = n(b), this._propLookup = [], this._siblings = [], e = 0; e < g.length; e++) f = g[e], f ? "string" != typeof f ? f.length && f !== a && f[0] && (f[0] === a || f[0].nodeType && f[0].style && !f.nodeType) ? (g.splice(e--, 1), this._targets = g = g.concat(n(f))) : (this._siblings[e] = Z(f, this, !1), 1 === i && this._siblings[e].length > 1 && _(f, this, null, 1, this._siblings[e])) : (f = g[e--] = G.selector(f), "string" == typeof f && g.splice(e + 1, 1)) : g.splice(e--, 1);
					else this._propLookup = {}, this._siblings = Z(b, this, !1), 1 === i && this._siblings.length > 1 && _(b, this, null, 1, this._siblings);
					(this.vars.immediateRender || 0 === c && 0 === this._delay && this.vars.immediateRender !== !1) && (this._time = -m, this.render(Math.min(0, -this._delay)))
				}, !0),
				H = function (b) {
					return b && b.length && b !== a && b[0] && (b[0] === a || b[0].nodeType && b[0].style && !b.nodeType)
				},
				I = function (a, b) {
					var c, d = {};
					for (c in a) T[c] || c in b && "transform" !== c && "x" !== c && "y" !== c && "width" !== c && "height" !== c && "className" !== c && "border" !== c || !(!Q[c] || Q[c] && Q[c]._autoCSS) || (d[c] = a[c], delete a[c]);
					a.css = d
				};
			h = G.prototype = new D, h.constructor = G, h.kill()._gc = !1, h.ratio = 0, h._firstPT = h._targets = h._overwrittenProps = h._startAt = null, h._notifyPluginsOfEnabled = h._lazy = !1, G.version = "1.19.1", G.defaultEase = h._ease = new v(null, null, 1, 1), G.defaultOverwrite = "auto", G.ticker = i, G.autoSleep = 120, G.lagSmoothing = function (a, b) {
				i.lagSmoothing(a, b)
			}, G.selector = a.$ || a.jQuery || function (b) {
				var c = a.$ || a.jQuery;
				return c ? (G.selector = c, c(b)) : "undefined" == typeof d ? b : d.querySelectorAll ? d.querySelectorAll(b) : d.getElementById("#" === b.charAt(0) ? b.substr(1) : b)
			};
			var J = [],
				K = {},
				L = /(?:(-|-=|\+=)?\d*\.?\d*(?:e[\-+]?\d+)?)[0-9]/gi,
				M = function (a) {
					for (var b, c = this._firstPT, d = 1e-6; c;) b = c.blob ? 1 === a ? this.end : a ? this.join("") : this.start : c.c * a + c.s, c.m ? b = c.m(b, this._target || c.t) : d > b && b > -d && !c.blob && (b = 0), c.f ? c.fp ? c.t[c.p](c.fp, b) : c.t[c.p](b) : c.t[c.p] = b, c = c._next
				},
				N = function (a, b, c, d) {
					var e, f, g, h, i, j, k, l = [],
						m = 0,
						n = "",
						o = 0;
					for (l.start = a, l.end = b, a = l[0] = a + "", b = l[1] = b + "", c && (c(l), a = l[0], b = l[1]), l.length = 0, e = a.match(L) || [], f = b.match(L) || [], d && (d._next = null, d.blob = 1, l._firstPT = l._applyPT = d), i = f.length, h = 0; i > h; h++) k = f[h], j = b.substr(m, b.indexOf(k, m) - m), n += j || !h ? j : ",", m += j.length, o ? o = (o + 1) % 5 : "rgba(" === j.substr(-5) && (o = 1), k === e[h] || e.length <= h ? n += k : (n && (l.push(n), n = ""), g = parseFloat(e[h]), l.push(g), l._firstPT = {
						_next: l._firstPT,
						t: l,
						p: l.length - 1,
						s: g,
						c: ("=" === k.charAt(1) ? parseInt(k.charAt(0) + "1", 10) * parseFloat(k.substr(2)) : parseFloat(k) - g) || 0,
						f: 0,
						m: o && 4 > o ? Math.round : 0
					}), m += k.length;
					return n += b.substr(m), n && l.push(n), l.setRatio = M, l
				},
				O = function (a, b, c, d, e, f, g, h, i) {
					"function" == typeof d && (d = d(i || 0, a));
					var j, k = typeof a[b],
						l = "function" !== k ? "" : b.indexOf("set") || "function" != typeof a["get" + b.substr(3)] ? b : "get" + b.substr(3),
						m = "get" !== c ? c : l ? g ? a[l](g) : a[l]() : a[b],
						n = "string" == typeof d && "=" === d.charAt(1),
						o = {
							t: a,
							p: b,
							s: m,
							f: "function" === k,
							pg: 0,
							n: e || b,
							m: f ? "function" == typeof f ? f : Math.round : 0,
							pr: 0,
							c: n ? parseInt(d.charAt(0) + "1", 10) * parseFloat(d.substr(2)) : parseFloat(d) - m || 0
						};
					return ("number" != typeof m || "number" != typeof d && !n) && (g || isNaN(m) || !n && isNaN(d) || "boolean" == typeof m || "boolean" == typeof d ? (o.fp = g, j = N(m, n ? o.s + o.c : d, h || G.defaultStringFilter, o), o = {
						t: j,
						p: "setRatio",
						s: 0,
						c: 1,
						f: 2,
						pg: 0,
						n: e || b,
						pr: 0,
						m: 0
					}) : (o.s = parseFloat(m), n || (o.c = parseFloat(d) - o.s || 0))), o.c ? ((o._next = this._firstPT) && (o._next._prev = o), this._firstPT = o, o) : void 0
				},
				P = G._internals = {
					isArray: p,
					isSelector: H,
					lazyTweens: J,
					blobDif: N
				},
				Q = G._plugins = {},
				R = P.tweenLookup = {},
				S = 0,
				T = P.reservedProps = {
					ease: 1,
					delay: 1,
					overwrite: 1,
					onComplete: 1,
					onCompleteParams: 1,
					onCompleteScope: 1,
					useFrames: 1,
					runBackwards: 1,
					startAt: 1,
					onUpdate: 1,
					onUpdateParams: 1,
					onUpdateScope: 1,
					onStart: 1,
					onStartParams: 1,
					onStartScope: 1,
					onReverseComplete: 1,
					onReverseCompleteParams: 1,
					onReverseCompleteScope: 1,
					onRepeat: 1,
					onRepeatParams: 1,
					onRepeatScope: 1,
					easeParams: 1,
					yoyo: 1,
					immediateRender: 1,
					repeat: 1,
					repeatDelay: 1,
					data: 1,
					paused: 1,
					reversed: 1,
					autoCSS: 1,
					lazy: 1,
					onOverwrite: 1,
					callbackScope: 1,
					stringFilter: 1,
					id: 1
				},
				U = {
					none: 0,
					all: 1,
					auto: 2,
					concurrent: 3,
					allOnStart: 4,
					preexisting: 5,
					"true": 1,
					"false": 0
				},
				V = D._rootFramesTimeline = new F,
				W = D._rootTimeline = new F,
				X = 30,
				Y = P.lazyRender = function () {
					var a, b = J.length;
					for (K = {}; --b > -1;) a = J[b], a && a._lazy !== !1 && (a.render(a._lazy[0], a._lazy[1], !0), a._lazy = !1);
					J.length = 0
				};
			W._startTime = i.time, V._startTime = i.frame, W._active = V._active = !0, setTimeout(Y, 1), D._updateRoot = G.render = function () {
				var a, b, c;
				if (J.length && Y(), W.render((i.time - W._startTime) * W._timeScale, !1, !1), V.render((i.frame - V._startTime) * V._timeScale, !1, !1), J.length && Y(), i.frame >= X) {
					X = i.frame + (parseInt(G.autoSleep, 10) || 120);
					for (c in R) {
						for (b = R[c].tweens, a = b.length; --a > -1;) b[a]._gc && b.splice(a, 1);
						0 === b.length && delete R[c]
					}
					if (c = W._first, (!c || c._paused) && G.autoSleep && !V._first && 1 === i._listeners.tick.length) {
						for (; c && c._paused;) c = c._next;
						c || i.sleep()
					}
				}
			}, i.addEventListener("tick", D._updateRoot);
			var Z = function (a, b, c) {
					var d, e, f = a._gsTweenID;
					if (R[f || (a._gsTweenID = f = "t" + S++)] || (R[f] = {
							target: a,
							tweens: []
						}), b && (d = R[f].tweens, d[e = d.length] = b, c))
						for (; --e > -1;) d[e] === b && d.splice(e, 1);
					return R[f].tweens
				},
				$ = function (a, b, c, d) {
					var e, f, g = a.vars.onOverwrite;
					return g && (e = g(a, b, c, d)), g = G.onOverwrite, g && (f = g(a, b, c, d)), e !== !1 && f !== !1
				},
				_ = function (a, b, c, d, e) {
					var f, g, h, i;
					if (1 === d || d >= 4) {
						for (i = e.length, f = 0; i > f; f++)
							if ((h = e[f]) !== b) h._gc || h._kill(null, a, b) && (g = !0);
							else if (5 === d) break;
						return g
					}
					var j, k = b._startTime + m,
						l = [],
						n = 0,
						o = 0 === b._duration;
					for (f = e.length; --f > -1;)(h = e[f]) === b || h._gc || h._paused || (h._timeline !== b._timeline ? (j = j || aa(b, 0, o), 0 === aa(h, j, o) && (l[n++] = h)) : h._startTime <= k && h._startTime + h.totalDuration() / h._timeScale > k && ((o || !h._initted) && k - h._startTime <= 2e-10 || (l[n++] = h)));
					for (f = n; --f > -1;)
						if (h = l[f], 2 === d && h._kill(c, a, b) && (g = !0), 2 !== d || !h._firstPT && h._initted) {
							if (2 !== d && !$(h, b)) continue;
							h._enabled(!1, !1) && (g = !0)
						}
					return g
				},
				aa = function (a, b, c) {
					for (var d = a._timeline, e = d._timeScale, f = a._startTime; d._timeline;) {
						if (f += d._startTime, e *= d._timeScale, d._paused) return -100;
						d = d._timeline
					}
					return f /= e, f > b ? f - b : c && f === b || !a._initted && 2 * m > f - b ? m : (f += a.totalDuration() / a._timeScale / e) > b + m ? 0 : f - b - m
				};
			h._init = function () {
				var a, b, c, d, e, f, g = this.vars,
					h = this._overwrittenProps,
					i = this._duration,
					j = !!g.immediateRender,
					k = g.ease;
				if (g.startAt) {
					this._startAt && (this._startAt.render(-1, !0), this._startAt.kill()), e = {};
					for (d in g.startAt) e[d] = g.startAt[d];
					if (e.overwrite = !1, e.immediateRender = !0, e.lazy = j && g.lazy !== !1, e.startAt = e.delay = null, this._startAt = G.to(this.target, 0, e), j)
						if (this._time > 0) this._startAt = null;
						else if (0 !== i) return
				} else if (g.runBackwards && 0 !== i)
					if (this._startAt) this._startAt.render(-1, !0), this._startAt.kill(), this._startAt = null;
					else {
						0 !== this._time && (j = !1), c = {};
						for (d in g) T[d] && "autoCSS" !== d || (c[d] = g[d]);
						if (c.overwrite = 0, c.data = "isFromStart", c.lazy = j && g.lazy !== !1, c.immediateRender = j, this._startAt = G.to(this.target, 0, c), j) {
							if (0 === this._time) return
						} else this._startAt._init(), this._startAt._enabled(!1), this.vars.immediateRender && (this._startAt = null)
					}
				if (this._ease = k = k ? k instanceof v ? k : "function" == typeof k ? new v(k, g.easeParams) : w[k] || G.defaultEase : G.defaultEase, g.easeParams instanceof Array && k.config && (this._ease = k.config.apply(k, g.easeParams)), this._easeType = this._ease._type, this._easePower = this._ease._power, this._firstPT = null, this._targets)
					for (f = this._targets.length, a = 0; f > a; a++) this._initProps(this._targets[a], this._propLookup[a] = {}, this._siblings[a], h ? h[a] : null, a) && (b = !0);
				else b = this._initProps(this.target, this._propLookup, this._siblings, h, 0);
				if (b && G._onPluginEvent("_onInitAllProps", this), h && (this._firstPT || "function" != typeof this.target && this._enabled(!1, !1)), g.runBackwards)
					for (c = this._firstPT; c;) c.s += c.c, c.c = -c.c, c = c._next;
				this._onUpdate = g.onUpdate, this._initted = !0
			}, h._initProps = function (b, c, d, e, f) {
				var g, h, i, j, k, l;
				if (null == b) return !1;
				K[b._gsTweenID] && Y(), this.vars.css || b.style && b !== a && b.nodeType && Q.css && this.vars.autoCSS !== !1 && I(this.vars, b);
				for (g in this.vars)
					if (l = this.vars[g], T[g]) l && (l instanceof Array || l.push && p(l)) && -1 !== l.join("").indexOf("{self}") && (this.vars[g] = l = this._swapSelfInParams(l, this));
					else if (Q[g] && (j = new Q[g])._onInitTween(b, this.vars[g], this, f)) {
					for (this._firstPT = k = {
							_next: this._firstPT,
							t: j,
							p: "setRatio",
							s: 0,
							c: 1,
							f: 1,
							n: g,
							pg: 1,
							pr: j._priority,
							m: 0
						}, h = j._overwriteProps.length; --h > -1;) c[j._overwriteProps[h]] = this._firstPT;
					(j._priority || j._onInitAllProps) && (i = !0), (j._onDisable || j._onEnable) && (this._notifyPluginsOfEnabled = !0), k._next && (k._next._prev = k)
				} else c[g] = O.call(this, b, g, "get", l, g, 0, null, this.vars.stringFilter, f);
				return e && this._kill(e, b) ? this._initProps(b, c, d, e, f) : this._overwrite > 1 && this._firstPT && d.length > 1 && _(b, this, c, this._overwrite, d) ? (this._kill(c, b), this._initProps(b, c, d, e, f)) : (this._firstPT && (this.vars.lazy !== !1 && this._duration || this.vars.lazy && !this._duration) && (K[b._gsTweenID] = !0), i)
			}, h.render = function (a, b, c) {
				var d, e, f, g, h = this._time,
					i = this._duration,
					j = this._rawPrevTime;
				if (a >= i - 1e-7 && a >= 0) this._totalTime = this._time = i, this.ratio = this._ease._calcEnd ? this._ease.getRatio(1) : 1, this._reversed || (d = !0, e = "onComplete", c = c || this._timeline.autoRemoveChildren), 0 === i && (this._initted || !this.vars.lazy || c) && (this._startTime === this._timeline._duration && (a = 0), (0 > j || 0 >= a && a >= -1e-7 || j === m && "isPause" !== this.data) && j !== a && (c = !0, j > m && (e = "onReverseComplete")), this._rawPrevTime = g = !b || a || j === a ? a : m);
				else if (1e-7 > a) this._totalTime = this._time = 0, this.ratio = this._ease._calcEnd ? this._ease.getRatio(0) : 0, (0 !== h || 0 === i && j > 0) && (e = "onReverseComplete", d = this._reversed), 0 > a && (this._active = !1, 0 === i && (this._initted || !this.vars.lazy || c) && (j >= 0 && (j !== m || "isPause" !== this.data) && (c = !0), this._rawPrevTime = g = !b || a || j === a ? a : m)), this._initted || (c = !0);
				else if (this._totalTime = this._time = a, this._easeType) {
					var k = a / i,
						l = this._easeType,
						n = this._easePower;
					(1 === l || 3 === l && k >= .5) && (k = 1 - k), 3 === l && (k *= 2), 1 === n ? k *= k : 2 === n ? k *= k * k : 3 === n ? k *= k * k * k : 4 === n && (k *= k * k * k * k), 1 === l ? this.ratio = 1 - k : 2 === l ? this.ratio = k : .5 > a / i ? this.ratio = k / 2 : this.ratio = 1 - k / 2
				} else this.ratio = this._ease.getRatio(a / i);
				if (this._time !== h || c) {
					if (!this._initted) {
						if (this._init(), !this._initted || this._gc) return;
						if (!c && this._firstPT && (this.vars.lazy !== !1 && this._duration || this.vars.lazy && !this._duration)) return this._time = this._totalTime = h, this._rawPrevTime = j, J.push(this), void(this._lazy = [a, b]);
						this._time && !d ? this.ratio = this._ease.getRatio(this._time / i) : d && this._ease._calcEnd && (this.ratio = this._ease.getRatio(0 === this._time ? 0 : 1))
					}
					for (this._lazy !== !1 && (this._lazy = !1), this._active || !this._paused && this._time !== h && a >= 0 && (this._active = !0), 0 === h && (this._startAt && (a >= 0 ? this._startAt.render(a, b, c) : e || (e = "_dummyGS")), this.vars.onStart && (0 !== this._time || 0 === i) && (b || this._callback("onStart"))), f = this._firstPT; f;) f.f ? f.t[f.p](f.c * this.ratio + f.s) : f.t[f.p] = f.c * this.ratio + f.s, f = f._next;
					this._onUpdate && (0 > a && this._startAt && a !== -1e-4 && this._startAt.render(a, b, c), b || (this._time !== h || d || c) && this._callback("onUpdate")), e && (!this._gc || c) && (0 > a && this._startAt && !this._onUpdate && a !== -1e-4 && this._startAt.render(a, b, c), d && (this._timeline.autoRemoveChildren && this._enabled(!1, !1), this._active = !1), !b && this.vars[e] && this._callback(e), 0 === i && this._rawPrevTime === m && g !== m && (this._rawPrevTime = 0))
				}
			}, h._kill = function (a, b, c) {
				if ("all" === a && (a = null), null == a && (null == b || b === this.target)) return this._lazy = !1, this._enabled(!1, !1);
				b = "string" != typeof b ? b || this._targets || this.target : G.selector(b) || b;
				var d, e, f, g, h, i, j, k, l, m = c && this._time && c._startTime === this._startTime && this._timeline === c._timeline;
				if ((p(b) || H(b)) && "number" != typeof b[0])
					for (d = b.length; --d > -1;) this._kill(a, b[d], c) && (i = !0);
				else {
					if (this._targets) {
						for (d = this._targets.length; --d > -1;)
							if (b === this._targets[d]) {
								h = this._propLookup[d] || {}, this._overwrittenProps = this._overwrittenProps || [], e = this._overwrittenProps[d] = a ? this._overwrittenProps[d] || {} : "all";
								break
							}
					} else {
						if (b !== this.target) return !1;
						h = this._propLookup, e = this._overwrittenProps = a ? this._overwrittenProps || {} : "all"
					}
					if (h) {
						if (j = a || h, k = a !== e && "all" !== e && a !== h && ("object" != typeof a || !a._tempKill), c && (G.onOverwrite || this.vars.onOverwrite)) {
							for (f in j) h[f] && (l || (l = []), l.push(f));
							if ((l || !a) && !$(this, c, b, l)) return !1
						}
						for (f in j)(g = h[f]) && (m && (g.f ? g.t[g.p](g.s) : g.t[g.p] = g.s, i = !0), g.pg && g.t._kill(j) && (i = !0), g.pg && 0 !== g.t._overwriteProps.length || (g._prev ? g._prev._next = g._next : g === this._firstPT && (this._firstPT = g._next), g._next && (g._next._prev = g._prev), g._next = g._prev = null), delete h[f]), k && (e[f] = 1);
						!this._firstPT && this._initted && this._enabled(!1, !1)
					}
				}
				return i
			}, h.invalidate = function () {
				return this._notifyPluginsOfEnabled && G._onPluginEvent("_onDisable", this), this._firstPT = this._overwrittenProps = this._startAt = this._onUpdate = null, this._notifyPluginsOfEnabled = this._active = this._lazy = !1, this._propLookup = this._targets ? {} : [], D.prototype.invalidate.call(this), this.vars.immediateRender && (this._time = -m, this.render(Math.min(0, -this._delay))), this
			}, h._enabled = function (a, b) {
				if (j || i.wake(), a && this._gc) {
					var c, d = this._targets;
					if (d)
						for (c = d.length; --c > -1;) this._siblings[c] = Z(d[c], this, !0);
					else this._siblings = Z(this.target, this, !0)
				}
				return D.prototype._enabled.call(this, a, b), this._notifyPluginsOfEnabled && this._firstPT ? G._onPluginEvent(a ? "_onEnable" : "_onDisable", this) : !1
			}, G.to = function (a, b, c) {
				return new G(a, b, c)
			}, G.from = function (a, b, c) {
				return c.runBackwards = !0, c.immediateRender = 0 != c.immediateRender, new G(a, b, c)
			}, G.fromTo = function (a, b, c, d) {
				return d.startAt = c, d.immediateRender = 0 != d.immediateRender && 0 != c.immediateRender, new G(a, b, d)
			}, G.delayedCall = function (a, b, c, d, e) {
				return new G(b, 0, {
					delay: a,
					onComplete: b,
					onCompleteParams: c,
					callbackScope: d,
					onReverseComplete: b,
					onReverseCompleteParams: c,
					immediateRender: !1,
					lazy: !1,
					useFrames: e,
					overwrite: 0
				})
			}, G.set = function (a, b) {
				return new G(a, 0, b)
			}, G.getTweensOf = function (a, b) {
				if (null == a) return [];
				a = "string" != typeof a ? a : G.selector(a) || a;
				var c, d, e, f;
				if ((p(a) || H(a)) && "number" != typeof a[0]) {
					for (c = a.length, d = []; --c > -1;) d = d.concat(G.getTweensOf(a[c], b));
					for (c = d.length; --c > -1;)
						for (f = d[c], e = c; --e > -1;) f === d[e] && d.splice(c, 1)
				} else
					for (d = Z(a).concat(), c = d.length; --c > -1;)(d[c]._gc || b && !d[c].isActive()) && d.splice(c, 1);
				return d
			}, G.killTweensOf = G.killDelayedCallsTo = function (a, b, c) {
				"object" == typeof b && (c = b, b = !1);
				for (var d = G.getTweensOf(a, b), e = d.length; --e > -1;) d[e]._kill(c, a)
			};
			var ba = t("plugins.TweenPlugin", function (a, b) {
				this._overwriteProps = (a || "").split(","), this._propName = this._overwriteProps[0], this._priority = b || 0, this._super = ba.prototype
			}, !0);
			if (h = ba.prototype, ba.version = "1.19.0", ba.API = 2, h._firstPT = null, h._addTween = O, h.setRatio = M, h._kill = function (a) {
					var b, c = this._overwriteProps,
						d = this._firstPT;
					if (null != a[this._propName]) this._overwriteProps = [];
					else
						for (b = c.length; --b > -1;) null != a[c[b]] && c.splice(b, 1);
					for (; d;) null != a[d.n] && (d._next && (d._next._prev = d._prev), d._prev ? (d._prev._next = d._next, d._prev = null) : this._firstPT === d && (this._firstPT = d._next)), d = d._next;
					return !1
				}, h._mod = h._roundProps = function (a) {
					for (var b, c = this._firstPT; c;) b = a[this._propName] || null != c.n && a[c.n.split(this._propName + "_").join("")], b && "function" == typeof b && (2 === c.f ? c.t._applyPT.m = b : c.m = b), c = c._next
				}, G._onPluginEvent = function (a, b) {
					var c, d, e, f, g, h = b._firstPT;
					if ("_onInitAllProps" === a) {
						for (; h;) {
							for (g = h._next, d = e; d && d.pr > h.pr;) d = d._next;
							(h._prev = d ? d._prev : f) ? h._prev._next = h: e = h, (h._next = d) ? d._prev = h : f = h, h = g
						}
						h = b._firstPT = e
					}
					for (; h;) h.pg && "function" == typeof h.t[a] && h.t[a]() && (c = !0), h = h._next;
					return c
				}, ba.activate = function (a) {
					for (var b = a.length; --b > -1;) a[b].API === ba.API && (Q[(new a[b])._propName] = a[b]);
					return !0
				}, s.plugin = function (a) {
					if (!(a && a.propName && a.init && a.API)) throw "illegal plugin definition.";
					var b, c = a.propName,
						d = a.priority || 0,
						e = a.overwriteProps,
						f = {
							init: "_onInitTween",
							set: "setRatio",
							kill: "_kill",
							round: "_mod",
							mod: "_mod",
							initAll: "_onInitAllProps"
						},
						g = t("plugins." + c.charAt(0).toUpperCase() + c.substr(1) + "Plugin", function () {
							ba.call(this, c, d), this._overwriteProps = e || []
						}, a.global === !0),
						h = g.prototype = new ba(c);
					h.constructor = g, g.API = a.API;
					for (b in f) "function" == typeof a[b] && (h[f[b]] = a[b]);
					return g.version = a.version, ba.activate([g]), g
				}, f = a._gsQueue) {
				for (g = 0; g < f.length; g++) f[g]();
				for (h in q) q[h].func || a.console.log("GSAP encountered missing dependency: " + h)
			}
			j = !1
		}
	}("undefined" != typeof module && module.exports && "undefined" != typeof global ? global : this || window, "TweenLite");
/* TIME LINE LITE */
/*!
 * VERSION: 1.17.0
 * DATE: 2015-05-27
 * UPDATES AND DOCS AT: http://greensock.com
 *
 * @license Copyright (c) 2008-2015, GreenSock. All rights reserved.
 * This work is subject to the terms at http://greensock.com/standard-license or for
 * Club GreenSock members, the software agreement that was issued with your membership.
 * 
 * @author: Jack Doyle, jack@greensock.com
 */
var _gsScope = "undefined" != typeof module && module.exports && "undefined" != typeof global ? global : this || window;
(_gsScope._gsQueue || (_gsScope._gsQueue = [])).push(function () {
		"use strict";
		_gsScope._gsDefine("TimelineLite", ["core.Animation", "core.SimpleTimeline", "TweenLite"], function (t, e, i) {
			var s = function (t) {
					e.call(this, t), this._labels = {}, this.autoRemoveChildren = this.vars.autoRemoveChildren === !0, this.smoothChildTiming = this.vars.smoothChildTiming === !0, this._sortChildren = !0, this._onUpdate = this.vars.onUpdate;
					var i, s, r = this.vars;
					for (s in r) i = r[s], h(i) && -1 !== i.join("").indexOf("{self}") && (r[s] = this._swapSelfInParams(i));
					h(r.tweens) && this.add(r.tweens, 0, r.align, r.stagger)
				},
				r = 1e-10,
				n = i._internals,
				a = s._internals = {},
				o = n.isSelector,
				h = n.isArray,
				l = n.lazyTweens,
				_ = n.lazyRender,
				u = [],
				f = _gsScope._gsDefine.globals,
				c = function (t) {
					var e, i = {};
					for (e in t) i[e] = t[e];
					return i
				},
				p = a.pauseCallback = function (t, e, i, s) {
					var n, a = t._timeline,
						o = a._totalTime,
						h = t._startTime,
						l = 0 > t._rawPrevTime || 0 === t._rawPrevTime && a._reversed,
						_ = l ? 0 : r,
						f = l ? r : 0;
					if (e || !this._forcingPlayhead) {
						for (a.pause(h), n = t._prev; n && n._startTime === h;) n._rawPrevTime = f, n = n._prev;
						for (n = t._next; n && n._startTime === h;) n._rawPrevTime = _, n = n._next;
						e && e.apply(s || a.vars.callbackScope || a, i || u), (this._forcingPlayhead || !a._paused) && a.seek(o)
					}
				},
				m = function (t) {
					var e, i = [],
						s = t.length;
					for (e = 0; e !== s; i.push(t[e++]));
					return i
				},
				d = s.prototype = new e;
			return s.version = "1.17.0", d.constructor = s, d.kill()._gc = d._forcingPlayhead = !1, d.to = function (t, e, s, r) {
				var n = s.repeat && f.TweenMax || i;
				return e ? this.add(new n(t, e, s), r) : this.set(t, s, r)
			}, d.from = function (t, e, s, r) {
				return this.add((s.repeat && f.TweenMax || i).from(t, e, s), r)
			}, d.fromTo = function (t, e, s, r, n) {
				var a = r.repeat && f.TweenMax || i;
				return e ? this.add(a.fromTo(t, e, s, r), n) : this.set(t, r, n)
			}, d.staggerTo = function (t, e, r, n, a, h, l, _) {
				var u, f = new s({
					onComplete: h,
					onCompleteParams: l,
					callbackScope: _,
					smoothChildTiming: this.smoothChildTiming
				});
				for ("string" == typeof t && (t = i.selector(t) || t), t = t || [], o(t) && (t = m(t)), n = n || 0, 0 > n && (t = m(t), t.reverse(), n *= -1), u = 0; t.length > u; u++) r.startAt && (r.startAt = c(r.startAt)), f.to(t[u], e, c(r), u * n);
				return this.add(f, a)
			}, d.staggerFrom = function (t, e, i, s, r, n, a, o) {
				return i.immediateRender = 0 != i.immediateRender, i.runBackwards = !0, this.staggerTo(t, e, i, s, r, n, a, o)
			}, d.staggerFromTo = function (t, e, i, s, r, n, a, o, h) {
				return s.startAt = i, s.immediateRender = 0 != s.immediateRender && 0 != i.immediateRender, this.staggerTo(t, e, s, r, n, a, o, h)
			}, d.call = function (t, e, s, r) {
				return this.add(i.delayedCall(0, t, e, s), r)
			}, d.set = function (t, e, s) {
				return s = this._parseTimeOrLabel(s, 0, !0), null == e.immediateRender && (e.immediateRender = s === this._time && !this._paused), this.add(new i(t, 0, e), s)
			}, s.exportRoot = function (t, e) {
				t = t || {}, null == t.smoothChildTiming && (t.smoothChildTiming = !0);
				var r, n, a = new s(t),
					o = a._timeline;
				for (null == e && (e = !0), o._remove(a, !0), a._startTime = 0, a._rawPrevTime = a._time = a._totalTime = o._time, r = o._first; r;) n = r._next, e && r instanceof i && r.target === r.vars.onComplete || a.add(r, r._startTime - r._delay), r = n;
				return o.add(a, 0), a
			}, d.add = function (r, n, a, o) {
				var l, _, u, f, c, p;
				if ("number" != typeof n && (n = this._parseTimeOrLabel(n, 0, !0, r)), !(r instanceof t)) {
					if (r instanceof Array || r && r.push && h(r)) {
						for (a = a || "normal", o = o || 0, l = n, _ = r.length, u = 0; _ > u; u++) h(f = r[u]) && (f = new s({
							tweens: f
						})), this.add(f, l), "string" != typeof f && "function" != typeof f && ("sequence" === a ? l = f._startTime + f.totalDuration() / f._timeScale : "start" === a && (f._startTime -= f.delay())), l += o;
						return this._uncache(!0)
					}
					if ("string" == typeof r) return this.addLabel(r, n);
					if ("function" != typeof r) throw "Cannot add " + r + " into the timeline; it is not a tween, timeline, function, or string.";
					r = i.delayedCall(0, r)
				}
				if (e.prototype.add.call(this, r, n), (this._gc || this._time === this._duration) && !this._paused && this._duration < this.duration())
					for (c = this, p = c.rawTime() > r._startTime; c._timeline;) p && c._timeline.smoothChildTiming ? c.totalTime(c._totalTime, !0) : c._gc && c._enabled(!0, !1), c = c._timeline;
				return this
			}, d.remove = function (e) {
				if (e instanceof t) return this._remove(e, !1);
				if (e instanceof Array || e && e.push && h(e)) {
					for (var i = e.length; --i > -1;) this.remove(e[i]);
					return this
				}
				return "string" == typeof e ? this.removeLabel(e) : this.kill(null, e)
			}, d._remove = function (t, i) {
				e.prototype._remove.call(this, t, i);
				var s = this._last;
				return s ? this._time > s._startTime + s._totalDuration / s._timeScale && (this._time = this.duration(), this._totalTime = this._totalDuration) : this._time = this._totalTime = this._duration = this._totalDuration = 0, this
			}, d.append = function (t, e) {
				return this.add(t, this._parseTimeOrLabel(null, e, !0, t))
			}, d.insert = d.insertMultiple = function (t, e, i, s) {
				return this.add(t, e || 0, i, s)
			}, d.appendMultiple = function (t, e, i, s) {
				return this.add(t, this._parseTimeOrLabel(null, e, !0, t), i, s)
			}, d.addLabel = function (t, e) {
				return this._labels[t] = this._parseTimeOrLabel(e), this
			}, d.addPause = function (t, e, s, r) {
				var n = i.delayedCall(0, p, ["{self}", e, s, r], this);
				return n.data = "isPause", this.add(n, t)
			}, d.removeLabel = function (t) {
				return delete this._labels[t], this
			}, d.getLabelTime = function (t) {
				return null != this._labels[t] ? this._labels[t] : -1
			}, d._parseTimeOrLabel = function (e, i, s, r) {
				var n;
				if (r instanceof t && r.timeline === this) this.remove(r);
				else if (r && (r instanceof Array || r.push && h(r)))
					for (n = r.length; --n > -1;) r[n] instanceof t && r[n].timeline === this && this.remove(r[n]);
				if ("string" == typeof i) return this._parseTimeOrLabel(i, s && "number" == typeof e && null == this._labels[i] ? e - this.duration() : 0, s);
				if (i = i || 0, "string" != typeof e || !isNaN(e) && null == this._labels[e]) null == e && (e = this.duration());
				else {
					if (n = e.indexOf("="), -1 === n) return null == this._labels[e] ? s ? this._labels[e] = this.duration() + i : i : this._labels[e] + i;
					i = parseInt(e.charAt(n - 1) + "1", 10) * Number(e.substr(n + 1)), e = n > 1 ? this._parseTimeOrLabel(e.substr(0, n - 1), 0, s) : this.duration()
				}
				return Number(e) + i
			}, d.seek = function (t, e) {
				return this.totalTime("number" == typeof t ? t : this._parseTimeOrLabel(t), e !== !1)
			}, d.stop = function () {
				return this.paused(!0)
			}, d.gotoAndPlay = function (t, e) {
				return this.play(t, e)
			}, d.gotoAndStop = function (t, e) {
				return this.pause(t, e)
			}, d.render = function (t, e, i) {
				this._gc && this._enabled(!0, !1);
				var s, n, a, o, h, u = this._dirty ? this.totalDuration() : this._totalDuration,
					f = this._time,
					c = this._startTime,
					p = this._timeScale,
					m = this._paused;
				if (t >= u) this._totalTime = this._time = u, this._reversed || this._hasPausedChild() || (n = !0, o = "onComplete", h = !!this._timeline.autoRemoveChildren, 0 === this._duration && (0 === t || 0 > this._rawPrevTime || this._rawPrevTime === r) && this._rawPrevTime !== t && this._first && (h = !0, this._rawPrevTime > r && (o = "onReverseComplete"))), this._rawPrevTime = this._duration || !e || t || this._rawPrevTime === t ? t : r, t = u + 1e-4;
				else if (1e-7 > t)
					if (this._totalTime = this._time = 0, (0 !== f || 0 === this._duration && this._rawPrevTime !== r && (this._rawPrevTime > 0 || 0 > t && this._rawPrevTime >= 0)) && (o = "onReverseComplete", n = this._reversed), 0 > t) this._active = !1, this._timeline.autoRemoveChildren && this._reversed ? (h = n = !0, o = "onReverseComplete") : this._rawPrevTime >= 0 && this._first && (h = !0), this._rawPrevTime = t;
					else {
						if (this._rawPrevTime = this._duration || !e || t || this._rawPrevTime === t ? t : r, 0 === t && n)
							for (s = this._first; s && 0 === s._startTime;) s._duration || (n = !1), s = s._next;
						t = 0, this._initted || (h = !0)
					}
				else this._totalTime = this._time = this._rawPrevTime = t;
				if (this._time !== f && this._first || i || h) {
					if (this._initted || (this._initted = !0), this._active || !this._paused && this._time !== f && t > 0 && (this._active = !0), 0 === f && this.vars.onStart && 0 !== this._time && (e || this._callback("onStart")), this._time >= f)
						for (s = this._first; s && (a = s._next, !this._paused || m);)(s._active || s._startTime <= this._time && !s._paused && !s._gc) && (s._reversed ? s.render((s._dirty ? s.totalDuration() : s._totalDuration) - (t - s._startTime) * s._timeScale, e, i) : s.render((t - s._startTime) * s._timeScale, e, i)), s = a;
					else
						for (s = this._last; s && (a = s._prev, !this._paused || m);)(s._active || f >= s._startTime && !s._paused && !s._gc) && (s._reversed ? s.render((s._dirty ? s.totalDuration() : s._totalDuration) - (t - s._startTime) * s._timeScale, e, i) : s.render((t - s._startTime) * s._timeScale, e, i)), s = a;
					this._onUpdate && (e || (l.length && _(), this._callback("onUpdate"))), o && (this._gc || (c === this._startTime || p !== this._timeScale) && (0 === this._time || u >= this.totalDuration()) && (n && (l.length && _(), this._timeline.autoRemoveChildren && this._enabled(!1, !1), this._active = !1), !e && this.vars[o] && this._callback(o)))
				}
			}, d._hasPausedChild = function () {
				for (var t = this._first; t;) {
					if (t._paused || t instanceof s && t._hasPausedChild()) return !0;
					t = t._next
				}
				return !1
			}, d.getChildren = function (t, e, s, r) {
				r = r || -9999999999;
				for (var n = [], a = this._first, o = 0; a;) r > a._startTime || (a instanceof i ? e !== !1 && (n[o++] = a) : (s !== !1 && (n[o++] = a), t !== !1 && (n = n.concat(a.getChildren(!0, e, s)), o = n.length))), a = a._next;
				return n
			}, d.getTweensOf = function (t, e) {
				var s, r, n = this._gc,
					a = [],
					o = 0;
				for (n && this._enabled(!0, !0), s = i.getTweensOf(t), r = s.length; --r > -1;)(s[r].timeline === this || e && this._contains(s[r])) && (a[o++] = s[r]);
				return n && this._enabled(!1, !0), a
			}, d.recent = function () {
				return this._recent
			}, d._contains = function (t) {
				for (var e = t.timeline; e;) {
					if (e === this) return !0;
					e = e.timeline
				}
				return !1
			}, d.shiftChildren = function (t, e, i) {
				i = i || 0;
				for (var s, r = this._first, n = this._labels; r;) r._startTime >= i && (r._startTime += t), r = r._next;
				if (e)
					for (s in n) n[s] >= i && (n[s] += t);
				return this._uncache(!0)
			}, d._kill = function (t, e) {
				if (!t && !e) return this._enabled(!1, !1);
				for (var i = e ? this.getTweensOf(e) : this.getChildren(!0, !0, !1), s = i.length, r = !1; --s > -1;) i[s]._kill(t, e) && (r = !0);
				return r
			}, d.clear = function (t) {
				var e = this.getChildren(!1, !0, !0),
					i = e.length;
				for (this._time = this._totalTime = 0; --i > -1;) e[i]._enabled(!1, !1);
				return t !== !1 && (this._labels = {}), this._uncache(!0)
			}, d.invalidate = function () {
				for (var e = this._first; e;) e.invalidate(), e = e._next;
				return t.prototype.invalidate.call(this)
			}, d._enabled = function (t, i) {
				if (t === this._gc)
					for (var s = this._first; s;) s._enabled(t, !0), s = s._next;
				return e.prototype._enabled.call(this, t, i)
			}, d.totalTime = function () {
				this._forcingPlayhead = !0;
				var e = t.prototype.totalTime.apply(this, arguments);
				return this._forcingPlayhead = !1, e
			}, d.duration = function (t) {
				return arguments.length ? (0 !== this.duration() && 0 !== t && this.timeScale(this._duration / t), this) : (this._dirty && this.totalDuration(), this._duration)
			}, d.totalDuration = function (t) {
				if (!arguments.length) {
					if (this._dirty) {
						for (var e, i, s = 0, r = this._last, n = 999999999999; r;) e = r._prev, r._dirty && r.totalDuration(), r._startTime > n && this._sortChildren && !r._paused ? this.add(r, r._startTime - r._delay) : n = r._startTime, 0 > r._startTime && !r._paused && (s -= r._startTime, this._timeline.smoothChildTiming && (this._startTime += r._startTime / this._timeScale), this.shiftChildren(-r._startTime, !1, -9999999999), n = 0), i = r._startTime + r._totalDuration / r._timeScale, i > s && (s = i), r = e;
						this._duration = this._totalDuration = s, this._dirty = !1
					}
					return this._totalDuration
				}
				return 0 !== this.totalDuration() && 0 !== t && this.timeScale(this._totalDuration / t), this
			}, d.paused = function (e) {
				if (!e)
					for (var i = this._first, s = this._time; i;) i._startTime === s && "isPause" === i.data && (i._rawPrevTime = 0), i = i._next;
				return t.prototype.paused.apply(this, arguments)
			}, d.usesFrames = function () {
				for (var e = this._timeline; e._timeline;) e = e._timeline;
				return e === t._rootFramesTimeline
			}, d.rawTime = function () {
				return this._paused ? this._totalTime : (this._timeline.rawTime() - this._startTime) * this._timeScale
			}, s
		}, !0)
	}), _gsScope._gsDefine && _gsScope._gsQueue.pop()(),
	function (t) {
		"use strict";
		var e = function () {
			return (_gsScope.GreenSockGlobals || _gsScope)[t]
		};
		"function" == typeof define && define.amd ? define(["TweenLite"], e) : "undefined" != typeof module && module.exports && (require("./TweenLite.js"), module.exports = e())
	}("TimelineLite");


/* EASING PLUGIN*/
/*!
 * VERSION: 1.15.5
 * DATE: 2016-07-08
 * UPDATES AND DOCS AT: http://greensock.com
 *
 * @license Copyright (c) 2008-2016, GreenSock. All rights reserved.
 * This work is subject to the terms at http://greensock.com/standard-license or for
 * Club GreenSock members, the software agreement that was issued with your membership.
 * 
 * @author: Jack Doyle, jack@greensock.com
 **/
var _gsScope = "undefined" != typeof module && module.exports && "undefined" != typeof global ? global : this || window;
(_gsScope._gsQueue || (_gsScope._gsQueue = [])).push(function () {
		"use strict";
		_gsScope._gsDefine("easing.Back", ["easing.Ease"], function (a) {
			var b, c, d, e = _gsScope.GreenSockGlobals || _gsScope,
				f = e.com.greensock,
				g = 2 * Math.PI,
				h = Math.PI / 2,
				i = f._class,
				j = function (b, c) {
					var d = i("easing." + b, function () {}, !0),
						e = d.prototype = new a;
					return e.constructor = d, e.getRatio = c, d
				},
				k = a.register || function () {},
				l = function (a, b, c, d, e) {
					var f = i("easing." + a, {
						easeOut: new b,
						easeIn: new c,
						easeInOut: new d
					}, !0);
					return k(f, a), f
				},
				m = function (a, b, c) {
					this.t = a, this.v = b, c && (this.next = c, c.prev = this, this.c = c.v - b, this.gap = c.t - a)
				},
				n = function (b, c) {
					var d = i("easing." + b, function (a) {
							this._p1 = a || 0 === a ? a : 1.70158, this._p2 = 1.525 * this._p1
						}, !0),
						e = d.prototype = new a;
					return e.constructor = d, e.getRatio = c, e.config = function (a) {
						return new d(a)
					}, d
				},
				o = l("Back", n("BackOut", function (a) {
					return (a -= 1) * a * ((this._p1 + 1) * a + this._p1) + 1
				}), n("BackIn", function (a) {
					return a * a * ((this._p1 + 1) * a - this._p1)
				}), n("BackInOut", function (a) {
					return (a *= 2) < 1 ? .5 * a * a * ((this._p2 + 1) * a - this._p2) : .5 * ((a -= 2) * a * ((this._p2 + 1) * a + this._p2) + 2)
				})),
				p = i("easing.SlowMo", function (a, b, c) {
					b = b || 0 === b ? b : .7, null == a ? a = .7 : a > 1 && (a = 1), this._p = 1 !== a ? b : 0, this._p1 = (1 - a) / 2, this._p2 = a, this._p3 = this._p1 + this._p2, this._calcEnd = c === !0
				}, !0),
				q = p.prototype = new a;
			return q.constructor = p, q.getRatio = function (a) {
				var b = a + (.5 - a) * this._p;
				return a < this._p1 ? this._calcEnd ? 1 - (a = 1 - a / this._p1) * a : b - (a = 1 - a / this._p1) * a * a * a * b : a > this._p3 ? this._calcEnd ? 1 - (a = (a - this._p3) / this._p1) * a : b + (a - b) * (a = (a - this._p3) / this._p1) * a * a * a : this._calcEnd ? 1 : b
			}, p.ease = new p(.7, .7), q.config = p.config = function (a, b, c) {
				return new p(a, b, c)
			}, b = i("easing.SteppedEase", function (a) {
				a = a || 1, this._p1 = 1 / a, this._p2 = a + 1
			}, !0), q = b.prototype = new a, q.constructor = b, q.getRatio = function (a) {
				return 0 > a ? a = 0 : a >= 1 && (a = .999999999), (this._p2 * a >> 0) * this._p1
			}, q.config = b.config = function (a) {
				return new b(a)
			}, c = i("easing.RoughEase", function (b) {
				b = b || {};
				for (var c, d, e, f, g, h, i = b.taper || "none", j = [], k = 0, l = 0 | (b.points || 20), n = l, o = b.randomize !== !1, p = b.clamp === !0, q = b.template instanceof a ? b.template : null, r = "number" == typeof b.strength ? .4 * b.strength : .4; --n > -1;) c = o ? Math.random() : 1 / l * n, d = q ? q.getRatio(c) : c, "none" === i ? e = r : "out" === i ? (f = 1 - c, e = f * f * r) : "in" === i ? e = c * c * r : .5 > c ? (f = 2 * c, e = f * f * .5 * r) : (f = 2 * (1 - c), e = f * f * .5 * r), o ? d += Math.random() * e - .5 * e : n % 2 ? d += .5 * e : d -= .5 * e, p && (d > 1 ? d = 1 : 0 > d && (d = 0)), j[k++] = {
					x: c,
					y: d
				};
				for (j.sort(function (a, b) {
						return a.x - b.x
					}), h = new m(1, 1, null), n = l; --n > -1;) g = j[n], h = new m(g.x, g.y, h);
				this._prev = new m(0, 0, 0 !== h.t ? h : h.next)
			}, !0), q = c.prototype = new a, q.constructor = c, q.getRatio = function (a) {
				var b = this._prev;
				if (a > b.t) {
					for (; b.next && a >= b.t;) b = b.next;
					b = b.prev
				} else
					for (; b.prev && a <= b.t;) b = b.prev;
				return this._prev = b, b.v + (a - b.t) / b.gap * b.c
			}, q.config = function (a) {
				return new c(a)
			}, c.ease = new c, l("Bounce", j("BounceOut", function (a) {
				return 1 / 2.75 > a ? 7.5625 * a * a : 2 / 2.75 > a ? 7.5625 * (a -= 1.5 / 2.75) * a + .75 : 2.5 / 2.75 > a ? 7.5625 * (a -= 2.25 / 2.75) * a + .9375 : 7.5625 * (a -= 2.625 / 2.75) * a + .984375
			}), j("BounceIn", function (a) {
				return (a = 1 - a) < 1 / 2.75 ? 1 - 7.5625 * a * a : 2 / 2.75 > a ? 1 - (7.5625 * (a -= 1.5 / 2.75) * a + .75) : 2.5 / 2.75 > a ? 1 - (7.5625 * (a -= 2.25 / 2.75) * a + .9375) : 1 - (7.5625 * (a -= 2.625 / 2.75) * a + .984375)
			}), j("BounceInOut", function (a) {
				var b = .5 > a;
				return a = b ? 1 - 2 * a : 2 * a - 1, a = 1 / 2.75 > a ? 7.5625 * a * a : 2 / 2.75 > a ? 7.5625 * (a -= 1.5 / 2.75) * a + .75 : 2.5 / 2.75 > a ? 7.5625 * (a -= 2.25 / 2.75) * a + .9375 : 7.5625 * (a -= 2.625 / 2.75) * a + .984375, b ? .5 * (1 - a) : .5 * a + .5
			})), l("Circ", j("CircOut", function (a) {
				return Math.sqrt(1 - (a -= 1) * a)
			}), j("CircIn", function (a) {
				return -(Math.sqrt(1 - a * a) - 1)
			}), j("CircInOut", function (a) {
				return (a *= 2) < 1 ? -.5 * (Math.sqrt(1 - a * a) - 1) : .5 * (Math.sqrt(1 - (a -= 2) * a) + 1)
			})), d = function (b, c, d) {
				var e = i("easing." + b, function (a, b) {
						this._p1 = a >= 1 ? a : 1, this._p2 = (b || d) / (1 > a ? a : 1), this._p3 = this._p2 / g * (Math.asin(1 / this._p1) || 0), this._p2 = g / this._p2
					}, !0),
					f = e.prototype = new a;
				return f.constructor = e, f.getRatio = c, f.config = function (a, b) {
					return new e(a, b)
				}, e
			}, l("Elastic", d("ElasticOut", function (a) {
				return this._p1 * Math.pow(2, -10 * a) * Math.sin((a - this._p3) * this._p2) + 1
			}, .3), d("ElasticIn", function (a) {
				return -(this._p1 * Math.pow(2, 10 * (a -= 1)) * Math.sin((a - this._p3) * this._p2))
			}, .3), d("ElasticInOut", function (a) {
				return (a *= 2) < 1 ? -.5 * (this._p1 * Math.pow(2, 10 * (a -= 1)) * Math.sin((a - this._p3) * this._p2)) : this._p1 * Math.pow(2, -10 * (a -= 1)) * Math.sin((a - this._p3) * this._p2) * .5 + 1
			}, .45)), l("Expo", j("ExpoOut", function (a) {
				return 1 - Math.pow(2, -10 * a)
			}), j("ExpoIn", function (a) {
				return Math.pow(2, 10 * (a - 1)) - .001
			}), j("ExpoInOut", function (a) {
				return (a *= 2) < 1 ? .5 * Math.pow(2, 10 * (a - 1)) : .5 * (2 - Math.pow(2, -10 * (a - 1)))
			})), l("Sine", j("SineOut", function (a) {
				return Math.sin(a * h)
			}), j("SineIn", function (a) {
				return -Math.cos(a * h) + 1
			}), j("SineInOut", function (a) {
				return -.5 * (Math.cos(Math.PI * a) - 1)
			})), i("easing.EaseLookup", {
				find: function (b) {
					return a.map[b]
				}
			}, !0), k(e.SlowMo, "SlowMo", "ease,"), k(c, "RoughEase", "ease,"), k(b, "SteppedEase", "ease,"), o
		}, !0)
	}), _gsScope._gsDefine && _gsScope._gsQueue.pop()(),
	function () {
		"use strict";
		var a = function () {
			return _gsScope.GreenSockGlobals || _gsScope
		};
		"function" == typeof define && define.amd ? define(["TweenLite"], a) : "undefined" != typeof module && module.exports && (require("../TweenLite.js"), module.exports = a())
	}();


/* CSS PLUGIN */
/*!
 * VERSION: 1.19.1
 * DATE: 2017-01-17
 * UPDATES AND DOCS AT: http://greensock.com
 *
 * @license Copyright (c) 2008-2017, GreenSock. All rights reserved.
 * This work is subject to the terms at http://greensock.com/standard-license or for
 * Club GreenSock members, the software agreement that was issued with your membership.
 * 
 * @author: Jack Doyle, jack@greensock.com
 */
var _gsScope = "undefined" != typeof module && module.exports && "undefined" != typeof global ? global : this || window;
(_gsScope._gsQueue || (_gsScope._gsQueue = [])).push(function () {
		"use strict";
		_gsScope._gsDefine("plugins.CSSPlugin", ["plugins.TweenPlugin", "TweenLite"], function (a, b) {
			var c, d, e, f, g = function () {
					a.call(this, "css"), this._overwriteProps.length = 0, this.setRatio = g.prototype.setRatio
				},
				h = _gsScope._gsDefine.globals,
				i = {},
				j = g.prototype = new a("css");
			j.constructor = g, g.version = "1.19.1", g.API = 2, g.defaultTransformPerspective = 0, g.defaultSkewType = "compensated", g.defaultSmoothOrigin = !0, j = "px", g.suffixMap = {
				top: j,
				right: j,
				bottom: j,
				left: j,
				width: j,
				height: j,
				fontSize: j,
				padding: j,
				margin: j,
				perspective: j,
				lineHeight: ""
			};
			var k, l, m, n, o, p, q, r, s = /(?:\-|\.|\b)(\d|\.|e\-)+/g,
				t = /(?:\d|\-\d|\.\d|\-\.\d|\+=\d|\-=\d|\+=.\d|\-=\.\d)+/g,
				u = /(?:\+=|\-=|\-|\b)[\d\-\.]+[a-zA-Z0-9]*(?:%|\b)/gi,
				v = /(?![+-]?\d*\.?\d+|[+-]|e[+-]\d+)[^0-9]/g,
				w = /(?:\d|\-|\+|=|#|\.)*/g,
				x = /opacity *= *([^)]*)/i,
				y = /opacity:([^;]*)/i,
				z = /alpha\(opacity *=.+?\)/i,
				A = /^(rgb|hsl)/,
				B = /([A-Z])/g,
				C = /-([a-z])/gi,
				D = /(^(?:url\(\"|url\())|(?:(\"\))$|\)$)/gi,
				E = function (a, b) {
					return b.toUpperCase()
				},
				F = /(?:Left|Right|Width)/i,
				G = /(M11|M12|M21|M22)=[\d\-\.e]+/gi,
				H = /progid\:DXImageTransform\.Microsoft\.Matrix\(.+?\)/i,
				I = /,(?=[^\)]*(?:\(|$))/gi,
				J = /[\s,\(]/i,
				K = Math.PI / 180,
				L = 180 / Math.PI,
				M = {},
				N = {
					style: {}
				},
				O = _gsScope.document || {
					createElement: function () {
						return N
					}
				},
				P = function (a, b) {
					return O.createElementNS ? O.createElementNS(b || "http://www.w3.org/1999/xhtml", a) : O.createElement(a)
				},
				Q = P("div"),
				R = P("img"),
				S = g._internals = {
					_specialProps: i
				},
				T = (_gsScope.navigator || {}).userAgent || "",
				U = function () {
					var a = T.indexOf("Android"),
						b = P("a");
					return m = -1 !== T.indexOf("Safari") && -1 === T.indexOf("Chrome") && (-1 === a || parseFloat(T.substr(a + 8, 2)) > 3), o = m && parseFloat(T.substr(T.indexOf("Version/") + 8, 2)) < 6, n = -1 !== T.indexOf("Firefox"), (/MSIE ([0-9]{1,}[\.0-9]{0,})/.exec(T) || /Trident\/.*rv:([0-9]{1,}[\.0-9]{0,})/.exec(T)) && (p = parseFloat(RegExp.$1)), b ? (b.style.cssText = "top:1px;opacity:.55;", /^0.55/.test(b.style.opacity)) : !1
				}(),
				V = function (a) {
					return x.test("string" == typeof a ? a : (a.currentStyle ? a.currentStyle.filter : a.style.filter) || "") ? parseFloat(RegExp.$1) / 100 : 1
				},
				W = function (a) {
					_gsScope.console && console.log(a)
				},
				X = "",
				Y = "",
				Z = function (a, b) {
					b = b || Q;
					var c, d, e = b.style;
					if (void 0 !== e[a]) return a;
					for (a = a.charAt(0).toUpperCase() + a.substr(1), c = ["O", "Moz", "ms", "Ms", "Webkit"], d = 5; --d > -1 && void 0 === e[c[d] + a];);
					return d >= 0 ? (Y = 3 === d ? "ms" : c[d], X = "-" + Y.toLowerCase() + "-", Y + a) : null
				},
				$ = O.defaultView ? O.defaultView.getComputedStyle : function () {},
				_ = g.getStyle = function (a, b, c, d, e) {
					var f;
					return U || "opacity" !== b ? (!d && a.style[b] ? f = a.style[b] : (c = c || $(a)) ? f = c[b] || c.getPropertyValue(b) || c.getPropertyValue(b.replace(B, "-$1").toLowerCase()) : a.currentStyle && (f = a.currentStyle[b]), null == e || f && "none" !== f && "auto" !== f && "auto auto" !== f ? f : e) : V(a)
				},
				aa = S.convertToPixels = function (a, c, d, e, f) {
					if ("px" === e || !e) return d;
					if ("auto" === e || !d) return 0;
					var h, i, j, k = F.test(c),
						l = a,
						m = Q.style,
						n = 0 > d,
						o = 1 === d;
					if (n && (d = -d), o && (d *= 100), "%" === e && -1 !== c.indexOf("border")) h = d / 100 * (k ? a.clientWidth : a.clientHeight);
					else {
						if (m.cssText = "border:0 solid red;position:" + _(a, "position") + ";line-height:0;", "%" !== e && l.appendChild && "v" !== e.charAt(0) && "rem" !== e) m[k ? "borderLeftWidth" : "borderTopWidth"] = d + e;
						else {
							if (l = a.parentNode || O.body, i = l._gsCache, j = b.ticker.frame, i && k && i.time === j) return i.width * d / 100;
							m[k ? "width" : "height"] = d + e
						}
						l.appendChild(Q), h = parseFloat(Q[k ? "offsetWidth" : "offsetHeight"]), l.removeChild(Q), k && "%" === e && g.cacheWidths !== !1 && (i = l._gsCache = l._gsCache || {}, i.time = j, i.width = h / d * 100), 0 !== h || f || (h = aa(a, c, d, e, !0))
					}
					return o && (h /= 100), n ? -h : h
				},
				ba = S.calculateOffset = function (a, b, c) {
					if ("absolute" !== _(a, "position", c)) return 0;
					var d = "left" === b ? "Left" : "Top",
						e = _(a, "margin" + d, c);
					return a["offset" + d] - (aa(a, b, parseFloat(e), e.replace(w, "")) || 0)
				},
				ca = function (a, b) {
					var c, d, e, f = {};
					if (b = b || $(a, null))
						if (c = b.length)
							for (; --c > -1;) e = b[c], (-1 === e.indexOf("-transform") || Da === e) && (f[e.replace(C, E)] = b.getPropertyValue(e));
						else
							for (c in b)(-1 === c.indexOf("Transform") || Ca === c) && (f[c] = b[c]);
					else if (b = a.currentStyle || a.style)
						for (c in b) "string" == typeof c && void 0 === f[c] && (f[c.replace(C, E)] = b[c]);
					return U || (f.opacity = V(a)), d = Ra(a, b, !1), f.rotation = d.rotation, f.skewX = d.skewX, f.scaleX = d.scaleX, f.scaleY = d.scaleY, f.x = d.x, f.y = d.y, Fa && (f.z = d.z, f.rotationX = d.rotationX, f.rotationY = d.rotationY, f.scaleZ = d.scaleZ), f.filters && delete f.filters, f
				},
				da = function (a, b, c, d, e) {
					var f, g, h, i = {},
						j = a.style;
					for (g in c) "cssText" !== g && "length" !== g && isNaN(g) && (b[g] !== (f = c[g]) || e && e[g]) && -1 === g.indexOf("Origin") && ("number" == typeof f || "string" == typeof f) && (i[g] = "auto" !== f || "left" !== g && "top" !== g ? "" !== f && "auto" !== f && "none" !== f || "string" != typeof b[g] || "" === b[g].replace(v, "") ? f : 0 : ba(a, g), void 0 !== j[g] && (h = new sa(j, g, j[g], h)));
					if (d)
						for (g in d) "className" !== g && (i[g] = d[g]);
					return {
						difs: i,
						firstMPT: h
					}
				},
				ea = {
					width: ["Left", "Right"],
					height: ["Top", "Bottom"]
				},
				fa = ["marginLeft", "marginRight", "marginTop", "marginBottom"],
				ga = function (a, b, c) {
					if ("svg" === (a.nodeName + "").toLowerCase()) return (c || $(a))[b] || 0;
					if (a.getCTM && Oa(a)) return a.getBBox()[b] || 0;
					var d = parseFloat("width" === b ? a.offsetWidth : a.offsetHeight),
						e = ea[b],
						f = e.length;
					for (c = c || $(a, null); --f > -1;) d -= parseFloat(_(a, "padding" + e[f], c, !0)) || 0, d -= parseFloat(_(a, "border" + e[f] + "Width", c, !0)) || 0;
					return d
				},
				ha = function (a, b) {
					if ("contain" === a || "auto" === a || "auto auto" === a) return a + " ";
					(null == a || "" === a) && (a = "0 0");
					var c, d = a.split(" "),
						e = -1 !== a.indexOf("left") ? "0%" : -1 !== a.indexOf("right") ? "100%" : d[0],
						f = -1 !== a.indexOf("top") ? "0%" : -1 !== a.indexOf("bottom") ? "100%" : d[1];
					if (d.length > 3 && !b) {
						for (d = a.split(", ").join(",").split(","), a = [], c = 0; c < d.length; c++) a.push(ha(d[c]));
						return a.join(",")
					}
					return null == f ? f = "center" === e ? "50%" : "0" : "center" === f && (f = "50%"), ("center" === e || isNaN(parseFloat(e)) && -1 === (e + "").indexOf("=")) && (e = "50%"), a = e + " " + f + (d.length > 2 ? " " + d[2] : ""), b && (b.oxp = -1 !== e.indexOf("%"), b.oyp = -1 !== f.indexOf("%"), b.oxr = "=" === e.charAt(1), b.oyr = "=" === f.charAt(1), b.ox = parseFloat(e.replace(v, "")), b.oy = parseFloat(f.replace(v, "")), b.v = a), b || a
				},
				ia = function (a, b) {
					return "function" == typeof a && (a = a(r, q)), "string" == typeof a && "=" === a.charAt(1) ? parseInt(a.charAt(0) + "1", 10) * parseFloat(a.substr(2)) : parseFloat(a) - parseFloat(b) || 0
				},
				ja = function (a, b) {
					return "function" == typeof a && (a = a(r, q)), null == a ? b : "string" == typeof a && "=" === a.charAt(1) ? parseInt(a.charAt(0) + "1", 10) * parseFloat(a.substr(2)) + b : parseFloat(a) || 0
				},
				ka = function (a, b, c, d) {
					var e, f, g, h, i, j = 1e-6;
					return "function" == typeof a && (a = a(r, q)), null == a ? h = b : "number" == typeof a ? h = a : (e = 360, f = a.split("_"), i = "=" === a.charAt(1), g = (i ? parseInt(a.charAt(0) + "1", 10) * parseFloat(f[0].substr(2)) : parseFloat(f[0])) * (-1 === a.indexOf("rad") ? 1 : L) - (i ? 0 : b), f.length && (d && (d[c] = b + g), -1 !== a.indexOf("short") && (g %= e, g !== g % (e / 2) && (g = 0 > g ? g + e : g - e)), -1 !== a.indexOf("_cw") && 0 > g ? g = (g + 9999999999 * e) % e - (g / e | 0) * e : -1 !== a.indexOf("ccw") && g > 0 && (g = (g - 9999999999 * e) % e - (g / e | 0) * e)), h = b + g), j > h && h > -j && (h = 0), h
				},
				la = {
					aqua: [0, 255, 255],
					lime: [0, 255, 0],
					silver: [192, 192, 192],
					black: [0, 0, 0],
					maroon: [128, 0, 0],
					teal: [0, 128, 128],
					blue: [0, 0, 255],
					navy: [0, 0, 128],
					white: [255, 255, 255],
					fuchsia: [255, 0, 255],
					olive: [128, 128, 0],
					yellow: [255, 255, 0],
					orange: [255, 165, 0],
					gray: [128, 128, 128],
					purple: [128, 0, 128],
					green: [0, 128, 0],
					red: [255, 0, 0],
					pink: [255, 192, 203],
					cyan: [0, 255, 255],
					transparent: [255, 255, 255, 0]
				},
				ma = function (a, b, c) {
					return a = 0 > a ? a + 1 : a > 1 ? a - 1 : a, 255 * (1 > 6 * a ? b + (c - b) * a * 6 : .5 > a ? c : 2 > 3 * a ? b + (c - b) * (2 / 3 - a) * 6 : b) + .5 | 0
				},
				na = g.parseColor = function (a, b) {
					var c, d, e, f, g, h, i, j, k, l, m;
					if (a)
						if ("number" == typeof a) c = [a >> 16, a >> 8 & 255, 255 & a];
						else {
							if ("," === a.charAt(a.length - 1) && (a = a.substr(0, a.length - 1)), la[a]) c = la[a];
							else if ("#" === a.charAt(0)) 4 === a.length && (d = a.charAt(1), e = a.charAt(2), f = a.charAt(3), a = "#" + d + d + e + e + f + f), a = parseInt(a.substr(1), 16), c = [a >> 16, a >> 8 & 255, 255 & a];
							else if ("hsl" === a.substr(0, 3))
								if (c = m = a.match(s), b) {
									if (-1 !== a.indexOf("=")) return a.match(t)
								} else g = Number(c[0]) % 360 / 360, h = Number(c[1]) / 100, i = Number(c[2]) / 100, e = .5 >= i ? i * (h + 1) : i + h - i * h, d = 2 * i - e, c.length > 3 && (c[3] = Number(a[3])), c[0] = ma(g + 1 / 3, d, e), c[1] = ma(g, d, e), c[2] = ma(g - 1 / 3, d, e);
							else c = a.match(s) || la.transparent;
							c[0] = Number(c[0]), c[1] = Number(c[1]), c[2] = Number(c[2]), c.length > 3 && (c[3] = Number(c[3]))
						}
					else c = la.black;
					return b && !m && (d = c[0] / 255, e = c[1] / 255, f = c[2] / 255, j = Math.max(d, e, f), k = Math.min(d, e, f), i = (j + k) / 2, j === k ? g = h = 0 : (l = j - k, h = i > .5 ? l / (2 - j - k) : l / (j + k), g = j === d ? (e - f) / l + (f > e ? 6 : 0) : j === e ? (f - d) / l + 2 : (d - e) / l + 4, g *= 60), c[0] = g + .5 | 0, c[1] = 100 * h + .5 | 0, c[2] = 100 * i + .5 | 0), c
				},
				oa = function (a, b) {
					var c, d, e, f = a.match(pa) || [],
						g = 0,
						h = f.length ? "" : a;
					for (c = 0; c < f.length; c++) d = f[c], e = a.substr(g, a.indexOf(d, g) - g), g += e.length + d.length, d = na(d, b), 3 === d.length && d.push(1), h += e + (b ? "hsla(" + d[0] + "," + d[1] + "%," + d[2] + "%," + d[3] : "rgba(" + d.join(",")) + ")";
					return h + a.substr(g)
				},
				pa = "(?:\\b(?:(?:rgb|rgba|hsl|hsla)\\(.+?\\))|\\B#(?:[0-9a-f]{3}){1,2}\\b";
			for (j in la) pa += "|" + j + "\\b";
			pa = new RegExp(pa + ")", "gi"), g.colorStringFilter = function (a) {
				var b, c = a[0] + a[1];
				pa.test(c) && (b = -1 !== c.indexOf("hsl(") || -1 !== c.indexOf("hsla("), a[0] = oa(a[0], b), a[1] = oa(a[1], b)), pa.lastIndex = 0
			}, b.defaultStringFilter || (b.defaultStringFilter = g.colorStringFilter);
			var qa = function (a, b, c, d) {
					if (null == a) return function (a) {
						return a
					};
					var e, f = b ? (a.match(pa) || [""])[0] : "",
						g = a.split(f).join("").match(u) || [],
						h = a.substr(0, a.indexOf(g[0])),
						i = ")" === a.charAt(a.length - 1) ? ")" : "",
						j = -1 !== a.indexOf(" ") ? " " : ",",
						k = g.length,
						l = k > 0 ? g[0].replace(s, "") : "";
					return k ? e = b ? function (a) {
						var b, m, n, o;
						if ("number" == typeof a) a += l;
						else if (d && I.test(a)) {
							for (o = a.replace(I, "|").split("|"), n = 0; n < o.length; n++) o[n] = e(o[n]);
							return o.join(",")
						}
						if (b = (a.match(pa) || [f])[0], m = a.split(b).join("").match(u) || [], n = m.length, k > n--)
							for (; ++n < k;) m[n] = c ? m[(n - 1) / 2 | 0] : g[n];
						return h + m.join(j) + j + b + i + (-1 !== a.indexOf("inset") ? " inset" : "")
					} : function (a) {
						var b, f, m;
						if ("number" == typeof a) a += l;
						else if (d && I.test(a)) {
							for (f = a.replace(I, "|").split("|"), m = 0; m < f.length; m++) f[m] = e(f[m]);
							return f.join(",")
						}
						if (b = a.match(u) || [], m = b.length, k > m--)
							for (; ++m < k;) b[m] = c ? b[(m - 1) / 2 | 0] : g[m];
						return h + b.join(j) + i
					} : function (a) {
						return a
					}
				},
				ra = function (a) {
					return a = a.split(","),
						function (b, c, d, e, f, g, h) {
							var i, j = (c + "").split(" ");
							for (h = {}, i = 0; 4 > i; i++) h[a[i]] = j[i] = j[i] || j[(i - 1) / 2 >> 0];
							return e.parse(b, h, f, g)
						}
				},
				sa = (S._setPluginRatio = function (a) {
					this.plugin.setRatio(a);
					for (var b, c, d, e, f, g = this.data, h = g.proxy, i = g.firstMPT, j = 1e-6; i;) b = h[i.v], i.r ? b = Math.round(b) : j > b && b > -j && (b = 0), i.t[i.p] = b, i = i._next;
					if (g.autoRotate && (g.autoRotate.rotation = g.mod ? g.mod(h.rotation, this.t) : h.rotation), 1 === a || 0 === a)
						for (i = g.firstMPT, f = 1 === a ? "e" : "b"; i;) {
							if (c = i.t, c.type) {
								if (1 === c.type) {
									for (e = c.xs0 + c.s + c.xs1, d = 1; d < c.l; d++) e += c["xn" + d] + c["xs" + (d + 1)];
									c[f] = e
								}
							} else c[f] = c.s + c.xs0;
							i = i._next
						}
				}, function (a, b, c, d, e) {
					this.t = a, this.p = b, this.v = c, this.r = e, d && (d._prev = this, this._next = d)
				}),
				ta = (S._parseToProxy = function (a, b, c, d, e, f) {
					var g, h, i, j, k, l = d,
						m = {},
						n = {},
						o = c._transform,
						p = M;
					for (c._transform = null, M = b, d = k = c.parse(a, b, d, e), M = p, f && (c._transform = o, l && (l._prev = null, l._prev && (l._prev._next = null))); d && d !== l;) {
						if (d.type <= 1 && (h = d.p, n[h] = d.s + d.c, m[h] = d.s, f || (j = new sa(d, "s", h, j, d.r), d.c = 0), 1 === d.type))
							for (g = d.l; --g > 0;) i = "xn" + g, h = d.p + "_" + i, n[h] = d.data[i], m[h] = d[i], f || (j = new sa(d, i, h, j, d.rxp[i]));
						d = d._next
					}
					return {
						proxy: m,
						end: n,
						firstMPT: j,
						pt: k
					}
				}, S.CSSPropTween = function (a, b, d, e, g, h, i, j, k, l, m) {
					this.t = a, this.p = b, this.s = d, this.c = e, this.n = i || b, a instanceof ta || f.push(this.n), this.r = j, this.type = h || 0, k && (this.pr = k, c = !0), this.b = void 0 === l ? d : l, this.e = void 0 === m ? d + e : m, g && (this._next = g, g._prev = this)
				}),
				ua = function (a, b, c, d, e, f) {
					var g = new ta(a, b, c, d - c, e, -1, f);
					return g.b = c, g.e = g.xs0 = d, g
				},
				va = g.parseComplex = function (a, b, c, d, e, f, h, i, j, l) {
					c = c || f || "", "function" == typeof d && (d = d(r, q)), h = new ta(a, b, 0, 0, h, l ? 2 : 1, null, !1, i, c, d), d += "", e && pa.test(d + c) && (d = [c, d], g.colorStringFilter(d), c = d[0], d = d[1]);
					var m, n, o, p, u, v, w, x, y, z, A, B, C, D = c.split(", ").join(",").split(" "),
						E = d.split(", ").join(",").split(" "),
						F = D.length,
						G = k !== !1;
					for ((-1 !== d.indexOf(",") || -1 !== c.indexOf(",")) && (D = D.join(" ").replace(I, ", ").split(" "), E = E.join(" ").replace(I, ", ").split(" "), F = D.length), F !== E.length && (D = (f || "").split(" "), F = D.length), h.plugin = j, h.setRatio = l, pa.lastIndex = 0, m = 0; F > m; m++)
						if (p = D[m], u = E[m], x = parseFloat(p), x || 0 === x) h.appendXtra("", x, ia(u, x), u.replace(t, ""), G && -1 !== u.indexOf("px"), !0);
						else if (e && pa.test(p)) B = u.indexOf(")") + 1, B = ")" + (B ? u.substr(B) : ""), C = -1 !== u.indexOf("hsl") && U, p = na(p, C), u = na(u, C), y = p.length + u.length > 6, y && !U && 0 === u[3] ? (h["xs" + h.l] += h.l ? " transparent" : "transparent", h.e = h.e.split(E[m]).join("transparent")) : (U || (y = !1), C ? h.appendXtra(y ? "hsla(" : "hsl(", p[0], ia(u[0], p[0]), ",", !1, !0).appendXtra("", p[1], ia(u[1], p[1]), "%,", !1).appendXtra("", p[2], ia(u[2], p[2]), y ? "%," : "%" + B, !1) : h.appendXtra(y ? "rgba(" : "rgb(", p[0], u[0] - p[0], ",", !0, !0).appendXtra("", p[1], u[1] - p[1], ",", !0).appendXtra("", p[2], u[2] - p[2], y ? "," : B, !0), y && (p = p.length < 4 ? 1 : p[3], h.appendXtra("", p, (u.length < 4 ? 1 : u[3]) - p, B, !1))), pa.lastIndex = 0;
					else if (v = p.match(s)) {
						if (w = u.match(t), !w || w.length !== v.length) return h;
						for (o = 0, n = 0; n < v.length; n++) A = v[n], z = p.indexOf(A, o), h.appendXtra(p.substr(o, z - o), Number(A), ia(w[n], A), "", G && "px" === p.substr(z + A.length, 2), 0 === n), o = z + A.length;
						h["xs" + h.l] += p.substr(o)
					} else h["xs" + h.l] += h.l || h["xs" + h.l] ? " " + u : u;
					if (-1 !== d.indexOf("=") && h.data) {
						for (B = h.xs0 + h.data.s, m = 1; m < h.l; m++) B += h["xs" + m] + h.data["xn" + m];
						h.e = B + h["xs" + m]
					}
					return h.l || (h.type = -1, h.xs0 = h.e), h.xfirst || h
				},
				wa = 9;
			for (j = ta.prototype, j.l = j.pr = 0; --wa > 0;) j["xn" + wa] = 0, j["xs" + wa] = "";
			j.xs0 = "", j._next = j._prev = j.xfirst = j.data = j.plugin = j.setRatio = j.rxp = null, j.appendXtra = function (a, b, c, d, e, f) {
				var g = this,
					h = g.l;
				return g["xs" + h] += f && (h || g["xs" + h]) ? " " + a : a || "", c || 0 === h || g.plugin ? (g.l++, g.type = g.setRatio ? 2 : 1, g["xs" + g.l] = d || "", h > 0 ? (g.data["xn" + h] = b + c, g.rxp["xn" + h] = e, g["xn" + h] = b, g.plugin || (g.xfirst = new ta(g, "xn" + h, b, c, g.xfirst || g, 0, g.n, e, g.pr), g.xfirst.xs0 = 0), g) : (g.data = {
					s: b + c
				}, g.rxp = {}, g.s = b, g.c = c, g.r = e, g)) : (g["xs" + h] += b + (d || ""), g)
			};
			var xa = function (a, b) {
					b = b || {}, this.p = b.prefix ? Z(a) || a : a, i[a] = i[this.p] = this, this.format = b.formatter || qa(b.defaultValue, b.color, b.collapsible, b.multi), b.parser && (this.parse = b.parser), this.clrs = b.color, this.multi = b.multi, this.keyword = b.keyword, this.dflt = b.defaultValue, this.pr = b.priority || 0
				},
				ya = S._registerComplexSpecialProp = function (a, b, c) {
					"object" != typeof b && (b = {
						parser: c
					});
					var d, e, f = a.split(","),
						g = b.defaultValue;
					for (c = c || [g], d = 0; d < f.length; d++) b.prefix = 0 === d && b.prefix, b.defaultValue = c[d] || g, e = new xa(f[d], b)
				},
				za = S._registerPluginProp = function (a) {
					if (!i[a]) {
						var b = a.charAt(0).toUpperCase() + a.substr(1) + "Plugin";
						ya(a, {
							parser: function (a, c, d, e, f, g, j) {
								var k = h.com.greensock.plugins[b];
								return k ? (k._cssRegister(), i[d].parse(a, c, d, e, f, g, j)) : (W("Error: " + b + " js file not loaded."), f)
							}
						})
					}
				};
			j = xa.prototype, j.parseComplex = function (a, b, c, d, e, f) {
				var g, h, i, j, k, l, m = this.keyword;
				if (this.multi && (I.test(c) || I.test(b) ? (h = b.replace(I, "|").split("|"), i = c.replace(I, "|").split("|")) : m && (h = [b], i = [c])), i) {
					for (j = i.length > h.length ? i.length : h.length, g = 0; j > g; g++) b = h[g] = h[g] || this.dflt, c = i[g] = i[g] || this.dflt, m && (k = b.indexOf(m), l = c.indexOf(m), k !== l && (-1 === l ? h[g] = h[g].split(m).join("") : -1 === k && (h[g] += " " + m)));
					b = h.join(", "), c = i.join(", ")
				}
				return va(a, this.p, b, c, this.clrs, this.dflt, d, this.pr, e, f)
			}, j.parse = function (a, b, c, d, f, g, h) {
				return this.parseComplex(a.style, this.format(_(a, this.p, e, !1, this.dflt)), this.format(b), f, g)
			}, g.registerSpecialProp = function (a, b, c) {
				ya(a, {
					parser: function (a, d, e, f, g, h, i) {
						var j = new ta(a, e, 0, 0, g, 2, e, !1, c);
						return j.plugin = h, j.setRatio = b(a, d, f._tween, e), j
					},
					priority: c
				})
			}, g.useSVGTransformAttr = !0;
			var Aa, Ba = "scaleX,scaleY,scaleZ,x,y,z,skewX,skewY,rotation,rotationX,rotationY,perspective,xPercent,yPercent".split(","),
				Ca = Z("transform"),
				Da = X + "transform",
				Ea = Z("transformOrigin"),
				Fa = null !== Z("perspective"),
				Ga = S.Transform = function () {
					this.perspective = parseFloat(g.defaultTransformPerspective) || 0, this.force3D = g.defaultForce3D !== !1 && Fa ? g.defaultForce3D || "auto" : !1
				},
				Ha = _gsScope.SVGElement,
				Ia = function (a, b, c) {
					var d, e = O.createElementNS("http://www.w3.org/2000/svg", a),
						f = /([a-z])([A-Z])/g;
					for (d in c) e.setAttributeNS(null, d.replace(f, "$1-$2").toLowerCase(), c[d]);
					return b.appendChild(e), e
				},
				Ja = O.documentElement || {},
				Ka = function () {
					var a, b, c, d = p || /Android/i.test(T) && !_gsScope.chrome;
					return O.createElementNS && !d && (a = Ia("svg", Ja), b = Ia("rect", a, {
						width: 100,
						height: 50,
						x: 100
					}), c = b.getBoundingClientRect().width, b.style[Ea] = "50% 50%", b.style[Ca] = "scaleX(0.5)", d = c === b.getBoundingClientRect().width && !(n && Fa), Ja.removeChild(a)), d
				}(),
				La = function (a, b, c, d, e, f) {
					var h, i, j, k, l, m, n, o, p, q, r, s, t, u, v = a._gsTransform,
						w = Qa(a, !0);
					v && (t = v.xOrigin, u = v.yOrigin), (!d || (h = d.split(" ")).length < 2) && (n = a.getBBox(), 0 === n.x && 0 === n.y && n.width + n.height === 0 && (n = {
						x: parseFloat(a.hasAttribute("x") ? a.getAttribute("x") : a.hasAttribute("cx") ? a.getAttribute("cx") : 0) || 0,
						y: parseFloat(a.hasAttribute("y") ? a.getAttribute("y") : a.hasAttribute("cy") ? a.getAttribute("cy") : 0) || 0,
						width: 0,
						height: 0
					}), b = ha(b).split(" "), h = [(-1 !== b[0].indexOf("%") ? parseFloat(b[0]) / 100 * n.width : parseFloat(b[0])) + n.x, (-1 !== b[1].indexOf("%") ? parseFloat(b[1]) / 100 * n.height : parseFloat(b[1])) + n.y]), c.xOrigin = k = parseFloat(h[0]), c.yOrigin = l = parseFloat(h[1]), d && w !== Pa && (m = w[0], n = w[1], o = w[2], p = w[3], q = w[4], r = w[5], s = m * p - n * o, s && (i = k * (p / s) + l * (-o / s) + (o * r - p * q) / s, j = k * (-n / s) + l * (m / s) - (m * r - n * q) / s, k = c.xOrigin = h[0] = i, l = c.yOrigin = h[1] = j)), v && (f && (c.xOffset = v.xOffset, c.yOffset = v.yOffset, v = c), e || e !== !1 && g.defaultSmoothOrigin !== !1 ? (i = k - t, j = l - u, v.xOffset += i * w[0] + j * w[2] - i, v.yOffset += i * w[1] + j * w[3] - j) : v.xOffset = v.yOffset = 0), f || a.setAttribute("data-svg-origin", h.join(" "))
				},
				Ma = function (a) {
					var b, c = P("svg", this.ownerSVGElement.getAttribute("xmlns") || "http://www.w3.org/2000/svg"),
						d = this.parentNode,
						e = this.nextSibling,
						f = this.style.cssText;
					if (Ja.appendChild(c), c.appendChild(this), this.style.display = "block", a) try {
						b = this.getBBox(), this._originalGetBBox = this.getBBox, this.getBBox = Ma
					} catch (g) {} else this._originalGetBBox && (b = this._originalGetBBox());
					return e ? d.insertBefore(this, e) : d.appendChild(this), Ja.removeChild(c), this.style.cssText = f, b
				},
				Na = function (a) {
					try {
						return a.getBBox()
					} catch (b) {
						return Ma.call(a, !0)
					}
				},
				Oa = function (a) {
					return !(!(Ha && a.getCTM && Na(a)) || a.parentNode && !a.ownerSVGElement)
				},
				Pa = [1, 0, 0, 1, 0, 0],
				Qa = function (a, b) {
					var c, d, e, f, g, h, i = a._gsTransform || new Ga,
						j = 1e5,
						k = a.style;
					if (Ca ? d = _(a, Da, null, !0) : a.currentStyle && (d = a.currentStyle.filter.match(G), d = d && 4 === d.length ? [d[0].substr(4), Number(d[2].substr(4)), Number(d[1].substr(4)), d[3].substr(4), i.x || 0, i.y || 0].join(",") : ""), c = !d || "none" === d || "matrix(1, 0, 0, 1, 0, 0)" === d, c && Ca && ((h = "none" === $(a).display) || !a.parentNode) && (h && (f = k.display, k.display = "block"), a.parentNode || (g = 1, Ja.appendChild(a)), d = _(a, Da, null, !0), c = !d || "none" === d || "matrix(1, 0, 0, 1, 0, 0)" === d, f ? k.display = f : h && Va(k, "display"), g && Ja.removeChild(a)), (i.svg || a.getCTM && Oa(a)) && (c && -1 !== (k[Ca] + "").indexOf("matrix") && (d = k[Ca], c = 0), e = a.getAttribute("transform"), c && e && (-1 !== e.indexOf("matrix") ? (d = e, c = 0) : -1 !== e.indexOf("translate") && (d = "matrix(1,0,0,1," + e.match(/(?:\-|\b)[\d\-\.e]+\b/gi).join(",") + ")", c = 0))), c) return Pa;
					for (e = (d || "").match(s) || [], wa = e.length; --wa > -1;) f = Number(e[wa]), e[wa] = (g = f - (f |= 0)) ? (g * j + (0 > g ? -.5 : .5) | 0) / j + f : f;
					return b && e.length > 6 ? [e[0], e[1], e[4], e[5], e[12], e[13]] : e
				},
				Ra = S.getTransform = function (a, c, d, e) {
					if (a._gsTransform && d && !e) return a._gsTransform;
					var f, h, i, j, k, l, m = d ? a._gsTransform || new Ga : new Ga,
						n = m.scaleX < 0,
						o = 2e-5,
						p = 1e5,
						q = Fa ? parseFloat(_(a, Ea, c, !1, "0 0 0").split(" ")[2]) || m.zOrigin || 0 : 0,
						r = parseFloat(g.defaultTransformPerspective) || 0;
					if (m.svg = !(!a.getCTM || !Oa(a)), m.svg && (La(a, _(a, Ea, c, !1, "50% 50%") + "", m, a.getAttribute("data-svg-origin")), Aa = g.useSVGTransformAttr || Ka), f = Qa(a), f !== Pa) {
						if (16 === f.length) {
							var s, t, u, v, w, x = f[0],
								y = f[1],
								z = f[2],
								A = f[3],
								B = f[4],
								C = f[5],
								D = f[6],
								E = f[7],
								F = f[8],
								G = f[9],
								H = f[10],
								I = f[12],
								J = f[13],
								K = f[14],
								M = f[11],
								N = Math.atan2(D, H);
							m.zOrigin && (K = -m.zOrigin, I = F * K - f[12], J = G * K - f[13], K = H * K + m.zOrigin - f[14]), m.rotationX = N * L, N && (v = Math.cos(-N), w = Math.sin(-N), s = B * v + F * w, t = C * v + G * w, u = D * v + H * w, F = B * -w + F * v, G = C * -w + G * v, H = D * -w + H * v, M = E * -w + M * v, B = s, C = t, D = u), N = Math.atan2(-z, H), m.rotationY = N * L, N && (v = Math.cos(-N), w = Math.sin(-N), s = x * v - F * w, t = y * v - G * w, u = z * v - H * w, G = y * w + G * v, H = z * w + H * v, M = A * w + M * v, x = s, y = t, z = u), N = Math.atan2(y, x), m.rotation = N * L, N && (v = Math.cos(-N), w = Math.sin(-N), x = x * v + B * w, t = y * v + C * w, C = y * -w + C * v, D = z * -w + D * v, y = t), m.rotationX && Math.abs(m.rotationX) + Math.abs(m.rotation) > 359.9 && (m.rotationX = m.rotation = 0, m.rotationY = 180 - m.rotationY), m.scaleX = (Math.sqrt(x * x + y * y) * p + .5 | 0) / p, m.scaleY = (Math.sqrt(C * C + G * G) * p + .5 | 0) / p, m.scaleZ = (Math.sqrt(D * D + H * H) * p + .5 | 0) / p, m.rotationX || m.rotationY ? m.skewX = 0 : (m.skewX = B || C ? Math.atan2(B, C) * L + m.rotation : m.skewX || 0, Math.abs(m.skewX) > 90 && Math.abs(m.skewX) < 270 && (n ? (m.scaleX *= -1, m.skewX += m.rotation <= 0 ? 180 : -180, m.rotation += m.rotation <= 0 ? 180 : -180) : (m.scaleY *= -1, m.skewX += m.skewX <= 0 ? 180 : -180))), m.perspective = M ? 1 / (0 > M ? -M : M) : 0, m.x = I, m.y = J, m.z = K, m.svg && (m.x -= m.xOrigin - (m.xOrigin * x - m.yOrigin * B), m.y -= m.yOrigin - (m.yOrigin * y - m.xOrigin * C))
						} else if (!Fa || e || !f.length || m.x !== f[4] || m.y !== f[5] || !m.rotationX && !m.rotationY) {
							var O = f.length >= 6,
								P = O ? f[0] : 1,
								Q = f[1] || 0,
								R = f[2] || 0,
								S = O ? f[3] : 1;
							m.x = f[4] || 0, m.y = f[5] || 0, i = Math.sqrt(P * P + Q * Q), j = Math.sqrt(S * S + R * R), k = P || Q ? Math.atan2(Q, P) * L : m.rotation || 0, l = R || S ? Math.atan2(R, S) * L + k : m.skewX || 0, Math.abs(l) > 90 && Math.abs(l) < 270 && (n ? (i *= -1, l += 0 >= k ? 180 : -180, k += 0 >= k ? 180 : -180) : (j *= -1, l += 0 >= l ? 180 : -180)), m.scaleX = i, m.scaleY = j, m.rotation = k, m.skewX = l, Fa && (m.rotationX = m.rotationY = m.z = 0, m.perspective = r, m.scaleZ = 1), m.svg && (m.x -= m.xOrigin - (m.xOrigin * P + m.yOrigin * R), m.y -= m.yOrigin - (m.xOrigin * Q + m.yOrigin * S))
						}
						m.zOrigin = q;
						for (h in m) m[h] < o && m[h] > -o && (m[h] = 0)
					}
					return d && (a._gsTransform = m, m.svg && (Aa && a.style[Ca] ? b.delayedCall(.001, function () {
						Va(a.style, Ca)
					}) : !Aa && a.getAttribute("transform") && b.delayedCall(.001, function () {
						a.removeAttribute("transform")
					}))), m
				},
				Sa = function (a) {
					var b, c, d = this.data,
						e = -d.rotation * K,
						f = e + d.skewX * K,
						g = 1e5,
						h = (Math.cos(e) * d.scaleX * g | 0) / g,
						i = (Math.sin(e) * d.scaleX * g | 0) / g,
						j = (Math.sin(f) * -d.scaleY * g | 0) / g,
						k = (Math.cos(f) * d.scaleY * g | 0) / g,
						l = this.t.style,
						m = this.t.currentStyle;
					if (m) {
						c = i, i = -j, j = -c, b = m.filter, l.filter = "";
						var n, o, q = this.t.offsetWidth,
							r = this.t.offsetHeight,
							s = "absolute" !== m.position,
							t = "progid:DXImageTransform.Microsoft.Matrix(M11=" + h + ", M12=" + i + ", M21=" + j + ", M22=" + k,
							u = d.x + q * d.xPercent / 100,
							v = d.y + r * d.yPercent / 100;
						if (null != d.ox && (n = (d.oxp ? q * d.ox * .01 : d.ox) - q / 2, o = (d.oyp ? r * d.oy * .01 : d.oy) - r / 2, u += n - (n * h + o * i), v += o - (n * j + o * k)), s ? (n = q / 2, o = r / 2, t += ", Dx=" + (n - (n * h + o * i) + u) + ", Dy=" + (o - (n * j + o * k) + v) + ")") : t += ", sizingMethod='auto expand')", -1 !== b.indexOf("DXImageTransform.Microsoft.Matrix(") ? l.filter = b.replace(H, t) : l.filter = t + " " + b, (0 === a || 1 === a) && 1 === h && 0 === i && 0 === j && 1 === k && (s && -1 === t.indexOf("Dx=0, Dy=0") || x.test(b) && 100 !== parseFloat(RegExp.$1) || -1 === b.indexOf(b.indexOf("Alpha")) && l.removeAttribute("filter")), !s) {
							var y, z, A, B = 8 > p ? 1 : -1;
							for (n = d.ieOffsetX || 0, o = d.ieOffsetY || 0, d.ieOffsetX = Math.round((q - ((0 > h ? -h : h) * q + (0 > i ? -i : i) * r)) / 2 + u), d.ieOffsetY = Math.round((r - ((0 > k ? -k : k) * r + (0 > j ? -j : j) * q)) / 2 + v), wa = 0; 4 > wa; wa++) z = fa[wa], y = m[z], c = -1 !== y.indexOf("px") ? parseFloat(y) : aa(this.t, z, parseFloat(y), y.replace(w, "")) || 0, A = c !== d[z] ? 2 > wa ? -d.ieOffsetX : -d.ieOffsetY : 2 > wa ? n - d.ieOffsetX : o - d.ieOffsetY, l[z] = (d[z] = Math.round(c - A * (0 === wa || 2 === wa ? 1 : B))) + "px"
						}
					}
				},
				Ta = S.set3DTransformRatio = S.setTransformRatio = function (a) {
					var b, c, d, e, f, g, h, i, j, k, l, m, o, p, q, r, s, t, u, v, w, x, y, z = this.data,
						A = this.t.style,
						B = z.rotation,
						C = z.rotationX,
						D = z.rotationY,
						E = z.scaleX,
						F = z.scaleY,
						G = z.scaleZ,
						H = z.x,
						I = z.y,
						J = z.z,
						L = z.svg,
						M = z.perspective,
						N = z.force3D,
						O = z.skewY,
						P = z.skewX;
					if (O && (P += O, B += O), ((1 === a || 0 === a) && "auto" === N && (this.tween._totalTime === this.tween._totalDuration || !this.tween._totalTime) || !N) && !J && !M && !D && !C && 1 === G || Aa && L || !Fa) return void(B || P || L ? (B *= K, x = P * K, y = 1e5, c = Math.cos(B) * E, f = Math.sin(B) * E, d = Math.sin(B - x) * -F, g = Math.cos(B - x) * F, x && "simple" === z.skewType && (b = Math.tan(x - O * K), b = Math.sqrt(1 + b * b), d *= b, g *= b, O && (b = Math.tan(O * K), b = Math.sqrt(1 + b * b), c *= b, f *= b)), L && (H += z.xOrigin - (z.xOrigin * c + z.yOrigin * d) + z.xOffset, I += z.yOrigin - (z.xOrigin * f + z.yOrigin * g) + z.yOffset, Aa && (z.xPercent || z.yPercent) && (q = this.t.getBBox(), H += .01 * z.xPercent * q.width, I += .01 * z.yPercent * q.height), q = 1e-6, q > H && H > -q && (H = 0), q > I && I > -q && (I = 0)), u = (c * y | 0) / y + "," + (f * y | 0) / y + "," + (d * y | 0) / y + "," + (g * y | 0) / y + "," + H + "," + I + ")", L && Aa ? this.t.setAttribute("transform", "matrix(" + u) : A[Ca] = (z.xPercent || z.yPercent ? "translate(" + z.xPercent + "%," + z.yPercent + "%) matrix(" : "matrix(") + u) : A[Ca] = (z.xPercent || z.yPercent ? "translate(" + z.xPercent + "%," + z.yPercent + "%) matrix(" : "matrix(") + E + ",0,0," + F + "," + H + "," + I + ")");
					if (n && (q = 1e-4, q > E && E > -q && (E = G = 2e-5), q > F && F > -q && (F = G = 2e-5), !M || z.z || z.rotationX || z.rotationY || (M = 0)), B || P) B *= K, r = c = Math.cos(B), s = f = Math.sin(B), P && (B -= P * K, r = Math.cos(B), s = Math.sin(B), "simple" === z.skewType && (b = Math.tan((P - O) * K), b = Math.sqrt(1 + b * b), r *= b, s *= b, z.skewY && (b = Math.tan(O * K), b = Math.sqrt(1 + b * b), c *= b, f *= b))), d = -s, g = r;
					else {
						if (!(D || C || 1 !== G || M || L)) return void(A[Ca] = (z.xPercent || z.yPercent ? "translate(" + z.xPercent + "%," + z.yPercent + "%) translate3d(" : "translate3d(") + H + "px," + I + "px," + J + "px)" + (1 !== E || 1 !== F ? " scale(" + E + "," + F + ")" : ""));
						c = g = 1, d = f = 0
					}
					k = 1, e = h = i = j = l = m = 0, o = M ? -1 / M : 0, p = z.zOrigin, q = 1e-6, v = ",", w = "0", B = D * K, B && (r = Math.cos(B), s = Math.sin(B), i = -s, l = o * -s, e = c * s, h = f * s, k = r, o *= r, c *= r, f *= r), B = C * K, B && (r = Math.cos(B), s = Math.sin(B), b = d * r + e * s, t = g * r + h * s, j = k * s, m = o * s, e = d * -s + e * r, h = g * -s + h * r, k *= r, o *= r, d = b, g = t), 1 !== G && (e *= G, h *= G, k *= G, o *= G), 1 !== F && (d *= F, g *= F, j *= F, m *= F), 1 !== E && (c *= E, f *= E, i *= E, l *= E), (p || L) && (p && (H += e * -p, I += h * -p, J += k * -p + p), L && (H += z.xOrigin - (z.xOrigin * c + z.yOrigin * d) + z.xOffset, I += z.yOrigin - (z.xOrigin * f + z.yOrigin * g) + z.yOffset), q > H && H > -q && (H = w), q > I && I > -q && (I = w), q > J && J > -q && (J = 0)), u = z.xPercent || z.yPercent ? "translate(" + z.xPercent + "%," + z.yPercent + "%) matrix3d(" : "matrix3d(", u += (q > c && c > -q ? w : c) + v + (q > f && f > -q ? w : f) + v + (q > i && i > -q ? w : i), u += v + (q > l && l > -q ? w : l) + v + (q > d && d > -q ? w : d) + v + (q > g && g > -q ? w : g), C || D || 1 !== G ? (u += v + (q > j && j > -q ? w : j) + v + (q > m && m > -q ? w : m) + v + (q > e && e > -q ? w : e), u += v + (q > h && h > -q ? w : h) + v + (q > k && k > -q ? w : k) + v + (q > o && o > -q ? w : o) + v) : u += ",0,0,0,0,1,0,", u += H + v + I + v + J + v + (M ? 1 + -J / M : 1) + ")", A[Ca] = u
				};
			j = Ga.prototype, j.x = j.y = j.z = j.skewX = j.skewY = j.rotation = j.rotationX = j.rotationY = j.zOrigin = j.xPercent = j.yPercent = j.xOffset = j.yOffset = 0, j.scaleX = j.scaleY = j.scaleZ = 1, ya("transform,scale,scaleX,scaleY,scaleZ,x,y,z,rotation,rotationX,rotationY,rotationZ,skewX,skewY,shortRotation,shortRotationX,shortRotationY,shortRotationZ,transformOrigin,svgOrigin,transformPerspective,directionalRotation,parseTransform,force3D,skewType,xPercent,yPercent,smoothOrigin", {
				parser: function (a, b, c, d, f, h, i) {
					if (d._lastParsedTransform === i) return f;
					d._lastParsedTransform = i;
					var j, k = i.scale && "function" == typeof i.scale ? i.scale : 0;
					"function" == typeof i[c] && (j = i[c], i[c] = b), k && (i.scale = k(r, a));
					var l, m, n, o, p, s, t, u, v, w = a._gsTransform,
						x = a.style,
						y = 1e-6,
						z = Ba.length,
						A = i,
						B = {},
						C = "transformOrigin",
						D = Ra(a, e, !0, A.parseTransform),
						E = A.transform && ("function" == typeof A.transform ? A.transform(r, q) : A.transform);
					if (d._transform = D, E && "string" == typeof E && Ca) m = Q.style, m[Ca] = E, m.display = "block", m.position = "absolute", O.body.appendChild(Q), l = Ra(Q, null, !1), D.svg && (s = D.xOrigin, t = D.yOrigin, l.x -= D.xOffset, l.y -= D.yOffset, (A.transformOrigin || A.svgOrigin) && (E = {}, La(a, ha(A.transformOrigin), E, A.svgOrigin, A.smoothOrigin, !0), s = E.xOrigin, t = E.yOrigin, l.x -= E.xOffset - D.xOffset, l.y -= E.yOffset - D.yOffset), (s || t) && (u = Qa(Q, !0), l.x -= s - (s * u[0] + t * u[2]), l.y -= t - (s * u[1] + t * u[3]))), O.body.removeChild(Q), l.perspective || (l.perspective = D.perspective), null != A.xPercent && (l.xPercent = ja(A.xPercent, D.xPercent)), null != A.yPercent && (l.yPercent = ja(A.yPercent, D.yPercent));
					else if ("object" == typeof A) {
						if (l = {
								scaleX: ja(null != A.scaleX ? A.scaleX : A.scale, D.scaleX),
								scaleY: ja(null != A.scaleY ? A.scaleY : A.scale, D.scaleY),
								scaleZ: ja(A.scaleZ, D.scaleZ),
								x: ja(A.x, D.x),
								y: ja(A.y, D.y),
								z: ja(A.z, D.z),
								xPercent: ja(A.xPercent, D.xPercent),
								yPercent: ja(A.yPercent, D.yPercent),
								perspective: ja(A.transformPerspective, D.perspective)
							}, p = A.directionalRotation, null != p)
							if ("object" == typeof p)
								for (m in p) A[m] = p[m];
							else A.rotation = p;
						"string" == typeof A.x && -1 !== A.x.indexOf("%") && (l.x = 0, l.xPercent = ja(A.x, D.xPercent)), "string" == typeof A.y && -1 !== A.y.indexOf("%") && (l.y = 0, l.yPercent = ja(A.y, D.yPercent)), l.rotation = ka("rotation" in A ? A.rotation : "shortRotation" in A ? A.shortRotation + "_short" : "rotationZ" in A ? A.rotationZ : D.rotation, D.rotation, "rotation", B), Fa && (l.rotationX = ka("rotationX" in A ? A.rotationX : "shortRotationX" in A ? A.shortRotationX + "_short" : D.rotationX || 0, D.rotationX, "rotationX", B), l.rotationY = ka("rotationY" in A ? A.rotationY : "shortRotationY" in A ? A.shortRotationY + "_short" : D.rotationY || 0, D.rotationY, "rotationY", B)), l.skewX = ka(A.skewX, D.skewX), l.skewY = ka(A.skewY, D.skewY)
					}
					for (Fa && null != A.force3D && (D.force3D = A.force3D, o = !0), D.skewType = A.skewType || D.skewType || g.defaultSkewType, n = D.force3D || D.z || D.rotationX || D.rotationY || l.z || l.rotationX || l.rotationY || l.perspective, n || null == A.scale || (l.scaleZ = 1); --z > -1;) v = Ba[z], E = l[v] - D[v], (E > y || -y > E || null != A[v] || null != M[v]) && (o = !0, f = new ta(D, v, D[v], E, f), v in B && (f.e = B[v]), f.xs0 = 0, f.plugin = h, d._overwriteProps.push(f.n));
					return E = A.transformOrigin, D.svg && (E || A.svgOrigin) && (s = D.xOffset, t = D.yOffset, La(a, ha(E), l, A.svgOrigin, A.smoothOrigin), f = ua(D, "xOrigin", (w ? D : l).xOrigin, l.xOrigin, f, C), f = ua(D, "yOrigin", (w ? D : l).yOrigin, l.yOrigin, f, C), (s !== D.xOffset || t !== D.yOffset) && (f = ua(D, "xOffset", w ? s : D.xOffset, D.xOffset, f, C), f = ua(D, "yOffset", w ? t : D.yOffset, D.yOffset, f, C)), E = "0px 0px"), (E || Fa && n && D.zOrigin) && (Ca ? (o = !0, v = Ea, E = (E || _(a, v, e, !1, "50% 50%")) + "", f = new ta(x, v, 0, 0, f, -1, C), f.b = x[v], f.plugin = h, Fa ? (m = D.zOrigin, E = E.split(" "), D.zOrigin = (E.length > 2 && (0 === m || "0px" !== E[2]) ? parseFloat(E[2]) : m) || 0, f.xs0 = f.e = E[0] + " " + (E[1] || "50%") + " 0px", f = new ta(D, "zOrigin", 0, 0, f, -1, f.n), f.b = m, f.xs0 = f.e = D.zOrigin) : f.xs0 = f.e = E) : ha(E + "", D)), o && (d._transformType = D.svg && Aa || !n && 3 !== this._transformType ? 2 : 3), j && (i[c] = j), k && (i.scale = k), f
				},
				prefix: !0
			}), ya("boxShadow", {
				defaultValue: "0px 0px 0px 0px #999",
				prefix: !0,
				color: !0,
				multi: !0,
				keyword: "inset"
			}), ya("borderRadius", {
				defaultValue: "0px",
				parser: function (a, b, c, f, g, h) {
					b = this.format(b);
					var i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y = ["borderTopLeftRadius", "borderTopRightRadius", "borderBottomRightRadius", "borderBottomLeftRadius"],
						z = a.style;
					for (q = parseFloat(a.offsetWidth), r = parseFloat(a.offsetHeight), i = b.split(" "), j = 0; j < y.length; j++) this.p.indexOf("border") && (y[j] = Z(y[j])), m = l = _(a, y[j], e, !1, "0px"), -1 !== m.indexOf(" ") && (l = m.split(" "), m = l[0], l = l[1]), n = k = i[j], o = parseFloat(m), t = m.substr((o + "").length), u = "=" === n.charAt(1), u ? (p = parseInt(n.charAt(0) + "1", 10), n = n.substr(2), p *= parseFloat(n), s = n.substr((p + "").length - (0 > p ? 1 : 0)) || "") : (p = parseFloat(n), s = n.substr((p + "").length)), "" === s && (s = d[c] || t), s !== t && (v = aa(a, "borderLeft", o, t), w = aa(a, "borderTop", o, t), "%" === s ? (m = v / q * 100 + "%", l = w / r * 100 + "%") : "em" === s ? (x = aa(a, "borderLeft", 1, "em"), m = v / x + "em", l = w / x + "em") : (m = v + "px", l = w + "px"), u && (n = parseFloat(m) + p + s, k = parseFloat(l) + p + s)), g = va(z, y[j], m + " " + l, n + " " + k, !1, "0px", g);
					return g
				},
				prefix: !0,
				formatter: qa("0px 0px 0px 0px", !1, !0)
			}), ya("borderBottomLeftRadius,borderBottomRightRadius,borderTopLeftRadius,borderTopRightRadius", {
				defaultValue: "0px",
				parser: function (a, b, c, d, f, g) {
					return va(a.style, c, this.format(_(a, c, e, !1, "0px 0px")), this.format(b), !1, "0px", f)
				},
				prefix: !0,
				formatter: qa("0px 0px", !1, !0)
			}), ya("backgroundPosition", {
				defaultValue: "0 0",
				parser: function (a, b, c, d, f, g) {
					var h, i, j, k, l, m, n = "background-position",
						o = e || $(a, null),
						q = this.format((o ? p ? o.getPropertyValue(n + "-x") + " " + o.getPropertyValue(n + "-y") : o.getPropertyValue(n) : a.currentStyle.backgroundPositionX + " " + a.currentStyle.backgroundPositionY) || "0 0"),
						r = this.format(b);
					if (-1 !== q.indexOf("%") != (-1 !== r.indexOf("%")) && r.split(",").length < 2 && (m = _(a, "backgroundImage").replace(D, ""), m && "none" !== m)) {
						for (h = q.split(" "), i = r.split(" "), R.setAttribute("src", m), j = 2; --j > -1;) q = h[j], k = -1 !== q.indexOf("%"), k !== (-1 !== i[j].indexOf("%")) && (l = 0 === j ? a.offsetWidth - R.width : a.offsetHeight - R.height, h[j] = k ? parseFloat(q) / 100 * l + "px" : parseFloat(q) / l * 100 + "%");
						q = h.join(" ")
					}
					return this.parseComplex(a.style, q, r, f, g)
				},
				formatter: ha
			}), ya("backgroundSize", {
				defaultValue: "0 0",
				formatter: function (a) {
					return a += "", ha(-1 === a.indexOf(" ") ? a + " " + a : a)
				}
			}), ya("perspective", {
				defaultValue: "0px",
				prefix: !0
			}), ya("perspectiveOrigin", {
				defaultValue: "50% 50%",
				prefix: !0
			}), ya("transformStyle", {
				prefix: !0
			}), ya("backfaceVisibility", {
				prefix: !0
			}), ya("userSelect", {
				prefix: !0
			}), ya("margin", {
				parser: ra("marginTop,marginRight,marginBottom,marginLeft")
			}), ya("padding", {
				parser: ra("paddingTop,paddingRight,paddingBottom,paddingLeft")
			}), ya("clip", {
				defaultValue: "rect(0px,0px,0px,0px)",
				parser: function (a, b, c, d, f, g) {
					var h, i, j;
					return 9 > p ? (i = a.currentStyle, j = 8 > p ? " " : ",", h = "rect(" + i.clipTop + j + i.clipRight + j + i.clipBottom + j + i.clipLeft + ")",
						b = this.format(b).split(",").join(j)) : (h = this.format(_(a, this.p, e, !1, this.dflt)), b = this.format(b)), this.parseComplex(a.style, h, b, f, g)
				}
			}), ya("textShadow", {
				defaultValue: "0px 0px 0px #999",
				color: !0,
				multi: !0
			}), ya("autoRound,strictUnits", {
				parser: function (a, b, c, d, e) {
					return e
				}
			}), ya("border", {
				defaultValue: "0px solid #000",
				parser: function (a, b, c, d, f, g) {
					var h = _(a, "borderTopWidth", e, !1, "0px"),
						i = this.format(b).split(" "),
						j = i[0].replace(w, "");
					return "px" !== j && (h = parseFloat(h) / aa(a, "borderTopWidth", 1, j) + j), this.parseComplex(a.style, this.format(h + " " + _(a, "borderTopStyle", e, !1, "solid") + " " + _(a, "borderTopColor", e, !1, "#000")), i.join(" "), f, g)
				},
				color: !0,
				formatter: function (a) {
					var b = a.split(" ");
					return b[0] + " " + (b[1] || "solid") + " " + (a.match(pa) || ["#000"])[0]
				}
			}), ya("borderWidth", {
				parser: ra("borderTopWidth,borderRightWidth,borderBottomWidth,borderLeftWidth")
			}), ya("float,cssFloat,styleFloat", {
				parser: function (a, b, c, d, e, f) {
					var g = a.style,
						h = "cssFloat" in g ? "cssFloat" : "styleFloat";
					return new ta(g, h, 0, 0, e, -1, c, !1, 0, g[h], b)
				}
			});
			var Ua = function (a) {
				var b, c = this.t,
					d = c.filter || _(this.data, "filter") || "",
					e = this.s + this.c * a | 0;
				100 === e && (-1 === d.indexOf("atrix(") && -1 === d.indexOf("radient(") && -1 === d.indexOf("oader(") ? (c.removeAttribute("filter"), b = !_(this.data, "filter")) : (c.filter = d.replace(z, ""), b = !0)), b || (this.xn1 && (c.filter = d = d || "alpha(opacity=" + e + ")"), -1 === d.indexOf("pacity") ? 0 === e && this.xn1 || (c.filter = d + " alpha(opacity=" + e + ")") : c.filter = d.replace(x, "opacity=" + e))
			};
			ya("opacity,alpha,autoAlpha", {
				defaultValue: "1",
				parser: function (a, b, c, d, f, g) {
					var h = parseFloat(_(a, "opacity", e, !1, "1")),
						i = a.style,
						j = "autoAlpha" === c;
					return "string" == typeof b && "=" === b.charAt(1) && (b = ("-" === b.charAt(0) ? -1 : 1) * parseFloat(b.substr(2)) + h), j && 1 === h && "hidden" === _(a, "visibility", e) && 0 !== b && (h = 0), U ? f = new ta(i, "opacity", h, b - h, f) : (f = new ta(i, "opacity", 100 * h, 100 * (b - h), f), f.xn1 = j ? 1 : 0, i.zoom = 1, f.type = 2, f.b = "alpha(opacity=" + f.s + ")", f.e = "alpha(opacity=" + (f.s + f.c) + ")", f.data = a, f.plugin = g, f.setRatio = Ua), j && (f = new ta(i, "visibility", 0, 0, f, -1, null, !1, 0, 0 !== h ? "inherit" : "hidden", 0 === b ? "hidden" : "inherit"), f.xs0 = "inherit", d._overwriteProps.push(f.n), d._overwriteProps.push(c)), f
				}
			});
			var Va = function (a, b) {
					b && (a.removeProperty ? (("ms" === b.substr(0, 2) || "webkit" === b.substr(0, 6)) && (b = "-" + b), a.removeProperty(b.replace(B, "-$1").toLowerCase())) : a.removeAttribute(b))
				},
				Wa = function (a) {
					if (this.t._gsClassPT = this, 1 === a || 0 === a) {
						this.t.setAttribute("class", 0 === a ? this.b : this.e);
						for (var b = this.data, c = this.t.style; b;) b.v ? c[b.p] = b.v : Va(c, b.p), b = b._next;
						1 === a && this.t._gsClassPT === this && (this.t._gsClassPT = null)
					} else this.t.getAttribute("class") !== this.e && this.t.setAttribute("class", this.e)
				};
			ya("className", {
				parser: function (a, b, d, f, g, h, i) {
					var j, k, l, m, n, o = a.getAttribute("class") || "",
						p = a.style.cssText;
					if (g = f._classNamePT = new ta(a, d, 0, 0, g, 2), g.setRatio = Wa, g.pr = -11, c = !0, g.b = o, k = ca(a, e), l = a._gsClassPT) {
						for (m = {}, n = l.data; n;) m[n.p] = 1, n = n._next;
						l.setRatio(1)
					}
					return a._gsClassPT = g, g.e = "=" !== b.charAt(1) ? b : o.replace(new RegExp("(?:\\s|^)" + b.substr(2) + "(?![\\w-])"), "") + ("+" === b.charAt(0) ? " " + b.substr(2) : ""), a.setAttribute("class", g.e), j = da(a, k, ca(a), i, m), a.setAttribute("class", o), g.data = j.firstMPT, a.style.cssText = p, g = g.xfirst = f.parse(a, j.difs, g, h)
				}
			});
			var Xa = function (a) {
				if ((1 === a || 0 === a) && this.data._totalTime === this.data._totalDuration && "isFromStart" !== this.data.data) {
					var b, c, d, e, f, g = this.t.style,
						h = i.transform.parse;
					if ("all" === this.e) g.cssText = "", e = !0;
					else
						for (b = this.e.split(" ").join("").split(","), d = b.length; --d > -1;) c = b[d], i[c] && (i[c].parse === h ? e = !0 : c = "transformOrigin" === c ? Ea : i[c].p), Va(g, c);
					e && (Va(g, Ca), f = this.t._gsTransform, f && (f.svg && (this.t.removeAttribute("data-svg-origin"), this.t.removeAttribute("transform")), delete this.t._gsTransform))
				}
			};
			for (ya("clearProps", {
					parser: function (a, b, d, e, f) {
						return f = new ta(a, d, 0, 0, f, 2), f.setRatio = Xa, f.e = b, f.pr = -10, f.data = e._tween, c = !0, f
					}
				}), j = "bezier,throwProps,physicsProps,physics2D".split(","), wa = j.length; wa--;) za(j[wa]);
			j = g.prototype, j._firstPT = j._lastParsedTransform = j._transform = null, j._onInitTween = function (a, b, h, j) {
				if (!a.nodeType) return !1;
				this._target = q = a, this._tween = h, this._vars = b, r = j, k = b.autoRound, c = !1, d = b.suffixMap || g.suffixMap, e = $(a, ""), f = this._overwriteProps;
				var n, p, s, t, u, v, w, x, z, A = a.style;
				if (l && "" === A.zIndex && (n = _(a, "zIndex", e), ("auto" === n || "" === n) && this._addLazySet(A, "zIndex", 0)), "string" == typeof b && (t = A.cssText, n = ca(a, e), A.cssText = t + ";" + b, n = da(a, n, ca(a)).difs, !U && y.test(b) && (n.opacity = parseFloat(RegExp.$1)), b = n, A.cssText = t), b.className ? this._firstPT = p = i.className.parse(a, b.className, "className", this, null, null, b) : this._firstPT = p = this.parse(a, b, null), this._transformType) {
					for (z = 3 === this._transformType, Ca ? m && (l = !0, "" === A.zIndex && (w = _(a, "zIndex", e), ("auto" === w || "" === w) && this._addLazySet(A, "zIndex", 0)), o && this._addLazySet(A, "WebkitBackfaceVisibility", this._vars.WebkitBackfaceVisibility || (z ? "visible" : "hidden"))) : A.zoom = 1, s = p; s && s._next;) s = s._next;
					x = new ta(a, "transform", 0, 0, null, 2), this._linkCSSP(x, null, s), x.setRatio = Ca ? Ta : Sa, x.data = this._transform || Ra(a, e, !0), x.tween = h, x.pr = -1, f.pop()
				}
				if (c) {
					for (; p;) {
						for (v = p._next, s = t; s && s.pr > p.pr;) s = s._next;
						(p._prev = s ? s._prev : u) ? p._prev._next = p: t = p, (p._next = s) ? s._prev = p : u = p, p = v
					}
					this._firstPT = t
				}
				return !0
			}, j.parse = function (a, b, c, f) {
				var g, h, j, l, m, n, o, p, s, t, u = a.style;
				for (g in b) n = b[g], "function" == typeof n && (n = n(r, q)), h = i[g], h ? c = h.parse(a, n, g, this, c, f, b) : (m = _(a, g, e) + "", s = "string" == typeof n, "color" === g || "fill" === g || "stroke" === g || -1 !== g.indexOf("Color") || s && A.test(n) ? (s || (n = na(n), n = (n.length > 3 ? "rgba(" : "rgb(") + n.join(",") + ")"), c = va(u, g, m, n, !0, "transparent", c, 0, f)) : s && J.test(n) ? c = va(u, g, m, n, !0, null, c, 0, f) : (j = parseFloat(m), o = j || 0 === j ? m.substr((j + "").length) : "", ("" === m || "auto" === m) && ("width" === g || "height" === g ? (j = ga(a, g, e), o = "px") : "left" === g || "top" === g ? (j = ba(a, g, e), o = "px") : (j = "opacity" !== g ? 0 : 1, o = "")), t = s && "=" === n.charAt(1), t ? (l = parseInt(n.charAt(0) + "1", 10), n = n.substr(2), l *= parseFloat(n), p = n.replace(w, "")) : (l = parseFloat(n), p = s ? n.replace(w, "") : ""), "" === p && (p = g in d ? d[g] : o), n = l || 0 === l ? (t ? l + j : l) + p : b[g], o !== p && "" !== p && (l || 0 === l) && j && (j = aa(a, g, j, o), "%" === p ? (j /= aa(a, g, 100, "%") / 100, b.strictUnits !== !0 && (m = j + "%")) : "em" === p || "rem" === p || "vw" === p || "vh" === p ? j /= aa(a, g, 1, p) : "px" !== p && (l = aa(a, g, l, p), p = "px"), t && (l || 0 === l) && (n = l + j + p)), t && (l += j), !j && 0 !== j || !l && 0 !== l ? void 0 !== u[g] && (n || n + "" != "NaN" && null != n) ? (c = new ta(u, g, l || j || 0, 0, c, -1, g, !1, 0, m, n), c.xs0 = "none" !== n || "display" !== g && -1 === g.indexOf("Style") ? n : m) : W("invalid " + g + " tween value: " + b[g]) : (c = new ta(u, g, j, l - j, c, 0, g, k !== !1 && ("px" === p || "zIndex" === g), 0, m, n), c.xs0 = p))), f && c && !c.plugin && (c.plugin = f);
				return c
			}, j.setRatio = function (a) {
				var b, c, d, e = this._firstPT,
					f = 1e-6;
				if (1 !== a || this._tween._time !== this._tween._duration && 0 !== this._tween._time)
					if (a || this._tween._time !== this._tween._duration && 0 !== this._tween._time || this._tween._rawPrevTime === -1e-6)
						for (; e;) {
							if (b = e.c * a + e.s, e.r ? b = Math.round(b) : f > b && b > -f && (b = 0), e.type)
								if (1 === e.type)
									if (d = e.l, 2 === d) e.t[e.p] = e.xs0 + b + e.xs1 + e.xn1 + e.xs2;
									else if (3 === d) e.t[e.p] = e.xs0 + b + e.xs1 + e.xn1 + e.xs2 + e.xn2 + e.xs3;
							else if (4 === d) e.t[e.p] = e.xs0 + b + e.xs1 + e.xn1 + e.xs2 + e.xn2 + e.xs3 + e.xn3 + e.xs4;
							else if (5 === d) e.t[e.p] = e.xs0 + b + e.xs1 + e.xn1 + e.xs2 + e.xn2 + e.xs3 + e.xn3 + e.xs4 + e.xn4 + e.xs5;
							else {
								for (c = e.xs0 + b + e.xs1, d = 1; d < e.l; d++) c += e["xn" + d] + e["xs" + (d + 1)];
								e.t[e.p] = c
							} else -1 === e.type ? e.t[e.p] = e.xs0 : e.setRatio && e.setRatio(a);
							else e.t[e.p] = b + e.xs0;
							e = e._next
						} else
							for (; e;) 2 !== e.type ? e.t[e.p] = e.b : e.setRatio(a), e = e._next;
					else
						for (; e;) {
							if (2 !== e.type)
								if (e.r && -1 !== e.type)
									if (b = Math.round(e.s + e.c), e.type) {
										if (1 === e.type) {
											for (d = e.l, c = e.xs0 + b + e.xs1, d = 1; d < e.l; d++) c += e["xn" + d] + e["xs" + (d + 1)];
											e.t[e.p] = c
										}
									} else e.t[e.p] = b + e.xs0;
							else e.t[e.p] = e.e;
							else e.setRatio(a);
							e = e._next
						}
			}, j._enableTransforms = function (a) {
				this._transform = this._transform || Ra(this._target, e, !0), this._transformType = this._transform.svg && Aa || !a && 3 !== this._transformType ? 2 : 3
			};
			var Ya = function (a) {
				this.t[this.p] = this.e, this.data._linkCSSP(this, this._next, null, !0)
			};
			j._addLazySet = function (a, b, c) {
				var d = this._firstPT = new ta(a, b, 0, 0, this._firstPT, 2);
				d.e = c, d.setRatio = Ya, d.data = this
			}, j._linkCSSP = function (a, b, c, d) {
				return a && (b && (b._prev = a), a._next && (a._next._prev = a._prev), a._prev ? a._prev._next = a._next : this._firstPT === a && (this._firstPT = a._next, d = !0), c ? c._next = a : d || null !== this._firstPT || (this._firstPT = a), a._next = b, a._prev = c), a
			}, j._mod = function (a) {
				for (var b = this._firstPT; b;) "function" == typeof a[b.p] && a[b.p] === Math.round && (b.r = 1), b = b._next
			}, j._kill = function (b) {
				var c, d, e, f = b;
				if (b.autoAlpha || b.alpha) {
					f = {};
					for (d in b) f[d] = b[d];
					f.opacity = 1, f.autoAlpha && (f.visibility = 1)
				}
				for (b.className && (c = this._classNamePT) && (e = c.xfirst, e && e._prev ? this._linkCSSP(e._prev, c._next, e._prev._prev) : e === this._firstPT && (this._firstPT = c._next), c._next && this._linkCSSP(c._next, c._next._next, e._prev), this._classNamePT = null), c = this._firstPT; c;) c.plugin && c.plugin !== d && c.plugin._kill && (c.plugin._kill(b), d = c.plugin), c = c._next;
				return a.prototype._kill.call(this, f)
			};
			var Za = function (a, b, c) {
				var d, e, f, g;
				if (a.slice)
					for (e = a.length; --e > -1;) Za(a[e], b, c);
				else
					for (d = a.childNodes, e = d.length; --e > -1;) f = d[e], g = f.type, f.style && (b.push(ca(f)), c && c.push(f)), 1 !== g && 9 !== g && 11 !== g || !f.childNodes.length || Za(f, b, c)
			};
			return g.cascadeTo = function (a, c, d) {
				var e, f, g, h, i = b.to(a, c, d),
					j = [i],
					k = [],
					l = [],
					m = [],
					n = b._internals.reservedProps;
				for (a = i._targets || i.target, Za(a, k, m), i.render(c, !0, !0), Za(a, l), i.render(0, !0, !0), i._enabled(!0), e = m.length; --e > -1;)
					if (f = da(m[e], k[e], l[e]), f.firstMPT) {
						f = f.difs;
						for (g in d) n[g] && (f[g] = d[g]);
						h = {};
						for (g in f) h[g] = k[e][g];
						j.push(b.fromTo(m[e], c, h, f))
					}
				return j
			}, a.activate([g]), g
		}, !0)
	}), _gsScope._gsDefine && _gsScope._gsQueue.pop()(),
	function (a) {
		"use strict";
		var b = function () {
			return (_gsScope.GreenSockGlobals || _gsScope)[a]
		};
		"function" == typeof define && define.amd ? define(["TweenLite"], b) : "undefined" != typeof module && module.exports && (require("../TweenLite.js"), module.exports = b())
	}("CSSPlugin");


/* SPLIT TEXT UTIL */
/*!
 * VERSION: 0.5.6
 * DATE: 2017-01-17
 * UPDATES AND DOCS AT: http://greensock.com
 *
 * @license Copyright (c) 2008-2017, GreenSock. All rights reserved.
 * SplitText is a Club GreenSock membership benefit; You must have a valid membership to use
 * this code without violating the terms of use. Visit http://greensock.com/club/ to sign up or get more details.
 * This work is subject to the software agreement that was issued with your membership.
 * 
 * @author: Jack Doyle, jack@greensock.com
 */
var _gsScope = "undefined" != typeof module && module.exports && "undefined" != typeof global ? global : this || window;
! function (a) {
	"use strict";
	var b = a.GreenSockGlobals || a,
		c = function (a) {
			var c, d = a.split("."),
				e = b;
			for (c = 0; c < d.length; c++) e[d[c]] = e = e[d[c]] || {};
			return e
		},
		d = c("com.greensock.utils"),
		e = function (a) {
			var b = a.nodeType,
				c = "";
			if (1 === b || 9 === b || 11 === b) {
				if ("string" == typeof a.textContent) return a.textContent;
				for (a = a.firstChild; a; a = a.nextSibling) c += e(a)
			} else if (3 === b || 4 === b) return a.nodeValue;
			return c
		},
		f = document,
		g = f.defaultView ? f.defaultView.getComputedStyle : function () {},
		h = /([A-Z])/g,
		i = function (a, b, c, d) {
			var e;
			return (c = c || g(a, null)) ? (a = c.getPropertyValue(b.replace(h, "-$1").toLowerCase()), e = a || c.length ? a : c[b]) : a.currentStyle && (c = a.currentStyle, e = c[b]), d ? e : parseInt(e, 10) || 0
		},
		j = function (a) {
			return a.length && a[0] && (a[0].nodeType && a[0].style && !a.nodeType || a[0].length && a[0][0]) ? !0 : !1
		},
		k = function (a) {
			var b, c, d, e = [],
				f = a.length;
			for (b = 0; f > b; b++)
				if (c = a[b], j(c))
					for (d = c.length, d = 0; d < c.length; d++) e.push(c[d]);
				else e.push(c);
			return e
		},
		l = /(?:\r|\n|\t\t)/g,
		m = /(?:\s\s+)/g,
		n = 55296,
		o = 56319,
		p = 56320,
		q = 127462,
		r = 127487,
		s = 127995,
		t = 127999,
		u = function (a) {
			return (a.charCodeAt(0) - n << 10) + (a.charCodeAt(1) - p) + 65536
		},
		v = f.all && !f.addEventListener,
		w = " style='position:relative;display:inline-block;" + (v ? "*display:inline;*zoom:1;'" : "'"),
		x = function (a, b) {
			a = a || "";
			var c = -1 !== a.indexOf("++"),
				d = 1;
			return c && (a = a.split("++").join("")),
				function () {
					return "<" + b + w + (a ? " class='" + a + (c ? d++ : "") + "'>" : ">")
				}
		},
		y = d.SplitText = b.SplitText = function (a, b) {
			if ("string" == typeof a && (a = y.selector(a)), !a) throw "cannot split a null element.";
			this.elements = j(a) ? k(a) : [a], this.chars = [], this.words = [], this.lines = [], this._originals = [], this.vars = b || {}, this.split(b)
		},
		z = function (a, b, c) {
			var d = a.nodeType;
			if (1 === d || 9 === d || 11 === d)
				for (a = a.firstChild; a; a = a.nextSibling) z(a, b, c);
			else(3 === d || 4 === d) && (a.nodeValue = a.nodeValue.split(b).join(c))
		},
		A = function (a, b) {
			for (var c = b.length; --c > -1;) a.push(b[c])
		},
		B = function (a) {
			var b, c = [],
				d = a.length;
			for (b = 0; b !== d; c.push(a[b++]));
			return c
		},
		C = function (a, b, c) {
			for (var d; a && a !== b;) {
				if (d = a._next || a.nextSibling) return d.textContent.charAt(0) === c;
				a = a.parentNode || a._parent
			}
			return !1
		},
		D = function (a) {
			var b, c, d = B(a.childNodes),
				e = d.length;
			for (b = 0; e > b; b++) c = d[b], c._isSplit ? D(c) : (b && 3 === c.previousSibling.nodeType ? c.previousSibling.nodeValue += 3 === c.nodeType ? c.nodeValue : c.firstChild.nodeValue : 3 !== c.nodeType && a.insertBefore(c.firstChild, c), a.removeChild(c))
		},
		E = function (a, b, c, d, e, h, j) {
			var k, l, m, n, o, p, q, r, s, t, u, v, w = g(a),
				x = i(a, "paddingLeft", w),
				y = -999,
				B = i(a, "borderBottomWidth", w) + i(a, "borderTopWidth", w),
				E = i(a, "borderLeftWidth", w) + i(a, "borderRightWidth", w),
				F = i(a, "paddingTop", w) + i(a, "paddingBottom", w),
				G = i(a, "paddingLeft", w) + i(a, "paddingRight", w),
				H = .2 * i(a, "fontSize"),
				I = i(a, "textAlign", w, !0),
				J = [],
				K = [],
				L = [],
				M = b.wordDelimiter || " ",
				N = b.span ? "span" : "div",
				O = b.type || b.split || "chars,words,lines",
				P = e && -1 !== O.indexOf("lines") ? [] : null,
				Q = -1 !== O.indexOf("words"),
				R = -1 !== O.indexOf("chars"),
				S = "absolute" === b.position || b.absolute === !0,
				T = b.linesClass,
				U = -1 !== (T || "").indexOf("++"),
				V = [];
			for (P && 1 === a.children.length && a.children[0]._isSplit && (a = a.children[0]), U && (T = T.split("++").join("")), l = a.getElementsByTagName("*"), m = l.length, o = [], k = 0; m > k; k++) o[k] = l[k];
			if (P || S)
				for (k = 0; m > k; k++) n = o[k], p = n.parentNode === a, (p || S || R && !Q) && (v = n.offsetTop, P && p && Math.abs(v - y) > H && "BR" !== n.nodeName && (q = [], P.push(q), y = v), S && (n._x = n.offsetLeft, n._y = v, n._w = n.offsetWidth, n._h = n.offsetHeight), P && ((n._isSplit && p || !R && p || Q && p || !Q && n.parentNode.parentNode === a && !n.parentNode._isSplit) && (q.push(n), n._x -= x, C(n, a, M) && (n._wordEnd = !0)), "BR" === n.nodeName && n.nextSibling && "BR" === n.nextSibling.nodeName && P.push([])));
			for (k = 0; m > k; k++) n = o[k], p = n.parentNode === a, "BR" !== n.nodeName ? (S && (s = n.style, Q || p || (n._x += n.parentNode._x, n._y += n.parentNode._y), s.left = n._x + "px", s.top = n._y + "px", s.position = "absolute", s.display = "block", s.width = n._w + 1 + "px", s.height = n._h + "px"), !Q && R ? n._isSplit ? (n._next = n.nextSibling, n.parentNode.appendChild(n)) : n.parentNode._isSplit ? (n._parent = n.parentNode, !n.previousSibling && n.firstChild && (n.firstChild._isFirst = !0), n.nextSibling && " " === n.nextSibling.textContent && !n.nextSibling.nextSibling && V.push(n.nextSibling), n._next = n.nextSibling && n.nextSibling._isFirst ? null : n.nextSibling, n.parentNode.removeChild(n), o.splice(k--, 1), m--) : p || (v = !n.nextSibling && C(n.parentNode, a, M), n.parentNode._parent && n.parentNode._parent.appendChild(n), v && n.parentNode.appendChild(f.createTextNode(" ")), b.span && (n.style.display = "inline"), J.push(n)) : n.parentNode._isSplit && !n._isSplit && "" !== n.innerHTML ? K.push(n) : R && !n._isSplit && (b.span && (n.style.display = "inline"), J.push(n))) : P || S ? (n.parentNode && n.parentNode.removeChild(n), o.splice(k--, 1), m--) : Q || a.appendChild(n);
			for (k = V.length; --k > -1;) V[k].parentNode.removeChild(V[k]);
			if (P) {
				for (S && (t = f.createElement(N), a.appendChild(t), u = t.offsetWidth + "px", v = t.offsetParent === a ? 0 : a.offsetLeft, a.removeChild(t)), s = a.style.cssText, a.style.cssText = "display:none;"; a.firstChild;) a.removeChild(a.firstChild);
				for (r = " " === M && (!S || !Q && !R), k = 0; k < P.length; k++) {
					for (q = P[k], t = f.createElement(N), t.style.cssText = "display:block;text-align:" + I + ";position:" + (S ? "absolute;" : "relative;"), T && (t.className = T + (U ? k + 1 : "")), L.push(t), m = q.length, l = 0; m > l; l++) "BR" !== q[l].nodeName && (n = q[l], t.appendChild(n), r && n._wordEnd && t.appendChild(f.createTextNode(" ")), S && (0 === l && (t.style.top = n._y + "px", t.style.left = x + v + "px"), n.style.top = "0px", v && (n.style.left = n._x - v + "px")));
					0 === m ? t.innerHTML = "&nbsp;" : Q || R || (D(t), z(t, String.fromCharCode(160), " ")), S && (t.style.width = u, t.style.height = n._h + "px"), a.appendChild(t)
				}
				a.style.cssText = s
			}
			S && (j > a.clientHeight && (a.style.height = j - F + "px", a.clientHeight < j && (a.style.height = j + B + "px")), h > a.clientWidth && (a.style.width = h - G + "px", a.clientWidth < h && (a.style.width = h + E + "px"))), A(c, J), A(d, K), A(e, L)
		},
		F = function (a, b, c, d) {
			var g, h, i, j, k, p, v, w, x, y = b.span ? "span" : "div",
				A = b.type || b.split || "chars,words,lines",
				B = (-1 !== A.indexOf("words"), -1 !== A.indexOf("chars")),
				C = "absolute" === b.position || b.absolute === !0,
				D = b.wordDelimiter || " ",
				E = " " !== D ? "" : C ? "&#173; " : " ",
				F = b.span ? "</span>" : "</div>",
				G = !0,
				H = f.createElement("div"),
				I = a.parentNode;
			for (I.insertBefore(H, a), H.textContent = a.nodeValue, I.removeChild(a), a = H, g = e(a), v = -1 !== g.indexOf("<"), b.reduceWhiteSpace !== !1 && (g = g.replace(m, " ").replace(l, "")), v && (g = g.split("<").join("{{LT}}")), k = g.length, h = (" " === g.charAt(0) ? E : "") + c(), i = 0; k > i; i++)
				if (p = g.charAt(i), p === D && g.charAt(i - 1) !== D && i) {
					for (h += G ? F : "", G = !1; g.charAt(i + 1) === D;) h += E, i++;
					i === k - 1 ? h += E : ")" !== g.charAt(i + 1) && (h += E + c(), G = !0)
				} else "{" === p && "{{LT}}" === g.substr(i, 6) ? (h += B ? d() + "{{LT}}</" + y + ">" : "{{LT}}", i += 5) : p.charCodeAt(0) >= n && p.charCodeAt(0) <= o || g.charCodeAt(i + 1) >= 65024 && g.charCodeAt(i + 1) <= 65039 ? (w = u(g.substr(i, 2)), x = u(g.substr(i + 2, 2)), j = w >= q && r >= w && x >= q && r >= x || x >= s && t >= x ? 4 : 2, h += B && " " !== p ? d() + g.substr(i, j) + "</" + y + ">" : g.substr(i, j), i += j - 1) : h += B && " " !== p ? d() + p + "</" + y + ">" : p;
			a.outerHTML = h + (G ? F : ""), v && z(I, "{{LT}}", "<")
		},
		G = function (a, b, c, d) {
			var e, f, g = B(a.childNodes),
				h = g.length,
				j = "absolute" === b.position || b.absolute === !0;
			if (3 !== a.nodeType || h > 1) {
				for (b.absolute = !1, e = 0; h > e; e++) f = g[e], (3 !== f.nodeType || /\S+/.test(f.nodeValue)) && (j && 3 !== f.nodeType && "inline" === i(f, "display", null, !0) && (f.style.display = "inline-block", f.style.position = "relative"), f._isSplit = !0, G(f, b, c, d));
				return b.absolute = j, void(a._isSplit = !0)
			}
			F(a, b, c, d)
		},
		H = y.prototype;
	H.split = function (a) {
		this.isSplit && this.revert(), this.vars = a = a || this.vars, this._originals.length = this.chars.length = this.words.length = this.lines.length = 0;
		for (var b, c, d, e = this.elements.length, f = a.span ? "span" : "div", g = ("absolute" === a.position || a.absolute === !0, x(a.wordsClass, f)), h = x(a.charsClass, f); --e > -1;) d = this.elements[e], this._originals[e] = d.innerHTML, b = d.clientHeight, c = d.clientWidth, G(d, a, g, h), E(d, a, this.chars, this.words, this.lines, c, b);
		return this.chars.reverse(), this.words.reverse(), this.lines.reverse(), this.isSplit = !0, this
	}, H.revert = function () {
		if (!this._originals) throw "revert() call wasn't scoped properly.";
		for (var a = this._originals.length; --a > -1;) this.elements[a].innerHTML = this._originals[a];
		return this.chars = [], this.words = [], this.lines = [], this.isSplit = !1, this
	}, y.selector = a.$ || a.jQuery || function (b) {
		var c = a.$ || a.jQuery;
		return c ? (y.selector = c, c(b)) : "undefined" == typeof document ? b : document.querySelectorAll ? document.querySelectorAll(b) : document.getElementById("#" === b.charAt(0) ? b.substr(1) : b)
	}, y.version = "0.5.6"
}(_gsScope),
function (a) {
	"use strict";
	var b = function () {
		return (_gsScope.GreenSockGlobals || _gsScope)[a]
	};
	"function" == typeof define && define.amd ? define([], b) : "undefined" != typeof module && module.exports && (module.exports = b())
}("SplitText");


try {
	window.GreenSockGlobals = null;
	window._gsQueue = null;
	window._gsDefine = null;

	delete(window.GreenSockGlobals);
	delete(window._gsQueue);
	delete(window._gsDefine);
} catch (e) {}

try {
	window.GreenSockGlobals = oldgs;
	window._gsQueue = oldgs_queue;
} catch (e) {}

if (window.tplogs == true)
	try {
		console.groupEnd();
	} catch (e) {}

(function (e, t) {
	e.waitForImages = {
		hasImageProperties: ["backgroundImage", "listStyleImage", "borderImage", "borderCornerImage"]
	};
	e.expr[":"].uncached = function (t) {
		var n = document.createElement("img");
		n.src = t.src;
		return e(t).is('img[src!=""]') && !n.complete
	};
	e.fn.waitForImages = function (t, n, r) {
		if (e.isPlainObject(arguments[0])) {
			n = t.each;
			r = t.waitForAll;
			t = t.finished
		}
		t = t || e.noop;
		n = n || e.noop;
		r = !!r;
		if (!e.isFunction(t) || !e.isFunction(n)) {
			throw new TypeError("An invalid callback was supplied.")
		}
		return this.each(function () {
			var i = e(this),
				s = [];
			if (r) {
				var o = e.waitForImages.hasImageProperties || [],
					u = /url\((['"]?)(.*?)\1\)/g;
				i.find("*").each(function () {
					var t = e(this);
					if (t.is("img:uncached")) {
						s.push({
							src: t.attr("src"),
							element: t[0]
						})
					}
					e.each(o, function (e, n) {
						var r = t.css(n);
						if (!r) {
							return true
						}
						var i;
						while (i = u.exec(r)) {
							s.push({
								src: i[2],
								element: t[0]
							})
						}
					})
				})
			} else {
				i.find("img:uncached").each(function () {
					s.push({
						src: this.src,
						element: this
					})
				})
			}
			var f = s.length,
				l = 0;
			if (f == 0) {
				t.call(i[0])
			}
			e.each(s, function (r, s) {
				var o = new Image;
				e(o).bind("load error", function (e) {
					l++;
					n.call(s.element, l, f, e.type == "load");
					if (l == f) {
						t.call(i[0]);
						return false
					}
				});
				o.src = s.src
			})
		})
	};
})(jQuery)

;
/**************************************************************************
 * jquery.themepunch.revolution.js - jQuery Plugin for Revolution Slider
 * @version: 5.4.6 (23.08.2017)
 * @requires jQuery v1.7 or later (tested on 1.9)
 * @author ThemePunch
 **************************************************************************/
! function (jQuery, undefined) {
	"use strict";
	var version = {
		core: "5.4.6",
		"revolution.extensions.actions.min.js": "2.1.0",
		"revolution.extensions.carousel.min.js": "1.2.1",
		"revolution.extensions.kenburn.min.js": "1.3.1",
		"revolution.extensions.layeranimation.min.js": "3.6.1",
		"revolution.extensions.navigation.min.js": "1.3.3",
		"revolution.extensions.parallax.min.js": "2.2.0",
		"revolution.extensions.slideanims.min.js": "1.7",
		"revolution.extensions.video.min.js": "2.1.1"
	};
	jQuery.fn.extend({
		revolution: function (e) {
			var i = {
				delay: 9e3,
				responsiveLevels: 4064,
				visibilityLevels: [2048, 1024, 778, 480],
				gridwidth: 960,
				gridheight: 500,
				minHeight: 0,
				autoHeight: "off",
				sliderType: "standard",
				sliderLayout: "auto",
				fullScreenAutoWidth: "off",
				fullScreenAlignForce: "off",
				fullScreenOffsetContainer: "",
				fullScreenOffset: "0",
				hideCaptionAtLimit: 0,
				hideAllCaptionAtLimit: 0,
				hideSliderAtLimit: 0,
				disableProgressBar: "off",
				stopAtSlide: -1,
				stopAfterLoops: -1,
				shadow: 0,
				dottedOverlay: "none",
				startDelay: 0,
				lazyType: "smart",
				spinner: "spinner0",
				shuffle: "off",
				viewPort: {
					enable: !1,
					outof: "wait",
					visible_area: "60%",
					presize: !1
				},
				fallbacks: {
					isJoomla: !1,
					panZoomDisableOnMobile: "off",
					simplifyAll: "on",
					nextSlideOnWindowFocus: "off",
					disableFocusListener: !0,
					ignoreHeightChanges: "off",
					ignoreHeightChangesSize: 0,
					allowHTML5AutoPlayOnAndroid: !0
				},
				parallax: {
					type: "off",
					levels: [10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85],
					origo: "enterpoint",
					speed: 400,
					bgparallax: "off",
					opacity: "on",
					disable_onmobile: "off",
					ddd_shadow: "on",
					ddd_bgfreeze: "off",
					ddd_overflow: "visible",
					ddd_layer_overflow: "visible",
					ddd_z_correction: 65,
					ddd_path: "mouse"
				},
				scrolleffect: {
					fade: "off",
					blur: "off",
					scale: "off",
					grayscale: "off",
					maxblur: 10,
					on_layers: "off",
					on_slidebg: "off",
					on_static_layers: "off",
					on_parallax_layers: "off",
					on_parallax_static_layers: "off",
					direction: "both",
					multiplicator: 1.35,
					multiplicator_layers: .5,
					tilt: 30,
					disable_on_mobile: "on"
				},
				carousel: {
					easing: punchgs.Power3.easeInOut,
					speed: 800,
					showLayersAllTime: "off",
					horizontal_align: "center",
					vertical_align: "center",
					infinity: "on",
					space: 0,
					maxVisibleItems: 3,
					stretch: "off",
					fadeout: "on",
					maxRotation: 0,
					minScale: 0,
					vary_fade: "off",
					vary_rotation: "on",
					vary_scale: "off",
					border_radius: "0px",
					padding_top: 0,
					padding_bottom: 0
				},
				navigation: {
					keyboardNavigation: "off",
					keyboard_direction: "horizontal",
					mouseScrollNavigation: "off",
					onHoverStop: "on",
					touch: {
						touchenabled: "off",
						touchOnDesktop: "off",
						swipe_treshold: 75,
						swipe_min_touches: 1,
						drag_block_vertical: !1,
						swipe_direction: "horizontal"
					},
					arrows: {
						style: "",
						enable: !1,
						hide_onmobile: !1,
						hide_onleave: !0,
						hide_delay: 200,
						hide_delay_mobile: 1200,
						hide_under: 0,
						hide_over: 9999,
						tmp: "",
						rtl: !1,
						left: {
							h_align: "left",
							v_align: "center",
							h_offset: 20,
							v_offset: 0,
							container: "slider"
						},
						right: {
							h_align: "right",
							v_align: "center",
							h_offset: 20,
							v_offset: 0,
							container: "slider"
						}
					},
					bullets: {
						container: "slider",
						rtl: !1,
						style: "",
						enable: !1,
						hide_onmobile: !1,
						hide_onleave: !0,
						hide_delay: 200,
						hide_delay_mobile: 1200,
						hide_under: 0,
						hide_over: 9999,
						direction: "horizontal",
						h_align: "left",
						v_align: "center",
						space: 0,
						h_offset: 20,
						v_offset: 0,
						tmp: '<span class="tp-bullet-image"></span><span class="tp-bullet-title"></span>'
					},
					thumbnails: {
						container: "slider",
						rtl: !1,
						style: "",
						enable: !1,
						width: 100,
						height: 50,
						min_width: 100,
						wrapper_padding: 2,
						wrapper_color: "#f5f5f5",
						wrapper_opacity: 1,
						tmp: '<span class="tp-thumb-image"></span><span class="tp-thumb-title"></span>',
						visibleAmount: 5,
						hide_onmobile: !1,
						hide_onleave: !0,
						hide_delay: 200,
						hide_delay_mobile: 1200,
						hide_under: 0,
						hide_over: 9999,
						direction: "horizontal",
						span: !1,
						position: "inner",
						space: 2,
						h_align: "left",
						v_align: "center",
						h_offset: 20,
						v_offset: 0
					},
					tabs: {
						container: "slider",
						rtl: !1,
						style: "",
						enable: !1,
						width: 100,
						min_width: 100,
						height: 50,
						wrapper_padding: 10,
						wrapper_color: "#f5f5f5",
						wrapper_opacity: 1,
						tmp: '<span class="tp-tab-image"></span>',
						visibleAmount: 5,
						hide_onmobile: !1,
						hide_onleave: !0,
						hide_delay: 200,
						hide_delay_mobile: 1200,
						hide_under: 0,
						hide_over: 9999,
						direction: "horizontal",
						span: !1,
						space: 0,
						position: "inner",
						h_align: "left",
						v_align: "center",
						h_offset: 20,
						v_offset: 0
					}
				},
				extensions: "extensions/",
				extensions_suffix: ".min.js",
				debugMode: !1
			};
			return e = jQuery.extend(!0, {}, i, e), this.each(function () {
				var i = jQuery(this);
				e.minHeight = e.minHeight != undefined ? parseInt(e.minHeight, 0) : e.minHeight, e.scrolleffect.on = "on" === e.scrolleffect.fade || "on" === e.scrolleffect.scale || "on" === e.scrolleffect.blur || "on" === e.scrolleffect.grayscale, "hero" == e.sliderType && i.find(">ul>li").each(function (e) {
					e > 0 && jQuery(this).remove()
				}), e.jsFileLocation = e.jsFileLocation || getScriptLocation("themepunch.revolution.min.js"), e.jsFileLocation = e.jsFileLocation + e.extensions, e.scriptsneeded = getNeededScripts(e, i), e.curWinRange = 0, e.rtl = !0, e.navigation != undefined && e.navigation.touch != undefined && (e.navigation.touch.swipe_min_touches = e.navigation.touch.swipe_min_touches > 5 ? 1 : e.navigation.touch.swipe_min_touches), jQuery(this).on("scriptsloaded", function () {
					if (e.modulesfailing) return i.html('<div style="margin:auto;line-height:40px;font-size:14px;color:#fff;padding:15px;background:#e74c3c;margin:20px 0px;">!! Error at loading Slider Revolution 5.0 Extrensions.' + e.errorm + "</div>").show(), !1;
					_R.migration != undefined && (e = _R.migration(i, e)), punchgs.force3D = !0, "on" !== e.simplifyAll && punchgs.TweenLite.lagSmoothing(1e3, 16), prepareOptions(i, e), initSlider(i, e)
				}), i[0].opt = e, waitForScripts(i, e)
			})
		},
		getRSVersion: function (e) {
			if (!0 === e) return jQuery("body").data("tp_rs_version");
			var i = jQuery("body").data("tp_rs_version"),
				t = "";
			t += "---------------------------------------------------------\n", t += "    Currently Loaded Slider Revolution & SR Modules :\n", t += "---------------------------------------------------------\n";
			for (var a in i) t += i[a].alias + ": " + i[a].ver + "\n";
			return t += "---------------------------------------------------------\n"
		},
		revremoveslide: function (e) {
			return this.each(function () {
				var i = jQuery(this),
					t = i[0].opt;
				if (!(e < 0 || e > t.slideamount) && i != undefined && i.length > 0 && jQuery("body").find("#" + i.attr("id")).length > 0 && t && t.li.length > 0 && (e > 0 || e <= t.li.length)) {
					var a = jQuery(t.li[e]),
						n = a.data("index"),
						r = !1;
					t.slideamount = t.slideamount - 1, t.realslideamount = t.realslideamount - 1, removeNavWithLiref(".tp-bullet", n, t), removeNavWithLiref(".tp-tab", n, t), removeNavWithLiref(".tp-thumb", n, t), a.hasClass("active-revslide") && (r = !0), a.remove(), t.li = removeArray(t.li, e), t.carousel && t.carousel.slides && (t.carousel.slides = removeArray(t.carousel.slides, e)), t.thumbs = removeArray(t.thumbs, e), _R.updateNavIndexes && _R.updateNavIndexes(t), r && i.revnext(), punchgs.TweenLite.set(t.li, {
						minWidth: "99%"
					}), punchgs.TweenLite.set(t.li, {
						minWidth: "100%"
					})
				}
			})
		},
		revaddcallback: function (e) {
			return this.each(function () {
				this.opt && (this.opt.callBackArray === undefined && (this.opt.callBackArray = new Array), this.opt.callBackArray.push(e))
			})
		},
		revgetparallaxproc: function () {
			return jQuery(this)[0].opt.scrollproc
		},
		revdebugmode: function () {
			return this.each(function () {
				var e = jQuery(this);
				e[0].opt.debugMode = !0, containerResized(e, e[0].opt)
			})
		},
		revscroll: function (e) {
			return this.each(function () {
				var i = jQuery(this);
				jQuery("body,html").animate({
					scrollTop: i.offset().top + i.height() - e + "px"
				}, {
					duration: 400
				})
			})
		},
		revredraw: function (e) {
			return this.each(function () {
				var e = jQuery(this);
				containerResized(e, e[0].opt)
			})
		},
		revkill: function (e) {
			var i = this,
				t = jQuery(this);
			if (punchgs.TweenLite.killDelayedCallsTo(_R.showHideNavElements), t != undefined && t.length > 0 && jQuery("body").find("#" + t.attr("id")).length > 0) {
				t.data("conthover", 1), t.data("conthover-changed", 1), t.trigger("revolution.slide.onpause");
				var a = t.parent().find(".tp-bannertimer"),
					n = t[0].opt;
				n.tonpause = !0, t.trigger("stoptimer");
				r = "resize.revslider-" + t.attr("id");
				jQuery(window).unbind(r), punchgs.TweenLite.killTweensOf(t.find("*"), !1), punchgs.TweenLite.killTweensOf(t, !1), t.unbind("hover, mouseover, mouseenter,mouseleave, resize");
				var r = "resize.revslider-" + t.attr("id");
				jQuery(window).off(r), t.find("*").each(function () {
					var e = jQuery(this);
					e.unbind("on, hover, mouseenter,mouseleave,mouseover, resize,restarttimer, stoptimer"), e.off("on, hover, mouseenter,mouseleave,mouseover, resize"), e.data("mySplitText", null), e.data("ctl", null), e.data("tween") != undefined && e.data("tween").kill(), e.data("kenburn") != undefined && e.data("kenburn").kill(), e.data("timeline_out") != undefined && e.data("timeline_out").kill(), e.data("timeline") != undefined && e.data("timeline").kill(), e.remove(), e.empty(), e = null
				}), punchgs.TweenLite.killTweensOf(t.find("*"), !1), punchgs.TweenLite.killTweensOf(t, !1), a.remove();
				try {
					t.closest(".forcefullwidth_wrapper_tp_banner").remove()
				} catch (e) {}
				try {
					t.closest(".rev_slider_wrapper").remove()
				} catch (e) {}
				try {
					t.remove()
				} catch (e) {}
				return t.empty(), t.html(), t = null, n = null, delete i.c, delete i.opt, delete i.container, !0
			}
			return !1
		},
		revpause: function () {
			return this.each(function () {
				var e = jQuery(this);
				e != undefined && e.length > 0 && jQuery("body").find("#" + e.attr("id")).length > 0 && (e.data("conthover", 1), e.data("conthover-changed", 1), e.trigger("revolution.slide.onpause"), e[0].opt.tonpause = !0, e.trigger("stoptimer"))
			})
		},
		revresume: function () {
			return this.each(function () {
				var e = jQuery(this);
				e != undefined && e.length > 0 && jQuery("body").find("#" + e.attr("id")).length > 0 && (e.data("conthover", 0), e.data("conthover-changed", 1), e.trigger("revolution.slide.onresume"), e[0].opt.tonpause = !1, e.trigger("starttimer"))
			})
		},
		revstart: function () {
			var e = jQuery(this);
			if (e != undefined && e.length > 0 && jQuery("body").find("#" + e.attr("id")).length > 0 && e[0].opt !== undefined) return e[0].opt.sliderisrunning ? (console.log("Slider Is Running Already"), !1) : (runSlider(e, e[0].opt), !0)
		},
		revnext: function () {
			return this.each(function () {
				var e = jQuery(this);
				e != undefined && e.length > 0 && jQuery("body").find("#" + e.attr("id")).length > 0 && _R.callingNewSlide(e, 1)
			})
		},
		revprev: function () {
			return this.each(function () {
				var e = jQuery(this);
				e != undefined && e.length > 0 && jQuery("body").find("#" + e.attr("id")).length > 0 && _R.callingNewSlide(e, -1)
			})
		},
		revmaxslide: function () {
			return jQuery(this).find(".tp-revslider-mainul >li").length
		},
		revcurrentslide: function () {
			var e = jQuery(this);
			if (e != undefined && e.length > 0 && jQuery("body").find("#" + e.attr("id")).length > 0) return parseInt(e[0].opt.act, 0) + 1
		},
		revlastslide: function () {
			return jQuery(this).find(".tp-revslider-mainul >li").length
		},
		revshowslide: function (e) {
			return this.each(function () {
				var i = jQuery(this);
				i != undefined && i.length > 0 && jQuery("body").find("#" + i.attr("id")).length > 0 && _R.callingNewSlide(i, "to" + (e - 1))
			})
		},
		revcallslidewithid: function (e) {
			return this.each(function () {
				var i = jQuery(this);
				i != undefined && i.length > 0 && jQuery("body").find("#" + i.attr("id")).length > 0 && _R.callingNewSlide(i, e)
			})
		}
	});
	var _R = jQuery.fn.revolution;
	jQuery.extend(!0, _R, {
		getversion: function () {
			return version
		},
		compare_version: function (e) {
			var i = jQuery("body").data("tp_rs_version");
			return (i = i === undefined ? new Object : i).Core === undefined && (i.Core = new Object, i.Core.alias = "Slider Revolution Core", i.Core.name = "jquery.themepunch.revolution.min.js", i.Core.ver = _R.getversion().core), "stop" != e.check && (_R.getversion().core < e.min_core ? (e.check === undefined && (console.log("%cSlider Revolution Warning (Core:" + _R.getversion().core + ")", "color:#c0392b;font-weight:bold;"), console.log("%c     Core is older than expected (" + e.min_core + ") from " + e.alias, "color:#333"), console.log("%c     Please update Slider Revolution to the latest version.", "color:#333"), console.log("%c     It might be required to purge and clear Server/Client side Caches.", "color:#333")), e.check = "stop") : _R.getversion()[e.name] != undefined && e.version < _R.getversion()[e.name] && (e.check === undefined && (console.log("%cSlider Revolution Warning (Core:" + _R.getversion().core + ")", "color:#c0392b;font-weight:bold;"), console.log("%c     " + e.alias + " (" + e.version + ") is older than requiered (" + _R.getversion()[e.name] + ")", "color:#333"), console.log("%c     Please update Slider Revolution to the latest version.", "color:#333"), console.log("%c     It might be required to purge and clear Server/Client side Caches.", "color:#333")), e.check = "stop")), i[e.alias] === undefined && (i[e.alias] = new Object, i[e.alias].alias = e.alias, i[e.alias].ver = e.version, i[e.alias].name = e.name), jQuery("body").data("tp_rs_version", i), e
		},
		currentSlideIndex: function (e) {
			var i = e.c.find(".active-revslide").index();
			return i = -1 == i ? 0 : i
		},
		simp: function (e, i, t) {
			var a = Math.abs(e) - Math.floor(Math.abs(e / i)) * i;
			return t ? a : e < 0 ? -1 * a : a
		},
		iOSVersion: function () {
			var e = !1;
			return navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/iPad/i) ? navigator.userAgent.match(/OS 4_\d like Mac OS X/i) && (e = !0) : e = !1, e
		},
		isIE: function (e, i) {
			var t = jQuery('<div style="display:none;"/>').appendTo(jQuery("body"));
			t.html("\x3c!--[if " + (i || "") + " IE " + (e || "") + "]><a>&nbsp;</a><![endif]--\x3e");
			var a = t.find("a").length;
			return t.remove(), a
		},
		is_mobile: function () {
			var e = ["android", "webos", "iphone", "ipad", "blackberry", "Android", "webos", , "iPod", "iPhone", "iPad", "Blackberry", "BlackBerry"],
				i = !1;
			for (var t in e) navigator.userAgent.split(e[t]).length > 1 && (i = !0);
			return i
		},
		is_android: function () {
			var e = ["android", "Android"],
				i = !1;
			for (var t in e) navigator.userAgent.split(e[t]).length > 1 && (i = !0);
			return i
		},
		callBackHandling: function (e, i, t) {
			try {
				e.callBackArray && jQuery.each(e.callBackArray, function (e, a) {
					a && a.inmodule && a.inmodule === i && a.atposition && a.atposition === t && a.callback && a.callback.call()
				})
			} catch (e) {
				console.log("Call Back Failed")
			}
		},
		get_browser: function () {
			var e, i = navigator.appName,
				t = navigator.userAgent,
				a = t.match(/(opera|chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i);
			return a && null != (e = t.match(/version\/([\.\d]+)/i)) && (a[2] = e[1]), (a = a ? [a[1], a[2]] : [i, navigator.appVersion, "-?"])[0]
		},
		get_browser_version: function () {
			var e, i = navigator.appName,
				t = navigator.userAgent,
				a = t.match(/(opera|chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i);
			return a && null != (e = t.match(/version\/([\.\d]+)/i)) && (a[2] = e[1]), (a = a ? [a[1], a[2]] : [i, navigator.appVersion, "-?"])[1]
		},
		getHorizontalOffset: function (e, i) {
			var t = gWiderOut(e, ".outer-left"),
				a = gWiderOut(e, ".outer-right");
			switch (i) {
				case "left":
					return t;
				case "right":
					return a;
				case "both":
					return t + a
			}
		},
		callingNewSlide: function (e, i) {
			var t = e.find(".next-revslide").length > 0 ? e.find(".next-revslide").index() : e.find(".processing-revslide").length > 0 ? e.find(".processing-revslide").index() : e.find(".active-revslide").index(),
				a = 0,
				n = e[0].opt;
			e.find(".next-revslide").removeClass("next-revslide"), e.find(".active-revslide").hasClass("tp-invisible-slide") && (t = n.last_shown_slide), i && jQuery.isNumeric(i) || i.match(/to/g) ? (a = 1 === i || -1 === i ? (a = t + i) < 0 ? n.slideamount - 1 : a >= n.slideamount ? 0 : a : (i = jQuery.isNumeric(i) ? i : parseInt(i.split("to")[1], 0)) < 0 ? 0 : i > n.slideamount - 1 ? n.slideamount - 1 : i, e.find(".tp-revslider-slidesli:eq(" + a + ")").addClass("next-revslide")) : i && e.find(".tp-revslider-slidesli").each(function () {
				var e = jQuery(this);
				e.data("index") === i && e.addClass("next-revslide")
			}), a = e.find(".next-revslide").index(), e.trigger("revolution.nextslide.waiting"), t === a && t === n.last_shown_slide || a !== t && -1 != a ? swapSlide(e) : e.find(".next-revslide").removeClass("next-revslide")
		},
		slotSize: function (e, i) {
			i.slotw = Math.ceil(i.width / i.slots), "fullscreen" == i.sliderLayout ? i.sloth = Math.ceil(jQuery(window).height() / i.slots) : i.sloth = Math.ceil(i.height / i.slots), "on" == i.autoHeight && e !== undefined && "" !== e && (i.sloth = Math.ceil(e.height() / i.slots))
		},
		setSize: function (e) {
			var i = (e.top_outer || 0) + (e.bottom_outer || 0),
				t = parseInt(e.carousel.padding_top || 0, 0),
				a = parseInt(e.carousel.padding_bottom || 0, 0),
				n = e.gridheight[e.curWinRange],
				r = 0,
				o = -1 === e.nextSlide || e.nextSlide === undefined ? 0 : e.nextSlide;
			if (e.paddings = e.paddings === undefined ? {
					top: parseInt(e.c.parent().css("paddingTop"), 0) || 0,
					bottom: parseInt(e.c.parent().css("paddingBottom"), 0) || 0
				} : e.paddings, e.rowzones && e.rowzones.length > 0)
				for (var s = 0; s < e.rowzones[o].length; s++) r += e.rowzones[o][s][0].offsetHeight;
			if (n = n < e.minHeight ? e.minHeight : n, n = n < r ? r : n, "fullwidth" == e.sliderLayout && "off" == e.autoHeight && punchgs.TweenLite.set(e.c, {
					maxHeight: n + "px"
				}), e.c.css({
					marginTop: t,
					marginBottom: a
				}), e.width = e.ul.width(), e.height = e.ul.height(), setScale(e), e.height = Math.round(e.gridheight[e.curWinRange] * (e.width / e.gridwidth[e.curWinRange])), e.height > e.gridheight[e.curWinRange] && "on" != e.autoHeight && (e.height = e.gridheight[e.curWinRange]), "fullscreen" == e.sliderLayout || e.infullscreenmode) {
				e.height = e.bw * e.gridheight[e.curWinRange];
				e.c.parent().width();
				var l = jQuery(window).height();
				if (e.fullScreenOffsetContainer != undefined) {
					try {
						var d = e.fullScreenOffsetContainer.split(",");
						d && jQuery.each(d, function (e, i) {
							l = jQuery(i).length > 0 ? l - jQuery(i).outerHeight(!0) : l
						})
					} catch (e) {}
					try {
						e.fullScreenOffset.split("%").length > 1 && e.fullScreenOffset != undefined && e.fullScreenOffset.length > 0 ? l -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : e.fullScreenOffset != undefined && e.fullScreenOffset.length > 0 && (l -= parseInt(e.fullScreenOffset, 0))
					} catch (e) {}
				}
				l = l < e.minHeight ? e.minHeight : l, l -= i, e.c.parent().height(l), e.c.closest(".rev_slider_wrapper").height(l), e.c.css({
					height: "100%"
				}), e.height = l, e.minHeight != undefined && e.height < e.minHeight && (e.height = e.minHeight), e.height = parseInt(r, 0) > parseInt(e.height, 0) ? r : e.height
			} else e.minHeight != undefined && e.height < e.minHeight && (e.height = e.minHeight), e.height = parseInt(r, 0) > parseInt(e.height, 0) ? r : e.height, e.c.height(e.height);
			var u = {
				height: t + a + i + e.height + e.paddings.top + e.paddings.bottom
			};
			e.c.closest(".forcefullwidth_wrapper_tp_banner").find(".tp-fullwidth-forcer").css(u), e.c.closest(".rev_slider_wrapper").css(u), setScale(e)
		},
		enterInViewPort: function (e) {
			e.waitForCountDown && (countDown(e.c, e), e.waitForCountDown = !1), e.waitForFirstSlide && (swapSlide(e.c), e.waitForFirstSlide = !1, setTimeout(function () {
				e.c.removeClass("tp-waitforfirststart")
			}, 500)), "playing" != e.sliderlaststatus && e.sliderlaststatus != undefined || e.c.trigger("starttimer"), e.lastplayedvideos != undefined && e.lastplayedvideos.length > 0 && jQuery.each(e.lastplayedvideos, function (i, t) {
				_R.playVideo(t, e)
			})
		},
		leaveViewPort: function (e) {
			e.sliderlaststatus = e.sliderstatus, e.c.trigger("stoptimer"), e.playingvideos != undefined && e.playingvideos.length > 0 && (e.lastplayedvideos = jQuery.extend(!0, [], e.playingvideos), e.playingvideos && jQuery.each(e.playingvideos, function (i, t) {
				e.leaveViewPortBasedStop = !0, _R.stopVideo && _R.stopVideo(t, e)
			}))
		},
		unToggleState: function (e) {
			e != undefined && e.length > 0 && jQuery.each(e, function (e, i) {
				i.removeClass("rs-toggle-content-active")
			})
		},
		toggleState: function (e) {
			e != undefined && e.length > 0 && jQuery.each(e, function (e, i) {
				i.addClass("rs-toggle-content-active")
			})
		},
		swaptoggleState: function (e) {
			e != undefined && e.length > 0 && jQuery.each(e, function (e, i) {
				jQuery(i).hasClass("rs-toggle-content-active") ? jQuery(i).removeClass("rs-toggle-content-active") : jQuery(i).addClass("rs-toggle-content-active")
			})
		},
		lastToggleState: function (e) {
			var i = 0;
			return e != undefined && e.length > 0 && jQuery.each(e, function (e, t) {
				i = t.hasClass("rs-toggle-content-active")
			}), i
		}
	});
	var _ISM = _R.is_mobile(),
		_ANDROID = _R.is_android(),
		checkIDS = function (e, i) {
			if (e.anyid = e.anyid === undefined ? [] : e.anyid, -1 != jQuery.inArray(i.attr("id"), e.anyid)) {
				var t = i.attr("id") + "_" + Math.round(9999 * Math.random());
				i.attr("id", t)
			}
			e.anyid.push(i.attr("id"))
		},
		removeArray = function (e, i) {
			var t = [];
			return jQuery.each(e, function (e, a) {
				e != i && t.push(a)
			}), t
		},
		removeNavWithLiref = function (e, i, t) {
			t.c.find(e).each(function () {
				var e = jQuery(this);
				e.data("liref") === i && e.remove()
			})
		},
		lAjax = function (e, i) {
			return !jQuery("body").data(e) && (i.filesystem ? (i.errorm === undefined && (i.errorm = "<br>Local Filesystem Detected !<br>Put this to your header:"), console.warn("Local Filesystem detected !"), i.errorm = i.errorm + '<br>&lt;script type="text/javascript" src="' + i.jsFileLocation + e + i.extensions_suffix + '"&gt;&lt;/script&gt;', console.warn(i.jsFileLocation + e + i.extensions_suffix + " could not be loaded !"), console.warn("Please use a local Server or work online or make sure that you load all needed Libraries manually in your Document."), console.log(" "), i.modulesfailing = !0, !1) : (jQuery.ajax({
				url: i.jsFileLocation + e + i.extensions_suffix + "?version=" + version.core,
				dataType: "script",
				cache: !0,
				error: function (t) {
					console.warn("Slider Revolution 5.0 Error !"), console.error("Failure at Loading:" + e + i.extensions_suffix + " on Path:" + i.jsFileLocation), console.info(t)
				}
			}), void jQuery("body").data(e, !0)))
		},
		getNeededScripts = function (e, i) {
			var t = new Object,
				a = e.navigation;
			return t.kenburns = !1, t.parallax = !1, t.carousel = !1, t.navigation = !1, t.videos = !1, t.actions = !1, t.layeranim = !1, t.migration = !1, i.data("version") && i.data("version").toString().match(/5./gi) ? (i.find("img").each(function () {
				"on" == jQuery(this).data("kenburns") && (t.kenburns = !0)
			}), ("carousel" == e.sliderType || "on" == a.keyboardNavigation || "on" == a.mouseScrollNavigation || "on" == a.touch.touchenabled || a.arrows.enable || a.bullets.enable || a.thumbnails.enable || a.tabs.enable) && (t.navigation = !0), i.find(".tp-caption, .tp-static-layer, .rs-background-video-layer").each(function () {
				var e = jQuery(this);
				(e.data("ytid") != undefined || e.find("iframe").length > 0 && e.find("iframe").attr("src").toLowerCase().indexOf("youtube") > 0) && (t.videos = !0), (e.data("vimeoid") != undefined || e.find("iframe").length > 0 && e.find("iframe").attr("src").toLowerCase().indexOf("vimeo") > 0) && (t.videos = !0), e.data("actions") !== undefined && (t.actions = !0), t.layeranim = !0
			}), i.find("li").each(function () {
				jQuery(this).data("link") && jQuery(this).data("link") != undefined && (t.layeranim = !0, t.actions = !0)
			}), !t.videos && (i.find(".rs-background-video-layer").length > 0 || i.find(".tp-videolayer").length > 0 || i.find(".tp-audiolayer").length > 0 || i.find("iframe").length > 0 || i.find("video").length > 0) && (t.videos = !0), "carousel" == e.sliderType && (t.carousel = !0), ("off" !== e.parallax.type || e.viewPort.enable || "true" == e.viewPort.enable || "true" === e.scrolleffect.on || e.scrolleffect.on) && (t.parallax = !0)) : (t.kenburns = !0, t.parallax = !0, t.carousel = !1, t.navigation = !0, t.videos = !0, t.actions = !0, t.layeranim = !0, t.migration = !0), "hero" == e.sliderType && (t.carousel = !1, t.navigation = !1), window.location.href.match(/file:/gi) && (t.filesystem = !0, e.filesystem = !0), t.videos && void 0 === _R.isVideoPlaying && lAjax("revolution.extension.video", e), t.carousel && void 0 === _R.prepareCarousel && lAjax("revolution.extension.carousel", e), t.carousel || void 0 !== _R.animateSlide || lAjax("revolution.extension.slideanims", e), t.actions && void 0 === _R.checkActions && lAjax("revolution.extension.actions", e), t.layeranim && void 0 === _R.handleStaticLayers && lAjax("revolution.extension.layeranimation", e), t.kenburns && void 0 === _R.stopKenBurn && lAjax("revolution.extension.kenburn", e), t.navigation && void 0 === _R.createNavigation && lAjax("revolution.extension.navigation", e), t.migration && void 0 === _R.migration && lAjax("revolution.extension.migration", e), t.parallax && void 0 === _R.checkForParallax && lAjax("revolution.extension.parallax", e), e.addons != undefined && e.addons.length > 0 && jQuery.each(e.addons, function (i, t) {
				"object" == typeof t && t.fileprefix != undefined && lAjax(t.fileprefix, e)
			}), t
		},
		waitForScripts = function (e, i) {
			var t = !0,
				a = i.scriptsneeded;
			i.addons != undefined && i.addons.length > 0 && jQuery.each(i.addons, function (e, i) {
				"object" == typeof i && i.init != undefined && _R[i.init] === undefined && (t = !1)
			}), a.filesystem || "undefined" != typeof punchgs && t && (!a.kenburns || a.kenburns && void 0 !== _R.stopKenBurn) && (!a.navigation || a.navigation && void 0 !== _R.createNavigation) && (!a.carousel || a.carousel && void 0 !== _R.prepareCarousel) && (!a.videos || a.videos && void 0 !== _R.resetVideo) && (!a.actions || a.actions && void 0 !== _R.checkActions) && (!a.layeranim || a.layeranim && void 0 !== _R.handleStaticLayers) && (!a.migration || a.migration && void 0 !== _R.migration) && (!a.parallax || a.parallax && void 0 !== _R.checkForParallax) && (a.carousel || !a.carousel && void 0 !== _R.animateSlide) ? e.trigger("scriptsloaded") : setTimeout(function () {
				waitForScripts(e, i)
			}, 50)
		},
		getScriptLocation = function (e) {
			var i = new RegExp("themepunch.revolution.min.js", "gi"),
				t = "";
			return jQuery("script").each(function () {
				var e = jQuery(this).attr("src");
				e && e.match(i) && (t = e)
			}), t = t.replace("jquery.themepunch.revolution.min.js", ""), t = t.replace("jquery.themepunch.revolution.js", ""), t = t.split("?")[0]
		},
		setCurWinRange = function (e, i) {
			var t = 9999,
				a = 0,
				n = 0,
				r = 0,
				o = jQuery(window).width(),
				s = i && 9999 == e.responsiveLevels ? e.visibilityLevels : e.responsiveLevels;
			s && s.length && jQuery.each(s, function (e, i) {
				o < i && (0 == a || a > i) && (t = i, r = e, a = i), o > i && a < i && (a = i, n = e)
			}), a < t && (r = n), i ? e.forcedWinRange = r : e.curWinRange = r
		},
		prepareOptions = function (e, i) {
			i.carousel.maxVisibleItems = i.carousel.maxVisibleItems < 1 ? 999 : i.carousel.maxVisibleItems, i.carousel.vertical_align = "top" === i.carousel.vertical_align ? "0%" : "bottom" === i.carousel.vertical_align ? "100%" : "50%"
		},
		gWiderOut = function (e, i) {
			var t = 0;
			return e.find(i).each(function () {
				var e = jQuery(this);
				!e.hasClass("tp-forcenotvisible") && t < e.outerWidth() && (t = e.outerWidth())
			}), t
		},
		initSlider = function (container, opt) {
			if (container == undefined) return !1;
			container.data("aimg") != undefined && ("enabled" == container.data("aie8") && _R.isIE(8) || "enabled" == container.data("amobile") && _ISM) && container.html('<img class="tp-slider-alternative-image" src="' + container.data("aimg") + '">'), container.find(">ul").addClass("tp-revslider-mainul"), opt.c = container, opt.ul = container.find(".tp-revslider-mainul"), opt.ul.find(">li").each(function (e) {
				var i = jQuery(this);
				"on" == i.data("hideslideonmobile") && _ISM && i.remove(), (i.data("invisible") || !0 === i.data("invisible")) && (i.addClass("tp-invisible-slide"), i.appendTo(opt.ul))
			}), opt.addons != undefined && opt.addons.length > 0 && jQuery.each(opt.addons, function (i, obj) {
				"object" == typeof obj && obj.init != undefined && _R[obj.init](eval(obj.params))
			}), opt.cid = container.attr("id"), opt.ul.css({
				visibility: "visible"
			}), opt.slideamount = opt.ul.find(">li").not(".tp-invisible-slide").length, opt.realslideamount = opt.ul.find(">li").length, opt.slayers = container.find(".tp-static-layers"), opt.slayers.data("index", "staticlayers"), 1 != opt.waitForInit && (container[0].opt = opt, runSlider(container, opt))
		},
		onFullScreenChange = function () {
			jQuery("body").data("rs-fullScreenMode", !jQuery("body").data("rs-fullScreenMode")), jQuery("body").data("rs-fullScreenMode") && setTimeout(function () {
				jQuery(window).trigger("resize")
			}, 200)
		},
		runSlider = function (e, i) {
			if (i.sliderisrunning = !0, i.ul.find(">li").each(function (e) {
					jQuery(this).data("originalindex", e)
				}), i.allli = i.ul.find(">li"), jQuery.each(i.allli, function (e, i) {
					(i = jQuery(i)).data("origindex", i.index())
				}), i.li = i.ul.find(">li").not(".tp-invisible-slide"), "on" == i.shuffle) {
				var t = new Object,
					a = i.ul.find(">li:first-child");
				t.fstransition = a.data("fstransition"), t.fsmasterspeed = a.data("fsmasterspeed"), t.fsslotamount = a.data("fsslotamount");
				for (var n = 0; n < i.slideamount; n++) {
					var r = Math.round(Math.random() * i.slideamount);
					i.ul.find(">li:eq(" + r + ")").prependTo(i.ul)
				}
				var o = i.ul.find(">li:first-child");
				o.data("fstransition", t.fstransition), o.data("fsmasterspeed", t.fsmasterspeed), o.data("fsslotamount", t.fsslotamount), i.allli = i.ul.find(">li"), i.li = i.ul.find(">li").not(".tp-invisible-slide")
			}
			if (i.inli = i.ul.find(">li.tp-invisible-slide"), i.thumbs = new Array, i.slots = 4, i.act = -1, i.firststart = 1, i.loadqueue = new Array, i.syncload = 0, i.conw = e.width(), i.conh = e.height(), i.responsiveLevels.length > 1 ? i.responsiveLevels[0] = 9999 : i.responsiveLevels = 9999, jQuery.each(i.allli, function (e, t) {
					var a = (t = jQuery(t)).find(".rev-slidebg") || t.find("img").first(),
						n = 0;
					t.addClass("tp-revslider-slidesli"), t.data("index") === undefined && t.data("index", "rs-" + Math.round(999999 * Math.random()));
					var r = new Object;
					r.params = new Array, r.id = t.data("index"), r.src = t.data("thumb") !== undefined ? t.data("thumb") : a.data("lazyload") !== undefined ? a.data("lazyload") : a.attr("src"), t.data("title") !== undefined && r.params.push({
						from: RegExp("\\{\\{title\\}\\}", "g"),
						to: t.data("title")
					}), t.data("description") !== undefined && r.params.push({
						from: RegExp("\\{\\{description\\}\\}", "g"),
						to: t.data("description")
					});
					for (n = 1; n <= 10; n++) t.data("param" + n) !== undefined && r.params.push({
						from: RegExp("\\{\\{param" + n + "\\}\\}", "g"),
						to: t.data("param" + n)
					});
					if (i.thumbs.push(r), t.data("link") != undefined) {
						var o = t.data("link"),
							s = t.data("target") || "_self",
							l = "back" === t.data("slideindex") ? 0 : 60,
							d = t.data("linktoslide"),
							u = d;
						d != undefined && "next" != d && "prev" != d && i.allli.each(function () {
							var e = jQuery(this);
							e.data("origindex") + 1 == u && (d = e.data("index"))
						}), "slide" != o && (d = "no");
						var c = '<div class="tp-caption slidelink" style="cursor:pointer;width:100%;height:100%;z-index:' + l + ';" data-x="center" data-y="center" data-basealign="slide" ',
							p = "scroll_under" === d ? '[{"event":"click","action":"scrollbelow","offset":"100px","delay":"0"}]' : "prev" === d ? '[{"event":"click","action":"jumptoslide","slide":"prev","delay":"0.2"}]' : "next" === d ? '[{"event":"click","action":"jumptoslide","slide":"next","delay":"0.2"}]' : '[{"event":"click","action":"jumptoslide","slide":"' + d + '","delay":"0.2"}]',
							f = ' data-frames=\'[{"delay":0,"speed":100,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]\'';
						c = "no" == d ? c + f + " >" : c + "data-actions='" + p + "'" + f + " >", c += '<a style="width:100%;height:100%;display:block"', c = "slide" != o ? c + ' target="' + s + '" href="' + o + '"' : c, c += '><span style="width:100%;height:100%;display:block"></span></a></div>', t.append(c)
					}
				}), i.rle = i.responsiveLevels.length || 1, i.gridwidth = cArray(i.gridwidth, i.rle), i.gridheight = cArray(i.gridheight, i.rle), "on" == i.simplifyAll && (_R.isIE(8) || _R.iOSVersion()) && (e.find(".tp-caption").each(function () {
					var e = jQuery(this);
					e.removeClass("customin customout").addClass("fadein fadeout"), e.data("splitin", ""), e.data("speed", 400)
				}), i.allli.each(function () {
					var e = jQuery(this);
					e.data("transition", "fade"), e.data("masterspeed", 500), e.data("slotamount", 1), (e.find(".rev-slidebg") || e.find(">img").first()).data("kenburns", "off")
				})), i.desktop = !navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|BB10|mobi|tablet|opera mini|nexus 7)/i), i.autoHeight = "fullscreen" == i.sliderLayout ? "on" : i.autoHeight, "fullwidth" == i.sliderLayout && "off" == i.autoHeight && e.css({
					maxHeight: i.gridheight[i.curWinRange] + "px"
				}), "auto" != i.sliderLayout && 0 == e.closest(".forcefullwidth_wrapper_tp_banner").length && ("fullscreen" !== i.sliderLayout || "on" != i.fullScreenAutoWidth)) {
				var s = e.parent(),
					l = s.css("marginBottom"),
					d = s.css("marginTop"),
					u = e.attr("id") + "_forcefullwidth";
				l = l === undefined ? 0 : l, d = d === undefined ? 0 : d, s.wrap('<div class="forcefullwidth_wrapper_tp_banner" id="' + u + '" style="position:relative;width:100%;height:auto;margin-top:' + d + ";margin-bottom:" + l + '"></div>'), e.closest(".forcefullwidth_wrapper_tp_banner").append('<div class="tp-fullwidth-forcer" style="width:100%;height:' + e.height() + 'px"></div>'), e.parent().css({
					marginTop: "0px",
					marginBottom: "0px"
				}), e.parent().css({
					position: "absolute"
				})
			}
			if (i.shadow !== undefined && i.shadow > 0 && (e.parent().addClass("tp-shadow" + i.shadow), e.parent().append('<div class="tp-shadowcover"></div>'), e.parent().find(".tp-shadowcover").css({
					backgroundColor: e.parent().css("backgroundColor"),
					backgroundImage: e.parent().css("backgroundImage")
				})), setCurWinRange(i), setCurWinRange(i, !0), !e.hasClass("revslider-initialised")) {
				e.addClass("revslider-initialised"), e.addClass("tp-simpleresponsive"), e.attr("id") == undefined && e.attr("id", "revslider-" + Math.round(1e3 * Math.random() + 5)), checkIDS(i, e), i.firefox13 = !1, i.ie = !jQuery.support.opacity, i.ie9 = 9 == document.documentMode, i.origcd = i.delay;
				var c = jQuery.fn.jquery.split("."),
					p = parseFloat(c[0]),
					f = parseFloat(c[1]);
				parseFloat(c[2] || "0");
				1 == p && f < 7 && e.html('<div style="text-align:center; padding:40px 0px; font-size:20px; color:#992222;"> The Current Version of jQuery:' + c + " <br>Please update your jQuery Version to min. 1.7 in Case you wish to use the Revolution Slider Plugin</div>"), p > 1 && (i.ie = !1);
				var h = new Object;
				h.addedyt = 0, h.addedvim = 0, h.addedvid = 0, i.scrolleffect.on && (i.scrolleffect.layers = new Array), e.find(".tp-caption, .rs-background-video-layer").each(function (e) {
					var t = jQuery(this),
						a = t.data(),
						n = a.autoplayonlyfirsttime,
						r = a.autoplay,
						o = a.videomp4 !== undefined || a.videowebm !== undefined || a.videoogv !== undefined,
						s = t.hasClass("tp-audiolayer"),
						l = a.videoloop,
						d = !0,
						u = !1;
					a.startclasses = t.attr("class"), a.isparallaxlayer = a.startclasses.indexOf("rs-parallax") >= 0, t.hasClass("tp-static-layer") && _R.handleStaticLayers && (_R.handleStaticLayers(t, i), i.scrolleffect.on && ("on" === i.scrolleffect.on_parallax_static_layers && a.isparallaxlayer || "on" === i.scrolleffect.on_static_layers && !a.isparallaxlayer) && (u = !0), d = !1);
					var c = t.data("noposteronmobile") || t.data("noPosterOnMobile") || t.data("posteronmobile") || t.data("posterOnMobile") || t.data("posterOnMObile");
					t.data("noposteronmobile", c);
					var p = 0;
					if (t.find("iframe").each(function () {
							punchgs.TweenLite.set(jQuery(this), {
								autoAlpha: 0
							}), p++
						}), p > 0 && t.data("iframes", !0), t.hasClass("tp-caption")) {
						var f = t.hasClass("slidelink") ? "width:100% !important;height:100% !important;" : "",
							g = t.data(),
							v = "",
							m = g.type,
							y = "row" === m || "column" === m ? "relative" : "absolute",
							w = "";
						"row" === m ? (t.addClass("rev_row").removeClass("tp-resizeme"), w = "rev_row_wrap") : "column" === m ? (v = g.verticalalign === undefined ? " vertical-align:bottom;" : " vertical-align:" + g.verticalalign + ";", w = "rev_column", t.addClass("rev_column_inner").removeClass("tp-resizeme"), t.data("width", "auto"), punchgs.TweenLite.set(t, {
							width: "auto"
						})) : "group" === m && t.removeClass("tp-resizeme");
						var b = "",
							_ = "";
						"row" !== m && "group" !== m && "column" !== m ? (b = "display:" + t.css("display") + ";", t.closest(".rev_column").length > 0 ? (t.addClass("rev_layer_in_column"), d = !1) : t.closest(".rev_group").length > 0 && (t.addClass("rev_layer_in_group"), d = !1)) : "column" === m && (d = !1), g.wrapper_class !== undefined && (w = w + " " + g.wrapper_class), g.wrapper_id !== undefined && (_ = 'id="' + g.wrapper_id + '"'), t.wrap("<div " + _ + ' class="tp-parallax-wrap ' + w + '" style="' + v + " " + f + "position:" + y + ";" + b + ';visibility:hidden"><div class="tp-loop-wrap" style="' + f + "position:" + y + ";" + b + ';"><div class="tp-mask-wrap" style="' + f + "position:" + y + ";" + b + ';" ></div></div></div>'), d && i.scrolleffect.on && ("on" === i.scrolleffect.on_parallax_layers && a.isparallaxlayer || "on" === i.scrolleffect.on_layers && !a.isparallaxlayer) && i.scrolleffect.layers.push(t.parent()), u && i.scrolleffect.layers.push(t.parent()), "column" === m && (t.append('<div class="rev_column_bg rev_column_bg_man_sized" style="visibility:hidden"></div>'), t.closest(".tp-parallax-wrap").append('<div class="rev_column_bg rev_column_bg_auto_sized"></div>'));
						var x = ["pendulum", "rotate", "slideloop", "pulse", "wave"],
							j = t.closest(".tp-loop-wrap");
						jQuery.each(x, function (e, i) {
							var a = t.find(".rs-" + i),
								n = a.data() || "";
							"" != n && (j.data(n), j.addClass("rs-" + i), a.children(0).unwrap(), t.data("loopanimation", "on"))
						}), t.attr("id") === undefined && t.attr("id", "layer-" + Math.round(999999999 * Math.random())), checkIDS(i, t), punchgs.TweenLite.set(t, {
							visibility: "hidden"
						})
					}
					var R = t.data("actions");
					R !== undefined && _R.checkActions(t, i, R), checkHoverDependencies(t, i), _R.checkVideoApis && (h = _R.checkVideoApis(t, i, h)), !_ISM || i.fallbacks.allowHTML5AutoPlayOnAndroid && o || (1 != n && "true" != n || (a.autoplayonlyfirsttime = !1, n = !1), 1 != r && "true" != r && "on" != r && "1sttime" != r || (a.autoplay = "off", r = "off")), s || 1 != n && "true" != n && "1sttime" != r || "loopandnoslidestop" == l || t.closest("li.tp-revslider-slidesli").addClass("rs-pause-timer-once"), s || 1 != r && "true" != r && "on" != r && "no1sttime" != r || "loopandnoslidestop" == l || t.closest("li.tp-revslider-slidesli").addClass("rs-pause-timer-always")
				}), e[0].addEventListener("mouseenter", function () {
					e.trigger("tp-mouseenter"), i.overcontainer = !0
				}, {
					passive: !0
				}), e[0].addEventListener("mouseover", function () {
					e.trigger("tp-mouseover"), i.overcontainer = !0
				}, {
					passive: !0
				}), e[0].addEventListener("mouseleave", function () {
					e.trigger("tp-mouseleft"), i.overcontainer = !1
				}, {
					passive: !0
				}), e.find(".tp-caption video").each(function (e) {
					var i = jQuery(this);
					i.removeClass("video-js vjs-default-skin"), i.attr("preload", ""), i.css({
						display: "none"
					})
				}), "standard" !== i.sliderType && (i.lazyType = "all"), loadImages(e.find(".tp-static-layers"), i, 0, !0), waitForCurrentImages(e.find(".tp-static-layers"), i, function () {
					e.find(".tp-static-layers img").each(function () {
						var e = jQuery(this),
							t = e.data("lazyload") != undefined ? e.data("lazyload") : e.attr("src"),
							a = getLoadObj(i, t);
						e.attr("src", a.src)
					})
				}), i.rowzones = [], i.allli.each(function (e) {
					var t = jQuery(this);
					i.rowzones[e] = [], t.find(".rev_row_zone").each(function () {
						i.rowzones[e].push(jQuery(this))
					}), "all" != i.lazyType && ("smart" != i.lazyType || 0 != e && 1 != e && e != i.slideamount && e != i.slideamount - 1) || (loadImages(t, i, e), waitForCurrentImages(t, i, function () {}))
				});
				var g = getUrlVars("#")[0];
				if (g.length < 9 && g.split("slide").length > 1) {
					var v = parseInt(g.split("slide")[1], 0);
					v < 1 && (v = 1), v > i.slideamount && (v = i.slideamount), i.startWithSlide = v - 1
				}
				e.append('<div class="tp-loader ' + i.spinner + '"><div class="dot1"></div><div class="dot2"></div><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>'), i.loader = e.find(".tp-loader"), 0 === e.find(".tp-bannertimer").length && e.append('<div class="tp-bannertimer" style="visibility:hidden"></div>'), e.find(".tp-bannertimer").css({
					width: "0%"
				}), i.ul.css({
					display: "block"
				}), prepareSlides(e, i), ("off" !== i.parallax.type || i.scrolleffect.on) && _R.checkForParallax && _R.checkForParallax(e, i), _R.setSize(i), "hero" !== i.sliderType && _R.createNavigation && _R.createNavigation(e, i), _R.resizeThumbsTabs && _R.resizeThumbsTabs && _R.resizeThumbsTabs(i), contWidthManager(i);
				var m = i.viewPort;
				i.inviewport = !1, m != undefined && m.enable && (jQuery.isNumeric(m.visible_area) || -1 !== m.visible_area.indexOf("%") && (m.visible_area = parseInt(m.visible_area) / 100), _R.scrollTicker && _R.scrollTicker(i, e)), "carousel" === i.sliderType && _R.prepareCarousel && (punchgs.TweenLite.set(i.ul, {
					opacity: 0
				}), _R.prepareCarousel(i, new punchgs.TimelineLite, undefined, 0), i.onlyPreparedSlide = !0), setTimeout(function () {
					if (!m.enable || m.enable && i.inviewport || m.enable && !i.inviewport && "wait" == !m.outof) swapSlide(e);
					else if (i.c.addClass("tp-waitforfirststart"), i.waitForFirstSlide = !0, m.presize) {
						var t = jQuery(i.li[0]);
						loadImages(t, i, 0, !0), waitForCurrentImages(t.find(".tp-layers"), i, function () {
							_R.animateTheCaptions({
								slide: t,
								opt: i,
								preset: !0
							})
						})
					}
					_R.manageNavigation && _R.manageNavigation(i), i.slideamount > 1 && (!m.enable || m.enable && i.inviewport ? countDown(e, i) : i.waitForCountDown = !0), setTimeout(function () {
						e.trigger("revolution.slide.onloaded")
					}, 100)
				}, i.startDelay), i.startDelay = 0, jQuery("body").data("rs-fullScreenMode", !1), window.addEventListener("fullscreenchange", onFullScreenChange, {
					passive: !0
				}), window.addEventListener("mozfullscreenchange", onFullScreenChange, {
					passive: !0
				}), window.addEventListener("webkitfullscreenchange", onFullScreenChange, {
					passive: !0
				});
				var y = "resize.revslider-" + e.attr("id");
				jQuery(window).on(y, function () {
					if (e == undefined) return !1;
					0 != jQuery("body").find(e) && contWidthManager(i);
					var t = !1;
					if ("fullscreen" == i.sliderLayout) {
						var a = jQuery(window).height();
						"mobile" == i.fallbacks.ignoreHeightChanges && _ISM || "always" == i.fallbacks.ignoreHeightChanges ? (i.fallbacks.ignoreHeightChangesSize = i.fallbacks.ignoreHeightChangesSize == undefined ? 0 : i.fallbacks.ignoreHeightChangesSize, t = a != i.lastwindowheight && Math.abs(a - i.lastwindowheight) > i.fallbacks.ignoreHeightChangesSize) : t = a != i.lastwindowheight
					}(e.outerWidth(!0) != i.width || e.is(":hidden") || t) && (i.lastwindowheight = jQuery(window).height(), containerResized(e, i))
				}), hideSliderUnder(e, i), contWidthManager(i), i.fallbacks.disableFocusListener || "true" == i.fallbacks.disableFocusListener || !0 === i.fallbacks.disableFocusListener || (e.addClass("rev_redraw_on_blurfocus"), tabBlurringCheck())
			}
		},
		cArray = function (e, i) {
			if (!jQuery.isArray(e)) {
				t = e;
				(e = new Array).push(t)
			}
			if (e.length < i)
				for (var t = e[e.length - 1], a = 0; a < i - e.length + 2; a++) e.push(t);
			return e
		},
		checkHoverDependencies = function (e, i) {
			var t = e.data();
			("sliderenter" === t.start || t.frames !== undefined && t.frames[0] != undefined && "sliderenter" === t.frames[0].delay) && (i.layersonhover === undefined && (i.c.on("tp-mouseenter", function () {
				i.layersonhover && jQuery.each(i.layersonhover, function (e, t) {
					var a = t.data("closestli") || t.closest(".tp-revslider-slidesli"),
						n = t.data("staticli") || t.closest(".tp-static-layers");
					t.data("closestli") === undefined && (t.data("closestli", a), t.data("staticli", n)), (a.length > 0 && a.hasClass("active-revslide") || a.hasClass("processing-revslide") || n.length > 0) && (t.data("animdirection", "in"), _R.playAnimationFrame && _R.playAnimationFrame({
						caption: t,
						opt: i,
						frame: "frame_0",
						triggerdirection: "in",
						triggerframein: "frame_0",
						triggerframeout: "frame_999"
					}), t.data("triggerstate", "on"))
				})
			}), i.c.on("tp-mouseleft", function () {
				i.layersonhover && jQuery.each(i.layersonhover, function (e, t) {
					t.data("animdirection", "out"), t.data("triggered", !0), t.data("triggerstate", "off"), _R.stopVideo && _R.stopVideo(t, i), _R.playAnimationFrame && _R.playAnimationFrame({
						caption: t,
						opt: i,
						frame: "frame_999",
						triggerdirection: "out",
						triggerframein: "frame_0",
						triggerframeout: "frame_999"
					})
				})
			}), i.layersonhover = new Array), i.layersonhover.push(e))
		},
		contWidthManager = function (e) {
			var i = _R.getHorizontalOffset(e.c, "left");
			if ("auto" == e.sliderLayout || "fullscreen" === e.sliderLayout && "on" == e.fullScreenAutoWidth) "fullscreen" == e.sliderLayout && "on" == e.fullScreenAutoWidth ? punchgs.TweenLite.set(e.ul, {
				left: 0,
				width: e.c.width()
			}) : punchgs.TweenLite.set(e.ul, {
				left: i,
				width: e.c.width() - _R.getHorizontalOffset(e.c, "both")
			});
			else {
				var t = Math.ceil(e.c.closest(".forcefullwidth_wrapper_tp_banner").offset().left - i);
				punchgs.TweenLite.set(e.c.parent(), {
					left: 0 - t + "px",
					width: jQuery(window).width() - _R.getHorizontalOffset(e.c, "both")
				})
			}
			e.slayers && "fullwidth" != e.sliderLayout && "fullscreen" != e.sliderLayout && punchgs.TweenLite.set(e.slayers, {
				left: i
			})
		},
		cv = function (e, i) {
			return e === undefined ? i : e
		},
		hideSliderUnder = function (e, i, t) {
			var a = e.parent();
			jQuery(window).width() < i.hideSliderAtLimit ? (e.trigger("stoptimer"), "none" != a.css("display") && a.data("olddisplay", a.css("display")), a.css({
				display: "none"
			})) : e.is(":hidden") && t && (a.data("olddisplay") != undefined && "undefined" != a.data("olddisplay") && "none" != a.data("olddisplay") ? a.css({
				display: a.data("olddisplay")
			}) : a.css({
				display: "block"
			}), e.trigger("restarttimer"), setTimeout(function () {
				containerResized(e, i)
			}, 150)), _R.hideUnHideNav && _R.hideUnHideNav(i)
		},
		containerResized = function (e, i) {
			if (e.trigger("revolution.slide.beforeredraw"), 1 == i.infullscreenmode && (i.minHeight = jQuery(window).height()), setCurWinRange(i), setCurWinRange(i, !0), !_R.resizeThumbsTabs || !0 === _R.resizeThumbsTabs(i)) {
				if (hideSliderUnder(e, i, !0), contWidthManager(i), "carousel" == i.sliderType && _R.prepareCarousel(i, !0), e === undefined) return !1;
				_R.setSize(i), i.conw = i.c.width(), i.conh = i.infullscreenmode ? i.minHeight : i.c.height();
				var t = e.find(".active-revslide .slotholder"),
					a = e.find(".processing-revslide .slotholder");
				removeSlots(e, i, e, 2), "standard" === i.sliderType && (punchgs.TweenLite.set(a.find(".defaultimg"), {
					opacity: 0
				}), t.find(".defaultimg").css({
					opacity: 1
				})), "carousel" === i.sliderType && i.lastconw != i.conw && (clearTimeout(i.pcartimer), i.pcartimer = setTimeout(function () {
					_R.prepareCarousel(i, !0), "carousel" == i.sliderType && "on" === i.carousel.showLayersAllTime && jQuery.each(i.li, function (e) {
						_R.animateTheCaptions({
							slide: jQuery(i.li[e]),
							opt: i,
							recall: !0
						})
					})
				}, 100), i.lastconw = i.conw), _R.manageNavigation && _R.manageNavigation(i), _R.animateTheCaptions && e.find(".active-revslide").length > 0 && _R.animateTheCaptions({
					slide: e.find(".active-revslide"),
					opt: i,
					recall: !0
				}), "on" == a.data("kenburns") && _R.startKenBurn(a, i, a.data("kbtl") !== undefined ? a.data("kbtl").progress() : 0), "on" == t.data("kenburns") && _R.startKenBurn(t, i, t.data("kbtl") !== undefined ? t.data("kbtl").progress() : 0), _R.animateTheCaptions && e.find(".processing-revslide").length > 0 && _R.animateTheCaptions({
					slide: e.find(".processing-revslide"),
					opt: i,
					recall: !0
				}), _R.manageNavigation && _R.manageNavigation(i)
			}
			e.trigger("revolution.slide.afterdraw")
		},
		setScale = function (e) {
			e.bw = e.width / e.gridwidth[e.curWinRange], e.bh = e.height / e.gridheight[e.curWinRange], e.bh > e.bw ? e.bh = e.bw : e.bw = e.bh, (e.bh > 1 || e.bw > 1) && (e.bw = 1, e.bh = 1)
		},
		prepareSlides = function (e, i) {
			if (e.find(".tp-caption").each(function () {
					var e = jQuery(this);
					e.data("transition") !== undefined && e.addClass(e.data("transition"))
				}), i.ul.css({
					overflow: "hidden",
					width: "100%",
					height: "100%",
					maxHeight: e.parent().css("maxHeight")
				}), "on" == i.autoHeight && (i.ul.css({
					overflow: "hidden",
					width: "100%",
					height: "100%",
					maxHeight: "none"
				}), e.css({
					maxHeight: "none"
				}), e.parent().css({
					maxHeight: "none"
				})), i.allli.each(function (e) {
					var t = jQuery(this),
						a = t.data("originalindex");
					(i.startWithSlide != undefined && a == i.startWithSlide || i.startWithSlide === undefined && 0 == e) && t.addClass("next-revslide"), t.css({
						width: "100%",
						height: "100%",
						overflow: "hidden"
					})
				}), "carousel" === i.sliderType) {
				i.ul.css({
					overflow: "visible"
				}).wrap('<div class="tp-carousel-wrapper" style="width:100%;height:100%;position:absolute;top:0px;left:0px;overflow:hidden;"></div>');
				var t = '<div style="clear:both;display:block;width:100%;height:1px;position:relative;margin-bottom:-1px"></div>';
				i.c.parent().prepend(t), i.c.parent().append(t), _R.prepareCarousel(i)
			}
			e.parent().css({
				overflow: "visible"
			}), i.allli.find(">img").each(function (e) {
				var t = jQuery(this),
					a = t.closest("li"),
					n = a.find(".rs-background-video-layer");
				n.addClass("defaultvid").css({
					zIndex: 30
				}), t.addClass("defaultimg"), "on" == i.fallbacks.panZoomDisableOnMobile && _ISM && (t.data("kenburns", "off"), t.data("bgfit", "cover"));
				var r = a.data("mediafilter");
				r = "none" === r || r === undefined ? "" : r, t.wrap('<div class="slotholder" style="position:absolute; top:0px; left:0px; z-index:0;width:100%;height:100%;"></div>'), n.appendTo(a.find(".slotholder"));
				var o = t.data();
				t.closest(".slotholder").data(o), n.length > 0 && o.bgparallax != undefined && (n.data("bgparallax", o.bgparallax), n.data("showcoveronpause", "on")), "none" != i.dottedOverlay && i.dottedOverlay != undefined && t.closest(".slotholder").append('<div class="tp-dottedoverlay ' + i.dottedOverlay + '"></div>');
				var s = t.attr("src");
				o.src = s, o.bgfit = o.bgfit || "cover", o.bgrepeat = o.bgrepeat || "no-repeat", o.bgposition = o.bgposition || "center center";
				t.closest(".slotholder");
				var l = t.data("bgcolor"),
					d = "";
				d = l !== undefined && l.indexOf("gradient") >= 0 ? '"background:' + l + ';width:100%;height:100%;"' : '"background-color:' + l + ";background-repeat:" + o.bgrepeat + ";background-image:url(" + s + ");background-size:" + o.bgfit + ";background-position:" + o.bgposition + ';width:100%;height:100%;"', t.data("mediafilter", r), r = "on" === t.data("kenburns") ? "" : r;
				var u = jQuery('<div class="tp-bgimg defaultimg ' + r + '" data-bgcolor="' + l + '" style=' + d + "></div>");
				t.parent().append(u);
				var c = document.createComment("Runtime Modification - Img tag is Still Available for SEO Goals in Source - " + t.get(0).outerHTML);
				t.replaceWith(c), u.data(o), u.attr("src", s), "standard" !== i.sliderType && "undefined" !== i.sliderType || u.css({
					opacity: 0
				})
			}), i.scrolleffect.on && "on" === i.scrolleffect.on_slidebg && (i.allslotholder = new Array, i.allli.find(".slotholder").each(function () {
				jQuery(this).wrap('<div style="display:block;position:absolute;top:0px;left:0px;width:100%;height:100%" class="slotholder_fadeoutwrap"></div>')
			}), i.allslotholder = i.c.find(".slotholder_fadeoutwrap"))
		},
		removeSlots = function (e, i, t, a) {
			i.removePrepare = i.removePrepare + a, t.find(".slot, .slot-circle-wrapper").each(function () {
				jQuery(this).remove()
			}), i.transition = 0, i.removePrepare = 0
		},
		cutParams = function (e) {
			var i = e;
			return e != undefined && e.length > 0 && (i = e.split("?")[0]), i
		},
		relativeRedir = function (e) {
			return location.pathname.replace(/(.*)\/[^/]*/, "$1/" + e)
		},
		abstorel = function (e, i) {
			var t = e.split("/"),
				a = i.split("/");
			t.pop();
			for (var n = 0; n < a.length; n++) "." != a[n] && (".." == a[n] ? t.pop() : t.push(a[n]));
			return t.join("/")
		},
		imgLoaded = function (e, i, t) {
			i.syncload--, i.loadqueue && jQuery.each(i.loadqueue, function (i, a) {
				var n = a.src.replace(/\.\.\/\.\.\//gi, ""),
					r = self.location.href,
					o = document.location.origin,
					s = r.substring(0, r.length - 1) + "/" + n,
					l = o + "/" + n,
					d = abstorel(self.location.href, a.src);
				r = r.substring(0, r.length - 1) + n, (cutParams(o += n) === cutParams(decodeURIComponent(e.src)) || cutParams(r) === cutParams(decodeURIComponent(e.src)) || cutParams(d) === cutParams(decodeURIComponent(e.src)) || cutParams(l) === cutParams(decodeURIComponent(e.src)) || cutParams(s) === cutParams(decodeURIComponent(e.src)) || cutParams(a.src) === cutParams(decodeURIComponent(e.src)) || cutParams(a.src).replace(/^.*\/\/[^\/]+/, "") === cutParams(decodeURIComponent(e.src)).replace(/^.*\/\/[^\/]+/, "") || "file://" === window.location.origin && cutParams(e.src).match(new RegExp(n))) && (a.progress = t, a.width = e.width, a.height = e.height)
			}), progressImageLoad(i)
		},
		progressImageLoad = function (e) {
			3 != e.syncload && e.loadqueue && jQuery.each(e.loadqueue, function (i, t) {
				if (t.progress.match(/prepared/g) && e.syncload <= 3) {
					if (e.syncload++, "img" == t.type) {
						var a = new Image;
						a.onload = function () {
							imgLoaded(this, e, "loaded"), t.error = !1
						}, a.onerror = function () {
							imgLoaded(this, e, "failed"), t.error = !0
						}, a.src = t.src
					} else jQuery.get(t.src, function (i) {
						t.innerHTML = (new XMLSerializer).serializeToString(i.documentElement), t.progress = "loaded", e.syncload--, progressImageLoad(e)
					}).fail(function () {
						t.progress = "failed", e.syncload--, progressImageLoad(e)
					});
					t.progress = "inload"
				}
			})
		},
		addToLoadQueue = function (e, i, t, a, n) {
			var r = !1;
			if (i.loadqueue && jQuery.each(i.loadqueue, function (i, t) {
					t.src === e && (r = !0)
				}), !r) {
				var o = new Object;
				o.src = e, o.starttoload = jQuery.now(), o.type = a || "img", o.prio = t, o.progress = "prepared", o.static = n, i.loadqueue.push(o)
			}
		},
		loadImages = function (e, i, t, a) {
			e.find("img,.defaultimg, .tp-svg-layer").each(function () {
				var e = jQuery(this),
					n = e.data("lazyload") !== undefined && "undefined" !== e.data("lazyload") ? e.data("lazyload") : e.data("svg_src") != undefined ? e.data("svg_src") : e.attr("src"),
					r = e.data("svg_src") != undefined ? "svg" : "img";
				e.data("start-to-load", jQuery.now()), addToLoadQueue(n, i, t, r, a)
			}), progressImageLoad(i)
		},
		getLoadObj = function (e, i) {
			var t = new Object;
			return e.loadqueue && jQuery.each(e.loadqueue, function (e, a) {
				a.src == i && (t = a)
			}), t
		},
		waitForCurrentImages = function (e, i, t) {
			var a = !1;
			e.find("img,.defaultimg, .tp-svg-layer").each(function () {
				var t = jQuery(this),
					n = t.data("lazyload") != undefined ? t.data("lazyload") : t.data("svg_src") != undefined ? t.data("svg_src") : t.attr("src"),
					r = getLoadObj(i, n);
				if (t.data("loaded") === undefined && r !== undefined && r.progress && r.progress.match(/loaded/g)) {
					if (t.attr("src", r.src), "img" == r.type)
						if (t.hasClass("defaultimg")) _R.isIE(8) ? defimg.attr("src", r.src) : -1 == r.src.indexOf("images/transparent.png") && -1 == r.src.indexOf("assets/transparent.png") || t.data("bgcolor") === undefined ? t.css({
							backgroundImage: 'url("' + r.src + '")'
						}) : t.data("bgcolor") !== undefined && t.css({
							background: t.data("bgcolor")
						}), e.data("owidth", r.width), e.data("oheight", r.height), e.find(".slotholder").data("owidth", r.width), e.find(".slotholder").data("oheight", r.height);
						else {
							var o = t.data("ww"),
								s = t.data("hh");
							t.data("owidth", r.width), t.data("oheight", r.height), o = o == undefined || "auto" == o || "" == o ? r.width : o, s = s == undefined || "auto" == s || "" == s ? r.height : s, !jQuery.isNumeric(o) && o.indexOf("%") > 0 && (s = o), t.data("ww", o), t.data("hh", s)
						}
					else "svg" == r.type && "loaded" == r.progress && (t.append('<div class="tp-svg-innercontainer"></div>'), t.find(".tp-svg-innercontainer").append(r.innerHTML));
					t.data("loaded", !0)
				}
				if (r && r.progress && r.progress.match(/inprogress|inload|prepared/g) && (!r.error && jQuery.now() - t.data("start-to-load") < 5e3 ? a = !0 : (r.progress = "failed", r.reported_img || (r.reported_img = !0, console.warn(n + "  Could not be loaded !")))), 1 == i.youtubeapineeded && (!window.YT || YT.Player == undefined) && (a = !0, jQuery.now() - i.youtubestarttime > 5e3 && 1 != i.youtubewarning)) {
					i.youtubewarning = !0;
					l = "YouTube Api Could not be loaded !";
					"https:" === location.protocol && (l += " Please Check and Renew SSL Certificate !"), console.error(l), i.c.append('<div style="position:absolute;top:50%;width:100%;color:#e74c3c;  font-size:16px; text-align:center; padding:15px;background:#000; display:block;"><strong>' + l + "</strong></div>")
				}
				if (1 == i.vimeoapineeded && !window.Froogaloop && (a = !0, jQuery.now() - i.vimeostarttime > 5e3 && 1 != i.vimeowarning)) {
					i.vimeowarning = !0;
					var l = "Vimeo Froogaloop Api Could not be loaded !";
					"https:" === location.protocol && (l += " Please Check and Renew SSL Certificate !"), console.error(l), i.c.append('<div style="position:absolute;top:50%;width:100%;color:#e74c3c;  font-size:16px; text-align:center; padding:15px;background:#000; display:block;"><strong>' + l + "</strong></div>")
				}
			}), !_ISM && i.audioqueue && i.audioqueue.length > 0 && jQuery.each(i.audioqueue, function (e, i) {
				i.status && "prepared" === i.status && jQuery.now() - i.start < i.waittime && (a = !0)
			}), jQuery.each(i.loadqueue, function (e, i) {
				!0 !== i.static || "loaded" == i.progress && "failed" !== i.progress || ("failed" == i.progress ? i.reported || (i.reported = !0, console.warn("Static Image " + i.src + "  Could not be loaded in time. Error Exists:" + i.error)) : !i.error && jQuery.now() - i.starttoload < 5e3 ? a = !0 : i.reported || (i.reported = !0, console.warn("Static Image " + i.src + "  Could not be loaded within 5s! Error Exists:" + i.error)))
			}), a ? punchgs.TweenLite.delayedCall(.18, waitForCurrentImages, [e, i, t]) : punchgs.TweenLite.delayedCall(.18, t)
		},
		swapSlide = function (e) {
			var i = e[0].opt;
			if (clearTimeout(i.waitWithSwapSlide), e.find(".processing-revslide").length > 0) return i.waitWithSwapSlide = setTimeout(function () {
				swapSlide(e)
			}, 150), !1;
			var t = e.find(".active-revslide"),
				a = e.find(".next-revslide"),
				n = a.find(".defaultimg");
			if ("carousel" !== i.sliderType || i.carousel.fadein || (punchgs.TweenLite.to(i.ul, 1, {
					opacity: 1
				}), i.carousel.fadein = !0), a.index() === t.index() && !0 !== i.onlyPreparedSlide) return a.removeClass("next-revslide"), !1;
			!0 === i.onlyPreparedSlide && (i.onlyPreparedSlide = !1, jQuery(i.li[0]).addClass("processing-revslide")), a.removeClass("next-revslide").addClass("processing-revslide"), -1 === a.index() && "carousel" === i.sliderType && (a = jQuery(i.li[0])), a.data("slide_on_focus_amount", a.data("slide_on_focus_amount") + 1 || 1), "on" == i.stopLoop && a.index() == i.lastslidetoshow - 1 && (e.find(".tp-bannertimer").css({
				visibility: "hidden"
			}), e.trigger("revolution.slide.onstop"), i.noloopanymore = 1), a.index() === i.slideamount - 1 && (i.looptogo = i.looptogo - 1, i.looptogo <= 0 && (i.stopLoop = "on")), i.tonpause = !0, e.trigger("stoptimer"), i.cd = 0, "off" === i.spinner && (i.loader !== undefined ? i.loader.css({
				display: "none"
			}) : i.loadertimer = setTimeout(function () {
				i.loader !== undefined && i.loader.css({
					display: "block"
				})
			}, 50)), loadImages(a, i, 1), _R.preLoadAudio && _R.preLoadAudio(a, i, 1), waitForCurrentImages(a, i, function () {
				a.find(".rs-background-video-layer").each(function () {
					var e = jQuery(this);
					e.hasClass("HasListener") || (e.data("bgvideo", 1), _R.manageVideoLayer && _R.manageVideoLayer(e, i)), 0 == e.find(".rs-fullvideo-cover").length && e.append('<div class="rs-fullvideo-cover"></div>')
				}), swapSlideProgress(n, e)
			})
		},
		swapSlideProgress = function (e, i) {
			var t = i.find(".active-revslide"),
				a = i.find(".processing-revslide"),
				n = t.find(".slotholder"),
				r = a.find(".slotholder"),
				o = i[0].opt;
			o.tonpause = !1, o.cd = 0, clearTimeout(o.loadertimer), o.loader !== undefined && o.loader.css({
				display: "none"
			}), _R.setSize(o), _R.slotSize(e, o), _R.manageNavigation && _R.manageNavigation(o);
			var s = {};
			s.nextslide = a, s.currentslide = t, i.trigger("revolution.slide.onbeforeswap", s), o.transition = 1, o.videoplaying = !1, a.data("delay") != undefined ? (o.cd = 0, o.delay = a.data("delay")) : o.delay = o.origcd, "true" == a.data("ssop") || !0 === a.data("ssop") ? o.ssop = !0 : o.ssop = !1, i.trigger("nulltimer");
			var l = t.index(),
				d = a.index();
			o.sdir = d < l ? 1 : 0, "arrow" == o.sc_indicator && (0 == l && d == o.slideamount - 1 && (o.sdir = 1), l == o.slideamount - 1 && 0 == d && (o.sdir = 0)), o.lsdir = o.lsdir === undefined ? o.sdir : o.lsdir, o.dirc = o.lsdir != o.sdir, o.lsdir = o.sdir, t.index() != a.index() && 1 != o.firststart && _R.removeTheCaptions && _R.removeTheCaptions(t, o), a.hasClass("rs-pause-timer-once") || a.hasClass("rs-pause-timer-always") ? o.videoplaying = !0 : i.trigger("restarttimer"), a.removeClass("rs-pause-timer-once");
			var u, c;
			if (o.currentSlide = t.index(), o.nextSlide = a.index(), "carousel" == o.sliderType) c = new punchgs.TimelineLite, _R.prepareCarousel(o, c), letItFree(i, r, n, a, t, c), o.transition = 0, o.firststart = 0;
			else {
				(c = new punchgs.TimelineLite({
					onComplete: function () {
						letItFree(i, r, n, a, t, c)
					}
				})).add(punchgs.TweenLite.set(r.find(".defaultimg"), {
					opacity: 0
				})), c.pause(), _R.animateTheCaptions && _R.animateTheCaptions({
					slide: a,
					opt: o,
					preset: !0
				}), 1 == o.firststart && (punchgs.TweenLite.set(t, {
					autoAlpha: 0
				}), o.firststart = 0), punchgs.TweenLite.set(t, {
					zIndex: 18
				}), punchgs.TweenLite.set(a, {
					autoAlpha: 0,
					zIndex: 20
				}), "prepared" == a.data("differentissplayed") && (a.data("differentissplayed", "done"), a.data("transition", a.data("savedtransition")), a.data("slotamount", a.data("savedslotamount")), a.data("masterspeed", a.data("savedmasterspeed"))), a.data("fstransition") != undefined && "done" != a.data("differentissplayed") && (a.data("savedtransition", a.data("transition")), a.data("savedslotamount", a.data("slotamount")), a.data("savedmasterspeed", a.data("masterspeed")), a.data("transition", a.data("fstransition")), a.data("slotamount", a.data("fsslotamount")), a.data("masterspeed", a.data("fsmasterspeed")), a.data("differentissplayed", "prepared")), a.data("transition") == undefined && a.data("transition", "random"), u = 0;
				var p = a.data("transition") !== undefined ? a.data("transition").split(",") : "fade",
					f = a.data("nexttransid") == undefined ? -1 : a.data("nexttransid");
				"on" == a.data("randomtransition") ? f = Math.round(Math.random() * p.length) : f += 1, f == p.length && (f = 0), a.data("nexttransid", f);
				var h = p[f];
				o.ie && ("boxfade" == h && (h = "boxslide"), "slotfade-vertical" == h && (h = "slotzoom-vertical"), "slotfade-horizontal" == h && (h = "slotzoom-horizontal")), _R.isIE(8) && (h = 11), c = _R.animateSlide(u, h, i, a, t, r, n, c), "on" == r.data("kenburns") && (_R.startKenBurn(r, o), c.add(punchgs.TweenLite.set(r, {
					autoAlpha: 0
				}))), c.pause()
			}
			_R.scrollHandling && (_R.scrollHandling(o, !0, 0), c.eventCallback("onUpdate", function () {
				_R.scrollHandling(o, !0, 0)
			})), "off" != o.parallax.type && o.parallax.firstgo == undefined && _R.scrollHandling && (o.parallax.firstgo = !0, o.lastscrolltop = -999, _R.scrollHandling(o, !0, 0), setTimeout(function () {
				o.lastscrolltop = -999, _R.scrollHandling(o, !0, 0)
			}, 210), setTimeout(function () {
				o.lastscrolltop = -999, _R.scrollHandling(o, !0, 0)
			}, 420)), _R.animateTheCaptions ? "carousel" === o.sliderType && "on" === o.carousel.showLayersAllTime ? (jQuery.each(o.li, function (e) {
				o.carousel.allLayersStarted ? _R.animateTheCaptions({
					slide: jQuery(o.li[e]),
					opt: o,
					recall: !0
				}) : o.li[e] === a ? _R.animateTheCaptions({
					slide: jQuery(o.li[e]),
					maintimeline: c,
					opt: o,
					startslideanimat: 0
				}) : _R.animateTheCaptions({
					slide: jQuery(o.li[e]),
					opt: o,
					startslideanimat: 0
				})
			}), o.carousel.allLayersStarted = !0) : _R.animateTheCaptions({
				slide: a,
				opt: o,
				maintimeline: c,
				startslideanimat: 0
			}) : c != undefined && setTimeout(function () {
				c.resume()
			}, 30), punchgs.TweenLite.to(a, .001, {
				autoAlpha: 1
			})
		},
		letItFree = function (e, i, t, a, n, r) {
			var o = e[0].opt;
			"carousel" === o.sliderType || (o.removePrepare = 0, punchgs.TweenLite.to(i.find(".defaultimg"), .001, {
				zIndex: 20,
				autoAlpha: 1,
				onComplete: function () {
					removeSlots(e, o, a, 1)
				}
			}), a.index() != n.index() && punchgs.TweenLite.to(n, .2, {
				zIndex: 18,
				autoAlpha: 0,
				onComplete: function () {
					removeSlots(e, o, n, 1)
				}
			})), e.find(".active-revslide").removeClass("active-revslide"), e.find(".processing-revslide").removeClass("processing-revslide").addClass("active-revslide"), o.act = a.index(), o.c.attr("data-slideactive", e.find(".active-revslide").data("index")), "scroll" != o.parallax.type && "scroll+mouse" != o.parallax.type && "mouse+scroll" != o.parallax.type || (o.lastscrolltop = -999, _R.scrollHandling(o)), r.clear(), t.data("kbtl") != undefined && (t.data("kbtl").reverse(), t.data("kbtl").timeScale(25)), "on" == i.data("kenburns") && (i.data("kbtl") != undefined ? (i.data("kbtl").timeScale(1), i.data("kbtl").play()) : _R.startKenBurn(i, o)), a.find(".rs-background-video-layer").each(function (e) {
				if (_ISM && !o.fallbacks.allowHTML5AutoPlayOnAndroid) return !1;
				var i = jQuery(this);
				_R.resetVideo(i, o), punchgs.TweenLite.fromTo(i, 1, {
					autoAlpha: 0
				}, {
					autoAlpha: 1,
					ease: punchgs.Power3.easeInOut,
					delay: .2,
					onComplete: function () {
						_R.animcompleted && _R.animcompleted(i, o)
					}
				})
			}), n.find(".rs-background-video-layer").each(function (e) {
				if (_ISM) return !1;
				var i = jQuery(this);
				_R.stopVideo && (_R.resetVideo(i, o), _R.stopVideo(i, o)), punchgs.TweenLite.to(i, 1, {
					autoAlpha: 0,
					ease: punchgs.Power3.easeInOut,
					delay: .2
				})
			});
			var s = {};
			if (s.slideIndex = a.index() + 1, s.slideLIIndex = a.index(), s.slide = a, s.currentslide = a, s.prevslide = n, o.last_shown_slide = n.index(), e.trigger("revolution.slide.onchange", s), e.trigger("revolution.slide.onafterswap", s), o.startWithSlide !== undefined && "done" !== o.startWithSlide && "carousel" === o.sliderType) {
				for (var l = o.startWithSlide, d = 0; d <= o.li.length - 1; d++) jQuery(o.li[d]).data("originalindex") === o.startWithSlide && (l = d);
				0 !== l && _R.callingNewSlide(o.c, l), o.startWithSlide = "done"
			}
			o.duringslidechange = !1;
			var u = n.data("slide_on_focus_amount"),
				c = n.data("hideafterloop");
			0 != c && c <= u && o.c.revremoveslide(n.index());
			var p = -1 === o.nextSlide || o.nextSlide === undefined ? 0 : o.nextSlide;
			o.rowzones != undefined && (p = p > o.rowzones.length ? o.rowzones.length : p), o.rowzones != undefined && o.rowzones.length > 0 && o.rowzones[p] != undefined && p >= 0 && p <= o.rowzones.length && o.rowzones[p].length > 0 && _R.setSize(o)
		},
		removeAllListeners = function (e, i) {
			e.children().each(function () {
				try {
					jQuery(this).die("click")
				} catch (e) {}
				try {
					jQuery(this).die("mouseenter")
				} catch (e) {}
				try {
					jQuery(this).die("mouseleave")
				} catch (e) {}
				try {
					jQuery(this).unbind("hover")
				} catch (e) {}
			});
			try {
				e.die("click", "mouseenter", "mouseleave")
			} catch (e) {}
			clearInterval(i.cdint), e = null
		},
		countDown = function (e, i) {
			i.cd = 0, i.loop = 0, i.stopAfterLoops != undefined && i.stopAfterLoops > -1 ? i.looptogo = i.stopAfterLoops : i.looptogo = 9999999, i.stopAtSlide != undefined && i.stopAtSlide > -1 ? i.lastslidetoshow = i.stopAtSlide : i.lastslidetoshow = 999, i.stopLoop = "off", 0 == i.looptogo && (i.stopLoop = "on");
			var t = e.find(".tp-bannertimer");
			e.on("stoptimer", function () {
				var e = jQuery(this).find(".tp-bannertimer");
				e[0].tween.pause(), "on" == i.disableProgressBar && e.css({
					visibility: "hidden"
				}), i.sliderstatus = "paused", _R.unToggleState(i.slidertoggledby)
			}), e.on("starttimer", function () {
				i.forcepause_viatoggle || (1 != i.conthover && 1 != i.videoplaying && i.width > i.hideSliderAtLimit && 1 != i.tonpause && 1 != i.overnav && 1 != i.ssop && (1 === i.noloopanymore || i.viewPort.enable && !i.inviewport || (t.css({
					visibility: "visible"
				}), t[0].tween.resume(), i.sliderstatus = "playing")), "on" == i.disableProgressBar && t.css({
					visibility: "hidden"
				}), _R.toggleState(i.slidertoggledby))
			}), e.on("restarttimer", function () {
				if (!i.forcepause_viatoggle) {
					var e = jQuery(this).find(".tp-bannertimer");
					if (i.mouseoncontainer && "on" == i.navigation.onHoverStop && !_ISM) return !1;
					1 === i.noloopanymore || i.viewPort.enable && !i.inviewport || 1 == i.ssop || (e.css({
						visibility: "visible"
					}), e[0].tween.kill(), e[0].tween = punchgs.TweenLite.fromTo(e, i.delay / 1e3, {
						width: "0%"
					}, {
						force3D: "auto",
						width: "100%",
						ease: punchgs.Linear.easeNone,
						onComplete: a,
						delay: 1
					}), i.sliderstatus = "playing"), "on" == i.disableProgressBar && e.css({
						visibility: "hidden"
					}), _R.toggleState(i.slidertoggledby)
				}
			}), e.on("nulltimer", function () {
				t[0].tween.kill(), t[0].tween = punchgs.TweenLite.fromTo(t, i.delay / 1e3, {
					width: "0%"
				}, {
					force3D: "auto",
					width: "100%",
					ease: punchgs.Linear.easeNone,
					onComplete: a,
					delay: 1
				}), t[0].tween.pause(0), "on" == i.disableProgressBar && t.css({
					visibility: "hidden"
				}), i.sliderstatus = "paused"
			});
			var a = function () {
				0 == jQuery("body").find(e).length && (removeAllListeners(e, i), clearInterval(i.cdint)), e.trigger("revolution.slide.slideatend"), 1 == e.data("conthover-changed") && (i.conthover = e.data("conthover"), e.data("conthover-changed", 0)), _R.callingNewSlide(e, 1)
			};
			t[0].tween = punchgs.TweenLite.fromTo(t, i.delay / 1e3, {
				width: "0%"
			}, {
				force3D: "auto",
				width: "100%",
				ease: punchgs.Linear.easeNone,
				onComplete: a,
				delay: 1
			}), i.slideamount > 1 && (0 != i.stopAfterLoops || 1 != i.stopAtSlide) ? e.trigger("starttimer") : (i.noloopanymore = 1, e.trigger("nulltimer")), e.on("tp-mouseenter", function () {
				i.mouseoncontainer = !0, "on" != i.navigation.onHoverStop || _ISM || (e.trigger("stoptimer"), e.trigger("revolution.slide.onpause"))
			}), e.on("tp-mouseleft", function () {
				i.mouseoncontainer = !1, 1 != e.data("conthover") && "on" == i.navigation.onHoverStop && (1 == i.viewPort.enable && i.inviewport || 0 == i.viewPort.enable) && (e.trigger("revolution.slide.onresume"), e.trigger("starttimer"))
			})
		},
		vis = function () {
			var e, i, t = {
				hidden: "visibilitychange",
				webkitHidden: "webkitvisibilitychange",
				mozHidden: "mozvisibilitychange",
				msHidden: "msvisibilitychange"
			};
			for (e in t)
				if (e in document) {
					i = t[e];
					break
				}
			return function (t) {
				return t && document.addEventListener(i, t, {
					pasive: !0
				}), !document[e]
			}
		}(),
		restartOnFocus = function () {
			jQuery(".rev_redraw_on_blurfocus").each(function () {
				var e = jQuery(this)[0].opt;
				if (e == undefined || e.c == undefined || 0 === e.c.length) return !1;
				1 != e.windowfocused && (e.windowfocused = !0, punchgs.TweenLite.delayedCall(.3, function () {
					"on" == e.fallbacks.nextSlideOnWindowFocus && e.c.revnext(), e.c.revredraw(), "playing" == e.lastsliderstatus && e.c.revresume()
				}))
			})
		},
		lastStatBlur = function () {
			jQuery(".rev_redraw_on_blurfocus").each(function () {
				var e = jQuery(this)[0].opt;
				e.windowfocused = !1, e.lastsliderstatus = e.sliderstatus, e.c.revpause();
				var i = e.c.find(".active-revslide .slotholder"),
					t = e.c.find(".processing-revslide .slotholder");
				"on" == t.data("kenburns") && _R.stopKenBurn(t, e), "on" == i.data("kenburns") && _R.stopKenBurn(i, e)
			})
		},
		tabBlurringCheck = function () {
			var e = document.documentMode === undefined,
				i = window.chrome;
			1 !== jQuery("body").data("revslider_focus_blur_listener") && (jQuery("body").data("revslider_focus_blur_listener", 1), e && !i ? jQuery(window).on("focusin", function () {
				restartOnFocus()
			}).on("focusout", function () {
				lastStatBlur()
			}) : window.addEventListener ? (window.addEventListener("focus", function (e) {
				restartOnFocus()
			}, {
				capture: !1,
				passive: !0
			}), window.addEventListener("blur", function (e) {
				lastStatBlur()
			}, {
				capture: !1,
				passive: !0
			})) : (window.attachEvent("focus", function (e) {
				restartOnFocus()
			}), window.attachEvent("blur", function (e) {
				lastStatBlur()
			})))
		},
		getUrlVars = function (e) {
			for (var i, t = [], a = window.location.href.slice(window.location.href.indexOf(e) + 1).split("_"), n = 0; n < a.length; n++) a[n] = a[n].replace("%3D", "="), i = a[n].split("="), t.push(i[0]), t[i[0]] = i[1];
			return t
		}
}(jQuery);;
jQuery(document).ready(function ($) {
	$('.zilla-likes').live('click', function () {
		var link = $(this);
		if (link.hasClass('active')) return false;
		var id = $(this).attr('id'),
			postfix = link.find('.zilla-likes-postfix').text();
		$.post(zilla_likes.ajaxurl, {
			action: 'zilla-likes',
			likes_id: id,
			postfix: postfix
		}, function (data) {
			link.html(data).addClass('active').attr('title', 'You already like this');
		});
		return false;
	});
	if ($('body.ajax-zilla-likes').length) {
		$('.zilla-likes').each(function () {
			var id = $(this).attr('id');
			$(this).load(zilla_likes.ajaxurl, {
				action: 'zilla-likes',
				post_id: id
			});
		});
	}
});

// fin minify thegem settings


;
(function ($, window, undefined) {
	'use strict';
	var $body = $('body');
	$.DLMenu = function (options, element) {
		this.$el = $(element);
		this._init(options);
	};
	$.DLMenu.defaults = {
		animationClasses: {
			classin: 'dl-animate-in-1',
			classout: 'dl-animate-out-1'
		},
		onLevelClick: function (el, name) {
			return false;
		},
		onLinkClick: function (el, ev) {
			return false;
		},
		backLabel: 'Back',
		showCurrentLabel: 'Show this page',
		useActiveItemAsBackLabel: false,
		useActiveItemAsLink: true
	};
	$.DLMenu.prototype = {
		_init: function (options) {
			this.options = $.extend(true, {}, $.DLMenu.defaults, options);
			this._config();
			var animEndEventNames = {
					'WebkitAnimation': 'webkitAnimationEnd',
					'OAnimation': 'oAnimationEnd',
					'msAnimation': 'MSAnimationEnd',
					'animation': 'animationend',
					"MozAnimation": "animationend"
				},
				transEndEventNames = {
					'WebkitTransition': 'webkitTransitionEnd',
					'MozTransition': 'transitionend',
					'OTransition': 'oTransitionEnd',
					'msTransition': 'MSTransitionEnd',
					'transition': 'transitionend'
				};
			if (animEndEventNames[window.supportedAnimation] != undefined) {
				this.animEndEventName = animEndEventNames[window.supportedAnimation] + '.dlmenu';
			} else {
				this.animEndEventName = animEndEventNames['animation'] + '.dlmenu';
			}
			if (transEndEventNames[window.supportedTransition] != undefined) {
				this.transEndEventName = transEndEventNames[window.supportedTransition] + '.dlmenu';
			} else {
				this.transEndEventName = transEndEventNames['transition'] + '.dlmenu';
			}
			this.supportAnimations = window.supportsAnimations;
			this.supportTransitions = window.supportsTransitions;
			this._initEvents();
		},
		_config: function () {
			var self = this;
			this.open = false;
			this.$trigger = this.$el.children('.dl-trigger');
			this.$menu = this.$el.children('ul.dl-menu');
			this.$menuitems = this.$menu.find('li:not(.dl-back)');
			this.$el.find('ul.dl-submenu').prepend('<li class="dl-back"><a href="#">' + this.options.backLabel + '</a></li>');
			this.$back = this.$menu.find('li.dl-back');
			if (this.options.useActiveItemAsBackLabel) {
				this.$back.each(function () {
					var $this = $(this),
						parentLabel = $this.parents('li:first').find('a:first').text();
					$this.find('a').html(parentLabel);
				});
			}
			if (this.options.useActiveItemAsLink) {
				this.$el.find('ul.dl-submenu').prepend(function () {
					var activeLi = $(this).parents('li:not(.dl-back):first');
					var parentli = activeLi.find('a:first');
					if (activeLi.hasClass('mobile-clickable'))
						return '<li class="dl-parent"><a href="' + parentli.attr('href') + '">' + self.options.showCurrentLabel + '</a></li>';
					else
						return '';
				});
			}
		},
		_initEvents: function () {
			var self = this;
			this.$trigger.on('click.dlmenu', function () {
				if (self.open) {
					self._closeMenu();
				} else {
					self._openMenu();
					$body.off('click').children().on('click.dlmenu', function () {
						self._closeMenu();
					});
				}
				return false;
			});
			this.$menuitems.on('click.dlmenu', function (event) {
				event.stopPropagation();
				var $item = $(this),
					$submenu = $item.children('ul.dl-submenu');
				if (($submenu.length > 0) && !($(event.currentTarget).hasClass('dl-subviewopen'))) {
					var $flyin = $submenu.clone().css('opacity', 0).insertAfter(self.$menu),
						onAnimationEndFn = function () {
							self.$menu.off(self.animEndEventName).removeClass(self.options.animationClasses.classout).addClass('dl-subview');
							$item.addClass('dl-subviewopen').parents('.dl-subviewopen:first').removeClass('dl-subviewopen').addClass('dl-subview');
							$flyin.remove();
						};
					setTimeout(function () {
						$flyin.addClass(self.options.animationClasses.classin);
						self.$menu.addClass(self.options.animationClasses.classout);
						if (self.supportAnimations) {
							self.$menu.on(self.animEndEventName, onAnimationEndFn);
						} else {
							onAnimationEndFn.call();
						}
						self.options.onLevelClick($item, $item.children('a:first').text());
					});
					return false;
				} else {
					self.options.onLinkClick($item, event);
				}
			});
			this.$back.on('click.dlmenu', function (event) {
				var $this = $(this),
					$submenu = $this.parents('ul.dl-submenu:first'),
					$item = $submenu.parent(),
					$flyin = $submenu.clone().insertAfter(self.$menu);
				var onAnimationEndFn = function () {
					self.$menu.off(self.animEndEventName).removeClass(self.options.animationClasses.classin);
					$flyin.remove();
				};
				setTimeout(function () {
					$flyin.addClass(self.options.animationClasses.classout);
					self.$menu.addClass(self.options.animationClasses.classin);
					if (self.supportAnimations) {
						self.$menu.on(self.animEndEventName, onAnimationEndFn);
					} else {
						onAnimationEndFn.call();
					}
					$item.removeClass('dl-subviewopen');
					var $subview = $this.parents('.dl-subview:first');
					if ($subview.is('li')) {
						$subview.addClass('dl-subviewopen');
					}
					$subview.removeClass('dl-subview');
				});
				return false;
			});
		},
		closeMenu: function () {
			if (this.open) {
				this._closeMenu();
			}
		},
		_closeMenu: function () {
			var self = this,
				onTransitionEndFn = function () {
					self.$menu.off(self.transEndEventName);
					self._resetMenu();
				};
			this.$menu.removeClass('dl-menuopen');
			this.$menu.addClass('dl-menu-toggle');
			this.$trigger.removeClass('dl-active');
			if (this.supportTransitions) {
				this.$menu.on(this.transEndEventName, onTransitionEndFn);
			} else {
				onTransitionEndFn.call();
			}
			this.open = false;
		},
		openMenu: function () {
			if (!this.open) {
				this._openMenu();
			}
		},
		_openMenu: function () {
			var self = this;
			$body.off('click').on('click.dlmenu', function () {
				self._closeMenu();
			});
			this.$menu.addClass('dl-menuopen dl-menu-toggle').on(this.transEndEventName, function () {
				$(this).removeClass('dl-menu-toggle');
			});
			this.$trigger.addClass('dl-active');
			this.open = true;
		},
		_resetMenu: function () {
			this.$menu.removeClass('dl-subview');
			this.$menuitems.removeClass('dl-subview dl-subviewopen');
		}
	};
	var logError = function (message) {
		if (window.console) {
			window.console.error(message);
		}
	};
	$.fn.dlmenu = function (options) {
		if (typeof options === 'string') {
			var args = Array.prototype.slice.call(arguments, 1);
			this.each(function () {
				var instance = $.data(this, 'dlmenu');
				if (!instance) {
					logError("cannot call methods on dlmenu prior to initialization; " + "attempted to call method '" + options + "'");
					return;
				}
				if (!$.isFunction(instance[options]) || options.charAt(0) === "_") {
					logError("no such method '" + options + "' for dlmenu instance");
					return;
				}
				instance[options].apply(instance, args);
			});
		} else {
			this.each(function () {
				var instance = $.data(this, 'dlmenu');
				if (instance) {
					instance._init();
				} else {
					instance = $.data(this, 'dlmenu', new $.DLMenu(options, this));
				}
			});
		}
		return this;
	};
})(jQuery, window);;

function supportsTransitions() {
	return getSupportedTransition() != '';
}

function getSupportedTransition() {
	var b = document.body || document.documentElement,
		s = b.style,
		p = 'transition';
	if (typeof s[p] == 'string') {
		return p;
	}
	var v = ['Moz', 'webkit', 'Webkit', 'Khtml', 'O', 'ms'];
	p = p.charAt(0).toUpperCase() + p.substr(1);
	for (var i = 0; i < v.length; i++) {
		if (typeof s[v[i] + p] == 'string') {
			return true;
		}
	}
	return '';
}
window.supportedTransition = getSupportedTransition();
window.supportsTransitions = supportsTransitions();

function supportsAnimations() {
	return getSupportedAnimation() != '';
}

function getSupportedAnimation() {
	var t, el = document.createElement("fakeelement");
	var animations = {
		"animation": "animationend",
		"OAnimation": "oAnimationEnd",
		"MozAnimation": "animationend",
		"WebkitAnimation": "webkitAnimationEnd",
		'msAnimation': 'MSAnimationEnd'
	};
	for (t in animations) {
		if (el.style[t] !== undefined) {
			return t;
		}
	}
	return '';
}
window.supportedAnimation = getSupportedAnimation();
window.supportsAnimations = supportsAnimations();

function getMobileMenuType() {
	var m = document.getElementById('site-header').className.match(/mobile-menu-layout-([a-zA-Z0-9]+)/);
	window.gemMobileMenuType = m ? m[1] : 'default';
	return window.gemMobileMenuType;
}
getMobileMenuType();
(function () {
	var logoFixTimeout = false;

	function getElementPosition(elem) {
		var w = elem.offsetWidth,
			h = elem.offsetHeight,
			l = 0,
			t = 0;
		while (elem) {
			l += elem.offsetLeft;
			t += elem.offsetTop;
			elem = elem.offsetParent;
		}
		return {
			"left": l,
			"top": t,
			"width": w,
			"height": h
		};
	}

	function fixMenuLogoPosition() {
		if (logoFixTimeout) {
			clearTimeout(logoFixTimeout);
		}
		var headerMain = document.querySelector('#site-header .header-main');
		if (headerMain == null) {
			return false;
		}
		var headerMainClass = headerMain.className;
		if (headerMainClass.indexOf('logo-position-menu_center') == -1 || headerMainClass.indexOf('header-layout-fullwidth_hamburger') != -1 || headerMainClass.indexOf('header-layout-vertical') != -1) {
			return false;
		}
		logoFixTimeout = setTimeout(function () {
			var page = document.getElementById('page'),
				primaryMenu = document.getElementById('primary-menu'),
				primaryNavigation = document.getElementById('primary-navigation'),
				windowWidth = page.offsetWidth;
			if (headerMainClass.indexOf('header-layout-fullwidth') != -1) {
				var logoItem = primaryMenu.querySelector('.menu-item-logo'),
					lastItem = primaryNavigation.querySelector('#primary-menu > li:last-child');
				primaryMenu.style.display = '';
				logoItem.style.marginLeft = '';
				logoItem.style.marginRight = '';
				if (windowWidth < 1212) {
					return;
				}
				primaryMenu.style.display = 'block';
				var pageCenter = windowWidth / 2,
					logoOffset = getElementPosition(logoItem),
					offset = pageCenter - logoOffset.left - logoItem.offsetWidth / 2;
				logoItem.style.marginLeft = offset + 'px';
				var primaryMenuOffsetWidth = primaryMenu.offsetWidth,
					primaryMenuOffsetLeft = getElementPosition(primaryMenu).left,
					lastItemOffsetWidth = lastItem.offsetWidth,
					lastItemOffsetLeft = getElementPosition(lastItem).left,
					rightItemsOffset = primaryMenuOffsetWidth - lastItemOffsetLeft - lastItemOffsetWidth + primaryMenuOffsetLeft;
				logoItem.style.marginRight = rightItemsOffset + 'px';
			} else {
				if (windowWidth < 1212) {
					primaryNavigation.style.textAlign = '';
					primaryMenu.style.position = '';
					primaryMenu.style.left = '';
					return;
				}
				primaryNavigation.style.textAlign = 'left';
				primaryMenu.style.left = 0 + 'px';
				var pageCenter = windowWidth / 2,
					logoOffset = getElementPosition(document.querySelector('#site-header .header-main #primary-navigation .menu-item-logo')),
					pageOffset = getElementPosition(page),
					offset = pageCenter - (logoOffset.left - pageOffset.left) - document.querySelector('#site-header .header-main #primary-navigation .menu-item-logo').offsetWidth / 2;
				primaryMenu.style.position = 'relative';
				primaryMenu.style.left = offset + 'px';
			}
		}, 50);
	}
	window.fixMenuLogoPosition = fixMenuLogoPosition;
})();
(function ($) {
	var isVerticalMenu = $('.header-main').hasClass('header-layout-vertical'),
		isHamburgerMenu = $('.header-main').hasClass('header-layout-fullwidth_hamburger');
	$(window).resize(function () {
		window.updateGemClientSize(false);
		window.updateGemInnerSize();
	});
	window.menuResizeTimeoutHandler = false;
	var megaMenuSettings = {};

	function getOffset(elem) {
		if (elem.getBoundingClientRect && window.gemBrowser.platform.name != 'ios') {
			var bound = elem.getBoundingClientRect(),
				html = elem.ownerDocument.documentElement,
				htmlScroll = getScroll(html),
				elemScrolls = getScrolls(elem),
				isFixed = (styleString(elem, 'position') == 'fixed');
			return {
				x: parseInt(bound.left) + elemScrolls.x + ((isFixed) ? 0 : htmlScroll.x) - html.clientLeft,
				y: parseInt(bound.top) + elemScrolls.y + ((isFixed) ? 0 : htmlScroll.y) - html.clientTop
			};
		}
		var element = elem,
			position = {
				x: 0,
				y: 0
			};
		if (isBody(elem)) return position;
		while (element && !isBody(element)) {
			position.x += element.offsetLeft;
			position.y += element.offsetTop;
			if (window.gemBrowser.name == 'firefox') {
				if (!borderBox(element)) {
					position.x += leftBorder(element);
					position.y += topBorder(element);
				}
				var parent = element.parentNode;
				if (parent && styleString(parent, 'overflow') != 'visible') {
					position.x += leftBorder(parent);
					position.y += topBorder(parent);
				}
			} else if (element != elem && window.gemBrowser.name == 'safari') {
				position.x += leftBorder(element);
				position.y += topBorder(element);
			}
			element = element.offsetParent;
		}
		if (window.gemBrowser.name == 'firefox' && !borderBox(elem)) {
			position.x -= leftBorder(elem);
			position.y -= topBorder(elem);
		}
		return position;
	};

	function getScroll(elem) {
		return {
			x: window.pageXOffset || document.documentElement.scrollLeft,
			y: window.pageYOffset || document.documentElement.scrollTop
		};
	};

	function getScrolls(elem) {
		var element = elem.parentNode,
			position = {
				x: 0,
				y: 0
			};
		while (element && !isBody(element)) {
			position.x += element.scrollLeft;
			position.y += element.scrollTop;
			element = element.parentNode;
		}
		return position;
	};

	function styleString(element, style) {
		return $(element).css(style);
	};

	function styleNumber(element, style) {
		return parseInt(styleString(element, style)) || 0;
	};

	function borderBox(element) {
		return styleString(element, '-moz-box-sizing') == 'border-box';
	};

	function topBorder(element) {
		return styleNumber(element, 'border-top-width');
	};

	function leftBorder(element) {
		return styleNumber(element, 'border-left-width');
	};

	function isBody(element) {
		return (/^(?:body|html)$/i).test(element.tagName);
	};

	function checkMegaMenuSettings() {
		if (window.customMegaMenuSettings == undefined || window.customMegaMenuSettings == null) {
			return false;
		}
		var uri = window.location.pathname;
		window.customMegaMenuSettings.forEach(function (item) {
			for (var i = 0; i < item.urls.length; i++) {
				if (uri.match(item.urls[i])) {
					megaMenuSettings[item.menuItem] = item.data;
				}
			}
		});
	}

	function fixMegaMenuWithSettings() {
		checkMegaMenuSettings();
		$('#primary-menu > li.megamenu-enable').each(function () {
			var m = this.className.match(/(menu-item-(\d+))/);
			if (!m) {
				return;
			}
			var itemId = parseInt(m[2]);
			if (megaMenuSettings[itemId] == undefined || megaMenuSettings[itemId] == null) {
				return;
			}
			var $item = $('> ul', this);
			if (megaMenuSettings[itemId].masonry != undefined) {
				if (megaMenuSettings[itemId].masonry) {
					$item.addClass('megamenu-masonry');
				} else {
					$item.removeClass('megamenu-masonry');
				}
			}
			if (megaMenuSettings[itemId].style != undefined) {
				$(this).removeClass('megamenu-style-default megamenu-style-grid').addClass('megamenu-style-' + megaMenuSettings[itemId].style);
			}
			var css = {};
			if (megaMenuSettings[itemId].backgroundImage != undefined) {
				css.backgroundImage = megaMenuSettings[itemId].backgroundImage;
			}
			if (megaMenuSettings[itemId].backgroundPosition != undefined) {
				css.backgroundPosition = megaMenuSettings[itemId].backgroundPosition;
			}
			if (megaMenuSettings[itemId].padding != undefined) {
				css.padding = megaMenuSettings[itemId].padding;
			}
			if (megaMenuSettings[itemId].borderRight != undefined) {
				css.borderRight = megaMenuSettings[itemId].borderRight;
			}
			$item.css(css);
		});
	}

	function isResponsiveMenuVisible() {
		return $('.primary-navigation .menu-toggle').is(':visible');
	}
	window.isResponsiveMenuVisible = isResponsiveMenuVisible;

	function isTopAreaVisible() {
		return window.gemSettings.topAreaMobileDisable ? window.gemOptions.clientWidth >= 768 : true;
	}
	window.isTopAreaVisible = isTopAreaVisible;

	function isVerticalToggleVisible() {
		return window.gemOptions.clientWidth > 1600;
	}
	$('#primary-menu > li.megamenu-enable').hover(function () {
		fix_megamenu_position(this);
	}, function () {});
	$('#primary-menu > li.megamenu-enable:hover').each(function () {
		fix_megamenu_position(this);
	});
	$('#primary-menu > li.megamenu-enable').each(function () {
		var $item = $('> ul', this);
		if ($item.length == 0) return;
		$item.addClass('megamenu-item-inited');
	});

	function fix_megamenu_position(elem) {
		if ($('#thegem-perspective').length > 0) {
			return false;
		}
		if (!$('.megamenu-inited', elem).length && isResponsiveMenuVisible()) {
			return false;
		}
		var $item = $('> ul', elem);
		if ($item.length == 0) return;
		var self = $item.get(0);
		$item.addClass('megamenu-item-inited');
		var default_item_css = {
			width: 'auto',
			height: 'auto'
		};
		if (!isVerticalMenu && !isHamburgerMenu) {
			default_item_css.left = 0;
		}
		$item.removeClass('megamenu-masonry-inited megamenu-fullwidth').css(default_item_css);
		$(' > li', $item).css({
			left: 0,
			top: 0
		}).each(function () {
			var old_width = $(this).data('old-width') || -1;
			if (old_width != -1) {
				$(this).width(old_width).data('old-width', -1);
			}
		});
		if (isResponsiveMenuVisible()) {
			return;
		}
		if (isVerticalMenu) {
			var container_width = window.gemOptions.clientWidth - $('#site-header-wrapper').outerWidth();
		} else if (isHamburgerMenu) {
			var container_width = window.gemOptions.clientWidth - $('#primary-menu').outerWidth();
		} else {
			var $container = $item.closest('.header-main'),
				container_width = $container.width(),
				container_padding_left = parseInt($container.css('padding-left')),
				container_padding_right = parseInt($container.css('padding-right')),
				parent_width = $item.parent().outerWidth();
		}
		var megamenu_width = $item.outerWidth();
		if (megamenu_width > container_width) {
			megamenu_width = container_width;
			var new_megamenu_width = container_width - parseInt($item.css('padding-left')) - parseInt($item.css('padding-right'));
			var columns = $item.data('megamenu-columns') || 4;
			var column_width = parseFloat(new_megamenu_width - columns * parseInt($(' > li:first', $item).css('margin-left'))) / columns;
			var column_width_int = parseInt(column_width);
			$(' > li', $item).each(function () {
				$(this).data('old-width', $(this).width()).css('width', column_width_int);
			});
			$item.addClass('megamenu-fullwidth').width(new_megamenu_width - (column_width - column_width_int) * columns);
		}
		if (!isVerticalMenu && !isHamburgerMenu) {
			if (megamenu_width > parent_width) {
				var left = -(megamenu_width - parent_width) / 2;
			} else {
				var left = 0;
			}
			var container_offset = getOffset($container[0]);
			var megamenu_offset = getOffset(self);
			if ((megamenu_offset.x - container_offset.x - container_padding_left + left) < 0) {
				left = -(megamenu_offset.x - container_offset.x - container_padding_left);
			}
			if ((megamenu_offset.x + megamenu_width + left) > (container_offset.x + $container.outerWidth() - container_padding_right)) {
				left -= (megamenu_offset.x + megamenu_width + left) - (container_offset.x + $container.outerWidth() - container_padding_right);
			}
			$item.css('left', left).css('left');
		}
		if ($item.hasClass('megamenu-masonry')) {
			var positions = {},
				max_bottom = 0;
			$item.width($item.width() - 1);
			var new_row_height = $('.megamenu-new-row', $item).outerHeight() + parseInt($('.megamenu-new-row', $item).css('margin-bottom'));
			$('> li.menu-item', $item).each(function () {
				var pos = $(this).position();
				if (positions[pos.left] != null && positions[pos.left] != undefined) {
					var top_position = positions[pos.left];
				} else {
					var top_position = pos.top;
				}
				positions[pos.left] = top_position + $(this).outerHeight() + new_row_height + parseInt($(this).css('margin-bottom'));
				if (positions[pos.left] > max_bottom)
					max_bottom = positions[pos.left];
				$(this).css({
					left: pos.left,
					top: top_position
				})
			});
			$item.height(max_bottom - new_row_height - parseInt($item.css('padding-top')) - 1);
			$item.addClass('megamenu-masonry-inited');
		}
		if ($item.hasClass('megamenu-empty-right')) {
			var mega_width = $item.width();
			var max_rights = {
				columns: [],
				position: -1
			};
			$('> li.menu-item', $item).removeClass('megamenu-no-right-border').each(function () {
				var pos = $(this).position();
				var column_right_position = pos.left + $(this).width();
				if (column_right_position > max_rights.position) {
					max_rights.position = column_right_position;
					max_rights.columns = [];
				}
				if (column_right_position == max_rights.position) {
					max_rights.columns.push($(this));
				}
			});
			if (max_rights.columns.length && max_rights.position >= (mega_width - 7)) {
				max_rights.columns.forEach(function ($li) {
					$li.addClass('megamenu-no-right-border');
				});
			}
		}
		if (isVerticalMenu || isHamburgerMenu) {
			var clientHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight,
				itemOffset = $item.offset(),
				itemHeight = $item.outerHeight(),
				scrollTop = $(window).scrollTop();
			if (itemOffset.top - scrollTop + itemHeight > clientHeight) {
				$item.css({
					top: clientHeight - itemOffset.top + scrollTop - itemHeight - 20
				});
			}
		}
		$item.addClass('megamenu-inited');
	}

	function primary_menu_reinit() {
		if (isResponsiveMenuVisible()) {
			if (window.gemMobileMenuType == 'default') {
				var $submenuDisabled = $('#primary-navigation .dl-submenu-disabled');
				if ($submenuDisabled.length) {
					$submenuDisabled.addClass('dl-submenu').removeClass('dl-submenu-disabled');
				}
			}
			if ($('#primary-menu').hasClass('no-responsive')) {
				$('#primary-menu').removeClass('no-responsive');
			}
			if (!$('#primary-navigation').hasClass('responsive')) {
				$('#primary-navigation').addClass('responsive');
			}
			window.fixMenuLogoPosition();
		} else {
			if (window.gemMobileMenuType == 'overlay' && !$('.header-layout-overlay').length && $('.menu-overlay').hasClass('active')) {
				$('.mobile-menu-layout-overlay .menu-toggle').click();
			}
			$('#primary-navigation').addClass('without-transition');
			if (window.gemMobileMenuType == 'default') {
				$('#primary-navigation .dl-submenu').addClass('dl-submenu-disabled').removeClass('dl-submenu');
			}
			$('#primary-menu').addClass('no-responsive');
			$('#primary-navigation').removeClass('responsive');
			window.fixMenuLogoPosition();
			setTimeout(function () {
				$('#primary-menu ul:not(.minicart ul), #primary-menu .minicart, #primary-menu .minisearch').each(function () {
					var $item = $(this);
					var self = this;
					$item.removeClass('invert');
					$item.removeClass('vertical-invert');
					$item.css({
						top: ''
					});
					if ($item.closest('.megamenu-enable').size() == 0 && $item.closest('.header-layout-overlay').size() == 0 && $item.closest('.mobile-menu-layout-overlay').size() == 0) {
						if ($item.offset().left - $('#page').offset().left + $item.outerWidth() > $('#page').width()) {
							$item.addClass('invert');
						}
						if ($('.header-main').first().hasClass('header-style-vertical')) {
							if ($item.offset().top - $('#site-header').offset().top + $item.outerHeight() > $(window).height()) {
								$item.addClass('vertical-invert');
								$item.css({
									top: -($item.offset().top - $('#site-header').offset().top + $item.outerHeight() - $(window).height()) + 'px'
								});
							}
						} else if ($item, $item.parents('#primary-menu ul').length) {
							if ($item.offset().top - $('#site-header').offset().top + $item.outerHeight() > $(window).height()) {
								$item.addClass('vertical-invert');
								$item.css({
									top: -($item.offset().top - $('#site-header').offset().top + $item.outerHeight() - $(window).height()) + 'px'
								});
							}
						}
					}
				});
			}, 50);
			$('#primary-navigation').removeClass('without-transition');
		}
	}
	if (window.gemMobileMenuType == 'default') {
		$('#primary-navigation .submenu-languages').addClass('dl-submenu');
	}
	$('#primary-navigation > ul> li.menu-item-language').addClass('menu-item-parent');
	fixMegaMenuWithSettings();
	if (window.gemMobileMenuType == 'default' && !$('.header-layout-overlay').length) {
		$('#primary-navigation').dlmenu({
			animationClasses: {
				classin: 'dl-animate-in',
				classout: 'dl-animate-out'
			},
			onLevelClick: function (el, name) {
				$('html, body').animate({
					scrollTop: 0
				});
			},
			backLabel: thegem_dlmenu_settings.backLabel,
			showCurrentLabel: thegem_dlmenu_settings.showCurrentLabel
		});
	}
	primary_menu_reinit();
	$('.hamburger-toggle').click(function (e) {
		e.preventDefault();
		$(this).closest('#primary-navigation').toggleClass('hamburger-active');
		$('.hamburger-overlay').toggleClass('active');
	});
	$('.overlay-toggle, .mobile-menu-layout-overlay .menu-toggle').click(function (e) {
		e.preventDefault();
		if ($('.menu-overlay').hasClass('active')) {
			$('.menu-overlay').removeClass('active');
			$('.primary-navigation').addClass('close');
			$('.primary-navigation').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function (e) {
				$('.primary-navigation').removeClass('overlay-active close');
				$('.overlay-menu-wrapper').removeClass('active');
			});
		} else {
			$('.overlay-menu-wrapper').addClass('active');
			$('.primary-navigation').off('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend');
			$('.primary-navigation').addClass('overlay-active').removeClass('close');
			$('.menu-overlay').addClass('active');
		}
	});
	$('.mobile-menu-layout-slide-horizontal .menu-toggle, .mobile-menu-layout-slide-vertical .menu-toggle,.mobile-menu-slide-wrapper .mobile-menu-slide-close').click(function (e) {
		e.preventDefault();
		$('.mobile-menu-slide-wrapper').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function (e) {
			$(this).removeClass('animation');
		});
		$('.mobile-menu-slide-wrapper').addClass('animation').toggleClass('opened');
	});
	$('.mobile-menu-layout-slide-horizontal .primary-navigation #primary-menu .menu-item-parent-toggle, .mobile-menu-layout-slide-vertical .primary-navigation #primary-menu .menu-item-parent-toggle').on('click', function (e) {
		e.preventDefault();
		var self = this;
		$(this).closest('li').toggleClass('opened');
		$(this).siblings('ul').slideToggle(200, function () {
			if (!$(self).closest('li').hasClass('opened')) {
				$(self).siblings('ul').find('li').removeClass('opened')
				$(self).siblings('ul').find('ul').css('display', '');
			}
		});
	});
	$('.header-layout-overlay #primary-menu a, .mobile-menu-layout-overlay .primary-navigation #primary-menu .menu-item-parent-toggle').on('click', function (e) {
		var $itemLink = $(this);
		var $item = $itemLink.closest('li');
		if ($item.hasClass('menu-item-parent')) {
			e.preventDefault();
			if ($item.hasClass('menu-overlay-item-open')) {
				$(' > ul, .menu-overlay-item-open > ul', $item).each(function () {
					$(this).css({
						height: $(this).outerHeight() + 'px'
					});
				});
				setTimeout(function () {
					$(' > ul, .menu-overlay-item-open > ul', $item).css({
						height: 0
					});
					$('.menu-overlay-item-open', $item).add($item).removeClass('menu-overlay-item-open');
				}, 50);
			} else {
				var $oldActive = $('.primary-navigation .menu-overlay-item-open').not($item.parents());
				$('> ul', $oldActive).not($item.parents()).each(function () {
					$(this).css({
						height: $(this).outerHeight() + 'px'
					});
				});
				setTimeout(function () {
					$('> ul', $oldActive).not($item.parents()).css({
						height: 0
					});
					$oldActive.removeClass('menu-overlay-item-open');
				}, 50);
				$('> ul', $item).css({
					height: 'auto'
				});
				var itemHeight = $('> ul', $item).outerHeight();
				$('> ul', $item).css({
					height: 0
				});
				setTimeout(function () {
					$('> ul', $item).css({
						height: itemHeight + 'px'
					});
					$item.addClass('menu-overlay-item-open');
					$('> ul', $item).one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function () {
						$('> ul', $item).css({
							height: 'auto'
						});
					});
				}, 50);
			}
		}
	});
	$('.vertical-toggle').click(function (e) {
		e.preventDefault();
		$(this).closest('#site-header-wrapper').toggleClass('vertical-active');
	});
	$(function () {
		$(window).resize(function () {
			if (window.menuResizeTimeoutHandler) {
				clearTimeout(window.menuResizeTimeoutHandler);
			}
			window.menuResizeTimeoutHandler = setTimeout(primary_menu_reinit, 50);
		});
	});
	$('#primary-navigation a').click(function (e) {
		var $item = $(this);
		if ($('#primary-menu').hasClass('no-responsive') && window.gemSettings.isTouch && $item.next('ul').length) {
			e.preventDefault();
		}
	});
})(jQuery);
(function ($) {
	var transitionEndEvent = {
			'WebkitTransition': 'webkitTransitionEnd',
			'MozTransition': 'transitionend',
			'OTransition': 'oTransitionEnd',
			'msTransition': 'MSTransitionEnd',
			'transition': 'transitionend'
		}[window.supportedTransition],
		clickEventName = window.gemSettings.isTouch ? 'touchstart' : 'click';

	function initPerspective() {
		var $menuToggleButton = $('.perspective-toggle'),
			$perspective = $('#thegem-perspective'),
			$page = $('#page');
		$menuToggleButton.on(clickEventName, function (event) {
			var documentScrollTop = $(window).scrollTop();
			$(window).scrollTop(0);
			$perspective.addClass('modalview animate');
			$page.scrollTop(documentScrollTop);
			event.preventDefault();
			event.stopPropagation ? event.stopPropagation() : (event.cancelBubble = true);
		});
		$page.on(clickEventName, function (event) {
			if (!$perspective.hasClass('animate')) {
				return;
			}
			var onEndTransitionCallback = function (event) {
				if (window.supportsTransitions && (event.originalEvent.target.id !== 'page' || event.originalEvent.propertyName.indexOf('transform') == -1)) {
					return;
				}
				$(this).off(transitionEndEvent, onEndTransitionCallback);
				var pageScrollTop = $page.scrollTop();
				$perspective.removeClass('modalview');
				$(window).scrollTop(pageScrollTop);
				$page.scrollTop(0);
			};
			if (window.supportsTransitions) {
				$perspective.on(transitionEndEvent, onEndTransitionCallback);
			} else {
				onEndTransitionCallback.call();
			}
			$perspective.removeClass('animate');
		});
	}
	initPerspective();
})(jQuery);;
(function (document, navigator, CACHE, IE9TO11) {
	if (IE9TO11) document.addEventListener('DOMContentLoaded', function () {
		[].forEach.call(document.querySelectorAll('use'), function (use) {
			var
				svg = use.parentNode,
				url = use.getAttribute('xlink:href').split('#'),
				url_root = url[0],
				url_hash = url[1],
				xhr = CACHE[url_root] = CACHE[url_root] || new XMLHttpRequest();
			if (!xhr.s) {
				xhr.s = [];
				xhr.open('GET', url_root);
				xhr.onload = function () {
					var x = document.createElement('x'),
						s = xhr.s;
					x.innerHTML = xhr.responseText;
					xhr.onload = function () {
						s.splice(0).map(function (array) {
							var g = x.querySelector('#' + array[2]);
							if (g) array[0].replaceChild(g.cloneNode(true), array[1]);
						});
					};
					xhr.onload();
				};
				xhr.send();
			}
			xhr.s.push([svg, use, url_hash]);
			if (xhr.responseText) xhr.onload();
		});
	});
})(document, navigator, {}, /Trident\/[567]\b/.test(navigator.userAgent));;
(function ($) {
	$.fn.checkbox = function () {
		$(this).each(function () {
			var $el = $(this);
			var typeClass = $el.attr('type');
			$el.hide();
			$el.next('.' + typeClass + '-sign').remove();
			var $checkbox = $('<span class="' + typeClass + '-sign" />').insertAfter($el);
			$checkbox.click(function () {
				if ($el.attr('type') == 'radio') {
					$el.prop('checked', true).trigger('change').trigger('click');
				} else {
					$el.prop('checked', !($el.is(':checked'))).trigger('change');
				}
			});
			$el.change(function () {
				$('input[name="' + $el.attr('name') + '"]').each(function () {
					if ($(this).is(':checked')) {
						$(this).next('.' + $(this).attr('type') + '-sign').addClass('checked');
					} else {
						$(this).next('.' + $(this).attr('type') + '-sign').removeClass('checked');
					}
				});
			});
			if ($el.is(':checked')) {
				$checkbox.addClass('checked');
			} else {
				$checkbox.removeClass('checked');
			}
		});
	}
	$.fn.combobox = function () {
		$(this).each(function () {
			var $el = $(this);
			$el.insertBefore($el.parent('.combobox-wrapper'));
			$el.next('.combobox-wrapper').remove();
			$el.css({
				'opacity': 0,
				'position': 'absolute',
				'left': 0,
				'right': 0,
				'top': 0,
				'bottom': 0
			});
			var $comboWrap = $('<span class="combobox-wrapper" />').insertAfter($el);
			var $text = $('<span class="combobox-text" />').appendTo($comboWrap);
			var $button = $('<span class="combobox-button" />').appendTo($comboWrap);
			$el.appendTo($comboWrap);
			$el.change(function () {
				$text.text($('option:selected', $el).text());
			});
			$text.text($('option:selected', $el).text());
			$el.comboWrap = $comboWrap;
		});
	}
})(jQuery);;
jQuery.easing['jswing'] = jQuery.easing['swing'];
jQuery.extend(jQuery.easing, {
	def: 'easeOutQuad',
	swing: function (x, t, b, c, d) {
		return jQuery.easing[jQuery.easing.def](x, t, b, c, d);
	},
	easeInQuad: function (x, t, b, c, d) {
		return c * (t /= d) * t + b;
	},
	easeOutQuad: function (x, t, b, c, d) {
		return -c * (t /= d) * (t - 2) + b;
	},
	easeInOutQuad: function (x, t, b, c, d) {
		if ((t /= d / 2) < 1) return c / 2 * t * t + b;
		return -c / 2 * ((--t) * (t - 2) - 1) + b;
	},
	easeInCubic: function (x, t, b, c, d) {
		return c * (t /= d) * t * t + b;
	},
	easeOutCubic: function (x, t, b, c, d) {
		return c * ((t = t / d - 1) * t * t + 1) + b;
	},
	easeInOutCubic: function (x, t, b, c, d) {
		if ((t /= d / 2) < 1) return c / 2 * t * t * t + b;
		return c / 2 * ((t -= 2) * t * t + 2) + b;
	},
	easeInQuart: function (x, t, b, c, d) {
		return c * (t /= d) * t * t * t + b;
	},
	easeOutQuart: function (x, t, b, c, d) {
		return -c * ((t = t / d - 1) * t * t * t - 1) + b;
	},
	easeInOutQuart: function (x, t, b, c, d) {
		if ((t /= d / 2) < 1) return c / 2 * t * t * t * t + b;
		return -c / 2 * ((t -= 2) * t * t * t - 2) + b;
	},
	easeInQuint: function (x, t, b, c, d) {
		return c * (t /= d) * t * t * t * t + b;
	},
	easeOutQuint: function (x, t, b, c, d) {
		return c * ((t = t / d - 1) * t * t * t * t + 1) + b;
	},
	easeInOutQuint: function (x, t, b, c, d) {
		if ((t /= d / 2) < 1) return c / 2 * t * t * t * t * t + b;
		return c / 2 * ((t -= 2) * t * t * t * t + 2) + b;
	},
	easeInSine: function (x, t, b, c, d) {
		return -c * Math.cos(t / d * (Math.PI / 2)) + c + b;
	},
	easeOutSine: function (x, t, b, c, d) {
		return c * Math.sin(t / d * (Math.PI / 2)) + b;
	},
	easeInOutSine: function (x, t, b, c, d) {
		return -c / 2 * (Math.cos(Math.PI * t / d) - 1) + b;
	},
	easeInExpo: function (x, t, b, c, d) {
		return (t == 0) ? b : c * Math.pow(2, 10 * (t / d - 1)) + b;
	},
	easeOutExpo: function (x, t, b, c, d) {
		return (t == d) ? b + c : c * (-Math.pow(2, -10 * t / d) + 1) + b;
	},
	easeInOutExpo: function (x, t, b, c, d) {
		if (t == 0) return b;
		if (t == d) return b + c;
		if ((t /= d / 2) < 1) return c / 2 * Math.pow(2, 10 * (t - 1)) + b;
		return c / 2 * (-Math.pow(2, -10 * --t) + 2) + b;
	},
	easeInCirc: function (x, t, b, c, d) {
		return -c * (Math.sqrt(1 - (t /= d) * t) - 1) + b;
	},
	easeOutCirc: function (x, t, b, c, d) {
		return c * Math.sqrt(1 - (t = t / d - 1) * t) + b;
	},
	easeInOutCirc: function (x, t, b, c, d) {
		if ((t /= d / 2) < 1) return -c / 2 * (Math.sqrt(1 - t * t) - 1) + b;
		return c / 2 * (Math.sqrt(1 - (t -= 2) * t) + 1) + b;
	},
	easeInElastic: function (x, t, b, c, d) {
		var s = 1.70158;
		var p = 0;
		var a = c;
		if (t == 0) return b;
		if ((t /= d) == 1) return b + c;
		if (!p) p = d * .3;
		if (a < Math.abs(c)) {
			a = c;
			var s = p / 4;
		} else var s = p / (2 * Math.PI) * Math.asin(c / a);
		return -(a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p)) + b;
	},
	easeOutElastic: function (x, t, b, c, d) {
		var s = 1.70158;
		var p = 0;
		var a = c;
		if (t == 0) return b;
		if ((t /= d) == 1) return b + c;
		if (!p) p = d * .3;
		if (a < Math.abs(c)) {
			a = c;
			var s = p / 4;
		} else var s = p / (2 * Math.PI) * Math.asin(c / a);
		return a * Math.pow(2, -10 * t) * Math.sin((t * d - s) * (2 * Math.PI) / p) + c + b;
	},
	easeInOutElastic: function (x, t, b, c, d) {
		var s = 1.70158;
		var p = 0;
		var a = c;
		if (t == 0) return b;
		if ((t /= d / 2) == 2) return b + c;
		if (!p) p = d * (.3 * 1.5);
		if (a < Math.abs(c)) {
			a = c;
			var s = p / 4;
		} else var s = p / (2 * Math.PI) * Math.asin(c / a);
		if (t < 1) return -.5 * (a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p)) + b;
		return a * Math.pow(2, -10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p) * .5 + c + b;
	},
	easeInBack: function (x, t, b, c, d, s) {
		if (s == undefined) s = 1.70158;
		return c * (t /= d) * t * ((s + 1) * t - s) + b;
	},
	easeOutBack: function (x, t, b, c, d, s) {
		if (s == undefined) s = 1.70158;
		return c * ((t = t / d - 1) * t * ((s + 1) * t + s) + 1) + b;
	},
	easeInOutBack: function (x, t, b, c, d, s) {
		if (s == undefined) s = 1.70158;
		if ((t /= d / 2) < 1) return c / 2 * (t * t * (((s *= (1.525)) + 1) * t - s)) + b;
		return c / 2 * ((t -= 2) * t * (((s *= (1.525)) + 1) * t + s) + 2) + b;
	},
	easeInBounce: function (x, t, b, c, d) {
		return c - jQuery.easing.easeOutBounce(x, d - t, 0, c, d) + b;
	},
	easeOutBounce: function (x, t, b, c, d) {
		if ((t /= d) < (1 / 2.75)) {
			return c * (7.5625 * t * t) + b;
		} else if (t < (2 / 2.75)) {
			return c * (7.5625 * (t -= (1.5 / 2.75)) * t + .75) + b;
		} else if (t < (2.5 / 2.75)) {
			return c * (7.5625 * (t -= (2.25 / 2.75)) * t + .9375) + b;
		} else {
			return c * (7.5625 * (t -= (2.625 / 2.75)) * t + .984375) + b;
		}
	},
	easeInOutBounce: function (x, t, b, c, d) {
		if (t < d / 2) return jQuery.easing.easeInBounce(x, t * 2, 0, c, d) * .5 + b;
		return jQuery.easing.easeOutBounce(x, t * 2 - d, 0, c, d) * .5 + c * .5 + b;
	}
});;
(function ($) {
	function HeaderAnimation(el, options) {
		this.el = el;
		this.$el = $(el);
		this.options = {
			startTop: 1
		};
		$.extend(this.options, options);
		this.initialize();
	}
	HeaderAnimation.prototype = {
		initialize: function () {
			var self = this;
			this.$wrapper = $('#site-header-wrapper');
			this.$topArea = $('#top-area');
			this.topAreaInSiteHeader = $('#site-header #top-area').length > 0;
			this.$headerMain = $('.header-main', this.$el);
			this.hasAdminBar = document.body.className.indexOf('admin-bar') != -1;
			this.htmlOffset = this.hasAdminBar ? parseInt($('html').css('margin-top')) : 0;
			this.scrollTop = 0;
			this.topOffset = 0;
			this.settedWrapperHeight = false;
			this.initedForDesktop = false;
			this.initedForMobile = false;
			this.hideWrapper = this.$wrapper.hasClass('site-header-wrapper-transparent');
			this.videoBackground = $('.page-title-block .gem-video-background').length && $('.page-title-block .gem-video-background').data('headerup');
			if (this.$el.hasClass('header-on-slideshow') && $('#main-content > *').first().is('.gem-slideshow, .block-slideshow')) {
				this.$wrapper.css({
					position: 'absolute'
				});
			}
			if (this.$el.hasClass('header-on-slideshow') && $('#main-content > *').first().is('.gem-slideshow, .block-slideshow')) {
				this.$wrapper.addClass('header-on-slideshow');
			} else {
				this.$el.removeClass('header-on-slideshow');
			}
			if (this.videoBackground) {
				this.$el.addClass('header-on-slideshow');
				this.$wrapper.addClass('header-on-slideshow');
			}
			this.initForDesktop();
			this.initForMobile();
			$(window).scroll(function () {
				self.scrollHandler();
			});
			$(window).resize(function () {
				self.initForDesktop();
				self.initForMobile();
				self.scrollHandler();
				setTimeout(function () {
					self.initializeHeight();
				}, 350);
			});
		},
		initForDesktop: function () {
			if (window.isResponsiveMenuVisible() || this.initedForDesktop) {
				return false;
			}
			this.initializeHeight();
			this.initializeStyles();
			if (this.$topArea.length && this.$topArea.is(':visible') && !this.topAreaInSiteHeader)
				this.options.startTop = this.$topArea.outerHeight();
		},
		initForMobile: function () {
			if (!window.isResponsiveMenuVisible() || this.initedForMobile) {
				return false;
			}
			if (this.$topArea.length && this.$topArea.is(':visible') && !this.topAreaInSiteHeader)
				this.options.startTop = this.$topArea.outerHeight();
		},
		setMargin: function ($img) {
			var $small = $img.siblings('img.small'),
				w = 0;
			if (this.$headerMain.hasClass('logo-position-right')) {
				w = $small.width();
			} else if (this.$headerMain.hasClass('logo-position-center') || this.$headerMain.hasClass('logo-position-menu_center')) {
				w = $img.width();
				var smallWidth = $small.width(),
					offset = (w - smallWidth) / 2;
				w = smallWidth + offset;
				$small.css('margin-right', offset + 'px');
			}
			if (!w) {
				w = $img.width();
			}
			$small.css('margin-left', '-' + w + 'px');
			$img.parent().css('min-width', w + 'px');
			$small.show();
		},
		initializeStyles: function () {
			var self = this;
			if (this.$headerMain.hasClass('logo-position-menu_center')) {
				var $img = $('#primary-navigation .menu-item-logo a .logo img.default', this.$el);
			} else {
				var $img = $('.site-title a .logo img.default', this.$el);
			}
			if ($img.length && $img.is(':visible') && $img[0].complete) {
				self.setMargin($img);
				self.initializeHeight();
			} else {
				$img.on('load error', function () {
					self.setMargin($img);
					self.initializeHeight();
				});
			}
		},
		initializeHeight: function () {
			if (window.isResponsiveMenuVisible()) {
				this.$el.removeClass('shrink fixed');
				if (this.settedWrapperHeight) {
					this.$wrapper.css({
						height: ''
					});
				}
				return false;
			}
			if (this.hideWrapper) {
				return false;
			}
			that = this;
			setTimeout(function () {
				var shrink = that.$el.hasClass('shrink');
				that.$el.removeClass('shrink');
				var elHeight = that.$el.outerHeight();
				that.$wrapper.height(elHeight);
				that.settedWrapperHeight = true;
				if (shrink) {
					that.$el.addClass('shrink');
				}
			}, 50);
		},
		updateTopOffset: function () {
			var offset = this.htmlOffset;
			if (this.$wrapper.hasClass('header-on-slideshow') && !this.$el.hasClass('fixed'))
				offset = 0;
			scrollTop = this.getScrollY();
			if (this.options.startTop > 0) {
				if (scrollTop < this.options.startTop)
					offset += this.options.startTop - scrollTop;
			}
			if (this.topOffset != offset) {
				this.topOffset = offset;
				this.$wrapper.css('top', offset + 'px');
			}
		},
		scrollHandlerMobile: function () {
			this.updateTopOffset();
		},
		scrollHandler: function () {
			if (window.isResponsiveMenuVisible()) {
				this.scrollHandlerMobile();
				return false;
			} else {
				this.$wrapper.css('top', '');
			}
			if (this.getScrollY() >= this.options.startTop) {
				if (!this.$el.hasClass('shrink')) {
					var shrinkClass = 'shrink fixed';
					if (window.gemSettings.fillTopArea) {
						shrinkClass += ' fill';
					}
					this.$el.addClass(shrinkClass)
					if (this.hasAdminBar) {
						this.$el.css({
							top: this.htmlOffset
						});
					}
				}
			} else {
				if (this.$el.hasClass('shrink')) {
					this.$el.removeClass('shrink fixed')
					if (this.hasAdminBar) {
						this.$el.css({
							top: ''
						});
					}
				}
			}
		},
		updateScrollTop: function () {
			this.scrollTop = $(window).scrollTop();
		},
		getScrollY: function () {
			return window.pageYOffset || document.documentElement.scrollTop;
		},
	};
	$.fn.headerAnimation = function (options) {
		options = options || {};
		return new HeaderAnimation(this.get(0), options);
	};
})(jQuery);;
(function ($) {
	var prefixes = 'Webkit Moz ms Ms O'.split(' ');
	var docElemStyle = document.documentElement.style;

	function getStyleProperty(propName) {
		if (!propName) {
			return;
		}
		if (typeof docElemStyle[propName] === 'string') {
			return propName;
		}
		propName = propName.charAt(0).toUpperCase() + propName.slice(1);
		var prefixed;
		for (var i = 0, len = prefixes.length; i < len; i++) {
			prefixed = prefixes[i] + propName;
			if (typeof docElemStyle[prefixed] === 'string') {
				return prefixed;
			}
		}
	}
	var transitionProperty = getStyleProperty('transition');
	var transitionEndEvent = {
		WebkitTransition: 'webkitTransitionEnd',
		MozTransition: 'transitionend',
		OTransition: 'otransitionend',
		transition: 'transitionend'
	}[transitionProperty];

	function getElementData(element, attributeNameCamel, attributeName, defaultValue) {
		if (element.dataset != undefined) {
			if (element.dataset[attributeNameCamel] != undefined) {
				return element.dataset[attributeNameCamel];
			} else {
				var value = $(element).data(attributeName);
				if (value == undefined) {
					return defaultValue;
				}
				return value;
			}
			return element.dataset[attributeNameCamel] != undefined ? element.dataset[attributeNameCamel] : defaultValue;
		}
		var value = this.getAttribute(attributeName);
		return value != null && value != '' ? value : defaultValue;
	}

	function Queue(lazyInstance) {
		this.lazyInstance = lazyInstance;
		this.queue = [];
		this.running = false;
		this.initTimer();
	}
	Queue.prototype = {
		add: function (element) {
			this.queue.push(element);
		},
		next: function () {
			if (this.running || this.queue.length == 0) return false;
			this.running = true;
			var element = this.queue.shift();
			if (element.isOnTop()) {
				element.forceShow();
				this.finishPosition();
				return;
			}
			element.startAnimation();
		},
		finishPosition: function () {
			this.running = false;
			this.next();
		},
		initTimer: function () {
			var self = this;
			this.timer = document.createElement('div');
			this.timer.className = 'lazy-loading-timer-element';
			document.body.appendChild(this.timer);
			this.timerCallback = function () {};
			$(this.timer).bind(transitionEndEvent, function (event) {
				self.timerCallback();
			});
			this.timer.className += ' start-timer';
		},
		startTimer: function (callback) {
			this.timerCallback = callback;
			if (this.timer.className.indexOf('start-timer') != -1) {
				this.timer.className = this.timer.className.replace(' start-timer', '');
			} else {
				this.timer.className += ' start-timer';
			}
		}
	};

	function Group(el, lazyInstance) {
		this.el = el;
		this.$el = $(el);
		this.lazyInstance = lazyInstance;
		this.elements = [];
		this.showed = false;
		this.finishedElementsCount = 0;
		this.position = {
			left: 0,
			top: 0
		};
		this.options = {
			offset: parseFloat(getElementData(el, 'llOffset', 'll-offset', 0.7)),
			itemDelay: getElementData(el, 'llItemDelay', 'll-item-delay', -1),
			isFirst: lazyInstance.hasHeaderVisuals && this.el.className.indexOf('lazy-loading-first') != -1,
			force: getElementData(el, 'llForceStart', 'll-force-start', 0) != 0,
			finishDelay: getElementData(el, 'llFinishDelay', 'll-finish-delay', 200)
		};
		this.$el.addClass('lazy-loading-before-start-animation');
	}
	timeNow = function () {
		var newDate = new Date();
		return ((newDate.getHours() < 10) ? "0" : "") + newDate.getHours() + ":" + ((newDate.getMinutes() < 10) ? "0" : "") + newDate.getMinutes() + ":" + ((newDate.getSeconds() < 10) ? "0" : "") + newDate.getSeconds();
	}
	Group.prototype = {
		addElement: function (element) {
			this.elements.push(element);
		},
		setElements: function (elements) {
			this.elements = elements;
		},
		getElements: function () {
			return this.elements;
		},
		getElementsCount: function () {
			return this.elements.length;
		},
		getItemDelay: function () {
			return this.options.itemDelay;
		},
		updatePosition: function () {
			this.position = $(this.el).offset();
		},
		getPosition: function () {
			return this.position;
		},
		isShowed: function () {
			return this.showed;
		},
		isVisible: function () {
			if (this.options.force) return true;
			return this.position.top + this.options.offset * this.el.offsetHeight <= this.lazyInstance.getWindowBottom();
		},
		isOnTop: function () {
			return this.position.top + this.el.offsetHeight < this.lazyInstance.getWindowBottom() - this.lazyInstance.getWindowHeight();
		},
		show: function () {
			this.lazyInstance.queue.add(this);
			this.showed = true;
		},
		forceShow: function () {
			this.showed = true;
			this.el.className = this.el.className.replace('lazy-loading-before-start-animation', 'lazy-loading-end-animation');
		},
		startAnimation: function () {
			var self = this;
			self.elements.forEach(function (element) {
				element.$el.bind(transitionEndEvent, function (event) {
					var target = event.target || event.srcElement;
					if (target != element.el) {
						return;
					}
					element.$el.unbind(transitionEndEvent);
					self.finishedElementsCount++;
					if (self.finishedElementsCount >= self.getElementsCount()) {
						var className = self.el.className.replace('lazy-loading-before-start-animation', '').replace('lazy-loading-start-animation', 'lazy-loading-end-animation');
						self.el.className = className;
					}
				});
				element.show();
			});
			if (self.options.finishDelay > 0) {
				self.lazyInstance.queue.startTimer(function () {
					self.finishAnimation();
				});
			} else {
				self.finishAnimation();
			}
			self.$el.addClass('lazy-loading-start-animation');
		},
		finishAnimation: function () {
			this.lazyInstance.queue.finishPosition();
		}
	};

	function Element(el, group) {
		this.el = el;
		this.$el = $(el);
		this.group = group;
		this.options = {
			effect: getElementData(el, 'llEffect', 'll-effect', ''),
			delay: getElementData(el, 'llItemDelay', 'll-item-delay', group.getItemDelay()),
			actionFunction: getElementData(el, 'llActionFunc', 'll-action-func', '')
		};
		this.options.queueType = this.options.delay != -1 ? 'async' : 'sync';
		if (this.options.effect != '') {
			this.$el.addClass('lazy-loading-item-' + this.getEffectClass());
		}
	}
	Element.prototype = {
		effects: {
			action: function (element) {
				if (!element.options.actionFunction || window[element.options.actionFunction] == null || window[element.options.actionFunction] == undefined) {
					return;
				}
				window[element.options.actionFunction](element.el);
			}
		},
		getEffectClass: function () {
			var effectClass = this.options.effect;
			if (effectClass == 'drop-right-without-wrap' || effectClass == 'drop-right-unwrap') {
				return 'drop-right';
			}
			return effectClass;
		},
		show: function () {
			if (this.effects[this.options.effect] != undefined) {
				this.effects[this.options.effect](this);
			}
		}
	};
	LazyLoading.prototype = {
		initialize: function () {
			this.queue = new Queue(this);
			this.groups = [];
			this.hasHeaderVisuals = $('.ls-wp-container').length > 0;
			this.$checkPoint = $('#lazy-loading-point');
			if (!this.$checkPoint.length) {
				$('<div id="lazy-loading-point"></div>').insertAfter('#main');
				this.$checkPoint = $('#lazy-loading-point');
			}
			this.windowBottom = 0;
			this.windowHeight = 0;
			this.scrollHandle = false;
			$(document).ready(this.documentReady.bind(this));
		},
		documentReady: function () {
			this.updateCheckPointOffset();
			this.updateWindowHeight();
			this.buildGroups();
			this.windowScroll();
			$(window).scroll(this.windowScroll.bind(this));
			$(window).resize(this.windowResize.bind(this));
		},
		windowResize: function () {
			this.updateWindowHeight();
			this.updateGroups();
			this.windowScroll();
		},
		buildGroups: function () {
			var self = this;
			self.groups = [];
			$('.lazy-loading').each(function () {
				var group = new Group(this, self);
				group.updatePosition();
				$('.lazy-loading-item', this).each(function () {
					group.addElement(new Element(this, group));
				});
				if (group.getElementsCount() > 0) {
					self.groups.push(group);
				}
			});
		},
		updateGroups: function () {
			var self = this;
			self.groups.forEach(function (group) {
				if (group.isShowed()) {
					return;
				}
				group.updatePosition();
			});
		},
		windowScroll: function () {
			if (this.scrollHandle) {}
			this.scrollHandle = true;
			this.calculateWindowBottom();
			if (this.isGroupsPositionsChanged()) {
				this.updateGroups();
			}
			this.groups.forEach(function (group) {
				if (group.isShowed()) {
					return;
				}
				if (group.isOnTop()) {
					group.forceShow();
				}
				if (group.isVisible()) {
					group.show();
				}
			});
			this.scrollHandle = false;
			this.queue.next();
		},
		calculateWindowBottom: function () {
			this.windowBottom = $(window).scrollTop() + this.windowHeight;
		},
		getWindowBottom: function () {
			return this.windowBottom;
		},
		updateWindowHeight: function () {
			this.windowHeight = $(window).height();
		},
		getWindowHeight: function () {
			return this.windowHeight;
		},
		updateCheckPointOffset: function () {
			this.checkPointOffset = this.$checkPoint.offset().top;
		},
		isGroupsPositionsChanged: function () {
			var oldCheckPointOffset = this.checkPointOffset;
			this.updateCheckPointOffset();
			return Math.abs(this.checkPointOffset - oldCheckPointOffset) > 1;
		},
		getLastGroup: function () {
			if (!this.groups.length) {
				return null;
			}
			return this.groups[this.groups.length - 1];
		}
	};

	function LazyLoading(options) {
		this.options = {};
		$.extend(this.options, options);
		this.initialize();
	}
	$.lazyLoading = function (options) {
		return new LazyLoading(options);
	}
})(jQuery);;
(function ($, window, document, Math, undefined) {
	var div = document.createElement("div"),
		divStyle = div.style,
		suffix = "Transform",
		testProperties = ["O" + suffix, "ms" + suffix, "Webkit" + suffix, "Moz" + suffix],
		i = testProperties.length,
		supportProperty, supportMatrixFilter, supportFloat32Array = "Float32Array" in window,
		propertyHook, propertyGet, rMatrix = /Matrix([^)]*)/,
		rAffine = /^\s*matrix\(\s*1\s*,\s*0\s*,\s*0\s*,\s*1\s*(?:,\s*0(?:px)?\s*){2}\)\s*$/,
		_transform = "transform",
		_transformOrigin = "transformOrigin",
		_translate = "translate",
		_rotate = "rotate",
		_scale = "scale",
		_skew = "skew",
		_matrix = "matrix";
	while (i--) {
		if (testProperties[i] in divStyle) {
			$.support[_transform] = supportProperty = testProperties[i];
			$.support[_transformOrigin] = supportProperty + "Origin";
			continue;
		}
	}
	if (!supportProperty) {
		$.support.matrixFilter = supportMatrixFilter = divStyle.filter === "";
	}
	$.cssNumber[_transform] = $.cssNumber[_transformOrigin] = true;
	if (supportProperty && supportProperty != _transform) {
		$.cssProps[_transform] = supportProperty;
		$.cssProps[_transformOrigin] = supportProperty + "Origin";
		if (supportProperty == "Moz" + suffix) {
			propertyHook = {
				get: function (elem, computed) {
					return (computed ? $.css(elem, supportProperty).split("px").join("") : elem.style[supportProperty]);
				},
				set: function (elem, value) {
					elem.style[supportProperty] = /matrix\([^)p]*\)/.test(value) ? value.replace(/matrix((?:[^,]*,){4})([^,]*),([^)]*)/, _matrix + "$1$2px,$3px") : value;
				}
			};
		} else if (/^1\.[0-5](?:\.|$)/.test($.fn.jquery)) {
			propertyHook = {
				get: function (elem, computed) {
					return (computed ? $.css(elem, supportProperty.replace(/^ms/, "Ms")) : elem.style[supportProperty]);
				}
			};
		}
	} else if (supportMatrixFilter) {
		propertyHook = {
			get: function (elem, computed, asArray) {
				var elemStyle = (computed && elem.currentStyle ? elem.currentStyle : elem.style),
					matrix, data;
				if (elemStyle && rMatrix.test(elemStyle.filter)) {
					matrix = RegExp.$1.split(",");
					matrix = [matrix[0].split("=")[1], matrix[2].split("=")[1], matrix[1].split("=")[1], matrix[3].split("=")[1]];
				} else {
					matrix = [1, 0, 0, 1];
				}
				if (!$.cssHooks[_transformOrigin]) {
					matrix[4] = elemStyle ? parseInt(elemStyle.left, 10) || 0 : 0;
					matrix[5] = elemStyle ? parseInt(elemStyle.top, 10) || 0 : 0;
				} else {
					data = $._data(elem, "transformTranslate", undefined);
					matrix[4] = data ? data[0] : 0;
					matrix[5] = data ? data[1] : 0;
				}
				return asArray ? matrix : _matrix + "(" + matrix + ")";
			},
			set: function (elem, value, animate) {
				var elemStyle = elem.style,
					currentStyle, Matrix, filter, centerOrigin;
				if (!animate) {
					elemStyle.zoom = 1;
				}
				value = matrix(value);
				Matrix = ["Matrix(" + "M11=" + value[0], "M12=" + value[2], "M21=" + value[1], "M22=" + value[3], "SizingMethod='auto expand'"].join();
				filter = (currentStyle = elem.currentStyle) && currentStyle.filter || elemStyle.filter || "";
				elemStyle.filter = rMatrix.test(filter) ? filter.replace(rMatrix, Matrix) : filter + " progid:DXImageTransform.Microsoft." + Matrix + ")";
				if (!$.cssHooks[_transformOrigin]) {
					if ((centerOrigin = $.transform.centerOrigin)) {
						elemStyle[centerOrigin == "margin" ? "marginLeft" : "left"] = -(elem.offsetWidth / 2) + (elem.clientWidth / 2) + "px";
						elemStyle[centerOrigin == "margin" ? "marginTop" : "top"] = -(elem.offsetHeight / 2) + (elem.clientHeight / 2) + "px";
					}
					elemStyle.left = value[4] + "px";
					elemStyle.top = value[5] + "px";
				} else {
					$.cssHooks[_transformOrigin].set(elem, value);
				}
			}
		};
	}
	if (propertyHook) {
		$.cssHooks[_transform] = propertyHook;
	}
	propertyGet = propertyHook && propertyHook.get || $.css;
	$.fx.step.transform = function (fx) {
		var elem = fx.elem,
			start = fx.start,
			end = fx.end,
			pos = fx.pos,
			transform = "",
			precision = 1E5,
			i, startVal, endVal, unit;
		if (!start || typeof start === "string") {
			if (!start) {
				start = propertyGet(elem, supportProperty);
			}
			if (supportMatrixFilter) {
				elem.style.zoom = 1;
			}
			end = end.split("+=").join(start);
			$.extend(fx, interpolationList(start, end));
			start = fx.start;
			end = fx.end;
		}
		i = start.length;
		while (i--) {
			startVal = start[i];
			endVal = end[i];
			unit = +false;
			switch (startVal[0]) {
				case _translate:
					unit = "px";
				case _scale:
					unit || (unit = "");
					transform = startVal[0] + "(" +
						Math.round((startVal[1][0] + (endVal[1][0] - startVal[1][0]) * pos) * precision) / precision + unit + "," +
						Math.round((startVal[1][1] + (endVal[1][1] - startVal[1][1]) * pos) * precision) / precision + unit + ")" +
						transform;
					break;
				case _skew + "X":
				case _skew + "Y":
				case _rotate:
					transform = startVal[0] + "(" +
						Math.round((startVal[1] + (endVal[1] - startVal[1]) * pos) * precision) / precision + "rad)" +
						transform;
					break;
			}
		}
		fx.origin && (transform = fx.origin + transform);
		propertyHook && propertyHook.set ? propertyHook.set(elem, transform, +true) : elem.style[supportProperty] = transform;
	};

	function matrix(transform) {
		transform = transform.split(")");
		var
			trim = $.trim,
			i = -1,
			l = transform.length - 1,
			split, prop, val, prev = supportFloat32Array ? new Float32Array(6) : [],
			curr = supportFloat32Array ? new Float32Array(6) : [],
			rslt = supportFloat32Array ? new Float32Array(6) : [1, 0, 0, 1, 0, 0];
		prev[0] = prev[3] = rslt[0] = rslt[3] = 1;
		prev[1] = prev[2] = prev[4] = prev[5] = 0;
		while (++i < l) {
			split = transform[i].split("(");
			prop = trim(split[0]);
			val = split[1];
			curr[0] = curr[3] = 1;
			curr[1] = curr[2] = curr[4] = curr[5] = 0;
			switch (prop) {
				case _translate + "X":
					curr[4] = parseInt(val, 10);
					break;
				case _translate + "Y":
					curr[5] = parseInt(val, 10);
					break;
				case _translate:
					val = val.split(",");
					curr[4] = parseInt(val[0], 10);
					curr[5] = parseInt(val[1] || 0, 10);
					break;
				case _rotate:
					val = toRadian(val);
					curr[0] = Math.cos(val);
					curr[1] = Math.sin(val);
					curr[2] = -Math.sin(val);
					curr[3] = Math.cos(val);
					break;
				case _scale + "X":
					curr[0] = +val;
					break;
				case _scale + "Y":
					curr[3] = val;
					break;
				case _scale:
					val = val.split(",");
					curr[0] = val[0];
					curr[3] = val.length > 1 ? val[1] : val[0];
					break;
				case _skew + "X":
					curr[2] = Math.tan(toRadian(val));
					break;
				case _skew + "Y":
					curr[1] = Math.tan(toRadian(val));
					break;
				case _matrix:
					val = val.split(",");
					curr[0] = val[0];
					curr[1] = val[1];
					curr[2] = val[2];
					curr[3] = val[3];
					curr[4] = parseInt(val[4], 10);
					curr[5] = parseInt(val[5], 10);
					break;
			}
			rslt[0] = prev[0] * curr[0] + prev[2] * curr[1];
			rslt[1] = prev[1] * curr[0] + prev[3] * curr[1];
			rslt[2] = prev[0] * curr[2] + prev[2] * curr[3];
			rslt[3] = prev[1] * curr[2] + prev[3] * curr[3];
			rslt[4] = prev[0] * curr[4] + prev[2] * curr[5] + prev[4];
			rslt[5] = prev[1] * curr[4] + prev[3] * curr[5] + prev[5];
			prev = [rslt[0], rslt[1], rslt[2], rslt[3], rslt[4], rslt[5]];
		}
		return rslt;
	}

	function unmatrix(matrix) {
		var
			scaleX, scaleY, skew, A = matrix[0],
			B = matrix[1],
			C = matrix[2],
			D = matrix[3];
		if (A * D - B * C) {
			scaleX = Math.sqrt(A * A + B * B);
			A /= scaleX;
			B /= scaleX;
			skew = A * C + B * D;
			C -= A * skew;
			D -= B * skew;
			scaleY = Math.sqrt(C * C + D * D);
			C /= scaleY;
			D /= scaleY;
			skew /= scaleY;
			if (A * D < B * C) {
				A = -A;
				B = -B;
				skew = -skew;
				scaleX = -scaleX;
			}
		} else {
			scaleX = scaleY = skew = 0;
		}
		return [[_translate, [+matrix[4], +matrix[5]]], [_rotate, Math.atan2(B, A)], [_skew + "X", Math.atan(skew)], [_scale, [scaleX, scaleY]]];
	}

	function interpolationList(start, end) {
		var list = {
				start: [],
				end: []
			},
			i = -1,
			l, currStart, currEnd, currType;
		(start == "none" || isAffine(start)) && (start = "");
		(end == "none" || isAffine(end)) && (end = "");
		if (start && end && !end.indexOf("matrix") && toArray(start).join() == toArray(end.split(")")[0]).join()) {
			list.origin = start;
			start = "";
			end = end.slice(end.indexOf(")") + 1);
		}
		if (!start && !end) {
			return;
		}
		if (!start || !end || functionList(start) == functionList(end)) {
			start && (start = start.split(")")) && (l = start.length);
			end && (end = end.split(")")) && (l = end.length);
			while (++i < l - 1) {
				start[i] && (currStart = start[i].split("("));
				end[i] && (currEnd = end[i].split("("));
				currType = $.trim((currStart || currEnd)[0]);
				append(list.start, parseFunction(currType, currStart ? currStart[1] : 0));
				append(list.end, parseFunction(currType, currEnd ? currEnd[1] : 0));
			}
		} else {
			list.start = unmatrix(matrix(start));
			list.end = unmatrix(matrix(end))
		}
		return list;
	}

	function parseFunction(type, value) {
		var
			defaultValue = +(!type.indexOf(_scale)),
			scaleX, cat = type.replace(/e[XY]/, "e");
		switch (type) {
			case _translate + "Y":
			case _scale + "Y":
				value = [defaultValue, value ? parseFloat(value) : defaultValue];
				break;
			case _translate + "X":
			case _translate:
			case _scale + "X":
				scaleX = 1;
			case _scale:
				value = value ? (value = value.split(",")) && [parseFloat(value[0]), parseFloat(value.length > 1 ? value[1] : type == _scale ? scaleX || value[0] : defaultValue + "")] : [defaultValue, defaultValue];
				break;
			case _skew + "X":
			case _skew + "Y":
			case _rotate:
				value = value ? toRadian(value) : 0;
				break;
			case _matrix:
				return unmatrix(value ? toArray(value) : [1, 0, 0, 1, 0, 0]);
				break;
		}
		return [[cat, value]];
	}

	function isAffine(matrix) {
		return rAffine.test(matrix);
	}

	function functionList(transform) {
		return transform.replace(/(?:\([^)]*\))|\s/g, "");
	}

	function append(arr1, arr2, value) {
		while (value = arr2.shift()) {
			arr1.push(value);
		}
	}

	function toRadian(value) {
		return ~value.indexOf("deg") ? parseInt(value, 10) * (Math.PI * 2 / 360) : ~value.indexOf("grad") ? parseInt(value, 10) * (Math.PI / 200) : parseFloat(value);
	}

	function toArray(matrix) {
		matrix = /([^,]*),([^,]*),([^,]*),([^,]*),([^,p]*)(?:px)?,([^)p]*)(?:px)?/.exec(matrix);
		return [matrix[1], matrix[2], matrix[3], matrix[4], matrix[5], matrix[6]];
	}
	$.transform = {
		centerOrigin: "margin"
	};
})(jQuery, window, document, Math);;
/*!
 * jQuery UI Effects 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/category/effects-core/
 */
! function (a) {
	"function" == typeof define && define.amd ? define(["jquery"], a) : a(jQuery)
}(function (a) {
	var b = "ui-effects-",
		c = a;
	/*!
	 * jQuery Color Animations v2.1.2
	 * https://github.com/jquery/jquery-color
	 *
	 * Copyright 2014 jQuery Foundation and other contributors
	 * Released under the MIT license.
	 * http://jquery.org/license
	 *
	 * Date: Wed Jan 16 08:47:09 2013 -0600
	 */
	return a.effects = {
			effect: {}
		},
		function (a, b) {
			function c(a, b, c) {
				var d = l[b.type] || {};
				return null == a ? c || !b.def ? null : b.def : (a = d.floor ? ~~a : parseFloat(a), isNaN(a) ? b.def : d.mod ? (a + d.mod) % d.mod : 0 > a ? 0 : d.max < a ? d.max : a)
			}

			function d(b) {
				var c = j(),
					d = c._rgba = [];
				return b = b.toLowerCase(), o(i, function (a, e) {
					var f, g = e.re.exec(b),
						h = g && e.parse(g),
						i = e.space || "rgba";
					if (h) return f = c[i](h), c[k[i].cache] = f[k[i].cache], d = c._rgba = f._rgba, !1
				}), d.length ? ("0,0,0,0" === d.join() && a.extend(d, f.transparent), c) : f[b]
			}

			function e(a, b, c) {
				return c = (c + 1) % 1, 6 * c < 1 ? a + (b - a) * c * 6 : 2 * c < 1 ? b : 3 * c < 2 ? a + (b - a) * (2 / 3 - c) * 6 : a
			}
			var f, g = "backgroundColor borderBottomColor borderLeftColor borderRightColor borderTopColor color columnRuleColor outlineColor textDecorationColor textEmphasisColor",
				h = /^([\-+])=\s*(\d+\.?\d*)/,
				i = [{
					re: /rgba?\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
					parse: function (a) {
						return [a[1], a[2], a[3], a[4]]
					}
				}, {
					re: /rgba?\(\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
					parse: function (a) {
						return [2.55 * a[1], 2.55 * a[2], 2.55 * a[3], a[4]]
					}
				}, {
					re: /#([a-f0-9]{2})([a-f0-9]{2})([a-f0-9]{2})/,
					parse: function (a) {
						return [parseInt(a[1], 16), parseInt(a[2], 16), parseInt(a[3], 16)]
					}
				}, {
					re: /#([a-f0-9])([a-f0-9])([a-f0-9])/,
					parse: function (a) {
						return [parseInt(a[1] + a[1], 16), parseInt(a[2] + a[2], 16), parseInt(a[3] + a[3], 16)]
					}
				}, {
					re: /hsla?\(\s*(\d+(?:\.\d+)?)\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
					space: "hsla",
					parse: function (a) {
						return [a[1], a[2] / 100, a[3] / 100, a[4]]
					}
				}],
				j = a.Color = function (b, c, d, e) {
					return new a.Color.fn.parse(b, c, d, e)
				},
				k = {
					rgba: {
						props: {
							red: {
								idx: 0,
								type: "byte"
							},
							green: {
								idx: 1,
								type: "byte"
							},
							blue: {
								idx: 2,
								type: "byte"
							}
						}
					},
					hsla: {
						props: {
							hue: {
								idx: 0,
								type: "degrees"
							},
							saturation: {
								idx: 1,
								type: "percent"
							},
							lightness: {
								idx: 2,
								type: "percent"
							}
						}
					}
				},
				l = {
					"byte": {
						floor: !0,
						max: 255
					},
					percent: {
						max: 1
					},
					degrees: {
						mod: 360,
						floor: !0
					}
				},
				m = j.support = {},
				n = a("<p>")[0],
				o = a.each;
			n.style.cssText = "background-color:rgba(1,1,1,.5)", m.rgba = n.style.backgroundColor.indexOf("rgba") > -1, o(k, function (a, b) {
				b.cache = "_" + a, b.props.alpha = {
					idx: 3,
					type: "percent",
					def: 1
				}
			}), j.fn = a.extend(j.prototype, {
				parse: function (e, g, h, i) {
					if (e === b) return this._rgba = [null, null, null, null], this;
					(e.jquery || e.nodeType) && (e = a(e).css(g), g = b);
					var l = this,
						m = a.type(e),
						n = this._rgba = [];
					return g !== b && (e = [e, g, h, i], m = "array"), "string" === m ? this.parse(d(e) || f._default) : "array" === m ? (o(k.rgba.props, function (a, b) {
						n[b.idx] = c(e[b.idx], b)
					}), this) : "object" === m ? (e instanceof j ? o(k, function (a, b) {
						e[b.cache] && (l[b.cache] = e[b.cache].slice())
					}) : o(k, function (b, d) {
						var f = d.cache;
						o(d.props, function (a, b) {
							if (!l[f] && d.to) {
								if ("alpha" === a || null == e[a]) return;
								l[f] = d.to(l._rgba)
							}
							l[f][b.idx] = c(e[a], b, !0)
						}), l[f] && a.inArray(null, l[f].slice(0, 3)) < 0 && (l[f][3] = 1, d.from && (l._rgba = d.from(l[f])))
					}), this) : void 0
				},
				is: function (a) {
					var b = j(a),
						c = !0,
						d = this;
					return o(k, function (a, e) {
						var f, g = b[e.cache];
						return g && (f = d[e.cache] || e.to && e.to(d._rgba) || [], o(e.props, function (a, b) {
							if (null != g[b.idx]) return c = g[b.idx] === f[b.idx]
						})), c
					}), c
				},
				_space: function () {
					var a = [],
						b = this;
					return o(k, function (c, d) {
						b[d.cache] && a.push(c)
					}), a.pop()
				},
				transition: function (a, b) {
					var d = j(a),
						e = d._space(),
						f = k[e],
						g = 0 === this.alpha() ? j("transparent") : this,
						h = g[f.cache] || f.to(g._rgba),
						i = h.slice();
					return d = d[f.cache], o(f.props, function (a, e) {
						var f = e.idx,
							g = h[f],
							j = d[f],
							k = l[e.type] || {};
						null !== j && (null === g ? i[f] = j : (k.mod && (j - g > k.mod / 2 ? g += k.mod : g - j > k.mod / 2 && (g -= k.mod)), i[f] = c((j - g) * b + g, e)))
					}), this[e](i)
				},
				blend: function (b) {
					if (1 === this._rgba[3]) return this;
					var c = this._rgba.slice(),
						d = c.pop(),
						e = j(b)._rgba;
					return j(a.map(c, function (a, b) {
						return (1 - d) * e[b] + d * a
					}))
				},
				toRgbaString: function () {
					var b = "rgba(",
						c = a.map(this._rgba, function (a, b) {
							return null == a ? b > 2 ? 1 : 0 : a
						});
					return 1 === c[3] && (c.pop(), b = "rgb("), b + c.join() + ")"
				},
				toHslaString: function () {
					var b = "hsla(",
						c = a.map(this.hsla(), function (a, b) {
							return null == a && (a = b > 2 ? 1 : 0), b && b < 3 && (a = Math.round(100 * a) + "%"), a
						});
					return 1 === c[3] && (c.pop(), b = "hsl("), b + c.join() + ")"
				},
				toHexString: function (b) {
					var c = this._rgba.slice(),
						d = c.pop();
					return b && c.push(~~(255 * d)), "#" + a.map(c, function (a) {
						return a = (a || 0).toString(16), 1 === a.length ? "0" + a : a
					}).join("")
				},
				toString: function () {
					return 0 === this._rgba[3] ? "transparent" : this.toRgbaString()
				}
			}), j.fn.parse.prototype = j.fn, k.hsla.to = function (a) {
				if (null == a[0] || null == a[1] || null == a[2]) return [null, null, null, a[3]];
				var b, c, d = a[0] / 255,
					e = a[1] / 255,
					f = a[2] / 255,
					g = a[3],
					h = Math.max(d, e, f),
					i = Math.min(d, e, f),
					j = h - i,
					k = h + i,
					l = .5 * k;
				return b = i === h ? 0 : d === h ? 60 * (e - f) / j + 360 : e === h ? 60 * (f - d) / j + 120 : 60 * (d - e) / j + 240, c = 0 === j ? 0 : l <= .5 ? j / k : j / (2 - k), [Math.round(b) % 360, c, l, null == g ? 1 : g]
			}, k.hsla.from = function (a) {
				if (null == a[0] || null == a[1] || null == a[2]) return [null, null, null, a[3]];
				var b = a[0] / 360,
					c = a[1],
					d = a[2],
					f = a[3],
					g = d <= .5 ? d * (1 + c) : d + c - d * c,
					h = 2 * d - g;
				return [Math.round(255 * e(h, g, b + 1 / 3)), Math.round(255 * e(h, g, b)), Math.round(255 * e(h, g, b - 1 / 3)), f]
			}, o(k, function (d, e) {
				var f = e.props,
					g = e.cache,
					i = e.to,
					k = e.from;
				j.fn[d] = function (d) {
					if (i && !this[g] && (this[g] = i(this._rgba)), d === b) return this[g].slice();
					var e, h = a.type(d),
						l = "array" === h || "object" === h ? d : arguments,
						m = this[g].slice();
					return o(f, function (a, b) {
						var d = l["object" === h ? a : b.idx];
						null == d && (d = m[b.idx]), m[b.idx] = c(d, b)
					}), k ? (e = j(k(m)), e[g] = m, e) : j(m)
				}, o(f, function (b, c) {
					j.fn[b] || (j.fn[b] = function (e) {
						var f, g = a.type(e),
							i = "alpha" === b ? this._hsla ? "hsla" : "rgba" : d,
							j = this[i](),
							k = j[c.idx];
						return "undefined" === g ? k : ("function" === g && (e = e.call(this, k), g = a.type(e)), null == e && c.empty ? this : ("string" === g && (f = h.exec(e), f && (e = k + parseFloat(f[2]) * ("+" === f[1] ? 1 : -1))), j[c.idx] = e, this[i](j)))
					})
				})
			}), j.hook = function (b) {
				var c = b.split(" ");
				o(c, function (b, c) {
					a.cssHooks[c] = {
						set: function (b, e) {
							var f, g, h = "";
							if ("transparent" !== e && ("string" !== a.type(e) || (f = d(e)))) {
								if (e = j(f || e), !m.rgba && 1 !== e._rgba[3]) {
									for (g = "backgroundColor" === c ? b.parentNode : b;
										("" === h || "transparent" === h) && g && g.style;) try {
										h = a.css(g, "backgroundColor"), g = g.parentNode
									} catch (i) {}
									e = e.blend(h && "transparent" !== h ? h : "_default")
								}
								e = e.toRgbaString()
							}
							try {
								b.style[c] = e
							} catch (i) {}
						}
					}, a.fx.step[c] = function (b) {
						b.colorInit || (b.start = j(b.elem, c), b.end = j(b.end), b.colorInit = !0), a.cssHooks[c].set(b.elem, b.start.transition(b.end, b.pos))
					}
				})
			}, j.hook(g), a.cssHooks.borderColor = {
				expand: function (a) {
					var b = {};
					return o(["Top", "Right", "Bottom", "Left"], function (c, d) {
						b["border" + d + "Color"] = a
					}), b
				}
			}, f = a.Color.names = {
				aqua: "#00ffff",
				black: "#000000",
				blue: "#0000ff",
				fuchsia: "#ff00ff",
				gray: "#808080",
				green: "#008000",
				lime: "#00ff00",
				maroon: "#800000",
				navy: "#000080",
				olive: "#808000",
				purple: "#800080",
				red: "#ff0000",
				silver: "#c0c0c0",
				teal: "#008080",
				white: "#ffffff",
				yellow: "#ffff00",
				transparent: [null, null, null, 0],
				_default: "#ffffff"
			}
		}(c),
		function () {
			function b(b) {
				var c, d, e = b.ownerDocument.defaultView ? b.ownerDocument.defaultView.getComputedStyle(b, null) : b.currentStyle,
					f = {};
				if (e && e.length && e[0] && e[e[0]])
					for (d = e.length; d--;) c = e[d], "string" == typeof e[c] && (f[a.camelCase(c)] = e[c]);
				else
					for (c in e) "string" == typeof e[c] && (f[c] = e[c]);
				return f
			}

			function d(b, c) {
				var d, e, g = {};
				for (d in c) e = c[d], b[d] !== e && (f[d] || !a.fx.step[d] && isNaN(parseFloat(e)) || (g[d] = e));
				return g
			}
			var e = ["add", "remove", "toggle"],
				f = {
					border: 1,
					borderBottom: 1,
					borderColor: 1,
					borderLeft: 1,
					borderRight: 1,
					borderTop: 1,
					borderWidth: 1,
					margin: 1,
					padding: 1
				};
			a.each(["borderLeftStyle", "borderRightStyle", "borderBottomStyle", "borderTopStyle"], function (b, d) {
				a.fx.step[d] = function (a) {
					("none" !== a.end && !a.setAttr || 1 === a.pos && !a.setAttr) && (c.style(a.elem, d, a.end), a.setAttr = !0)
				}
			}), a.fn.addBack || (a.fn.addBack = function (a) {
				return this.add(null == a ? this.prevObject : this.prevObject.filter(a))
			}), a.effects.animateClass = function (c, f, g, h) {
				var i = a.speed(f, g, h);
				return this.queue(function () {
					var f, g = a(this),
						h = g.attr("class") || "",
						j = i.children ? g.find("*").addBack() : g;
					j = j.map(function () {
						var c = a(this);
						return {
							el: c,
							start: b(this)
						}
					}), f = function () {
						a.each(e, function (a, b) {
							c[b] && g[b + "Class"](c[b])
						})
					}, f(), j = j.map(function () {
						return this.end = b(this.el[0]), this.diff = d(this.start, this.end), this
					}), g.attr("class", h), j = j.map(function () {
						var b = this,
							c = a.Deferred(),
							d = a.extend({}, i, {
								queue: !1,
								complete: function () {
									c.resolve(b)
								}
							});
						return this.el.animate(this.diff, d), c.promise()
					}), a.when.apply(a, j.get()).done(function () {
						f(), a.each(arguments, function () {
							var b = this.el;
							a.each(this.diff, function (a) {
								b.css(a, "")
							})
						}), i.complete.call(g[0])
					})
				})
			}, a.fn.extend({
				addClass: function (b) {
					return function (c, d, e, f) {
						return d ? a.effects.animateClass.call(this, {
							add: c
						}, d, e, f) : b.apply(this, arguments)
					}
				}(a.fn.addClass),
				removeClass: function (b) {
					return function (c, d, e, f) {
						return arguments.length > 1 ? a.effects.animateClass.call(this, {
							remove: c
						}, d, e, f) : b.apply(this, arguments)
					}
				}(a.fn.removeClass),
				toggleClass: function (b) {
					return function (c, d, e, f, g) {
						return "boolean" == typeof d || void 0 === d ? e ? a.effects.animateClass.call(this, d ? {
							add: c
						} : {
							remove: c
						}, e, f, g) : b.apply(this, arguments) : a.effects.animateClass.call(this, {
							toggle: c
						}, d, e, f)
					}
				}(a.fn.toggleClass),
				switchClass: function (b, c, d, e, f) {
					return a.effects.animateClass.call(this, {
						add: c,
						remove: b
					}, d, e, f)
				}
			})
		}(),
		function () {
			function c(b, c, d, e) {
				return a.isPlainObject(b) && (c = b, b = b.effect), b = {
					effect: b
				}, null == c && (c = {}), a.isFunction(c) && (e = c, d = null, c = {}), ("number" == typeof c || a.fx.speeds[c]) && (e = d, d = c, c = {}), a.isFunction(d) && (e = d, d = null), c && a.extend(b, c), d = d || c.duration, b.duration = a.fx.off ? 0 : "number" == typeof d ? d : d in a.fx.speeds ? a.fx.speeds[d] : a.fx.speeds._default, b.complete = e || c.complete, b
			}

			function d(b) {
				return !(b && "number" != typeof b && !a.fx.speeds[b]) || ("string" == typeof b && !a.effects.effect[b] || (!!a.isFunction(b) || "object" == typeof b && !b.effect))
			}
			a.extend(a.effects, {
				version: "1.11.4",
				save: function (a, c) {
					for (var d = 0; d < c.length; d++) null !== c[d] && a.data(b + c[d], a[0].style[c[d]])
				},
				restore: function (a, c) {
					var d, e;
					for (e = 0; e < c.length; e++) null !== c[e] && (d = a.data(b + c[e]), void 0 === d && (d = ""), a.css(c[e], d))
				},
				setMode: function (a, b) {
					return "toggle" === b && (b = a.is(":hidden") ? "show" : "hide"), b
				},
				getBaseline: function (a, b) {
					var c, d;
					switch (a[0]) {
						case "top":
							c = 0;
							break;
						case "middle":
							c = .5;
							break;
						case "bottom":
							c = 1;
							break;
						default:
							c = a[0] / b.height
					}
					switch (a[1]) {
						case "left":
							d = 0;
							break;
						case "center":
							d = .5;
							break;
						case "right":
							d = 1;
							break;
						default:
							d = a[1] / b.width
					}
					return {
						x: d,
						y: c
					}
				},
				createWrapper: function (b) {
					if (b.parent().is(".ui-effects-wrapper")) return b.parent();
					var c = {
							width: b.outerWidth(!0),
							height: b.outerHeight(!0),
							"float": b.css("float")
						},
						d = a("<div></div>").addClass("ui-effects-wrapper").css({
							fontSize: "100%",
							background: "transparent",
							border: "none",
							margin: 0,
							padding: 0
						}),
						e = {
							width: b.width(),
							height: b.height()
						},
						f = document.activeElement;
					try {
						f.id
					} catch (g) {
						f = document.body
					}
					return b.wrap(d), (b[0] === f || a.contains(b[0], f)) && a(f).focus(), d = b.parent(), "static" === b.css("position") ? (d.css({
						position: "relative"
					}), b.css({
						position: "relative"
					})) : (a.extend(c, {
						position: b.css("position"),
						zIndex: b.css("z-index")
					}), a.each(["top", "left", "bottom", "right"], function (a, d) {
						c[d] = b.css(d), isNaN(parseInt(c[d], 10)) && (c[d] = "auto")
					}), b.css({
						position: "relative",
						top: 0,
						left: 0,
						right: "auto",
						bottom: "auto"
					})), b.css(e), d.css(c).show()
				},
				removeWrapper: function (b) {
					var c = document.activeElement;
					return b.parent().is(".ui-effects-wrapper") && (b.parent().replaceWith(b), (b[0] === c || a.contains(b[0], c)) && a(c).focus()), b
				},
				setTransition: function (b, c, d, e) {
					return e = e || {}, a.each(c, function (a, c) {
						var f = b.cssUnit(c);
						f[0] > 0 && (e[c] = f[0] * d + f[1])
					}), e
				}
			}), a.fn.extend({
				effect: function () {
					function b(b) {
						function c() {
							a.isFunction(f) && f.call(e[0]), a.isFunction(b) && b()
						}
						var e = a(this),
							f = d.complete,
							h = d.mode;
						(e.is(":hidden") ? "hide" === h : "show" === h) ? (e[h](), c()) : g.call(e[0], d, c)
					}
					var d = c.apply(this, arguments),
						e = d.mode,
						f = d.queue,
						g = a.effects.effect[d.effect];
					return a.fx.off || !g ? e ? this[e](d.duration, d.complete) : this.each(function () {
						d.complete && d.complete.call(this)
					}) : f === !1 ? this.each(b) : this.queue(f || "fx", b)
				},
				show: function (a) {
					return function (b) {
						if (d(b)) return a.apply(this, arguments);
						var e = c.apply(this, arguments);
						return e.mode = "show", this.effect.call(this, e)
					}
				}(a.fn.show),
				hide: function (a) {
					return function (b) {
						if (d(b)) return a.apply(this, arguments);
						var e = c.apply(this, arguments);
						return e.mode = "hide", this.effect.call(this, e)
					}
				}(a.fn.hide),
				toggle: function (a) {
					return function (b) {
						if (d(b) || "boolean" == typeof b) return a.apply(this, arguments);
						var e = c.apply(this, arguments);
						return e.mode = "toggle", this.effect.call(this, e)
					}
				}(a.fn.toggle),
				cssUnit: function (b) {
					var c = this.css(b),
						d = [];
					return a.each(["em", "px", "%", "pt"], function (a, b) {
						c.indexOf(b) > 0 && (d = [parseFloat(c), b])
					}), d
				}
			})
		}(),
		function () {
			var b = {};
			a.each(["Quad", "Cubic", "Quart", "Quint", "Expo"], function (a, c) {
				b[c] = function (b) {
					return Math.pow(b, a + 2)
				}
			}), a.extend(b, {
				Sine: function (a) {
					return 1 - Math.cos(a * Math.PI / 2)
				},
				Circ: function (a) {
					return 1 - Math.sqrt(1 - a * a)
				},
				Elastic: function (a) {
					return 0 === a || 1 === a ? a : -Math.pow(2, 8 * (a - 1)) * Math.sin((80 * (a - 1) - 7.5) * Math.PI / 15)
				},
				Back: function (a) {
					return a * a * (3 * a - 2)
				},
				Bounce: function (a) {
					for (var b, c = 4; a < ((b = Math.pow(2, --c)) - 1) / 11;);
					return 1 / Math.pow(4, 3 - c) - 7.5625 * Math.pow((3 * b - 2) / 22 - a, 2)
				}
			}), a.each(b, function (b, c) {
				a.easing["easeIn" + b] = c, a.easing["easeOut" + b] = function (a) {
					return 1 - c(1 - a)
				}, a.easing["easeInOut" + b] = function (a) {
					return a < .5 ? c(2 * a) / 2 : 1 - c(a * -2 + 2) / 2
				}
			})
		}(), a.effects
});;
/*!
 * jQuery UI Effects Drop 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/drop-effect/
 */
! function (a) {
	"function" == typeof define && define.amd ? define(["jquery", "./effect"], a) : a(jQuery)
}(function (a) {
	return a.effects.effect.drop = function (b, c) {
		var d, e = a(this),
			f = ["position", "top", "bottom", "left", "right", "opacity", "height", "width"],
			g = a.effects.setMode(e, b.mode || "hide"),
			h = "show" === g,
			i = b.direction || "left",
			j = "up" === i || "down" === i ? "top" : "left",
			k = "up" === i || "left" === i ? "pos" : "neg",
			l = {
				opacity: h ? 1 : 0
			};
		a.effects.save(e, f), e.show(), a.effects.createWrapper(e), d = b.distance || e["top" === j ? "outerHeight" : "outerWidth"](!0) / 2, h && e.css("opacity", 0).css(j, "pos" === k ? -d : d), l[j] = (h ? "pos" === k ? "+=" : "-=" : "pos" === k ? "-=" : "+=") + d, e.animate(l, {
			queue: !1,
			duration: b.duration,
			easing: b.easing,
			complete: function () {
				"hide" === g && e.hide(), a.effects.restore(e, f), a.effects.removeWrapper(e), c()
			}
		})
	}
});;
(function () {
	var COUNT_FRAMERATE, COUNT_MS_PER_FRAME, DIGIT_FORMAT, DIGIT_HTML, DIGIT_SPEEDBOOST, DURATION, FORMAT_MARK_HTML, FORMAT_PARSER, FRAMERATE, FRAMES_PER_VALUE, MS_PER_FRAME, MutationObserver, Odometer, RIBBON_HTML, TRANSITION_END_EVENTS, TRANSITION_SUPPORT, VALUE_HTML, addClass, createFromHTML, fractionalPart, now, removeClass, requestAnimationFrame, round, transitionCheckStyles, trigger, truncate, wrapJQuery, _jQueryWrapped, _old, _ref, _ref1, __slice = [].slice;
	VALUE_HTML = '<span class="odometer-value"></span>';
	RIBBON_HTML = '<span class="odometer-ribbon"><span class="odometer-ribbon-inner">' + VALUE_HTML + '</span></span>';
	DIGIT_HTML = '<span class="odometer-digit"><span class="odometer-digit-spacer">0</span><span class="odometer-digit-inner">' + RIBBON_HTML + '</span></span>';
	FORMAT_MARK_HTML = '<span class="odometer-formatting-mark"></span>';
	DIGIT_FORMAT = '(,ddd).dd';
	FORMAT_PARSER = /^\(?([^)]*)\)?(?:(.)(d+))?$/;
	FRAMERATE = 30;
	DURATION = 2000;
	COUNT_FRAMERATE = 20;
	FRAMES_PER_VALUE = 2;
	DIGIT_SPEEDBOOST = .5;
	MS_PER_FRAME = 1000 / FRAMERATE;
	COUNT_MS_PER_FRAME = 1000 / COUNT_FRAMERATE;
	TRANSITION_END_EVENTS = 'transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd';
	transitionCheckStyles = document.createElement('div').style;
	TRANSITION_SUPPORT = (transitionCheckStyles.transition != null) || (transitionCheckStyles.webkitTransition != null) || (transitionCheckStyles.mozTransition != null) || (transitionCheckStyles.oTransition != null);
	requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
	MutationObserver = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver;
	createFromHTML = function (html) {
		var el;
		el = document.createElement('div');
		el.innerHTML = html;
		return el.children[0];
	};
	removeClass = function (el, name) {
		return el.className = el.className.replace(new RegExp("(^| )" + (name.split(' ').join('|')) + "( |$)", 'gi'), ' ');
	};
	addClass = function (el, name) {
		removeClass(el, name);
		return el.className += " " + name;
	};
	trigger = function (el, name) {
		var evt;
		if (document.createEvent != null) {
			evt = document.createEvent('HTMLEvents');
			evt.initEvent(name, true, true);
			return el.dispatchEvent(evt);
		}
	};
	now = function () {
		var _ref, _ref1;
		return (_ref = (_ref1 = window.performance) != null ? typeof _ref1.now === "function" ? _ref1.now() : void 0 : void 0) != null ? _ref : +(new Date);
	};
	round = function (val, precision) {
		if (precision == null) {
			precision = 0;
		}
		if (!precision) {
			return Math.round(val);
		}
		val *= Math.pow(10, precision);
		val += 0.5;
		val = Math.floor(val);
		return val /= Math.pow(10, precision);
	};
	truncate = function (val) {
		if (val < 0) {
			return Math.ceil(val);
		} else {
			return Math.floor(val);
		}
	};
	fractionalPart = function (val) {
		return val - round(val);
	};
	_jQueryWrapped = false;
	(wrapJQuery = function () {
		var property, _i, _len, _ref, _results;
		if (_jQueryWrapped) {
			return;
		}
		if (window.jQuery != null) {
			_jQueryWrapped = true;
			_ref = ['html', 'text'];
			_results = [];
			for (_i = 0, _len = _ref.length; _i < _len; _i++) {
				property = _ref[_i];
				_results.push((function (property) {
					var old;
					old = window.jQuery.fn[property];
					return window.jQuery.fn[property] = function (val) {
						var _ref1;
						if ((val == null) || (((_ref1 = this[0]) != null ? _ref1.odometer : void 0) == null)) {
							return old.apply(this, arguments);
						}
						return this[0].odometer.update(val);
					};
				})(property));
			}
			return _results;
		}
	})();
	setTimeout(wrapJQuery, 0);
	Odometer = (function () {
		function Odometer(options) {
			var e, k, property, v, _base, _i, _len, _ref, _ref1, _ref2, _this = this;
			this.options = options;
			this.el = this.options.el;
			if (this.el.odometer != null) {
				return this.el.odometer;
			}
			this.el.odometer = this;
			_ref = Odometer.options;
			for (k in _ref) {
				v = _ref[k];
				if (this.options[k] == null) {
					this.options[k] = v;
				}
			}
			if ((_base = this.options).duration == null) {
				_base.duration = DURATION;
			}
			this.MAX_VALUES = ((this.options.duration / MS_PER_FRAME) / FRAMES_PER_VALUE) | 0;
			this.resetFormat();
			this.value = this.cleanValue((_ref1 = this.options.value) != null ? _ref1 : '');
			this.renderInside();
			this.render();
			try {
				_ref2 = ['innerHTML', 'innerText', 'textContent'];
				for (_i = 0, _len = _ref2.length; _i < _len; _i++) {
					property = _ref2[_i];
					if (this.el[property] != null) {
						(function (property) {
							return Object.defineProperty(_this.el, property, {
								get: function () {
									var _ref3;
									if (property === 'innerHTML') {
										return _this.inside.outerHTML;
									} else {
										return (_ref3 = _this.inside.innerText) != null ? _ref3 : _this.inside.textContent;
									}
								},
								set: function (val) {
									return _this.update(val);
								}
							});
						})(property);
					}
				}
			} catch (_error) {
				e = _error;
				this.watchForMutations();
			}
			this;
		}
		Odometer.prototype.renderInside = function () {
			this.inside = document.createElement('div');
			this.inside.className = 'odometer-inside';
			this.el.innerHTML = '';
			return this.el.appendChild(this.inside);
		};
		Odometer.prototype.watchForMutations = function () {
			var e, _this = this;
			if (MutationObserver == null) {
				return;
			}
			try {
				if (this.observer == null) {
					this.observer = new MutationObserver(function (mutations) {
						var newVal;
						newVal = _this.el.innerText;
						_this.renderInside();
						_this.render(_this.value);
						return _this.update(newVal);
					});
				}
				this.watchMutations = true;
				return this.startWatchingMutations();
			} catch (_error) {
				e = _error;
			}
		};
		Odometer.prototype.startWatchingMutations = function () {
			if (this.watchMutations) {
				return this.observer.observe(this.el, {
					childList: true
				});
			}
		};
		Odometer.prototype.stopWatchingMutations = function () {
			var _ref;
			return (_ref = this.observer) != null ? _ref.disconnect() : void 0;
		};
		Odometer.prototype.cleanValue = function (val) {
			var _ref;
			if (typeof val === 'string') {
				val = val.replace((_ref = this.format.radix) != null ? _ref : '.', '<radix>');
				val = val.replace(/[.,]/g, '');
				val = val.replace('<radix>', '.');
				val = parseFloat(val, 10) || 0;
			}
			return round(val, this.format.precision);
		};
		Odometer.prototype.bindTransitionEnd = function () {
			var event, renderEnqueued, _i, _len, _ref, _results, _this = this;
			if (this.transitionEndBound) {
				return;
			}
			this.transitionEndBound = true;
			renderEnqueued = false;
			_ref = TRANSITION_END_EVENTS.split(' ');
			_results = [];
			for (_i = 0, _len = _ref.length; _i < _len; _i++) {
				event = _ref[_i];
				_results.push(this.el.addEventListener(event, function () {
					if (renderEnqueued) {
						return true;
					}
					renderEnqueued = true;
					setTimeout(function () {
						_this.render();
						renderEnqueued = false;
						return trigger(_this.el, 'odometerdone');
					}, 0);
					return true;
				}, false));
			}
			return _results;
		};
		Odometer.prototype.resetFormat = function () {
			var format, fractional, parsed, precision, radix, repeating, _ref, _ref1;
			format = (_ref = this.options.format) != null ? _ref : DIGIT_FORMAT;
			format || (format = 'd');
			parsed = FORMAT_PARSER.exec(format);
			if (!parsed) {
				throw new Error("Odometer: Unparsable digit format");
			}
			_ref1 = parsed.slice(1, 4), repeating = _ref1[0], radix = _ref1[1], fractional = _ref1[2];
			precision = (fractional != null ? fractional.length : void 0) || 0;
			return this.format = {
				repeating: repeating,
				radix: radix,
				precision: precision
			};
		};
		Odometer.prototype.render = function (value) {
			var classes, cls, digit, match, newClasses, theme, wholePart, _i, _j, _len, _len1, _ref;
			if (value == null) {
				value = this.value;
			}
			this.stopWatchingMutations();
			this.resetFormat();
			this.inside.innerHTML = '';
			theme = this.options.theme;
			classes = this.el.className.split(' ');
			newClasses = [];
			for (_i = 0, _len = classes.length; _i < _len; _i++) {
				cls = classes[_i];
				if (!cls.length) {
					continue;
				}
				if (match = /^odometer-theme-(.+)$/.exec(cls)) {
					theme = match[1];
					continue;
				}
				if (/^odometer(-|$)/.test(cls)) {
					continue;
				}
				newClasses.push(cls);
			}
			newClasses.push('odometer');
			if (!TRANSITION_SUPPORT) {
				newClasses.push('odometer-no-transitions');
			}
			if (theme) {
				newClasses.push("odometer-theme-" + theme);
			} else {
				newClasses.push("odometer-auto-theme");
			}
			this.el.className = newClasses.join(' ');
			this.ribbons = {};
			this.digits = [];
			wholePart = !this.format.precision || !fractionalPart(value) || false;
			_ref = value.toString().split('').reverse();
			for (_j = 0, _len1 = _ref.length; _j < _len1; _j++) {
				digit = _ref[_j];
				if (digit === '.') {
					wholePart = true;
				}
				this.addDigit(digit, wholePart);
			}
			return this.startWatchingMutations();
		};
		Odometer.prototype.update = function (newValue) {
			var diff, _this = this;
			newValue = this.cleanValue(newValue);
			if (!(diff = newValue - this.value)) {
				return;
			}
			removeClass(this.el, 'odometer-animating-up odometer-animating-down odometer-animating');
			if (diff > 0) {
				addClass(this.el, 'odometer-animating-up');
			} else {
				addClass(this.el, 'odometer-animating-down');
			}
			this.stopWatchingMutations();
			this.animate(newValue);
			this.startWatchingMutations();
			setTimeout(function () {
				_this.el.offsetHeight;
				return addClass(_this.el, 'odometer-animating');
			}, 0);
			return this.value = newValue;
		};
		Odometer.prototype.renderDigit = function () {
			return createFromHTML(DIGIT_HTML);
		};
		Odometer.prototype.insertDigit = function (digit, before) {
			if (before != null) {
				return this.inside.insertBefore(digit, before);
			} else if (!this.inside.children.length) {
				return this.inside.appendChild(digit);
			} else {
				return this.inside.insertBefore(digit, this.inside.children[0]);
			}
		};
		Odometer.prototype.addSpacer = function (chr, before, extraClasses) {
			var spacer;
			spacer = createFromHTML(FORMAT_MARK_HTML);
			spacer.innerHTML = chr;
			if (extraClasses) {
				addClass(spacer, extraClasses);
			}
			return this.insertDigit(spacer, before);
		};
		Odometer.prototype.addDigit = function (value, repeating) {
			var chr, digit, resetted, _ref;
			if (repeating == null) {
				repeating = true;
			}
			if (value === '-') {
				return this.addSpacer(value, null, 'odometer-negation-mark');
			}
			if (value === '.') {
				return this.addSpacer((_ref = this.format.radix) != null ? _ref : '.', null, 'odometer-radix-mark');
			}
			if (repeating) {
				resetted = false;
				while (true) {
					if (!this.format.repeating.length) {
						if (resetted) {
							throw new Error("Bad odometer format without digits");
						}
						this.resetFormat();
						resetted = true;
					}
					chr = this.format.repeating[this.format.repeating.length - 1];
					this.format.repeating = this.format.repeating.substring(0, this.format.repeating.length - 1);
					if (chr === 'd') {
						break;
					}
					this.addSpacer(chr);
				}
			}
			digit = this.renderDigit();
			digit.querySelector('.odometer-value').innerHTML = value;
			this.digits.push(digit);
			return this.insertDigit(digit);
		};
		Odometer.prototype.animate = function (newValue) {
			if (!TRANSITION_SUPPORT || this.options.animation === 'count') {
				return this.animateCount(newValue);
			} else {
				return this.animateSlide(newValue);
			}
		};
		Odometer.prototype.animateCount = function (newValue) {
			var cur, diff, last, start, tick, _this = this;
			if (!(diff = +newValue - this.value)) {
				return;
			}
			start = last = now();
			cur = this.value;
			return (tick = function () {
				var delta, dist, fraction;
				if ((now() - start) > _this.options.duration) {
					_this.value = newValue;
					_this.render();
					trigger(_this.el, 'odometerdone');
					return;
				}
				delta = now() - last;
				if (delta > COUNT_MS_PER_FRAME) {
					last = now();
					fraction = delta / _this.options.duration;
					dist = diff * fraction;
					cur += dist;
					_this.render(Math.round(cur));
				}
				if (requestAnimationFrame != null) {
					return requestAnimationFrame(tick);
				} else {
					return setTimeout(tick, COUNT_MS_PER_FRAME);
				}
			})();
		};
		Odometer.prototype.getDigitCount = function () {
			var i, max, value, values, _i, _len;
			values = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
			for (i = _i = 0, _len = values.length; _i < _len; i = ++_i) {
				value = values[i];
				values[i] = Math.abs(value);
			}
			max = Math.max.apply(Math, values);
			return Math.ceil(Math.log(max + 1) / Math.log(10));
		};
		Odometer.prototype.getFractionalDigitCount = function () {
			var i, parser, parts, value, values, _i, _len;
			values = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
			parser = /^\-?\d*\.(\d*?)0*$/;
			for (i = _i = 0, _len = values.length; _i < _len; i = ++_i) {
				value = values[i];
				values[i] = value.toString();
				parts = parser.exec(values[i]);
				if (parts == null) {
					values[i] = 0;
				} else {
					values[i] = parts[1].length;
				}
			}
			return Math.max.apply(Math, values);
		};
		Odometer.prototype.resetDigits = function () {
			this.digits = [];
			this.ribbons = [];
			this.inside.innerHTML = '';
			return this.resetFormat();
		};
		Odometer.prototype.animateSlide = function (newValue) {
			var boosted, cur, diff, digitCount, digits, dist, end, fractionalCount, frame, frames, i, incr, j, mark, numEl, oldValue, start, _base, _i, _j, _k, _l, _len, _len1, _len2, _m, _ref, _results;
			oldValue = this.value;
			fractionalCount = this.getFractionalDigitCount(oldValue, newValue);
			if (fractionalCount) {
				newValue = newValue * Math.pow(10, fractionalCount);
				oldValue = oldValue * Math.pow(10, fractionalCount);
			}
			if (!(diff = newValue - oldValue)) {
				return;
			}
			this.bindTransitionEnd();
			digitCount = this.getDigitCount(oldValue, newValue);
			digits = [];
			boosted = 0;
			for (i = _i = 0; 0 <= digitCount ? _i < digitCount : _i > digitCount; i = 0 <= digitCount ? ++_i : --_i) {
				start = truncate(oldValue / Math.pow(10, digitCount - i - 1));
				end = truncate(newValue / Math.pow(10, digitCount - i - 1));
				dist = end - start;
				if (Math.abs(dist) > this.MAX_VALUES) {
					frames = [];
					incr = dist / (this.MAX_VALUES + this.MAX_VALUES * boosted * DIGIT_SPEEDBOOST);
					cur = start;
					while ((dist > 0 && cur < end) || (dist < 0 && cur > end)) {
						frames.push(Math.round(cur));
						cur += incr;
					}
					if (frames[frames.length - 1] !== end) {
						frames.push(end);
					}
					boosted++;
				} else {
					frames = (function () {
						_results = [];
						for (var _j = start; start <= end ? _j <= end : _j >= end; start <= end ? _j++ : _j--) {
							_results.push(_j);
						}
						return _results;
					}).apply(this);
				}
				for (i = _k = 0, _len = frames.length; _k < _len; i = ++_k) {
					frame = frames[i];
					frames[i] = Math.abs(frame % 10);
				}
				digits.push(frames);
			}
			this.resetDigits();
			_ref = digits.reverse();
			for (i = _l = 0, _len1 = _ref.length; _l < _len1; i = ++_l) {
				frames = _ref[i];
				if (!this.digits[i]) {
					this.addDigit(' ', i >= fractionalCount);
				}
				if ((_base = this.ribbons)[i] == null) {
					_base[i] = this.digits[i].querySelector('.odometer-ribbon-inner');
				}
				this.ribbons[i].innerHTML = '';
				if (diff < 0) {
					frames = frames.reverse();
				}
				for (j = _m = 0, _len2 = frames.length; _m < _len2; j = ++_m) {
					frame = frames[j];
					numEl = document.createElement('div');
					numEl.className = 'odometer-value';
					numEl.innerHTML = frame;
					this.ribbons[i].appendChild(numEl);
					if (j === frames.length - 1) {
						addClass(numEl, 'odometer-last-value');
					}
					if (j === 0) {
						addClass(numEl, 'odometer-first-value');
					}
				}
			}
			if (start < 0) {
				this.addDigit('-');
			}
			mark = this.inside.querySelector('.odometer-radix-mark');
			if (mark != null) {
				mark.parent.removeChild(mark);
			}
			if (fractionalCount) {
				return this.addSpacer(this.format.radix, this.digits[fractionalCount - 1], 'odometer-radix-mark');
			}
		};
		return Odometer;
	})();
	Odometer.options = (_ref = window.odometerOptions) != null ? _ref : {};
	setTimeout(function () {
		var k, v, _base, _ref1, _results;
		if (window.odometerOptions) {
			_ref1 = window.odometerOptions;
			_results = [];
			for (k in _ref1) {
				v = _ref1[k];
				_results.push((_base = Odometer.options)[k] != null ? (_base = Odometer.options)[k] : _base[k] = v);
			}
			return _results;
		}
	}, 0);
	Odometer.init = function () {
		var el, elements, _i, _len, _ref1, _results;
		if (document.querySelectorAll == null) {
			return;
		}
		elements = document.querySelectorAll(Odometer.options.selector || '.odometer');
		_results = [];
		for (_i = 0, _len = elements.length; _i < _len; _i++) {
			el = elements[_i];
			_results.push(el.odometer = new Odometer({
				el: el,
				value: (_ref1 = el.innerText) != null ? _ref1 : el.textContent
			}));
		}
		return _results;
	};
	if ((((_ref1 = document.documentElement) != null ? _ref1.doScroll : void 0) != null) && (document.createEventObject != null)) {
		_old = document.onreadystatechange;
		document.onreadystatechange = function () {
			if (document.readyState === 'complete' && Odometer.options.auto !== false) {
				Odometer.init();
			}
			return _old != null ? _old.apply(this, arguments) : void 0;
		};
	} else {
		document.addEventListener('DOMContentLoaded', function () {
			if (Odometer.options.auto !== false) {
				return Odometer.init();
			}
		}, false);
	}
	if (typeof define === 'function' && define.amd) {
		define(['jquery'], function () {
			return Odometer;
		});
	} else if (typeof exports === !'undefined') {
		module.exports = Odometer;
	} else {
		window.Odometer = Odometer;
	}
}).call(this);;
(function ($) {
	function getScrollY(elem) {
		return window.pageYOffset || document.documentElement.scrollTop;
	}

	function Sticky(el, options) {
		var self = this;
		this.el = el;
		this.$el = $(el);
		this.options = {};
		$.extend(this.options, options);
		self.init();
	}
	$.fn.scSticky = function (options) {
		$(this).each(function () {
			return new Sticky(this, options);
		});
	}
	Sticky.prototype = {
		init: function () {
			var self = this;
			this.$wrapper = false;
			this.$parent = this.getParent();
			$(window).scroll(function () {
				if (self.useSticky()) {
					self.wrap();
					self.scroll();
				} else {
					self.unwrap();
				}
			});
			$(window).resize(function () {
				if (self.useSticky()) {
					self.wrap();
					self.scroll();
				} else {
					self.unwrap();
				}
			});
		},
		wrap: function () {
			if (!this.$wrapper)
				this.$wrapper = this.$el.wrap('<div />').parent();
			this.$wrapper.attr('class', this.$el.attr('class')).addClass('gem-sticky-block').css({
				padding: 0,
				height: this.$el.outerHeight()
			});
			this.$el.css({
				width: this.$wrapper.outerWidth(),
				margin: 0
			});
		},
		getParent: function () {
			return this.$el.parent();
		},
		useSticky: function () {
			var is_sidebar = true;
			if (this.$el.hasClass('sidebar')) {
				if (this.$wrapper) {
					if (this.$wrapper.outerHeight() > this.$wrapper.siblings('.panel-center:first').outerHeight())
						is_sidebar = false;
				} else {
					if (this.$el.outerHeight() > this.$el.siblings('.panel-center:first').outerHeight())
						is_sidebar = false;
				}
			}
			return $(window).width() > 1000 && is_sidebar;
		},
		unwrap: function () {
			if (this.$el.parent().is('.gem-sticky-block')) {
				this.$el.unwrap();
				this.$wrapper = false;
			}
			this.$el.css({
				width: "",
				top: "",
				bottom: "",
				margin: ""
			});
		},
		scroll: function () {
			var top_offset = parseInt($('html').css('margin-top'));
			var $header = $('#site-header');
			if ($header.hasClass('fixed')) {
				top_offset += $header.outerHeight();
			}
			var scroll = getScrollY();
			var offset = this.$wrapper.offset();
			var parent_offset = this.$parent.offset();
			var parent_bottom = parent_offset.top + this.$parent.outerHeight() - scroll;
			var bottom = $(window).height() - parent_bottom;
			if ((top_offset + this.$el.outerHeight()) >= parent_bottom) {
				this.$el.addClass('sticky-fixed').css({
					top: "",
					bottom: bottom,
					left: offset.left
				});
				return;
			}
			if ((scroll + top_offset) > offset.top) {
				this.$el.addClass('sticky-fixed').css({
					top: top_offset,
					bottom: "",
					left: offset.left
				});
			} else {
				this.$el.removeClass('sticky-fixed').css({
					top: "",
					bottom: "",
					left: ""
				});
			}
		}
	};
}(jQuery));;
(function ($) {
	$.fn.thegemPreloader = function (callback) {
		$(this).each(function () {
			var $el = $(this);
			if (!$el.prev('.preloader').length) {
				$('<div class="preloader">').insertBefore($el);
			}
			$el.data('thegemPreloader', $('img, iframe', $el).add($el.filter('img, iframe')).length);
			if ($el.data('thegemPreloader') == 0) {
				$el.prev('.preloader').remove();
				callback();
				$el.trigger('thegem-preloader-loaded');
				return;
			}
			$('img, iframe', $el).add($el.filter('img, iframe')).each(function () {
				var $obj = $('<img>');
				if ($(this).prop('tagName').toLowerCase() == 'iframe') {
					$obj = $(this);
				}
				$obj.attr('src', $(this).attr('src'));
				$obj.on('load error', function () {
					$el.data('thegemPreloader', $el.data('thegemPreloader') - 1);
					if ($el.data('thegemPreloader') == 0) {
						$el.prev('.preloader').remove();
						callback();
						$el.trigger('thegem-preloader-loaded');
					}
				});
			});
		});
	}
})(jQuery);
(function ($) {
	var oWidth = $.fn.width;
	$.fn.width = function (argument) {
		if (arguments.length == 0 && this.length == 1 && this[0] === window) {
			if (window.gemOptions.innerWidth != -1) {
				return window.gemOptions.innerWidth;
			}
			var width = oWidth.apply(this, arguments);
			window.updateGemInnerSize(width);
			return width;
		}
		return oWidth.apply(this, arguments);
	};
	var $page = $('#page');
	$(window).load(function () {
		var $preloader = $('#page-preloader');
		if ($preloader.length && !$preloader.hasClass('preloader-loaded')) {
			$preloader.addClass('preloader-loaded');
		}
	});
	$('#site-header.animated-header').headerAnimation();
	$.fn.updateTabs = function () {
		jQuery('.gem-tabs', this).each(function (index) {
			var $tabs = $(this);
			$tabs.thegemPreloader(function () {
				$tabs.easyResponsiveTabs({
					type: 'default',
					width: 'auto',
					fit: false,
					activate: function (currentTab, e) {
						var $tab = $(currentTab.target);
						var controls = $tab.attr('aria-controls');
						$tab.closest('.ui-tabs').find('.gem_tab[aria-labelledby="' + controls + '"]').trigger('tab-update');
					}
				});
			});
		});
		jQuery('.gem-tour', this).each(function (index) {
			var $tabs = $(this);
			$tabs.thegemPreloader(function () {
				$tabs.easyResponsiveTabs({
					type: 'vertical',
					width: 'auto',
					fit: false,
					activate: function (currentTab, e) {
						var $tab = $(currentTab.target);
						var controls = $tab.attr('aria-controls');
						$tab.closest('.ui-tabs').find('.gem_tab[aria-labelledby="' + controls + '"]').trigger('tab-update');
					}
				});
			});
		});
	};

	function fullwidth_block_after_update($item) {
		$item.trigger('updateTestimonialsCarousel');
		$item.trigger('updateClientsCarousel');
		$item.trigger('fullwidthUpdate');
	}

	function fullwidth_block_update($item, pageOffset, pagePaddingLeft, pageWidth, skipTrigger) {
		var $prevElement = $item.prev(),
			extra_padding = 0;
		if ($prevElement.length == 0 || $prevElement.hasClass('fullwidth-block')) {
			$prevElement = $item.parent();
			extra_padding = parseInt($prevElement.css('padding-left'));
		}
		var offsetKey = window.gemSettings.isRTL ? 'right' : 'left';
		var cssData = {
			width: pageWidth
		};
		cssData[offsetKey] = pageOffset.left - ($prevElement.length ? $prevElement.offset().left : 0) + parseInt(pagePaddingLeft) - extra_padding;
		$item.css(cssData);
		if (!skipTrigger) {
			fullwidth_block_after_update($item);
		}
	}
	var inlineFullwidths = [],
		notInlineFullwidths = [];
	$('.fullwidth-block').each(function () {
		var $item = $(this),
			$parents = $item.parents('.vc_row'),
			fullw = {
				isInline: false
			};
		$parents.each(function () {
			if (this.hasAttribute('data-vc-full-width')) {
				fullw.isInline = true;
				return false;
			}
		});
		if (fullw.isInline) {
			inlineFullwidths.push(this);
		} else {
			notInlineFullwidths.push(this);
		}
	});

	function update_fullwidths(inline, init) {
		var $needUpdate = [];
		(inline ? inlineFullwidths : notInlineFullwidths).forEach(function (item) {
			$needUpdate.push(item);
		});
		if ($needUpdate.length > 0) {
			var pageOffset = $page.offset(),
				pagePaddingLeft = $page.css('padding-left'),
				pageWidth = $page.width();
			$needUpdate.forEach(function (item) {
				fullwidth_block_update($(item), pageOffset, pagePaddingLeft, pageWidth);
			});
		}
	}
	if (!window.disableGemSlideshowPreloaderHandle) {
		jQuery('.gem-slideshow').each(function () {
			var $slideshow = $(this);
			$slideshow.thegemPreloader(function () {});
		});
	}
	$(function () {
		$('#gem-icons-loading-hide').remove();
		$('#thegem-preloader-inline-css').remove();
		jQuery('iframe').not('.gem-video-background iframe').each(function () {
			$(this).thegemPreloader(function () {});
		});
		jQuery('.gem-video-background').each(function () {
			var $videoBG = $(this);
			var $videoContainer = $('.gem-video-background-inner', this);
			var ratio = $videoBG.data('aspect-ratio') ? $videoBG.data('aspect-ratio') : '16:9';
			var regexp = /(\d+):(\d+)/;
			var $fullwidth = $videoBG.closest('.fullwidth-block');
			ratio = regexp.exec(ratio);
			if (!ratio || parseInt(ratio[1]) == 0 || parseInt(ratio[2]) == 0) {
				ratio = 16 / 9;
			} else {
				ratio = parseInt(ratio[1]) / parseInt(ratio[2]);
			}

			function gemVideoUpdate() {
				$videoContainer.removeAttr('style');
				if ($videoContainer.width() / $videoContainer.height() > ratio) {
					$videoContainer.css({
						height: ($videoContainer.width() / ratio) + 'px',
						marginTop: -($videoContainer.width() / ratio - $videoBG.height()) / 2 + 'px'
					});
				} else {
					$videoContainer.css({
						width: ($videoContainer.height() * ratio) + 'px',
						marginLeft: -($videoContainer.height() * ratio - $videoBG.width()) / 2 + 'px'
					});
				}
			}
			if ($videoBG.closest('.page-title-block').length > 0) {
				gemVideoUpdate();
			}
			if ($fullwidth.length) {
				$fullwidth.on('fullwidthUpdate', gemVideoUpdate);
			} else {
				$(window).resize(gemVideoUpdate);
			}
		});
		update_fullwidths(false, true);
		if (!window.gemSettings.parallaxDisabled) {
			$('.fullwidth-block').each(function () {
				var $item = $(this),
					mobile_enabled = $item.data('mobile-parallax-enable') || '0';
				if (!window.gemSettings.isTouch || mobile_enabled == '1') {
					if ($item.hasClass('fullwidth-block-parallax-vertical')) {
						$('.fullwidth-block-background', $item).parallaxVertical('50%');
					} else if ($item.hasClass('fullwidth-block-parallax-horizontal')) {
						$('.fullwidth-block-background', $item).parallaxHorizontal();
					}
				} else {
					$('.fullwidth-block-background', $item).css({
						backgroundAttachment: 'scroll'
					});
				}
			});
		}
		$(window).resize(function () {
			update_fullwidths(false, false);
		});
		jQuery('select.gem-combobox, .gem-combobox select, .widget_archive select, .widget_product_categories select, .widget_layered_nav select, .widget_categories select').each(function (index) {
			$(this).combobox();
		});
		jQuery('input.gem-checkbox, .gem-checkbox input').checkbox();
		if (typeof ($.fn.ReStable) == "function") {
			jQuery('.gem-table-responsive').each(function (index) {
				$('> table', this).ReStable({
					maxWidth: 768,
					rowHeaders: $(this).hasClass('row-headers')
				});
			});
		}
		jQuery('.fancybox').each(function () {
			$(this).fancybox();
		});

		function init_odometer(el) {
			if (jQuery('.gem-counter-odometer', el).size() == 0)
				return;
			var odometer = jQuery('.gem-counter-odometer', el).get(0);
			var format = jQuery(el).closest('.gem-counter-box').data('number-format');
			format = format ? format : '(ddd).ddd';
			var od = new Odometer({
				el: odometer,
				value: $(odometer).text(),
				format: format
			});
			od.update($(odometer).data('to'));
		}
		window['thegem_init_odometer'] = init_odometer;
		jQuery('.gem-counter').each(function (index) {
			if (jQuery(this).closest('.gem-counter-box').size() > 0 && jQuery(this).closest('.gem-counter-box').hasClass('lazy-loading') && !window.gemSettings.lasyDisabled) {
				jQuery(this).addClass('lazy-loading-item').data('ll-effect', 'action').data('item-delay', '0').data('ll-action-func', 'thegem_init_odometer');
				jQuery('.gem-icon', this).addClass('lazy-loading-item').data('ll-effect', 'fading').data('item-delay', '0');
				jQuery('.gem-counter-text', this).addClass('lazy-loading-item').data('ll-effect', 'fading').data('item-delay', '0');
				return;
			}
			init_odometer(this);
		});
		jQuery('.panel-sidebar-sticky > .sidebar').scSticky();
		jQuery('iframe + .map-locker').each(function () {
			var $locker = $(this);
			$locker.click(function (e) {
				e.preventDefault();
				if ($locker.hasClass('disabled')) {
					$locker.prev('iframe').css({
						'pointer-events': 'none'
					});
				} else {
					$locker.prev('iframe').css({
						'pointer-events': 'auto'
					});
				}
				$locker.toggleClass('disabled');
			});
		});
		$('.primary-navigation a.mega-no-link').closest('li').removeClass('menu-item-active current-menu-item');
		$('.primary-navigation a, .gem-button, .footer-navigation a, .scroll-top-button, .scroll-to-anchor, .scroll-to-anchor a, .top-area-menu a').each(function (e) {
			var $anhor = $(this);
			var link = $anhor.attr('href');
			if (!link) return;
			link = link.split('#');
			if ($('#' + link[1]).hasClass('vc_tta-panel')) return;
			if ($('#' + link[1]).length) {
				$anhor.closest('li').removeClass('menu-item-active current-menu-item');
				$anhor.closest('li').parents('li').removeClass('menu-item-current');
				$(window).scroll(function () {
					if (!$anhor.closest('li.menu-item').length) return;
					var correction = 0;
					if (!$('#page').hasClass('vertical-header')) {
						correction = $('#site-header').outerHeight() + $('#site-header').position().top;
					}
					var target_top = $('#' + link[1]).offset().top - correction;
					if (getScrollY() >= target_top && getScrollY() <= target_top + $('#' + link[1]).outerHeight()) {
						$anhor.closest('li').addClass('menu-item-active');
						$anhor.closest('li').parents('li').addClass('menu-item-current');
					} else {
						$anhor.closest('li').removeClass('menu-item-active');
						$anhor.closest('li').parents('li.menu-item-current').each(function () {
							if (!$('.menu-item-active', this).length) {
								$(this).removeClass('menu-item-current');
							}
						});
					}
				});
				$(document).on('update-page-scroller', function (e, elem) {
					var $elem = $(elem);
					if (!$anhor.closest('li.menu-item').length) return;
					if ($elem.is($('#' + link[1])) || $elem.find($('#' + link[1])).length) {
						$anhor.closest('li').addClass('menu-item-active');
						$anhor.closest('li').parents('li').addClass('menu-item-current');
					} else {
						$anhor.closest('li').removeClass('menu-item-active');
						$anhor.closest('li').parents('li.menu-item-current').each(function () {
							if (!$('.menu-item-active', this).length) {
								$(this).removeClass('menu-item-current');
							}
						});
					}
				});
				$anhor.click(function (e) {
					e.preventDefault();
					var correction = 0;
					if ($('#site-header.animated-header').length) {
						var shrink = $('#site-header').hasClass('shrink');
						$('#site-header').addClass('scroll-counting');
						$('#site-header').addClass('fixed shrink');
						correction = $('#site-header').outerHeight() + $('#site-header').position().top;
						if (!shrink && $('#top-area').length && !$('#site-header').find('#top-area').length) {
							correction = correction - $('#top-area').outerHeight();
						}
						if (!shrink) {
							$('#site-header').removeClass('fixed shrink');
						}
						setTimeout(function () {
							$('#site-header').removeClass('scroll-counting');
						}, 50);
					}
					var target_top = $('#' + link[1]).offset().top - correction + 1;
					if ($('body').hasClass('page-scroller') && $('.page-scroller-nav-pane').is(':visible')) {
						var $block = $('#' + link[1] + '.scroller-block').add($('#' + link[1]).closest('.scroller-block')).eq(0);
						if ($block.length) {
							$('.page-scroller-nav-pane .page-scroller-nav-item').eq($('.scroller-block').index($block)).trigger('click');
						}
					} else {
						$('html, body').stop(true, true).animate({
							scrollTop: target_top
						}, 1500, 'easeInOutQuint');
					}
				});
			}
			$(window).load(function () {
				if (window.location.href == $anhor.attr('href')) {
					$anhor.click();
				}
			});
		});
		$('body').on('click', '.post-footer-sharing .gem-button', function (e) {
			e.preventDefault();
			$(this).closest('.post-footer-sharing').find('.sharing-popup').toggleClass('active');
		});
		var scrollTimer, body = document.body;
		$(window).scroll(function () {
			clearTimeout(scrollTimer);
			if (!body.classList.contains('disable-hover')) {}
			scrollTimer = setTimeout(function () {}, 300);
			if (getScrollY() > 0) {
				$('.scroll-top-button').addClass('visible');
			} else {
				$('.scroll-top-button').removeClass('visible');
			}
		}).scroll();

		function getScrollY(elem) {
			return window.pageYOffset || document.documentElement.scrollTop;
		}
		$('a.hidden-email').each(function () {
			$(this).attr('href', 'mailto:' + $(this).data('name') + '@' + $(this).data('domain'));
		});
		$('#colophon .footer-widget-area').thegemPreloader(function () {
			$('#colophon .footer-widget-area').isotope({
				itemSelector: '.widget',
				layoutMode: 'masonry'
			});
		});
		$('body').updateTabs();
	});
	var vc_update_fullwidth_init = true;
	$(document).on('vc-full-width-row', function (e) {
		if (window.gemOptions.clientWidth - $page.width() > 25 || window.gemSettings.isRTL) {
			for (var i = 1; i < arguments.length; i++) {
				var $el = $(arguments[i]);
				$el.addClass("vc_hidden");
				var $el_full = $el.next(".vc_row-full-width");
				$el_full.length || ($el_full = $el.parent().next(".vc_row-full-width"));
				var el_margin_left = parseInt($el.css("margin-left"), 10),
					el_margin_right = parseInt($el.css("margin-right"), 10),
					offset = 0 - $el_full.offset().left - el_margin_left + $('#page').offset().left + parseInt($('#page').css('padding-left')),
					width = $('#page').width();
				var offsetKey = window.gemSettings.isRTL ? 'right' : 'left';
				var cssData = {
					position: "relative",
					left: offset,
					"box-sizing": "border-box",
					width: $("#page").width()
				};
				cssData[offsetKey] = offset;
				if ($el.css(cssData), !$el.data("vcStretchContent")) {
					var padding = -1 * offset;
					0 > padding && (padding = 0);
					var paddingRight = width - padding - $el_full.width() + el_margin_left + el_margin_right;
					0 > paddingRight && (paddingRight = 0), $el.css({
						"padding-left": padding + "px",
						"padding-right": paddingRight + "px"
					})
				}
				$el.attr("data-vc-full-width-init", "true"), $el.removeClass("vc_hidden");
				$el.trigger('VCRowFullwidthUpdate');
			}
		}
		update_fullwidths(true, vc_update_fullwidth_init);
		vc_update_fullwidth_init = false;
	});
	if (!window.gemSettings.lasyDisabled && $.support.opacity) {
		$('.wpb_text_column.wpb_animate_when_almost_visible.wpb_fade').each(function () {
			$(this).wrap('<div class="lazy-loading"></div>').addClass('lazy-loading-item').data('ll-effect', 'fading');
		});
		$('.gem-list.lazy-loading').each(function () {
			$(this).data('ll-item-delay', '200');
			$('li', this).addClass('lazy-loading-item').data('ll-effect', 'slide-right');
			$('li', this).each(function (index) {
				$(this).attr("style", "transition-delay: " + (index + 1) * 0.2 + "s;");
			});
		});
		$.lazyLoading();
	}
})(jQuery);
(function ($) {
	$('.menu-item-search a').on('click', function (e) {
		e.preventDefault();
		$('.menu-item-search').toggleClass('active');
	});
})(jQuery);;
/*! Copyright (c) 2011 Brandon Aaron (http://brandonaaron.net)
 * Licensed under the MIT License (LICENSE.txt).
 *
 * Thanks to: http://adomas.org/javascript-mouse-wheel/ for some pointers.
 * Thanks to: Mathias Bank(http://www.mathias-bank.de) for a scope bug fix.
 * Thanks to: Seamus Leahy for adding deltaX and deltaY
 *
 * Version: 3.0.6
 * 
 * Requires: 1.2.2+
 */
(function (d) {
	function e(a) {
		var b = a || window.event,
			c = [].slice.call(arguments, 1),
			f = 0,
			e = 0,
			g = 0,
			a = d.event.fix(b);
		a.type = "mousewheel";
		b.wheelDelta && (f = b.wheelDelta / 120);
		b.detail && (f = -b.detail / 3);
		g = f;
		b.axis !== void 0 && b.axis === b.HORIZONTAL_AXIS && (g = 0, e = -1 * f);
		b.wheelDeltaY !== void 0 && (g = b.wheelDeltaY / 120);
		b.wheelDeltaX !== void 0 && (e = -1 * b.wheelDeltaX / 120);
		c.unshift(a, f, e, g);
		return (d.event.dispatch || d.event.handle).apply(this, c)
	}
	var c = ["DOMMouseScroll", "mousewheel"];
	if (d.event.fixHooks)
		for (var h = c.length; h;) d.event.fixHooks[c[--h]] = d.event.mouseHooks;
	d.event.special.mousewheel = {
		setup: function () {
			if (this.addEventListener)
				for (var a = c.length; a;) this.addEventListener(c[--a], e, false);
			else this.onmousewheel = e
		},
		teardown: function () {
			if (this.removeEventListener)
				for (var a = c.length; a;) this.removeEventListener(c[--a], e, false);
			else this.onmousewheel = null
		}
	};
	d.fn.extend({
		mousewheel: function (a) {
			return a ? this.bind("mousewheel", a) : this.trigger("mousewheel")
		},
		unmousewheel: function (a) {
			return this.unbind("mousewheel", a)
		}
	})
})(jQuery);;
/*! fancyBox v2.1.4 fancyapps.com | fancyapps.com/fancybox/#license */
(function (C, z, f, r) {
	var q = f(C),
		n = f(z),
		b = f.fancybox = function () {
			b.open.apply(this, arguments)
		},
		H = navigator.userAgent.match(/msie/),
		w = null,
		s = z.createTouch !== r,
		t = function (a) {
			return a && a.hasOwnProperty && a instanceof f
		},
		p = function (a) {
			return a && "string" === f.type(a)
		},
		F = function (a) {
			return p(a) && 0 < a.indexOf("%")
		},
		l = function (a, d) {
			var e = parseInt(a, 10) || 0;
			d && F(a) && (e *= b.getViewport()[d] / 100);
			return Math.ceil(e)
		},
		x = function (a, b) {
			return l(a, b) + "px"
		};
	f.extend(b, {
		version: "2.1.4",
		defaults: {
			padding: 15,
			margin: 20,
			width: 800,
			height: 600,
			minWidth: 100,
			minHeight: 100,
			maxWidth: 9999,
			maxHeight: 9999,
			autoSize: !0,
			autoHeight: !1,
			autoWidth: !1,
			autoResize: !0,
			autoCenter: !s,
			fitToView: !0,
			aspectRatio: !1,
			topRatio: 0.5,
			leftRatio: 0.5,
			scrolling: "auto",
			wrapCSS: "",
			arrows: !0,
			closeBtn: !0,
			closeClick: !1,
			nextClick: !1,
			mouseWheel: !0,
			autoPlay: !1,
			playSpeed: 3E3,
			preload: 3,
			modal: !1,
			loop: !0,
			ajax: {
				dataType: "html",
				headers: {
					"X-fancyBox": !0
				}
			},
			iframe: {
				scrolling: "auto",
				preload: !0
			},
			swf: {
				wmode: "transparent",
				allowfullscreen: "true",
				allowscriptaccess: "always"
			},
			keys: {
				next: {
					13: "left",
					34: "up",
					39: "left",
					40: "up"
				},
				prev: {
					8: "right",
					33: "down",
					37: "right",
					38: "down"
				},
				close: [27],
				play: [32],
				toggle: [70]
			},
			direction: {
				next: "left",
				prev: "right"
			},
			scrollOutside: !0,
			index: 0,
			type: null,
			href: null,
			content: null,
			title: null,
			tpl: {
				wrap: '<div class="fancybox-wrap" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner"></div></div></div></div>',
				image: '<img class="fancybox-image" src="{href}" alt="" />',
				iframe: '<iframe id="fancybox-frame{rnd}" name="fancybox-frame{rnd}" class="fancybox-iframe" frameborder="0" vspace="0" hspace="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen' +
					(H ? ' allowtransparency="true"' : "") + "></iframe>",
				error: '<p class="fancybox-error">The requested content cannot be loaded.<br/>Please try again later.</p>',
				closeBtn: '<a title="Close" class="fancybox-item fancybox-close" href="javascript:;"></a>',
				next: '<a title="Next" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
				prev: '<a title="Previous" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'
			},
			openEffect: "fade",
			openSpeed: 250,
			openEasing: "swing",
			openOpacity: !0,
			openMethod: "zoomIn",
			closeEffect: "fade",
			closeSpeed: 250,
			closeEasing: "swing",
			closeOpacity: !0,
			closeMethod: "zoomOut",
			nextEffect: "elastic",
			nextSpeed: 250,
			nextEasing: "swing",
			nextMethod: "changeIn",
			prevEffect: "elastic",
			prevSpeed: 250,
			prevEasing: "swing",
			prevMethod: "changeOut",
			helpers: {
				overlay: !0,
				title: !0
			},
			onCancel: f.noop,
			beforeLoad: f.noop,
			afterLoad: f.noop,
			beforeShow: f.noop,
			afterShow: f.noop,
			beforeChange: f.noop,
			beforeClose: f.noop,
			afterClose: f.noop
		},
		group: {},
		opts: {},
		previous: null,
		coming: null,
		current: null,
		isActive: !1,
		isOpen: !1,
		isOpened: !1,
		wrap: null,
		skin: null,
		outer: null,
		inner: null,
		player: {
			timer: null,
			isActive: !1
		},
		ajaxLoad: null,
		imgPreload: null,
		transitions: {},
		helpers: {},
		open: function (a, d) {
			if (a && (f.isPlainObject(d) || (d = {}), !1 !== b.close(!0))) return f.isArray(a) || (a = t(a) ? f(a).get() : [a]), f.each(a, function (e, c) {
				var k = {},
					g, h, j, m, l;
				"object" === f.type(c) && (c.nodeType && (c = f(c)), t(c) ? (k = {
					href: c.data("fancybox-href") || c.attr("href"),
					title: c.data("fancybox-title") || c.attr("title"),
					isDom: !0,
					element: c
				}, f.metadata && f.extend(!0, k, c.metadata())) : k = c);
				g = d.href || k.href || (p(c) ? c : null);
				h = d.title !== r ? d.title : k.title || "";
				m = (j = d.content || k.content) ? "html" : d.type || k.type;
				!m && k.isDom && (m = c.data("fancybox-type"), m || (m = (m = c.prop("class").match(/fancybox\.(\w+)/)) ? m[1] : null));
				p(g) && (m || (b.isImage(g) ? m = "image" : b.isSWF(g) ? m = "swf" : "#" === g.charAt(0) ? m = "inline" : p(c) && (m = "html", j = c)), "ajax" === m && (l = g.split(/\s+/, 2), g = l.shift(), l = l.shift()));
				j || ("inline" === m ? g ? j = f(p(g) ? g.replace(/.*(?=#[^\s]+$)/, "") : g) : k.isDom && (j = c) : "html" === m ? j = g : !m && (!g && k.isDom) && (m = "inline", j = c));
				f.extend(k, {
					href: g,
					type: m,
					content: j,
					title: h,
					selector: l
				});
				a[e] = k
			}), b.opts = f.extend(!0, {}, b.defaults, d), d.keys !== r && (b.opts.keys = d.keys ? f.extend({}, b.defaults.keys, d.keys) : !1), b.group = a, b._start(b.opts.index)
		},
		cancel: function () {
			var a = b.coming;
			a && !1 !== b.trigger("onCancel") && (b.hideLoading(), b.ajaxLoad && b.ajaxLoad.abort(), b.ajaxLoad = null, b.imgPreload && (b.imgPreload.onload = b.imgPreload.onerror = null), a.wrap && a.wrap.stop(!0, !0).trigger("onReset").remove(), b.coming = null, b.current || b._afterZoomOut(a))
		},
		close: function (a) {
			b.cancel();
			!1 !== b.trigger("beforeClose") && (b.unbindEvents(), b.isActive && (!b.isOpen || !0 === a ? (f(".fancybox-wrap").stop(!0).trigger("onReset").remove(), b._afterZoomOut()) : (b.isOpen = b.isOpened = !1, b.isClosing = !0, f(".fancybox-item, .fancybox-nav").remove(), b.wrap.stop(!0, !0).removeClass("fancybox-opened"), b.transitions[b.current.closeMethod]())))
		},
		play: function (a) {
			var d = function () {
					clearTimeout(b.player.timer)
				},
				e = function () {
					d();
					b.current && b.player.isActive && (b.player.timer = setTimeout(b.next, b.current.playSpeed))
				},
				c = function () {
					d();
					f("body").unbind(".player");
					b.player.isActive = !1;
					b.trigger("onPlayEnd")
				};
			if (!0 === a || !b.player.isActive && !1 !== a) {
				if (b.current && (b.current.loop || b.current.index < b.group.length - 1)) b.player.isActive = !0, f("body").bind({
					"afterShow.player onUpdate.player": e,
					"onCancel.player beforeClose.player": c,
					"beforeLoad.player": d
				}), e(), b.trigger("onPlayStart")
			} else c()
		},
		next: function (a) {
			var d = b.current;
			d && (p(a) || (a = d.direction.next), b.jumpto(d.index + 1, a, "next"))
		},
		prev: function (a) {
			var d = b.current;
			d && (p(a) || (a = d.direction.prev), b.jumpto(d.index - 1, a, "prev"))
		},
		jumpto: function (a, d, e) {
			var c = b.current;
			c && (a = l(a), b.direction = d || c.direction[a >= c.index ? "next" : "prev"], b.router = e || "jumpto", c.loop && (0 > a && (a = c.group.length + a % c.group.length), a %= c.group.length), c.group[a] !== r && (b.cancel(), b._start(a)))
		},
		reposition: function (a, d) {
			var e = b.current,
				c = e ? e.wrap : null,
				k;
			c && (k = b._getPosition(d), a && "scroll" === a.type ? (delete k.position, c.stop(!0, !0).animate(k, 200)) : (c.css(k), e.pos = f.extend({}, e.dim, k)))
		},
		update: function (a) {
			var d = a && a.type,
				e = !d || "orientationchange" === d;
			e && (clearTimeout(w), w = null);
			b.isOpen && !w && (w = setTimeout(function () {
				var c = b.current;
				c && !b.isClosing && (b.wrap.removeClass("fancybox-tmp"), (e || "load" === d || "resize" === d && c.autoResize) && b._setDimension(), "scroll" === d && c.canShrink || b.reposition(a), b.trigger("onUpdate"), w = null)
			}, e && !s ? 0 : 300))
		},
		toggle: function (a) {
			b.isOpen && (b.current.fitToView = "boolean" === f.type(a) ? a : !b.current.fitToView, s && (b.wrap.removeAttr("style").addClass("fancybox-tmp"), b.trigger("onUpdate")), b.update())
		},
		hideLoading: function () {
			n.unbind(".loading");
			f("#fancybox-loading").remove()
		},
		showLoading: function () {
			var a, d;
			b.hideLoading();
			a = f('<div id="fancybox-loading"><div></div></div>').click(b.cancel).appendTo("body");
			n.bind("keydown.loading", function (a) {
				if (27 === (a.which || a.keyCode)) a.preventDefault(), b.cancel()
			});
			b.defaults.fixed || (d = b.getViewport(), a.css({
				position: "absolute",
				top: 0.5 * d.h + d.y,
				left: 0.5 * d.w + d.x
			}))
		},
		getViewport: function () {
			var a = b.current && b.current.locked || !1,
				d = {
					x: q.scrollLeft(),
					y: q.scrollTop()
				};
			a ? (d.w = a[0].clientWidth, d.h = a[0].clientHeight) : (d.w = s && C.innerWidth ? C.innerWidth : q.width(), d.h = s && C.innerHeight ? C.innerHeight : q.height());
			return d
		},
		unbindEvents: function () {
			b.wrap && t(b.wrap) && b.wrap.unbind(".fb");
			n.unbind(".fb");
			q.unbind(".fb")
		},
		bindEvents: function () {
			var a = b.current,
				d;
			a && (q.bind("orientationchange.fb" + (s ? "" : " resize.fb") + (a.autoCenter && !a.locked ? " scroll.fb" : ""), b.update), (d = a.keys) && n.bind("keydown.fb", function (e) {
				var c = e.which || e.keyCode,
					k = e.target || e.srcElement;
				if (27 === c && b.coming) return !1;
				!e.ctrlKey && (!e.altKey && !e.shiftKey && !e.metaKey && (!k || !k.type && !f(k).is("[contenteditable]"))) && f.each(d, function (d, k) {
					if (1 < a.group.length && k[c] !== r) return b[d](k[c]), e.preventDefault(), !1;
					if (-1 < f.inArray(c, k)) return b[d](), e.preventDefault(), !1
				})
			}), f.fn.mousewheel && a.mouseWheel && b.wrap.bind("mousewheel.fb", function (d, c, k, g) {
				for (var h = f(d.target || null), j = !1; h.length && !j && !h.is(".fancybox-skin") && !h.is(".fancybox-wrap");) j = h[0] && !(h[0].style.overflow && "hidden" === h[0].style.overflow) && (h[0].clientWidth && h[0].scrollWidth > h[0].clientWidth || h[0].clientHeight && h[0].scrollHeight > h[0].clientHeight), h = f(h).parent();
				if (0 !== c && !j && 1 < b.group.length && !a.canShrink) {
					if (0 < g || 0 < k) b.prev(0 < g ? "down" : "left");
					else if (0 > g || 0 > k) b.next(0 > g ? "up" : "right");
					d.preventDefault()
				}
			}))
		},
		trigger: function (a, d) {
			var e, c = d || b.coming || b.current;
			if (c) {
				f.isFunction(c[a]) && (e = c[a].apply(c, Array.prototype.slice.call(arguments, 1)));
				if (!1 === e) return !1;
				c.helpers && f.each(c.helpers, function (d, e) {
					e && (b.helpers[d] && f.isFunction(b.helpers[d][a])) && (e = f.extend(!0, {}, b.helpers[d].defaults, e), b.helpers[d][a](e, c))
				});
				f.event.trigger(a + ".fb")
			}
		},
		isImage: function (a) {
			return p(a) && a.match(/(^data:image\/.*,)|(\.(jp(e|g|eg)|gif|png|bmp|webp)((\?|#).*)?$)/i)
		},
		isSWF: function (a) {
			return p(a) && a.match(/\.(swf)((\?|#).*)?$/i)
		},
		_start: function (a) {
			var d = {},
				e, c;
			a = l(a);
			e = b.group[a] || null;
			if (!e) return !1;
			d = f.extend(!0, {}, b.opts, e);
			e = d.margin;
			c = d.padding;
			"number" === f.type(e) && (d.margin = [e, e, e, e]);
			"number" === f.type(c) && (d.padding = [c, c, c, c]);
			d.modal && f.extend(!0, d, {
				closeBtn: !1,
				closeClick: !1,
				nextClick: !1,
				arrows: !1,
				mouseWheel: !1,
				keys: null,
				helpers: {
					overlay: {
						closeClick: !1
					}
				}
			});
			d.autoSize && (d.autoWidth = d.autoHeight = !0);
			"auto" === d.width && (d.autoWidth = !0);
			"auto" === d.height && (d.autoHeight = !0);
			d.group = b.group;
			d.index = a;
			b.coming = d;
			if (!1 === b.trigger("beforeLoad")) b.coming = null;
			else {
				c = d.type;
				e = d.href;
				if (!c) return b.coming = null, b.current && b.router && "jumpto" !== b.router ? (b.current.index = a, b[b.router](b.direction)) : !1;
				b.isActive = !0;
				if ("image" === c || "swf" === c) d.autoHeight = d.autoWidth = !1, d.scrolling = "visible";
				"image" === c && (d.aspectRatio = !0);
				"iframe" === c && s && (d.scrolling = "scroll");
				d.wrap = f(d.tpl.wrap).addClass("fancybox-" + (s ? "mobile" : "desktop") + " fancybox-type-" + c + " fancybox-tmp " + d.wrapCSS).appendTo(d.parent || "body");
				f.extend(d, {
					skin: f(".fancybox-skin", d.wrap),
					outer: f(".fancybox-outer", d.wrap),
					inner: f(".fancybox-inner", d.wrap)
				});
				f.each(["Top", "Right", "Bottom", "Left"], function (a, b) {
					d.skin.css("padding" + b, x(d.padding[a]))
				});
				b.trigger("onReady");
				if ("inline" === c || "html" === c) {
					if (!d.content || !d.content.length) return b._error("content")
				} else if (!e) return b._error("href");
				"image" === c ? b._loadImage() : "ajax" === c ? b._loadAjax() : "iframe" === c ? b._loadIframe() : b._afterLoad()
			}
		},
		_error: function (a) {
			f.extend(b.coming, {
				type: "html",
				autoWidth: !0,
				autoHeight: !0,
				minWidth: 0,
				minHeight: 0,
				scrolling: "no",
				hasError: a,
				content: b.coming.tpl.error
			});
			b._afterLoad()
		},
		_loadImage: function () {
			var a = b.imgPreload = new Image;
			a.onload = function () {
				this.onload = this.onerror = null;
				b.coming.width = this.width;
				b.coming.height = this.height;
				b._afterLoad()
			};
			a.onerror = function () {
				this.onload = this.onerror = null;
				b._error("image")
			};
			a.src = b.coming.href;
			!0 !== a.complete && b.showLoading()
		},
		_loadAjax: function () {
			var a = b.coming;
			b.showLoading();
			b.ajaxLoad = f.ajax(f.extend({}, a.ajax, {
				url: a.href,
				error: function (a, e) {
					b.coming && "abort" !== e ? b._error("ajax", a) : b.hideLoading()
				},
				success: function (d, e) {
					"success" === e && (a.content = d, b._afterLoad())
				}
			}))
		},
		_loadIframe: function () {
			var a = b.coming,
				d = f(a.tpl.iframe.replace(/\{rnd\}/g, (new Date).getTime())).attr("scrolling", s ? "auto" : a.iframe.scrolling).attr("src", a.href);
			f(a.wrap).bind("onReset", function () {
				try {
					f(this).find("iframe").hide().attr("src", "//about:blank").end().empty()
				} catch (a) {}
			});
			a.iframe.preload && (b.showLoading(), d.one("load", function () {
				f(this).data("ready", 1);
				s || f(this).bind("load.fb", b.update);
				f(this).parents(".fancybox-wrap").width("100%").removeClass("fancybox-tmp").show();
				b._afterLoad()
			}));
			a.content = d.appendTo(a.inner);
			a.iframe.preload || b._afterLoad()
		},
		_preloadImages: function () {
			var a = b.group,
				d = b.current,
				e = a.length,
				c = d.preload ? Math.min(d.preload, e - 1) : 0,
				f, g;
			for (g = 1; g <= c; g += 1) f = a[(d.index + g) % e], "image" === f.type && f.href && ((new Image).src = f.href)
		},
		_afterLoad: function () {
			var a = b.coming,
				d = b.current,
				e, c, k, g, h;
			b.hideLoading();
			if (a && !1 !== b.isActive)
				if (!1 === b.trigger("afterLoad", a, d)) a.wrap.stop(!0).trigger("onReset").remove(), b.coming = null;
				else {
					d && (b.trigger("beforeChange", d), d.wrap.stop(!0).removeClass("fancybox-opened").find(".fancybox-item, .fancybox-nav").remove());
					b.unbindEvents();
					e = a.content;
					c = a.type;
					k = a.scrolling;
					f.extend(b, {
						wrap: a.wrap,
						skin: a.skin,
						outer: a.outer,
						inner: a.inner,
						current: a,
						previous: d
					});
					g = a.href;
					switch (c) {
						case "inline":
						case "ajax":
						case "html":
							a.selector ? e = f("<div>").html(e).find(a.selector) : t(e) && (e.data("fancybox-placeholder") || e.data("fancybox-placeholder", f('<div class="fancybox-placeholder"></div>').insertAfter(e).hide()), e = e.show().detach(), a.wrap.bind("onReset", function () {
								f(this).find(e).length && e.hide().replaceAll(e.data("fancybox-placeholder")).data("fancybox-placeholder", !1)
							}));
							break;
						case "image":
							e = a.tpl.image.replace("{href}", g);
							break;
						case "swf":
							e = '<object id="fancybox-swf" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="100%" height="100%"><param name="movie" value="' + g + '"></param>', h = "", f.each(a.swf, function (a, b) {
								e += '<param name="' + a + '" value="' + b + '"></param>';
								h += " " + a + '="' + b + '"'
							}), e += '<embed src="' + g + '" type="application/x-shockwave-flash" width="100%" height="100%"' + h + "></embed></object>"
					}(!t(e) || !e.parent().is(a.inner)) && a.inner.append(e);
					b.trigger("beforeShow");
					a.inner.css("overflow", "yes" === k ? "scroll" : "no" === k ? "hidden" : k);
					b._setDimension();
					b.reposition();
					b.isOpen = !1;
					b.coming = null;
					b.bindEvents();
					if (b.isOpened) {
						if (d.prevMethod) b.transitions[d.prevMethod]()
					} else f(".fancybox-wrap").not(a.wrap).stop(!0).trigger("onReset").remove();
					b.transitions[b.isOpened ? a.nextMethod : a.openMethod]();
					b._preloadImages()
				}
		},
		_setDimension: function () {
			var a = b.getViewport(),
				d = 0,
				e = !1,
				c = !1,
				e = b.wrap,
				k = b.skin,
				g = b.inner,
				h = b.current,
				c = h.width,
				j = h.height,
				m = h.minWidth,
				u = h.minHeight,
				n = h.maxWidth,
				v = h.maxHeight,
				s = h.scrolling,
				q = h.scrollOutside ? h.scrollbarWidth : 0,
				y = h.margin,
				p = l(y[1] + y[3]),
				r = l(y[0] + y[2]),
				z, A, t, D, B, G, C, E, w;
			e.add(k).add(g).width("auto").height("auto").removeClass("fancybox-tmp");
			y = l(k.outerWidth(!0) - k.width());
			z = l(k.outerHeight(!0) - k.height());
			A = p + y;
			t = r + z;
			D = F(c) ? (a.w - A) * l(c) / 100 : c;
			B = F(j) ? (a.h - t) * l(j) / 100 : j;
			if ("iframe" === h.type) {
				if (w = h.content, h.autoHeight && 1 === w.data("ready")) try {
					w[0].contentWindow.document.location && (g.width(D).height(9999), G = w.contents().find("body"), q && G.css("overflow-x", "hidden"), B = G.height())
				} catch (H) {}
			} else if (h.autoWidth || h.autoHeight) g.addClass("fancybox-tmp"), h.autoWidth || g.width(D), h.autoHeight || g.height(B), h.autoWidth && (D = g.width()), h.autoHeight && (B = g.height()), g.removeClass("fancybox-tmp");
			c = l(D);
			j = l(B);
			E = D / B;
			m = l(F(m) ? l(m, "w") - A : m);
			n = l(F(n) ? l(n, "w") - A : n);
			u = l(F(u) ? l(u, "h") - t : u);
			v = l(F(v) ? l(v, "h") - t : v);
			G = n;
			C = v;
			h.fitToView && (n = Math.min(a.w - A, n), v = Math.min(a.h - t, v));
			A = a.w - p;
			r = a.h - r;
			h.aspectRatio ? (c > n && (c = n, j = l(c / E)), j > v && (j = v, c = l(j * E)), c < m && (c = m, j = l(c / E)), j < u && (j = u, c = l(j * E))) : (c = Math.max(m, Math.min(c, n)), h.autoHeight && "iframe" !== h.type && (g.width(c), j = g.height()), j = Math.max(u, Math.min(j, v)));
			if (h.fitToView)
				if (g.width(c).height(j), e.width(c + y), a = e.width(), p = e.height(), h.aspectRatio)
					for (;
						(a > A || p > r) && (c > m && j > u) && !(19 < d++);) j = Math.max(u, Math.min(v, j - 10)), c = l(j * E), c < m && (c = m, j = l(c / E)), c > n && (c = n, j = l(c / E)), g.width(c).height(j), e.width(c + y), a = e.width(), p = e.height();
				else c = Math.max(m, Math.min(c, c - (a - A))), j = Math.max(u, Math.min(j, j - (p - r)));
			q && ("auto" === s && j < B && c + y +
				q < A) && (c += q);
			g.width(c).height(j);
			e.width(c + y);
			a = e.width();
			p = e.height();
			e = (a > A || p > r) && c > m && j > u;
			c = h.aspectRatio ? c < G && j < C && c < D && j < B : (c < G || j < C) && (c < D || j < B);
			f.extend(h, {
				dim: {
					width: x(a),
					height: x(p)
				},
				origWidth: D,
				origHeight: B,
				canShrink: e,
				canExpand: c,
				wPadding: y,
				hPadding: z,
				wrapSpace: p - k.outerHeight(!0),
				skinSpace: k.height() - j
			});
			!w && (h.autoHeight && j > u && j < v && !c) && g.height("auto")
		},
		_getPosition: function (a) {
			var d = b.current,
				e = b.getViewport(),
				c = d.margin,
				f = b.wrap.width() + c[1] + c[3],
				g = b.wrap.height() + c[0] + c[2],
				c = {
					position: "absolute",
					top: c[0],
					left: c[3]
				};
			d.autoCenter && d.fixed && !a && g <= e.h && f <= e.w ? c.position = "fixed" : d.locked || (c.top += e.y, c.left += e.x);
			c.top = x(Math.max(c.top, c.top + (e.h - g) * d.topRatio));
			c.left = x(Math.max(c.left, c.left + (e.w - f) * d.leftRatio));
			return c
		},
		_afterZoomIn: function () {
			var a = b.current;
			a && (b.isOpen = b.isOpened = !0, b.wrap.css("overflow", "visible").addClass("fancybox-opened"), b.update(), (a.closeClick || a.nextClick && 1 < b.group.length) && b.inner.css("cursor", "pointer").bind("click.fb", function (d) {
				!f(d.target).is("a") && !f(d.target).parent().is("a") && (d.preventDefault(), b[a.closeClick ? "close" : "next"]())
			}), a.closeBtn && f(a.tpl.closeBtn).appendTo(b.skin).bind("click.fb", function (a) {
				a.preventDefault();
				b.close()
			}), a.arrows && 1 < b.group.length && ((a.loop || 0 < a.index) && f(a.tpl.prev).appendTo(b.outer).bind("click.fb", b.prev), (a.loop || a.index < b.group.length - 1) && f(a.tpl.next).appendTo(b.outer).bind("click.fb", b.next)), b.trigger("afterShow"), !a.loop && a.index === a.group.length - 1 ? b.play(!1) : b.opts.autoPlay && !b.player.isActive && (b.opts.autoPlay = !1, b.play()))
		},
		_afterZoomOut: function (a) {
			a = a || b.current;
			f(".fancybox-wrap").trigger("onReset").remove();
			f.extend(b, {
				group: {},
				opts: {},
				router: !1,
				current: null,
				isActive: !1,
				isOpened: !1,
				isOpen: !1,
				isClosing: !1,
				wrap: null,
				skin: null,
				outer: null,
				inner: null
			});
			b.trigger("afterClose", a)
		}
	});
	b.transitions = {
		getOrigPosition: function () {
			var a = b.current,
				d = a.element,
				e = a.orig,
				c = {},
				f = 50,
				g = 50,
				h = a.hPadding,
				j = a.wPadding,
				m = b.getViewport();
			!e && (a.isDom && d.is(":visible")) && (e = d.find("img:first"), e.length || (e = d));
			t(e) ? (c = e.offset(), e.is("img") && (f = e.outerWidth(), g = e.outerHeight())) : (c.top = m.y + (m.h - g) * a.topRatio, c.left = m.x + (m.w - f) * a.leftRatio);
			if ("fixed" === b.wrap.css("position") || a.locked) c.top -= m.y, c.left -= m.x;
			return c = {
				top: x(c.top - h * a.topRatio),
				left: x(c.left - j * a.leftRatio),
				width: x(f + j),
				height: x(g + h)
			}
		},
		step: function (a, d) {
			var e, c, f = d.prop;
			c = b.current;
			var g = c.wrapSpace,
				h = c.skinSpace;
			if ("width" === f || "height" === f) e = d.end === d.start ? 1 : (a - d.start) / (d.end - d.start), b.isClosing && (e = 1 - e), c = "width" === f ? c.wPadding : c.hPadding, c = a - c, b.skin[f](l("width" === f ? c : c - g * e)), b.inner[f](l("width" === f ? c : c - g * e - h * e))
		},
		zoomIn: function () {
			var a = b.current,
				d = a.pos,
				e = a.openEffect,
				c = "elastic" === e,
				k = f.extend({
					opacity: 1
				}, d);
			delete k.position;
			c ? (d = this.getOrigPosition(), a.openOpacity && (d.opacity = 0.1)) : "fade" === e && (d.opacity = 0.1);
			b.wrap.css(d).animate(k, {
				duration: "none" === e ? 0 : a.openSpeed,
				easing: a.openEasing,
				step: c ? this.step : null,
				complete: b._afterZoomIn
			})
		},
		zoomOut: function () {
			var a = b.current,
				d = a.closeEffect,
				e = "elastic" === d,
				c = {
					opacity: 0.1
				};
			e && (c = this.getOrigPosition(), a.closeOpacity && (c.opacity = 0.1));
			b.wrap.animate(c, {
				duration: "none" === d ? 0 : a.closeSpeed,
				easing: a.closeEasing,
				step: e ? this.step : null,
				complete: b._afterZoomOut
			})
		},
		changeIn: function () {
			var a = b.current,
				d = a.nextEffect,
				e = a.pos,
				c = {
					opacity: 1
				},
				f = b.direction,
				g;
			e.opacity = 0.1;
			"elastic" === d && (g = "down" === f || "up" === f ? "top" : "left", "down" === f || "right" === f ? (e[g] = x(l(e[g]) - 200), c[g] = "+=200px") : (e[g] = x(l(e[g]) + 200), c[g] = "-=200px"));
			"none" === d ? b._afterZoomIn() : b.wrap.css(e).animate(c, {
				duration: a.nextSpeed,
				easing: a.nextEasing,
				complete: b._afterZoomIn
			})
		},
		changeOut: function () {
			var a = b.previous,
				d = a.prevEffect,
				e = {
					opacity: 0.1
				},
				c = b.direction;
			"elastic" === d && (e["down" === c || "up" === c ? "top" : "left"] = ("up" === c || "left" === c ? "-" : "+") + "=200px");
			a.wrap.animate(e, {
				duration: "none" === d ? 0 : a.prevSpeed,
				easing: a.prevEasing,
				complete: function () {
					f(this).trigger("onReset").remove()
				}
			})
		}
	};
	b.helpers.overlay = {
		defaults: {
			closeClick: !0,
			speedOut: 200,
			showEarly: !0,
			css: {},
			locked: !s,
			fixed: !0
		},
		overlay: null,
		fixed: !1,
		create: function (a) {
			a = f.extend({}, this.defaults, a);
			this.overlay && this.close();
			this.overlay = f('<div class="fancybox-overlay"></div>').appendTo("body");
			this.fixed = !1;
			a.fixed && b.defaults.fixed && (this.overlay.addClass("fancybox-overlay-fixed"), this.fixed = !0)
		},
		open: function (a) {
			var d = this;
			a = f.extend({}, this.defaults, a);
			this.overlay ? this.overlay.unbind(".overlay").width("auto").height("auto") : this.create(a);
			this.fixed || (q.bind("resize.overlay", f.proxy(this.update, this)), this.update());
			a.closeClick && this.overlay.bind("click.overlay", function (a) {
				f(a.target).hasClass("fancybox-overlay") && (b.isActive ? b.close() : d.close())
			});
			this.overlay.css(a.css).show()
		},
		close: function () {
			f(".fancybox-overlay").remove();
			q.unbind("resize.overlay");
			this.overlay = null;
			!1 !== this.margin && (f("body").css("margin-right", this.margin), this.margin = !1);
			this.el && this.el.removeClass("fancybox-lock")
		},
		update: function () {
			var a = "100%",
				b;
			this.overlay.width(a).height("100%");
			H ? (b = Math.max(z.documentElement.offsetWidth, z.body.offsetWidth), n.width() > b && (a = n.width())) : n.width() > q.width() && (a = n.width());
			this.overlay.width(a).height(n.height())
		},
		onReady: function (a, b) {
			f(".fancybox-overlay").stop(!0, !0);
			this.overlay || (this.margin = n.height() > q.height() || "scroll" === f("body").css("overflow-y") ? f("body").css("margin-right") : !1, this.el = z.all && !z.querySelector ? f("html") : f("body"), this.create(a));
			a.locked && this.fixed && (b.locked = this.overlay.append(b.wrap), b.fixed = !1);
			!0 === a.showEarly && this.beforeShow.apply(this, arguments)
		},
		beforeShow: function (a, b) {
			b.locked && (this.el.addClass("fancybox-lock"), !1 !== this.margin && f("body").css("margin-right", l(this.margin) + b.scrollbarWidth));
			this.open(a)
		},
		onUpdate: function () {
			this.fixed || this.update()
		},
		afterClose: function (a) {
			this.overlay && !b.isActive && this.overlay.fadeOut(a.speedOut, f.proxy(this.close, this))
		}
	};
	b.helpers.title = {
		defaults: {
			type: "float",
			position: "bottom"
		},
		beforeShow: function (a) {
			var d = b.current,
				e = d.title,
				c = a.type;
			f.isFunction(e) && (e = e.call(d.element, d));
			if (p(e) && "" !== f.trim(e)) {
				d = f('<div class="fancybox-title fancybox-title-' + c + '-wrap">' + e + "</div>");
				switch (c) {
					case "inside":
						c = b.skin;
						break;
					case "outside":
						c = b.wrap;
						break;
					case "over":
						c = b.inner;
						break;
					default:
						c = b.skin, d.appendTo("body"), H && d.width(d.width()), d.wrapInner('<span class="child"></span>'), b.current.margin[2] += Math.abs(l(d.css("margin-bottom")))
				}
				d["top" === a.position ? "prependTo" : "appendTo"](c)
			}
		}
	};
	f.fn.fancybox = function (a) {
		var d, e = f(this),
			c = this.selector || "",
			k = function (g) {
				var h = f(this).blur(),
					j = d,
					k, l;
				!g.ctrlKey && (!g.altKey && !g.shiftKey && !g.metaKey) && !h.is(".fancybox-wrap") && (k = a.groupAttr || "data-fancybox-group", l = h.attr(k), l || (k = "rel", l = h.get(0)[k]), l && ("" !== l && "nofollow" !== l) && (h = c.length ? f(c) : e, h = h.filter("[" + k + '="' + l + '"]'), j = h.index(this)), a.index = j, !1 !== b.open(h, a) && g.preventDefault())
			};
		a = a || {};
		d = a.index || 0;
		!c || !1 === a.live ? e.unbind("click.fb-start").bind("click.fb-start", k) : n.undelegate(c, "click.fb-start").delegate(c + ":not('.fancybox-item, .fancybox-nav')", "click.fb-start", k);
		this.filter("[data-fancybox-start=1]").trigger("click");
		return this
	};
	n.ready(function () {
		f.scrollbarWidth === r && (f.scrollbarWidth = function () {
			var a = f('<div style="width:50px;height:50px;overflow:auto"><div/></div>').appendTo("body"),
				b = a.children(),
				b = b.innerWidth() - b.height(99).innerWidth();
			a.remove();
			return b
		});
		if (f.support.fixedPosition === r) {
			var a = f.support,
				d = f('<div style="position:fixed;top:20px;"></div>').appendTo("body"),
				e = 20 === d[0].offsetTop || 15 === d[0].offsetTop;
			d.remove();
			a.fixedPosition = e
		}
		f.extend(b.defaults, {
			scrollbarWidth: f.scrollbarWidth(),
			fixed: f.support.fixedPosition,
			parent: f("body")
		})
	})
})(window, document, jQuery);;
(function ($) {
	$('a.fancy, .fancy-link-inner a').fancybox();
	$('a.fancy-gallery').fancybox({
		helpers: {
			title: {
				type: 'over'
			}
		},
		wrapCSS: 'slideinfo',
		beforeLoad: function () {
			var clone = $(this.element).children('.slide-info').clone();
			if (clone.length) {
				this.title = clone.html();
			}
		}
	});
	$('.portfolio-item a.vimeo, .portfolio-item a.youtube, .blog article a.youtube, .blog article a.vimeo').fancybox({
		type: 'iframe'
	});
	$('.portfolio-item a.self_video').fancybox({
		width: '80%',
		height: '80%',
		autoSize: false,
		content: '<div id="fancybox-video"></div>',
		afterShow: function () {
			var $video = $('<video width="100%" height="100%" autoplay="autoplay" controls="controls" src="' + this.element.attr('href') + '" preload="none"></video>');
			$video.appendTo($('#fancybox-video'));
			$video.mediaelementplayer();
		}
	});
})(jQuery);;
(function ($) {
	$(function () {
		$('body').updateAccordions();
	});
	$.fn.updateAccordions = function () {
		$('.gem_accordion', this).each(function (index) {
			var $accordion = $(this);
			$accordion.thegemPreloader(function () {
				var $tabs, interval = $accordion.attr("data-interval"),
					active_tab = !isNaN($accordion.data('active-tab')) && parseInt($accordion.data('active-tab')) > 0 ? parseInt($accordion.data('active-tab')) - 1 : false,
					collapsible = $accordion.data('collapsible') === 'yes';
				$tabs = $accordion.find('.gem_accordion_wrapper').accordion({
					header: "> div > .gem_accordion_header",
					autoHeight: false,
					heightStyle: "content",
					active: active_tab,
					collapsible: collapsible,
					navigation: true,
					activate: function (event, ui) {
						if (ui.newPanel.size() > 0) {
							ui.newPanel.trigger('accordion-update');
						}
					},
					beforeActivate: function (event, ui) {
						if (ui.newPanel.size() > 0) {
							$("html, body").animate({
								scrollTop: ui.newPanel.closest('.gem_accordion').offset().top - 200
							}, 300);
						}
					}
				});
			});
		});
	}
})(jQuery);;
/*!
 * jQuery Form Plugin
 * version: 3.51.0-2014.06.20
 * Requires jQuery v1.5 or later
 * Copyright (c) 2014 M. Alsup
 * Examples and documentation at: http://malsup.com/jquery/form/
 * Project repository: https://github.com/malsup/form
 * Dual licensed under the MIT and GPL licenses.
 * https://github.com/malsup/form#copyright-and-license
 */
! function (e) {
	"use strict";
	"function" == typeof define && define.amd ? define(["jquery"], e) : e("undefined" != typeof jQuery ? jQuery : window.Zepto)
}(function (e) {
	"use strict";

	function t(t) {
		var r = t.data;
		t.isDefaultPrevented() || (t.preventDefault(), e(t.target).ajaxSubmit(r))
	}

	function r(t) {
		var r = t.target,
			a = e(r);
		if (!a.is("[type=submit],[type=image]")) {
			var n = a.closest("[type=submit]");
			if (0 === n.length) return;
			r = n[0]
		}
		var i = this;
		if (i.clk = r, "image" == r.type)
			if (void 0 !== t.offsetX) i.clk_x = t.offsetX, i.clk_y = t.offsetY;
			else if ("function" == typeof e.fn.offset) {
			var o = a.offset();
			i.clk_x = t.pageX - o.left, i.clk_y = t.pageY - o.top
		} else i.clk_x = t.pageX - r.offsetLeft, i.clk_y = t.pageY - r.offsetTop;
		setTimeout(function () {
			i.clk = i.clk_x = i.clk_y = null
		}, 100)
	}

	function a() {
		if (e.fn.ajaxSubmit.debug) {
			var t = "[jquery.form] " + Array.prototype.join.call(arguments, "");
			window.console && window.console.log ? window.console.log(t) : window.opera && window.opera.postError && window.opera.postError(t)
		}
	}
	var n = {};
	n.fileapi = void 0 !== e("<input type='file'/>").get(0).files, n.formdata = void 0 !== window.FormData;
	var i = !!e.fn.prop;
	e.fn.attr2 = function () {
		if (!i) return this.attr.apply(this, arguments);
		var e = this.prop.apply(this, arguments);
		return e && e.jquery || "string" == typeof e ? e : this.attr.apply(this, arguments)
	}, e.fn.ajaxSubmit = function (t) {
		function r(r) {
			var a, n, i = e.param(r, t.traditional).split("&"),
				o = i.length,
				s = [];
			for (a = 0; o > a; a++) i[a] = i[a].replace(/\+/g, " "), n = i[a].split("="), s.push([decodeURIComponent(n[0]), decodeURIComponent(n[1])]);
			return s
		}

		function o(a) {
			for (var n = new FormData, i = 0; i < a.length; i++) n.append(a[i].name, a[i].value);
			if (t.extraData) {
				var o = r(t.extraData);
				for (i = 0; i < o.length; i++) o[i] && n.append(o[i][0], o[i][1])
			}
			t.data = null;
			var s = e.extend(!0, {}, e.ajaxSettings, t, {
				contentType: !1,
				processData: !1,
				cache: !1,
				type: u || "POST"
			});
			t.uploadProgress && (s.xhr = function () {
				var r = e.ajaxSettings.xhr();
				return r.upload && r.upload.addEventListener("progress", function (e) {
					var r = 0,
						a = e.loaded || e.position,
						n = e.total;
					e.lengthComputable && (r = Math.ceil(a / n * 100)), t.uploadProgress(e, a, n, r)
				}, !1), r
			}), s.data = null;
			var c = s.beforeSend;
			return s.beforeSend = function (e, r) {
				r.data = t.formData ? t.formData : n, c && c.call(this, e, r)
			}, e.ajax(s)
		}

		function s(r) {
			function n(e) {
				var t = null;
				try {
					e.contentWindow && (t = e.contentWindow.document)
				} catch (r) {
					a("cannot get iframe.contentWindow document: " + r)
				}
				if (t) return t;
				try {
					t = e.contentDocument ? e.contentDocument : e.document
				} catch (r) {
					a("cannot get iframe.contentDocument: " + r), t = e.document
				}
				return t
			}

			function o() {
				function t() {
					try {
						var e = n(g).readyState;
						a("state = " + e), e && "uninitialized" == e.toLowerCase() && setTimeout(t, 50)
					} catch (r) {
						a("Server abort: ", r, " (", r.name, ")"), s(k), j && clearTimeout(j), j = void 0
					}
				}
				var r = f.attr2("target"),
					i = f.attr2("action"),
					o = "multipart/form-data",
					c = f.attr("enctype") || f.attr("encoding") || o;
				w.setAttribute("target", p), (!u || /post/i.test(u)) && w.setAttribute("method", "POST"), i != m.url && w.setAttribute("action", m.url), m.skipEncodingOverride || u && !/post/i.test(u) || f.attr({
					encoding: "multipart/form-data",
					enctype: "multipart/form-data"
				}), m.timeout && (j = setTimeout(function () {
					T = !0, s(D)
				}, m.timeout));
				var l = [];
				try {
					if (m.extraData)
						for (var d in m.extraData) m.extraData.hasOwnProperty(d) && l.push(e.isPlainObject(m.extraData[d]) && m.extraData[d].hasOwnProperty("name") && m.extraData[d].hasOwnProperty("value") ? e('<input type="hidden" name="' + m.extraData[d].name + '">').val(m.extraData[d].value).appendTo(w)[0] : e('<input type="hidden" name="' + d + '">').val(m.extraData[d]).appendTo(w)[0]);
					m.iframeTarget || v.appendTo("body"), g.attachEvent ? g.attachEvent("onload", s) : g.addEventListener("load", s, !1), setTimeout(t, 15);
					try {
						w.submit()
					} catch (h) {
						var x = document.createElement("form").submit;
						x.apply(w)
					}
				} finally {
					w.setAttribute("action", i), w.setAttribute("enctype", c), r ? w.setAttribute("target", r) : f.removeAttr("target"), e(l).remove()
				}
			}

			function s(t) {
				if (!x.aborted && !F) {
					if (M = n(g), M || (a("cannot access response document"), t = k), t === D && x) return x.abort("timeout"), void S.reject(x, "timeout");
					if (t == k && x) return x.abort("server abort"), void S.reject(x, "error", "server abort");
					if (M && M.location.href != m.iframeSrc || T) {
						g.detachEvent ? g.detachEvent("onload", s) : g.removeEventListener("load", s, !1);
						var r, i = "success";
						try {
							if (T) throw "timeout";
							var o = "xml" == m.dataType || M.XMLDocument || e.isXMLDoc(M);
							if (a("isXml=" + o), !o && window.opera && (null === M.body || !M.body.innerHTML) && --O) return a("requeing onLoad callback, DOM not available"), void setTimeout(s, 250);
							var u = M.body ? M.body : M.documentElement;
							x.responseText = u ? u.innerHTML : null, x.responseXML = M.XMLDocument ? M.XMLDocument : M, o && (m.dataType = "xml"), x.getResponseHeader = function (e) {
								var t = {
									"content-type": m.dataType
								};
								return t[e.toLowerCase()]
							}, u && (x.status = Number(u.getAttribute("status")) || x.status, x.statusText = u.getAttribute("statusText") || x.statusText);
							var c = (m.dataType || "").toLowerCase(),
								l = /(json|script|text)/.test(c);
							if (l || m.textarea) {
								var f = M.getElementsByTagName("textarea")[0];
								if (f) x.responseText = f.value, x.status = Number(f.getAttribute("status")) || x.status, x.statusText = f.getAttribute("statusText") || x.statusText;
								else if (l) {
									var p = M.getElementsByTagName("pre")[0],
										h = M.getElementsByTagName("body")[0];
									p ? x.responseText = p.textContent ? p.textContent : p.innerText : h && (x.responseText = h.textContent ? h.textContent : h.innerText)
								}
							} else "xml" == c && !x.responseXML && x.responseText && (x.responseXML = X(x.responseText));
							try {
								E = _(x, c, m)
							} catch (y) {
								i = "parsererror", x.error = r = y || i
							}
						} catch (y) {
							a("error caught: ", y), i = "error", x.error = r = y || i
						}
						x.aborted && (a("upload aborted"), i = null), x.status && (i = x.status >= 200 && x.status < 300 || 304 === x.status ? "success" : "error"), "success" === i ? (m.success && m.success.call(m.context, E, "success", x), S.resolve(x.responseText, "success", x), d && e.event.trigger("ajaxSuccess", [x, m])) : i && (void 0 === r && (r = x.statusText), m.error && m.error.call(m.context, x, i, r), S.reject(x, "error", r), d && e.event.trigger("ajaxError", [x, m, r])), d && e.event.trigger("ajaxComplete", [x, m]), d && !--e.active && e.event.trigger("ajaxStop"), m.complete && m.complete.call(m.context, x, i), F = !0, m.timeout && clearTimeout(j), setTimeout(function () {
							m.iframeTarget ? v.attr("src", m.iframeSrc) : v.remove(), x.responseXML = null
						}, 100)
					}
				}
			}
			var c, l, m, d, p, v, g, x, y, b, T, j, w = f[0],
				S = e.Deferred();
			if (S.abort = function (e) {
					x.abort(e)
				}, r)
				for (l = 0; l < h.length; l++) c = e(h[l]), i ? c.prop("disabled", !1) : c.removeAttr("disabled");
			if (m = e.extend(!0, {}, e.ajaxSettings, t), m.context = m.context || m, p = "jqFormIO" + (new Date).getTime(), m.iframeTarget ? (v = e(m.iframeTarget), b = v.attr2("name"), b ? p = b : v.attr2("name", p)) : (v = e('<iframe name="' + p + '" src="' + m.iframeSrc + '" />'), v.css({
					position: "absolute",
					top: "-1000px",
					left: "-1000px"
				})), g = v[0], x = {
					aborted: 0,
					responseText: null,
					responseXML: null,
					status: 0,
					statusText: "n/a",
					getAllResponseHeaders: function () {},
					getResponseHeader: function () {},
					setRequestHeader: function () {},
					abort: function (t) {
						var r = "timeout" === t ? "timeout" : "aborted";
						a("aborting upload... " + r), this.aborted = 1;
						try {
							g.contentWindow.document.execCommand && g.contentWindow.document.execCommand("Stop")
						} catch (n) {}
						v.attr("src", m.iframeSrc), x.error = r, m.error && m.error.call(m.context, x, r, t), d && e.event.trigger("ajaxError", [x, m, r]), m.complete && m.complete.call(m.context, x, r)
					}
				}, d = m.global, d && 0 === e.active++ && e.event.trigger("ajaxStart"), d && e.event.trigger("ajaxSend", [x, m]), m.beforeSend && m.beforeSend.call(m.context, x, m) === !1) return m.global && e.active--, S.reject(), S;
			if (x.aborted) return S.reject(), S;
			y = w.clk, y && (b = y.name, b && !y.disabled && (m.extraData = m.extraData || {}, m.extraData[b] = y.value, "image" == y.type && (m.extraData[b + ".x"] = w.clk_x, m.extraData[b + ".y"] = w.clk_y)));
			var D = 1,
				k = 2,
				A = e("meta[name=csrf-token]").attr("content"),
				L = e("meta[name=csrf-param]").attr("content");
			L && A && (m.extraData = m.extraData || {}, m.extraData[L] = A), m.forceSync ? o() : setTimeout(o, 10);
			var E, M, F, O = 50,
				X = e.parseXML || function (e, t) {
					return window.ActiveXObject ? (t = new ActiveXObject("Microsoft.XMLDOM"), t.async = "false", t.loadXML(e)) : t = (new DOMParser).parseFromString(e, "text/xml"), t && t.documentElement && "parsererror" != t.documentElement.nodeName ? t : null
				},
				C = e.parseJSON || function (e) {
					return window.eval("(" + e + ")")
				},
				_ = function (t, r, a) {
					var n = t.getResponseHeader("content-type") || "",
						i = "xml" === r || !r && n.indexOf("xml") >= 0,
						o = i ? t.responseXML : t.responseText;
					return i && "parsererror" === o.documentElement.nodeName && e.error && e.error("parsererror"), a && a.dataFilter && (o = a.dataFilter(o, r)), "string" == typeof o && ("json" === r || !r && n.indexOf("json") >= 0 ? o = C(o) : ("script" === r || !r && n.indexOf("javascript") >= 0) && e.globalEval(o)), o
				};
			return S
		}
		if (!this.length) return a("ajaxSubmit: skipping submit process - no element selected"), this;
		var u, c, l, f = this;
		"function" == typeof t ? t = {
			success: t
		} : void 0 === t && (t = {}), u = t.type || this.attr2("method"), c = t.url || this.attr2("action"), l = "string" == typeof c ? e.trim(c) : "", l = l || window.location.href || "", l && (l = (l.match(/^([^#]+)/) || [])[1]), t = e.extend(!0, {
			url: l,
			success: e.ajaxSettings.success,
			type: u || e.ajaxSettings.type,
			iframeSrc: /^https/i.test(window.location.href || "") ? "javascript:false" : "about:blank"
		}, t);
		var m = {};
		if (this.trigger("form-pre-serialize", [this, t, m]), m.veto) return a("ajaxSubmit: submit vetoed via form-pre-serialize trigger"), this;
		if (t.beforeSerialize && t.beforeSerialize(this, t) === !1) return a("ajaxSubmit: submit aborted via beforeSerialize callback"), this;
		var d = t.traditional;
		void 0 === d && (d = e.ajaxSettings.traditional);
		var p, h = [],
			v = this.formToArray(t.semantic, h);
		if (t.data && (t.extraData = t.data, p = e.param(t.data, d)), t.beforeSubmit && t.beforeSubmit(v, this, t) === !1) return a("ajaxSubmit: submit aborted via beforeSubmit callback"), this;
		if (this.trigger("form-submit-validate", [v, this, t, m]), m.veto) return a("ajaxSubmit: submit vetoed via form-submit-validate trigger"), this;
		var g = e.param(v, d);
		p && (g = g ? g + "&" + p : p), "GET" == t.type.toUpperCase() ? (t.url += (t.url.indexOf("?") >= 0 ? "&" : "?") + g, t.data = null) : t.data = g;
		var x = [];
		if (t.resetForm && x.push(function () {
				f.resetForm()
			}), t.clearForm && x.push(function () {
				f.clearForm(t.includeHidden)
			}), !t.dataType && t.target) {
			var y = t.success || function () {};
			x.push(function (r) {
				var a = t.replaceTarget ? "replaceWith" : "html";
				e(t.target)[a](r).each(y, arguments)
			})
		} else t.success && x.push(t.success);
		if (t.success = function (e, r, a) {
				for (var n = t.context || this, i = 0, o = x.length; o > i; i++) x[i].apply(n, [e, r, a || f, f])
			}, t.error) {
			var b = t.error;
			t.error = function (e, r, a) {
				var n = t.context || this;
				b.apply(n, [e, r, a, f])
			}
		}
		if (t.complete) {
			var T = t.complete;
			t.complete = function (e, r) {
				var a = t.context || this;
				T.apply(a, [e, r, f])
			}
		}
		var j = e("input[type=file]:enabled", this).filter(function () {
				return "" !== e(this).val()
			}),
			w = j.length > 0,
			S = "multipart/form-data",
			D = f.attr("enctype") == S || f.attr("encoding") == S,
			k = n.fileapi && n.formdata;
		a("fileAPI :" + k);
		var A, L = (w || D) && !k;
		t.iframe !== !1 && (t.iframe || L) ? t.closeKeepAlive ? e.get(t.closeKeepAlive, function () {
			A = s(v)
		}) : A = s(v) : A = (w || D) && k ? o(v) : e.ajax(t), f.removeData("jqxhr").data("jqxhr", A);
		for (var E = 0; E < h.length; E++) h[E] = null;
		return this.trigger("form-submit-notify", [this, t]), this
	}, e.fn.ajaxForm = function (n) {
		if (n = n || {}, n.delegation = n.delegation && e.isFunction(e.fn.on), !n.delegation && 0 === this.length) {
			var i = {
				s: this.selector,
				c: this.context
			};
			return !e.isReady && i.s ? (a("DOM not ready, queuing ajaxForm"), e(function () {
				e(i.s, i.c).ajaxForm(n)
			}), this) : (a("terminating; zero elements found by selector" + (e.isReady ? "" : " (DOM not ready)")), this)
		}
		return n.delegation ? (e(document).off("submit.form-plugin", this.selector, t).off("click.form-plugin", this.selector, r).on("submit.form-plugin", this.selector, n, t).on("click.form-plugin", this.selector, n, r), this) : this.ajaxFormUnbind().bind("submit.form-plugin", n, t).bind("click.form-plugin", n, r)
	}, e.fn.ajaxFormUnbind = function () {
		return this.unbind("submit.form-plugin click.form-plugin")
	}, e.fn.formToArray = function (t, r) {
		var a = [];
		if (0 === this.length) return a;
		var i, o = this[0],
			s = this.attr("id"),
			u = t ? o.getElementsByTagName("*") : o.elements;
		if (u && !/MSIE [678]/.test(navigator.userAgent) && (u = e(u).get()), s && (i = e(':input[form="' + s + '"]').get(), i.length && (u = (u || []).concat(i))), !u || !u.length) return a;
		var c, l, f, m, d, p, h;
		for (c = 0, p = u.length; p > c; c++)
			if (d = u[c], f = d.name, f && !d.disabled)
				if (t && o.clk && "image" == d.type) o.clk == d && (a.push({
					name: f,
					value: e(d).val(),
					type: d.type
				}), a.push({
					name: f + ".x",
					value: o.clk_x
				}, {
					name: f + ".y",
					value: o.clk_y
				}));
				else if (m = e.fieldValue(d, !0), m && m.constructor == Array)
			for (r && r.push(d), l = 0, h = m.length; h > l; l++) a.push({
				name: f,
				value: m[l]
			});
		else if (n.fileapi && "file" == d.type) {
			r && r.push(d);
			var v = d.files;
			if (v.length)
				for (l = 0; l < v.length; l++) a.push({
					name: f,
					value: v[l],
					type: d.type
				});
			else a.push({
				name: f,
				value: "",
				type: d.type
			})
		} else null !== m && "undefined" != typeof m && (r && r.push(d), a.push({
			name: f,
			value: m,
			type: d.type,
			required: d.required
		}));
		if (!t && o.clk) {
			var g = e(o.clk),
				x = g[0];
			f = x.name, f && !x.disabled && "image" == x.type && (a.push({
				name: f,
				value: g.val()
			}), a.push({
				name: f + ".x",
				value: o.clk_x
			}, {
				name: f + ".y",
				value: o.clk_y
			}))
		}
		return a
	}, e.fn.formSerialize = function (t) {
		return e.param(this.formToArray(t))
	}, e.fn.fieldSerialize = function (t) {
		var r = [];
		return this.each(function () {
			var a = this.name;
			if (a) {
				var n = e.fieldValue(this, t);
				if (n && n.constructor == Array)
					for (var i = 0, o = n.length; o > i; i++) r.push({
						name: a,
						value: n[i]
					});
				else null !== n && "undefined" != typeof n && r.push({
					name: this.name,
					value: n
				})
			}
		}), e.param(r)
	}, e.fn.fieldValue = function (t) {
		for (var r = [], a = 0, n = this.length; n > a; a++) {
			var i = this[a],
				o = e.fieldValue(i, t);
			null === o || "undefined" == typeof o || o.constructor == Array && !o.length || (o.constructor == Array ? e.merge(r, o) : r.push(o))
		}
		return r
	}, e.fieldValue = function (t, r) {
		var a = t.name,
			n = t.type,
			i = t.tagName.toLowerCase();
		if (void 0 === r && (r = !0), r && (!a || t.disabled || "reset" == n || "button" == n || ("checkbox" == n || "radio" == n) && !t.checked || ("submit" == n || "image" == n) && t.form && t.form.clk != t || "select" == i && -1 == t.selectedIndex)) return null;
		if ("select" == i) {
			var o = t.selectedIndex;
			if (0 > o) return null;
			for (var s = [], u = t.options, c = "select-one" == n, l = c ? o + 1 : u.length, f = c ? o : 0; l > f; f++) {
				var m = u[f];
				if (m.selected) {
					var d = m.value;
					if (d || (d = m.attributes && m.attributes.value && !m.attributes.value.specified ? m.text : m.value), c) return d;
					s.push(d)
				}
			}
			return s
		}
		return e(t).val()
	}, e.fn.clearForm = function (t) {
		return this.each(function () {
			e("input,select,textarea", this).clearFields(t)
		})
	}, e.fn.clearFields = e.fn.clearInputs = function (t) {
		var r = /^(?:color|date|datetime|email|month|number|password|range|search|tel|text|time|url|week)$/i;
		return this.each(function () {
			var a = this.type,
				n = this.tagName.toLowerCase();
			r.test(a) || "textarea" == n ? this.value = "" : "checkbox" == a || "radio" == a ? this.checked = !1 : "select" == n ? this.selectedIndex = -1 : "file" == a ? /MSIE/.test(navigator.userAgent) ? e(this).replaceWith(e(this).clone(!0)) : e(this).val("") : t && (t === !0 && /hidden/.test(a) || "string" == typeof t && e(this).is(t)) && (this.value = "")
		})
	}, e.fn.resetForm = function () {
		return this.each(function () {
			("function" == typeof this.reset || "object" == typeof this.reset && !this.reset.nodeType) && this.reset()
		})
	}, e.fn.enable = function (e) {
		return void 0 === e && (e = !0), this.each(function () {
			this.disabled = !e
		})
	}, e.fn.selected = function (t) {
		return void 0 === t && (t = !0), this.each(function () {
			var r = this.type;
			if ("checkbox" == r || "radio" == r) this.checked = t;
			else if ("option" == this.tagName.toLowerCase()) {
				var a = e(this).parent("select");
				t && a[0] && "select-one" == a[0].type && a.find("option").selected(!1), this.selected = t
			}
		})
	}, e.fn.ajaxSubmit.debug = !1
});;
(function ($) {
	'use strict';
	if (typeof _wpcf7 == 'undefined' || _wpcf7 === null) {
		_wpcf7 = {};
	}
	_wpcf7 = $.extend({
		cached: 0
	}, _wpcf7);
	$.fn.wpcf7InitForm = function () {
		this.ajaxForm({
			beforeSubmit: function (arr, $form, options) {
				$form.wpcf7ClearResponseOutput();
				$form.find('[aria-invalid]').attr('aria-invalid', 'false');
				$form.find('img.ajax-loader').css({
					visibility: 'visible'
				});
				return true;
			},
			beforeSerialize: function ($form, options) {
				$form.find('[placeholder].placeheld').each(function (i, n) {
					$(n).val('');
				});
				return true;
			},
			data: {
				'_wpcf7_is_ajax_call': 1
			},
			dataType: 'json',
			success: $.wpcf7AjaxSuccess,
			error: function (xhr, status, error, $form) {
				var e = $('<div class="ajax-error"></div>').text(error.message);
				$form.after(e);
			}
		});
		if (_wpcf7.cached) {
			this.wpcf7OnloadRefill();
		}
		this.wpcf7ToggleSubmit();
		this.find('.wpcf7-submit').wpcf7AjaxLoader();
		this.find('.wpcf7-acceptance').click(function () {
			$(this).closest('form').wpcf7ToggleSubmit();
		});
		this.find('.wpcf7-exclusive-checkbox').wpcf7ExclusiveCheckbox();
		this.find('.wpcf7-list-item.has-free-text').wpcf7ToggleCheckboxFreetext();
		this.find('[placeholder]').wpcf7Placeholder();
		if (_wpcf7.jqueryUi && !_wpcf7.supportHtml5.date) {
			this.find('input.wpcf7-date[type="date"]').each(function () {
				$(this).datepicker({
					dateFormat: 'yy-mm-dd',
					minDate: new Date($(this).attr('min')),
					maxDate: new Date($(this).attr('max'))
				});
			});
		}
		if (_wpcf7.jqueryUi && !_wpcf7.supportHtml5.number) {
			this.find('input.wpcf7-number[type="number"]').each(function () {
				$(this).spinner({
					min: $(this).attr('min'),
					max: $(this).attr('max'),
					step: $(this).attr('step')
				});
			});
		}
		this.find('.wpcf7-character-count').wpcf7CharacterCount();
		this.find('.wpcf7-validates-as-url').change(function () {
			$(this).wpcf7NormalizeUrl();
		});
		this.find('.wpcf7-recaptcha').wpcf7Recaptcha();
	};
	$.wpcf7AjaxSuccess = function (data, status, xhr, $form) {
		if (!$.isPlainObject(data) || $.isEmptyObject(data)) {
			return;
		}
		var $responseOutput = $form.find('div.wpcf7-response-output');
		$form.wpcf7ClearResponseOutput();
		$form.find('.wpcf7-form-control').removeClass('wpcf7-not-valid');
		$form.removeClass('invalid spam sent failed');
		if (data.captcha) {
			$form.wpcf7RefillCaptcha(data.captcha);
		}
		if (data.quiz) {
			$form.wpcf7RefillQuiz(data.quiz);
		}
		if (data.invalids) {
			$.each(data.invalids, function (i, n) {
				$form.find(n.into).wpcf7NotValidTip(n.message);
				$form.find(n.into).find('.wpcf7-form-control').addClass('wpcf7-not-valid');
				$form.find(n.into).find('[aria-invalid]').attr('aria-invalid', 'true');
			});
			$responseOutput.addClass('wpcf7-validation-errors');
			$form.addClass('invalid');
			$(data.into).trigger('wpcf7:invalid');
			$(data.into).trigger('invalid.wpcf7');
		} else if (1 == data.spam) {
			$form.find('[name="g-recaptcha-response"]').each(function () {
				if ('' == $(this).val()) {
					var $recaptcha = $(this).closest('.wpcf7-form-control-wrap');
					$recaptcha.wpcf7NotValidTip(_wpcf7.recaptcha.messages.empty);
				}
			});
			$responseOutput.addClass('wpcf7-spam-blocked');
			$form.addClass('spam');
			$(data.into).trigger('wpcf7:spam');
			$(data.into).trigger('spam.wpcf7');
		} else if (1 == data.mailSent) {
			$responseOutput.addClass('wpcf7-mail-sent-ok');
			$form.addClass('sent');
			if (data.onSentOk) {
				$.each(data.onSentOk, function (i, n) {
					eval(n)
				});
			}
			$(data.into).trigger('wpcf7:mailsent');
			$(data.into).trigger('mailsent.wpcf7');
		} else {
			$responseOutput.addClass('wpcf7-mail-sent-ng');
			$form.addClass('failed');
			$(data.into).trigger('wpcf7:mailfailed');
			$(data.into).trigger('mailfailed.wpcf7');
		}
		if (data.onSubmit) {
			$.each(data.onSubmit, function (i, n) {
				eval(n)
			});
		}
		$(data.into).trigger('wpcf7:submit');
		$(data.into).trigger('submit.wpcf7');
		if (1 == data.mailSent) {
			$form.resetForm();
		}
		$form.find('[placeholder].placeheld').each(function (i, n) {
			$(n).val($(n).attr('placeholder'));
		});
		$responseOutput.append(data.message).slideDown('fast');
		$responseOutput.attr('role', 'alert');
		$.wpcf7UpdateScreenReaderResponse($form, data);
	};
	$.fn.wpcf7ExclusiveCheckbox = function () {
		return this.find('input:checkbox').click(function () {
			var name = $(this).attr('name');
			$(this).closest('form').find('input:checkbox[name="' + name + '"]').not(this).prop('checked', false);
		});
	};
	$.fn.wpcf7Placeholder = function () {
		if (_wpcf7.supportHtml5.placeholder) {
			return this;
		}
		return this.each(function () {
			$(this).val($(this).attr('placeholder'));
			$(this).addClass('placeheld');
			$(this).focus(function () {
				if ($(this).hasClass('placeheld'))
					$(this).val('').removeClass('placeheld');
			});
			$(this).blur(function () {
				if ('' == $(this).val()) {
					$(this).val($(this).attr('placeholder'));
					$(this).addClass('placeheld');
				}
			});
		});
	};
	$.fn.wpcf7AjaxLoader = function () {
		return this.each(function () {
			var loader = $('<img class="ajax-loader" />').attr({
				src: _wpcf7.loaderUrl,
				alt: _wpcf7.sending
			}).css('visibility', 'hidden');
			$(this).after(loader);
		});
	};
	$.fn.wpcf7ToggleSubmit = function () {
		return this.each(function () {
			var form = $(this);
			if (this.tagName.toLowerCase() != 'form') {
				form = $(this).find('form').first();
			}
			if (form.hasClass('wpcf7-acceptance-as-validation')) {
				return;
			}
			var submit = form.find('input:submit');
			if (!submit.length) return;
			var acceptances = form.find('input:checkbox.wpcf7-acceptance');
			if (!acceptances.length) return;
			submit.removeAttr('disabled');
			acceptances.each(function (i, n) {
				n = $(n);
				if (n.hasClass('wpcf7-invert') && n.is(':checked') || !n.hasClass('wpcf7-invert') && !n.is(':checked')) {
					submit.attr('disabled', 'disabled');
				}
			});
		});
	};
	$.fn.wpcf7ToggleCheckboxFreetext = function () {
		return this.each(function () {
			var $wrap = $(this).closest('.wpcf7-form-control');
			if ($(this).find(':checkbox, :radio').is(':checked')) {
				$(this).find(':input.wpcf7-free-text').prop('disabled', false);
			} else {
				$(this).find(':input.wpcf7-free-text').prop('disabled', true);
			}
			$wrap.find(':checkbox, :radio').change(function () {
				var $cb = $('.has-free-text', $wrap).find(':checkbox, :radio');
				var $freetext = $(':input.wpcf7-free-text', $wrap);
				if ($cb.is(':checked')) {
					$freetext.prop('disabled', false).focus();
				} else {
					$freetext.prop('disabled', true);
				}
			});
		});
	};
	$.fn.wpcf7CharacterCount = function () {
		return this.each(function () {
			var $count = $(this);
			var name = $count.attr('data-target-name');
			var down = $count.hasClass('down');
			var starting = parseInt($count.attr('data-starting-value'), 10);
			var maximum = parseInt($count.attr('data-maximum-value'), 10);
			var minimum = parseInt($count.attr('data-minimum-value'), 10);
			var updateCount = function ($target) {
				var length = $target.val().length;
				var count = down ? starting - length : length;
				$count.attr('data-current-value', count);
				$count.text(count);
				if (maximum && maximum < length) {
					$count.addClass('too-long');
				} else {
					$count.removeClass('too-long');
				}
				if (minimum && length < minimum) {
					$count.addClass('too-short');
				} else {
					$count.removeClass('too-short');
				}
			};
			$count.closest('form').find(':input[name="' + name + '"]').each(function () {
				updateCount($(this));
				$(this).keyup(function () {
					updateCount($(this));
				});
			});
		});
	};
	$.fn.wpcf7NormalizeUrl = function () {
		return this.each(function () {
			var val = $.trim($(this).val());
			if (val && !val.match(/^[a-z][a-z0-9.+-]*:/i)) {
				val = val.replace(/^\/+/, '');
				val = 'http://' + val;
			}
			$(this).val(val);
		});
	};
	$.fn.wpcf7NotValidTip = function (message) {
		return this.each(function () {
			var $into = $(this);
			$into.find('span.wpcf7-not-valid-tip').remove();
			$into.append('<span role="alert" class="wpcf7-not-valid-tip">' + message + '</span>');
			if ($into.is('.use-floating-validation-tip *')) {
				$('.wpcf7-not-valid-tip', $into).mouseover(function () {
					$(this).wpcf7FadeOut();
				});
				$(':input', $into).focus(function () {
					$('.wpcf7-not-valid-tip', $into).not(':hidden').wpcf7FadeOut();
				});
			}
		});
	};
	$.fn.wpcf7FadeOut = function () {
		return this.each(function () {
			$(this).animate({
				opacity: 0
			}, 'fast', function () {
				$(this).css({
					'z-index': -100
				});
			});
		});
	};
	$.fn.wpcf7OnloadRefill = function () {
		return this.each(function () {
			var url = $(this).attr('action');
			if (0 < url.indexOf('#')) {
				url = url.substr(0, url.indexOf('#'));
			}
			var id = $(this).find('input[name="_wpcf7"]').val();
			var unitTag = $(this).find('input[name="_wpcf7_unit_tag"]').val();
			$.getJSON(url, {
				_wpcf7_is_ajax_call: 1,
				_wpcf7: id,
				_wpcf7_request_ver: $.now()
			}, function (data) {
				if (data && data.captcha) {
					$('#' + unitTag).wpcf7RefillCaptcha(data.captcha);
				}
				if (data && data.quiz) {
					$('#' + unitTag).wpcf7RefillQuiz(data.quiz);
				}
			});
		});
	};
	$.fn.wpcf7RefillCaptcha = function (captcha) {
		return this.each(function () {
			var form = $(this);
			$.each(captcha, function (i, n) {
				form.find(':input[name="' + i + '"]').clearFields();
				form.find('img.wpcf7-captcha-' + i).attr('src', n);
				var match = /([0-9]+)\.(png|gif|jpeg)$/.exec(n);
				form.find('input:hidden[name="_wpcf7_captcha_challenge_' + i + '"]').attr('value', match[1]);
			});
		});
	};
	$.fn.wpcf7RefillQuiz = function (quiz) {
		return this.each(function () {
			var form = $(this);
			$.each(quiz, function (i, n) {
				form.find(':input[name="' + i + '"]').clearFields();
				form.find(':input[name="' + i + '"]').siblings('span.wpcf7-quiz-label').text(n[0]);
				form.find('input:hidden[name="_wpcf7_quiz_answer_' + i + '"]').attr('value', n[1]);
			});
		});
	};
	$.fn.wpcf7ClearResponseOutput = function () {
		return this.each(function () {
			$(this).find('div.wpcf7-response-output').hide().empty().removeClass('wpcf7-mail-sent-ok wpcf7-mail-sent-ng wpcf7-validation-errors wpcf7-spam-blocked').removeAttr('role');
			$(this).find('span.wpcf7-not-valid-tip').remove();
			$(this).find('img.ajax-loader').css({
				visibility: 'hidden'
			});
		});
	};
	$.fn.wpcf7Recaptcha = function () {
		return this.each(function () {
			var events = 'wpcf7:spam wpcf7:mailsent wpcf7:mailfailed';
			$(this).closest('div.wpcf7').on(events, function (e) {
				if (recaptchaWidgets && grecaptcha) {
					$.each(recaptchaWidgets, function (index, value) {
						grecaptcha.reset(value);
					});
				}
			});
		});
	};
	$.wpcf7UpdateScreenReaderResponse = function ($form, data) {
		$('.wpcf7 .screen-reader-response').html('').attr('role', '');
		if (data.message) {
			var $response = $form.siblings('.screen-reader-response').first();
			$response.append(data.message);
			if (data.invalids) {
				var $invalids = $('<ul></ul>');
				$.each(data.invalids, function (i, n) {
					if (n.idref) {
						var $li = $('<li></li>').append($('<a></a>').attr('href', '#' + n.idref).append(n.message));
					} else {
						var $li = $('<li></li>').append(n.message);
					}
					$invalids.append($li);
				});
				$response.append($invalids);
			}
			$response.attr('role', 'alert').focus();
		}
	};
	$.wpcf7SupportHtml5 = function () {
		var features = {};
		var input = document.createElement('input');
		features.placeholder = 'placeholder' in input;
		var inputTypes = ['email', 'url', 'tel', 'number', 'range', 'date'];
		$.each(inputTypes, function (index, value) {
			input.setAttribute('type', value);
			features[value] = input.type !== 'text';
		});
		return features;
	};
	$(function () {
		_wpcf7.supportHtml5 = $.wpcf7SupportHtml5();
		$('div.wpcf7 > form').wpcf7InitForm();
	});
})(jQuery);;
! function (a, b) {
	"use strict";

	function c() {
		if (!e) {
			e = !0;
			var a, c, d, f, g = -1 !== navigator.appVersion.indexOf("MSIE 10"),
				h = !!navigator.userAgent.match(/Trident.*rv:11\./),
				i = b.querySelectorAll("iframe.wp-embedded-content");
			for (c = 0; c < i.length; c++) {
				if (d = i[c], !d.getAttribute("data-secret")) f = Math.random().toString(36).substr(2, 10), d.src += "#?secret=" + f, d.setAttribute("data-secret", f);
				if (g || h) a = d.cloneNode(!0), a.removeAttribute("security"), d.parentNode.replaceChild(a, d)
			}
		}
	}
	var d = !1,
		e = !1;
	if (b.querySelector)
		if (a.addEventListener) d = !0;
	if (a.wp = a.wp || {}, !a.wp.receiveEmbedMessage)
		if (a.wp.receiveEmbedMessage = function (c) {
				var d = c.data;
				if (d.secret || d.message || d.value)
					if (!/[^a-zA-Z0-9]/.test(d.secret)) {
						var e, f, g, h, i, j = b.querySelectorAll('iframe[data-secret="' + d.secret + '"]'),
							k = b.querySelectorAll('blockquote[data-secret="' + d.secret + '"]');
						for (e = 0; e < k.length; e++) k[e].style.display = "none";
						for (e = 0; e < j.length; e++)
							if (f = j[e], c.source === f.contentWindow) {
								if (f.removeAttribute("style"), "height" === d.message) {
									if (g = parseInt(d.value, 10), g > 1e3) g = 1e3;
									else if (~~g < 200) g = 200;
									f.height = g
								}
								if ("link" === d.message)
									if (h = b.createElement("a"), i = b.createElement("a"), h.href = f.getAttribute("src"), i.href = d.value, i.host === h.host)
										if (b.activeElement === f) a.top.location.href = d.value
							} else;
					}
			}, d) a.addEventListener("message", a.wp.receiveEmbedMessage, !1), b.addEventListener("DOMContentLoaded", c, !1), a.addEventListener("load", c, !1)
}(window, document);;

function vc_js() {
	vc_toggleBehaviour(), vc_tabsBehaviour(), vc_accordionBehaviour(), vc_teaserGrid(), vc_carouselBehaviour(), vc_slidersBehaviour(), vc_prettyPhoto(), vc_googleplus(), vc_pinterest(), vc_progress_bar(), vc_plugin_flexslider(), vc_google_fonts(), vc_gridBehaviour(), vc_rowBehaviour(), vc_prepareHoverBox(), vc_googleMapsPointer(), vc_ttaActivation(), jQuery(document).trigger("vc_js"), window.setTimeout(vc_waypoints, 500)
}

function getSizeName() {
	var screen_w = jQuery(window).width();
	return 1170 < screen_w ? "desktop_wide" : 960 < screen_w && 1169 > screen_w ? "desktop" : 768 < screen_w && 959 > screen_w ? "tablet" : 300 < screen_w && 767 > screen_w ? "mobile" : 300 > screen_w ? "mobile_portrait" : ""
}

function loadScript(url, $obj, callback) {
	var script = document.createElement("script");
	script.type = "text/javascript", script.readyState && (script.onreadystatechange = function () {
		"loaded" !== script.readyState && "complete" !== script.readyState || (script.onreadystatechange = null, callback())
	}), script.src = url, $obj.get(0).appendChild(script)
}

function vc_ttaActivation() {
	jQuery("[data-vc-accordion]").on("show.vc.accordion", function (e) {
		var $ = window.jQuery,
			ui = {};
		ui.newPanel = $(this).data("vc.accordion").getTarget(), window.wpb_prepare_tab_content(e, ui)
	})
}

function vc_accordionActivate(event, ui) {
	if (ui.newPanel.length && ui.newHeader.length) {
		var $pie_charts = ui.newPanel.find(".vc_pie_chart:not(.vc_ready)"),
			$round_charts = ui.newPanel.find(".vc_round-chart"),
			$line_charts = ui.newPanel.find(".vc_line-chart"),
			$carousel = ui.newPanel.find('[data-ride="vc_carousel"]');
		void 0 !== jQuery.fn.isotope && ui.newPanel.find(".isotope, .wpb_image_grid_ul").isotope("layout"), ui.newPanel.find(".vc_masonry_media_grid, .vc_masonry_grid").length && ui.newPanel.find(".vc_masonry_media_grid, .vc_masonry_grid").each(function () {
			var grid = jQuery(this).data("vcGrid");
			grid && grid.gridBuilder && grid.gridBuilder.setMasonry && grid.gridBuilder.setMasonry()
		}), vc_carouselBehaviour(ui.newPanel), vc_plugin_flexslider(ui.newPanel), $pie_charts.length && jQuery.fn.vcChat && $pie_charts.vcChat(), $round_charts.length && jQuery.fn.vcRoundChart && $round_charts.vcRoundChart({
			reload: !1
		}), $line_charts.length && jQuery.fn.vcLineChart && $line_charts.vcLineChart({
			reload: !1
		}), $carousel.length && jQuery.fn.carousel && $carousel.carousel("resizeAction"), ui.newPanel.parents(".isotope").length && ui.newPanel.parents(".isotope").each(function () {
			jQuery(this).isotope("layout")
		})
	}
}

function initVideoBackgrounds() {
	return window.console && window.console.warn && window.console.warn("this function is deprecated use vc_initVideoBackgrounds"), vc_initVideoBackgrounds()
}

function vc_initVideoBackgrounds() {
	jQuery("[data-vc-video-bg]").each(function () {
		var youtubeUrl, youtubeId, $element = jQuery(this);
		$element.data("vcVideoBg") ? (youtubeUrl = $element.data("vcVideoBg"), youtubeId = vcExtractYoutubeId(youtubeUrl), youtubeId && ($element.find(".vc_video-bg").remove(), insertYoutubeVideoAsBackground($element, youtubeId)), jQuery(window).on("grid:items:added", function (event, $grid) {
			$element.has($grid).length && vcResizeVideoBackground($element)
		})) : $element.find(".vc_video-bg").remove()
	})
}

function insertYoutubeVideoAsBackground($element, youtubeId, counter) {
	if ("undefined" == typeof YT || void 0 === YT.Player) return 100 < (counter = void 0 === counter ? 0 : counter) ? void console.warn("Too many attempts to load YouTube api") : void setTimeout(function () {
		insertYoutubeVideoAsBackground($element, youtubeId, counter++)
	}, 100);
	var $container = $element.prepend('<div class="vc_video-bg vc_hidden-xs"><div class="inner"></div></div>').find(".inner");
	new YT.Player($container[0], {
		width: "100%",
		height: "100%",
		videoId: youtubeId,
		playerVars: {
			playlist: youtubeId,
			iv_load_policy: 3,
			enablejsapi: 1,
			disablekb: 1,
			autoplay: 1,
			controls: 0,
			showinfo: 0,
			rel: 0,
			loop: 1,
			wmode: "transparent"
		},
		events: {
			onReady: function (event) {
				event.target.mute().setLoop(!0)
			}
		}
	}), vcResizeVideoBackground($element), jQuery(window).bind("resize", function () {
		vcResizeVideoBackground($element)
	})
}

function vcResizeVideoBackground($element) {
	var iframeW, iframeH, marginLeft, marginTop, containerW = $element.innerWidth(),
		containerH = $element.innerHeight();
	containerW / containerH < 16 / 9 ? (iframeW = containerH * (16 / 9), iframeH = containerH, marginLeft = -Math.round((iframeW - containerW) / 2) + "px", marginTop = -Math.round((iframeH - containerH) / 2) + "px", iframeW += "px", iframeH += "px") : (iframeW = containerW, iframeH = containerW * (9 / 16), marginTop = -Math.round((iframeH - containerH) / 2) + "px", marginLeft = -Math.round((iframeW - containerW) / 2) + "px", iframeW += "px", iframeH += "px"), $element.find(".vc_video-bg iframe").css({
		maxWidth: "1000%",
		marginLeft: marginLeft,
		marginTop: marginTop,
		width: iframeW,
		height: iframeH
	})
}

function vcExtractYoutubeId(url) {
	if (void 0 === url) return !1;
	var id = url.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);
	return null !== id && id[1]
}

function vc_googleMapsPointer() {
	var $ = window.jQuery,
		$wpbGmapsWidget = $(".wpb_gmaps_widget");
	$wpbGmapsWidget.click(function () {
		$("iframe", this).css("pointer-events", "auto")
	}), $wpbGmapsWidget.mouseleave(function () {
		$("iframe", this).css("pointer-events", "none")
	}), $(".wpb_gmaps_widget iframe").css("pointer-events", "none")
}

function vc_setHoverBoxPerspective(hoverBox) {
	hoverBox.each(function () {
		var $this = jQuery(this),
			width = $this.width(),
			perspective = 4 * width + "px";
		$this.css("perspective", perspective)
	})
}

function vc_setHoverBoxHeight(hoverBox) {
	hoverBox.each(function () {
		var $this = jQuery(this),
			hoverBoxInner = $this.find(".vc-hoverbox-inner");
		hoverBoxInner.css("min-height", 0);
		var frontHeight = $this.find(".vc-hoverbox-front-inner").outerHeight(),
			backHeight = $this.find(".vc-hoverbox-back-inner").outerHeight(),
			hoverBoxHeight = frontHeight > backHeight ? frontHeight : backHeight;
		hoverBoxHeight < 250 && (hoverBoxHeight = 250), hoverBoxInner.css("min-height", hoverBoxHeight + "px")
	})
}

function vc_prepareHoverBox() {
	var hoverBox = jQuery(".vc-hoverbox");
	vc_setHoverBoxHeight(hoverBox), vc_setHoverBoxPerspective(hoverBox)
}
document.documentElement.className += " js_active ", document.documentElement.className += "ontouchstart" in document.documentElement ? " vc_mobile " : " vc_desktop ",
	function () {
		for (var prefix = ["-webkit-", "-moz-", "-ms-", "-o-", ""], i = 0; i < prefix.length; i++) prefix[i] + "transform" in document.documentElement.style && (document.documentElement.className += " vc_transform ")
	}(), "function" != typeof window.vc_plugin_flexslider && (window.vc_plugin_flexslider = function ($parent) {
		($parent ? $parent.find(".wpb_flexslider") : jQuery(".wpb_flexslider")).each(function () {
			var this_element = jQuery(this),
				sliderTimeout = 1e3 * parseInt(this_element.attr("data-interval")),
				sliderFx = this_element.attr("data-flex_fx"),
				slideshow = !0;
			0 === sliderTimeout && (slideshow = !1), this_element.is(":visible") && this_element.flexslider({
				animation: sliderFx,
				slideshow: slideshow,
				slideshowSpeed: sliderTimeout,
				sliderSpeed: 800,
				smoothHeight: !0
			})
		})
	}), "function" != typeof window.vc_googleplus && (window.vc_googleplus = function () {
		0 < jQuery(".wpb_googleplus").length && function () {
			var po = document.createElement("script");
			po.type = "text/javascript", po.async = !0, po.src = "//apis.google.com/js/plusone.js";
			var s = document.getElementsByTagName("script")[0];
			s.parentNode.insertBefore(po, s)
		}()
	}), "function" != typeof window.vc_pinterest && (window.vc_pinterest = function () {
		0 < jQuery(".wpb_pinterest").length && function () {
			var po = document.createElement("script");
			po.type = "text/javascript", po.async = !0, po.src = "//assets.pinterest.com/js/pinit.js";
			var s = document.getElementsByTagName("script")[0];
			s.parentNode.insertBefore(po, s)
		}()
	}), "function" != typeof window.vc_progress_bar && (window.vc_progress_bar = function () {
		void 0 !== jQuery.fn.waypoint && jQuery(".vc_progress_bar").waypoint(function () {
			jQuery(this).find(".vc_single_bar").each(function (index) {
				var $this = jQuery(this),
					bar = $this.find(".vc_bar"),
					val = bar.data("percentage-value");
				setTimeout(function () {
					bar.css({
						width: val + "%"
					})
				}, 200 * index)
			})
		}, {
			offset: "85%"
		})
	}), "function" != typeof window.vc_waypoints && (window.vc_waypoints = function () {
		void 0 !== jQuery.fn.waypoint && jQuery(".wpb_animate_when_almost_visible:not(.wpb_start_animation)").waypoint(function () {
			jQuery(this).addClass("wpb_start_animation animated")
		}, {
			offset: "85%"
		})
	}), "function" != typeof window.vc_toggleBehaviour && (window.vc_toggleBehaviour = function ($el) {
		function event(e) {
			e && e.preventDefault && e.preventDefault();
			var title = jQuery(this),
				element = title.closest(".vc_toggle"),
				content = element.find(".vc_toggle_content");
			element.hasClass("vc_toggle_active") ? content.slideUp({
				duration: 300,
				complete: function () {
					element.removeClass("vc_toggle_active")
				}
			}) : content.slideDown({
				duration: 300,
				complete: function () {
					element.addClass("vc_toggle_active")
				}
			})
		}
		$el ? $el.hasClass("vc_toggle_title") ? $el.unbind("click").click(event) : $el.find(".vc_toggle_title").unbind("click").click(event) : jQuery(".vc_toggle_title").unbind("click").on("click", event)
	}), "function" != typeof window.vc_tabsBehaviour && (window.vc_tabsBehaviour = function ($tab) {
		if (jQuery.ui) {
			var $call = $tab || jQuery(".wpb_tabs, .wpb_tour"),
				ver = jQuery.ui && jQuery.ui.version ? jQuery.ui.version.split(".") : "1.10",
				old_version = 1 === parseInt(ver[0]) && 9 > parseInt(ver[1]);
			$call.each(function (index) {
				var $tabs, interval = jQuery(this).attr("data-interval"),
					tabs_array = [];
				if ($tabs = jQuery(this).find(".wpb_tour_tabs_wrapper").tabs({
						show: function (event, ui) {
							wpb_prepare_tab_content(event, ui)
						},
						beforeActivate: function (event, ui) {
							1 !== ui.newPanel.index() && ui.newPanel.find(".vc_pie_chart:not(.vc_ready)")
						},
						activate: function (event, ui) {
							wpb_prepare_tab_content(event, ui)
						}
					}), interval && 0 < interval) try {
					$tabs.tabs("rotate", 1e3 * interval)
				} catch (e) {
					window.console && window.console.log && console.log(e)
				}
				jQuery(this).find(".wpb_tab").each(function () {
					tabs_array.push(this.id)
				}), jQuery(this).find(".wpb_tabs_nav li").click(function (e) {
					return e.preventDefault(), old_version ? $tabs.tabs("select", jQuery("a", this).attr("href")) : $tabs.tabs("option", "active", jQuery(this).index()), !1
				}), jQuery(this).find(".wpb_prev_slide a, .wpb_next_slide a").click(function (e) {
					if (e.preventDefault(), old_version) {
						var index = $tabs.tabs("option", "selected");
						jQuery(this).parent().hasClass("wpb_next_slide") ? index++ : index--, 0 > index ? index = $tabs.tabs("length") - 1 : index >= $tabs.tabs("length") && (index = 0), $tabs.tabs("select", index)
					} else {
						var index = $tabs.tabs("option", "active"),
							length = $tabs.find(".wpb_tab").length;
						index = jQuery(this).parent().hasClass("wpb_next_slide") ? index + 1 >= length ? 0 : index + 1 : 0 > index - 1 ? length - 1 : index - 1, $tabs.tabs("option", "active", index)
					}
				})
			})
		}
	}), "function" != typeof window.vc_accordionBehaviour && (window.vc_accordionBehaviour = function () {
		jQuery(".wpb_accordion").each(function (index) {
			var $tabs, $this = jQuery(this),
				active_tab = ($this.attr("data-interval"), !isNaN(jQuery(this).data("active-tab")) && 0 < parseInt($this.data("active-tab")) && parseInt($this.data("active-tab")) - 1),
				collapsible = !1 === active_tab || "yes" === $this.data("collapsible");
			$tabs = $this.find(".wpb_accordion_wrapper").accordion({
				header: "> div > h3",
				autoHeight: !1,
				heightStyle: "content",
				active: active_tab,
				collapsible: collapsible,
				navigation: !0,
				activate: vc_accordionActivate,
				change: function (event, ui) {
					void 0 !== jQuery.fn.isotope && ui.newContent.find(".isotope").isotope("layout"), vc_carouselBehaviour(ui.newPanel)
				}
			}), !0 === $this.data("vcDisableKeydown") && ($tabs.data("uiAccordion")._keydown = function () {})
		})
	}), "function" != typeof window.vc_teaserGrid && (window.vc_teaserGrid = function () {
		var layout_modes = {
			fitrows: "fitRows",
			masonry: "masonry"
		};
		jQuery(".wpb_grid .teaser_grid_container:not(.wpb_carousel), .wpb_filtered_grid .teaser_grid_container:not(.wpb_carousel)").each(function () {
			var $container = jQuery(this),
				$thumbs = $container.find(".wpb_thumbnails"),
				layout_mode = $thumbs.attr("data-layout-mode");
			$thumbs.isotope({
				itemSelector: ".isotope-item",
				layoutMode: void 0 === layout_modes[layout_mode] ? "fitRows" : layout_modes[layout_mode]
			}), $container.find(".categories_filter a").data("isotope", $thumbs).click(function (e) {
				e.preventDefault();
				var $thumbs = jQuery(this).data("isotope");
				jQuery(this).parent().parent().find(".active").removeClass("active"), jQuery(this).parent().addClass("active"), $thumbs.isotope({
					filter: jQuery(this).attr("data-filter")
				})
			}), jQuery(window).bind("load resize", function () {
				$thumbs.isotope("layout")
			})
		})
	}), "function" != typeof window.vc_carouselBehaviour && (window.vc_carouselBehaviour = function ($parent) {
		($parent ? $parent.find(".wpb_carousel") : jQuery(".wpb_carousel")).each(function () {
			var $this = jQuery(this);
			if (!0 !== $this.data("carousel_enabled") && $this.is(":visible")) {
				$this.data("carousel_enabled", !0), getColumnsCount(jQuery(this)), jQuery(this).hasClass("columns_count_1");
				var carousele_li = jQuery(this).find(".wpb_thumbnails-fluid li");
				carousele_li.css({
					"margin-right": carousele_li.css("margin-left"),
					"margin-left": 0
				});
				var fluid_ul = jQuery(this).find("ul.wpb_thumbnails-fluid");
				fluid_ul.width(fluid_ul.width() + 300), jQuery(window).resize(function () {
					var before_resize = screen_size;
					screen_size = getSizeName(), before_resize != screen_size && window.setTimeout("location.reload()", 20)
				})
			}
		})
	}), "function" != typeof window.vc_slidersBehaviour && (window.vc_slidersBehaviour = function () {
		jQuery(".wpb_gallery_slides").each(function (index) {
			var $imagesGrid, this_element = jQuery(this);
			if (this_element.hasClass("wpb_slider_nivo")) {
				var sliderTimeout = 1e3 * this_element.attr("data-interval");
				0 === sliderTimeout && (sliderTimeout = 9999999999), this_element.find(".nivoSlider").nivoSlider({
					effect: "boxRainGrow,boxRain,boxRainReverse,boxRainGrowReverse",
					slices: 15,
					boxCols: 8,
					boxRows: 4,
					animSpeed: 800,
					pauseTime: sliderTimeout,
					startSlide: 0,
					directionNav: !0,
					directionNavHide: !0,
					controlNav: !0,
					keyboardNav: !1,
					pauseOnHover: !0,
					manualAdvance: !1,
					prevText: "Prev",
					nextText: "Next"
				})
			} else this_element.hasClass("wpb_image_grid") && (jQuery.fn.imagesLoaded ? $imagesGrid = this_element.find(".wpb_image_grid_ul").imagesLoaded(function () {
				$imagesGrid.isotope({
					itemSelector: ".isotope-item",
					layoutMode: "fitRows"
				})
			}) : this_element.find(".wpb_image_grid_ul").isotope({
				itemSelector: ".isotope-item",
				layoutMode: "fitRows"
			}))
		})
	}), "function" != typeof window.vc_prettyPhoto && (window.vc_prettyPhoto = function () {
		try {
			jQuery && jQuery.fn && jQuery.fn.prettyPhoto && jQuery('a.prettyphoto, .gallery-icon a[href*=".jpg"]').prettyPhoto({
				animationSpeed: "normal",
				hook: "data-rel",
				padding: 15,
				opacity: .7,
				showTitle: !0,
				allowresize: !0,
				counter_separator_label: "/",
				hideflash: !1,
				deeplinking: !1,
				modal: !1,
				callback: function () {
					location.href.indexOf("#!prettyPhoto") > -1 && (location.hash = "")
				},
				social_tools: ""
			})
		} catch (err) {
			window.console && window.console.log && console.log(err)
		}
	}), "function" != typeof window.vc_google_fonts && (window.vc_google_fonts = function () {
		return !1
	}), window.vcParallaxSkroll = !1, "function" != typeof window.vc_rowBehaviour && (window.vc_rowBehaviour = function () {
		function fullWidthRow() {
			var $elements = $('[data-vc-full-width="true"]');
			$.each($elements, function (key, item) {
				var $el = $(this);
				$el.addClass("vc_hidden");
				var $el_full = $el.next(".vc_row-full-width");
				if ($el_full.length || ($el_full = $el.parent().next(".vc_row-full-width")), $el_full.length) {
					var el_margin_left = parseInt($el.css("margin-left"), 10),
						el_margin_right = parseInt($el.css("margin-right"), 10),
						offset = 0 - $el_full.offset().left - el_margin_left,
						width = $(window).width();
					if ($el.css({
							position: "relative",
							left: offset,
							"box-sizing": "border-box",
							width: $(window).width()
						}), !$el.data("vcStretchContent")) {
						var padding = -1 * offset;
						0 > padding && (padding = 0);
						var paddingRight = width - padding - $el_full.width() + el_margin_left + el_margin_right;
						0 > paddingRight && (paddingRight = 0), $el.css({
							"padding-left": padding + "px",
							"padding-right": paddingRight + "px"
						})
					}
					$el.attr("data-vc-full-width-init", "true"), $el.removeClass("vc_hidden"), $(document).trigger("vc-full-width-row-single", {
						el: $el,
						offset: offset,
						marginLeft: el_margin_left,
						marginRight: el_margin_right,
						elFull: $el_full,
						width: width
					})
				}
			}), $(document).trigger("vc-full-width-row", $elements)
		}

		function fullHeightRow() {
			var $element = $(".vc_row-o-full-height:first");
			if ($element.length) {
				var $window, windowHeight, offsetTop, fullHeight;
				$window = $(window), windowHeight = $window.height(), offsetTop = $element.offset().top, offsetTop < windowHeight && (fullHeight = 100 - offsetTop / (windowHeight / 100), $element.css("min-height", fullHeight + "vh"))
			}
			$(document).trigger("vc-full-height-row", $element)
		}
		var $ = window.jQuery;
		$(window).off("resize.vcRowBehaviour").on("resize.vcRowBehaviour", fullWidthRow).on("resize.vcRowBehaviour", fullHeightRow), fullWidthRow(), fullHeightRow(),
			function () {
				(window.navigator.userAgent.indexOf("MSIE ") > 0 || navigator.userAgent.match(/Trident.*rv\:11\./)) && $(".vc_row-o-full-height").each(function () {
					"flex" === $(this).css("display") && $(this).wrap('<div class="vc_ie-flexbox-fixer"></div>')
				})
			}(), vc_initVideoBackgrounds(),
			function () {
				var vcSkrollrOptions, callSkrollInit = !1;
				window.vcParallaxSkroll && window.vcParallaxSkroll.destroy(), $(".vc_parallax-inner").remove(), $("[data-5p-top-bottom]").removeAttr("data-5p-top-bottom data-30p-top-bottom"), $("[data-vc-parallax]").each(function () {
					var skrollrSpeed, skrollrSize, skrollrStart, skrollrEnd, $parallaxElement, parallaxImage, youtubeId;
					callSkrollInit = !0, "on" === $(this).data("vcParallaxOFade") && $(this).children().attr("data-5p-top-bottom", "opacity:0;").attr("data-30p-top-bottom", "opacity:1;"), skrollrSize = 100 * $(this).data("vcParallax"), $parallaxElement = $("<div />").addClass("vc_parallax-inner").appendTo($(this)), $parallaxElement.height(skrollrSize + "%"), parallaxImage = $(this).data("vcParallaxImage"), youtubeId = vcExtractYoutubeId(parallaxImage), youtubeId ? insertYoutubeVideoAsBackground($parallaxElement, youtubeId) : void 0 !== parallaxImage && $parallaxElement.css("background-image", "url(" + parallaxImage + ")"), skrollrSpeed = skrollrSize - 100, skrollrStart = -skrollrSpeed, skrollrEnd = 0, $parallaxElement.attr("data-bottom-top", "top: " + skrollrStart + "%;").attr("data-top-bottom", "top: " + skrollrEnd + "%;")
				}), !(!callSkrollInit || !window.skrollr) && (vcSkrollrOptions = {
					forceHeight: !1,
					smoothScrolling: !1,
					mobileCheck: function () {
						return !1
					}
				}, window.vcParallaxSkroll = skrollr.init(vcSkrollrOptions), window.vcParallaxSkroll)
			}()
	}), "function" != typeof window.vc_gridBehaviour && (window.vc_gridBehaviour = function () {
		jQuery.fn.vcGrid && jQuery("[data-vc-grid]").vcGrid()
	}), "function" != typeof window.getColumnsCount && (window.getColumnsCount = function (el) {
		for (var find = !1, i = 1; !1 === find;) {
			if (el.hasClass("columns_count_" + i)) return find = !0, i;
			i++
		}
	});
var screen_size = getSizeName();
"function" != typeof window.wpb_prepare_tab_content && (window.wpb_prepare_tab_content = function (event, ui) {
	var $ui_panel, $google_maps, panel = ui.panel || ui.newPanel,
		$pie_charts = panel.find(".vc_pie_chart:not(.vc_ready)"),
		$round_charts = panel.find(".vc_round-chart"),
		$line_charts = panel.find(".vc_line-chart"),
		$carousel = panel.find('[data-ride="vc_carousel"]');
	if (vc_carouselBehaviour(), vc_plugin_flexslider(panel), ui.newPanel.find(".vc_masonry_media_grid, .vc_masonry_grid").length && ui.newPanel.find(".vc_masonry_media_grid, .vc_masonry_grid").each(function () {
			var grid = jQuery(this).data("vcGrid");
			grid && grid.gridBuilder && grid.gridBuilder.setMasonry && grid.gridBuilder.setMasonry()
		}), panel.find(".vc_masonry_media_grid, .vc_masonry_grid").length && panel.find(".vc_masonry_media_grid, .vc_masonry_grid").each(function () {
			var grid = jQuery(this).data("vcGrid");
			grid && grid.gridBuilder && grid.gridBuilder.setMasonry && grid.gridBuilder.setMasonry()
		}), $pie_charts.length && jQuery.fn.vcChat && $pie_charts.vcChat(), $round_charts.length && jQuery.fn.vcRoundChart && $round_charts.vcRoundChart({
			reload: !1
		}), $line_charts.length && jQuery.fn.vcLineChart && $line_charts.vcLineChart({
			reload: !1
		}), $carousel.length && jQuery.fn.carousel && $carousel.carousel("resizeAction"), $ui_panel = panel.find(".isotope, .wpb_image_grid_ul"), $google_maps = panel.find(".wpb_gmaps_widget"), 0 < $ui_panel.length && $ui_panel.isotope("layout"), $google_maps.length && !$google_maps.is(".map_ready")) {
		var $frame = $google_maps.find("iframe");
		$frame.attr("src", $frame.attr("src")), $google_maps.addClass("map_ready")
	}
	panel.parents(".isotope").length && panel.parents(".isotope").each(function () {
		jQuery(this).isotope("layout")
	})
}), window.vc_googleMapsPointer, jQuery(document).ready(vc_prepareHoverBox), jQuery(window).resize(vc_prepareHoverBox), jQuery(document).ready(function ($) {
	window.vc_js()
});; // Generated by CoffeeScript 1.6.2
/*
jQuery Waypoints - v2.0.2
Copyright (c) 2011-2013 Caleb Troughton
Dual licensed under the MIT license and GPL license.
https://github.com/imakewebthings/jquery-waypoints/blob/master/licenses.txt
*/
(function () {
	var t = [].indexOf || function (t) {
			for (var e = 0, n = this.length; e < n; e++) {
				if (e in this && this[e] === t) return e
			}
			return -1
		},
		e = [].slice;
	(function (t, e) {
		if (typeof define === "function" && define.amd) {
			return define("waypoints", ["jquery"], function (n) {
				return e(n, t)
			})
		} else {
			return e(t.jQuery, t)
		}
	})(this, function (n, r) {
		var i, o, l, s, f, u, a, c, h, d, p, y, v, w, g, m;
		i = n(r);
		c = t.call(r, "ontouchstart") >= 0;
		s = {
			horizontal: {},
			vertical: {}
		};
		f = 1;
		a = {};
		u = "waypoints-context-id";
		p = "resize.waypoints";
		y = "scroll.waypoints";
		v = 1;
		w = "waypoints-waypoint-ids";
		g = "waypoint";
		m = "waypoints";
		o = function () {
			function t(t) {
				var e = this;
				this.$element = t;
				this.element = t[0];
				this.didResize = false;
				this.didScroll = false;
				this.id = "context" + f++;
				this.oldScroll = {
					x: t.scrollLeft(),
					y: t.scrollTop()
				};
				this.waypoints = {
					horizontal: {},
					vertical: {}
				};
				t.data(u, this.id);
				a[this.id] = this;
				t.bind(y, function () {
					var t;
					if (!(e.didScroll || c)) {
						e.didScroll = true;
						t = function () {
							e.doScroll();
							return e.didScroll = false
						};
						return r.setTimeout(t, n[m].settings.scrollThrottle)
					}
				});
				t.bind(p, function () {
					var t;
					if (!e.didResize) {
						e.didResize = true;
						t = function () {
							n[m]("refresh");
							return e.didResize = false
						};
						return r.setTimeout(t, n[m].settings.resizeThrottle)
					}
				})
			}
			t.prototype.doScroll = function () {
				var t, e = this;
				t = {
					horizontal: {
						newScroll: this.$element.scrollLeft(),
						oldScroll: this.oldScroll.x,
						forward: "right",
						backward: "left"
					},
					vertical: {
						newScroll: this.$element.scrollTop(),
						oldScroll: this.oldScroll.y,
						forward: "down",
						backward: "up"
					}
				};
				if (c && (!t.vertical.oldScroll || !t.vertical.newScroll)) {
					n[m]("refresh")
				}
				n.each(t, function (t, r) {
					var i, o, l;
					l = [];
					o = r.newScroll > r.oldScroll;
					i = o ? r.forward : r.backward;
					n.each(e.waypoints[t], function (t, e) {
						var n, i;
						if (r.oldScroll < (n = e.offset) && n <= r.newScroll) {
							return l.push(e)
						} else if (r.newScroll < (i = e.offset) && i <= r.oldScroll) {
							return l.push(e)
						}
					});
					l.sort(function (t, e) {
						return t.offset - e.offset
					});
					if (!o) {
						l.reverse()
					}
					return n.each(l, function (t, e) {
						if (e.options.continuous || t === l.length - 1) {
							return e.trigger([i])
						}
					})
				});
				return this.oldScroll = {
					x: t.horizontal.newScroll,
					y: t.vertical.newScroll
				}
			};
			t.prototype.refresh = function () {
				var t, e, r, i = this;
				r = n.isWindow(this.element);
				e = this.$element.offset();
				this.doScroll();
				t = {
					horizontal: {
						contextOffset: r ? 0 : e.left,
						contextScroll: r ? 0 : this.oldScroll.x,
						contextDimension: this.$element.width(),
						oldScroll: this.oldScroll.x,
						forward: "right",
						backward: "left",
						offsetProp: "left"
					},
					vertical: {
						contextOffset: r ? 0 : e.top,
						contextScroll: r ? 0 : this.oldScroll.y,
						contextDimension: r ? n[m]("viewportHeight") : this.$element.height(),
						oldScroll: this.oldScroll.y,
						forward: "down",
						backward: "up",
						offsetProp: "top"
					}
				};
				return n.each(t, function (t, e) {
					return n.each(i.waypoints[t], function (t, r) {
						var i, o, l, s, f;
						i = r.options.offset;
						l = r.offset;
						o = n.isWindow(r.element) ? 0 : r.$element.offset()[e.offsetProp];
						if (n.isFunction(i)) {
							i = i.apply(r.element)
						} else if (typeof i === "string") {
							i = parseFloat(i);
							if (r.options.offset.indexOf("%") > -1) {
								i = Math.ceil(e.contextDimension * i / 100)
							}
						}
						r.offset = o - e.contextOffset + e.contextScroll - i;
						if (r.options.onlyOnScroll && l != null || !r.enabled) {
							return
						}
						if (l !== null && l < (s = e.oldScroll) && s <= r.offset) {
							return r.trigger([e.backward])
						} else if (l !== null && l > (f = e.oldScroll) && f >= r.offset) {
							return r.trigger([e.forward])
						} else if (l === null && e.oldScroll >= r.offset) {
							return r.trigger([e.forward])
						}
					})
				})
			};
			t.prototype.checkEmpty = function () {
				if (n.isEmptyObject(this.waypoints.horizontal) && n.isEmptyObject(this.waypoints.vertical)) {
					this.$element.unbind([p, y].join(" "));
					return delete a[this.id]
				}
			};
			return t
		}();
		l = function () {
			function t(t, e, r) {
				var i, o;
				r = n.extend({}, n.fn[g].defaults, r);
				if (r.offset === "bottom-in-view") {
					r.offset = function () {
						var t;
						t = n[m]("viewportHeight");
						if (!n.isWindow(e.element)) {
							t = e.$element.height()
						}
						return t - n(this).outerHeight()
					}
				}
				this.$element = t;
				this.element = t[0];
				this.axis = r.horizontal ? "horizontal" : "vertical";
				this.callback = r.handler;
				this.context = e;
				this.enabled = r.enabled;
				this.id = "waypoints" + v++;
				this.offset = null;
				this.options = r;
				e.waypoints[this.axis][this.id] = this;
				s[this.axis][this.id] = this;
				i = (o = t.data(w)) != null ? o : [];
				i.push(this.id);
				t.data(w, i)
			}
			t.prototype.trigger = function (t) {
				if (!this.enabled) {
					return
				}
				if (this.callback != null) {
					this.callback.apply(this.element, t)
				}
				if (this.options.triggerOnce) {
					return this.destroy()
				}
			};
			t.prototype.disable = function () {
				return this.enabled = false
			};
			t.prototype.enable = function () {
				this.context.refresh();
				return this.enabled = true
			};
			t.prototype.destroy = function () {
				delete s[this.axis][this.id];
				delete this.context.waypoints[this.axis][this.id];
				return this.context.checkEmpty()
			};
			t.getWaypointsByElement = function (t) {
				var e, r;
				r = n(t).data(w);
				if (!r) {
					return []
				}
				e = n.extend({}, s.horizontal, s.vertical);
				return n.map(r, function (t) {
					return e[t]
				})
			};
			return t
		}();
		d = {
			init: function (t, e) {
				var r;
				if (e == null) {
					e = {}
				}
				if ((r = e.handler) == null) {
					e.handler = t
				}
				this.each(function () {
					var t, r, i, s;
					t = n(this);
					i = (s = e.context) != null ? s : n.fn[g].defaults.context;
					if (!n.isWindow(i)) {
						i = t.closest(i)
					}
					i = n(i);
					r = a[i.data(u)];
					if (!r) {
						r = new o(i)
					}
					return new l(t, r, e)
				});
				n[m]("refresh");
				return this
			},
			disable: function () {
				return d._invoke(this, "disable")
			},
			enable: function () {
				return d._invoke(this, "enable")
			},
			destroy: function () {
				return d._invoke(this, "destroy")
			},
			prev: function (t, e) {
				return d._traverse.call(this, t, e, function (t, e, n) {
					if (e > 0) {
						return t.push(n[e - 1])
					}
				})
			},
			next: function (t, e) {
				return d._traverse.call(this, t, e, function (t, e, n) {
					if (e < n.length - 1) {
						return t.push(n[e + 1])
					}
				})
			},
			_traverse: function (t, e, i) {
				var o, l;
				if (t == null) {
					t = "vertical"
				}
				if (e == null) {
					e = r
				}
				l = h.aggregate(e);
				o = [];
				this.each(function () {
					var e;
					e = n.inArray(this, l[t]);
					return i(o, e, l[t])
				});
				return this.pushStack(o)
			},
			_invoke: function (t, e) {
				t.each(function () {
					var t;
					t = l.getWaypointsByElement(this);
					return n.each(t, function (t, n) {
						n[e]();
						return true
					})
				});
				return this
			}
		};
		n.fn[g] = function () {
			var t, r;
			r = arguments[0], t = 2 <= arguments.length ? e.call(arguments, 1) : [];
			if (d[r]) {
				return d[r].apply(this, t)
			} else if (n.isFunction(r)) {
				return d.init.apply(this, arguments)
			} else if (n.isPlainObject(r)) {
				return d.init.apply(this, [null, r])
			} else if (!r) {
				return n.error("jQuery Waypoints needs a callback function or handler option.")
			} else {
				return n.error("The " + r + " method does not exist in jQuery Waypoints.")
			}
		};
		n.fn[g].defaults = {
			context: r,
			continuous: true,
			enabled: true,
			horizontal: false,
			offset: 0,
			triggerOnce: false
		};
		h = {
			refresh: function () {
				return n.each(a, function (t, e) {
					return e.refresh()
				})
			},
			viewportHeight: function () {
				var t;
				return (t = r.innerHeight) != null ? t : i.height()
			},
			aggregate: function (t) {
				var e, r, i;
				e = s;
				if (t) {
					e = (i = a[n(t).data(u)]) != null ? i.waypoints : void 0
				}
				if (!e) {
					return []
				}
				r = {
					horizontal: [],
					vertical: []
				};
				n.each(r, function (t, i) {
					n.each(e[t], function (t, e) {
						return i.push(e)
					});
					i.sort(function (t, e) {
						return t.offset - e.offset
					});
					r[t] = n.map(i, function (t) {
						return t.element
					});
					return r[t] = n.unique(r[t])
				});
				return r
			},
			above: function (t) {
				if (t == null) {
					t = r
				}
				return h._filter(t, "vertical", function (t, e) {
					return e.offset <= t.oldScroll.y
				})
			},
			below: function (t) {
				if (t == null) {
					t = r
				}
				return h._filter(t, "vertical", function (t, e) {
					return e.offset > t.oldScroll.y
				})
			},
			left: function (t) {
				if (t == null) {
					t = r
				}
				return h._filter(t, "horizontal", function (t, e) {
					return e.offset <= t.oldScroll.x
				})
			},
			right: function (t) {
				if (t == null) {
					t = r
				}
				return h._filter(t, "horizontal", function (t, e) {
					return e.offset > t.oldScroll.x
				})
			},
			enable: function () {
				return h._invoke("enable")
			},
			disable: function () {
				return h._invoke("disable")
			},
			destroy: function () {
				return h._invoke("destroy")
			},
			extendFn: function (t, e) {
				return d[t] = e
			},
			_invoke: function (t) {
				var e;
				e = n.extend({}, s.vertical, s.horizontal);
				return n.each(e, function (e, n) {
					n[t]();
					return true
				})
			},
			_filter: function (t, e, r) {
				var i, o;
				i = a[n(t).data(u)];
				if (!i) {
					return []
				}
				o = [];
				n.each(i.waypoints[e], function (t, e) {
					if (r(i, e)) {
						return o.push(e)
					}
				});
				o.sort(function (t, e) {
					return t.offset - e.offset
				});
				return n.map(o, function (t) {
					return t.element
				})
			}
		};
		n[m] = function () {
			var t, n;
			n = arguments[0], t = 2 <= arguments.length ? e.call(arguments, 1) : [];
			if (h[n]) {
				return h[n].apply(null, t)
			} else {
				return h.aggregate.call(null, n)
			}
		};
		n[m].settings = {
			resizeThrottle: 100,
			scrollThrottle: 30
		};
		return i.load(function () {
			return n[m]("refresh")
		})
	})
}).call(this);;
(function ($) {
	$(function () {
		$('.quickfinder-item').each(function () {
			var $item = $(this);
			var $quickfinder = $item.closest('.quickfinder');
			var initHover = {
				icon_color1: $('.gem-icon-half-1', $item).css('color'),
				icon_color2: $('.gem-icon-half-2', $item).css('color'),
				icon_background: $('.gem-icon-inner', $item).css('background-color'),
				icon_border: $('.gem-icon', $item).css('border-left-color'),
				box_color: $('.quickfinder-item-box', $item).css('background-color'),
				border_color: $('.quickfinder-item-box', $item).css('border-left-color'),
				title_color: $('.quickfinder-item-title', $item).css('color'),
				description_color: $('.quickfinder-item-text', $item).css('color'),
				button_text_color: $('.quickfinder-button .gem-button', $item).css('color'),
				button_background_color: $('.quickfinder-button .gem-button', $item).css('background-color'),
				button_border_color: $('.quickfinder-button .gem-button', $item).css('border-left-color')
			};
			if ($('.gem-icon', $item).hasClass('gem-icon-shape-hexagon')) {
				initHover.icon_background = $('.gem-icon .gem-icon-shape-hexagon-top-inner-before', $item).css('background-color');
				initHover.icon_border = $('.gem-icon .gem-icon-shape-hexagon-back-inner-before', $item).css('background-color');
			}
			$item.data('initHover', initHover);
			if ($('a', $item).length) {
				if ($item.hasClass('quickfinder-item-effect-background-reverse') || $item.hasClass('quickfinder-item-effect-border-reverse') && !$item.hasClass('border-reverse-with-background')) {
					$('.gem-icon-inner', $item).prepend('<div class="quickfinder-animation"/>');
				}
			}
		});
		$('body').on('mouseenter', '.quickfinder-item a', function () {
			var $item = $(this).closest('.quickfinder-item');
			var $quickfinder = $item.closest('.quickfinder');
			var initHover = $item.data('initHover');
			$item.addClass('hover');
			if ($quickfinder.data('hover-icon-color')) {
				if ($item.hasClass('quickfinder-item-effect-background-reverse')) {
					if ($('.gem-icon', $item).hasClass('gem-icon-shape-hexagon')) {
						$('.gem-icon .gem-icon-shape-hexagon-back-inner-before', $item).css('background-color', $quickfinder.data('hover-icon-color'));
						$('.gem-icon .gem-icon-shape-hexagon-top-inner-before', $item).css('background-color', '#ffffff');
					} else {
						$('.gem-icon', $item).css('border-color', $quickfinder.data('hover-icon-color'));
						$('.gem-icon-inner', $item).css('background-color', $quickfinder.data('hover-icon-color'));
					}
					$('.gem-icon-half-1', $item).css('color', $quickfinder.data('hover-icon-color'));
					$('.gem-icon-half-2', $item).css('color', $quickfinder.data('hover-icon-color'));
				}
				if ($item.hasClass('quickfinder-item-effect-border-reverse')) {
					if ($('.gem-icon', $item).hasClass('gem-icon-shape-hexagon')) {
						$('.gem-icon .gem-icon-shape-hexagon-back-inner-before', $item).css('background-color', $quickfinder.data('hover-icon-color'));
						$('.gem-icon .gem-icon-shape-hexagon-top-inner-before', $item).css('background-color', $quickfinder.data('hover-icon-color'));
					} else {
						$('.gem-icon', $item).css('border-color', $quickfinder.data('hover-icon-color'));
						$('.gem-icon-inner', $item).css('background-color', $quickfinder.data('hover-icon-color'));
					}
					$('.gem-icon-half-1', $item).css('color', '#ffffff');
					$('.gem-icon-half-2', $item).css('color', '#ffffff');
				}
				if ($item.hasClass('quickfinder-item-effect-simple')) {
					$('.gem-icon-half-1', $item).css('color', $quickfinder.data('hover-icon-color'));
					$('.gem-icon-half-2', $item).css('color', $quickfinder.data('hover-icon-color'));
				}
			} else {
				if ($item.hasClass('quickfinder-item-effect-background-reverse')) {
					if ($('.gem-icon', $item).hasClass('gem-icon-shape-hexagon')) {
						$('.gem-icon .gem-icon-shape-hexagon-top-inner-before', $item).css('background-color', '#ffffff');
					} else {
						$('.gem-icon', $item).css('border-color', $quickfinder.data('hover-icon-color'));
					}
					if (initHover.icon_color1 == '#ffffff' || initHover.icon_color1 == 'rgb(255, 255, 255)') {
						$('.gem-icon-half-1', $item).css('color', initHover.icon_border);
					}
					if (initHover.icon_color2 == '#ffffff' || initHover.icon_color2 == 'rgb(255, 255, 255)') {
						$('.gem-icon-half-2', $item).css('color', initHover.icon_border);
					}
				}
				if ($item.hasClass('quickfinder-item-effect-border-reverse')) {
					if ($('.gem-icon', $item).hasClass('gem-icon-shape-hexagon')) {
						$('.gem-icon .gem-icon-shape-hexagon-top-inner-before', $item).css('background-color', initHover.icon_border);
					} else {
						$('.gem-icon-inner', $item).css('background-color', initHover.icon_border);
					}
					$('.gem-icon-half-1', $item).css('color', '#ffffff');
					$('.gem-icon-half-2', $item).css('color', '#ffffff');
				}
			}
			if ($quickfinder.data('hover-box-color') && !$quickfinder.hasClass('quickfinder-style-default') && !$quickfinder.hasClass('quickfinder-style-vertical')) {
				$('.quickfinder-item-box', $item).css('background-color', $quickfinder.data('hover-box-color'));
			}
			if ($quickfinder.data('hover-border-color') && !$quickfinder.hasClass('quickfinder-style-default') && !$quickfinder.hasClass('quickfinder-style-vertical')) {
				$('.quickfinder-item-box', $item).css('border-color', $quickfinder.data('hover-border-color'));
			}
			if ($quickfinder.data('hover-title-color')) {
				$('.quickfinder-item-title', $item).css('color', $quickfinder.data('hover-title-color'));
			}
			if ($quickfinder.data('hover-description-color')) {
				$('.quickfinder-item-text', $item).css('color', $quickfinder.data('hover-description-color'));
			}
			if ($quickfinder.data('hover-button-text-color')) {
				$('.quickfinder-button .gem-button', $item).css('color', $quickfinder.data('hover-button-text-color'));
			}
			if ($quickfinder.data('hover-button-background-color')) {
				$('.quickfinder-button .gem-button', $item).css('background-color', $quickfinder.data('hover-button-background-color'));
			}
			if ($quickfinder.data('hover-button-border-color')) {
				$('.quickfinder-button .gem-button', $item).css('border-color', $quickfinder.data('hover-button-border-color'));
			}
		});
		$('body').on('mouseleave', '.quickfinder-item a', function () {
			var $item = $(this).closest('.quickfinder-item');
			var $quickfinder = $item.closest('.quickfinder');
			var initHover = $item.data('initHover');
			$item.removeClass('hover');
			$('.gem-icon', $item).css('border-color', initHover.icon_border);
			$('.gem-icon-inner', $item).css('background-color', initHover.icon_background);
			$('.gem-icon-half-1', $item).css('color', initHover.icon_color1);
			$('.gem-icon-half-2', $item).css('color', initHover.icon_color2);
			$('.quickfinder-item-box', $item).css('background-color', initHover.box_color);
			$('.quickfinder-item-box', $item).css('border-color', initHover.border_color);
			$('.quickfinder-item-title', $item).css('color', initHover.title_color);
			$('.quickfinder-item-text', $item).css('color', initHover.description_color);
			$('.quickfinder-button .gem-button', $item).css('color', initHover.button_text_color);
			$('.quickfinder-button .gem-button', $item).css('background-color', initHover.button_background_color);
			$('.quickfinder-button .gem-button', $item).css('border-color', initHover.button_border_color);
			if ($('.gem-icon', $item).hasClass('gem-icon-shape-hexagon')) {
				$('.gem-icon .gem-icon-shape-hexagon-top-inner-before', $item).css('background-color', initHover.icon_background);
				$('.gem-icon .gem-icon-shape-hexagon-back-inner-before', $item).css('background-color', initHover.icon_border);
			}
		});
	});
})(jQuery);;
(function () {
	"use strict";

	function a() {}

	function b(a, b) {
		for (var c = a.length; c--;)
			if (a[c].listener === b) return c;
		return -1
	}

	function c(a) {
		return function () {
			return this[a].apply(this, arguments)
		}
	}
	var d = a.prototype,
		e = this,
		f = e.EventEmitter;
	d.getListeners = function (a) {
		var b, c, d = this._getEvents();
		if ("object" == typeof a) {
			b = {};
			for (c in d) d.hasOwnProperty(c) && a.test(c) && (b[c] = d[c])
		} else b = d[a] || (d[a] = []);
		return b
	}, d.flattenListeners = function (a) {
		var b, c = [];
		for (b = 0; b < a.length; b += 1) c.push(a[b].listener);
		return c
	}, d.getListenersAsObject = function (a) {
		var b, c = this.getListeners(a);
		return c instanceof Array && (b = {}, b[a] = c), b || c
	}, d.addListener = function (a, c) {
		var d, e = this.getListenersAsObject(a),
			f = "object" == typeof c;
		for (d in e) e.hasOwnProperty(d) && -1 === b(e[d], c) && e[d].push(f ? c : {
			listener: c,
			once: !1
		});
		return this
	}, d.on = c("addListener"), d.addOnceListener = function (a, b) {
		return this.addListener(a, {
			listener: b,
			once: !0
		})
	}, d.once = c("addOnceListener"), d.defineEvent = function (a) {
		return this.getListeners(a), this
	}, d.defineEvents = function (a) {
		for (var b = 0; b < a.length; b += 1) this.defineEvent(a[b]);
		return this
	}, d.removeListener = function (a, c) {
		var d, e, f = this.getListenersAsObject(a);
		for (e in f) f.hasOwnProperty(e) && (d = b(f[e], c), -1 !== d && f[e].splice(d, 1));
		return this
	}, d.off = c("removeListener"), d.addListeners = function (a, b) {
		return this.manipulateListeners(!1, a, b)
	}, d.removeListeners = function (a, b) {
		return this.manipulateListeners(!0, a, b)
	}, d.manipulateListeners = function (a, b, c) {
		var d, e, f = a ? this.removeListener : this.addListener,
			g = a ? this.removeListeners : this.addListeners;
		if ("object" != typeof b || b instanceof RegExp)
			for (d = c.length; d--;) f.call(this, b, c[d]);
		else
			for (d in b) b.hasOwnProperty(d) && (e = b[d]) && ("function" == typeof e ? f.call(this, d, e) : g.call(this, d, e));
		return this
	}, d.removeEvent = function (a) {
		var b, c = typeof a,
			d = this._getEvents();
		if ("string" === c) delete d[a];
		else if ("object" === c)
			for (b in d) d.hasOwnProperty(b) && a.test(b) && delete d[b];
		else delete this._events;
		return this
	}, d.removeAllListeners = c("removeEvent"), d.emitEvent = function (a, b) {
		var c, d, e, f, g = this.getListenersAsObject(a);
		for (e in g)
			if (g.hasOwnProperty(e))
				for (d = g[e].length; d--;) c = g[e][d], c.once === !0 && this.removeListener(a, c.listener), f = c.listener.apply(this, b || []), f === this._getOnceReturnValue() && this.removeListener(a, c.listener);
		return this
	}, d.trigger = c("emitEvent"), d.emit = function (a) {
		var b = Array.prototype.slice.call(arguments, 1);
		return this.emitEvent(a, b)
	}, d.setOnceReturnValue = function (a) {
		return this._onceReturnValue = a, this
	}, d._getOnceReturnValue = function () {
		return !this.hasOwnProperty("_onceReturnValue") || this._onceReturnValue
	}, d._getEvents = function () {
		return this._events || (this._events = {})
	}, a.noConflict = function () {
		return e.EventEmitter = f, a
	}, "function" == typeof define && define.amd ? define("eventEmitter/EventEmitter", [], function () {
		return a
	}) : "object" == typeof module && module.exports ? module.exports = a : this.EventEmitter = a
}).call(this),
	function (a) {
		function b(b) {
			var c = a.event;
			return c.target = c.target || c.srcElement || b, c
		}
		var c = document.documentElement,
			d = function () {};
		c.addEventListener ? d = function (a, b, c) {
			a.addEventListener(b, c, !1)
		} : c.attachEvent && (d = function (a, c, d) {
			a[c + d] = d.handleEvent ? function () {
				var c = b(a);
				d.handleEvent.call(d, c)
			} : function () {
				var c = b(a);
				d.call(a, c)
			}, a.attachEvent("on" + c, a[c + d])
		});
		var e = function () {};
		c.removeEventListener ? e = function (a, b, c) {
			a.removeEventListener(b, c, !1)
		} : c.detachEvent && (e = function (a, b, c) {
			a.detachEvent("on" + b, a[b + c]);
			try {
				delete a[b + c]
			} catch (d) {
				a[b + c] = void 0
			}
		});
		var f = {
			bind: d,
			unbind: e
		};
		"function" == typeof define && define.amd ? define("eventie/eventie", f) : a.eventie = f
	}(this),
	function (a, b) {
		"use strict";
		"function" == typeof define && define.amd ? define(["eventEmitter/EventEmitter", "eventie/eventie"], function (c, d) {
			return b(a, c, d)
		}) : "object" == typeof module && module.exports ? module.exports = b(a, require("wolfy87-eventemitter"), require("eventie")) : a.imagesLoaded = b(a, a.EventEmitter, a.eventie)
	}(window, function (a, b, c) {
		function d(a, b) {
			for (var c in b) a[c] = b[c];
			return a
		}

		function e(a) {
			return "[object Array]" == l.call(a)
		}

		function f(a) {
			var b = [];
			if (e(a)) b = a;
			else if ("number" == typeof a.length)
				for (var c = 0; c < a.length; c++) b.push(a[c]);
			else b.push(a);
			return b
		}

		function g(a, b, c) {
			if (!(this instanceof g)) return new g(a, b, c);
			"string" == typeof a && (a = document.querySelectorAll(a)), this.elements = f(a), this.options = d({}, this.options), "function" == typeof b ? c = b : d(this.options, b), c && this.on("always", c), this.getImages(), j && (this.jqDeferred = new j.Deferred);
			var e = this;
			setTimeout(function () {
				e.check()
			})
		}

		function h(a) {
			this.img = a
		}

		function i(a, b) {
			this.url = a, this.element = b, this.img = new Image
		}
		var j = a.jQuery,
			k = a.console,
			l = Object.prototype.toString;
		g.prototype = new b, g.prototype.options = {}, g.prototype.getImages = function () {
			this.images = [];
			for (var a = 0; a < this.elements.length; a++) {
				var b = this.elements[a];
				this.addElementImages(b)
			}
		}, g.prototype.addElementImages = function (a) {
			"IMG" == a.nodeName && this.addImage(a), this.options.background === !0 && this.addElementBackgroundImages(a);
			var b = a.nodeType;
			if (b && m[b]) {
				for (var c = a.querySelectorAll("img"), d = 0; d < c.length; d++) {
					var e = c[d];
					this.addImage(e)
				}
				if ("string" == typeof this.options.background) {
					var f = a.querySelectorAll(this.options.background);
					for (d = 0; d < f.length; d++) {
						var g = f[d];
						this.addElementBackgroundImages(g)
					}
				}
			}
		};
		var m = {
			1: !0,
			9: !0,
			11: !0
		};
		g.prototype.addElementBackgroundImages = function (a) {
			for (var b = n(a), c = /url\(['"]*([^'"\)]+)['"]*\)/gi, d = c.exec(b.backgroundImage); null !== d;) {
				var e = d && d[1];
				e && this.addBackground(e, a), d = c.exec(b.backgroundImage)
			}
		};
		var n = a.getComputedStyle || function (a) {
			return a.currentStyle
		};
		return g.prototype.addImage = function (a) {
			var b = new h(a);
			this.images.push(b)
		}, g.prototype.addBackground = function (a, b) {
			var c = new i(a, b);
			this.images.push(c)
		}, g.prototype.check = function () {
			function a(a, c, d) {
				setTimeout(function () {
					b.progress(a, c, d)
				})
			}
			var b = this;
			if (this.progressedCount = 0, this.hasAnyBroken = !1, !this.images.length) return void this.complete();
			for (var c = 0; c < this.images.length; c++) {
				var d = this.images[c];
				d.once("progress", a), d.check()
			}
		}, g.prototype.progress = function (a, b, c) {
			this.progressedCount++, this.hasAnyBroken = this.hasAnyBroken || !a.isLoaded, this.emit("progress", this, a, b), this.jqDeferred && this.jqDeferred.notify && this.jqDeferred.notify(this, a), this.progressedCount == this.images.length && this.complete(), this.options.debug && k && k.log("progress: " + c, a, b)
		}, g.prototype.complete = function () {
			var a = this.hasAnyBroken ? "fail" : "done";
			if (this.isComplete = !0, this.emit(a, this), this.emit("always", this), this.jqDeferred) {
				var b = this.hasAnyBroken ? "reject" : "resolve";
				this.jqDeferred[b](this)
			}
		}, h.prototype = new b, h.prototype.check = function () {
			var a = this.getIsImageComplete();
			return a ? void this.confirm(0 !== this.img.naturalWidth, "naturalWidth") : (this.proxyImage = new Image, c.bind(this.proxyImage, "load", this), c.bind(this.proxyImage, "error", this), c.bind(this.img, "load", this), c.bind(this.img, "error", this), void(this.proxyImage.src = this.img.src))
		}, h.prototype.getIsImageComplete = function () {
			return this.img.complete && void 0 !== this.img.naturalWidth
		}, h.prototype.confirm = function (a, b) {
			this.isLoaded = a, this.emit("progress", this, this.img, b)
		}, h.prototype.handleEvent = function (a) {
			var b = "on" + a.type;
			this[b] && this[b](a)
		}, h.prototype.onload = function () {
			this.confirm(!0, "onload"), this.unbindEvents()
		}, h.prototype.onerror = function () {
			this.confirm(!1, "onerror"), this.unbindEvents()
		}, h.prototype.unbindEvents = function () {
			c.unbind(this.proxyImage, "load", this), c.unbind(this.proxyImage, "error", this), c.unbind(this.img, "load", this), c.unbind(this.img, "error", this)
		}, i.prototype = new h, i.prototype.check = function () {
			c.bind(this.img, "load", this), c.bind(this.img, "error", this), this.img.src = this.url;
			var a = this.getIsImageComplete();
			a && (this.confirm(0 !== this.img.naturalWidth, "naturalWidth"), this.unbindEvents())
		}, i.prototype.unbindEvents = function () {
			c.unbind(this.img, "load", this), c.unbind(this.img, "error", this)
		}, i.prototype.confirm = function (a, b) {
			this.isLoaded = a, this.emit("progress", this, this.element, b)
		}, g.makeJQueryPlugin = function (b) {
			b = b || a.jQuery, b && (j = b, j.fn.imagesLoaded = function (a, b) {
				var c = new g(this, a, b);
				return c.jqDeferred.promise(j(this))
			})
		}, g.makeJQueryPlugin(), g
	});;
/*!
 * Isotope PACKAGED v2.2.2
 *
 * Licensed GPLv3 for open source use
 * or Isotope Commercial License for commercial use
 *
 * http://isotope.metafizzy.co
 * Copyright 2015 Metafizzy
 */

! function (a) {
	function b() {}

	function c(a) {
		function c(b) {
			b.prototype.option || (b.prototype.option = function (b) {
				a.isPlainObject(b) && (this.options = a.extend(!0, this.options, b))
			})
		}

		function e(b, c) {
			a.fn[b] = function (e) {
				if ("string" == typeof e) {
					for (var g = d.call(arguments, 1), h = 0, i = this.length; i > h; h++) {
						var j = this[h],
							k = a.data(j, b);
						if (k)
							if (a.isFunction(k[e]) && "_" !== e.charAt(0)) {
								var l = k[e].apply(k, g);
								if (void 0 !== l) return l
							} else f("no such method '" + e + "' for " + b + " instance");
						else f("cannot call methods on " + b + " prior to initialization; attempted to call '" + e + "'")
					}
					return this
				}
				return this.each(function () {
					var d = a.data(this, b);
					d ? (d.option(e), d._init()) : (d = new c(this, e), a.data(this, b, d))
				})
			}
		}
		if (a) {
			var f = "undefined" == typeof console ? b : function (a) {
				console.error(a)
			};
			return a.bridget = function (a, b) {
				c(b), e(a, b)
			}, a.bridget
		}
	}
	var d = Array.prototype.slice;
	"function" == typeof define && define.amd ? define("jquery-bridget/jquery.bridget", ["jquery"], c) : c("object" == typeof exports ? require("jquery") : a.jQuery)
}(window),
function (a) {
	function b(b) {
		var c = a.event;
		return c.target = c.target || c.srcElement || b, c
	}
	var c = document.documentElement,
		d = function () {};
	c.addEventListener ? d = function (a, b, c) {
		a.addEventListener(b, c, !1)
	} : c.attachEvent && (d = function (a, c, d) {
		a[c + d] = d.handleEvent ? function () {
			var c = b(a);
			d.handleEvent.call(d, c)
		} : function () {
			var c = b(a);
			d.call(a, c)
		}, a.attachEvent("on" + c, a[c + d])
	});
	var e = function () {};
	c.removeEventListener ? e = function (a, b, c) {
		a.removeEventListener(b, c, !1)
	} : c.detachEvent && (e = function (a, b, c) {
		a.detachEvent("on" + b, a[b + c]);
		try {
			delete a[b + c]
		} catch (d) {
			a[b + c] = void 0
		}
	});
	var f = {
		bind: d,
		unbind: e
	};
	"function" == typeof define && define.amd ? define("eventie/eventie", f) : "object" == typeof exports ? module.exports = f : a.eventie = f
}(window),
function () {
	"use strict";

	function a() {}

	function b(a, b) {
		for (var c = a.length; c--;)
			if (a[c].listener === b) return c;
		return -1
	}

	function c(a) {
		return function () {
			return this[a].apply(this, arguments)
		}
	}
	var d = a.prototype,
		e = this,
		f = e.EventEmitter;
	d.getListeners = function (a) {
		var b, c, d = this._getEvents();
		if (a instanceof RegExp) {
			b = {};
			for (c in d) d.hasOwnProperty(c) && a.test(c) && (b[c] = d[c])
		} else b = d[a] || (d[a] = []);
		return b
	}, d.flattenListeners = function (a) {
		var b, c = [];
		for (b = 0; b < a.length; b += 1) c.push(a[b].listener);
		return c
	}, d.getListenersAsObject = function (a) {
		var b, c = this.getListeners(a);
		return c instanceof Array && (b = {}, b[a] = c), b || c
	}, d.addListener = function (a, c) {
		var d, e = this.getListenersAsObject(a),
			f = "object" == typeof c;
		for (d in e) e.hasOwnProperty(d) && -1 === b(e[d], c) && e[d].push(f ? c : {
			listener: c,
			once: !1
		});
		return this
	}, d.on = c("addListener"), d.addOnceListener = function (a, b) {
		return this.addListener(a, {
			listener: b,
			once: !0
		})
	}, d.once = c("addOnceListener"), d.defineEvent = function (a) {
		return this.getListeners(a), this
	}, d.defineEvents = function (a) {
		for (var b = 0; b < a.length; b += 1) this.defineEvent(a[b]);
		return this
	}, d.removeListener = function (a, c) {
		var d, e, f = this.getListenersAsObject(a);
		for (e in f) f.hasOwnProperty(e) && (d = b(f[e], c), -1 !== d && f[e].splice(d, 1));
		return this
	}, d.off = c("removeListener"), d.addListeners = function (a, b) {
		return this.manipulateListeners(!1, a, b)
	}, d.removeListeners = function (a, b) {
		return this.manipulateListeners(!0, a, b)
	}, d.manipulateListeners = function (a, b, c) {
		var d, e, f = a ? this.removeListener : this.addListener,
			g = a ? this.removeListeners : this.addListeners;
		if ("object" != typeof b || b instanceof RegExp)
			for (d = c.length; d--;) f.call(this, b, c[d]);
		else
			for (d in b) b.hasOwnProperty(d) && (e = b[d]) && ("function" == typeof e ? f.call(this, d, e) : g.call(this, d, e));
		return this
	}, d.removeEvent = function (a) {
		var b, c = typeof a,
			d = this._getEvents();
		if ("string" === c) delete d[a];
		else if (a instanceof RegExp)
			for (b in d) d.hasOwnProperty(b) && a.test(b) && delete d[b];
		else delete this._events;
		return this
	}, d.removeAllListeners = c("removeEvent"), d.emitEvent = function (a, b) {
		var c, d, e, f, g = this.getListenersAsObject(a);
		for (e in g)
			if (g.hasOwnProperty(e))
				for (d = g[e].length; d--;) c = g[e][d], c.once === !0 && this.removeListener(a, c.listener), f = c.listener.apply(this, b || []), f === this._getOnceReturnValue() && this.removeListener(a, c.listener);
		return this
	}, d.trigger = c("emitEvent"), d.emit = function (a) {
		var b = Array.prototype.slice.call(arguments, 1);
		return this.emitEvent(a, b)
	}, d.setOnceReturnValue = function (a) {
		return this._onceReturnValue = a, this
	}, d._getOnceReturnValue = function () {
		return this.hasOwnProperty("_onceReturnValue") ? this._onceReturnValue : !0
	}, d._getEvents = function () {
		return this._events || (this._events = {})
	}, a.noConflict = function () {
		return e.EventEmitter = f, a
	}, "function" == typeof define && define.amd ? define("eventEmitter/EventEmitter", [], function () {
		return a
	}) : "object" == typeof module && module.exports ? module.exports = a : e.EventEmitter = a
}.call(this),
	function (a) {
		function b(a) {
			if (a) {
				if ("string" == typeof d[a]) return a;
				a = a.charAt(0).toUpperCase() + a.slice(1);
				for (var b, e = 0, f = c.length; f > e; e++)
					if (b = c[e] + a, "string" == typeof d[b]) return b
			}
		}
		var c = "Webkit Moz ms Ms O".split(" "),
			d = document.documentElement.style;
		"function" == typeof define && define.amd ? define("get-style-property/get-style-property", [], function () {
			return b
		}) : "object" == typeof exports ? module.exports = b : a.getStyleProperty = b
	}(window),
	function (a, b) {
		function c(a) {
			var b = parseFloat(a),
				c = -1 === a.indexOf("%") && !isNaN(b);
			return c && b
		}

		function d() {}

		function e() {
			for (var a = {
					width: 0,
					height: 0,
					innerWidth: 0,
					innerHeight: 0,
					outerWidth: 0,
					outerHeight: 0
				}, b = 0, c = h.length; c > b; b++) {
				var d = h[b];
				a[d] = 0
			}
			return a
		}

		function f(b) {
			function d() {
				if (!m) {
					m = !0;
					var d = a.getComputedStyle;
					if (j = function () {
							var a = d ? function (a) {
								return d(a, null)
							} : function (a) {
								return a.currentStyle
							};
							return function (b) {
								var c = a(b);
								return c || g("Style returned " + c + ". Are you running this code in a hidden iframe on Firefox? See http://bit.ly/getsizebug1"), c
							}
						}(), k = b("boxSizing")) {
						var e = document.createElement("div");
						e.style.width = "200px", e.style.padding = "1px 2px 3px 4px", e.style.borderStyle = "solid", e.style.borderWidth = "1px 2px 3px 4px", e.style[k] = "border-box";
						var f = document.body || document.documentElement;
						f.appendChild(e);
						var h = j(e);
						l = 200 === c(h.width), f.removeChild(e)
					}
				}
			}

			function f(a) {
				if (d(), "string" == typeof a && (a = document.querySelector(a)), a && "object" == typeof a && a.nodeType) {
					var b = j(a);
					if ("none" === b.display) return e();
					var f = {};
					f.width = a.offsetWidth, f.height = a.offsetHeight;
					for (var g = f.isBorderBox = !(!k || !b[k] || "border-box" !== b[k]), m = 0, n = h.length; n > m; m++) {
						var o = h[m],
							p = b[o];
						p = i(a, p);
						var q = parseFloat(p);
						f[o] = isNaN(q) ? 0 : q
					}
					var r = f.paddingLeft + f.paddingRight,
						s = f.paddingTop + f.paddingBottom,
						t = f.marginLeft + f.marginRight,
						u = f.marginTop + f.marginBottom,
						v = f.borderLeftWidth + f.borderRightWidth,
						w = f.borderTopWidth + f.borderBottomWidth,
						x = g && l,
						y = c(b.width);
					y !== !1 && (f.width = y + (x ? 0 : r + v));
					var z = c(b.height);
					return z !== !1 && (f.height = z + (x ? 0 : s + w)), f.innerWidth = f.width - (r + v), f.innerHeight = f.height - (s + w), f.outerWidth = f.width + t, f.outerHeight = f.height + u, f
				}
			}

			function i(b, c) {
				if (a.getComputedStyle || -1 === c.indexOf("%")) return c;
				var d = b.style,
					e = d.left,
					f = b.runtimeStyle,
					g = f && f.left;
				return g && (f.left = b.currentStyle.left), d.left = c, c = d.pixelLeft, d.left = e, g && (f.left = g), c
			}
			var j, k, l, m = !1;
			return f
		}
		var g = "undefined" == typeof console ? d : function (a) {
				console.error(a)
			},
			h = ["paddingLeft", "paddingRight", "paddingTop", "paddingBottom", "marginLeft", "marginRight", "marginTop", "marginBottom", "borderLeftWidth", "borderRightWidth", "borderTopWidth", "borderBottomWidth"];
		"function" == typeof define && define.amd ? define("get-size/get-size", ["get-style-property/get-style-property"], f) : "object" == typeof exports ? module.exports = f(require("desandro-get-style-property")) : a.getSize = f(a.getStyleProperty)
	}(window),
	function (a) {
		function b(a) {
			"function" == typeof a && (b.isReady ? a() : g.push(a))
		}

		function c(a) {
			var c = "readystatechange" === a.type && "complete" !== f.readyState;
			b.isReady || c || d()
		}

		function d() {
			b.isReady = !0;
			for (var a = 0, c = g.length; c > a; a++) {
				var d = g[a];
				d()
			}
		}

		function e(e) {
			return "complete" === f.readyState ? d() : (e.bind(f, "DOMContentLoaded", c), e.bind(f, "readystatechange", c), e.bind(a, "load", c)), b
		}
		var f = a.document,
			g = [];
		b.isReady = !1, "function" == typeof define && define.amd ? define("doc-ready/doc-ready", ["eventie/eventie"], e) : "object" == typeof exports ? module.exports = e(require("eventie")) : a.docReady = e(a.eventie)
	}(window),
	function (a) {
		"use strict";

		function b(a, b) {
			return a[g](b)
		}

		function c(a) {
			if (!a.parentNode) {
				var b = document.createDocumentFragment();
				b.appendChild(a)
			}
		}

		function d(a, b) {
			c(a);
			for (var d = a.parentNode.querySelectorAll(b), e = 0, f = d.length; f > e; e++)
				if (d[e] === a) return !0;
			return !1
		}

		function e(a, d) {
			return c(a), b(a, d)
		}
		var f, g = function () {
			if (a.matches) return "matches";
			if (a.matchesSelector) return "matchesSelector";
			for (var b = ["webkit", "moz", "ms", "o"], c = 0, d = b.length; d > c; c++) {
				var e = b[c],
					f = e + "MatchesSelector";
				if (a[f]) return f
			}
		}();
		if (g) {
			var h = document.createElement("div"),
				i = b(h, "div");
			f = i ? b : e
		} else f = d;
		"function" == typeof define && define.amd ? define("matches-selector/matches-selector", [], function () {
			return f
		}) : "object" == typeof exports ? module.exports = f : window.matchesSelector = f
	}(Element.prototype),
	function (a, b) {
		"use strict";
		"function" == typeof define && define.amd ? define("fizzy-ui-utils/utils", ["doc-ready/doc-ready", "matches-selector/matches-selector"], function (c, d) {
			return b(a, c, d)
		}) : "object" == typeof exports ? module.exports = b(a, require("doc-ready"), require("desandro-matches-selector")) : a.fizzyUIUtils = b(a, a.docReady, a.matchesSelector)
	}(window, function (a, b, c) {
		var d = {};
		d.extend = function (a, b) {
			for (var c in b) a[c] = b[c];
			return a
		}, d.modulo = function (a, b) {
			return (a % b + b) % b
		};
		var e = Object.prototype.toString;
		d.isArray = function (a) {
			return "[object Array]" == e.call(a)
		}, d.makeArray = function (a) {
			var b = [];
			if (d.isArray(a)) b = a;
			else if (a && "number" == typeof a.length)
				for (var c = 0, e = a.length; e > c; c++) b.push(a[c]);
			else b.push(a);
			return b
		}, d.indexOf = Array.prototype.indexOf ? function (a, b) {
			return a.indexOf(b)
		} : function (a, b) {
			for (var c = 0, d = a.length; d > c; c++)
				if (a[c] === b) return c;
			return -1
		}, d.removeFrom = function (a, b) {
			var c = d.indexOf(a, b); - 1 != c && a.splice(c, 1)
		}, d.isElement = "function" == typeof HTMLElement || "object" == typeof HTMLElement ? function (a) {
			return a instanceof HTMLElement
		} : function (a) {
			return a && "object" == typeof a && 1 == a.nodeType && "string" == typeof a.nodeName
		}, d.setText = function () {
			function a(a, c) {
				b = b || (void 0 !== document.documentElement.textContent ? "textContent" : "innerText"), a[b] = c
			}
			var b;
			return a
		}(), d.getParent = function (a, b) {
			for (; a != document.body;)
				if (a = a.parentNode, c(a, b)) return a
		}, d.getQueryElement = function (a) {
			return "string" == typeof a ? document.querySelector(a) : a
		}, d.handleEvent = function (a) {
			var b = "on" + a.type;
			this[b] && this[b](a)
		}, d.filterFindElements = function (a, b) {
			a = d.makeArray(a);
			for (var e = [], f = 0, g = a.length; g > f; f++) {
				var h = a[f];
				if (d.isElement(h))
					if (b) {
						c(h, b) && e.push(h);
						for (var i = h.querySelectorAll(b), j = 0, k = i.length; k > j; j++) e.push(i[j])
					} else e.push(h)
			}
			return e
		}, d.debounceMethod = function (a, b, c) {
			var d = a.prototype[b],
				e = b + "Timeout";
			a.prototype[b] = function () {
				var a = this[e];
				a && clearTimeout(a);
				var b = arguments,
					f = this;
				this[e] = setTimeout(function () {
					d.apply(f, b), delete f[e]
				}, c || 100)
			}
		}, d.toDashed = function (a) {
			return a.replace(/(.)([A-Z])/g, function (a, b, c) {
				return b + "-" + c
			}).toLowerCase()
		};
		var f = a.console;
		return d.htmlInit = function (c, e) {
			b(function () {
				for (var b = d.toDashed(e), g = document.querySelectorAll(".js-" + b), h = "data-" + b + "-options", i = 0, j = g.length; j > i; i++) {
					var k, l = g[i],
						m = l.getAttribute(h);
					try {
						k = m && JSON.parse(m)
					} catch (n) {
						f && f.error("Error parsing " + h + " on " + l.nodeName.toLowerCase() + (l.id ? "#" + l.id : "") + ": " + n);
						continue
					}
					var o = new c(l, k),
						p = a.jQuery;
					p && p.data(l, e, o)
				}
			})
		}, d
	}),
	function (a, b) {
		"use strict";
		"function" == typeof define && define.amd ? define("outlayer/item", ["eventEmitter/EventEmitter", "get-size/get-size", "get-style-property/get-style-property", "fizzy-ui-utils/utils"], function (c, d, e, f) {
			return b(a, c, d, e, f)
		}) : "object" == typeof exports ? module.exports = b(a, require("wolfy87-eventemitter"), require("get-size"), require("desandro-get-style-property"), require("fizzy-ui-utils")) : (a.Outlayer = {}, a.Outlayer.Item = b(a, a.EventEmitter, a.getSize, a.getStyleProperty, a.fizzyUIUtils))
	}(window, function (a, b, c, d, e) {
		"use strict";

		function f(a) {
			for (var b in a) return !1;
			return b = null, !0
		}

		function g(a, b) {
			a && (this.element = a, this.layout = b, this.position = {
				x: 0,
				y: 0
			}, this._create())
		}

		function h(a) {
			return a.replace(/([A-Z])/g, function (a) {
				return "-" + a.toLowerCase()
			})
		}
		var i = a.getComputedStyle,
			j = i ? function (a) {
				return i(a, null)
			} : function (a) {
				return a.currentStyle
			},
			k = d("transition"),
			l = d("transform"),
			m = k && l,
			n = !!d("perspective"),
			o = {
				WebkitTransition: "webkitTransitionEnd",
				MozTransition: "transitionend",
				OTransition: "otransitionend",
				transition: "transitionend"
			}[k],
			p = ["transform", "transition", "transitionDuration", "transitionProperty"],
			q = function () {
				for (var a = {}, b = 0, c = p.length; c > b; b++) {
					var e = p[b],
						f = d(e);
					f && f !== e && (a[e] = f)
				}
				return a
			}();
		e.extend(g.prototype, b.prototype), g.prototype._create = function () {
			this._transn = {
				ingProperties: {},
				clean: {},
				onEnd: {}
			}, this.css({
				position: "absolute"
			})
		}, g.prototype.handleEvent = function (a) {
			var b = "on" + a.type;
			this[b] && this[b](a)
		}, g.prototype.getSize = function () {
			this.size = c(this.element)
		}, g.prototype.css = function (a) {
			var b = this.element.style;
			for (var c in a) {
				var d = q[c] || c;
				b[d] = a[c]
			}
		}, g.prototype.getPosition = function () {
			var a = j(this.element),
				b = this.layout.options,
				c = b.isOriginLeft,
				d = b.isOriginTop,
				e = a[c ? "left" : "right"],
				f = a[d ? "top" : "bottom"],
				g = this.layout.size,
				h = -1 != e.indexOf("%") ? parseFloat(e) / 100 * g.width : parseInt(e, 10),
				i = -1 != f.indexOf("%") ? parseFloat(f) / 100 * g.height : parseInt(f, 10);
			h = isNaN(h) ? 0 : h, i = isNaN(i) ? 0 : i, h -= c ? g.paddingLeft : g.paddingRight, i -= d ? g.paddingTop : g.paddingBottom, this.position.x = h, this.position.y = i
		}, g.prototype.layoutPosition = function () {
			var a = this.layout.size,
				b = this.layout.options,
				c = {},
				d = b.isOriginLeft ? "paddingLeft" : "paddingRight",
				e = b.isOriginLeft ? "left" : "right",
				f = b.isOriginLeft ? "right" : "left",
				g = this.position.x + a[d];
			c[e] = this.getXValue(g), c[f] = "";
			var h = b.isOriginTop ? "paddingTop" : "paddingBottom",
				i = b.isOriginTop ? "top" : "bottom",
				j = b.isOriginTop ? "bottom" : "top",
				k = this.position.y + a[h];
			c[i] = this.getYValue(k), c[j] = "", this.css(c), this.emitEvent("layout", [this])
		}, g.prototype.getXValue = function (a) {
			var b = this.layout.options;
			return b.percentPosition && !b.isHorizontal ? a / this.layout.size.width * 100 + "%" : a + "px"
		}, g.prototype.getYValue = function (a) {
			var b = this.layout.options;
			return b.percentPosition && b.isHorizontal ? a / this.layout.size.height * 100 + "%" : a + "px"
		}, g.prototype._transitionTo = function (a, b) {
			this.getPosition();
			var c = this.position.x,
				d = this.position.y,
				e = parseInt(a, 10),
				f = parseInt(b, 10),
				g = e === this.position.x && f === this.position.y;
			if (this.setPosition(a, b), g && !this.isTransitioning) return void this.layoutPosition();
			var h = a - c,
				i = b - d,
				j = {};
			j.transform = this.getTranslate(h, i), this.transition({
				to: j,
				onTransitionEnd: {
					transform: this.layoutPosition
				},
				isCleaning: !0
			})
		}, g.prototype.getTranslate = function (a, b) {
			var c = this.layout.options;
			return a = c.isOriginLeft ? a : -a, b = c.isOriginTop ? b : -b, n ? "translate3d(" + a + "px, " + b + "px, 0)" : "translate(" + a + "px, " + b + "px)"
		}, g.prototype.goTo = function (a, b) {
			this.setPosition(a, b), this.layoutPosition()
		}, g.prototype.moveTo = m ? g.prototype._transitionTo : g.prototype.goTo, g.prototype.setPosition = function (a, b) {
			this.position.x = parseInt(a, 10), this.position.y = parseInt(b, 10)
		}, g.prototype._nonTransition = function (a) {
			this.css(a.to), a.isCleaning && this._removeStyles(a.to);
			for (var b in a.onTransitionEnd) a.onTransitionEnd[b].call(this)
		}, g.prototype._transition = function (a) {
			if (!parseFloat(this.layout.options.transitionDuration)) return void this._nonTransition(a);
			var b = this._transn;
			for (var c in a.onTransitionEnd) b.onEnd[c] = a.onTransitionEnd[c];
			for (c in a.to) b.ingProperties[c] = !0, a.isCleaning && (b.clean[c] = !0);
			if (a.from) {
				this.css(a.from);
				var d = this.element.offsetHeight;
				d = null
			}
			this.enableTransition(a.to), this.css(a.to), this.isTransitioning = !0
		};
		var r = "opacity," + h(q.transform || "transform");
		g.prototype.enableTransition = function () {
			this.isTransitioning || (this.css({
				transitionProperty: r,
				transitionDuration: this.layout.options.transitionDuration
			}), this.element.addEventListener(o, this, !1))
		}, g.prototype.transition = g.prototype[k ? "_transition" : "_nonTransition"], g.prototype.onwebkitTransitionEnd = function (a) {
			this.ontransitionend(a)
		}, g.prototype.onotransitionend = function (a) {
			this.ontransitionend(a)
		};
		var s = {
			"-webkit-transform": "transform",
			"-moz-transform": "transform",
			"-o-transform": "transform"
		};
		g.prototype.ontransitionend = function (a) {
			if (a.target === this.element) {
				var b = this._transn,
					c = s[a.propertyName] || a.propertyName;
				if (delete b.ingProperties[c], f(b.ingProperties) && this.disableTransition(), c in b.clean && (this.element.style[a.propertyName] = "", delete b.clean[c]), c in b.onEnd) {
					var d = b.onEnd[c];
					d.call(this), delete b.onEnd[c]
				}
				this.emitEvent("transitionEnd", [this])
			}
		}, g.prototype.disableTransition = function () {
			this.removeTransitionStyles(), this.element.removeEventListener(o, this, !1), this.isTransitioning = !1
		}, g.prototype._removeStyles = function (a) {
			var b = {};
			for (var c in a) b[c] = "";
			this.css(b)
		};
		var t = {
			transitionProperty: "",
			transitionDuration: ""
		};
		return g.prototype.removeTransitionStyles = function () {
			this.css(t)
		}, g.prototype.removeElem = function () {
			this.element.parentNode.removeChild(this.element), this.css({
				display: ""
			}), this.emitEvent("remove", [this])
		}, g.prototype.remove = function () {
			if (!k || !parseFloat(this.layout.options.transitionDuration)) return void this.removeElem();
			var a = this;
			this.once("transitionEnd", function () {
				a.removeElem()
			}), this.hide()
		}, g.prototype.reveal = function () {
			delete this.isHidden, this.css({
				display: ""
			});
			var a = this.layout.options,
				b = {},
				c = this.getHideRevealTransitionEndProperty("visibleStyle");
			b[c] = this.onRevealTransitionEnd, this.transition({
				from: a.hiddenStyle,
				to: a.visibleStyle,
				isCleaning: !0,
				onTransitionEnd: b
			})
		}, g.prototype.onRevealTransitionEnd = function () {
			this.isHidden || this.emitEvent("reveal")
		}, g.prototype.getHideRevealTransitionEndProperty = function (a) {
			var b = this.layout.options[a];
			if (b.opacity) return "opacity";
			for (var c in b) return c
		}, g.prototype.hide = function () {
			this.isHidden = !0, this.css({
				display: ""
			});
			var a = this.layout.options,
				b = {},
				c = this.getHideRevealTransitionEndProperty("hiddenStyle");
			b[c] = this.onHideTransitionEnd, this.transition({
				from: a.visibleStyle,
				to: a.hiddenStyle,
				isCleaning: !0,
				onTransitionEnd: b
			})
		}, g.prototype.onHideTransitionEnd = function () {
			this.isHidden && (this.css({
				display: "none"
			}), this.emitEvent("hide"))
		}, g.prototype.destroy = function () {
			this.css({
				position: "",
				left: "",
				right: "",
				top: "",
				bottom: "",
				transition: "",
				transform: ""
			})
		}, g
	}),
	function (a, b) {
		"use strict";
		"function" == typeof define && define.amd ? define("outlayer/outlayer", ["eventie/eventie", "eventEmitter/EventEmitter", "get-size/get-size", "fizzy-ui-utils/utils", "./item"], function (c, d, e, f, g) {
			return b(a, c, d, e, f, g)
		}) : "object" == typeof exports ? module.exports = b(a, require("eventie"), require("wolfy87-eventemitter"), require("get-size"), require("fizzy-ui-utils"), require("./item")) : a.Outlayer = b(a, a.eventie, a.EventEmitter, a.getSize, a.fizzyUIUtils, a.Outlayer.Item)
	}(window, function (a, b, c, d, e, f) {
		"use strict";

		function g(a, b) {
			var c = e.getQueryElement(a);
			if (!c) return void(h && h.error("Bad element for " + this.constructor.namespace + ": " + (c || a)));
			this.element = c, i && (this.$element = i(this.element)), this.options = e.extend({}, this.constructor.defaults), this.option(b);
			var d = ++k;
			this.element.outlayerGUID = d, l[d] = this, this._create(), this.options.isInitLayout && this.layout()
		}
		var h = a.console,
			i = a.jQuery,
			j = function () {},
			k = 0,
			l = {};
		return g.namespace = "outlayer", g.Item = f, g.defaults = {
			containerStyle: {
				position: "relative"
			},
			isInitLayout: !0,
			isOriginLeft: !0,
			isOriginTop: !0,
			isResizeBound: !0,
			isResizingContainer: !0,
			transitionDuration: "0.4s",
			hiddenStyle: {
				opacity: 0,
				transform: "scale(0.001)"
			},
			visibleStyle: {
				opacity: 1,
				transform: "scale(1)"
			}
		}, e.extend(g.prototype, c.prototype), g.prototype.option = function (a) {
			e.extend(this.options, a)
		}, g.prototype._create = function () {
			this.reloadItems(), this.stamps = [], this.stamp(this.options.stamp), e.extend(this.element.style, this.options.containerStyle), this.options.isResizeBound && this.bindResize()
		}, g.prototype.reloadItems = function () {
			this.items = this._itemize(this.element.children)
		}, g.prototype._itemize = function (a) {
			for (var b = this._filterFindItemElements(a), c = this.constructor.Item, d = [], e = 0, f = b.length; f > e; e++) {
				var g = b[e],
					h = new c(g, this);
				d.push(h)
			}
			return d
		}, g.prototype._filterFindItemElements = function (a) {
			return e.filterFindElements(a, this.options.itemSelector)
		}, g.prototype.getItemElements = function () {
			for (var a = [], b = 0, c = this.items.length; c > b; b++) a.push(this.items[b].element);
			return a
		}, g.prototype.layout = function () {
			this._resetLayout(), this._manageStamps();
			var a = void 0 !== this.options.isLayoutInstant ? this.options.isLayoutInstant : !this._isLayoutInited;
			this.layoutItems(this.items, a), this._isLayoutInited = !0
		}, g.prototype._init = g.prototype.layout, g.prototype._resetLayout = function () {
			this.getSize()
		}, g.prototype.getSize = function () {
			this.size = d(this.element)
		}, g.prototype._getMeasurement = function (a, b) {
			var c, f = this.options[a];
			f ? ("string" == typeof f ? c = this.element.querySelector(f) : e.isElement(f) && (c = f), this[a] = c ? d(c)[b] : f) : this[a] = 0
		}, g.prototype.layoutItems = function (a, b) {
			a = this._getItemsForLayout(a), this._layoutItems(a, b), this._postLayout()
		}, g.prototype._getItemsForLayout = function (a) {
			for (var b = [], c = 0, d = a.length; d > c; c++) {
				var e = a[c];
				e.isIgnored || b.push(e)
			}
			return b
		}, g.prototype._layoutItems = function (a, b) {
			if (this._emitCompleteOnItems("layout", a), a && a.length) {
				for (var c = [], d = 0, e = a.length; e > d; d++) {
					var f = a[d],
						g = this._getItemLayoutPosition(f);
					g.item = f, g.isInstant = b || f.isLayoutInstant, c.push(g)
				}
				this._processLayoutQueue(c)
			}
		}, g.prototype._getItemLayoutPosition = function () {
			return {
				x: 0,
				y: 0
			}
		}, g.prototype._processLayoutQueue = function (a) {
			for (var b = 0, c = a.length; c > b; b++) {
				var d = a[b];
				this._positionItem(d.item, d.x, d.y, d.isInstant)
			}
		}, g.prototype._positionItem = function (a, b, c, d) {
			d ? a.goTo(b, c) : a.moveTo(b, c)
		}, g.prototype._postLayout = function () {
			this.resizeContainer()
		}, g.prototype.resizeContainer = function () {
			if (this.options.isResizingContainer) {
				var a = this._getContainerSize();
				a && (this._setContainerMeasure(a.width, !0), this._setContainerMeasure(a.height, !1))
			}
		}, g.prototype._getContainerSize = j, g.prototype._setContainerMeasure = function (a, b) {
			if (void 0 !== a) {
				var c = this.size;
				c.isBorderBox && (a += b ? c.paddingLeft + c.paddingRight + c.borderLeftWidth + c.borderRightWidth : c.paddingBottom + c.paddingTop + c.borderTopWidth + c.borderBottomWidth), a = Math.max(a, 0), this.element.style[b ? "width" : "height"] = a + "px"
			}
		}, g.prototype._emitCompleteOnItems = function (a, b) {
			function c() {
				e.dispatchEvent(a + "Complete", null, [b])
			}

			function d() {
				g++, g === f && c()
			}
			var e = this,
				f = b.length;
			if (!b || !f) return void c();
			for (var g = 0, h = 0, i = b.length; i > h; h++) {
				var j = b[h];
				j.once(a, d)
			}
		}, g.prototype.dispatchEvent = function (a, b, c) {
			var d = b ? [b].concat(c) : c;
			if (this.emitEvent(a, d), i)
				if (this.$element = this.$element || i(this.element), b) {
					var e = i.Event(b);
					e.type = a, this.$element.trigger(e, c)
				} else this.$element.trigger(a, c)
		}, g.prototype.ignore = function (a) {
			var b = this.getItem(a);
			b && (b.isIgnored = !0)
		}, g.prototype.unignore = function (a) {
			var b = this.getItem(a);
			b && delete b.isIgnored
		}, g.prototype.stamp = function (a) {
			if (a = this._find(a)) {
				this.stamps = this.stamps.concat(a);
				for (var b = 0, c = a.length; c > b; b++) {
					var d = a[b];
					this.ignore(d)
				}
			}
		}, g.prototype.unstamp = function (a) {
			if (a = this._find(a))
				for (var b = 0, c = a.length; c > b; b++) {
					var d = a[b];
					e.removeFrom(this.stamps, d), this.unignore(d)
				}
		}, g.prototype._find = function (a) {
			return a ? ("string" == typeof a && (a = this.element.querySelectorAll(a)), a = e.makeArray(a)) : void 0
		}, g.prototype._manageStamps = function () {
			if (this.stamps && this.stamps.length) {
				this._getBoundingRect();
				for (var a = 0, b = this.stamps.length; b > a; a++) {
					var c = this.stamps[a];
					this._manageStamp(c)
				}
			}
		}, g.prototype._getBoundingRect = function () {
			var a = this.element.getBoundingClientRect(),
				b = this.size;
			this._boundingRect = {
				left: a.left + b.paddingLeft + b.borderLeftWidth,
				top: a.top + b.paddingTop + b.borderTopWidth,
				right: a.right - (b.paddingRight + b.borderRightWidth),
				bottom: a.bottom - (b.paddingBottom + b.borderBottomWidth)
			}
		}, g.prototype._manageStamp = j, g.prototype._getElementOffset = function (a) {
			var b = a.getBoundingClientRect(),
				c = this._boundingRect,
				e = d(a),
				f = {
					left: b.left - c.left - e.marginLeft,
					top: b.top - c.top - e.marginTop,
					right: c.right - b.right - e.marginRight,
					bottom: c.bottom - b.bottom - e.marginBottom
				};
			return f
		}, g.prototype.handleEvent = function (a) {
			var b = "on" + a.type;
			this[b] && this[b](a)
		}, g.prototype.bindResize = function () {
			this.isResizeBound || (b.bind(a, "resize", this), this.isResizeBound = !0)
		}, g.prototype.unbindResize = function () {
			this.isResizeBound && b.unbind(a, "resize", this), this.isResizeBound = !1
		}, g.prototype.onresize = function () {
			function a() {
				b.resize(), delete b.resizeTimeout
			}
			this.resizeTimeout && clearTimeout(this.resizeTimeout);
			var b = this;
			this.resizeTimeout = setTimeout(a, 100)
		}, g.prototype.resize = function () {
			this.isResizeBound && this.needsResizeLayout() && this.layout()
		}, g.prototype.needsResizeLayout = function () {
			var a = d(this.element),
				b = this.size && a;
			return b && a.innerWidth !== this.size.innerWidth
		}, g.prototype.addItems = function (a) {
			var b = this._itemize(a);
			return b.length && (this.items = this.items.concat(b)), b
		}, g.prototype.appended = function (a) {
			var b = this.addItems(a);
			b.length && (this.layoutItems(b, !0), this.reveal(b))
		}, g.prototype.prepended = function (a) {
			var b = this._itemize(a);
			if (b.length) {
				var c = this.items.slice(0);
				this.items = b.concat(c), this._resetLayout(), this._manageStamps(), this.layoutItems(b, !0), this.reveal(b), this.layoutItems(c)
			}
		}, g.prototype.reveal = function (a) {
			this._emitCompleteOnItems("reveal", a);
			for (var b = a && a.length, c = 0; b && b > c; c++) {
				var d = a[c];
				d.reveal()
			}
		}, g.prototype.hide = function (a) {
			this._emitCompleteOnItems("hide", a);
			for (var b = a && a.length, c = 0; b && b > c; c++) {
				var d = a[c];
				d.hide()
			}
		}, g.prototype.revealItemElements = function (a) {
			var b = this.getItems(a);
			this.reveal(b)
		}, g.prototype.hideItemElements = function (a) {
			var b = this.getItems(a);
			this.hide(b)
		}, g.prototype.getItem = function (a) {
			for (var b = 0, c = this.items.length; c > b; b++) {
				var d = this.items[b];
				if (d.element === a) return d
			}
		}, g.prototype.getItems = function (a) {
			a = e.makeArray(a);
			for (var b = [], c = 0, d = a.length; d > c; c++) {
				var f = a[c],
					g = this.getItem(f);
				g && b.push(g)
			}
			return b
		}, g.prototype.remove = function (a) {
			var b = this.getItems(a);
			if (this._emitCompleteOnItems("remove", b), b && b.length)
				for (var c = 0, d = b.length; d > c; c++) {
					var f = b[c];
					f.remove(), e.removeFrom(this.items, f)
				}
		}, g.prototype.destroy = function () {
			var a = this.element.style;
			a.height = "", a.position = "", a.width = "";
			for (var b = 0, c = this.items.length; c > b; b++) {
				var d = this.items[b];
				d.destroy()
			}
			this.unbindResize();
			var e = this.element.outlayerGUID;
			delete l[e], delete this.element.outlayerGUID, i && i.removeData(this.element, this.constructor.namespace)
		}, g.data = function (a) {
			a = e.getQueryElement(a);
			var b = a && a.outlayerGUID;
			return b && l[b]
		}, g.create = function (a, b) {
			function c() {
				g.apply(this, arguments)
			}
			return Object.create ? c.prototype = Object.create(g.prototype) : e.extend(c.prototype, g.prototype), c.prototype.constructor = c, c.defaults = e.extend({}, g.defaults), e.extend(c.defaults, b), c.prototype.settings = {}, c.namespace = a, c.data = g.data, c.Item = function () {
				f.apply(this, arguments)
			}, c.Item.prototype = new f, e.htmlInit(c, a), i && i.bridget && i.bridget(a, c), c
		}, g.Item = f, g
	}),
	function (a, b) {
		"use strict";
		"function" == typeof define && define.amd ? define("isotope/js/item", ["outlayer/outlayer"], b) : "object" == typeof exports ? module.exports = b(require("outlayer")) : (a.Isotope = a.Isotope || {}, a.Isotope.Item = b(a.Outlayer))
	}(window, function (a) {
		"use strict";

		function b() {
			a.Item.apply(this, arguments)
		}
		b.prototype = new a.Item, b.prototype._create = function () {
			this.id = this.layout.itemGUID++, a.Item.prototype._create.call(this), this.sortData = {}
		}, b.prototype.updateSortData = function () {
			if (!this.isIgnored) {
				this.sortData.id = this.id, this.sortData["original-order"] = this.id, this.sortData.random = Math.random();
				var a = this.layout.options.getSortData,
					b = this.layout._sorters;
				for (var c in a) {
					var d = b[c];
					this.sortData[c] = d(this.element, this)
				}
			}
		};
		var c = b.prototype.destroy;
		return b.prototype.destroy = function () {
			c.apply(this, arguments), this.css({
				display: ""
			})
		}, b
	}),
	function (a, b) {
		"use strict";
		"function" == typeof define && define.amd ? define("isotope/js/layout-mode", ["get-size/get-size", "outlayer/outlayer"], b) : "object" == typeof exports ? module.exports = b(require("get-size"), require("outlayer")) : (a.Isotope = a.Isotope || {}, a.Isotope.LayoutMode = b(a.getSize, a.Outlayer))
	}(window, function (a, b) {
		"use strict";

		function c(a) {
			this.isotope = a, a && (this.options = a.options[this.namespace], this.element = a.element, this.items = a.filteredItems, this.size = a.size)
		}
		return function () {
			function a(a) {
				return function () {
					return b.prototype[a].apply(this.isotope, arguments)
				}
			}
			for (var d = ["_resetLayout", "_getItemLayoutPosition", "_manageStamp", "_getContainerSize", "_getElementOffset", "needsResizeLayout"], e = 0, f = d.length; f > e; e++) {
				var g = d[e];
				c.prototype[g] = a(g)
			}
		}(), c.prototype.needsVerticalResizeLayout = function () {
			var b = a(this.isotope.element),
				c = this.isotope.size && b;
			return c && b.innerHeight != this.isotope.size.innerHeight
		}, c.prototype._getMeasurement = function () {
			this.isotope._getMeasurement.apply(this, arguments)
		}, c.prototype.getColumnWidth = function () {
			this.getSegmentSize("column", "Width")
		}, c.prototype.getRowHeight = function () {
			this.getSegmentSize("row", "Height")
		}, c.prototype.getSegmentSize = function (a, b) {
			var c = a + b,
				d = "outer" + b;
			if (this._getMeasurement(c, d), !this[c]) {
				var e = this.getFirstItemSize();
				this[c] = e && e[d] || this.isotope.size["inner" + b]
			}
		}, c.prototype.getFirstItemSize = function () {
			var b = this.isotope.filteredItems[0];
			return b && b.element && a(b.element)
		}, c.prototype.layout = function () {
			this.isotope.layout.apply(this.isotope, arguments)
		}, c.prototype.getSize = function () {
			this.isotope.getSize(), this.size = this.isotope.size
		}, c.modes = {}, c.create = function (a, b) {
			function d() {
				c.apply(this, arguments)
			}
			return d.prototype = new c, b && (d.options = b), d.prototype.namespace = a, c.modes[a] = d, d
		}, c
	}),
	function (a, b) {
		"use strict";
		"function" == typeof define && define.amd ? define("masonry/masonry", ["outlayer/outlayer", "get-size/get-size", "fizzy-ui-utils/utils"], b) : "object" == typeof exports ? module.exports = b(require("outlayer"), require("get-size"), require("fizzy-ui-utils")) : a.Masonry = b(a.Outlayer, a.getSize, a.fizzyUIUtils)
	}(window, function (a, b, c) {
		var d = a.create("masonry");
		return d.prototype._resetLayout = function () {
			this.getSize(), this._getMeasurement("columnWidth", "outerWidth"), this._getMeasurement("gutter", "outerWidth"), this.measureColumns();
			var a = this.cols;
			for (this.colYs = []; a--;) this.colYs.push(0);
			this.maxY = 0
		}, d.prototype.measureColumns = function () {
			if (this.getContainerWidth(), !this.columnWidth) {
				var a = this.items[0],
					c = a && a.element;
				this.columnWidth = c && b(c).outerWidth || this.containerWidth
			}
			var d = this.columnWidth += this.gutter,
				e = this.containerWidth + this.gutter,
				f = e / d,
				g = d - e % d,
				h = g && 1 > g ? "round" : "floor";
			f = Math[h](f), this.cols = Math.max(f, 1)
		}, d.prototype.getContainerWidth = function () {
			var a = this.options.isFitWidth ? this.element.parentNode : this.element,
				c = b(a);
			this.containerWidth = c && c.innerWidth
		}, d.prototype._getItemLayoutPosition = function (a) {
			a.getSize();
			var b = a.size.outerWidth % this.columnWidth,
				d = b && 1 > b ? "round" : "ceil",
				e = Math[d](a.size.outerWidth / this.columnWidth);
			e = Math.min(e, this.cols);
			for (var f = this._getColGroup(e), g = Math.min.apply(Math, f), h = c.indexOf(f, g), i = {
					x: this.columnWidth * h,
					y: g
				}, j = g + a.size.outerHeight, k = this.cols + 1 - f.length, l = 0; k > l; l++) this.colYs[h + l] = j;
			return i
		}, d.prototype._getColGroup = function (a) {
			if (2 > a) return this.colYs;
			for (var b = [], c = this.cols + 1 - a, d = 0; c > d; d++) {
				var e = this.colYs.slice(d, d + a);
				b[d] = Math.max.apply(Math, e)
			}
			return b
		}, d.prototype._manageStamp = function (a) {
			var c = b(a),
				d = this._getElementOffset(a),
				e = this.options.isOriginLeft ? d.left : d.right,
				f = e + c.outerWidth,
				g = Math.floor(e / this.columnWidth);
			g = Math.max(0, g);
			var h = Math.floor(f / this.columnWidth);
			h -= f % this.columnWidth ? 0 : 1, h = Math.min(this.cols - 1, h);
			for (var i = (this.options.isOriginTop ? d.top : d.bottom) + c.outerHeight, j = g; h >= j; j++) this.colYs[j] = Math.max(i, this.colYs[j])
		}, d.prototype._getContainerSize = function () {
			this.maxY = Math.max.apply(Math, this.colYs);
			var a = {
				height: this.maxY
			};
			return this.options.isFitWidth && (a.width = this._getContainerFitWidth()), a
		}, d.prototype._getContainerFitWidth = function () {
			for (var a = 0, b = this.cols; --b && 0 === this.colYs[b];) a++;
			return (this.cols - a) * this.columnWidth - this.gutter
		}, d.prototype.needsResizeLayout = function () {
			var a = this.containerWidth;
			return this.getContainerWidth(), a !== this.containerWidth
		}, d
	}),
	function (a, b) {
		"use strict";
		"function" == typeof define && define.amd ? define("isotope/js/layout-modes/masonry", ["../layout-mode", "masonry/masonry"], b) : "object" == typeof exports ? module.exports = b(require("../layout-mode"), require("masonry-layout")) : b(a.Isotope.LayoutMode, a.Masonry)
	}(window, function (a, b) {
		"use strict";

		function c(a, b) {
			for (var c in b) a[c] = b[c];
			return a
		}
		var d = a.create("masonry"),
			e = d.prototype._getElementOffset,
			f = d.prototype.layout,
			g = d.prototype._getMeasurement;
		c(d.prototype, b.prototype), d.prototype._getElementOffset = e, d.prototype.layout = f, d.prototype._getMeasurement = g;
		var h = d.prototype.measureColumns;
		d.prototype.measureColumns = function () {
			this.items = this.isotope.filteredItems, h.call(this)
		};
		var i = d.prototype._manageStamp;
		return d.prototype._manageStamp = function () {
			this.options.isOriginLeft = this.isotope.options.isOriginLeft, this.options.isOriginTop = this.isotope.options.isOriginTop, i.apply(this, arguments)
		}, d
	}),
	function (a, b) {
		"use strict";
		"function" == typeof define && define.amd ? define("isotope/js/layout-modes/fit-rows", ["../layout-mode"], b) : "object" == typeof exports ? module.exports = b(require("../layout-mode")) : b(a.Isotope.LayoutMode)
	}(window, function (a) {
		"use strict";
		var b = a.create("fitRows");
		return b.prototype._resetLayout = function () {
			this.x = 0, this.y = 0, this.maxY = 0, this._getMeasurement("gutter", "outerWidth")
		}, b.prototype._getItemLayoutPosition = function (a) {
			a.getSize();
			var b = a.size.outerWidth + this.gutter,
				c = this.isotope.size.innerWidth + this.gutter;
			0 !== this.x && b + this.x > c && (this.x = 0, this.y = this.maxY);
			var d = {
				x: this.x,
				y: this.y
			};
			return this.maxY = Math.max(this.maxY, this.y + a.size.outerHeight), this.x += b, d
		}, b.prototype._getContainerSize = function () {
			return {
				height: this.maxY
			}
		}, b
	}),
	function (a, b) {
		"use strict";
		"function" == typeof define && define.amd ? define("isotope/js/layout-modes/vertical", ["../layout-mode"], b) : "object" == typeof exports ? module.exports = b(require("../layout-mode")) : b(a.Isotope.LayoutMode)
	}(window, function (a) {
		"use strict";
		var b = a.create("vertical", {
			horizontalAlignment: 0
		});
		return b.prototype._resetLayout = function () {
			this.y = 0
		}, b.prototype._getItemLayoutPosition = function (a) {
			a.getSize();
			var b = (this.isotope.size.innerWidth - a.size.outerWidth) * this.options.horizontalAlignment,
				c = this.y;
			return this.y += a.size.outerHeight, {
				x: b,
				y: c
			}
		}, b.prototype._getContainerSize = function () {
			return {
				height: this.y
			}
		}, b
	}),
	function (a, b) {
		"use strict";
		"function" == typeof define && define.amd ? define(["outlayer/outlayer", "get-size/get-size", "matches-selector/matches-selector", "fizzy-ui-utils/utils", "isotope/js/item", "isotope/js/layout-mode", "isotope/js/layout-modes/masonry", "isotope/js/layout-modes/fit-rows", "isotope/js/layout-modes/vertical"], function (c, d, e, f, g, h) {
			return b(a, c, d, e, f, g, h)
		}) : "object" == typeof exports ? module.exports = b(a, require("outlayer"), require("get-size"), require("desandro-matches-selector"), require("fizzy-ui-utils"), require("./item"), require("./layout-mode"), require("./layout-modes/masonry"), require("./layout-modes/fit-rows"), require("./layout-modes/vertical")) : a.Isotope = b(a, a.Outlayer, a.getSize, a.matchesSelector, a.fizzyUIUtils, a.Isotope.Item, a.Isotope.LayoutMode)
	}(window, function (a, b, c, d, e, f, g) {
		function h(a, b) {
			return function (c, d) {
				for (var e = 0, f = a.length; f > e; e++) {
					var g = a[e],
						h = c.sortData[g],
						i = d.sortData[g];
					if (h > i || i > h) {
						var j = void 0 !== b[g] ? b[g] : b,
							k = j ? 1 : -1;
						return (h > i ? 1 : -1) * k
					}
				}
				return 0
			}
		}
		var i = a.jQuery,
			j = String.prototype.trim ? function (a) {
				return a.trim()
			} : function (a) {
				return a.replace(/^\s+|\s+$/g, "")
			},
			k = document.documentElement,
			l = k.textContent ? function (a) {
				return a.textContent
			} : function (a) {
				return a.innerText
			},
			m = b.create("isotope", {
				layoutMode: "masonry",
				isJQueryFiltering: !0,
				sortAscending: !0
			});
		m.Item = f, m.LayoutMode = g, m.prototype._create = function () {
			this.itemGUID = 0, this._sorters = {}, this._getSorters(), b.prototype._create.call(this), this.modes = {}, this.filteredItems = this.items, this.sortHistory = ["original-order"];
			for (var a in g.modes) this._initLayoutMode(a)
		}, m.prototype.reloadItems = function () {
			this.itemGUID = 0, b.prototype.reloadItems.call(this)
		}, m.prototype._itemize = function () {
			for (var a = b.prototype._itemize.apply(this, arguments), c = 0, d = a.length; d > c; c++) {
				var e = a[c];
				e.id = this.itemGUID++
			}
			return this._updateItemsSortData(a), a
		}, m.prototype._initLayoutMode = function (a) {
			var b = g.modes[a],
				c = this.options[a] || {};
			this.options[a] = b.options ? e.extend(b.options, c) : c, this.modes[a] = new b(this)
		}, m.prototype.layout = function () {
			return !this._isLayoutInited && this.options.isInitLayout ? void this.arrange() : void this._layout()
		}, m.prototype._layout = function () {
			var a = this._getIsInstant();
			this._resetLayout(), this._manageStamps(), this.layoutItems(this.filteredItems, a), this._isLayoutInited = !0
		}, m.prototype.arrange = function (a) {
			function b() {
				d.reveal(c.needReveal), d.hide(c.needHide)
			}
			this.option(a), this._getIsInstant();
			var c = this._filter(this.items);
			this.filteredItems = c.matches;
			var d = this;
			this._bindArrangeComplete(), this._isInstant ? this._noTransition(b) : b(), this._sort(), this._layout()
		}, m.prototype._init = m.prototype.arrange, m.prototype._getIsInstant = function () {
			var a = void 0 !== this.options.isLayoutInstant ? this.options.isLayoutInstant : !this._isLayoutInited;
			return this._isInstant = a, a
		}, m.prototype._bindArrangeComplete = function () {
			function a() {
				b && c && d && e.dispatchEvent("arrangeComplete", null, [e.filteredItems])
			}
			var b, c, d, e = this;
			this.once("layoutComplete", function () {
				b = !0, a()
			}), this.once("hideComplete", function () {
				c = !0, a()
			}), this.once("revealComplete", function () {
				d = !0, a()
			})
		}, m.prototype._filter = function (a) {
			var b = this.options.filter;
			b = b || "*";
			for (var c = [], d = [], e = [], f = this._getFilterTest(b), g = 0, h = a.length; h > g; g++) {
				var i = a[g];
				if (!i.isIgnored) {
					var j = f(i);
					j && c.push(i), j && i.isHidden ? d.push(i) : j || i.isHidden || e.push(i)
				}
			}
			return {
				matches: c,
				needReveal: d,
				needHide: e
			}
		}, m.prototype._getFilterTest = function (a) {
			return i && this.options.isJQueryFiltering ? function (b) {
				return i(b.element).is(a)
			} : "function" == typeof a ? function (b) {
				return a(b.element)
			} : function (b) {
				return d(b.element, a)
			}
		}, m.prototype.updateSortData = function (a) {
			var b;
			a ? (a = e.makeArray(a), b = this.getItems(a)) : b = this.items, this._getSorters(), this._updateItemsSortData(b)
		}, m.prototype._getSorters = function () {
			var a = this.options.getSortData;
			for (var b in a) {
				var c = a[b];
				this._sorters[b] = n(c)
			}
		}, m.prototype._updateItemsSortData = function (a) {
			for (var b = a && a.length, c = 0; b && b > c; c++) {
				var d = a[c];
				d.updateSortData()
			}
		};
		var n = function () {
			function a(a) {
				if ("string" != typeof a) return a;
				var c = j(a).split(" "),
					d = c[0],
					e = d.match(/^\[(.+)\]$/),
					f = e && e[1],
					g = b(f, d),
					h = m.sortDataParsers[c[1]];
				return a = h ? function (a) {
					return a && h(g(a))
				} : function (a) {
					return a && g(a)
				}
			}

			function b(a, b) {
				var c;
				return c = a ? function (b) {
					return b.getAttribute(a)
				} : function (a) {
					var c = a.querySelector(b);
					return c && l(c)
				}
			}
			return a
		}();
		m.sortDataParsers = {
			parseInt: function (a) {
				return parseInt(a, 10)
			},
			parseFloat: function (a) {
				return parseFloat(a)
			}
		}, m.prototype._sort = function () {
			var a = this.options.sortBy;
			if (a) {
				var b = [].concat.apply(a, this.sortHistory),
					c = h(b, this.options.sortAscending);
				this.filteredItems.sort(c), a != this.sortHistory[0] && this.sortHistory.unshift(a)
			}
		}, m.prototype._mode = function () {
			var a = this.options.layoutMode,
				b = this.modes[a];
			if (!b) throw new Error("No layout mode: " + a);
			return b.options = this.options[a], b
		}, m.prototype._resetLayout = function () {
			b.prototype._resetLayout.call(this), this._mode()._resetLayout()
		}, m.prototype._getItemLayoutPosition = function (a) {
			return this._mode()._getItemLayoutPosition(a)
		}, m.prototype._manageStamp = function (a) {
			this._mode()._manageStamp(a)
		}, m.prototype._getContainerSize = function () {
			return this._mode()._getContainerSize()
		}, m.prototype.needsResizeLayout = function () {
			return this._mode().needsResizeLayout()
		}, m.prototype.appended = function (a) {
			var b = this.addItems(a);
			if (b.length) {
				var c = this._filterRevealAdded(b);
				this.filteredItems = this.filteredItems.concat(c)
			}
		}, m.prototype.prepended = function (a) {
			var b = this._itemize(a);
			if (b.length) {
				this._resetLayout(), this._manageStamps();
				var c = this._filterRevealAdded(b);
				this.layoutItems(this.filteredItems), this.filteredItems = c.concat(this.filteredItems), this.items = b.concat(this.items)
			}
		}, m.prototype._filterRevealAdded = function (a) {
			var b = this._filter(a);
			return this.hide(b.needHide), this.reveal(b.matches), this.layoutItems(b.matches, !0), b.matches
		}, m.prototype.insert = function (a) {
			var b = this.addItems(a);
			if (b.length) {
				var c, d, e = b.length;
				for (c = 0; e > c; c++) d = b[c], this.element.appendChild(d.element);
				var f = this._filter(b).matches;
				for (c = 0; e > c; c++) b[c].isLayoutInstant = !0;
				for (this.arrange(), c = 0; e > c; c++) delete b[c].isLayoutInstant;
				this.reveal(f)
			}
		};
		var o = m.prototype.remove;
		return m.prototype.remove = function (a) {
			a = e.makeArray(a);
			var b = this.getItems(a);
			o.call(this, a);
			var c = b && b.length;
			if (c)
				for (var d = 0; c > d; d++) {
					var f = b[d];
					e.removeFrom(this.filteredItems, f)
				}
		}, m.prototype.shuffle = function () {
			for (var a = 0, b = this.items.length; b > a; a++) {
				var c = this.items[a];
				c.sortData.random = Math.random()
			}
			this.options.sortBy = "random", this._sort(), this._layout()
		}, m.prototype._noTransition = function (a) {
			var b = this.options.transitionDuration;
			this.options.transitionDuration = 0;
			var c = a.call(this);
			return this.options.transitionDuration = b, c
		}, m.prototype.getFilteredItemElements = function () {
			for (var a = [], b = 0, c = this.filteredItems.length; c > b; b++) a.push(this.filteredItems[b].element);
			return a
		}, m
	});;
(function (window, factory) {
	'use strict';
	if (typeof define == 'function' && define.amd) {
		define(['../layout-mode'], factory);
	} else if (typeof exports == 'object') {
		module.exports = factory(require('../layout-mode'));
	} else {
		factory(window.Isotope.LayoutMode);
	}
}(window, function factory(LayoutMode) {
	'use strict';
	var Metro = LayoutMode.create('metro');
	Metro.prototype._resetLayout = function () {
		this.x = 0;
		this.y = 0;
		this.maxY = 0;
		this._getMeasurement('gutter', 'outerWidth');
		this.perColumn = 0;
		this.rows = [];
		this.rowIndex = 0;
		var container = this.options.isFitWidth ? this.element.parentNode : this.element;
		this.targetHeight = parseFloat(jQuery(container).data('max-row-height')) || 380;
		this.minHeight = 210;
		if (this.minHeight > this.targetHeight) {
			this.minHeight = this.targetHeight * 0.8;
		}
		this.getColumnCount();
		this.initItems();
		this.buildRows();
	};
	Metro.prototype.getRowItemsHeight = function (rowItems, item, minHeight) {
		var rowItemsInnerWidth = 0;
		var rowItemsOuterWidth = 0;
		rowItems.push(item);
		rowItems.forEach(function (item, index) {
			if (item.imageHeight > minHeight) {
				var relHeight = minHeight / item.imageHeight;
			} else {
				var relHeight = 1;
			}
			rowItemsInnerWidth += item.size.innerWidth * relHeight;
			rowItemsOuterWidth += item.size.innerWidth * relHeight + (item.size.outerWidth - item.size.innerWidth);
		});
		if (rowItemsOuterWidth > this.containerWidth) {
			return (this.containerWidth - (rowItemsOuterWidth - rowItemsInnerWidth)) * minHeight / rowItemsInnerWidth;
		} else {
			return minHeight;
		}
	};
	Metro.prototype.checkRowItemsFull = function (rowItems) {
		var rowItemsMinHeight = -1;
		rowItems.forEach(function (item, index) {
			if (rowItemsMinHeight == -1 || item.imageHeight < rowItemsMinHeight) {
				rowItemsMinHeight = item.imageHeight;
			}
		});
		if (rowItemsMinHeight == -1) {
			return false;
		}
		var rowItemsHeight = this.getRowItemsHeight(rowItems.slice(0, rowItems.length - 1), rowItems.slice(-1)[0], rowItemsMinHeight);
		if (rowItemsHeight < this.targetHeight * 1.3) {
			var rowItemsInnerWidth = 0;
			var rowItemsOuterWidth = 0;
			rowItems.forEach(function (item, index) {
				if (item.imageHeight > rowItemsHeight) {
					var relHeight = rowItemsHeight / item.imageHeight;
				} else {
					var relHeight = 1;
				}
				rowItemsInnerWidth += item.size.innerWidth * relHeight;
				rowItemsOuterWidth += item.size.innerWidth * relHeight + (item.size.outerWidth - item.size.innerWidth);
			});
			return {
				items: rowItems,
				innerWidth: rowItemsInnerWidth,
				outerWidth: rowItemsOuterWidth,
				height: rowItemsHeight
			};
		}
		return false;
	};
	Metro.prototype.getRowItems = function (targetHeight) {
		var rowItems = [];
		var rowItemsInnerWidth = 0;
		var rowItemsOuterWidth = 0;
		var defaultItemIndex = this.itemIndex;
		var minHeight = targetHeight;
		while ((rowItemsOuterWidth < this.containerWidth || rowItems.length < this.perColumn) && this.itemIndex < this.items.length) {
			var item = this.items[this.itemIndex];
			item.getSize();
			if (rowItems.length >= this.perColumn) {
				var canSizeResult = this.checkRowItemsFull(rowItems);
				if (canSizeResult) {
					return canSizeResult;
				}
			}
			var rowHeight = this.getRowItemsHeight(rowItems.slice(0), item, minHeight);
			if (rowHeight < this.minHeight && rowItems.length > 0) {
				var canSizeResult = this.checkRowItemsFull(rowItems);
				if (canSizeResult) {
					return canSizeResult;
				}
				return {
					items: rowItems,
					innerWidth: rowItemsInnerWidth,
					outerWidth: rowItemsOuterWidth,
					height: targetHeight
				};
			}
			if (item.imageHeight > targetHeight) {
				var relHeight = targetHeight / item.imageHeight;
			} else {
				var relHeight = 1;
			}
			rowItemsInnerWidth += item.size.innerWidth * relHeight;
			rowItemsOuterWidth += item.size.innerWidth * relHeight + (item.size.outerWidth - item.size.innerWidth);
			if (minHeight > item.imageHeight) {
				minHeight = item.imageHeight;
			}
			rowItems.push(item);
			this.itemIndex += 1;
			if (minHeight < targetHeight) {
				this.itemIndex = defaultItemIndex;
				return this.getRowItems(minHeight);
			}
		}
		if (rowItemsOuterWidth < this.containerWidth) {
			var canSizeResult = this.checkRowItemsFull(rowItems);
			if (canSizeResult) {
				return canSizeResult;
			}
		}
		return {
			items: rowItems,
			innerWidth: rowItemsInnerWidth,
			outerWidth: rowItemsOuterWidth,
			height: targetHeight
		};
	}
	Metro.prototype.buildRows = function () {
		var self = this;
		var container = this.options.isFitWidth ? this.element.parentNode : this.element;
		jQuery(this.isotope.options.itemSelector + ' .caption', container).hide();
		this.getContainerWidth();
		this.itemIndex = 0;
		while (this.itemIndex < this.items.length) {
			var targetHeight = this.targetHeight;
			var rowItems = this.getRowItems(this.targetHeight);
			if (rowItems.outerWidth > this.containerWidth) {
				var height = (this.containerWidth - (rowItems.outerWidth - rowItems.innerWidth)) * rowItems.height / rowItems.innerWidth;
			} else {
				var height = rowItems.height;
			}
			rowItems.items.forEach(function (item, index) {
				var rel = 1;
				if (item.imageHeight > height) {
					rel = height / item.imageHeight;
				}
				var calculatedWidth = Math.round(item.size.innerWidth * rel - 0.5);
				calculatedWidth += (item.size.outerWidth - item.size.innerWidth);
				jQuery(item.element).css('width', calculatedWidth);
			});
		}
		jQuery(this.isotope.options.itemSelector + ' .caption', container).show();
	};
	Metro.prototype.initItems = function () {
		var self = this;
		this.items = this.isotope.filteredItems;
		var container = this.options.isFitWidth ? this.element.parentNode : this.element;
		this.items.forEach(function (item, index) {
			if (item.imageHeight == null || item.imageHeight == undefined) {
				self.items[index].imageWidth = parseInt(jQuery(self.isotope.options.itemImageWrapperSelector + ' img', item.element).attr('width'));
				self.items[index].imageHeight = parseInt(jQuery(self.isotope.options.itemImageWrapperSelector + ' img', item.element).attr('height'));
				if (isNaN(self.items[index].imageHeight)) {
					self.items[index].imageHeight = self.targetHeight;
				}
			}
			var $element = jQuery(item.element);
			if (!$element.data('original-width')) {
				var original = self.items[index].imageWidth / 1.1;
				var padding = parseFloat($element.css('padding-left'));
				if (isNaN(padding)) {
					padding = 0;
				}
				$element.data('original-width', original + 2 * padding);
			}
			$element.css('width', $element.data('original-width'));
		});
	};
	Metro.prototype.getColumnCount = function () {
		var container = this.options.isFitWidth ? this.element.parentNode : this.element;
		if (this.isotope.options.itemSelector == '.portfolio-item') {
			var classes = jQuery(container).closest('.portfolio').attr('class');
		} else {
			var classes = jQuery(container).closest('.gem-gallery-grid').attr('class');
		}
		var m = classes.match(/columns-(\d)/);
		if (m) {
			this.perColumn = parseInt(m[1]);
		} else {
			this.perColumn = 4;
		}
	};
	Metro.prototype.getContainerWidth = function () {
		var container = this.options.isFitWidth ? this.element.parentNode : this.element;
		var size = getSize(container);
		this.containerWidth = size && size.innerWidth;
	};
	Metro.prototype._getItemLayoutPosition = function (item) {
		item.getSize();
		var itemWidth = item.size.outerWidth + this.gutter;
		var containerWidth = this.isotope.size.innerWidth + this.gutter;
		if (this.x !== 0 && itemWidth + this.x > containerWidth) {
			this.x = 0;
			this.y = this.maxY;
		}
		var position = {
			x: this.x,
			y: this.y
		};
		this.maxY = Math.max(this.maxY, this.y + item.size.outerHeight);
		this.x += itemWidth;
		return position;
	};
	Metro.prototype._getContainerSize = function () {
		return {
			height: this.maxY
		};
	};
	return Metro;
}));;
/*!
 * Masonry layout mode
 * sub-classes Masonry
 * http://masonry.desandro.com
 */
(function (window, factory) {
	'use strict';
	if (typeof define == 'function' && define.amd) {
		define(['../layout-mode', 'masonry/masonry'], factory);
	} else if (typeof exports == 'object') {
		module.exports = factory(require('../layout-mode'), require('masonry-layout'));
	} else {
		factory(window.Isotope.LayoutMode, window.Masonry);
	}
}(window, function factory(LayoutMode, Masonry) {
	'use strict';

	function extend(a, b) {
		for (var prop in b) {
			a[prop] = b[prop];
		}
		return a;
	}
	var MasonryMode = LayoutMode.create('masonry-custom');
	var _getElementOffset = MasonryMode.prototype._getElementOffset;
	var layout = MasonryMode.prototype.layout;
	var _getMeasurement = MasonryMode.prototype._getMeasurement;
	extend(MasonryMode.prototype, Masonry.prototype);
	MasonryMode.prototype._getElementOffset = _getElementOffset;
	MasonryMode.prototype.layout = layout;
	MasonryMode.prototype._getMeasurement = _getMeasurement;
	var measureColumns = MasonryMode.prototype.measureColumns;
	MasonryMode.prototype.measureColumns = function () {
		this.items = this.isotope.filteredItems;
		measureColumns.call(this);
	};
	var _manageStamp = MasonryMode.prototype._manageStamp;
	MasonryMode.prototype._manageStamp = function () {
		this.options.isOriginLeft = this.isotope.options.isOriginLeft;
		this.options.isOriginTop = this.isotope.options.isOriginTop;
		_manageStamp.apply(this, arguments);
	};

	function getStyle(elem) {
		return window.getComputedStyle ? getComputedStyle(elem, "") : elem.currentStyle;
	}
	MasonryMode.prototype.fix_images_height = function () {
		var self = this;
		var $ = jQuery;
		var container = this.options.isFitWidth ? this.element.parentNode : this.element;
		var $set = $(container);
		var max_heigth = 0;
		var padding = parseInt($(self.isotope.options.itemSelector, $set).not('.double-item').first().css('padding-top'));
		var caption = 0;
		if (self.isotope.options.itemSelector == '.portfolio-item') {
			if ($(self.isotope.options.itemSelector, $set).not('.double-item').first().find('.wrap > .caption').is(':visible')) {
				caption = parseInt($(self.isotope.options.itemSelector, $set).not('.double-item').first().find('.wrap > .caption').outerHeight());
			}
		}
		var fix_caption = false;
		$(self.isotope.options.itemSelector, $set).not('.double-item').each(function () {
			var height = parseFloat(getStyle($(self.isotope.options.itemImageWrapperSelector, this)[0]).height);
			var diff = height - parseInt(height);
			if (diff < 0.5) {
				height = parseInt(height);
			}
			if ((height - parseInt(height)) > 0.5) {
				height = parseInt(height + 0.5);
				fix_caption = true;
			}
			if (height > max_heigth) {
				max_heigth = height;
			}
		});
		if (caption > 0 && fix_caption) {
			caption -= 1;
		}
		$(self.isotope.options.itemSelector + '.double-item-horizontal ' + self.isotope.options.itemImageWrapperSelector + ', ' + self.isotope.options.itemSelector + '.double-item-vertical ' + self.isotope.options.itemImageWrapperSelector, $set).css('height', '');
		$(self.isotope.options.itemSelector + '.double-item-horizontal', $set).each(function () {
			var height = $(self.isotope.options.itemImageWrapperSelector, this).height();
			if (height > max_heigth) {
				$(self.isotope.options.itemImageWrapperSelector, this).height(max_heigth);
			}
		});
		$(self.isotope.options.itemSelector + '.double-item-vertical', $set).each(function () {
			var height = $(self.isotope.options.itemImageWrapperSelector, this).height();
			var calc_height = 2 * (max_heigth + padding) + caption;
			if (height > calc_height) {
				$(self.isotope.options.itemImageWrapperSelector, this).height(calc_height);
			}
		});
	}
	var _resetLayout = MasonryMode.prototype._resetLayout;
	MasonryMode.prototype._resetLayout = function () {
		if (this.isotope.options.fixHeightDoubleItems) {
			this.fix_images_height();
		}
		_resetLayout.apply(this, arguments);
	};
	return MasonryMode;
}));;
(function (factory) {
	if (typeof define !== 'undefined' && define.amd) {
		define([], factory);
	} else if (typeof module !== 'undefined' && module.exports) {
		module.exports = factory();
	} else {
		window.scrollMonitor = factory();
	}
})(function () {
	var scrollTop = function () {
		return window.pageYOffset || (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop;
	};
	var exports = {};
	var watchers = [];
	var VISIBILITYCHANGE = 'visibilityChange';
	var ENTERVIEWPORT = 'enterViewport';
	var FULLYENTERVIEWPORT = 'fullyEnterViewport';
	var EXITVIEWPORT = 'exitViewport';
	var PARTIALLYEXITVIEWPORT = 'partiallyExitViewport';
	var LOCATIONCHANGE = 'locationChange';
	var STATECHANGE = 'stateChange';
	var eventTypes = [VISIBILITYCHANGE, ENTERVIEWPORT, FULLYENTERVIEWPORT, EXITVIEWPORT, PARTIALLYEXITVIEWPORT, LOCATIONCHANGE, STATECHANGE];
	var defaultOffsets = {
		top: 0,
		bottom: 0
	};
	var getViewportHeight = function () {
		return window.innerHeight || document.documentElement.clientHeight;
	};
	var getDocumentHeight = function () {
		return Math.max(document.body.scrollHeight, document.documentElement.scrollHeight, document.body.offsetHeight, document.documentElement.offsetHeight, document.documentElement.clientHeight);
	};
	exports.viewportTop = null;
	exports.viewportBottom = null;
	exports.documentHeight = null;
	exports.viewportHeight = getViewportHeight();
	var previousDocumentHeight;
	var latestEvent;
	var calculateViewportI;

	function calculateViewport() {
		exports.viewportTop = scrollTop();
		exports.viewportBottom = exports.viewportTop + exports.viewportHeight;
		exports.documentHeight = getDocumentHeight();
		if (exports.documentHeight !== previousDocumentHeight) {
			calculateViewportI = watchers.length;
			while (calculateViewportI--) {
				watchers[calculateViewportI].recalculateLocation();
			}
			previousDocumentHeight = exports.documentHeight;
		}
	}

	function recalculateWatchLocationsAndTrigger() {
		exports.viewportHeight = getViewportHeight();
		calculateViewport();
		updateAndTriggerWatchers();
	}
	var recalculateAndTriggerTimer;

	function debouncedRecalcuateAndTrigger() {
		clearTimeout(recalculateAndTriggerTimer);
		recalculateAndTriggerTimer = setTimeout(recalculateWatchLocationsAndTrigger, 100);
	}
	var updateAndTriggerWatchersI;

	function updateAndTriggerWatchers() {
		updateAndTriggerWatchersI = watchers.length;
		var i = 0;
		while (i < updateAndTriggerWatchersI) {
			watchers[i].update();
			i++;
		}
		updateAndTriggerWatchersI = watchers.length;
		var j = 0,
			newLength;
		while (j < updateAndTriggerWatchersI) {
			watchers[j].triggerCallbacks();
			newLength = watchers.length;
			if (newLength < updateAndTriggerWatchersI) {
				j -= updateAndTriggerWatchersI - newLength;
			}
			updateAndTriggerWatchersI = newLength;
			j++;
		}
	}

	function ElementWatcher(watchItem, offsets) {
		var self = this;
		this.watchItem = watchItem;
		this.uid = Math.random().toString(36).substring(8);
		if (!offsets) {
			this.offsets = defaultOffsets;
		} else if (offsets === +offsets) {
			this.offsets = {
				top: offsets,
				bottom: offsets
			};
		} else {
			this.offsets = {
				top: offsets.top || defaultOffsets.top,
				bottom: offsets.bottom || defaultOffsets.bottom
			};
		}
		this.callbacks = {};
		for (var i = 0, j = eventTypes.length; i < j; i++) {
			self.callbacks[eventTypes[i]] = [];
		}
		this.locked = false;
		var wasInViewport;
		var wasFullyInViewport;
		var wasAboveViewport;
		var wasBelowViewport;
		var listenerToTriggerListI;
		var listener;

		function triggerCallbackArray(listeners) {
			if (listeners.length === 0) {
				return;
			}
			listenerToTriggerListI = listeners.length;
			while (listenerToTriggerListI--) {
				listener = listeners[listenerToTriggerListI];
				listener.callback.call(self, latestEvent);
				if (listener.isOne) {
					listeners.splice(listenerToTriggerListI, 1);
				}
			}
		}
		this.triggerCallbacks = function triggerCallbacks() {
			if (this.isInViewport && !wasInViewport) {
				triggerCallbackArray(this.callbacks[ENTERVIEWPORT]);
			}
			if (this.isFullyInViewport && !wasFullyInViewport) {
				triggerCallbackArray(this.callbacks[FULLYENTERVIEWPORT]);
			}
			if (this.isAboveViewport !== wasAboveViewport && this.isBelowViewport !== wasBelowViewport) {
				triggerCallbackArray(this.callbacks[VISIBILITYCHANGE]);
				if (!wasFullyInViewport && !this.isFullyInViewport) {
					triggerCallbackArray(this.callbacks[FULLYENTERVIEWPORT]);
					triggerCallbackArray(this.callbacks[PARTIALLYEXITVIEWPORT]);
				}
				if (!wasInViewport && !this.isInViewport) {
					triggerCallbackArray(this.callbacks[ENTERVIEWPORT]);
					triggerCallbackArray(this.callbacks[EXITVIEWPORT]);
				}
			}
			if (!this.isFullyInViewport && wasFullyInViewport) {
				triggerCallbackArray(this.callbacks[PARTIALLYEXITVIEWPORT]);
			}
			if (!this.isInViewport && wasInViewport) {
				triggerCallbackArray(this.callbacks[EXITVIEWPORT]);
			}
			if (this.isInViewport !== wasInViewport) {
				triggerCallbackArray(this.callbacks[VISIBILITYCHANGE]);
			}
			switch (true) {
				case wasInViewport !== this.isInViewport:
				case wasFullyInViewport !== this.isFullyInViewport:
				case wasAboveViewport !== this.isAboveViewport:
				case wasBelowViewport !== this.isBelowViewport:
					triggerCallbackArray(this.callbacks[STATECHANGE]);
			}
			wasInViewport = this.isInViewport;
			wasFullyInViewport = this.isFullyInViewport;
			wasAboveViewport = this.isAboveViewport;
			wasBelowViewport = this.isBelowViewport;
		};
		this.recalculateLocation = function () {
			if (this.locked) {
				return;
			}
			var previousTop = this.top;
			var previousBottom = this.bottom;
			if (this.watchItem.nodeName) {
				var cachedDisplay = this.watchItem.style.display;
				if (cachedDisplay === 'none') {
					this.watchItem.style.display = '';
				}
				var boundingRect = this.watchItem.getBoundingClientRect();
				this.top = boundingRect.top + exports.viewportTop;
				this.bottom = boundingRect.bottom + exports.viewportTop;
				if (cachedDisplay === 'none') {
					this.watchItem.style.display = cachedDisplay;
				}
			} else if (this.watchItem === +this.watchItem) {
				if (this.watchItem > 0) {
					this.top = this.bottom = this.watchItem;
				} else {
					this.top = this.bottom = exports.documentHeight - this.watchItem;
				}
			} else {
				this.top = this.watchItem.top;
				this.bottom = this.watchItem.bottom;
			}
			this.top -= this.offsets.top;
			this.bottom += this.offsets.bottom;
			this.height = this.bottom - this.top;
			if ((previousTop !== undefined || previousBottom !== undefined) && (this.top !== previousTop || this.bottom !== previousBottom)) {
				triggerCallbackArray(this.callbacks[LOCATIONCHANGE]);
			}
		};
		this.recalculateLocation();
		this.update();
		wasInViewport = this.isInViewport;
		wasFullyInViewport = this.isFullyInViewport;
		wasAboveViewport = this.isAboveViewport;
		wasBelowViewport = this.isBelowViewport;
	}
	ElementWatcher.prototype = {
		on: function (event, callback, isOne) {
			switch (true) {
				case event === VISIBILITYCHANGE && !this.isInViewport && this.isAboveViewport:
				case event === ENTERVIEWPORT && this.isInViewport:
				case event === FULLYENTERVIEWPORT && this.isFullyInViewport:
				case event === EXITVIEWPORT && this.isAboveViewport && !this.isInViewport:
				case event === PARTIALLYEXITVIEWPORT && this.isAboveViewport:
					callback.call(this, latestEvent);
					if (isOne) {
						return;
					}
			}
			if (this.callbacks[event]) {
				this.callbacks[event].push({
					callback: callback,
					isOne: isOne || false
				});
			} else {
				throw new Error('Tried to add a scroll monitor listener of type ' + event + '. Your options are: ' + eventTypes.join(', '));
			}
		},
		off: function (event, callback) {
			if (this.callbacks[event]) {
				for (var i = 0, item; item = this.callbacks[event][i]; i++) {
					if (item.callback === callback) {
						this.callbacks[event].splice(i, 1);
						break;
					}
				}
			} else {
				throw new Error('Tried to remove a scroll monitor listener of type ' + event + '. Your options are: ' + eventTypes.join(', '));
			}
		},
		one: function (event, callback) {
			this.on(event, callback, true);
		},
		recalculateSize: function () {
			this.height = this.watchItem.offsetHeight + this.offsets.top + this.offsets.bottom;
			this.bottom = this.top + this.height;
		},
		update: function () {
			this.isAboveViewport = this.top < exports.viewportTop;
			this.isBelowViewport = this.bottom > exports.viewportBottom;
			this.isInViewport = (this.top <= exports.viewportBottom && this.bottom >= exports.viewportTop);
			this.isFullyInViewport = (this.top >= exports.viewportTop && this.bottom <= exports.viewportBottom) || (this.isAboveViewport && this.isBelowViewport);
		},
		destroy: function () {
			var index = -1;
			self = this;
			for (var i = 0; i < watchers.length; i++) {
				if (this.uid == watchers[i].uid) {
					index = i;
					break;
				}
			}
			if (index == -1) {
				index = watchers.indexOf(this);
			}
			watchers.splice(index, 1);
			for (var i = 0, j = eventTypes.length; i < j; i++) {
				self.callbacks[eventTypes[i]].length = 0;
			}
		},
		lock: function () {
			this.locked = true;
		},
		unlock: function () {
			this.locked = false;
		}
	};
	var eventHandlerFactory = function (type) {
		return function (callback, isOne) {
			this.on.call(this, type, callback, isOne);
		};
	};
	for (var i = 0, j = eventTypes.length; i < j; i++) {
		var type = eventTypes[i];
		ElementWatcher.prototype[type] = eventHandlerFactory(type);
	}
	try {
		calculateViewport();
	} catch (e) {
		try {
			window.$(calculateViewport);
		} catch (e) {
			throw new Error('If you must put scrollMonitor in the <head>, you must use jQuery.');
		}
	}

	function scrollMonitorListener(event) {
		latestEvent = event;
		calculateViewport();
		updateAndTriggerWatchers();
	}
	if (window.addEventListener) {
		window.addEventListener('scroll', scrollMonitorListener);
		window.addEventListener('resize', debouncedRecalcuateAndTrigger);
	} else {
		window.attachEvent('onscroll', scrollMonitorListener);
		window.attachEvent('onresize', debouncedRecalcuateAndTrigger);
	}
	exports.beget = exports.create = function (element, offsets) {
		if (typeof element === 'string') {
			element = document.querySelector(element);
		} else if (element && element.length > 0) {
			element = element[0];
		}
		var watcher = new ElementWatcher(element, offsets);
		watchers.push(watcher);
		watcher.update();
		return watcher;
	};
	exports.update = function () {
		latestEvent = null;
		calculateViewport();
		updateAndTriggerWatchers();
	};
	exports.recalculateLocations = function () {
		exports.documentHeight = 0;
		exports.update();
	};
	return exports;
});;
(function ($) {
	var animations = {
		'move-up': {
			timeout: 200
		},
		bounce: {
			timeout: 0
		},
		'fade-in': {
			timeout: 100
		},
		scale: {
			timeout: 100
		},
		flip: {
			timeout: 100
		},
		'fall-perspective': {
			timeout: 100
		},
	};
	var prefixes = 'Webkit Moz ms Ms O'.split(' ');
	var docElemStyle = document.documentElement.style;

	function getStyleProperty(propName) {
		if (!propName) {
			return;
		}
		if (typeof docElemStyle[propName] === 'string') {
			return propName;
		}
		propName = propName.charAt(0).toUpperCase() + propName.slice(1);
		var prefixed;
		for (var i = 0, len = prefixes.length; i < len; i++) {
			prefixed = prefixes[i] + propName;
			if (typeof docElemStyle[prefixed] === 'string') {
				return prefixed;
			}
		}
	}
	var transitionProperty = getStyleProperty('transition');
	var transitionEndEvent = {
		WebkitTransition: 'webkitTransitionEnd',
		MozTransition: 'transitionend',
		OTransition: 'otransitionend',
		transition: 'transitionend'
	}[transitionProperty];

	function ItemsAnimations(el, options) {
		var self = this;
		this.el = el;
		this.$el = $(el);
		this.options = {
			itemSelector: '',
			scrollMonitor: false,
			firstItemStatic: false
		};
		$.extend(this.options, options);
		this.$el.data('itemsAnimations', this);
		self.initialize();
	}
	$.fn.itemsAnimations = function (options) {
		if (typeof options === 'string') {
			var instance = $(this.get(0)).data('itemsAnimations');
			if (!instance) {
				return false;
			}
			if (options === 'instance') {
				return instance;
			}
		} else {
			return new ItemsAnimations(this.get(0), options);
		}
	}
	ItemsAnimations.prototype = {
		initialize: function () {
			var self = this;
			this.queue = [];
			this.queue_is_run = false;
			this.watchers = {};
			this.animation = this.getAnimation();
			if (!this.animation || $(window).width() < 767) {
				this.animationName = 'disabled';
				this.animation = this.getAnimationByName('disabled');
			}
			if (this.options.firstItemStatic) {
				this.firstStatisItem = $(this.options.itemSelector + ':first', this.$el);
				this.firstStatisItem.removeClass('item-animations-not-inited');
			}
			if (this.animationName == 'disabled') {
				$(this.options.itemSelector, this.$el).removeClass('item-animations-not-inited');
			}
			this.initTimer();
		},
		initTimer: function () {
			var self = this;
			this.timer = document.createElement('div');
			this.timer.className = 'items-animations-timer-element';
			if (this.animation.timeout > 0) {
				this.timer.setAttribute("style", "transition-duration: " + this.animation.timeout + "ms; -webkit-transition-duration: " + this.animation.timeout + "ms; -moz-transition-duration: " + this.animation.timeout + "ms; -o-transition-duration: " + this.animation.timeout + "ms;");
			}
			document.body.appendChild(this.timer);
			this.timerCallback = function () {};
			$(this.timer).bind(transitionEndEvent, function (event) {
				self.timerCallback();
			});
			this.timer.className += ' start-timer';
		},
		startTimer: function (callback) {
			this.timerCallback = callback;
			if (this.timer.className.indexOf('start-timer') != -1) {
				this.timer.className = this.timer.className.replace(' start-timer', '');
			} else {
				this.timer.className += ' start-timer';
			}
		},
		show: function ($items) {
			var self = this;
			if (this.animationName == 'disabled') {
				$(this.options.itemSelector, this.$el).removeClass('item-animations-not-inited');
				return false;
			}
			if ($items == undefined) {
				$items = $(this.options.itemSelector, this.$el);
			}
			$items.not('.item-animations-inited').each(function (index) {
				var $this = $(this);
				if (self.options.firstItemStatic && self.firstStatisItem && self.firstStatisItem.get(0) == this) {
					$this.addClass('item-animations-inited');
					return;
				}
				$this.addClass('item-animations-inited');
				if (self.options.scrollMonitor && this.animationName != 'disabled') {
					var watcher = scrollMonitor.create(this, -50);
					watcher.enterViewport(function () {
						var watcher = this;
						self.showItem($this, watcher);
					});
					self.watchers[watcher.uid] = watcher;
				} else {
					self.showItem($this);
				}
			});
			$(this.options.itemSelector, this.$el).not('.item-animations-inited').removeClass('item-animations-not-inited');
		},
		reinitItems: function ($items) {
			$items.removeClass('start-animation item-animations-inited item-animations-loading before-start').addClass('item-animations-not-inited');
			this.clear();
		},
		getAnimationName: function () {
			var m = this.$el[0].className.match(/item-animation-(\S+)/);
			if (!m) {
				return '';
			}
			return m[1];
		},
		getAnimation: function () {
			this.animationName = this.getAnimationName();
			return this.getAnimationByName(this.animationName);
		},
		getAnimationByName: function (name) {
			if (!name || animations[name] == undefined) {
				return false;
			}
			return animations[name];
		},
		showItem: function ($item, watcher) {
			var self = this;
			if ($item.hasClass('item-animations-loading')) {
				return false;
			}
			$item.addClass('before-start');

			function showItemCallback() {
				if ($item.length == 0) {
					return false;
				}
				self.animate($item);
				if (watcher != undefined) {
					self.destroyWatcher(watcher);
				}
			}
			$item.addClass('item-animations-loading');
			if (this.animation.timeout > 0) {
				this.queueAdd(showItemCallback, this.animation.timeout);
			} else {
				showItemCallback();
			}
		},
		destroyWatcher: function (watcher) {
			if (this.watchers[watcher.uid] != undefined) {
				delete this.watchers[watcher.uid];
			}
			watcher.destroy();
		},
		animate: function ($item, animation) {
			$item.bind(transitionEndEvent, function (event) {
				var target = event.target || event.srcElement;
				if (target != $item[0]) {
					return;
				}
				$item.unbind(transitionEndEvent);
				$item.removeClass('before-start start-animation');
			});
			$item.removeClass('item-animations-loading item-animations-not-inited').addClass('start-animation');
		},
		queueAdd: function (callback, timeout) {
			var self = this;
			this.queue.push({
				callback: callback,
				timeout: timeout
			});
			if (this.queue.length == 1 && !this.queue_is_run) {
				this.startTimer(function () {
					self.queueNext();
				});
			}
		},
		queueNext: function () {
			var self = this;
			if (this.queue.length == 0) {
				return false;
			}
			var next_action = this.queue.shift();
			if (next_action == undefined) {
				return false;
			}
			this.queue_is_run = true;
			next_action.callback();
			this.startTimer(function () {
				self.queue_is_run = false;
				self.queueNext();
			});
		},
		clear: function () {
			this.queue = [];
			this.queue_is_run = false;
			for (var watcher_uid in this.watchers) {
				if (this.watchers.hasOwnProperty(watcher_uid)) {
					this.destroyWatcher(this.watchers[watcher_uid]);
				}
			}
			this.watchers = [];
		}
	};
})(jQuery);;
(function ($) {
	function Slider(el, options) {
		var self = this;
		this.el = el;
		this.$el = $(el);
		this.options = {
			element: 'li',
			margin: 0,
			delay: 100,
			duration: 200,
			prevButton: false,
			nextButton: false,
			loop: true,
			afterInit: false,
			autoscroll: false
		};
		$.extend(this.options, options);
		self.initialize(true);
	}
	$.fn.reverse = [].reverse;
	$.fn.juraSlider = function (options) {
		return new Slider(this.get(0), options);
	}
	Slider.prototype = {
		initialize: function (first_init) {
			var self = this;
			if (first_init == undefined) {
				first_init = false;
			}
			this.is_animation = false;
			var first_element_height = this.$el.find(this.options.element + ':first').outerHeight();
			var padding_left = parseInt(this.$el.parent().css('padding-left'));
			var padding_right = parseInt(this.$el.parent().css('padding-right'));
			this.$el.css({
				whiteSpace: 'nowrap',
				left: padding_left,
				right: padding_right,
				top: 0,
				bottom: 0,
				height: first_element_height,
				position: 'absolute',
				clip: 'rect(auto, auto, ' + (first_element_height + 60) + 'px, auto)'
			});
			this.$el.parent().css({
				height: first_element_height,
				position: 'relative'
			});
			this.$el.find(this.options.element).css({
				margin: 0,
				position: 'absolute',
				left: this.$el.outerWidth(),
				top: 0,
				zIndex: 1
			}).removeClass('leftPosition currentPosition').addClass('rightPosition');
			if (first_init && this.options.nextButton)
				this.options.nextButton.click(function () {
					self.triggerNext(false);
				});
			if (first_init && this.options.prevButton)
				this.options.prevButton.click(function () {
					self.triggerPrev();
				});
			if (first_init) {
				$(window).resize(function () {
					self.initialize(false);
				});
			}
			if (first_init && $.isFunction(this.options.afterInit))
				this.options.afterInit();
			if (!first_init && autoscrollInterval) {
				clearInterval(autoscrollInterval);
			}
			this.triggerNext(true, !first_init);
			if (!first_init && this.options.autoscroll) {
				autoscrollInterval = setInterval(function () {
					self.triggerNext(false);
				}, this.options.autoscroll);
			}
			if (first_init && this.options.autoscroll) {
				var autoscrollInterval;
				var that = this;
				autoscrollInterval = setInterval(function () {
					self.triggerNext(false);
				}, that.options.autoscroll);
				that.$el.hover(function () {
					clearInterval(autoscrollInterval);
				}, function () {
					autoscrollInterval = setInterval(function () {
						self.triggerNext(false);
					}, that.options.autoscroll);
				});
			}
		},
		getNextCount: function () {
			var self = this;
			var count = 0;
			var next_width = 0;
			var index = 0;
			var el_width = parseFloat(getComputedStyle(this.el, '').getPropertyValue('width'));
			var new_width = 0;
			this.$el.find(this.options.element + '.rightPosition').each(function () {
				var width = parseFloat(getComputedStyle(this, '').getPropertyValue('width'));
				if (index > 0)
					width += self.options.margin;
				new_width = next_width + width;
				if (new_width > el_width)
					return false;
				next_width = next_width + width;
				count += 1;
				index += 1;
			});
			if (this.options.loop && new_width < el_width) {
				this.$el.find(this.options.element + '.leftPosition').each(function () {
					var width = parseFloat(getComputedStyle(this, '').getPropertyValue('width'));
					if (index > 0)
						width += self.options.margin;
					new_width = next_width + width;
					if (new_width > el_width)
						return false;
					$(this).css({
						left: el_width
					}).removeClass('leftPosition').addClass('rightPosition').appendTo(self.$el);
					next_width = next_width + width;
					count += 1;
					index += 1;
				});
			}
			return [count, next_width];
		},
		triggerNext: function (init, without_transition) {
			if (this.is_animation)
				return false;
			if (without_transition == undefined) {
				without_transition = false;
			}
			var self = this;
			var info = this.getNextCount();
			if (init && info[0] == this.$el.find(this.options.element).size()) {
				if (this.options.nextButton)
					this.options.nextButton.hide();
				if (this.options.prevButton)
					this.options.prevButton.hide();
			}
			if (info[0] < 1)
				return false;
			this.is_animation = true;
			this.hideLeft();
			setTimeout(function () {
				self.showNext(info, without_transition);
			}, without_transition ? 1 : 300);
		},
		hideLeft: function () {
			var delay = 0;
			var app = this;
			app.$el.find(app.options.element + '.currentPosition').each(function () {
				var self = this;
				setTimeout(function () {
					var offset = $(self).outerWidth();
					$(self).animate({
						left: -offset
					}, {
						duration: app.options.duration,
						queue: false,
						complete: function () {
							$(this).removeClass('currentPosition').addClass('leftPosition');
						}
					});
				}, delay);
				delay += app.options.delay;
			});
		},
		showNext: function (info, without_transition) {
			var app = this;
			if (info[0] < 1)
				return false;
			var offset = (app.$el.width() - info[1]) / 2;
			var delay = 0;
			var index = 0;
			app.$el.find(app.options.element + '.rightPosition:lt(' + info[0] + ')').each(function () {
				var self = this;
				if (without_transition) {
					$(self).removeClass('leftPosition rightPosition').addClass('currentPosition').css({
						left: offset
					});
				} else {
					app.showElement(self, offset, delay, index == (info[0] - 1));
				}
				delay += app.options.delay;
				offset += $(self).outerWidth() + app.options.margin;
				index += 1;
			});
			if (without_transition) {
				app.is_animation = false;
			}
		},
		showElement: function (element, offset, delay, is_last) {
			var app = this;
			setTimeout(function () {
				$(element).animate({
					left: offset
				}, {
					duration: app.options.duration,
					queue: false,
					complete: function () {
						$(this).removeClass('rightPosition').removeClass('leftPosition').addClass('currentPosition');
						if (is_last)
							app.is_animation = false;
					}
				});
			}, delay);
		},
		getPrevCount: function () {
			var self = this;
			var count = 0;
			var prev_width = 0;
			var index = 0;
			var el_width = parseFloat(getComputedStyle(this.el, '').getPropertyValue('width'));
			var new_width = 0;
			this.$el.find(this.options.element + '.leftPosition').reverse().each(function () {
				var width = parseFloat(getComputedStyle(this, '').getPropertyValue('width'));
				if (index > 0)
					width += self.options.margin;
				new_width = prev_width + width;
				if (new_width > el_width)
					return false;
				prev_width = prev_width + width;
				count += 1;
				index += 1;
			});
			if (this.options.loop && new_width < el_width) {
				this.$el.find(this.options.element + '.rightPosition').reverse().each(function () {
					var width = parseFloat(getComputedStyle(this, '').getPropertyValue('width'));
					if (index > 0)
						width += self.options.margin;
					new_width = prev_width + width;
					if (new_width > el_width)
						return false;
					$(this).css({
						left: -width
					}).removeClass('rightPosition').addClass('leftPosition').prependTo(self.$el);
					prev_width = prev_width + width;
					count += 1;
					index += 1;
				});
			}
			return [count, prev_width];
		},
		triggerPrev: function () {
			if (this.is_animation)
				return false;
			var self = this;
			var info = this.getPrevCount();
			if (info[0] < 1)
				return false;
			this.is_animation = true;
			this.hideRight();
			setTimeout(function () {
				self.showPrev(info);
			}, 300);
		},
		hideRight: function () {
			var delay = 0;
			var app = this;
			var offset = app.$el.width();
			app.$el.find(app.options.element + '.currentPosition').reverse().each(function () {
				var self = this;
				setTimeout(function () {
					$(self).animate({
						left: offset
					}, {
						duration: app.options.duration,
						queue: false,
						complete: function () {
							$(this).removeClass('currentPosition').addClass('rightPosition');
						}
					});
				}, delay);
				delay += app.options.delay;
			});
		},
		showPrev: function (info) {
			var app = this;
			if (info[0] < 1)
				return false;
			var offset = info[1] + (app.$el.width() - info[1]) / 2;
			var delay = 0;
			var index = 0;
			app.$el.find(app.options.element + '.leftPosition').slice(-info[0]).reverse().each(function () {
				var self = this;
				offset -= $(self).outerWidth();
				if (index > 0)
					offset -= app.options.margin;
				app.showElement(self, offset, delay, index == (info[0] - 1));
				delay += app.options.delay;
				index += 1;
			});
		}
	};
}(jQuery));;
(function ($) {
	$(function () {
		window.defaultSortData = {
			date: '[data-sort-date] parseInt',
			name: '.title'
		};

		function portfolio_images_loaded($box, image_selector, callback) {
			var $images = $(image_selector, $box).filter(function () {
					return !(this.complete && this.naturalWidth !== undefined && this.naturalWidth != 0);
				}),
				images_count = $images.length;
			if (images_count == 0) {
				return callback();
			}
			$images.on('load error', function () {
				images_count--;
				if (images_count == 0) {
					callback();
				}
			});
		}

		function init_prev_next_navigator_buttons($portfolio) {
			var current_page = $portfolio.data('current-page');
			var pages_count = $portfolio.data('pages-count');
			if (current_page <= 1)
				$('.portfolio-navigator a.prev', $portfolio).css('visibility', 'hidden');
			else
				$('.portfolio-navigator a.prev', $portfolio).css('visibility', 'visible');
			if (current_page >= pages_count)
				$('.portfolio-navigator a.next', $portfolio).css('visibility', 'hidden');
			else
				$('.portfolio-navigator a.next', $portfolio).css('visibility', 'visible');
		}

		function get_portfolio_sorted_items($portfolio) {
			if ($('.portfolio-sorting a.sorting-switcher', $portfolio).length == 0) {
				return $('.portfolio-set .portfolio-item', $portfolio);
			}
			var sortOptions = get_portfolio_sorting_data($portfolio);
			var sortBy = window.defaultSortData[sortOptions.sortBy];
			var isParseInt = false;
			if (sortBy.indexOf('parseInt') != -1) {
				sortBy = sortBy.replace(' parseInt', '');
				var isParseInt = true;
			}
			var isSortByAttr = false;
			var m = sortBy.match(/^\[(.+)\]$/);
			if (m) {
				sortBy = m[1];
				var isSortByAttr = true;
			}
			var $items = $('.portfolio-set .portfolio-item', $portfolio);
			$items.sort(function ($item1, $item2) {
				if (isSortByAttr) {
					var item1_value = $item1.getAttribute(sortBy);
					var item2_value = $item2.getAttribute(sortBy);
				} else {
					var item1_value = $(sortBy, $item1).text();
					var item2_value = $(sortBy, $item2).text();
				}
				if (isParseInt) {
					item1_value = parseInt(item1_value);
					item2_value = parseInt(item2_value);
				}
				return (item1_value > item2_value ? 1 : -1) * (sortOptions.sortAscending ? 1 : -1);
			});
			return $items;
		}

		function init_portfolio_pages($portfolio) {
			var count = $('.portfolio-set .portfolio-item', $portfolio).size();
			var default_per_page = $portfolio.data('per-page') || count;
			if ($('.portfolio-count select', $portfolio).size() > 0)
				var per_page = $('.portfolio-count select', $portfolio).val();
			else
				var per_page = default_per_page;
			var pages_count = Math.ceil(count / per_page);
			var current_page = 1;
			$portfolio.data('per-page', per_page);
			$portfolio.data('pages-count', pages_count);
			$portfolio.data('current-page', current_page);
			if ($('.portfolio-navigator', $portfolio).size() > 0 && pages_count > 1) {
				var pagenavigator = '<a href="#" class="prev">&#xe603;</a>';
				for (var i = 0; i < pages_count; i++)
					pagenavigator += '<a href="#" data-page="' + (i + 1) + '">' + (i + 1) + '</a>';
				pagenavigator += '<a href="#" class="next">&#xe601;</a>';
				$('.portfolio-navigator', $portfolio).html(pagenavigator).show();
				$('.portfolio-set', $portfolio).css('margin-bottom', '');
				$('.portfolio-navigator a[data-page="' + current_page + '"]', $portfolio).addClass('current')
				init_prev_next_navigator_buttons($portfolio);
			} else {
				$('.portfolio-navigator', $portfolio).html('').hide();
				$('.portfolio-set', $portfolio).css('margin-bottom', 0);
			}
			$('.portfolio-set .portfolio-item', $portfolio).removeClass(function (index, class_name) {
				return (class_name.match(/\bpaginator-page-\S+/g) || []).join(' ');
			});
			var sorted_items = get_portfolio_sorted_items($portfolio);
			$.each(sorted_items, function (i, item) {
				var page = Math.ceil((i + 1) / per_page);
				$(item).addClass('paginator-page-' + page);
			});
			$('.portfolio-navigator', $portfolio).on('click', 'a', function () {
				if ($(this).hasClass('current'))
					return false;
				var current_page = $(this).siblings('.current:first').data('page');
				if ($(this).hasClass('prev')) {
					var page = current_page - 1;
				} else if ($(this).hasClass('next')) {
					var page = current_page + 1
				} else {
					var page = $(this).data('page');
				}
				if (page < 1)
					page = 1;
				if (page > pages_count)
					page = pages_count;
				$(this).siblings('a').removeClass('current');
				$(this).parent().find('a[data-page="' + page + '"]').addClass('current');
				$portfolio.data('current-page', page);
				init_prev_next_navigator_buttons($portfolio);
				var filterValue = '';
				if ($('.portfolio-filters a.active', $portfolio).size() > 0) {
					filterValue += $('.portfolio-filters a.active', $portfolio).data('filter');
				}
				filterValue += '.paginator-page-' + page;
				$portfolio.itemsAnimations('instance').reinitItems($('.portfolio-set .portfolio-item', $portfolio));
				$('.portfolio-set', $portfolio).isotope({
					filter: filterValue
				});
				$("html, body").animate({
					scrollTop: $portfolio.offset().top - 200
				}, 600);
				return false;
			});
		}

		function init_portfolio_count($portfolio) {
			if (!$('.portfolio-count select', $portfolio).length) {
				return false;
			}
			$('.portfolio-count select', $portfolio).on('change', function () {
				init_portfolio_pages($portfolio);
				if ($('.portfolio-filters', $portfolio).length) {
					$('.portfolio-filters a', $portfolio).removeClass('active');
					$('.portfolio-filters a[data-filter="*"]', $portfolio).addClass('active');
				}
				var current_page = $portfolio.data('current-page');
				$portfolio.itemsAnimations('instance').reinitItems($('.portfolio-set .portfolio-item', $portfolio));
				$('.portfolio-set', $portfolio).isotope({
					filter: '.paginator-page-' + current_page
				});
			});
		}

		function get_portfolio_sorting_data($portfolio) {
			var sorting = {
				sortBy: $('.portfolio-sorting .orderby .sorting-switcher', $portfolio).data('current'),
				sortAscending: $('.portfolio-sorting .order .sorting-switcher', $portfolio).data('current') == 'ASC'
			};
			return sorting;
		}

		function init_portfolio_sorting($portfolio) {
			if ($('.portfolio-sorting a.sorting-switcher', $portfolio).length == 0)
				return false;
			$('.portfolio-sorting a.sorting-switcher', $portfolio).on('click', function (e) {
				var $selected = $('label[data-value!="' + $(this).data('current') + '"]', $(this).parent());
				$(this).data('current', $selected.data('value'));
				if ($(this).next().is($selected)) {
					$(this).addClass('right');
				} else {
					$(this).removeClass('right');
				}
				if ($portfolio.hasClass('portfolio-pagination-scroll')) {
					$portfolio.data('next-page', 1);
					portfolio_scroll_load_next_request($portfolio);
				} else if ($('.portfolio-load-more', $portfolio).size() == 0) {
					init_portfolio_pages($portfolio);
					var current_page = $portfolio.data('current-page');
					var sortOptions = get_portfolio_sorting_data($portfolio);
					$portfolio.itemsAnimations('instance').reinitItems($('.portfolio-set .portfolio-item', $portfolio));
					$('.portfolio-set', $portfolio).isotope({
						filter: '.paginator-page-' + current_page,
						sortBy: sortOptions.sortBy,
						sortAscending: sortOptions.sortAscending
					});
				} else {
					$portfolio.data('next-page', 1);
					portfolio_load_core_request($portfolio);
				}
				e.preventDefault();
				return false;
			});
			$('.portfolio-sorting label', $portfolio).on('click', function (e) {
				if ($(this).data('value') != $('.sorting-switcher', $(this).parent()).data('current')) {
					$('.sorting-switcher', $(this).parent()).click();
				}
				e.preventDefault();
				return false;
			});
		}

		function portfolio_load_more_request($portfolio, $set, is_scroll) {
			var uid = $portfolio.data('portfolio-uid'),
				is_processing_request = $set.data('request-process') || false;
			if (is_processing_request) {
				return false;
			}
			var data = $.extend(true, {}, window['portfolio_ajax_' + uid]);
			if ($('.portfolio-count select', $portfolio).length) {
				data['data']['more_count'] = $('.portfolio-count select', $portfolio).val();
			}
			data['data']['more_page'] = $portfolio.data('next-page');
			if (data['data']['more_page'] == null || data['data']['more_page'] == undefined) {
				data['data']['more_page'] = 1;
			}
			if (data['data']['more_page'] == 0) {
				return false;
			}
			if ($('.portfolio-filters', $portfolio).length) {
				data['data']['portfolio'] = $portfolio.data('more-filter') || data['data']['portfolio'];
			}
			if ($('.portfolio-sorting', $portfolio).length) {
				data['data']['orderby'] = $('.portfolio-sorting .orderby .sorting-switcher', $portfolio).data('current');
				data['data']['order'] = $('.portfolio-sorting .order .sorting-switcher', $portfolio).data('current');
			}
			data['action'] = 'portfolio_load_more';
			$set.data('request-process', true);
			if (is_scroll) {
				$('.portfolio-scroll-pagination', $portfolio).addClass('active').html('<div class="loading"></div>');
			} else {
				$('.portfolio-load-more .gem-button', $portfolio).before('<div class="loading"></div>');
			}
			$.ajax({
				type: 'post',
				dataType: 'json',
				url: data.url,
				data: data,
				success: function (response) {
					if (response.status == 'success') {
						var $newItems = $(response.html),
							current_page = $newItems.data('page'),
							next_page = $newItems.data('next-page'),
							$inserted_data = $($newItems.html());
						$inserted_data.addClass('paginator-page-1');
						if ($portfolio.itemsAnimations('instance').getAnimationName() != 'disabled') {
							$inserted_data.addClass('item-animations-not-inited');
						} else {
							$inserted_data.removeClass('item-animations-not-inited');
						}
						if (($portfolio.hasClass('columns-2') || $portfolio.hasClass('columns-3') || $portfolio.hasClass('columns-4')) && $portfolio.closest('.vc_row[data-vc-stretch-content="true"]').length > 0) {
							$('.image-inner picture source', $inserted_data).remove();
						}
						portfolio_images_loaded($inserted_data, '.image-inner img', function () {
							if (current_page == 1) {
								$portfolio.itemsAnimations('instance').clear();
								$set.html('');
							}
							$set.isotope('insert', $inserted_data);
							$portfolio.itemsAnimations('instance').show($inserted_data);
							if (is_scroll) {
								$('.portfolio-scroll-pagination', $portfolio).removeClass('active').html('');
							} else {
								$('.portfolio-scroll-pagination', $portfolio).addClass('active').html('<div class="loading"></div>');
								if (next_page > 0) {
									$('.portfolio-load-more', $portfolio).show();
								} else {
									$('.portfolio-load-more', $portfolio).hide();
								}
							}
							$portfolio.data('next-page', next_page);
							$set.data('request-process', false);
						});
					} else {
						alert(response.message);
					}
				}
			});
		}

		function portfolio_load_core_request($portfolio) {
			var $set = $('.portfolio-set', $portfolio);
			var uid = $portfolio.data('portfolio-uid');
			var is_processing_request = $set.data('request-process') || false;
			if (is_processing_request)
				return false;
			$set.data('request-process', true);
			var data = $.extend(true, {}, window['portfolio_ajax_' + uid]);
			data['action'] = 'portfolio_load_more';
			if ($('.portfolio-count select', $portfolio).size() > 0)
				data['data']['more_count'] = $('.portfolio-count select', $portfolio).val();
			data['data']['more_page'] = $portfolio.data('next-page') || 1;
			if (data['data']['more_page'] == 0)
				return false;
			if ($('.portfolio-filters', $portfolio).size() > 0) {
				data['data']['portfolio'] = $portfolio.data('more-filter') || data['data']['portfolio'];
			}
			if ($('.portfolio-sorting', $portfolio).length > 0) {
				data['data']['orderby'] = $('.portfolio-sorting .orderby .sorting-switcher', $portfolio).data('current');
				data['data']['order'] = $('.portfolio-sorting .order .sorting-switcher', $portfolio).data('current');
			}
			$('.portfolio-load-more .gem-button', $portfolio).before('<div class="loading"><div class="preloader-spin"></div></div>');
			$.ajax({
				type: 'post',
				dataType: 'json',
				url: data.url,
				data: data,
				success: function (response) {
					if (response.status == 'success') {
						var minZIndex = $('.portfolio-item:last', $set).css('z-index') - 1;
						var $newItems = $(response.html);
						$('.portfolio-item', $newItems).addClass('paginator-page-1')
						$('.portfolio-item', $newItems).each(function () {
							$(this).css('z-index', minZIndex--);
						});
						var current_page = $newItems.data('page');
						var next_page = $newItems.data('next-page');
						var $inserted_data = $($newItems.html());
						if ($portfolio.itemsAnimations('instance').getAnimationName() != 'disabled') {
							$inserted_data.addClass('item-animations-not-inited');
						} else {
							$inserted_data.removeClass('item-animations-not-inited');
						}
						if (($portfolio.hasClass('columns-2') || $portfolio.hasClass('columns-3') || $portfolio.hasClass('columns-4')) && $portfolio.closest('.vc_row[data-vc-stretch-content="true"]').length > 0) {
							$('.image-inner picture source', $inserted_data).remove();
						}
						portfolio_images_loaded($inserted_data, '.image-inner img', function () {
							if (current_page == 1) {
								$portfolio.itemsAnimations('instance').clear();
								$set.html('');
							}
							$set.isotope('insert', $inserted_data);
							init_circular_overlay($portfolio, $set);
							$portfolio.itemsAnimations('instance').show($inserted_data);
							$('.portfolio-load-more .loading', $portfolio).remove();
							$portfolio.data('next-page', next_page);
							if (next_page > 0) {
								$('.portfolio-load-more', $portfolio).show();
							} else {
								$('.portfolio-load-more', $portfolio).hide();
							}
							$set.data('request-process', false);
						});
					} else {
						alert(response.message);
						$('.portfolio-load-more .gem-button .loading', $portfolio).remove();
					}
				}
			});
		}

		function init_portfolio_more_count($portfolio) {
			if ($('.portfolio-count select', $portfolio).size() == 0)
				return false;
			$('.portfolio-count select', $portfolio).on('change', function () {
				$portfolio.data('next-page', 1);
				portfolio_load_core_request($portfolio);
			});
		}

		function init_portfolio_scroll_next_count($portfolio) {
			if ($('.portfolio-count select', $portfolio).size() == 0)
				return false;
			$('.portfolio-count select', $portfolio).on('change', function () {
				$portfolio.data('next-page', 1);
				portfolio_scroll_load_next_request($portfolio);
			});
		}

		function portfolio_scroll_load_next_request($portfolio) {
			var $set = $('.portfolio-set', $portfolio);
			var uid = $portfolio.data('portfolio-uid');
			var is_processing_request = $set.data('request-process') || false;
			if (is_processing_request)
				return false;
			var data = $.extend(true, {}, window['portfolio_ajax_' + uid]);
			data['action'] = 'portfolio_load_more';
			if ($('.portfolio-count select', $portfolio).size() > 0)
				data['data']['more_count'] = $('.portfolio-count select', $portfolio).val();
			data['data']['more_page'] = $portfolio.data('next-page');
			if (data['data']['more_page'] == null || data['data']['more_page'] == undefined) {
				data['data']['more_page'] = 1;
			}
			if (data['data']['more_page'] == 0)
				return false;
			if ($('.portfolio-filters', $portfolio).size() > 0) {
				data['data']['portfolio'] = $portfolio.data('more-filter') || data['data']['portfolio'];
			}
			if ($('.portfolio-sorting', $portfolio).length > 0) {
				data['data']['orderby'] = $('.portfolio-sorting .orderby .sorting-switcher', $portfolio).data('current');
				data['data']['order'] = $('.portfolio-sorting .order .sorting-switcher', $portfolio).data('current');
			}
			$set.data('request-process', true);
			$('.portfolio-scroll-pagination', $portfolio).addClass('active').html('<div class="loading"><div class="preloader-spin"></div></div>');
			$.ajax({
				type: 'post',
				dataType: 'json',
				url: data.url,
				data: data,
				success: function (response) {
					if (response.status == 'success') {
						var minZIndex = $('.portfolio-item:last', $set).css('z-index') - 1;
						var $newItems = $(response.html);
						$('.portfolio-item', $newItems).addClass('paginator-page-1')
						$('.portfolio-item', $newItems).each(function () {
							$(this).css('z-index', minZIndex--);
						});
						var current_page = $newItems.data('page');
						var next_page = $newItems.data('next-page');
						var $inserted_data = $($newItems.html());
						if ($portfolio.itemsAnimations('instance').getAnimationName() != 'disabled') {
							$inserted_data.addClass('item-animations-not-inited');
						} else {
							$inserted_data.removeClass('item-animations-not-inited');
						}
						if (($portfolio.hasClass('columns-2') || $portfolio.hasClass('columns-3') || $portfolio.hasClass('columns-4')) && $portfolio.closest('.vc_row[data-vc-stretch-content="true"]').length > 0) {
							$('.image-inner picture source', $inserted_data).remove();
						}
						portfolio_images_loaded($inserted_data, '.image-inner img', function () {
							if (current_page == 1) {
								$portfolio.itemsAnimations('instance').clear();
								$set.html('');
							}
							$set.isotope('insert', $inserted_data);
							init_circular_overlay($portfolio, $set);
							$portfolio.itemsAnimations('instance').show($inserted_data);
							$('.portfolio-scroll-pagination', $portfolio).removeClass('active').html('');
							$portfolio.data('next-page', next_page);
							$set.data('request-process', false);
						});
					} else {
						alert(response.message);
						$('.portfolio-scroll-pagination', $portfolio).removeClass('active').html('');
					}
				}
			});
		}

		function init_portfolio_scroll_next_page($portfolio) {
			if ($('.portfolio-scroll-pagination', $portfolio).length == 0) {
				return false;
			}
			var $pagination = $('.portfolio-scroll-pagination', $portfolio);
			var watcher = scrollMonitor.create($pagination[0]);
			watcher.enterViewport(function () {
				portfolio_scroll_load_next_request($portfolio);
			});
		}
		$('.portfolio-count select').combobox();

		function init_circular_overlay($portfolio, $set) {
			if (!$portfolio.hasClass('hover-circular')) {
				return;
			}
			$('.portfolio-item', $set).on('mouseenter', function () {
				var overlayWidth = $('.overlay', this).width(),
					overlayHeight = $('.overlay', this).height(),
					$overlayCircle = $('.overlay-circle', this),
					maxSize = 0;
				if (overlayWidth > overlayHeight) {
					maxSize = overlayWidth;
					$overlayCircle.height(overlayWidth)
				} else {
					maxSize = overlayHeight;
					$overlayCircle.width(overlayHeight);
				}
				maxSize += overlayWidth * 0.3;
				$overlayCircle.css({
					marginLeft: -maxSize / 2,
					marginTop: -maxSize / 2
				});
			});
		}
		$('.portfolio').not('.portfolio-slider').each(function () {
			var $portfolio = $(this);
			var $set = $('.portfolio-set', this);
			if ($portfolio.hasClass('portfolio-pagination-scroll')) {
				var current_page = 1;
				$('.portfolio-set .portfolio-item', $portfolio).addClass('paginator-page-1');
				init_portfolio_sorting($portfolio);
				init_portfolio_scroll_next_count($portfolio);
			} else if ($('.portfolio-load-more', $portfolio).size() == 0) {
				init_portfolio_count($portfolio);
				init_portfolio_sorting($portfolio);
				init_portfolio_pages($portfolio);
				var current_page = $portfolio.data('current-page');
			} else {
				var current_page = 1;
				$('.portfolio-set .portfolio-item', $portfolio).addClass('paginator-page-1');
				init_portfolio_sorting($portfolio);
				init_portfolio_more_count($portfolio);
			}
			if (($portfolio.hasClass('columns-2') || $portfolio.hasClass('columns-3') || $portfolio.hasClass('columns-4')) && $portfolio.closest('.vc_row[data-vc-stretch-content="true"]').length > 0) {
				$('.image-inner picture source', $set).remove();
			}
			portfolio_images_loaded($set, '.image-inner img', function () {
				var sortOptions = get_portfolio_sorting_data($portfolio);
				var layoutMode = 'masonry-custom';
				if ($portfolio.hasClass('portfolio-style-metro')) {
					layoutMode = 'metro';
				}
				var itemsAnimations = $portfolio.itemsAnimations({
					itemSelector: '.portfolio-item',
					scrollMonitor: true
				});
				init_circular_overlay($portfolio, $set);
				var isotope_options = {
					itemSelector: '.portfolio-item',
					layoutMode: layoutMode,
					itemImageWrapperSelector: '.image-inner',
					fixHeightDoubleItems: $portfolio.hasClass('portfolio-style-justified'),
					'masonry-custom': {
						columnWidth: '.portfolio-item:not(.double-item)'
					},
					filter: '.paginator-page-' + current_page,
					transitionDuration: 0
				};
				if ($('.portfolio-load-more', $portfolio).size() == 0 && !$portfolio.hasClass('portfolio-pagination-scroll')) {
					isotope_options['getSortData'] = window.defaultSortData;
					isotope_options['sortBy'] = sortOptions.sortBy;
					isotope_options['sortAscending'] = sortOptions.sortAscending;
				}
				var init_portfolio = true;
				$portfolio.closest('.portfolio-preloader-wrapper').prev('.preloader').remove();
				$set.on('arrangeComplete', function (event, filteredItems) {
					if ($set.closest('.fullwidth-block').size() > 0) {
						$set.closest('.fullwidth-block').bind('fullwidthUpdate', function () {
							if ($set.data('isotope')) {
								$set.isotope('layout');
								return false;
							}
						});
					} else {
						if ($set.closest('.vc_row[data-vc-stretch-content="true"]').length > 0) {
							$set.closest('.vc_row[data-vc-stretch-content="true"]').bind('VCRowFullwidthUpdate', function () {
								if ($set.data('isotope')) {
									$set.isotope('layout');
									return false;
								}
							});
						}
					}
					if (init_portfolio) {
						var items = [];
						filteredItems.forEach(function (item) {
							items.push(item.element);
						});
						itemsAnimations.show($(items));
					}
				}).isotope(isotope_options);
				if (!window.gemSettings.lasyDisabled) {
					var elems = $('.portfolio-item:visible', $set);
					var items = [];
					for (var i = 0; i < elems.length; i++)
						items.push($set.isotope('getItem', elems[i]));
					$set.isotope('reveal', items);
				}
				if ($set.closest('.gem_tab').size() > 0) {
					$set.closest('.gem_tab').bind('tab-update', function () {
						if ($set.data('isotope')) {
							$set.isotope('layout');
						}
					});
				}
				$(document).on('gem.show.vc.tabs', '[data-vc-accordion]', function () {
					var $tab = $(this).data('vc.accordion').getTarget();
					if ($tab.find($set).length) {
						if ($set.data('isotope')) {
							$set.isotope('layout');
						}
					}
				});
				$(document).on('gem.show.vc.accordion', '[data-vc-accordion]', function () {
					var $tab = $(this).data('vc.accordion').getTarget();
					if ($tab.find($set).length) {
						if ($set.data('isotope')) {
							$set.isotope('layout');
						}
					}
				});
				if ($set.closest('.gem_accordion_content').size() > 0) {
					$set.closest('.gem_accordion_content').bind('accordion-update', function () {
						if ($set.data('isotope')) {
							$set.isotope('layout');
						}
					});
				}
				if ($('.portfolio-filters', $portfolio).size() > 0) {
					$('.portfolio-filters, .portfolio-filters-resp ul li', $portfolio).on('click', 'a', function () {
						if ($('.portfolio-load-more', $portfolio).size() == 0 && !$portfolio.hasClass('portfolio-pagination-scroll')) {
							var current_page = $portfolio.data('current-page');
							var filterValue = $(this).data('filter') || '';
							filterValue += '.paginator-page-' + current_page;
							$('.portfolio-filters a.active, .portfolio-filters-resp ul li a.active', $portfolio).removeClass('active');
							$(this).addClass('active');
							$portfolio.itemsAnimations('instance').reinitItems($('.portfolio-set .portfolio-item', $portfolio));
							$('.portfolio-set', $portfolio).isotope({
								filter: filterValue
							});
						} else {
							var filterValue = $(this).data('filter') || '';
							$('.portfolio-filters a.active, .portfolio-filters-resp ul li a.active', $portfolio).removeClass('active');
							$(this).addClass('active');
							$portfolio.data('more-filter', filterValue.substr(1));
							$portfolio.data('next-page', 1);
							if ($portfolio.hasClass('portfolio-pagination-scroll')) {
								portfolio_scroll_load_next_request($portfolio);
							} else {
								portfolio_load_core_request($portfolio);
							}
						}
						if ($('.portfolio-filters-resp', $portfolio).size() > 0)
							$('.portfolio-filters-resp', $portfolio).dlmenu('closeMenu');
						return false;
					});
				}
				$('.info', $portfolio).on('click', 'a:not(.zilla-likes)', function () {
					var slug = $(this).data('slug') || '';
					$('.portfolio-filters a[data-filter=".' + slug + '"]').click();
					return false;
				});
				$('.portfolio-load-more', $portfolio).on('click', function () {
					portfolio_load_core_request($portfolio);
				});
				if ($portfolio.hasClass('portfolio-pagination-scroll')) {
					init_portfolio_scroll_next_page($portfolio);
				}
			});
			$('.portfolio-filters-resp', $portfolio).dlmenu({
				animationClasses: {
					classin: 'dl-animate-in',
					classout: 'dl-animate-out'
				}
			});
		});

		function update_slider_paddings($portfolio) {
			var first_item_height = $('.portfolio-item:first .image-inner', $portfolio).outerHeight();
			var button_height = $('.portolio-slider-prev span', $portfolio).outerHeight();
			$('.portolio-slider-prev', $portfolio).css('padding-top', (first_item_height - button_height) / 2);
			$('.portolio-slider-next', $portfolio).css('padding-top', (first_item_height - button_height) / 2);
		}
		$('.portfolio.portfolio-slider').each(function () {
			var $portfolio = $(this);
			var $set = $('.portfolio-set', this);
			var $prev = $('.portolio-slider-prev span', $portfolio);
			var $next = $('.portolio-slider-next span', $portfolio);
			portfolio_images_loaded($set, '.image-inner img', function () {
				init_circular_overlay($portfolio, $set);
				$set.juraSlider({
					element: '.portfolio-item',
					prevButton: $prev,
					nextButton: $next,
					afterInit: function () {
						$portfolio.prev('.preloader').remove();
					},
					autoscroll: $set.data('autoscroll') ? $set.data('autoscroll') : false
				});
				update_slider_paddings($portfolio);
			});
		});
		$(window).resize(function () {
			$('.portfolio.portfolio-slider').each(function () {
				var $portfolio = $(this);
				setTimeout(function () {
					update_slider_paddings($portfolio);
				}, 10);
			});
		});
		$('body').on('click', 'a.icon.share', function (e) {
			e.preventDefault();
			$(this).closest('.links').find('.portfolio-sharing-pane').toggleClass('active');
			return false;
		});
		$('.portfolio-item').on('mouseleave', function () {
			$('.portfolio-sharing-pane').removeClass('active');
		});
		$('.portfolio').on('click', '.portfolio-item', function () {
			$(this).mouseover();
		});
	});
})(jQuery);;
(function ($) {
	var $window = $(window);
	$.fn.parallaxVertical = function (xpos) {
		var windowHeight = $window.height();
		this.each(function () {
			var $this = $(this),
				speedFactor, offsetFactor = 0,
				getHeight, topOffset = 0,
				containerHeight = 0,
				containerWidth = 0,
				disableParallax = false,
				parallaxIsDisabled = false,
				baseImgHeight = 0,
				baseImgWidth = 0,
				isBgCover = ($this.css('background-size') == 'cover'),
				curImgHeight = 0,
				reversed = $this.hasClass('parallax-reversed'),
				baseSpeedFactor = reversed ? -0.1 : 0.61,
				outerHeight = true;
			if (xpos === undefined) xpos = "50%";
			if (outerHeight) {
				getHeight = function (jqo) {
					return jqo.outerHeight(true);
				};
			} else {
				getHeight = function (jqo) {
					return jqo.height();
				};
			}

			function getBackgroundSize(callback) {
				var img = new Image(),
					width, height, backgroundSize = ($this.css('background-size') || ' ').split(' ');
				if (/px/.test(backgroundSize[0])) width = parseInt(backgroundSize[0]);
				if (/%/.test(backgroundSize[0])) width = $this.parent().width() * (parseInt(backgroundSize[0]) / 100);
				if (/px/.test(backgroundSize[1])) height = parseInt(backgroundSize[1]);
				if (/%/.test(backgroundSize[1])) height = $this.parent().height() * (parseInt(backgroundSize[0]) / 100);
				if (width !== undefined && height !== undefined) {
					return callback({
						width: width,
						height: height
					});
				}
				img.onload = function () {
					if (typeof width == 'undefined') width = this.width;
					if (typeof height == 'undefined') height = this.height;
					callback({
						width: width,
						height: height
					});
				};
				img.src = ($this.css('background-image') || '').replace(/url\(['"]*(.*?)['"]*\)/g, '$1');
			}

			function update() {
				if (disableParallax) {
					if (!parallaxIsDisabled) {
						$this.css('backgroundPosition', '');
						$this.removeClass('fullwidth-block-parallax-vertical');
						parallaxIsDisabled = true;
					}
					return;
				} else {
					if (parallaxIsDisabled) {
						$this.addClass('fullwidth-block-parallax-vertical');
						parallaxIsDisabled = false;
					}
				}
				if (isNaN(speedFactor))
					return;
				var pos = $window.scrollTop();
				if ((topOffset + containerHeight < pos) || (pos < topOffset - windowHeight)) return;
				$this.css('backgroundPosition', xpos + " " + (offsetFactor + speedFactor * (topOffset - pos)) + "px");
			}

			function resize() {
				setTimeout(function () {
					windowHeight = $window.height();
					containerHeight = getHeight($this);
					containerWidth = $this.width();
					if (isBgCover) {
						if (baseImgWidth / baseImgHeight <= containerWidth / containerHeight) {
							curImgHeight = baseImgHeight * ($this.width() / baseImgWidth);
							disableParallax = false;
						} else {
							disableParallax = true;
						}
					}
					if (curImgHeight !== 0) {
						if (baseSpeedFactor >= 0) {
							speedFactor = Math.min(baseSpeedFactor, curImgHeight / windowHeight);
							offsetFactor = Math.min(0, .5 * (windowHeight - curImgHeight - speedFactor * (windowHeight - containerHeight)));
						} else {
							speedFactor = Math.min(baseSpeedFactor, (windowHeight - containerHeight) / (windowHeight + containerHeight));
							offsetFactor = Math.max(0, speedFactor * containerHeight);
						}
					} else {
						speedFactor = baseSpeedFactor;
						offsetFactor = 0;
					}
					topOffset = $this.offset().top;
					update();
				}, 10);
			}
			getBackgroundSize(function (sz) {
				curImgHeight = baseImgHeight = sz.height;
				baseImgWidth = sz.width;
				resize();
			});
			$window.bind({
				scroll: update,
				load: resize,
				resize: resize
			});
			resize();
		});
	};
})(jQuery);;
(function ($) {
	$(function () {
		$('.gem-counter').each(function () {
			var $item = $(this);
			var initHover = {
				icon_color1: $('.gem-icon-half-1', $item).css('color'),
				icon_color2: $('.gem-icon-half-2', $item).css('color'),
				icon_background: $('.gem-icon-inner', $item).css('background-color'),
				icon_border: $('.gem-icon', $item).css('border-left-color'),
				icon_box_border: $('.gem-counter-icon-circle-1', $item).css('border-left-color'),
				icon_box_shadow: $('.gem-icon', $item).css('box-shadow'),
				box_color: $('.gem-counter-inner', $item).css('background-color'),
				number_color: $('.gem-counter-number', $item).css('color'),
				text_color: $('.gem-counter-text', $item).css('color'),
			};
			$item.data('initHover', initHover);
			if ($item.hasClass('gem-counter-effect-background-reverse') || $item.hasClass('gem-counter-effect-border-reverse')) {
				$('.gem-icon-inner', $item).prepend('<div class="gem-counter-animation"/>');
				if ($item.hasClass('gem-counter-effect-border-reverse')) {
					$('.gem-counter-animation', $item).css('background-color', initHover.box_color);
				} else if ($item.data('hover-background-color')) {
					$('.gem-counter-animation', $item).css('background-color', $item.data('hover-background-color'));
				}
			}
		});
		$('body').on('mouseenter', '.gem-counter a', function () {
			var $item = $(this).closest('.gem-counter');
			var initHover = $item.data('initHover');
			var $box = $item.closest('.gem-counter-box');
			$item.addClass('hover');
			if ($item.data('hover-icon-color')) {
				if ($box.hasClass('gem-counter-style-2')) {
					$('.gem-icon-half-1', $item).css('color', initHover.icon_box_border);
					$('.gem-icon-half-2', $item).css('color', initHover.icon_box_border);
					$('.gem-counter-icon-circle-1', $item).css('border-color', $item.data('hover-icon-color'));
					$('.gem-counter-icon-circle-1', $item).css('background-color', $item.data('hover-icon-color'));
					$('.gem-counter-icon-circle-2', $item).css('border-color', 'transparent');
				} else {
					if ($item.hasClass('gem-counter-effect-background-reverse')) {
						$('.gem-icon', $item).css('border-color', $item.data('hover-icon-color'));
						$('.gem-icon-half-1', $item).css('color', $item.data('hover-icon-color'));
						$('.gem-icon-half-2', $item).css('color', $item.data('hover-icon-color'));
					}
					if ($item.hasClass('gem-counter-effect-border-reverse')) {
						$('.gem-icon', $item).css('border-color', $item.data('hover-icon-color'));
						$('.gem-icon-inner', $item).css('background-color', $item.data('hover-icon-color'));
						$('.gem-icon-half-1', $item).css('color', '#ffffff');
						$('.gem-icon-half-2', $item).css('color', '#ffffff');
					}
					if ($item.hasClass('gem-counter-effect-simple')) {
						$('.gem-icon-half-1', $item).css('color', $item.data('hover-icon-color'));
						$('.gem-icon-half-2', $item).css('color', $item.data('hover-icon-color'));
					}
				}
			}
			if ($item.data('hover-numbers-color')) {
				$('.gem-counter-number', $item).css('color', $item.data('hover-numbers-color'));
			}
			if ($item.data('hover-text-color')) {
				$('.gem-counter-text', $item).css('color', $item.data('hover-text-color'));
			}
			if ($item.data('hover-background-color')) {
				$('.gem-counter-inner', $item).css('background-color', $item.data('hover-background-color'));
				$('.gem-counter-bottom-left, .gem-counter-bottom-right', $item).css('background-color', $item.data('hover-background-color'));
				$('.gem-counter-bottom svg', $item).css('fill', $item.data('hover-background-color'));
				if (!$box.hasClass('gem-counter-style-vertical')) {
					$('.gem-icon', $item).css('box-shadow', '0 0 0 5px ' + $item.data('hover-background-color') + ', 0 0 0 6px ' + ($item.data('hover-icon-color') ? $item.data('hover-icon-color') : '#ffffff'));
				}
			}
		});
		$('body').on('mouseleave', '.gem-counter a', function () {
			var $item = $(this).closest('.gem-counter');
			var initHover = $item.data('initHover');
			$item.removeClass('hover');
			$('.gem-icon', $item).css('border-color', initHover.icon_border);
			$('.gem-icon-inner', $item).css('background-color', initHover.icon_background);
			$('.gem-icon-half-1', $item).css('color', initHover.icon_color1);
			$('.gem-icon-half-2', $item).css('color', initHover.icon_color2);
			$('.gem-icon', $item).css('box-shadow', initHover.icon_box_shadow), $('.gem-counter-icon-circle-1, .gem-counter-icon-circle-2', $item).css('border-color', initHover.icon_box_border);
			$('.gem-counter-icon-circle-1').css('background-color', 'transparent');
			$('.gem-counter-inner', $item).css('background-color', initHover.box_color);
			$('.gem-counter-bottom-left, .gem-counter-bottom-right', $item).css('background-color', initHover.box_color);
			$('.gem-counter-bottom svg', $item).css('fill', initHover.box_color);
			$('.gem-counter-number', $item).css('color', initHover.number_color);
			$('.gem-counter-text', $item).css('color', initHover.text_color);
		});
	});
})(jQuery);;
(function (a) {
	if (typeof define === "function" && define.amd && define.amd.jQuery) {
		define(["jquery"], a)
	} else {
		if (typeof module !== "undefined" && module.exports) {
			a(require("jquery"))
		} else {
			a(jQuery)
		}
	}
}(function (f) {
	var y = "1.6.15",
		p = "left",
		o = "right",
		e = "up",
		x = "down",
		c = "in",
		A = "out",
		m = "none",
		s = "auto",
		l = "swipe",
		t = "pinch",
		B = "tap",
		j = "doubletap",
		b = "longtap",
		z = "hold",
		E = "horizontal",
		u = "vertical",
		i = "all",
		r = 10,
		g = "start",
		k = "move",
		h = "end",
		q = "cancel",
		a = "ontouchstart" in window,
		v = window.navigator.msPointerEnabled && !window.navigator.pointerEnabled && !a,
		d = (window.navigator.pointerEnabled || window.navigator.msPointerEnabled) && !a,
		C = "TouchSwipe";
	var n = {
		fingers: 1,
		threshold: 75,
		cancelThreshold: null,
		pinchThreshold: 20,
		maxTimeThreshold: null,
		fingerReleaseThreshold: 250,
		longTapThreshold: 500,
		doubleTapThreshold: 200,
		swipe: null,
		swipeLeft: null,
		swipeRight: null,
		swipeUp: null,
		swipeDown: null,
		swipeStatus: null,
		pinchIn: null,
		pinchOut: null,
		pinchStatus: null,
		click: null,
		tap: null,
		doubleTap: null,
		longTap: null,
		hold: null,
		triggerOnTouchEnd: true,
		triggerOnTouchLeave: false,
		allowPageScroll: "auto",
		fallbackToMouseEvents: true,
		excludedElements: "label, button, input, select, textarea, .noSwipe",
		preventDefaultEvents: true
	};
	f.fn.swipe = function (H) {
		var G = f(this),
			F = G.data(C);
		if (F && typeof H === "string") {
			if (F[H]) {
				return F[H].apply(this, Array.prototype.slice.call(arguments, 1))
			} else {
				f.error("Method " + H + " does not exist on jQuery.swipe")
			}
		} else {
			if (F && typeof H === "object") {
				F.option.apply(this, arguments)
			} else {
				if (!F && (typeof H === "object" || !H)) {
					return w.apply(this, arguments)
				}
			}
		}
		return G
	};
	f.fn.swipe.version = y;
	f.fn.swipe.defaults = n;
	f.fn.swipe.phases = {
		PHASE_START: g,
		PHASE_MOVE: k,
		PHASE_END: h,
		PHASE_CANCEL: q
	};
	f.fn.swipe.directions = {
		LEFT: p,
		RIGHT: o,
		UP: e,
		DOWN: x,
		IN: c,
		OUT: A
	};
	f.fn.swipe.pageScroll = {
		NONE: m,
		HORIZONTAL: E,
		VERTICAL: u,
		AUTO: s
	};
	f.fn.swipe.fingers = {
		ONE: 1,
		TWO: 2,
		THREE: 3,
		FOUR: 4,
		FIVE: 5,
		ALL: i
	};

	function w(F) {
		if (F && (F.allowPageScroll === undefined && (F.swipe !== undefined || F.swipeStatus !== undefined))) {
			F.allowPageScroll = m
		}
		if (F.click !== undefined && F.tap === undefined) {
			F.tap = F.click
		}
		if (!F) {
			F = {}
		}
		F = f.extend({}, f.fn.swipe.defaults, F);
		return this.each(function () {
			var H = f(this);
			var G = H.data(C);
			if (!G) {
				G = new D(this, F);
				H.data(C, G)
			}
		})
	}

	function D(a5, au) {
		var au = f.extend({}, au);
		var az = (a || d || !au.fallbackToMouseEvents),
			K = az ? (d ? (v ? "MSPointerDown" : "pointerdown") : "touchstart") : "mousedown",
			ax = az ? (d ? (v ? "MSPointerMove" : "pointermove") : "touchmove") : "mousemove",
			V = az ? (d ? (v ? "MSPointerUp" : "pointerup") : "touchend") : "mouseup",
			T = az ? (d ? "mouseleave" : null) : "mouseleave",
			aD = (d ? (v ? "MSPointerCancel" : "pointercancel") : "touchcancel");
		var ag = 0,
			aP = null,
			a2 = null,
			ac = 0,
			a1 = 0,
			aZ = 0,
			H = 1,
			ap = 0,
			aJ = 0,
			N = null;
		var aR = f(a5);
		var aa = "start";
		var X = 0;
		var aQ = {};
		var U = 0,
			a3 = 0,
			a6 = 0,
			ay = 0,
			O = 0;
		var aW = null,
			af = null;
		try {
			aR.bind(K, aN);
			aR.bind(aD, ba)
		} catch (aj) {
			f.error("events not supported " + K + "," + aD + " on jQuery.swipe")
		}
		this.enable = function () {
			aR.bind(K, aN);
			aR.bind(aD, ba);
			return aR
		};
		this.disable = function () {
			aK();
			return aR
		};
		this.destroy = function () {
			aK();
			aR.data(C, null);
			aR = null
		};
		this.option = function (bd, bc) {
			if (typeof bd === "object") {
				au = f.extend(au, bd)
			} else {
				if (au[bd] !== undefined) {
					if (bc === undefined) {
						return au[bd]
					} else {
						au[bd] = bc
					}
				} else {
					if (!bd) {
						return au
					} else {
						f.error("Option " + bd + " does not exist on jQuery.swipe.options")
					}
				}
			}
			return null
		};

		function aN(be) {
			if (aB()) {
				return
			}
			if (f(be.target).closest(au.excludedElements, aR).length > 0) {
				return
			}
			var bf = be.originalEvent ? be.originalEvent : be;
			var bd, bg = bf.touches,
				bc = bg ? bg[0] : bf;
			aa = g;
			if (bg) {
				X = bg.length
			} else {
				if (au.preventDefaultEvents !== false) {
					be.preventDefault()
				}
			}
			ag = 0;
			aP = null;
			a2 = null;
			aJ = null;
			ac = 0;
			a1 = 0;
			aZ = 0;
			H = 1;
			ap = 0;
			N = ab();
			S();
			ai(0, bc);
			if (!bg || (X === au.fingers || au.fingers === i) || aX()) {
				U = ar();
				if (X == 2) {
					ai(1, bg[1]);
					a1 = aZ = at(aQ[0].start, aQ[1].start)
				}
				if (au.swipeStatus || au.pinchStatus) {
					bd = P(bf, aa)
				}
			} else {
				bd = false
			}
			if (bd === false) {
				aa = q;
				P(bf, aa);
				return bd
			} else {
				if (au.hold) {
					af = setTimeout(f.proxy(function () {
						aR.trigger("hold", [bf.target]);
						if (au.hold) {
							bd = au.hold.call(aR, bf, bf.target)
						}
					}, this), au.longTapThreshold)
				}
				an(true)
			}
			return null
		}

		function a4(bf) {
			var bi = bf.originalEvent ? bf.originalEvent : bf;
			if (aa === h || aa === q || al()) {
				return
			}
			var be, bj = bi.touches,
				bd = bj ? bj[0] : bi;
			var bg = aH(bd);
			a3 = ar();
			if (bj) {
				X = bj.length
			}
			if (au.hold) {
				clearTimeout(af)
			}
			aa = k;
			if (X == 2) {
				if (a1 == 0) {
					ai(1, bj[1]);
					a1 = aZ = at(aQ[0].start, aQ[1].start)
				} else {
					aH(bj[1]);
					aZ = at(aQ[0].end, aQ[1].end);
					aJ = aq(aQ[0].end, aQ[1].end)
				}
				H = a8(a1, aZ);
				ap = Math.abs(a1 - aZ)
			}
			if ((X === au.fingers || au.fingers === i) || !bj || aX()) {
				aP = aL(bg.start, bg.end);
				a2 = aL(bg.last, bg.end);
				ak(bf, a2);
				ag = aS(bg.start, bg.end);
				ac = aM();
				aI(aP, ag);
				be = P(bi, aa);
				if (!au.triggerOnTouchEnd || au.triggerOnTouchLeave) {
					var bc = true;
					if (au.triggerOnTouchLeave) {
						var bh = aY(this);
						bc = F(bg.end, bh)
					}
					if (!au.triggerOnTouchEnd && bc) {
						aa = aC(k)
					} else {
						if (au.triggerOnTouchLeave && !bc) {
							aa = aC(h)
						}
					}
					if (aa == q || aa == h) {
						P(bi, aa)
					}
				}
			} else {
				aa = q;
				P(bi, aa)
			}
			if (be === false) {
				aa = q;
				P(bi, aa)
			}
		}

		function M(bc) {
			var bd = bc.originalEvent ? bc.originalEvent : bc,
				be = bd.touches;
			if (be) {
				if (be.length && !al()) {
					G(bd);
					return true
				} else {
					if (be.length && al()) {
						return true
					}
				}
			}
			if (al()) {
				X = ay
			}
			a3 = ar();
			ac = aM();
			if (bb() || !am()) {
				aa = q;
				P(bd, aa)
			} else {
				if (au.triggerOnTouchEnd || (au.triggerOnTouchEnd == false && aa === k)) {
					if (au.preventDefaultEvents !== false) {
						bc.preventDefault()
					}
					aa = h;
					P(bd, aa)
				} else {
					if (!au.triggerOnTouchEnd && a7()) {
						aa = h;
						aF(bd, aa, B)
					} else {
						if (aa === k) {
							aa = q;
							P(bd, aa)
						}
					}
				}
			}
			an(false);
			return null
		}

		function ba() {
			X = 0;
			a3 = 0;
			U = 0;
			a1 = 0;
			aZ = 0;
			H = 1;
			S();
			an(false)
		}

		function L(bc) {
			var bd = bc.originalEvent ? bc.originalEvent : bc;
			if (au.triggerOnTouchLeave) {
				aa = aC(h);
				P(bd, aa)
			}
		}

		function aK() {
			aR.unbind(K, aN);
			aR.unbind(aD, ba);
			aR.unbind(ax, a4);
			aR.unbind(V, M);
			if (T) {
				aR.unbind(T, L)
			}
			an(false)
		}

		function aC(bg) {
			var bf = bg;
			var be = aA();
			var bd = am();
			var bc = bb();
			if (!be || bc) {
				bf = q
			} else {
				if (bd && bg == k && (!au.triggerOnTouchEnd || au.triggerOnTouchLeave)) {
					bf = h
				} else {
					if (!bd && bg == h && au.triggerOnTouchLeave) {
						bf = q
					}
				}
			}
			return bf
		}

		function P(be, bc) {
			var bd, bf = be.touches;
			if (J() || W()) {
				bd = aF(be, bc, l)
			}
			if ((Q() || aX()) && bd !== false) {
				bd = aF(be, bc, t)
			}
			if (aG() && bd !== false) {
				bd = aF(be, bc, j)
			} else {
				if (ao() && bd !== false) {
					bd = aF(be, bc, b)
				} else {
					if (ah() && bd !== false) {
						bd = aF(be, bc, B)
					}
				}
			}
			if (bc === q) {
				if (W()) {
					bd = aF(be, bc, l)
				}
				if (aX()) {
					bd = aF(be, bc, t)
				}
				ba(be)
			}
			if (bc === h) {
				if (bf) {
					if (!bf.length) {
						ba(be)
					}
				} else {
					ba(be)
				}
			}
			return bd
		}

		function aF(bf, bc, be) {
			var bd;
			if (be == l) {
				aR.trigger("swipeStatus", [bc, aP || null, ag || 0, ac || 0, X, aQ, a2]);
				if (au.swipeStatus) {
					bd = au.swipeStatus.call(aR, bf, bc, aP || null, ag || 0, ac || 0, X, aQ, a2);
					if (bd === false) {
						return false
					}
				}
				if (bc == h && aV()) {
					clearTimeout(aW);
					clearTimeout(af);
					aR.trigger("swipe", [aP, ag, ac, X, aQ, a2]);
					if (au.swipe) {
						bd = au.swipe.call(aR, bf, aP, ag, ac, X, aQ, a2);
						if (bd === false) {
							return false
						}
					}
					switch (aP) {
						case p:
							aR.trigger("swipeLeft", [aP, ag, ac, X, aQ, a2]);
							if (au.swipeLeft) {
								bd = au.swipeLeft.call(aR, bf, aP, ag, ac, X, aQ, a2)
							}
							break;
						case o:
							aR.trigger("swipeRight", [aP, ag, ac, X, aQ, a2]);
							if (au.swipeRight) {
								bd = au.swipeRight.call(aR, bf, aP, ag, ac, X, aQ, a2)
							}
							break;
						case e:
							aR.trigger("swipeUp", [aP, ag, ac, X, aQ, a2]);
							if (au.swipeUp) {
								bd = au.swipeUp.call(aR, bf, aP, ag, ac, X, aQ, a2)
							}
							break;
						case x:
							aR.trigger("swipeDown", [aP, ag, ac, X, aQ, a2]);
							if (au.swipeDown) {
								bd = au.swipeDown.call(aR, bf, aP, ag, ac, X, aQ, a2)
							}
							break
					}
				}
			}
			if (be == t) {
				aR.trigger("pinchStatus", [bc, aJ || null, ap || 0, ac || 0, X, H, aQ]);
				if (au.pinchStatus) {
					bd = au.pinchStatus.call(aR, bf, bc, aJ || null, ap || 0, ac || 0, X, H, aQ);
					if (bd === false) {
						return false
					}
				}
				if (bc == h && a9()) {
					switch (aJ) {
						case c:
							aR.trigger("pinchIn", [aJ || null, ap || 0, ac || 0, X, H, aQ]);
							if (au.pinchIn) {
								bd = au.pinchIn.call(aR, bf, aJ || null, ap || 0, ac || 0, X, H, aQ)
							}
							break;
						case A:
							aR.trigger("pinchOut", [aJ || null, ap || 0, ac || 0, X, H, aQ]);
							if (au.pinchOut) {
								bd = au.pinchOut.call(aR, bf, aJ || null, ap || 0, ac || 0, X, H, aQ)
							}
							break
					}
				}
			}
			if (be == B) {
				if (bc === q || bc === h) {
					clearTimeout(aW);
					clearTimeout(af);
					if (Z() && !I()) {
						O = ar();
						aW = setTimeout(f.proxy(function () {
							O = null;
							aR.trigger("tap", [bf.target]);
							if (au.tap) {
								bd = au.tap.call(aR, bf, bf.target)
							}
						}, this), au.doubleTapThreshold)
					} else {
						O = null;
						aR.trigger("tap", [bf.target]);
						if (au.tap) {
							bd = au.tap.call(aR, bf, bf.target)
						}
					}
				}
			} else {
				if (be == j) {
					if (bc === q || bc === h) {
						clearTimeout(aW);
						clearTimeout(af);
						O = null;
						aR.trigger("doubletap", [bf.target]);
						if (au.doubleTap) {
							bd = au.doubleTap.call(aR, bf, bf.target)
						}
					}
				} else {
					if (be == b) {
						if (bc === q || bc === h) {
							clearTimeout(aW);
							O = null;
							aR.trigger("longtap", [bf.target]);
							if (au.longTap) {
								bd = au.longTap.call(aR, bf, bf.target)
							}
						}
					}
				}
			}
			return bd
		}

		function am() {
			var bc = true;
			if (au.threshold !== null) {
				bc = ag >= au.threshold
			}
			return bc
		}

		function bb() {
			var bc = false;
			if (au.cancelThreshold !== null && aP !== null) {
				bc = (aT(aP) - ag) >= au.cancelThreshold
			}
			return bc
		}

		function ae() {
			if (au.pinchThreshold !== null) {
				return ap >= au.pinchThreshold
			}
			return true
		}

		function aA() {
			var bc;
			if (au.maxTimeThreshold) {
				if (ac >= au.maxTimeThreshold) {
					bc = false
				} else {
					bc = true
				}
			} else {
				bc = true
			}
			return bc
		}

		function ak(bc, bd) {
			if (au.preventDefaultEvents === false) {
				return
			}
			if (au.allowPageScroll === m) {
				bc.preventDefault()
			} else {
				var be = au.allowPageScroll === s;
				switch (bd) {
					case p:
						if ((au.swipeLeft && be) || (!be && au.allowPageScroll != E)) {
							bc.preventDefault()
						}
						break;
					case o:
						if ((au.swipeRight && be) || (!be && au.allowPageScroll != E)) {
							bc.preventDefault()
						}
						break;
					case e:
						if ((au.swipeUp && be) || (!be && au.allowPageScroll != u)) {
							bc.preventDefault()
						}
						break;
					case x:
						if ((au.swipeDown && be) || (!be && au.allowPageScroll != u)) {
							bc.preventDefault()
						}
						break
				}
			}
		}

		function a9() {
			var bd = aO();
			var bc = Y();
			var be = ae();
			return bd && bc && be
		}

		function aX() {
			return !!(au.pinchStatus || au.pinchIn || au.pinchOut)
		}

		function Q() {
			return !!(a9() && aX())
		}

		function aV() {
			var bf = aA();
			var bh = am();
			var be = aO();
			var bc = Y();
			var bd = bb();
			var bg = !bd && bc && be && bh && bf;
			return bg
		}

		function W() {
			return !!(au.swipe || au.swipeStatus || au.swipeLeft || au.swipeRight || au.swipeUp || au.swipeDown)
		}

		function J() {
			return !!(aV() && W())
		}

		function aO() {
			return ((X === au.fingers || au.fingers === i) || !a)
		}

		function Y() {
			return aQ[0].end.x !== 0
		}

		function a7() {
			return !!(au.tap)
		}

		function Z() {
			return !!(au.doubleTap)
		}

		function aU() {
			return !!(au.longTap)
		}

		function R() {
			if (O == null) {
				return false
			}
			var bc = ar();
			return (Z() && ((bc - O) <= au.doubleTapThreshold))
		}

		function I() {
			return R()
		}

		function aw() {
			return ((X === 1 || !a) && (isNaN(ag) || ag < au.threshold))
		}

		function a0() {
			return ((ac > au.longTapThreshold) && (ag < r))
		}

		function ah() {
			return !!(aw() && a7())
		}

		function aG() {
			return !!(R() && Z())
		}

		function ao() {
			return !!(a0() && aU())
		}

		function G(bc) {
			a6 = ar();
			ay = bc.touches.length + 1
		}

		function S() {
			a6 = 0;
			ay = 0
		}

		function al() {
			var bc = false;
			if (a6) {
				var bd = ar() - a6;
				if (bd <= au.fingerReleaseThreshold) {
					bc = true
				}
			}
			return bc
		}

		function aB() {
			return !!(aR.data(C + "_intouch") === true)
		}

		function an(bc) {
			if (!aR) {
				return
			}
			if (bc === true) {
				aR.bind(ax, a4);
				aR.bind(V, M);
				if (T) {
					aR.bind(T, L)
				}
			} else {
				aR.unbind(ax, a4, false);
				aR.unbind(V, M, false);
				if (T) {
					aR.unbind(T, L, false)
				}
			}
			aR.data(C + "_intouch", bc === true)
		}

		function ai(be, bc) {
			var bd = {
				start: {
					x: 0,
					y: 0
				},
				last: {
					x: 0,
					y: 0
				},
				end: {
					x: 0,
					y: 0
				}
			};
			bd.start.x = bd.last.x = bd.end.x = bc.pageX || bc.clientX;
			bd.start.y = bd.last.y = bd.end.y = bc.pageY || bc.clientY;
			aQ[be] = bd;
			return bd
		}

		function aH(bc) {
			var be = bc.identifier !== undefined ? bc.identifier : 0;
			var bd = ad(be);
			if (bd === null) {
				bd = ai(be, bc)
			}
			bd.last.x = bd.end.x;
			bd.last.y = bd.end.y;
			bd.end.x = bc.pageX || bc.clientX;
			bd.end.y = bc.pageY || bc.clientY;
			return bd
		}

		function ad(bc) {
			return aQ[bc] || null
		}

		function aI(bc, bd) {
			bd = Math.max(bd, aT(bc));
			N[bc].distance = bd
		}

		function aT(bc) {
			if (N[bc]) {
				return N[bc].distance
			}
			return undefined
		}

		function ab() {
			var bc = {};
			bc[p] = av(p);
			bc[o] = av(o);
			bc[e] = av(e);
			bc[x] = av(x);
			return bc
		}

		function av(bc) {
			return {
				direction: bc,
				distance: 0
			}
		}

		function aM() {
			return a3 - U
		}

		function at(bf, be) {
			var bd = Math.abs(bf.x - be.x);
			var bc = Math.abs(bf.y - be.y);
			return Math.round(Math.sqrt(bd * bd + bc * bc))
		}

		function a8(bc, bd) {
			var be = (bd / bc) * 1;
			return be.toFixed(2)
		}

		function aq() {
			if (H < 1) {
				return A
			} else {
				return c
			}
		}

		function aS(bd, bc) {
			return Math.round(Math.sqrt(Math.pow(bc.x - bd.x, 2) + Math.pow(bc.y - bd.y, 2)))
		}

		function aE(bf, bd) {
			var bc = bf.x - bd.x;
			var bh = bd.y - bf.y;
			var be = Math.atan2(bh, bc);
			var bg = Math.round(be * 180 / Math.PI);
			if (bg < 0) {
				bg = 360 - Math.abs(bg)
			}
			return bg
		}

		function aL(bd, bc) {
			var be = aE(bd, bc);
			if ((be <= 45) && (be >= 0)) {
				return p
			} else {
				if ((be <= 360) && (be >= 315)) {
					return p
				} else {
					if ((be >= 135) && (be <= 225)) {
						return o
					} else {
						if ((be > 45) && (be < 135)) {
							return x
						} else {
							return e
						}
					}
				}
			}
		}

		function ar() {
			var bc = new Date();
			return bc.getTime()
		}

		function aY(bc) {
			bc = f(bc);
			var be = bc.offset();
			var bd = {
				left: be.left,
				right: be.left + bc.outerWidth(),
				top: be.top,
				bottom: be.top + bc.outerHeight()
			};
			return bd
		}

		function F(bc, bd) {
			return (bc.x > bd.left && bc.x < bd.right && bc.y > bd.top && bc.y < bd.bottom)
		}
	}
}));;
(function ($) {
	function sc_setScroll(a, b, c) {
		return "transition" == c.transition && "swing" == b && (b = "ease"), {
			anims: [],
			duration: a,
			orgDuration: a,
			easing: b,
			startTime: getTime()
		}
	}

	function sc_startScroll(a, b) {
		for (var c = 0, d = a.anims.length; d > c; c++) {
			var e = a.anims[c];
			e && e[0][b.transition](e[1], a.duration, a.easing, e[2])
		}
	}

	function sc_stopScroll(a, b) {
		is_boolean(b) || (b = !0), is_object(a.pre) && sc_stopScroll(a.pre, b);
		for (var c = 0, d = a.anims.length; d > c; c++) {
			var e = a.anims[c];
			e[0].stop(!0), b && (e[0].css(e[1]), is_function(e[2]) && e[2]())
		}
		is_object(a.post) && sc_stopScroll(a.post, b)
	}

	function sc_afterScroll(a, b, c) {
		switch (b && b.remove(), c.fx) {
			case "fade":
			case "crossfade":
			case "cover-fade":
			case "uncover-fade":
				a.css("opacity", 1), a.css("filter", "")
		}
	}

	function sc_fireCallbacks(a, b, c, d, e) {
		if (b[c] && b[c].call(a, d), e[c].length)
			for (var f = 0, g = e[c].length; g > f; f++) e[c][f].call(a, d);
		return []
	}

	function sc_fireQueue(a, b, c) {
		return b.length && (a.trigger(cf_e(b[0][0], c), b[0][1]), b.shift()), b
	}

	function sc_hideHiddenItems(a) {
		a.each(function () {
			var a = $(this);
			a.data("_cfs_isHidden", a.is(":hidden")).hide()
		})
	}

	function sc_showHiddenItems(a) {
		a && a.each(function () {
			var a = $(this);
			a.data("_cfs_isHidden") || a.show()
		})
	}

	function sc_clearTimers(a) {
		return a.auto && clearTimeout(a.auto), a.progress && clearInterval(a.progress), a
	}

	function sc_mapCallbackArguments(a, b, c, d, e, f, g) {
		return {
			width: g.width,
			height: g.height,
			items: {
				old: a,
				skipped: b,
				visible: c
			},
			scroll: {
				items: d,
				direction: e,
				duration: f
			}
		}
	}

	function sc_getDuration(a, b, c, d) {
		var e = a.duration;
		return "none" == a.fx ? 0 : ("auto" == e ? e = b.scroll.duration / b.scroll.items * c : 10 > e && (e = d / e), 1 > e ? 0 : ("fade" == a.fx && (e /= 2), Math.round(e)))
	}

	function nv_showNavi(a, b, c) {
		var d = is_number(a.items.minimum) ? a.items.minimum : a.items.visible + 1;
		if ("show" == b || "hide" == b) var e = b;
		else if (d > b) {
			debug(c, "Not enough items (" + b + " total, " + d + " needed): Hiding navigation.");
			var e = "hide"
		} else var e = "show";
		var f = "show" == e ? "removeClass" : "addClass",
			g = cf_c("hidden", c);
		a.auto.button && a.auto.button[e]()[f](g), a.prev.button && a.prev.button[e]()[f](g), a.next.button && a.next.button[e]()[f](g), a.pagination.container && a.pagination.container[e]()[f](g)
	}

	function nv_enableNavi(a, b, c) {
		if (!a.circular && !a.infinite) {
			var d = "removeClass" == b || "addClass" == b ? b : !1,
				e = cf_c("disabled", c);
			if (a.auto.button && d && a.auto.button[d](e), a.prev.button) {
				var f = d || 0 == b ? "addClass" : "removeClass";
				a.prev.button[f](e)
			}
			if (a.next.button) {
				var f = d || b == a.items.visible ? "addClass" : "removeClass";
				a.next.button[f](e)
			}
		}
	}

	function go_getObject(a, b) {
		return is_function(b) ? b = b.call(a) : is_undefined(b) && (b = {}), b
	}

	function go_getItemsObject(a, b) {
		return b = go_getObject(a, b), is_number(b) ? b = {
			visible: b
		} : "variable" == b ? b = {
			visible: b,
			width: b,
			height: b
		} : is_object(b) || (b = {}), b
	}

	function go_getScrollObject(a, b) {
		return b = go_getObject(a, b), is_number(b) ? b = 50 >= b ? {
			items: b
		} : {
			duration: b
		} : is_string(b) ? b = {
			easing: b
		} : is_object(b) || (b = {}), b
	}

	function go_getNaviObject(a, b) {
		if (b = go_getObject(a, b), is_string(b)) {
			var c = cf_getKeyCode(b);
			b = -1 == c ? $(b) : c
		}
		return b
	}

	function go_getAutoObject(a, b) {
		return b = go_getNaviObject(a, b), is_jquery(b) ? b = {
			button: b
		} : is_boolean(b) ? b = {
			play: b
		} : is_number(b) && (b = {
			timeoutDuration: b
		}), b.progress && (is_string(b.progress) || is_jquery(b.progress)) && (b.progress = {
			bar: b.progress
		}), b
	}

	function go_complementAutoObject(a, b) {
		return is_function(b.button) && (b.button = b.button.call(a)), is_string(b.button) && (b.button = $(b.button)), is_boolean(b.play) || (b.play = !0), is_number(b.delay) || (b.delay = 0), is_undefined(b.pauseOnEvent) && (b.pauseOnEvent = !0), is_boolean(b.pauseOnResize) || (b.pauseOnResize = !0), is_number(b.timeoutDuration) || (b.timeoutDuration = 10 > b.duration ? 2500 : 5 * b.duration), b.progress && (is_function(b.progress.bar) && (b.progress.bar = b.progress.bar.call(a)), is_string(b.progress.bar) && (b.progress.bar = $(b.progress.bar)), b.progress.bar ? (is_function(b.progress.updater) || (b.progress.updater = $.fn.carouFredSel.progressbarUpdater), is_number(b.progress.interval) || (b.progress.interval = 50)) : b.progress = !1), b
	}

	function go_getPrevNextObject(a, b) {
		return b = go_getNaviObject(a, b), is_jquery(b) ? b = {
			button: b
		} : is_number(b) && (b = {
			key: b
		}), b
	}

	function go_complementPrevNextObject(a, b) {
		return is_function(b.button) && (b.button = b.button.call(a)), is_string(b.button) && (b.button = $(b.button)), is_string(b.key) && (b.key = cf_getKeyCode(b.key)), b
	}

	function go_getPaginationObject(a, b) {
		return b = go_getNaviObject(a, b), is_jquery(b) ? b = {
			container: b
		} : is_boolean(b) && (b = {
			keys: b
		}), b
	}

	function go_complementPaginationObject(a, b) {
		return is_function(b.container) && (b.container = b.container.call(a)), is_string(b.container) && (b.container = $(b.container)), is_number(b.items) || (b.items = !1), is_boolean(b.keys) || (b.keys = !1), is_function(b.anchorBuilder) || is_false(b.anchorBuilder) || (b.anchorBuilder = $.fn.carouFredSel.pageAnchorBuilder), is_number(b.deviation) || (b.deviation = 0), b
	}

	function go_getSwipeObject(a, b) {
		return is_function(b) && (b = b.call(a)), is_undefined(b) && (b = {
			onTouch: !1
		}), is_true(b) ? b = {
			onTouch: b
		} : is_number(b) && (b = {
			items: b
		}), b
	}

	function go_complementSwipeObject(a, b) {
		return is_boolean(b.onTouch) || (b.onTouch = !0), is_boolean(b.onMouse) || (b.onMouse = !1), is_object(b.options) || (b.options = {}), is_boolean(b.options.triggerOnTouchEnd) || (b.options.triggerOnTouchEnd = !1), b
	}

	function go_getMousewheelObject(a, b) {
		return is_function(b) && (b = b.call(a)), is_true(b) ? b = {} : is_number(b) ? b = {
			items: b
		} : is_undefined(b) && (b = !1), b
	}

	function go_complementMousewheelObject(a, b) {
		return b
	}

	function gn_getItemIndex(a, b, c, d, e) {
		if (is_string(a) && (a = $(a, e)), is_object(a) && (a = $(a, e)), is_jquery(a) ? (a = e.children().index(a), is_boolean(c) || (c = !1)) : is_boolean(c) || (c = !0), is_number(a) || (a = 0), is_number(b) || (b = 0), c && (a += d.first), a += b, d.total > 0) {
			for (; a >= d.total;) a -= d.total;
			for (; 0 > a;) a += d.total
		}
		return a
	}

	function gn_getVisibleItemsPrev(a, b, c) {
		for (var d = 0, e = 0, f = c; f >= 0; f--) {
			var g = a.eq(f);
			if (d += g.is(":visible") ? g[b.d.outerWidth](!0) : 0, d > b.maxDimension) return e;
			0 == f && (f = a.length), e++
		}
	}

	function gn_getVisibleItemsPrevFilter(a, b, c) {
		return gn_getItemsPrevFilter(a, b.items.filter, b.items.visibleConf.org, c)
	}

	function gn_getScrollItemsPrevFilter(a, b, c, d) {
		return gn_getItemsPrevFilter(a, b.items.filter, d, c)
	}

	function gn_getItemsPrevFilter(a, b, c, d) {
		for (var e = 0, f = 0, g = d, h = a.length; g >= 0; g--) {
			if (f++, f == h) return f;
			var i = a.eq(g);
			if (i.is(b) && (e++, e == c)) return f;
			0 == g && (g = h)
		}
	}

	function gn_getVisibleOrg(a, b) {
		return b.items.visibleConf.org || a.children().slice(0, b.items.visible).filter(b.items.filter).length
	}

	function gn_getVisibleItemsNext(a, b, c) {
		for (var d = 0, e = 0, f = c, g = a.length - 1; g >= f; f++) {
			var h = a.eq(f);
			if (d += h.is(":visible") ? h[b.d.outerWidth](!0) : 0, d > b.maxDimension) return e;
			if (e++, e == g + 1) return e;
			f == g && (f = -1)
		}
	}

	function gn_getVisibleItemsNextTestCircular(a, b, c, d) {
		var e = gn_getVisibleItemsNext(a, b, c);
		return b.circular || c + e > d && (e = d - c), e
	}

	function gn_getVisibleItemsNextFilter(a, b, c) {
		return gn_getItemsNextFilter(a, b.items.filter, b.items.visibleConf.org, c, b.circular)
	}

	function gn_getScrollItemsNextFilter(a, b, c, d) {
		return gn_getItemsNextFilter(a, b.items.filter, d + 1, c, b.circular) - 1
	}

	function gn_getItemsNextFilter(a, b, c, d) {
		for (var f = 0, g = 0, h = d, i = a.length - 1; i >= h; h++) {
			if (g++, g >= i) return g;
			var j = a.eq(h);
			if (j.is(b) && (f++, f == c)) return g;
			h == i && (h = -1)
		}
	}

	function gi_getCurrentItems(a, b) {
		return a.slice(0, b.items.visible)
	}

	function gi_getOldItemsPrev(a, b, c) {
		return a.slice(c, b.items.visibleConf.old + c)
	}

	function gi_getNewItemsPrev(a, b) {
		return a.slice(0, b.items.visible)
	}

	function gi_getOldItemsNext(a, b) {
		return a.slice(0, b.items.visibleConf.old)
	}

	function gi_getNewItemsNext(a, b, c) {
		return a.slice(c, b.items.visible + c)
	}

	function sz_storeMargin(a, b, c) {
		b.usePadding && (is_string(c) || (c = "_cfs_origCssMargin"), a.each(function () {
			var a = $(this),
				d = parseInt(a.css(b.d.marginRight), 10);
			is_number(d) || (d = 0), a.data(c, d)
		}))
	}

	function sz_resetMargin(a, b, c) {
		if (b.usePadding) {
			var d = is_boolean(c) ? c : !1;
			is_number(c) || (c = 0), sz_storeMargin(a, b, "_cfs_tempCssMargin"), a.each(function () {
				var a = $(this);
				a.css(b.d.marginRight, d ? a.data("_cfs_tempCssMargin") : c + a.data("_cfs_origCssMargin"))
			})
		}
	}

	function sz_storeOrigCss(a) {
		a.each(function () {
			var a = $(this);
			a.data("_cfs_origCss", a.attr("style") || "")
		})
	}

	function sz_restoreOrigCss(a) {
		a.each(function () {
			var a = $(this);
			a.attr("style", a.data("_cfs_origCss") || "")
		})
	}

	function sz_setResponsiveSizes(a, b) {
		var d = (a.items.visible, a.items[a.d.width]),
			e = a[a.d.height],
			f = is_percentage(e);
		b.each(function () {
			var b = $(this),
				c = d - ms_getPaddingBorderMargin(b, a, "Width");
			b[a.d.width](c), f && b[a.d.height](ms_getPercentage(c, e))
		})
	}

	function sz_setSizes(a, b) {
		var c = a.parent(),
			d = a.children(),
			e = gi_getCurrentItems(d, b),
			f = cf_mapWrapperSizes(ms_getSizes(e, b, !0), b, !1);
		if (c.css(f), b.usePadding) {
			var g = b.padding,
				h = g[b.d[1]];
			b.align && 0 > h && (h = 0);
			var i = e.last();
			i.css(b.d.marginRight, i.data("_cfs_origCssMargin") + h), a.css(b.d.top, g[b.d[0]]), a.css(b.d.left, g[b.d[3]])
		}
		return a.css(b.d.width, f[b.d.width] + 2 * ms_getTotalSize(d, b, "width")), a.css(b.d.height, ms_getLargestSize(d, b, "height")), f
	}

	function ms_getSizes(a, b, c) {
		return [ms_getTotalSize(a, b, "width", c), ms_getLargestSize(a, b, "height", c)]
	}

	function ms_getLargestSize(a, b, c, d) {
		return is_boolean(d) || (d = !1), is_number(b[b.d[c]]) && d ? b[b.d[c]] : is_number(b.items[b.d[c]]) ? b.items[b.d[c]] : (c = c.toLowerCase().indexOf("width") > -1 ? "outerWidth" : "outerHeight", ms_getTrueLargestSize(a, b, c))
	}

	function ms_getTrueLargestSize(a, b, c) {
		for (var d = 0, e = 0, f = a.length; f > e; e++) {
			var g = a.eq(e),
				h = g.is(":visible") ? g[b.d[c]](!0) : 0;
			h > d && (d = h)
		}
		return d
	}

	function ms_getTotalSize(a, b, c, d) {
		if (is_boolean(d) || (d = !1), is_number(b[b.d[c]]) && d) return b[b.d[c]];
		if (is_number(b.items[b.d[c]])) return b.items[b.d[c]] * a.length;
		for (var e = c.toLowerCase().indexOf("width") > -1 ? "outerWidth" : "outerHeight", f = 0, g = 0, h = a.length; h > g; g++) {
			var i = a.eq(g);
			f += i.is(":visible") ? i[b.d[e]](!0) : 0
		}
		return f
	}

	function ms_getParentSize(a, b, c) {
		var d = a.is(":visible");
		d && a.hide();
		var e = a.parent()[b.d[c]]();
		return d && a.show(), e
	}

	function ms_getMaxDimension(a, b) {
		return is_number(a[a.d.width]) ? a[a.d.width] : b
	}

	function ms_hasVariableSizes(a, b, c) {
		for (var d = !1, e = !1, f = 0, g = a.length; g > f; f++) {
			var h = a.eq(f),
				i = h.is(":visible") ? h[b.d[c]](!0) : 0;
			d === !1 ? d = i : d != i && (e = !0), 0 == d && (e = !0)
		}
		return e
	}

	function ms_getPaddingBorderMargin(a, b, c) {
		return a[b.d["outer" + c]](!0) - a[b.d[c.toLowerCase()]]()
	}

	function ms_getPercentage(a, b) {
		if (is_percentage(b)) {
			if (b = parseInt(b.slice(0, -1), 10), !is_number(b)) return a;
			a *= b / 100
		}
		return a
	}

	function cf_e(a, b, c, d, e) {
		return is_boolean(c) || (c = !0), is_boolean(d) || (d = !0), is_boolean(e) || (e = !1), c && (a = b.events.prefix + a), d && (a = a + "." + b.events.namespace), d && e && (a += b.serialNumber), a
	}

	function cf_c(a, b) {
		return is_string(b.classnames[a]) ? b.classnames[a] : a
	}

	function cf_mapWrapperSizes(a, b, c) {
		is_boolean(c) || (c = !0);
		var d = b.usePadding && c ? b.padding : [0, 0, 0, 0],
			e = {};
		return e[b.d.width] = a[0] + d[1] + d[3], e[b.d.height] = a[1] + d[0] + d[2], e
	}

	function cf_sortParams(a, b) {
		for (var c = [], d = 0, e = a.length; e > d; d++)
			for (var f = 0, g = b.length; g > f; f++)
				if (b[f].indexOf(typeof a[d]) > -1 && is_undefined(c[f])) {
					c[f] = a[d];
					break
				}
		return c
	}

	function cf_getPadding(a) {
		if (is_undefined(a)) return [0, 0, 0, 0];
		if (is_number(a)) return [a, a, a, a];
		if (is_string(a) && (a = a.split("px").join("").split("em").join("").split(" ")), !is_array(a)) return [0, 0, 0, 0];
		for (var b = 0; 4 > b; b++) a[b] = parseInt(a[b], 10);
		switch (a.length) {
			case 0:
				return [0, 0, 0, 0];
			case 1:
				return [a[0], a[0], a[0], a[0]];
			case 2:
				return [a[0], a[1], a[0], a[1]];
			case 3:
				return [a[0], a[1], a[2], a[1]];
			default:
				return [a[0], a[1], a[2], a[3]]
		}
	}

	function cf_getAlignPadding(a, b) {
		var c = is_number(b[b.d.width]) ? Math.ceil(b[b.d.width] - ms_getTotalSize(a, b, "width")) : 0;
		switch (b.align) {
			case "left":
				return [0, c];
			case "right":
				return [c, 0];
			case "center":
			default:
				return [Math.ceil(c / 2), Math.floor(c / 2)]
		}
	}

	function cf_getDimensions(a) {
		for (var b = [["width", "innerWidth", "outerWidth", "height", "innerHeight", "outerHeight", "left", "top", "marginRight", 0, 1, 2, 3], ["height", "innerHeight", "outerHeight", "width", "innerWidth", "outerWidth", "top", "left", "marginBottom", 3, 2, 1, 0]], c = b[0].length, d = "right" == a.direction || "left" == a.direction ? 0 : 1, e = {}, f = 0; c > f; f++) e[b[0][f]] = b[d][f];
		return e
	}

	function cf_getAdjust(a, b, c, d) {
		var e = a;
		if (is_function(c)) e = c.call(d, e);
		else if (is_string(c)) {
			var f = c.split("+"),
				g = c.split("-");
			if (g.length > f.length) var h = !0,
				i = g[0],
				j = g[1];
			else var h = !1,
				i = f[0],
				j = f[1];
			switch (i) {
				case "even":
					e = 1 == a % 2 ? a - 1 : a;
					break;
				case "odd":
					e = 0 == a % 2 ? a - 1 : a;
					break;
				default:
					e = a
			}
			j = parseInt(j, 10), is_number(j) && (h && (j = -j), e += j)
		}
		return (!is_number(e) || 1 > e) && (e = 1), e
	}

	function cf_getItemsAdjust(a, b, c, d) {
		return cf_getItemAdjustMinMax(cf_getAdjust(a, b, c, d), b.items.visibleConf)
	}

	function cf_getItemAdjustMinMax(a, b) {
		return is_number(b.min) && b.min > a && (a = b.min), is_number(b.max) && a > b.max && (a = b.max), 1 > a && (a = 1), a
	}

	function cf_getSynchArr(a) {
		is_array(a) || (a = [[a]]), is_array(a[0]) || (a = [a]);
		for (var b = 0, c = a.length; c > b; b++) is_string(a[b][0]) && (a[b][0] = $(a[b][0])), is_boolean(a[b][1]) || (a[b][1] = !0), is_boolean(a[b][2]) || (a[b][2] = !0), is_number(a[b][3]) || (a[b][3] = 0);
		return a
	}

	function cf_getKeyCode(a) {
		return "right" == a ? 39 : "left" == a ? 37 : "up" == a ? 38 : "down" == a ? 40 : -1
	}

	function cf_setCookie(a, b, c) {
		if (a) {
			var d = b.triggerHandler(cf_e("currentPosition", c));
			$.fn.carouFredSel.cookie.set(a, d)
		}
	}

	function cf_getCookie(a) {
		var b = $.fn.carouFredSel.cookie.get(a);
		return "" == b ? 0 : b
	}

	function in_mapCss(a, b) {
		for (var c = {}, d = 0, e = b.length; e > d; d++) c[b[d]] = a.css(b[d]);
		return c
	}

	function in_complementItems(a, b, c, d) {
		return is_object(a.visibleConf) || (a.visibleConf = {}), is_object(a.sizesConf) || (a.sizesConf = {}), 0 == a.start && is_number(d) && (a.start = d), is_object(a.visible) ? (a.visibleConf.min = a.visible.min, a.visibleConf.max = a.visible.max, a.visible = !1) : is_string(a.visible) ? ("variable" == a.visible ? a.visibleConf.variable = !0 : a.visibleConf.adjust = a.visible, a.visible = !1) : is_function(a.visible) && (a.visibleConf.adjust = a.visible, a.visible = !1), is_string(a.filter) || (a.filter = c.filter(":hidden").length > 0 ? ":visible" : "*"), a[b.d.width] || (b.responsive ? (debug(!0, "Set a " + b.d.width + " for the items!"), a[b.d.width] = ms_getTrueLargestSize(c, b, "outerWidth")) : a[b.d.width] = ms_hasVariableSizes(c, b, "outerWidth") ? "variable" : c[b.d.outerWidth](!0)), a[b.d.height] || (a[b.d.height] = ms_hasVariableSizes(c, b, "outerHeight") ? "variable" : c[b.d.outerHeight](!0)), a.sizesConf.width = a.width, a.sizesConf.height = a.height, a
	}

	function in_complementVisibleItems(a, b) {
		return "variable" == a.items[a.d.width] && (a.items.visibleConf.variable = !0), a.items.visibleConf.variable || (is_number(a[a.d.width]) ? a.items.visible = Math.floor(a[a.d.width] / a.items[a.d.width]) : (a.items.visible = Math.floor(b / a.items[a.d.width]), a[a.d.width] = a.items.visible * a.items[a.d.width], a.items.visibleConf.adjust || (a.align = !1)), ("Infinity" == a.items.visible || 1 > a.items.visible) && (debug(!0, 'Not a valid number of visible items: Set to "variable".'), a.items.visibleConf.variable = !0)), a
	}

	function in_complementPrimarySize(a, b, c) {
		return "auto" == a && (a = ms_getTrueLargestSize(c, b, "outerWidth")), a
	}

	function in_complementSecondarySize(a, b, c) {
		return "auto" == a && (a = ms_getTrueLargestSize(c, b, "outerHeight")), a || (a = b.items[b.d.height]), a
	}

	function in_getAlignPadding(a, b) {
		var c = cf_getAlignPadding(gi_getCurrentItems(b, a), a);
		return a.padding[a.d[1]] = c[1], a.padding[a.d[3]] = c[0], a
	}

	function in_getResponsiveValues(a, b) {
		var d = cf_getItemAdjustMinMax(Math.ceil(a[a.d.width] / a.items[a.d.width]), a.items.visibleConf);
		d > b.length && (d = b.length);
		var e = Math.floor(a[a.d.width] / d);
		return a.items.visible = d, a.items[a.d.width] = e, a[a.d.width] = d * e, a
	}

	function bt_pauseOnHoverConfig(a) {
		if (is_string(a)) var b = a.indexOf("immediate") > -1 ? !0 : !1,
			c = a.indexOf("resume") > -1 ? !0 : !1;
		else var b = c = !1;
		return [b, c]
	}

	function bt_mousesheelNumber(a) {
		return is_number(a) ? a : null
	}

	function is_null(a) {
		return null === a
	}

	function is_undefined(a) {
		return is_null(a) || a === void 0 || "" === a || "undefined" === a
	}

	function is_array(a) {
		return a instanceof Array
	}

	function is_jquery(a) {
		return a instanceof jQuery
	}

	function is_object(a) {
		return (a instanceof Object || "object" == typeof a) && !is_null(a) && !is_jquery(a) && !is_array(a) && !is_function(a)
	}

	function is_number(a) {
		return (a instanceof Number || "number" == typeof a) && !isNaN(a)
	}

	function is_string(a) {
		return (a instanceof String || "string" == typeof a) && !is_undefined(a) && !is_true(a) && !is_false(a)
	}

	function is_function(a) {
		return a instanceof Function || "function" == typeof a
	}

	function is_boolean(a) {
		return a instanceof Boolean || "boolean" == typeof a || is_true(a) || is_false(a)
	}

	function is_true(a) {
		return a === !0 || "true" === a
	}

	function is_false(a) {
		return a === !1 || "false" === a
	}

	function is_percentage(a) {
		return is_string(a) && "%" == a.slice(-1)
	}

	function getTime() {
		return (new Date).getTime()
	}

	function deprecated(a, b) {
		debug(!0, a + " is DEPRECATED, support for it will be removed. Use " + b + " instead.")
	}

	function debug(a, b) {
		if (!is_undefined(window.console) && !is_undefined(window.console.log)) {
			if (is_object(a)) {
				var c = " (" + a.selector + ")";
				a = a.debug
			} else var c = "";
			if (!a) return !1;
			b = is_string(b) ? "carouFredSel" + c + ": " + b : ["carouFredSel" + c + ":", b], window.console.log(b)
		}
		return !1
	}
	$.fn.carouFredSel || ($.fn.caroufredsel = $.fn.carouFredSel = function (options, configs) {
		if (0 == this.length) return debug(!0, 'No element found for "' + this.selector + '".'), this;
		if (this.length > 1) return this.each(function () {
			$(this).carouFredSel(options, configs)
		});
		var $cfs = this,
			$tt0 = this[0],
			starting_position = !1;
		$cfs.data("_cfs_isCarousel") && (starting_position = $cfs.triggerHandler("_cfs_triggerEvent", "currentPosition"), $cfs.trigger("_cfs_triggerEvent", ["destroy", !0]));
		var FN = {};
		FN._init = function (a, b, c) {
			a = go_getObject($tt0, a), a.items = go_getItemsObject($tt0, a.items), a.scroll = go_getScrollObject($tt0, a.scroll), a.auto = go_getAutoObject($tt0, a.auto), a.prev = go_getPrevNextObject($tt0, a.prev), a.next = go_getPrevNextObject($tt0, a.next), a.pagination = go_getPaginationObject($tt0, a.pagination), a.swipe = go_getSwipeObject($tt0, a.swipe), a.mousewheel = go_getMousewheelObject($tt0, a.mousewheel), b && (opts_orig = $.extend(!0, {}, $.fn.carouFredSel.defaults, a)), opts = $.extend(!0, {}, $.fn.carouFredSel.defaults, a), opts.d = cf_getDimensions(opts), crsl.direction = "up" == opts.direction || "left" == opts.direction ? "next" : "prev";
			var d = $cfs.children(),
				e = ms_getParentSize($wrp, opts, "width");
			if (is_true(opts.cookie) && (opts.cookie = "caroufredsel_cookie_" + conf.serialNumber), opts.maxDimension = ms_getMaxDimension(opts, e), opts.items = in_complementItems(opts.items, opts, d, c), opts[opts.d.width] = in_complementPrimarySize(opts[opts.d.width], opts, d), opts[opts.d.height] = in_complementSecondarySize(opts[opts.d.height], opts, d), opts.responsive && (is_percentage(opts[opts.d.width]) || (opts[opts.d.width] = "100%")), is_percentage(opts[opts.d.width]) && (crsl.upDateOnWindowResize = !0, crsl.primarySizePercentage = opts[opts.d.width], opts[opts.d.width] = ms_getPercentage(e, crsl.primarySizePercentage), opts.items.visible || (opts.items.visibleConf.variable = !0)), opts.responsive ? (opts.usePadding = !1, opts.padding = [0, 0, 0, 0], opts.align = !1, opts.items.visibleConf.variable = !1) : (opts.items.visible || (opts = in_complementVisibleItems(opts, e)), opts[opts.d.width] || (!opts.items.visibleConf.variable && is_number(opts.items[opts.d.width]) && "*" == opts.items.filter ? (opts[opts.d.width] = opts.items.visible * opts.items[opts.d.width], opts.align = !1) : opts[opts.d.width] = "variable"), is_undefined(opts.align) && (opts.align = is_number(opts[opts.d.width]) ? "center" : !1), opts.items.visibleConf.variable && (opts.items.visible = gn_getVisibleItemsNext(d, opts, 0))), "*" == opts.items.filter || opts.items.visibleConf.variable || (opts.items.visibleConf.org = opts.items.visible, opts.items.visible = gn_getVisibleItemsNextFilter(d, opts, 0)), opts.items.visible = cf_getItemsAdjust(opts.items.visible, opts, opts.items.visibleConf.adjust, $tt0), opts.items.visibleConf.old = opts.items.visible, opts.responsive) opts.items.visibleConf.min || (opts.items.visibleConf.min = opts.items.visible), opts.items.visibleConf.max || (opts.items.visibleConf.max = opts.items.visible), opts = in_getResponsiveValues(opts, d, e);
			else switch (opts.padding = cf_getPadding(opts.padding), "top" == opts.align ? opts.align = "left" : "bottom" == opts.align && (opts.align = "right"), opts.align) {
				case "center":
				case "left":
				case "right":
					"variable" != opts[opts.d.width] && (opts = in_getAlignPadding(opts, d), opts.usePadding = !0);
					break;
				default:
					opts.align = !1, opts.usePadding = 0 == opts.padding[0] && 0 == opts.padding[1] && 0 == opts.padding[2] && 0 == opts.padding[3] ? !1 : !0
			}
			is_number(opts.scroll.duration) || (opts.scroll.duration = 500), is_undefined(opts.scroll.items) && (opts.scroll.items = opts.responsive || opts.items.visibleConf.variable || "*" != opts.items.filter ? "visible" : opts.items.visible), opts.auto = $.extend(!0, {}, opts.scroll, opts.auto), opts.prev = $.extend(!0, {}, opts.scroll, opts.prev), opts.next = $.extend(!0, {}, opts.scroll, opts.next), opts.pagination = $.extend(!0, {}, opts.scroll, opts.pagination), opts.auto = go_complementAutoObject($tt0, opts.auto), opts.prev = go_complementPrevNextObject($tt0, opts.prev), opts.next = go_complementPrevNextObject($tt0, opts.next), opts.pagination = go_complementPaginationObject($tt0, opts.pagination), opts.swipe = go_complementSwipeObject($tt0, opts.swipe), opts.mousewheel = go_complementMousewheelObject($tt0, opts.mousewheel), opts.synchronise && (opts.synchronise = cf_getSynchArr(opts.synchronise)), opts.auto.onPauseStart && (opts.auto.onTimeoutStart = opts.auto.onPauseStart, deprecated("auto.onPauseStart", "auto.onTimeoutStart")), opts.auto.onPausePause && (opts.auto.onTimeoutPause = opts.auto.onPausePause, deprecated("auto.onPausePause", "auto.onTimeoutPause")), opts.auto.onPauseEnd && (opts.auto.onTimeoutEnd = opts.auto.onPauseEnd, deprecated("auto.onPauseEnd", "auto.onTimeoutEnd")), opts.auto.pauseDuration && (opts.auto.timeoutDuration = opts.auto.pauseDuration, deprecated("auto.pauseDuration", "auto.timeoutDuration"))
		}, FN._build = function () {
			$cfs.data("_cfs_isCarousel", !0);
			var a = $cfs.children(),
				b = in_mapCss($cfs, ["textAlign", "float", "position", "top", "right", "bottom", "left", "zIndex", "width", "height", "marginTop", "marginRight", "marginBottom", "marginLeft"]),
				c = "relative";
			switch (b.position) {
				case "absolute":
				case "fixed":
					c = b.position
			}
			"parent" == conf.wrapper ? sz_storeOrigCss($wrp) : $wrp.css(b), $wrp.css({
				overflow: "hidden",
				position: c
			}), sz_storeOrigCss($cfs), $cfs.data("_cfs_origCssZindex", b.zIndex), $cfs.css({
				textAlign: "left",
				"float": "none",
				position: "absolute",
				top: 0,
				right: "auto",
				bottom: "auto",
				left: 0,
				marginTop: 0,
				marginRight: 0,
				marginBottom: 0,
				marginLeft: 0
			}), sz_storeMargin(a, opts), sz_storeOrigCss(a), opts.responsive && sz_setResponsiveSizes(opts, a)
		}, FN._bind_events = function () {
			FN._unbind_events(), $cfs.bind(cf_e("stop", conf), function (a, b) {
				return a.stopPropagation(), crsl.isStopped || opts.auto.button && opts.auto.button.addClass(cf_c("stopped", conf)), crsl.isStopped = !0, opts.auto.play && (opts.auto.play = !1, $cfs.trigger(cf_e("pause", conf), b)), !0
			}), $cfs.bind(cf_e("finish", conf), function (a) {
				return a.stopPropagation(), crsl.isScrolling && sc_stopScroll(scrl), !0
			}), $cfs.bind(cf_e("pause", conf), function (a, b, c) {
				if (a.stopPropagation(), tmrs = sc_clearTimers(tmrs), b && crsl.isScrolling) {
					scrl.isStopped = !0;
					var d = getTime() - scrl.startTime;
					scrl.duration -= d, scrl.pre && (scrl.pre.duration -= d), scrl.post && (scrl.post.duration -= d), sc_stopScroll(scrl, !1)
				}
				if (crsl.isPaused || crsl.isScrolling || c && (tmrs.timePassed += getTime() - tmrs.startTime), crsl.isPaused || opts.auto.button && opts.auto.button.addClass(cf_c("paused", conf)), crsl.isPaused = !0, opts.auto.onTimeoutPause) {
					var e = opts.auto.timeoutDuration - tmrs.timePassed,
						f = 100 - Math.ceil(100 * e / opts.auto.timeoutDuration);
					opts.auto.onTimeoutPause.call($tt0, f, e)
				}
				return !0
			}), $cfs.bind(cf_e("play", conf), function (a, b, c, d) {
				a.stopPropagation(), tmrs = sc_clearTimers(tmrs);
				var e = [b, c, d],
					f = ["string", "number", "boolean"],
					g = cf_sortParams(e, f);
				if (b = g[0], c = g[1], d = g[2], "prev" != b && "next" != b && (b = crsl.direction), is_number(c) || (c = 0), is_boolean(d) || (d = !1), d && (crsl.isStopped = !1, opts.auto.play = !0), !opts.auto.play) return a.stopImmediatePropagation(), debug(conf, "Carousel stopped: Not scrolling.");
				crsl.isPaused && opts.auto.button && (opts.auto.button.removeClass(cf_c("stopped", conf)), opts.auto.button.removeClass(cf_c("paused", conf))), crsl.isPaused = !1, tmrs.startTime = getTime();
				var h = opts.auto.timeoutDuration + c;
				return dur2 = h - tmrs.timePassed, perc = 100 - Math.ceil(100 * dur2 / h), opts.auto.progress && (tmrs.progress = setInterval(function () {
					var a = getTime() - tmrs.startTime + tmrs.timePassed,
						b = Math.ceil(100 * a / h);
					opts.auto.progress.updater.call(opts.auto.progress.bar[0], b)
				}, opts.auto.progress.interval)), tmrs.auto = setTimeout(function () {
					opts.auto.progress && opts.auto.progress.updater.call(opts.auto.progress.bar[0], 100), opts.auto.onTimeoutEnd && opts.auto.onTimeoutEnd.call($tt0, perc, dur2), crsl.isScrolling ? $cfs.trigger(cf_e("play", conf), b) : $cfs.trigger(cf_e(b, conf), opts.auto)
				}, dur2), opts.auto.onTimeoutStart && opts.auto.onTimeoutStart.call($tt0, perc, dur2), !0
			}), $cfs.bind(cf_e("resume", conf), function (a) {
				return a.stopPropagation(), scrl.isStopped ? (scrl.isStopped = !1, crsl.isPaused = !1, crsl.isScrolling = !0, scrl.startTime = getTime(), sc_startScroll(scrl, conf)) : $cfs.trigger(cf_e("play", conf)), !0
			}), $cfs.bind(cf_e("prev", conf) + " " + cf_e("next", conf), function (a, b, c, d, e) {
				if (a.stopPropagation(), crsl.isStopped || $cfs.is(":hidden")) return a.stopImmediatePropagation(), debug(conf, "Carousel stopped or hidden: Not scrolling.");
				var f = is_number(opts.items.minimum) ? opts.items.minimum : opts.items.visible + 1;
				if (f > itms.total) return a.stopImmediatePropagation(), debug(conf, "Not enough items (" + itms.total + " total, " + f + " needed): Not scrolling.");
				var g = [b, c, d, e],
					h = ["object", "number/string", "function", "boolean"],
					i = cf_sortParams(g, h);
				b = i[0], c = i[1], d = i[2], e = i[3];
				var j = a.type.slice(conf.events.prefix.length);
				if (is_object(b) || (b = {}), is_function(d) && (b.onAfter = d), is_boolean(e) && (b.queue = e), b = $.extend(!0, {}, opts[j], b), b.conditions && !b.conditions.call($tt0, j)) return a.stopImmediatePropagation(), debug(conf, 'Callback "conditions" returned false.');
				if (!is_number(c)) {
					if ("*" != opts.items.filter) c = "visible";
					else
						for (var k = [c, b.items, opts[j].items], i = 0, l = k.length; l > i; i++)
							if (is_number(k[i]) || "page" == k[i] || "visible" == k[i]) {
								c = k[i];
								break
							} switch (c) {
						case "page":
							return a.stopImmediatePropagation(), $cfs.triggerHandler(cf_e(j + "Page", conf), [b, d]);
						case "visible":
							opts.items.visibleConf.variable || "*" != opts.items.filter || (c = opts.items.visible)
					}
				}
				if (scrl.isStopped) return $cfs.trigger(cf_e("resume", conf)), $cfs.trigger(cf_e("queue", conf), [j, [b, c, d]]), a.stopImmediatePropagation(), debug(conf, "Carousel resumed scrolling.");
				if (b.duration > 0 && crsl.isScrolling) return b.queue && ("last" == b.queue && (queu = []), ("first" != b.queue || 0 == queu.length) && $cfs.trigger(cf_e("queue", conf), [j, [b, c, d]])), a.stopImmediatePropagation(), debug(conf, "Carousel currently scrolling.");
				if (tmrs.timePassed = 0, $cfs.trigger(cf_e("slide_" + j, conf), [b, c]), opts.synchronise)
					for (var m = opts.synchronise, n = [b, c], o = 0, l = m.length; l > o; o++) {
						var p = j;
						m[o][2] || (p = "prev" == p ? "next" : "prev"), m[o][1] || (n[0] = m[o][0].triggerHandler("_cfs_triggerEvent", ["configuration", p])), n[1] = c + m[o][3], m[o][0].trigger("_cfs_triggerEvent", ["slide_" + p, n])
					}
				return !0
			}), $cfs.bind(cf_e("slide_prev", conf), function (a, b, c) {
				a.stopPropagation();
				var d = $cfs.children();
				if (!opts.circular && 0 == itms.first) return opts.infinite && $cfs.trigger(cf_e("next", conf), itms.total - 1), a.stopImmediatePropagation();
				if (sz_resetMargin(d, opts), !is_number(c)) {
					if (opts.items.visibleConf.variable) c = gn_getVisibleItemsPrev(d, opts, itms.total - 1);
					else if ("*" != opts.items.filter) {
						var e = is_number(b.items) ? b.items : gn_getVisibleOrg($cfs, opts);
						c = gn_getScrollItemsPrevFilter(d, opts, itms.total - 1, e)
					} else c = opts.items.visible;
					c = cf_getAdjust(c, opts, b.items, $tt0)
				}
				if (opts.circular || itms.total - c < itms.first && (c = itms.total - itms.first), opts.items.visibleConf.old = opts.items.visible, opts.items.visibleConf.variable) {
					var f = cf_getItemsAdjust(gn_getVisibleItemsNext(d, opts, itms.total - c), opts, opts.items.visibleConf.adjust, $tt0);
					f >= opts.items.visible + c && itms.total > c && (c++, f = cf_getItemsAdjust(gn_getVisibleItemsNext(d, opts, itms.total - c), opts, opts.items.visibleConf.adjust, $tt0)), opts.items.visible = f
				} else if ("*" != opts.items.filter) {
					var f = gn_getVisibleItemsNextFilter(d, opts, itms.total - c);
					opts.items.visible = cf_getItemsAdjust(f, opts, opts.items.visibleConf.adjust, $tt0)
				}
				if (sz_resetMargin(d, opts, !0), 0 == c) return a.stopImmediatePropagation(), debug(conf, "0 items to scroll: Not scrolling.");
				for (debug(conf, "Scrolling " + c + " items backward."), itms.first += c; itms.first >= itms.total;) itms.first -= itms.total;
				opts.circular || (0 == itms.first && b.onEnd && b.onEnd.call($tt0, "prev"), opts.infinite || nv_enableNavi(opts, itms.first, conf)), $cfs.children().slice(itms.total - c, itms.total).prependTo($cfs), itms.total < opts.items.visible + c && $cfs.children().slice(0, opts.items.visible + c - itms.total).clone(!0).appendTo($cfs);
				var d = $cfs.children(),
					g = gi_getOldItemsPrev(d, opts, c),
					h = gi_getNewItemsPrev(d, opts),
					i = d.eq(c - 1),
					j = g.last(),
					k = h.last();
				sz_resetMargin(d, opts);
				var l = 0,
					m = 0;
				if (opts.align) {
					var n = cf_getAlignPadding(h, opts);
					l = n[0], m = n[1]
				}
				var o = 0 > l ? opts.padding[opts.d[3]] : 0,
					p = !1,
					q = $();
				if (c > opts.items.visible && (q = d.slice(opts.items.visibleConf.old, c), "directscroll" == b.fx)) {
					var r = opts.items[opts.d.width];
					p = q, i = k, sc_hideHiddenItems(p), opts.items[opts.d.width] = "variable"
				}
				var s = !1,
					t = ms_getTotalSize(d.slice(0, c), opts, "width"),
					u = cf_mapWrapperSizes(ms_getSizes(h, opts, !0), opts, !opts.usePadding),
					v = 0,
					w = {},
					x = {},
					y = {},
					z = {},
					A = {},
					B = {},
					C = {},
					D = sc_getDuration(b, opts, c, t);
				switch (b.fx) {
					case "cover":
					case "cover-fade":
						v = ms_getTotalSize(d.slice(0, opts.items.visible), opts, "width")
				}
				p && (opts.items[opts.d.width] = r), sz_resetMargin(d, opts, !0), m >= 0 && sz_resetMargin(j, opts, opts.padding[opts.d[1]]), l >= 0 && sz_resetMargin(i, opts, opts.padding[opts.d[3]]), opts.align && (opts.padding[opts.d[1]] = m, opts.padding[opts.d[3]] = l), B[opts.d.left] = -(t - o), C[opts.d.left] = -(v - o), x[opts.d.left] = u[opts.d.width];
				var E = function () {},
					F = function () {},
					G = function () {},
					H = function () {},
					I = function () {},
					J = function () {},
					K = function () {},
					L = function () {},
					M = function () {},
					N = function () {},
					O = function () {};
				switch (b.fx) {
					case "crossfade":
					case "cover":
					case "cover-fade":
					case "uncover":
					case "uncover-fade":
						s = $cfs.clone(!0).appendTo($wrp)
				}
				switch (b.fx) {
					case "crossfade":
					case "uncover":
					case "uncover-fade":
						s.children().slice(0, c).remove(), s.children().slice(opts.items.visibleConf.old).remove();
						break;
					case "cover":
					case "cover-fade":
						s.children().slice(opts.items.visible).remove(), s.css(C)
				}
				if ($cfs.css(B), scrl = sc_setScroll(D, b.easing, conf), w[opts.d.left] = opts.usePadding ? opts.padding[opts.d[3]] : 0, ("variable" == opts[opts.d.width] || "variable" == opts[opts.d.height]) && (E = function () {
						$wrp.css(u)
					}, F = function () {
						scrl.anims.push([$wrp, u])
					}), opts.usePadding) {
					switch (k.not(i).length && (y[opts.d.marginRight] = i.data("_cfs_origCssMargin"), 0 > l ? i.css(y) : (K = function () {
						i.css(y)
					}, L = function () {
						scrl.anims.push([i, y])
					})), b.fx) {
						case "cover":
						case "cover-fade":
							s.children().eq(c - 1).css(y)
					}
					k.not(j).length && (z[opts.d.marginRight] = j.data("_cfs_origCssMargin"), G = function () {
						j.css(z)
					}, H = function () {
						scrl.anims.push([j, z])
					}), m >= 0 && (A[opts.d.marginRight] = k.data("_cfs_origCssMargin") + opts.padding[opts.d[1]], I = function () {
						k.css(A)
					}, J = function () {
						scrl.anims.push([k, A])
					})
				}
				O = function () {
					$cfs.css(w)
				};
				var P = opts.items.visible + c - itms.total;
				N = function () {
					if (P > 0 && ($cfs.children().slice(itms.total).remove(), g = $($cfs.children().slice(itms.total - (opts.items.visible - P)).get().concat($cfs.children().slice(0, P).get()))), sc_showHiddenItems(p), opts.usePadding) {
						var a = $cfs.children().eq(opts.items.visible + c - 1);
						a.css(opts.d.marginRight, a.data("_cfs_origCssMargin"))
					}
				};
				var Q = sc_mapCallbackArguments(g, q, h, c, "prev", D, u);
				switch (M = function () {
					sc_afterScroll($cfs, s, b), crsl.isScrolling = !1, clbk.onAfter = sc_fireCallbacks($tt0, b, "onAfter", Q, clbk), queu = sc_fireQueue($cfs, queu, conf), crsl.isPaused || $cfs.trigger(cf_e("play", conf))
				}, crsl.isScrolling = !0, tmrs = sc_clearTimers(tmrs), clbk.onBefore = sc_fireCallbacks($tt0, b, "onBefore", Q, clbk), b.fx) {
					case "none":
						$cfs.css(w), E(), G(), I(), K(), O(), N(), M();
						break;
					case "fade":
						scrl.anims.push([$cfs, {
							opacity: 0
						}, function () {
							E(), G(), I(), K(), O(), N(), scrl = sc_setScroll(D, b.easing, conf), scrl.anims.push([$cfs, {
								opacity: 1
							}, M]), sc_startScroll(scrl, conf)
						}]);
						break;
					case "crossfade":
						$cfs.css({
							opacity: 0
						}), scrl.anims.push([s, {
							opacity: 0
						}]), scrl.anims.push([$cfs, {
							opacity: 1
						}, M]), F(), G(), I(), K(), O(), N();
						break;
					case "cover":
						scrl.anims.push([s, w, function () {
							G(), I(), K(), O(), N(), M()
						}]), F();
						break;
					case "cover-fade":
						scrl.anims.push([$cfs, {
							opacity: 0
						}]), scrl.anims.push([s, w, function () {
							G(), I(), K(), O(), N(), M()
						}]), F();
						break;
					case "uncover":
						scrl.anims.push([s, x, M]), F(), G(), I(), K(), O(), N();
						break;
					case "uncover-fade":
						$cfs.css({
							opacity: 0
						}), scrl.anims.push([$cfs, {
							opacity: 1
						}]), scrl.anims.push([s, x, M]), F(), G(), I(), K(), O(), N();
						break;
					default:
						scrl.anims.push([$cfs, w, function () {
							N(), M()
						}]), F(), H(), J(), L()
				}
				return sc_startScroll(scrl, conf), cf_setCookie(opts.cookie, $cfs, conf), $cfs.trigger(cf_e("updatePageStatus", conf), [!1, u]), !0
			}), $cfs.bind(cf_e("slide_next", conf), function (a, b, c) {
				a.stopPropagation();
				var d = $cfs.children();
				if (!opts.circular && itms.first == opts.items.visible) return opts.infinite && $cfs.trigger(cf_e("prev", conf), itms.total - 1), a.stopImmediatePropagation();
				if (sz_resetMargin(d, opts), !is_number(c)) {
					if ("*" != opts.items.filter) {
						var e = is_number(b.items) ? b.items : gn_getVisibleOrg($cfs, opts);
						c = gn_getScrollItemsNextFilter(d, opts, 0, e)
					} else c = opts.items.visible;
					c = cf_getAdjust(c, opts, b.items, $tt0)
				}
				var f = 0 == itms.first ? itms.total : itms.first;
				if (!opts.circular) {
					if (opts.items.visibleConf.variable) var g = gn_getVisibleItemsNext(d, opts, c),
						e = gn_getVisibleItemsPrev(d, opts, f - 1);
					else var g = opts.items.visible,
						e = opts.items.visible;
					c + g > f && (c = f - e)
				}
				if (opts.items.visibleConf.old = opts.items.visible, opts.items.visibleConf.variable) {
					for (var g = cf_getItemsAdjust(gn_getVisibleItemsNextTestCircular(d, opts, c, f), opts, opts.items.visibleConf.adjust, $tt0); opts.items.visible - c >= g && itms.total > c;) c++, g = cf_getItemsAdjust(gn_getVisibleItemsNextTestCircular(d, opts, c, f), opts, opts.items.visibleConf.adjust, $tt0);
					opts.items.visible = g
				} else if ("*" != opts.items.filter) {
					var g = gn_getVisibleItemsNextFilter(d, opts, c);
					opts.items.visible = cf_getItemsAdjust(g, opts, opts.items.visibleConf.adjust, $tt0)
				}
				if (sz_resetMargin(d, opts, !0), 0 == c) return a.stopImmediatePropagation(), debug(conf, "0 items to scroll: Not scrolling.");
				for (debug(conf, "Scrolling " + c + " items forward."), itms.first -= c; 0 > itms.first;) itms.first += itms.total;
				opts.circular || (itms.first == opts.items.visible && b.onEnd && b.onEnd.call($tt0, "next"), opts.infinite || nv_enableNavi(opts, itms.first, conf)), itms.total < opts.items.visible + c && $cfs.children().slice(0, opts.items.visible + c - itms.total).clone(!0).appendTo($cfs);
				var d = $cfs.children(),
					h = gi_getOldItemsNext(d, opts),
					i = gi_getNewItemsNext(d, opts, c),
					j = d.eq(c - 1),
					k = h.last(),
					l = i.last();
				sz_resetMargin(d, opts);
				var m = 0,
					n = 0;
				if (opts.align) {
					var o = cf_getAlignPadding(i, opts);
					m = o[0], n = o[1]
				}
				var p = !1,
					q = $();
				if (c > opts.items.visibleConf.old && (q = d.slice(opts.items.visibleConf.old, c), "directscroll" == b.fx)) {
					var r = opts.items[opts.d.width];
					p = q, j = k, sc_hideHiddenItems(p), opts.items[opts.d.width] = "variable"
				}
				var s = !1,
					t = ms_getTotalSize(d.slice(0, c), opts, "width"),
					u = cf_mapWrapperSizes(ms_getSizes(i, opts, !0), opts, !opts.usePadding),
					v = 0,
					w = {},
					x = {},
					y = {},
					z = {},
					A = {},
					B = sc_getDuration(b, opts, c, t);
				switch (b.fx) {
					case "uncover":
					case "uncover-fade":
						v = ms_getTotalSize(d.slice(0, opts.items.visibleConf.old), opts, "width")
				}
				p && (opts.items[opts.d.width] = r), opts.align && 0 > opts.padding[opts.d[1]] && (opts.padding[opts.d[1]] = 0), sz_resetMargin(d, opts, !0), sz_resetMargin(k, opts, opts.padding[opts.d[1]]), opts.align && (opts.padding[opts.d[1]] = n, opts.padding[opts.d[3]] = m), A[opts.d.left] = opts.usePadding ? opts.padding[opts.d[3]] : 0;
				var C = function () {},
					D = function () {},
					E = function () {},
					F = function () {},
					G = function () {},
					H = function () {},
					I = function () {},
					J = function () {},
					K = function () {};
				switch (b.fx) {
					case "crossfade":
					case "cover":
					case "cover-fade":
					case "uncover":
					case "uncover-fade":
						s = $cfs.clone(!0).appendTo($wrp), s.children().slice(opts.items.visibleConf.old).remove()
				}
				switch (b.fx) {
					case "crossfade":
					case "cover":
					case "cover-fade":
						$cfs.css("zIndex", 1), s.css("zIndex", 0)
				}
				if (scrl = sc_setScroll(B, b.easing, conf), w[opts.d.left] = -t, x[opts.d.left] = -v, 0 > m && (w[opts.d.left] += m), ("variable" == opts[opts.d.width] || "variable" == opts[opts.d.height]) && (C = function () {
						$wrp.css(u)
					}, D = function () {
						scrl.anims.push([$wrp, u])
					}), opts.usePadding) {
					var L = l.data("_cfs_origCssMargin");
					n >= 0 && (L += opts.padding[opts.d[1]]), l.css(opts.d.marginRight, L), j.not(k).length && (z[opts.d.marginRight] = k.data("_cfs_origCssMargin")), E = function () {
						k.css(z)
					}, F = function () {
						scrl.anims.push([k, z])
					};
					var M = j.data("_cfs_origCssMargin");
					m > 0 && (M += opts.padding[opts.d[3]]), y[opts.d.marginRight] = M, G = function () {
						j.css(y)
					}, H = function () {
						scrl.anims.push([j, y])
					}
				}
				K = function () {
					$cfs.css(A)
				};
				var N = opts.items.visible + c - itms.total;
				J = function () {
					N > 0 && $cfs.children().slice(itms.total).remove();
					var a = $cfs.children().slice(0, c).appendTo($cfs).last();
					if (N > 0 && (i = gi_getCurrentItems(d, opts)), sc_showHiddenItems(p), opts.usePadding) {
						if (itms.total < opts.items.visible + c) {
							var b = $cfs.children().eq(opts.items.visible - 1);
							b.css(opts.d.marginRight, b.data("_cfs_origCssMargin") + opts.padding[opts.d[1]])
						}
						a.css(opts.d.marginRight, a.data("_cfs_origCssMargin"))
					}
				};
				var O = sc_mapCallbackArguments(h, q, i, c, "next", B, u);
				switch (I = function () {
					$cfs.css("zIndex", $cfs.data("_cfs_origCssZindex")), sc_afterScroll($cfs, s, b), crsl.isScrolling = !1, clbk.onAfter = sc_fireCallbacks($tt0, b, "onAfter", O, clbk), queu = sc_fireQueue($cfs, queu, conf), crsl.isPaused || $cfs.trigger(cf_e("play", conf))
				}, crsl.isScrolling = !0, tmrs = sc_clearTimers(tmrs), clbk.onBefore = sc_fireCallbacks($tt0, b, "onBefore", O, clbk), b.fx) {
					case "none":
						$cfs.css(w), C(), E(), G(), K(), J(), I();
						break;
					case "fade":
						scrl.anims.push([$cfs, {
							opacity: 0
						}, function () {
							C(), E(), G(), K(), J(), scrl = sc_setScroll(B, b.easing, conf), scrl.anims.push([$cfs, {
								opacity: 1
							}, I]), sc_startScroll(scrl, conf)
						}]);
						break;
					case "crossfade":
						$cfs.css({
							opacity: 0
						}), scrl.anims.push([s, {
							opacity: 0
						}]), scrl.anims.push([$cfs, {
							opacity: 1
						}, I]), D(), E(), G(), K(), J();
						break;
					case "cover":
						$cfs.css(opts.d.left, $wrp[opts.d.width]()), scrl.anims.push([$cfs, A, I]), D(), E(), G(), J();
						break;
					case "cover-fade":
						$cfs.css(opts.d.left, $wrp[opts.d.width]()), scrl.anims.push([s, {
							opacity: 0
						}]), scrl.anims.push([$cfs, A, I]), D(), E(), G(), J();
						break;
					case "uncover":
						scrl.anims.push([s, x, I]), D(), E(), G(), K(), J();
						break;
					case "uncover-fade":
						$cfs.css({
							opacity: 0
						}), scrl.anims.push([$cfs, {
							opacity: 1
						}]), scrl.anims.push([s, x, I]), D(), E(), G(), K(), J();
						break;
					default:
						scrl.anims.push([$cfs, w, function () {
							K(), J(), I()
						}]), D(), F(), H()
				}
				return sc_startScroll(scrl, conf), cf_setCookie(opts.cookie, $cfs, conf), $cfs.trigger(cf_e("updatePageStatus", conf), [!1, u]), !0
			}), $cfs.bind(cf_e("slideTo", conf), function (a, b, c, d, e, f, g) {
				a.stopPropagation();
				var h = [b, c, d, e, f, g],
					i = ["string/number/object", "number", "boolean", "object", "string", "function"],
					j = cf_sortParams(h, i);
				return e = j[3], f = j[4], g = j[5], b = gn_getItemIndex(j[0], j[1], j[2], itms, $cfs), 0 == b ? !1 : (is_object(e) || (e = !1), "prev" != f && "next" != f && (f = opts.circular ? itms.total / 2 >= b ? "next" : "prev" : 0 == itms.first || itms.first > b ? "next" : "prev"), "prev" == f && (b = itms.total - b), $cfs.trigger(cf_e(f, conf), [e, b, g]), !0)
			}), $cfs.bind(cf_e("prevPage", conf), function (a, b, c) {
				a.stopPropagation();
				var d = $cfs.triggerHandler(cf_e("currentPage", conf));
				return $cfs.triggerHandler(cf_e("slideToPage", conf), [d - 1, b, "prev", c])
			}), $cfs.bind(cf_e("nextPage", conf), function (a, b, c) {
				a.stopPropagation();
				var d = $cfs.triggerHandler(cf_e("currentPage", conf));
				return $cfs.triggerHandler(cf_e("slideToPage", conf), [d + 1, b, "next", c])
			}), $cfs.bind(cf_e("slideToPage", conf), function (a, b, c, d, e) {
				a.stopPropagation(), is_number(b) || (b = $cfs.triggerHandler(cf_e("currentPage", conf)));
				var f = opts.pagination.items || opts.items.visible,
					g = Math.ceil(itms.total / f) - 1;
				return 0 > b && (b = g), b > g && (b = 0), $cfs.triggerHandler(cf_e("slideTo", conf), [b * f, 0, !0, c, d, e])
			}), $cfs.bind(cf_e("jumpToStart", conf), function (a, b) {
				if (a.stopPropagation(), b = b ? gn_getItemIndex(b, 0, !0, itms, $cfs) : 0, b += itms.first, 0 != b) {
					if (itms.total > 0)
						for (; b > itms.total;) b -= itms.total;
					$cfs.prepend($cfs.children().slice(b, itms.total))
				}
				return !0
			}), $cfs.bind(cf_e("synchronise", conf), function (a, b) {
				if (a.stopPropagation(), b) b = cf_getSynchArr(b);
				else {
					if (!opts.synchronise) return debug(conf, "No carousel to synchronise.");
					b = opts.synchronise
				}
				for (var c = $cfs.triggerHandler(cf_e("currentPosition", conf)), d = !0, e = 0, f = b.length; f > e; e++) b[e][0].triggerHandler(cf_e("slideTo", conf), [c, b[e][3], !0]) || (d = !1);
				return d
			}), $cfs.bind(cf_e("queue", conf), function (a, b, c) {
				return a.stopPropagation(), is_function(b) ? b.call($tt0, queu) : is_array(b) ? queu = b : is_undefined(b) || queu.push([b, c]), queu
			}), $cfs.bind(cf_e("insertItem", conf), function (a, b, c, d, e) {
				a.stopPropagation();
				var f = [b, c, d, e],
					g = ["string/object", "string/number/object", "boolean", "number"],
					h = cf_sortParams(f, g);
				if (b = h[0], c = h[1], d = h[2], e = h[3], is_object(b) && !is_jquery(b) ? b = $(b) : is_string(b) && (b = $(b)), !is_jquery(b) || 0 == b.length) return debug(conf, "Not a valid object.");
				is_undefined(c) && (c = "end"), sz_storeMargin(b, opts), sz_storeOrigCss(b);
				var i = c,
					j = "before";
				"end" == c ? d ? (0 == itms.first ? (c = itms.total - 1, j = "after") : (c = itms.first, itms.first += b.length), 0 > c && (c = 0)) : (c = itms.total - 1, j = "after") : c = gn_getItemIndex(c, e, d, itms, $cfs);
				var k = $cfs.children().eq(c);
				return k.length ? k[j](b) : (debug(conf, "Correct insert-position not found! Appending item to the end."), $cfs.append(b)), "end" == i || d || itms.first > c && (itms.first += b.length), itms.total = $cfs.children().length, itms.first >= itms.total && (itms.first -= itms.total), $cfs.trigger(cf_e("updateSizes", conf)), $cfs.trigger(cf_e("linkAnchors", conf)), !0
			}), $cfs.bind(cf_e("removeItem", conf), function (a, b, c, d) {
				a.stopPropagation();
				var e = [b, c, d],
					f = ["string/number/object", "boolean", "number"],
					g = cf_sortParams(e, f);
				if (b = g[0], c = g[1], d = g[2], b instanceof $ && b.length > 1) return i = $(), b.each(function () {
					var e = $cfs.trigger(cf_e("removeItem", conf), [$(this), c, d]);
					e && (i = i.add(e))
				}), i;
				if (is_undefined(b) || "end" == b) i = $cfs.children().last();
				else {
					b = gn_getItemIndex(b, d, c, itms, $cfs);
					var i = $cfs.children().eq(b);
					i.length && itms.first > b && (itms.first -= i.length)
				}
				return i && i.length && (i.detach(), itms.total = $cfs.children().length, $cfs.trigger(cf_e("updateSizes", conf))), i
			}), $cfs.bind(cf_e("onBefore", conf) + " " + cf_e("onAfter", conf), function (a, b) {
				a.stopPropagation();
				var c = a.type.slice(conf.events.prefix.length);
				return is_array(b) && (clbk[c] = b), is_function(b) && clbk[c].push(b), clbk[c]
			}), $cfs.bind(cf_e("currentPosition", conf), function (a, b) {
				if (a.stopPropagation(), 0 == itms.first) var c = 0;
				else var c = itms.total - itms.first;
				return is_function(b) && b.call($tt0, c), c
			}), $cfs.bind(cf_e("currentPage", conf), function (a, b) {
				a.stopPropagation();
				var e, c = opts.pagination.items || opts.items.visible,
					d = Math.ceil(itms.total / c - 1);
				return e = 0 == itms.first ? 0 : itms.first < itms.total % c ? 0 : itms.first != c || opts.circular ? Math.round((itms.total - itms.first) / c) : d, 0 > e && (e = 0), e > d && (e = d), is_function(b) && b.call($tt0, e), e
			}), $cfs.bind(cf_e("currentVisible", conf), function (a, b) {
				a.stopPropagation();
				var c = gi_getCurrentItems($cfs.children(), opts);
				return is_function(b) && b.call($tt0, c), c
			}), $cfs.bind(cf_e("slice", conf), function (a, b, c, d) {
				if (a.stopPropagation(), 0 == itms.total) return !1;
				var e = [b, c, d],
					f = ["number", "number", "function"],
					g = cf_sortParams(e, f);
				if (b = is_number(g[0]) ? g[0] : 0, c = is_number(g[1]) ? g[1] : itms.total, d = g[2], b += itms.first, c += itms.first, itms.total > 0) {
					for (; b > itms.total;) b -= itms.total;
					for (; c > itms.total;) c -= itms.total;
					for (; 0 > b;) b += itms.total;
					for (; 0 > c;) c += itms.total
				}
				var i, h = $cfs.children();
				return i = c > b ? h.slice(b, c) : $(h.slice(b, itms.total).get().concat(h.slice(0, c).get())), is_function(d) && d.call($tt0, i), i
			}), $cfs.bind(cf_e("isPaused", conf) + " " + cf_e("isStopped", conf) + " " + cf_e("isScrolling", conf), function (a, b) {
				a.stopPropagation();
				var c = a.type.slice(conf.events.prefix.length),
					d = crsl[c];
				return is_function(b) && b.call($tt0, d), d
			}), $cfs.bind(cf_e("configuration", conf), function (e, a, b, c) {
				e.stopPropagation();
				var reInit = !1;
				if (is_function(a)) a.call($tt0, opts);
				else if (is_object(a)) opts_orig = $.extend(!0, {}, opts_orig, a), b !== !1 ? reInit = !0 : opts = $.extend(!0, {}, opts, a);
				else if (!is_undefined(a))
					if (is_function(b)) {
						var val = eval("opts." + a);
						is_undefined(val) && (val = ""), b.call($tt0, val)
					} else {
						if (is_undefined(b)) return eval("opts." + a);
						"boolean" != typeof c && (c = !0), eval("opts_orig." + a + " = b"), c !== !1 ? reInit = !0 : eval("opts." + a + " = b")
					}
				if (reInit) {
					sz_resetMargin($cfs.children(), opts), FN._init(opts_orig), FN._bind_buttons();
					var sz = sz_setSizes($cfs, opts);
					$cfs.trigger(cf_e("updatePageStatus", conf), [!0, sz])
				}
				return opts
			}), $cfs.bind(cf_e("linkAnchors", conf), function (a, b, c) {
				return a.stopPropagation(), is_undefined(b) ? b = $("body") : is_string(b) && (b = $(b)), is_jquery(b) && 0 != b.length ? (is_string(c) || (c = "a.caroufredsel"), b.find(c).each(function () {
					var a = this.hash || "";
					a.length > 0 && -1 != $cfs.children().index($(a)) && $(this).unbind("click").click(function (b) {
						b.preventDefault(), $cfs.trigger(cf_e("slideTo", conf), a)
					})
				}), !0) : debug(conf, "Not a valid object.")
			}), $cfs.bind(cf_e("updatePageStatus", conf), function (a, b) {
				if (a.stopPropagation(), opts.pagination.container) {
					var d = opts.pagination.items || opts.items.visible,
						e = Math.ceil(itms.total / d);
					b && (opts.pagination.anchorBuilder && (opts.pagination.container.children().remove(), opts.pagination.container.each(function () {
						for (var a = 0; e > a; a++) {
							var b = $cfs.children().eq(gn_getItemIndex(a * d, 0, !0, itms, $cfs));
							$(this).append(opts.pagination.anchorBuilder.call(b[0], a + 1))
						}
					})), opts.pagination.container.each(function () {
						$(this).children().unbind(opts.pagination.event).each(function (a) {
							$(this).bind(opts.pagination.event, function (b) {
								b.preventDefault(), $cfs.trigger(cf_e("slideTo", conf), [a * d, -opts.pagination.deviation, !0, opts.pagination])
							})
						})
					}));
					var f = $cfs.triggerHandler(cf_e("currentPage", conf)) + opts.pagination.deviation;
					return f >= e && (f = 0), 0 > f && (f = e - 1), opts.pagination.container.each(function () {
						$(this).children().removeClass(cf_c("selected", conf)).eq(f).addClass(cf_c("selected", conf))
					}), !0
				}
			}), $cfs.bind(cf_e("updateSizes", conf), function () {
				var b = opts.items.visible,
					c = $cfs.children(),
					d = ms_getParentSize($wrp, opts, "width");
				if (itms.total = c.length, crsl.primarySizePercentage ? (opts.maxDimension = d, opts[opts.d.width] = ms_getPercentage(d, crsl.primarySizePercentage)) : opts.maxDimension = ms_getMaxDimension(opts, d), opts.responsive ? (opts.items.width = opts.items.sizesConf.width, opts.items.height = opts.items.sizesConf.height, opts = in_getResponsiveValues(opts, c, d), b = opts.items.visible, sz_setResponsiveSizes(opts, c)) : opts.items.visibleConf.variable ? b = gn_getVisibleItemsNext(c, opts, 0) : "*" != opts.items.filter && (b = gn_getVisibleItemsNextFilter(c, opts, 0)), !opts.circular && 0 != itms.first && b > itms.first) {
					if (opts.items.visibleConf.variable) var e = gn_getVisibleItemsPrev(c, opts, itms.first) - itms.first;
					else if ("*" != opts.items.filter) var e = gn_getVisibleItemsPrevFilter(c, opts, itms.first) - itms.first;
					else var e = opts.items.visible - itms.first;
					debug(conf, "Preventing non-circular: sliding " + e + " items backward."), $cfs.trigger(cf_e("prev", conf), e)
				}
				opts.items.visible = cf_getItemsAdjust(b, opts, opts.items.visibleConf.adjust, $tt0), opts.items.visibleConf.old = opts.items.visible, opts = in_getAlignPadding(opts, c);
				var f = sz_setSizes($cfs, opts);
				return $cfs.trigger(cf_e("updatePageStatus", conf), [!0, f]), nv_showNavi(opts, itms.total, conf), nv_enableNavi(opts, itms.first, conf), f
			}), $cfs.bind(cf_e("destroy", conf), function (a, b) {
				return a.stopPropagation(), tmrs = sc_clearTimers(tmrs), $cfs.data("_cfs_isCarousel", !1), $cfs.trigger(cf_e("finish", conf)), b && $cfs.trigger(cf_e("jumpToStart", conf)), sz_restoreOrigCss($cfs.children()), sz_restoreOrigCss($cfs), FN._unbind_events(), FN._unbind_buttons(), "parent" == conf.wrapper ? sz_restoreOrigCss($wrp) : $wrp.replaceWith($cfs), !0
			}), $cfs.bind(cf_e("debug", conf), function () {
				return debug(conf, "Carousel width: " + opts.width), debug(conf, "Carousel height: " + opts.height), debug(conf, "Item widths: " + opts.items.width), debug(conf, "Item heights: " + opts.items.height), debug(conf, "Number of items visible: " + opts.items.visible), opts.auto.play && debug(conf, "Number of items scrolled automatically: " + opts.auto.items), opts.prev.button && debug(conf, "Number of items scrolled backward: " + opts.prev.items), opts.next.button && debug(conf, "Number of items scrolled forward: " + opts.next.items), conf.debug
			}), $cfs.bind("_cfs_triggerEvent", function (a, b, c) {
				return a.stopPropagation(), $cfs.triggerHandler(cf_e(b, conf), c)
			})
		}, FN._unbind_events = function () {
			$cfs.unbind(cf_e("", conf)), $cfs.unbind(cf_e("", conf, !1)), $cfs.unbind("_cfs_triggerEvent")
		}, FN._bind_buttons = function () {
			if (FN._unbind_buttons(), nv_showNavi(opts, itms.total, conf), nv_enableNavi(opts, itms.first, conf), opts.auto.pauseOnHover) {
				var a = bt_pauseOnHoverConfig(opts.auto.pauseOnHover);
				$wrp.bind(cf_e("mouseenter", conf, !1), function () {
					$cfs.trigger(cf_e("pause", conf), a)
				}).bind(cf_e("mouseleave", conf, !1), function () {
					$cfs.trigger(cf_e("resume", conf))
				})
			}
			if (opts.auto.button && opts.auto.button.bind(cf_e(opts.auto.event, conf, !1), function (a) {
					a.preventDefault();
					var b = !1,
						c = null;
					crsl.isPaused ? b = "play" : opts.auto.pauseOnEvent && (b = "pause", c = bt_pauseOnHoverConfig(opts.auto.pauseOnEvent)), b && $cfs.trigger(cf_e(b, conf), c)
				}), opts.prev.button && (opts.prev.button.bind(cf_e(opts.prev.event, conf, !1), function (a) {
					a.preventDefault(), $cfs.trigger(cf_e("prev", conf))
				}), opts.prev.pauseOnHover)) {
				var a = bt_pauseOnHoverConfig(opts.prev.pauseOnHover);
				opts.prev.button.bind(cf_e("mouseenter", conf, !1), function () {
					$cfs.trigger(cf_e("pause", conf), a)
				}).bind(cf_e("mouseleave", conf, !1), function () {
					$cfs.trigger(cf_e("resume", conf))
				})
			}
			if (opts.next.button && (opts.next.button.bind(cf_e(opts.next.event, conf, !1), function (a) {
					a.preventDefault(), $cfs.trigger(cf_e("next", conf))
				}), opts.next.pauseOnHover)) {
				var a = bt_pauseOnHoverConfig(opts.next.pauseOnHover);
				opts.next.button.bind(cf_e("mouseenter", conf, !1), function () {
					$cfs.trigger(cf_e("pause", conf), a)
				}).bind(cf_e("mouseleave", conf, !1), function () {
					$cfs.trigger(cf_e("resume", conf))
				})
			}
			if (opts.pagination.container && opts.pagination.pauseOnHover) {
				var a = bt_pauseOnHoverConfig(opts.pagination.pauseOnHover);
				opts.pagination.container.bind(cf_e("mouseenter", conf, !1), function () {
					$cfs.trigger(cf_e("pause", conf), a)
				}).bind(cf_e("mouseleave", conf, !1), function () {
					$cfs.trigger(cf_e("resume", conf))
				})
			}
			if ((opts.prev.key || opts.next.key) && $(document).bind(cf_e("keyup", conf, !1, !0, !0), function (a) {
					var b = a.keyCode;
					b == opts.next.key && (a.preventDefault(), $cfs.trigger(cf_e("next", conf))), b == opts.prev.key && (a.preventDefault(), $cfs.trigger(cf_e("prev", conf)))
				}), opts.pagination.keys && $(document).bind(cf_e("keyup", conf, !1, !0, !0), function (a) {
					var b = a.keyCode;
					b >= 49 && 58 > b && (b = (b - 49) * opts.items.visible, itms.total >= b && (a.preventDefault(), $cfs.trigger(cf_e("slideTo", conf), [b, 0, !0, opts.pagination])))
				}), $.fn.swipe) {
				var b = "ontouchstart" in window;
				if (b && opts.swipe.onTouch || !b && opts.swipe.onMouse) {
					var c = $.extend(!0, {}, opts.prev, opts.swipe),
						d = $.extend(!0, {}, opts.next, opts.swipe),
						e = function () {
							$cfs.trigger(cf_e("prev", conf), [c])
						},
						f = function () {
							$cfs.trigger(cf_e("next", conf), [d])
						};
					switch (opts.direction) {
						case "up":
						case "down":
							opts.swipe.options.swipeUp = f, opts.swipe.options.swipeDown = e;
							break;
						default:
							opts.swipe.options.swipeLeft = f, opts.swipe.options.swipeRight = e
					}
					crsl.swipe && $cfs.swipe("destroy"), $wrp.swipe(opts.swipe.options), $wrp.css("cursor", "move"), crsl.swipe = !0
				}
			}
			if ($.fn.mousewheel && opts.mousewheel) {
				var g = $.extend(!0, {}, opts.prev, opts.mousewheel),
					h = $.extend(!0, {}, opts.next, opts.mousewheel);
				crsl.mousewheel && $wrp.unbind(cf_e("mousewheel", conf, !1)), $wrp.bind(cf_e("mousewheel", conf, !1), function (a, b) {
					a.preventDefault(), b > 0 ? $cfs.trigger(cf_e("prev", conf), [g]) : $cfs.trigger(cf_e("next", conf), [h])
				}), crsl.mousewheel = !0
			}
			if (opts.auto.play && $cfs.trigger(cf_e("play", conf), opts.auto.delay), crsl.upDateOnWindowResize) {
				var i = function () {
						$cfs.trigger(cf_e("finish", conf)), opts.auto.pauseOnResize && !crsl.isPaused && $cfs.trigger(cf_e("play", conf)), sz_resetMargin($cfs.children(), opts), $cfs.trigger(cf_e("updateSizes", conf))
					},
					j = $(window),
					k = null;
				if ($.debounce && "debounce" == conf.onWindowResize) k = $.debounce(200, i);
				else if ($.throttle && "throttle" == conf.onWindowResize) k = $.throttle(300, i);
				else {
					var l = 0,
						m = 0;
					k = function () {
						var a = j.width(),
							b = j.height();
						(a != l || b != m) && (i(), l = a, m = b)
					}
				}
				j.bind(cf_e("resize", conf, !1, !0, !0), k)
			}
		}, FN._unbind_buttons = function () {
			var b = (cf_e("", conf), cf_e("", conf, !1));
			var ns3 = cf_e("", conf, !1, !0, !0);
			$(document).unbind(ns3), $(window).unbind(ns3), $wrp.unbind(b), opts.auto.button && opts.auto.button.unbind(b), opts.prev.button && opts.prev.button.unbind(b), opts.next.button && opts.next.button.unbind(b), opts.pagination.container && (opts.pagination.container.unbind(b), opts.pagination.anchorBuilder && opts.pagination.container.children().remove()), crsl.swipe && ($cfs.swipe("destroy"), $wrp.css("cursor", "default"), crsl.swipe = !1), crsl.mousewheel && (crsl.mousewheel = !1), nv_showNavi(opts, "hide", conf), nv_enableNavi(opts, "removeClass", conf)
		}, is_boolean(configs) && (configs = {
			debug: configs
		});
		var crsl = {
				direction: "next",
				isPaused: !0,
				isScrolling: !1,
				isStopped: !1,
				mousewheel: !1,
				swipe: !1
			},
			itms = {
				total: $cfs.children().length,
				first: 0
			},
			tmrs = {
				auto: null,
				progress: null,
				startTime: getTime(),
				timePassed: 0
			},
			scrl = {
				isStopped: !1,
				duration: 0,
				startTime: 0,
				easing: "",
				anims: []
			},
			clbk = {
				onBefore: [],
				onAfter: []
			},
			queu = [],
			conf = $.extend(!0, {}, $.fn.carouFredSel.configs, configs),
			opts = {},
			opts_orig = $.extend(!0, {}, options),
			$wrp = "parent" == conf.wrapper ? $cfs.parent() : $cfs.wrap("<" + conf.wrapper.element + ' class="' + conf.wrapper.classname + '" />').parent();
		if (conf.selector = $cfs.selector, conf.serialNumber = $.fn.carouFredSel.serialNumber++, conf.transition = conf.transition && $.fn.transition ? "transition" : "animate", FN._init(opts_orig, !0, starting_position), FN._build(), FN._bind_events(), FN._bind_buttons(), is_array(opts.items.start)) var start_arr = opts.items.start;
		else {
			var start_arr = [];
			0 != opts.items.start && start_arr.push(opts.items.start)
		}
		if (opts.cookie && start_arr.unshift(parseInt(cf_getCookie(opts.cookie), 10)), start_arr.length > 0)
			for (var a = 0, l = start_arr.length; l > a; a++) {
				var s = start_arr[a];
				if (0 != s) {
					if (s === !0) {
						if (s = window.location.hash, 1 > s.length) continue
					} else "random" === s && (s = Math.floor(Math.random() * itms.total));
					if ($cfs.triggerHandler(cf_e("slideTo", conf), [s, 0, !0, {
							fx: "none"
						}])) break
				}
			}
		var siz = sz_setSizes($cfs, opts),
			itm = gi_getCurrentItems($cfs.children(), opts);
		return opts.onCreate && opts.onCreate.call($tt0, {
			width: siz.width,
			height: siz.height,
			items: itm
		}), $cfs.trigger(cf_e("updatePageStatus", conf), [!0, siz]), $cfs.trigger(cf_e("linkAnchors", conf)), conf.debug && $cfs.trigger(cf_e("debug", conf)), $cfs
	}, $.fn.carouFredSel.serialNumber = 1, $.fn.carouFredSel.defaults = {
		synchronise: !1,
		infinite: !0,
		circular: !0,
		responsive: !1,
		direction: "left",
		items: {
			start: 0
		},
		scroll: {
			easing: "swing",
			duration: 500,
			pauseOnHover: !1,
			event: "click",
			queue: !1
		}
	}, $.fn.carouFredSel.configs = {
		debug: !1,
		transition: !1,
		onWindowResize: "throttle",
		events: {
			prefix: "",
			namespace: "cfs"
		},
		wrapper: {
			element: "div",
			classname: "caroufredsel_wrapper"
		},
		classnames: {}
	}, $.fn.carouFredSel.pageAnchorBuilder = function (a) {
		return '<a href="#"><span>' + a + "</span></a>"
	}, $.fn.carouFredSel.progressbarUpdater = function (a) {
		$(this).css("width", a + "%")
	}, $.fn.carouFredSel.cookie = {
		get: function (a) {
			a += "=";
			for (var b = document.cookie.split(";"), c = 0, d = b.length; d > c; c++) {
				for (var e = b[c];
					" " == e.charAt(0);) e = e.slice(1);
				if (0 == e.indexOf(a)) return e.slice(a.length)
			}
			return 0
		},
		set: function (a, b, c) {
			var d = "";
			if (c) {
				var e = new Date;
				e.setTime(e.getTime() + 1e3 * 60 * 60 * 24 * c), d = "; expires=" + e.toGMTString()
			}
			document.cookie = a + "=" + b + d + "; path=/"
		},
		remove: function (a) {
			$.fn.carouFredSel.cookie.set(a, "", -1)
		}
	}, $.extend($.easing, {
		quadratic: function (a) {
			var b = a * a;
			return a * (-b * a + 4 * b - 6 * a + 4)
		},
		cubic: function (a) {
			return a * (4 * a * a - 9 * a + 6)
		},
		elastic: function (a) {
			var b = a * a;
			return a * (33 * b * b - 106 * b * a + 126 * b - 67 * a + 15)
		}
	}))
})(jQuery);;
(function ($) {
	$(function () {
		$('.gem-clients-type-carousel-grid:not(.carousel-disabled)').each(function () {
			var $clientsCarouselElement = $(this);
			var $clientsItems = $('.gem-clients-slide', $clientsCarouselElement);
			var $clientsItemsWrap = $('<div class="gem-clients-grid-carousel-wrap"/>').appendTo($clientsCarouselElement);
			var $clientsItemsCarousel = $('<div class="gem-clients-grid-carousel"/>').appendTo($clientsItemsWrap);
			var $clientsItemsPagination = $('<div class="gem-clients-grid-pagination gem-mini-pagination"/>').appendTo($clientsItemsWrap);
			$clientsItems.appendTo($clientsItemsCarousel);
		});
		$('.gem_client_carousel-items').each(function () {
			var $clientsElement = $(this);
			var $clients = $('.gem-client-item', $clientsElement);
			var $clientsWrap = $('<div class="gem-client-carousel-item-wrap"/>').appendTo($clientsElement);
			var $clientsCarousel = $('<div class="gem-client-carousel"/>').appendTo($clientsWrap);
			var $clientsNavigation = $('<div class="gem-client-carousel-navigation"/>').appendTo($clientsWrap);
			var $clientsPrev = $('<a href="#" class="gem-prev gem-client-prev"/></a>').appendTo($clientsNavigation);
			var $clientsNext = $('<a href="#" class="gem-next gem-client-next"/></a>').appendTo($clientsNavigation);
			$clients.appendTo($clientsCarousel);
		});
		$('body').updateClientsGrid();
		$('body').updateClientsCarousel();
		$('.fullwidth-block').each(function () {
			$(this).on('updateClientsCarousel', function () {
				$(this).updateClientsCarousel();
			});
		});
		$('.gem_tab').on('tab-update', function () {
			$(this).updateClientsGrid();
		});
		$('.gem_accordion_content').on('accordion-update', function () {
			$(this).updateClientsGrid();
		});
		$(document).on('gem.show.vc.tabs', '[data-vc-accordion]', function () {
			$(this).data('vc.accordion').getTarget().updateClientsGrid();
		});
		$(document).on('gem.show.vc.accordion', '[data-vc-accordion]', function () {
			$(this).data('vc.accordion').getTarget().updateClientsGrid();
		});
	});
	$.fn.updateClientsGrid = function () {
		$('.gem-clients-type-carousel-grid:not(.carousel-disabled)', this).each(function () {
			var $clientsCarouselElement = $(this);
			var $clientsItemsCarousel = $('.gem-clients-grid-carousel', $clientsCarouselElement);
			var $clientsItemsPagination = $('.gem-mini-pagination', $clientsCarouselElement);
			var autoscroll = $clientsCarouselElement.data('autoscroll') > 0 ? $clientsCarouselElement.data('autoscroll') : false;
			$clientsCarouselElement.thegemPreloader(function () {
				var $clientsGridCarousel = $clientsItemsCarousel.carouFredSel({
					auto: autoscroll,
					circular: false,
					infinite: true,
					width: '100%',
					items: 1,
					responsive: true,
					height: 'auto',
					align: 'center',
					pagination: $clientsItemsPagination,
					scroll: {
						pauseOnHover: true
					}
				});
			});
		});
	}
	$.fn.updateClientsCarousel = function () {
		$('.gem_client_carousel-items:not(.carousel-disabled)', this).each(function () {
			var $clientsElement = $(this);
			var $clientsCarousel = $('.gem-client-carousel', $clientsElement);
			var $clientsPrev = $('.gem-client-prev', $clientsElement);
			var $clientsNext = $('.gem-client-next', $clientsElement);
			var autoscroll = $clientsElement.data('autoscroll') > 0 ? $clientsElement.data('autoscroll') : false;
			$clientsElement.thegemPreloader(function () {
				var $clientsView = $clientsCarousel.carouFredSel({
					auto: autoscroll,
					circular: true,
					infinite: false,
					scroll: {
						items: 1
					},
					width: '100%',
					responsive: false,
					height: 'auto',
					align: 'center',
					prev: $clientsPrev,
					next: $clientsNext
				});
			});
		});
	}
})(jQuery);;
(function ($) {
	$(function () {
		$('.gem-testimonials').each(function () {
			var $testimonialsElement = $(this);
			var $testimonials = $('.gem-testimonial-item', $testimonialsElement);
			var $testimonialsWrap = $('<div class="gem-testimonials-carousel-wrap"/>').appendTo($testimonialsElement);
			var $testimonialsCarousel = $('<div class="gem-testimonials-carousel"/>').appendTo($testimonialsWrap);
			if ($testimonialsElement.hasClass('fullwidth-block')) {
				$testimonialsCarousel.wrap('<div class="container" />');
			}
			var $testimonialsNavigation = $('<div class="gem-testimonials-navigation"/>').appendTo($testimonialsWrap);
			var $testimonialsPrev = $('<a href="javascript:void(0);" class="gem-prev gem-testimonials-prev"/></a>').appendTo($testimonialsNavigation);
			var $testimonialsNext = $('<a href="javascript:void(0);" class="gem-next gem-testimonials-next"/></a>').appendTo($testimonialsNavigation);
			$testimonials.appendTo($testimonialsCarousel);
		});
		$('body').updateTestimonialsCarousel();
		$('.fullwidth-block').each(function () {
			$(this).on('updateTestimonialsCarousel', function () {
				$(this).updateTestimonialsCarousel();
			});
		});
		$('.gem_tab').on('tab-update', function () {
			$(this).updateTestimonialsCarousel();
		});
		$('.gem_accordion_content').on('accordion-update', function () {
			$(this).updateTestimonialsCarousel();
		});
	});
	$.fn.updateTestimonialsCarousel = function () {
		$('.gem-testimonials', this).add($(this).filter('.gem-testimonials')).each(function () {
			var $testimonialsElement = $(this);
			var $testimonialsCarousel = $('.gem-testimonials-carousel', $testimonialsElement);
			var $testimonials = $('.gem-testimonial-item', $testimonialsCarousel);
			var $testimonialsPrev = $('.gem-testimonials-prev', $testimonialsElement);
			var $testimonialsNext = $('.gem-testimonials-next', $testimonialsElement);
			$testimonialsElement.thegemPreloader(function () {
				var $testimonialsView = $testimonialsCarousel.carouFredSel({
					auto: ($testimonialsElement.data('autoscroll') > 0 ? $testimonialsElement.data('autoscroll') : false),
					circular: true,
					infinite: true,
					width: '100%',
					height: 'auto',
					items: 1,
					align: 'center',
					responsive: true,
					swipe: true,
					prev: $testimonialsPrev,
					next: $testimonialsNext,
					scroll: {
						pauseOnHover: true,
						fx: 'scroll',
						easing: 'easeInOutCubic',
						duration: 1000,
						onBefore: function (data) {
							data.items.old.css({
								opacity: 1
							}).animate({
								opacity: 0
							}, 500, 'linear');
							data.items.visible.css({
								opacity: 0
							}).animate({
								opacity: 1
							}, 1000, 'linear');
						}
					}
				});
			});
		});
	}
})(jQuery);

// fin minify jquery dlmeanu 

/************************************************
 * REVOLUTION 5.4.2 EXTENSION - SLIDE ANIMATIONS
 * @version: 1.8 (17.05.2017)
 * @requires jquery.themepunch.revolution.js
 * @author ThemePunch
************************************************/
!function(a){"use strict";var b=jQuery.fn.revolution,c={alias:"SlideAnimations Min JS",name:"revolution.extensions.slideanims.min.js",min_core:"5.4.5",version:"1.8"};jQuery.extend(!0,b,{animateSlide:function(a,d,e,f,h,i,j,k){return"stop"===b.compare_version(c).check?k:g(a,d,e,f,h,i,j,k)}});var d=function(a,c,d,e){var f=a,g=f.find(".defaultimg"),h=g.data("mediafilter"),i=f.data("zoomstart"),j=f.data("rotationstart");void 0!=g.data("currotate")&&(j=g.data("currotate")),void 0!=g.data("curscale")&&"box"==e?i=100*g.data("curscale"):void 0!=g.data("curscale")&&(i=g.data("curscale")),b.slotSize(g,c);var k=g.attr("src"),l=g.data("bgcolor"),m=c.width,n=c.height,o=g.data("fxof"),p=0;void 0===l&&(l=g.css("backgroundColor")),"on"==c.autoHeight&&(n=c.c.height()),void 0==o&&(o=0);var q=0,r=g.data("bgfit"),s=g.data("bgrepeat"),t=g.data("bgposition");void 0==r&&(r="cover"),void 0==s&&(s="no-repeat"),void 0==t&&(t="center center");var u="";switch(u=void 0!==l&&l.indexOf("gradient")>=0?"background:"+l:"background-color:"+l+";background-image:url("+k+");background-repeat:"+s+";background-size:"+r+";background-position:"+t,e){case"box":for(var v=0,w=0,x=0;x<c.slots;x++){w=0;for(var y=0;y<c.slots;y++)f.append('<div class="slot" style="position:absolute;top:'+(p+w)+"px;left:"+(o+v)+"px;width:"+c.slotw+"px;height:"+c.sloth+'px;overflow:hidden;"><div class="slotslide '+h+'" data-x="'+v+'" data-y="'+w+'" style="position:absolute;top:0px;left:0px;width:'+c.slotw+"px;height:"+c.sloth+'px;overflow:hidden;"><div style="position:absolute;top:'+(0-w)+"px;left:"+(0-v)+"px;width:"+m+"px;height:"+n+"px;"+u+';"></div></div></div>'),w+=c.sloth,void 0!=i&&void 0!=j&&punchgs.TweenLite.set(f.find(".slot").last(),{rotationZ:j});v+=c.slotw}break;case"vertical":case"horizontal":if("horizontal"==e){if(!d)var q=0-c.slotw;for(var y=0;y<c.slots;y++)f.append('<div class="slot" style="position:absolute;top:'+(0+p)+"px;left:"+(o+y*c.slotw)+"px;overflow:hidden;width:"+(c.slotw+.3)+"px;height:"+n+'px"><div class="slotslide '+h+'" style="position:absolute;top:0px;left:'+q+"px;width:"+(c.slotw+.6)+"px;height:"+n+'px;overflow:hidden;"><div style="position:absolute;top:0px;left:'+(0-y*c.slotw)+"px;width:"+m+"px;height:"+n+"px;"+u+';"></div></div></div>'),void 0!=i&&void 0!=j&&punchgs.TweenLite.set(f.find(".slot").last(),{rotationZ:j})}else{if(!d)var q=0-c.sloth;for(var y=0;y<c.slots+2;y++)f.append('<div class="slot" style="position:absolute;top:'+(p+y*c.sloth)+"px;left:"+o+"px;overflow:hidden;width:"+m+"px;height:"+c.sloth+'px"><div class="slotslide '+h+'" style="position:absolute;top:'+q+"px;left:0px;width:"+m+"px;height:"+c.sloth+'px;overflow:hidden;"><div style="position:absolute;top:'+(0-y*c.sloth)+"px;left:0px;width:"+m+"px;height:"+n+"px;"+u+';"></div></div></div>'),void 0!=i&&void 0!=j&&punchgs.TweenLite.set(f.find(".slot").last(),{rotationZ:j})}}},e=function(a,b,c,d){function y(){jQuery.each(v,function(a,c){c[0]!=b&&c[8]!=b||(q=c[1],r=c[2],s=t),t+=1})}var e=a[0].opt,f=punchgs.Power1.easeIn,g=punchgs.Power1.easeOut,h=punchgs.Power1.easeInOut,i=punchgs.Power2.easeIn,j=punchgs.Power2.easeOut,k=punchgs.Power2.easeInOut,m=(punchgs.Power3.easeIn,punchgs.Power3.easeOut),n=punchgs.Power3.easeInOut,o=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45],p=[16,17,18,19,20,21,22,23,24,25,27],q=0,r=1,s=0,t=0,v=(new Array,[["boxslide",0,1,10,0,"box",!1,null,0,g,g,500,6],["boxfade",1,0,10,0,"box",!1,null,1,h,h,700,5],["slotslide-horizontal",2,0,0,200,"horizontal",!0,!1,2,k,k,700,3],["slotslide-vertical",3,0,0,200,"vertical",!0,!1,3,k,k,700,3],["curtain-1",4,3,0,0,"horizontal",!0,!0,4,g,g,300,5],["curtain-2",5,3,0,0,"horizontal",!0,!0,5,g,g,300,5],["curtain-3",6,3,25,0,"horizontal",!0,!0,6,g,g,300,5],["slotzoom-horizontal",7,0,0,400,"horizontal",!0,!0,7,g,g,300,7],["slotzoom-vertical",8,0,0,0,"vertical",!0,!0,8,j,j,500,8],["slotfade-horizontal",9,0,0,1e3,"horizontal",!0,null,9,j,j,2e3,10],["slotfade-vertical",10,0,0,1e3,"vertical",!0,null,10,j,j,2e3,10],["fade",11,0,1,300,"horizontal",!0,null,11,k,k,1e3,1],["crossfade",11,1,1,300,"horizontal",!0,null,11,k,k,1e3,1],["fadethroughdark",11,2,1,300,"horizontal",!0,null,11,k,k,1e3,1],["fadethroughlight",11,3,1,300,"horizontal",!0,null,11,k,k,1e3,1],["fadethroughtransparent",11,4,1,300,"horizontal",!0,null,11,k,k,1e3,1],["slideleft",12,0,1,0,"horizontal",!0,!0,12,n,n,1e3,1],["slideup",13,0,1,0,"horizontal",!0,!0,13,n,n,1e3,1],["slidedown",14,0,1,0,"horizontal",!0,!0,14,n,n,1e3,1],["slideright",15,0,1,0,"horizontal",!0,!0,15,n,n,1e3,1],["slideoverleft",12,7,1,0,"horizontal",!0,!0,12,n,n,1e3,1],["slideoverup",13,7,1,0,"horizontal",!0,!0,13,n,n,1e3,1],["slideoverdown",14,7,1,0,"horizontal",!0,!0,14,n,n,1e3,1],["slideoverright",15,7,1,0,"horizontal",!0,!0,15,n,n,1e3,1],["slideremoveleft",12,8,1,0,"horizontal",!0,!0,12,n,n,1e3,1],["slideremoveup",13,8,1,0,"horizontal",!0,!0,13,n,n,1e3,1],["slideremovedown",14,8,1,0,"horizontal",!0,!0,14,n,n,1e3,1],["slideremoveright",15,8,1,0,"horizontal",!0,!0,15,n,n,1e3,1],["papercut",16,0,0,600,"",null,null,16,n,n,1e3,2],["3dcurtain-horizontal",17,0,20,100,"vertical",!1,!0,17,h,h,500,7],["3dcurtain-vertical",18,0,10,100,"horizontal",!1,!0,18,h,h,500,5],["cubic",19,0,20,600,"horizontal",!1,!0,19,n,n,500,1],["cube",19,0,20,600,"horizontal",!1,!0,20,n,n,500,1],["flyin",20,0,4,600,"vertical",!1,!0,21,m,n,500,1],["turnoff",21,0,1,500,"horizontal",!1,!0,22,n,n,500,1],["incube",22,0,20,200,"horizontal",!1,!0,23,k,k,500,1],["cubic-horizontal",23,0,20,500,"vertical",!1,!0,24,j,j,500,1],["cube-horizontal",23,0,20,500,"vertical",!1,!0,25,j,j,500,1],["incube-horizontal",24,0,20,500,"vertical",!1,!0,26,k,k,500,1],["turnoff-vertical",25,0,1,200,"horizontal",!1,!0,27,k,k,500,1],["fadefromright",12,1,1,0,"horizontal",!0,!0,28,k,k,1e3,1],["fadefromleft",15,1,1,0,"horizontal",!0,!0,29,k,k,1e3,1],["fadefromtop",14,1,1,0,"horizontal",!0,!0,30,k,k,1e3,1],["fadefrombottom",13,1,1,0,"horizontal",!0,!0,31,k,k,1e3,1],["fadetoleftfadefromright",12,2,1,0,"horizontal",!0,!0,32,k,k,1e3,1],["fadetorightfadefromleft",15,2,1,0,"horizontal",!0,!0,33,k,k,1e3,1],["fadetobottomfadefromtop",14,2,1,0,"horizontal",!0,!0,34,k,k,1e3,1],["fadetotopfadefrombottom",13,2,1,0,"horizontal",!0,!0,35,k,k,1e3,1],["parallaxtoright",15,3,1,0,"horizontal",!0,!0,36,k,i,1500,1],["parallaxtoleft",12,3,1,0,"horizontal",!0,!0,37,k,i,1500,1],["parallaxtotop",14,3,1,0,"horizontal",!0,!0,38,k,f,1500,1],["parallaxtobottom",13,3,1,0,"horizontal",!0,!0,39,k,f,1500,1],["scaledownfromright",12,4,1,0,"horizontal",!0,!0,40,k,i,1e3,1],["scaledownfromleft",15,4,1,0,"horizontal",!0,!0,41,k,i,1e3,1],["scaledownfromtop",14,4,1,0,"horizontal",!0,!0,42,k,i,1e3,1],["scaledownfrombottom",13,4,1,0,"horizontal",!0,!0,43,k,i,1e3,1],["zoomout",13,5,1,0,"horizontal",!0,!0,44,k,i,1e3,1],["zoomin",13,6,1,0,"horizontal",!0,!0,45,k,i,1e3,1],["slidingoverlayup",27,0,1,0,"horizontal",!0,!0,47,h,g,2e3,1],["slidingoverlaydown",28,0,1,0,"horizontal",!0,!0,48,h,g,2e3,1],["slidingoverlayright",30,0,1,0,"horizontal",!0,!0,49,h,g,2e3,1],["slidingoverlayleft",29,0,1,0,"horizontal",!0,!0,50,h,g,2e3,1],["parallaxcirclesup",31,0,1,0,"horizontal",!0,!0,51,k,f,1500,1],["parallaxcirclesdown",32,0,1,0,"horizontal",!0,!0,52,k,f,1500,1],["parallaxcirclesright",33,0,1,0,"horizontal",!0,!0,53,k,f,1500,1],["parallaxcirclesleft",34,0,1,0,"horizontal",!0,!0,54,k,f,1500,1],["notransition",26,0,1,0,"horizontal",!0,null,46,k,i,1e3,1],["parallaxright",15,3,1,0,"horizontal",!0,!0,55,k,i,1500,1],["parallaxleft",12,3,1,0,"horizontal",!0,!0,56,k,i,1500,1],["parallaxup",14,3,1,0,"horizontal",!0,!0,57,k,f,1500,1],["parallaxdown",13,3,1,0,"horizontal",!0,!0,58,k,f,1500,1],["grayscale",11,5,1,300,"horizontal",!0,null,11,k,k,1e3,1],["grayscalecross",11,6,1,300,"horizontal",!0,null,11,k,k,1e3,1],["brightness",11,7,1,300,"horizontal",!0,null,11,k,k,1e3,1],["brightnesscross",11,8,1,300,"horizontal",!0,null,11,k,k,1e3,1],["blurlight",11,9,1,300,"horizontal",!0,null,11,k,k,1e3,1],["blurlightcross",11,10,1,300,"horizontal",!0,null,11,k,k,1e3,1],["blurstrong",11,9,1,300,"horizontal",!0,null,11,k,k,1e3,1],["blurstrongcross",11,10,1,300,"horizontal",!0,null,11,k,k,1e3,1]]);e.duringslidechange=!0,e.testanims=!1,1==e.testanims&&(e.nexttesttransform=void 0===e.nexttesttransform?34:e.nexttesttransform+1,e.nexttesttransform=e.nexttesttransform>70?0:e.nexttesttransform,b=v[e.nexttesttransform][0],console.log(b+"  "+e.nexttesttransform+"  "+v[e.nexttesttransform][1]+"  "+v[e.nexttesttransform][2])),jQuery.each(["parallaxcircles","slidingoverlay","slide","slideover","slideremove","parallax","parralaxto"],function(a,c){b==c+"horizontal"&&(b=1!=d?c+"left":c+"right"),b==c+"vertical"&&(b=1!=d?c+"up":c+"down")}),"random"==b&&(b=Math.round(Math.random()*v.length-1))>v.length-1&&(b=v.length-1),"random-static"==b&&(b=Math.round(Math.random()*o.length-1),b>o.length-1&&(b=o.length-1),b=o[b]),"random-premium"==b&&(b=Math.round(Math.random()*p.length-1),b>p.length-1&&(b=p.length-1),b=p[b]);var w=[12,13,14,15,16,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45];if(1==e.isJoomla&&void 0!=window.MooTools&&-1!=w.indexOf(b)){var x=Math.round(Math.random()*(p.length-2))+1;x>p.length-1&&(x=p.length-1),0==x&&(x=1),b=p[x]}y(),q>30&&(q=30),q<0&&(q=0);var z=new Object;return z.nexttrans=q,z.STA=v[s],z.specials=r,z},f=function(a,b){return void 0==b||jQuery.isNumeric(a)?a:void 0==a?a:a.split(",")[b]},g=function(a,b,c,g,h,i,j,k){function V(a,b,c,d,e){var f=a.find(".slot"),g=6,h=[2,1.2,.9,.7,.55,.42],j=a.width(),l=a.height();f.wrap('<div class="slot-circle-wrapper" style="overflow:hidden;position:absolute;border:1px solid #fff"></div>');for(var n=0;n<g;n++)f.parent().clone(!1).appendTo(i);a.find(".slot-circle-wrapper").each(function(a){if(a<g){var d=jQuery(this),f=d.find(".slot"),i=j>l?h[a]*j:h[a]*l,m=i,n=m/2-j/2+0,o=i/2-l/2+0,p=0!=a?"50%":"0",q=l/2-i/2,r=33==c?j/2-m/2:34==c?j-m:j/2-m/2,s={scale:1,transformOrigo:"50% 50%",width:m+"px",height:i+"px",top:q+"px",left:r+"px",borderRadius:p},t={scale:1,top:l/2-i/2,left:j/2-m/2,ease:e},u=o,v=33==c?n:34==c?n+j/2:n,w={width:j,height:l,autoAlpha:1,top:u+"px",position:"absolute",left:v+"px"},x={top:o+"px",left:n+"px",ease:e},y=b,z=0;k.add(punchgs.TweenLite.fromTo(d,y,s,t),z),k.add(punchgs.TweenLite.fromTo(f,y,w,x),z),k.add(punchgs.TweenLite.fromTo(d,.001,{autoAlpha:0},{autoAlpha:1}),0)}})}var l=c[0].opt,m=h.index(),n=g.index(),o=n<m?1:0;"arrow"==l.sc_indicator&&(o=l.sc_indicator_dir);var p=e(c,b,i,o),q=p.STA,r=p.specials,a=p.nexttrans;"on"==i.data("kenburns")&&(a=11);var s=g.data("nexttransid")||0,t=f(g.data("masterspeed"),s);t="default"===t?q[11]:"random"===t?Math.round(1e3*Math.random()+300):void 0!=t?parseInt(t,0):q[11],t=t>l.delay?l.delay:t,t+=q[4],l.slots=f(g.data("slotamount"),s),l.slots=void 0==l.slots||"default"==l.slots?q[12]:"random"==l.slots?Math.round(12*Math.random()+4):l.slots,l.slots=l.slots<1?"boxslide"==b?Math.round(6*Math.random()+3):"flyin"==b?Math.round(4*Math.random()+1):l.slots:l.slots,l.slots=(4==a||5==a||6==a)&&l.slots<3?3:l.slots,l.slots=0!=q[3]?Math.min(l.slots,q[3]):l.slots,l.slots=9==a?l.width/l.slots:10==a?l.height/l.slots:l.slots,l.rotate=f(g.data("rotate"),s),l.rotate=void 0==l.rotate||"default"==l.rotate?0:999==l.rotate||"random"==l.rotate?Math.round(360*Math.random()):l.rotate,l.rotate=l.ie||l.ie9?0:l.rotate,11!=a&&(null!=q[7]&&d(j,l,q[7],q[5]),null!=q[6]&&d(i,l,q[6],q[5])),k.add(punchgs.TweenLite.set(i.find(".defaultvid"),{y:0,x:0,top:0,left:0,scale:1}),0),k.add(punchgs.TweenLite.set(j.find(".defaultvid"),{y:0,x:0,top:0,left:0,scale:1}),0),k.add(punchgs.TweenLite.set(i.find(".defaultvid"),{y:"+0%",x:"+0%"}),0),k.add(punchgs.TweenLite.set(j.find(".defaultvid"),{y:"+0%",x:"+0%"}),0),k.add(punchgs.TweenLite.set(i,{autoAlpha:1,y:"+0%",x:"+0%"}),0),k.add(punchgs.TweenLite.set(j,{autoAlpha:1,y:"+0%",x:"+0%"}),0),k.add(punchgs.TweenLite.set(i.parent(),{backgroundColor:"transparent"}),0),k.add(punchgs.TweenLite.set(j.parent(),{backgroundColor:"transparent"}),0);var u=f(g.data("easein"),s),v=f(g.data("easeout"),s);if(u="default"===u?q[9]||punchgs.Power2.easeInOut:u||q[9]||punchgs.Power2.easeInOut,v="default"===v?q[10]||punchgs.Power2.easeInOut:v||q[10]||punchgs.Power2.easeInOut,0==a){var w=Math.ceil(l.height/l.sloth),x=0;i.find(".slotslide").each(function(a){var b=jQuery(this);x+=1,x==w&&(x=0),k.add(punchgs.TweenLite.from(b,t/600,{opacity:0,top:0-l.sloth,left:0-l.slotw,rotation:l.rotate,force3D:"auto",ease:u}),(15*a+30*x)/1500)})}if(1==a){var y,z=0;i.find(".slotslide").each(function(a){var b=jQuery(this),c=Math.random()*t+300,d=500*Math.random()+200;c+d>y&&(y=d+d,z=a),k.add(punchgs.TweenLite.from(b,c/1e3,{autoAlpha:0,force3D:"auto",rotation:l.rotate,ease:u}),d/1e3)})}if(2==a){var A=new punchgs.TimelineLite;j.find(".slotslide").each(function(){var a=jQuery(this);A.add(punchgs.TweenLite.to(a,t/1e3,{left:l.slotw,ease:u,force3D:"auto",rotation:0-l.rotate}),0),k.add(A,0)}),i.find(".slotslide").each(function(){var a=jQuery(this);A.add(punchgs.TweenLite.from(a,t/1e3,{left:0-l.slotw,ease:u,force3D:"auto",rotation:l.rotate}),0),k.add(A,0)})}if(3==a){var A=new punchgs.TimelineLite;j.find(".slotslide").each(function(){var a=jQuery(this);A.add(punchgs.TweenLite.to(a,t/1e3,{top:l.sloth,ease:u,rotation:l.rotate,force3D:"auto",transformPerspective:600}),0),k.add(A,0)}),i.find(".slotslide").each(function(){var a=jQuery(this);A.add(punchgs.TweenLite.from(a,t/1e3,{top:0-l.sloth,rotation:l.rotate,ease:v,force3D:"auto",transformPerspective:600}),0),k.add(A,0)})}if(4==a||5==a){setTimeout(function(){j.find(".defaultimg").css({opacity:0})},100);var B=t/1e3,A=new punchgs.TimelineLite;j.find(".slotslide").each(function(b){var c=jQuery(this),d=b*B/l.slots;5==a&&(d=(l.slots-b-1)*B/l.slots/1.5),A.add(punchgs.TweenLite.to(c,3*B,{transformPerspective:600,force3D:"auto",top:0+l.height,opacity:.5,rotation:l.rotate,ease:u,delay:d}),0),k.add(A,0)}),i.find(".slotslide").each(function(b){var c=jQuery(this),d=b*B/l.slots;5==a&&(d=(l.slots-b-1)*B/l.slots/1.5),A.add(punchgs.TweenLite.from(c,3*B,{top:0-l.height,opacity:.5,rotation:l.rotate,force3D:"auto",ease:punchgs.eo,delay:d}),0),k.add(A,0)})}if(6==a){l.slots<2&&(l.slots=2),l.slots%2&&(l.slots=l.slots+1);var A=new punchgs.TimelineLite;setTimeout(function(){j.find(".defaultimg").css({opacity:0})},100),j.find(".slotslide").each(function(a){var b=jQuery(this);if(a+1<l.slots/2)var c=90*(a+2);else var c=90*(2+l.slots-a);A.add(punchgs.TweenLite.to(b,(t+c)/1e3,{top:0+l.height,opacity:1,force3D:"auto",rotation:l.rotate,ease:u}),0),k.add(A,0)}),i.find(".slotslide").each(function(a){var b=jQuery(this);if(a+1<l.slots/2)var c=90*(a+2);else var c=90*(2+l.slots-a);A.add(punchgs.TweenLite.from(b,(t+c)/1e3,{top:0-l.height,opacity:1,force3D:"auto",rotation:l.rotate,ease:v}),0),k.add(A,0)})}if(7==a){t*=2,t>l.delay&&(t=l.delay);var A=new punchgs.TimelineLite;setTimeout(function(){j.find(".defaultimg").css({opacity:0})},100),j.find(".slotslide").each(function(){var a=jQuery(this).find("div");A.add(punchgs.TweenLite.to(a,t/1e3,{left:0-l.slotw/2+"px",top:0-l.height/2+"px",width:2*l.slotw+"px",height:2*l.height+"px",opacity:0,rotation:l.rotate,force3D:"auto",ease:u}),0),k.add(A,0)}),i.find(".slotslide").each(function(a){var b=jQuery(this).find("div");A.add(punchgs.TweenLite.fromTo(b,t/1e3,{left:0,top:0,opacity:0,transformPerspective:600},{left:0-a*l.slotw+"px",ease:v,force3D:"auto",top:"0px",width:l.width,height:l.height,opacity:1,rotation:0,delay:.1}),0),k.add(A,0)})}if(8==a){t*=3,t>l.delay&&(t=l.delay);var A=new punchgs.TimelineLite;j.find(".slotslide").each(function(){var a=jQuery(this).find("div");A.add(punchgs.TweenLite.to(a,t/1e3,{left:0-l.width/2+"px",top:0-l.sloth/2+"px",width:2*l.width+"px",height:2*l.sloth+"px",force3D:"auto",ease:u,opacity:0,rotation:l.rotate}),0),k.add(A,0)}),i.find(".slotslide").each(function(a){var b=jQuery(this).find("div");A.add(punchgs.TweenLite.fromTo(b,t/1e3,{left:0,top:0,opacity:0,force3D:"auto"},{left:"0px",top:0-a*l.sloth+"px",width:i.find(".defaultimg").data("neww")+"px",height:i.find(".defaultimg").data("newh")+"px",opacity:1,ease:v,rotation:0}),0),k.add(A,0)})}if(9==a||10==a){var D=0;i.find(".slotslide").each(function(a){var b=jQuery(this);D++,k.add(punchgs.TweenLite.fromTo(b,t/2e3,{autoAlpha:0,force3D:"auto",transformPerspective:600},{autoAlpha:1,ease:u,delay:a*l.slots/100/2e3}),0)})}if(27==a||28==a||29==a||30==a){var E=i.find(".slot"),F=27==a||28==a?1:2,G=27==a||29==a?"-100%":"+100%",H=27==a||29==a?"+100%":"-100%",I=27==a||29==a?"-80%":"80%",J=27==a||29==a?"+80%":"-80%",K=27==a||29==a?"+10%":"-10%",L={overwrite:"all"},M={autoAlpha:0,zIndex:1,force3D:"auto",ease:u},N={position:"inherit",autoAlpha:0,overwrite:"all",zIndex:1},O={autoAlpha:1,force3D:"auto",ease:v},P={overwrite:"all",zIndex:2,opacity:1,autoAlpha:1},Q={autoAlpha:1,force3D:"auto",overwrite:"all",ease:u},R={overwrite:"all",zIndex:2,autoAlpha:1},S={autoAlpha:1,force3D:"auto",ease:u},T=1==F?"y":"x";L[T]="0px",M[T]=G,N[T]=K,O[T]="0%",P[T]=H,Q[T]=G,R[T]=I,S[T]=J,E.append('<span style="background-color:rgba(0,0,0,0.6);width:100%;height:100%;position:absolute;top:0px;left:0px;display:block;z-index:2"></span>'),k.add(punchgs.TweenLite.fromTo(j,t/1e3,L,M),0),k.add(punchgs.TweenLite.fromTo(i.find(".defaultimg"),t/2e3,N,O),t/2e3),k.add(punchgs.TweenLite.fromTo(E,t/1e3,P,Q),0),k.add(punchgs.TweenLite.fromTo(E.find(".slotslide div"),t/1e3,R,S),0)}if(31==a||32==a||33==a||34==a){t=6e3,u=punchgs.Power3.easeInOut;var U=t/1e3;mas=U-U/5,_nt=a,fy=31==_nt?"+100%":32==_nt?"-100%":"0%",fx=33==_nt?"+100%":34==_nt?"-100%":"0%",ty=31==_nt?"-100%":32==_nt?"+100%":"0%",tx=33==_nt?"-100%":34==_nt?"+100%":"0%",k.add(punchgs.TweenLite.fromTo(j,U-.2*U,{y:0,x:0},{y:ty,x:tx,ease:v}),.2*U),k.add(punchgs.TweenLite.fromTo(i,U,{y:fy,x:fx},{y:"0%",x:"0%",ease:u}),0),i.find(".slot").remove(),i.find(".defaultimg").clone().appendTo(i).addClass("slot"),V(i,U,_nt,"in",u)}if(11==a){r>12&&(r=0);var D=0,W=2==r?"#000000":3==r?"#ffffff":"transparent";switch(r){case 0:k.add(punchgs.TweenLite.fromTo(i,t/1e3,{autoAlpha:0},{autoAlpha:1,force3D:"auto",ease:u}),0);break;case 1:k.add(punchgs.TweenLite.fromTo(i,t/1e3,{autoAlpha:0},{autoAlpha:1,force3D:"auto",ease:u}),0),k.add(punchgs.TweenLite.fromTo(j,t/1e3,{autoAlpha:1},{autoAlpha:0,force3D:"auto",ease:u}),0);break;case 2:case 3:case 4:k.add(punchgs.TweenLite.set(j.parent(),{backgroundColor:W,force3D:"auto"}),0),k.add(punchgs.TweenLite.set(i.parent(),{backgroundColor:"transparent",force3D:"auto"}),0),k.add(punchgs.TweenLite.to(j,t/2e3,{autoAlpha:0,force3D:"auto",ease:u}),0),k.add(punchgs.TweenLite.fromTo(i,t/2e3,{autoAlpha:0},{autoAlpha:1,force3D:"auto",ease:u}),t/2e3);break;case 5:case 6:case 7:case 8:case 9:case 10:case 11:case 12:var X=jQuery.inArray(r,[9,10])>=0?5:jQuery.inArray(r,[11,12])>=0?10:0,Y=jQuery.inArray(r,[5,6,7,8])>=0?100:0,Z=jQuery.inArray(r,[7,8])>=0?300:0,$="blur("+X+"px) grayscale("+Y+"%) brightness("+Z+"%)",_="blur(0px) grayscale(0%) brightness(100%)";k.add(punchgs.TweenLite.fromTo(i,t/1e3,{autoAlpha:0,filter:$,"-webkit-filter":$},{autoAlpha:1,filter:_,"-webkit-filter":_,force3D:"auto",ease:u}),0),jQuery.inArray(r,[6,8,10])>=0&&k.add(punchgs.TweenLite.fromTo(j,t/1e3,{autoAlpha:1,filter:_,"-webkit-filter":_},{autoAlpha:0,force3D:"auto",ease:u,filter:$,"-webkit-filter":$}),0)}k.add(punchgs.TweenLite.set(i.find(".defaultimg"),{autoAlpha:1}),0),k.add(punchgs.TweenLite.set(j.find("defaultimg"),{autoAlpha:1}),0)}if(26==a){var D=0;t=0,k.add(punchgs.TweenLite.fromTo(i,t/1e3,{autoAlpha:0},{autoAlpha:1,force3D:"auto",ease:u}),0),k.add(punchgs.TweenLite.to(j,t/1e3,{autoAlpha:0,force3D:"auto",ease:u}),0),k.add(punchgs.TweenLite.set(i.find(".defaultimg"),{autoAlpha:1}),0),k.add(punchgs.TweenLite.set(j.find("defaultimg"),{autoAlpha:1}),0)}if(12==a||13==a||14==a||15==a){t=t,t>l.delay&&(t=l.delay),setTimeout(function(){punchgs.TweenLite.set(j.find(".defaultimg"),{autoAlpha:0})},100);var aa=l.width,ba=l.height,ca=i.find(".slotslide, .defaultvid"),da=0,ea=0,fa=1,ga=1,ha=1,ia=t/1e3,ja=ia;"fullwidth"!=l.sliderLayout&&"fullscreen"!=l.sliderLayout||(aa=ca.width(),ba=ca.height()),12==a?da=aa:15==a?da=0-aa:13==a?ea=ba:14==a&&(ea=0-ba),1==r&&(fa=0),2==r&&(fa=0),3==r&&(ia=t/1300),4!=r&&5!=r||(ga=.6),6==r&&(ga=1.4),5!=r&&6!=r||(ha=1.4,fa=0,aa=0,ba=0,da=0,ea=0),6==r&&(ha=.6);7==r&&(aa=0,ba=0);var la=i.find(".slotslide"),ma=j.find(".slotslide, .defaultvid");if(k.add(punchgs.TweenLite.set(h,{zIndex:15}),0),k.add(punchgs.TweenLite.set(g,{zIndex:20}),0),8==r?(k.add(punchgs.TweenLite.set(h,{zIndex:20}),0),k.add(punchgs.TweenLite.set(g,{zIndex:15}),0),k.add(punchgs.TweenLite.set(la,{left:0,top:0,scale:1,opacity:1,rotation:0,ease:u,force3D:"auto"}),0)):k.add(punchgs.TweenLite.from(la,ia,{left:da,top:ea,scale:ha,opacity:fa,rotation:l.rotate,ease:u,force3D:"auto"}),0),4!=r&&5!=r||(aa=0,ba=0),1!=r)switch(a){case 12:k.add(punchgs.TweenLite.to(ma,ja,{left:0-aa+"px",force3D:"auto",scale:ga,opacity:fa,rotation:l.rotate,ease:v}),0);break;case 15:k.add(punchgs.TweenLite.to(ma,ja,{left:aa+"px",force3D:"auto",scale:ga,opacity:fa,rotation:l.rotate,ease:v}),0);break;case 13:k.add(punchgs.TweenLite.to(ma,ja,{top:0-ba+"px",force3D:"auto",scale:ga,opacity:fa,rotation:l.rotate,ease:v}),0);break;case 14:k.add(punchgs.TweenLite.to(ma,ja,{top:ba+"px",force3D:"auto",scale:ga,opacity:fa,rotation:l.rotate,ease:v}),0)}}if(16==a){var A=new punchgs.TimelineLite;k.add(punchgs.TweenLite.set(h,{position:"absolute","z-index":20}),0),k.add(punchgs.TweenLite.set(g,{position:"absolute","z-index":15}),0),h.wrapInner('<div class="tp-half-one" style="position:relative; width:100%;height:100%"></div>'),h.find(".tp-half-one").clone(!0).appendTo(h).addClass("tp-half-two"),h.find(".tp-half-two").removeClass("tp-half-one");var aa=l.width,ba=l.height;"on"==l.autoHeight&&(ba=c.height()),h.find(".tp-half-one .defaultimg").wrap('<div class="tp-papercut" style="width:'+aa+"px;height:"+ba+'px;"></div>'),h.find(".tp-half-two .defaultimg").wrap('<div class="tp-papercut" style="width:'+aa+"px;height:"+ba+'px;"></div>'),h.find(".tp-half-two .defaultimg").css({position:"absolute",top:"-50%"}),h.find(".tp-half-two .tp-caption").wrapAll('<div style="position:absolute;top:-50%;left:0px;"></div>'),k.add(punchgs.TweenLite.set(h.find(".tp-half-two"),{width:aa,height:ba,overflow:"hidden",zIndex:15,position:"absolute",top:ba/2,left:"0px",transformPerspective:600,transformOrigin:"center bottom"}),0),k.add(punchgs.TweenLite.set(h.find(".tp-half-one"),{width:aa,height:ba/2,overflow:"visible",zIndex:10,position:"absolute",top:"0px",left:"0px",transformPerspective:600,transformOrigin:"center top"}),0);var oa=(h.find(".defaultimg"),Math.round(20*Math.random()-10)),pa=Math.round(20*Math.random()-10),qa=Math.round(20*Math.random()-10),ra=.4*Math.random()-.2,sa=.4*Math.random()-.2,ta=1*Math.random()+1,ua=1*Math.random()+1,va=.3*Math.random()+.3;k.add(punchgs.TweenLite.set(h.find(".tp-half-one"),{overflow:"hidden"}),0),k.add(punchgs.TweenLite.fromTo(h.find(".tp-half-one"),t/800,{width:aa,height:ba/2,position:"absolute",top:"0px",left:"0px",force3D:"auto",transformOrigin:"center top"},{scale:ta,rotation:oa,y:0-ba-ba/4,autoAlpha:0,ease:u}),0),k.add(punchgs.TweenLite.fromTo(h.find(".tp-half-two"),t/800,{width:aa,height:ba,overflow:"hidden",position:"absolute",top:ba/2,left:"0px",force3D:"auto",transformOrigin:"center bottom"},{scale:ua,rotation:pa,y:ba+ba/4,ease:u,autoAlpha:0,onComplete:function(){punchgs.TweenLite.set(h,{position:"absolute","z-index":15}),punchgs.TweenLite.set(g,{position:"absolute","z-index":20}),h.find(".tp-half-one").length>0&&(h.find(".tp-half-one .defaultimg").unwrap(),h.find(".tp-half-one .slotholder").unwrap()),h.find(".tp-half-two").remove()}}),0),A.add(punchgs.TweenLite.set(i.find(".defaultimg"),{autoAlpha:1}),0),null!=h.html()&&k.add(punchgs.TweenLite.fromTo(g,(t-200)/1e3,{scale:va,x:l.width/4*ra,y:ba/4*sa,rotation:qa,force3D:"auto",transformOrigin:"center center",ease:v},{autoAlpha:1,scale:1,x:0,y:0,rotation:0}),0),k.add(A,0)}if(17==a&&i.find(".slotslide").each(function(a){var b=jQuery(this);k.add(punchgs.TweenLite.fromTo(b,t/800,{opacity:0,rotationY:0,scale:.9,rotationX:-110,force3D:"auto",transformPerspective:600,transformOrigin:"center center"},{opacity:1,top:0,left:0,scale:1,rotation:0,rotationX:0,force3D:"auto",rotationY:0,ease:u,delay:.06*a}),0)}),18==a&&i.find(".slotslide").each(function(a){var b=jQuery(this);k.add(punchgs.TweenLite.fromTo(b,t/500,{autoAlpha:0,rotationY:110,scale:.9,rotationX:10,force3D:"auto",transformPerspective:600,transformOrigin:"center center"},{autoAlpha:1,top:0,left:0,scale:1,rotation:0,rotationX:0,force3D:"auto",rotationY:0,ease:u,delay:.06*a}),0)}),19==a||22==a){var A=new punchgs.TimelineLite;k.add(punchgs.TweenLite.set(h,{zIndex:20}),0),k.add(punchgs.TweenLite.set(g,{zIndex:20}),0),setTimeout(function(){j.find(".defaultimg").css({opacity:0})},100);var wa=90,fa=1,xa="center center ";1==o&&(wa=-90),19==a?(xa=xa+"-"+l.height/2,fa=0):xa+=l.height/2,punchgs.TweenLite.set(c,{transformStyle:"flat",backfaceVisibility:"hidden",transformPerspective:600}),i.find(".slotslide").each(function(a){var b=jQuery(this);A.add(punchgs.TweenLite.fromTo(b,t/1e3,{transformStyle:"flat",backfaceVisibility:"hidden",left:0,rotationY:l.rotate,z:10,top:0,scale:1,force3D:"auto",transformPerspective:600,transformOrigin:xa,rotationX:wa},{left:0,rotationY:0,top:0,z:0,scale:1,force3D:"auto",rotationX:0,delay:50*a/1e3,ease:u}),0),A.add(punchgs.TweenLite.to(b,.1,{autoAlpha:1,delay:50*a/1e3}),0),k.add(A)}),j.find(".slotslide").each(function(a){var b=jQuery(this),c=-90;1==o&&(c=90),A.add(punchgs.TweenLite.fromTo(b,t/1e3,{transformStyle:"flat",backfaceVisibility:"hidden",autoAlpha:1,rotationY:0,top:0,z:0,scale:1,force3D:"auto",transformPerspective:600,transformOrigin:xa,rotationX:0},{autoAlpha:1,rotationY:l.rotate,top:0,z:10,scale:1,rotationX:c,delay:50*a/1e3,force3D:"auto",ease:v}),0),k.add(A)}),k.add(punchgs.TweenLite.set(h,{zIndex:18}),0)}if(20==a){if(setTimeout(function(){j.find(".defaultimg").css({opacity:0})},100),1==o)var ya=-l.width,wa=80,xa="20% 70% -"+l.height/2;else var ya=l.width,wa=-80,xa="80% 70% -"+l.height/2;i.find(".slotslide").each(function(a){var b=jQuery(this),c=50*a/1e3;k.add(punchgs.TweenLite.fromTo(b,t/1e3,{left:ya,rotationX:40,z:-600,opacity:fa,top:0,scale:1,force3D:"auto",transformPerspective:600,transformOrigin:xa,transformStyle:"flat",rotationY:wa},{left:0,rotationX:0,opacity:1,top:0,z:0,scale:1,rotationY:0,delay:c,ease:u}),0)}),j.find(".slotslide").each(function(a){var b=jQuery(this),c=50*a/1e3;if(c=a>0?c+t/9e3:0,1!=o)var d=-l.width/2,e=30,f="20% 70% -"+l.height/2;else var d=l.width/2,e=-30,f="80% 70% -"+l.height/2;v=punchgs.Power2.easeInOut,k.add(punchgs.TweenLite.fromTo(b,t/1e3,{opacity:1,rotationX:0,top:0,z:0,scale:1,left:0,force3D:"auto",transformPerspective:600,transformOrigin:f,transformStyle:"flat",rotationY:0},{opacity:1,rotationX:20,top:0,z:-600,left:d,force3D:"auto",rotationY:e,delay:c,ease:v}),0)})}if(21==a||25==a){setTimeout(function(){j.find(".defaultimg").css({opacity:0})},100);var wa=90,ya=-l.width,za=-wa;if(1==o)if(25==a){var xa="center top 0";wa=l.rotate}else{var xa="left center 0";za=l.rotate}else if(ya=l.width,wa=-90,25==a){var xa="center bottom 0";za=-wa,wa=l.rotate}else{var xa="right center 0";za=l.rotate}i.find(".slotslide").each(function(a){var b=jQuery(this),c=t/1.5/3;k.add(punchgs.TweenLite.fromTo(b,2*c/1e3,{left:0,transformStyle:"flat",rotationX:za,z:0,autoAlpha:0,top:0,scale:1,force3D:"auto",transformPerspective:1200,transformOrigin:xa,rotationY:wa},{left:0,rotationX:0,top:0,z:0,autoAlpha:1,scale:1,rotationY:0,force3D:"auto",delay:c/1e3,ease:u}),0)}),1!=o?(ya=-l.width,wa=90,25==a?(xa="center top 0",za=-wa,wa=l.rotate):(xa="left center 0",za=l.rotate)):(ya=l.width,wa=-90,25==a?(xa="center bottom 0",za=-wa,wa=l.rotate):(xa="right center 0",za=l.rotate)),j.find(".slotslide").each(function(a){var b=jQuery(this);k.add(punchgs.TweenLite.fromTo(b,t/1e3,{left:0,transformStyle:"flat",rotationX:0,z:0,autoAlpha:1,top:0,scale:1,force3D:"auto",transformPerspective:1200,transformOrigin:xa,rotationY:0},{left:0,rotationX:za,top:0,z:0,autoAlpha:1,force3D:"auto",scale:1,rotationY:wa,ease:v}),0)})}if(23==a||24==a){setTimeout(function(){j.find(".defaultimg").css({opacity:0})},100);var wa=-90,fa=1,Aa=0;if(1==o&&(wa=90),23==a){var xa="center center -"+l.width/2;fa=0}else var xa="center center "+l.width/2;punchgs.TweenLite.set(c,{transformStyle:"preserve-3d",backfaceVisibility:"hidden",perspective:2500}),i.find(".slotslide").each(function(a){var b=jQuery(this);k.add(punchgs.TweenLite.fromTo(b,t/1e3,{left:Aa,rotationX:l.rotate,force3D:"auto",opacity:fa,top:0,scale:1,transformPerspective:1200,transformOrigin:xa,rotationY:wa},{left:0,rotationX:0,autoAlpha:1,top:0,z:0,scale:1,rotationY:0,delay:50*a/500,ease:u}),0)}),wa=90,1==o&&(wa=-90),j.find(".slotslide").each(function(b){var c=jQuery(this);k.add(punchgs.TweenLite.fromTo(c,t/1e3,{left:0,rotationX:0,top:0,z:0,scale:1,force3D:"auto",transformStyle:"flat",transformPerspective:1200,transformOrigin:xa,rotationY:0},{left:Aa,rotationX:l.rotate,top:0,scale:1,rotationY:wa,delay:50*b/500,ease:v}),0),23==a&&k.add(punchgs.TweenLite.fromTo(c,t/2e3,{autoAlpha:1},{autoAlpha:0,delay:50*b/500+t/3e3,ease:v}),0)})}return k}}(jQuery);


// fin slide

/************************************************
 * REVOLUTION 5.4.2 EXTENSION - LAYER ANIMATION
 * @version: 3.6.3 (17.05.2017)
 * @requires jquery.themepunch.revolution.js
 * @author ThemePunch
************************************************/
!function(a){"use strict";function w(a,b,c,d,e,f,g){var h=a.find(b);h.css("borderWidth",f+"px"),h.css(c,0-f+"px"),h.css(d,"0px solid transparent"),h.css(e,g)}var b=jQuery.fn.revolution,e=(b.is_mobile(),b.is_android(),{alias:"LayerAnimation Min JS",name:"revolution.extensions.layeranimation.min.js",min_core:"5.4.5",version:"3.6.3"});jQuery.extend(!0,b,{updateMarkup:function(a,b){var c=jQuery(a).data();if(void 0!==c.start&&!c.frames_added&&void 0===c.frames){var d=new Array,e=t(m(),c.transform_in,void 0,!1),f=t(m(),c.transform_out,void 0,!1),g=t(m(),c.transform_hover,void 0,!1);jQuery.isNumeric(c.end)&&jQuery.isNumeric(c.start)&&jQuery.isNumeric(e.speed)&&(c.end=parseInt(c.end,0)-(parseInt(c.start,0)+parseFloat(e.speed,0))),d.push({frame:"0",delay:c.start,from:c.transform_in,to:c.transform_idle,split:c.splitin,speed:e.speed,ease:e.anim.ease,mask:c.mask_in,splitdelay:c.elementdelay}),d.push({frame:"5",delay:c.end,to:c.transform_out,split:c.splitout,speed:f.speed,ease:f.anim.ease,mask:c.mask_out,splitdelay:c.elementdelay}),c.transform_hover&&d.push({frame:"hover",to:c.transform_hover,style:c.style_hover,speed:g.speed,ease:g.anim.ease,splitdelay:c.elementdelay}),c.frames=d}if(!c.frames_added){if(c.inframeindex=0,c.outframeindex=-1,c.hoverframeindex=-1,void 0!==c.frames)for(var h=0;h<c.frames.length;h++)void 0!==c.frames[h].sfx_effect&&c.frames[h].sfx_effect.indexOf("block")>=0&&(0===h?(c.frames[h].from="o:0",c.frames[h].to="o:1"):c.frames[h].to="o:0",c._sfx="block"),void 0===c.frames[0].from&&(c.frames[0].from="o:inherit"),0===c.frames[0].delay&&(c.frames[0].delay=20),"hover"===c.frames[h].frame?c.hoverframeindex=h:"frame_999"!==c.frames[h].frame&&"frame_out"!==c.frames[h].frame&&"last"!==c.frames[h].frame&&"end"!==c.frames[h].frame||(c.outframeindex=h),void 0!==c.frames[h].split&&c.frames[h].split.match(/chars|words|lines/g)&&(c.splittext=!0);c.outframeindex=-1===c.outframeindex?-1===c.hoverframeindex?c.frames.length-1:c.frames.length-2:c.outframeindex,c.frames_added=!0}},animcompleted:function(a,c){var d=a.data(),e=d.videotype,f=d.autoplay,g=d.autoplayonlyfirsttime;void 0!=e&&"none"!=e&&(1==f||"true"==f||"on"==f||"1sttime"==f||g?(("carousel"!==c.sliderType||"carousel"===c.sliderType&&"on"===c.carousel.showLayersAllTime&&a.closest("li").hasClass("active-revslide")||"carousel"===c.sliderType&&"on"!==c.carousel.showLayersAllTime&&a.closest("li").hasClass("active-revslide"))&&b.playVideo(a,c),b.toggleState(a.data("videotoggledby")),(g||"1sttime"==f)&&(d.autoplayonlyfirsttime=!1,d.autoplay="off")):("no1sttime"==f&&(d.datasetautoplay="on"),b.unToggleState(a.data("videotoggledby"))))},handleStaticLayers:function(a,b){var c=parseInt(a.data("startslide"),0),d=parseInt(a.data("endslide"),0);c<0&&(c=0),d<0&&(d=b.realslideamount),0===c&&d===b.realslideamount-1&&(d=b.realslideamount+1),a.data("startslide",c),a.data("endslide",d)},animateTheCaptions:function(a){if("stop"===b.compare_version(e).check)return!1;var c=a.opt,d=a.slide,f=a.recall,g=a.maintimeline,h=a.preset,i=a.startslideanimat,j="carousel"===c.sliderType?0:c.width/2-c.gridwidth[c.curWinRange]*c.bw/2,k=0,l=d.data("index");if(c.layers=c.layers||new Object,c.layers[l]=c.layers[l]||d.find(".tp-caption"),c.layers.static=c.layers.static||c.c.find(".tp-static-layers").find(".tp-caption"),void 0===c.timelines&&b.createTimelineStructure(c),c.conh=c.c.height(),c.conw=c.c.width(),c.ulw=c.ul.width(),c.ulh=c.ul.height(),c.debugMode){d.addClass("indebugmode"),d.find(".helpgrid").remove(),c.c.find(".hglayerinfo").remove(),d.append('<div class="helpgrid" style="width:'+c.gridwidth[c.curWinRange]*c.bw+"px;height:"+c.gridheight[c.curWinRange]*c.bw+'px;"></div>');var m=d.find(".helpgrid");m.append('<div class="hginfo">Zoom:'+Math.round(100*c.bw)+"% &nbsp;&nbsp;&nbsp; Device Level:"+c.curWinRange+"&nbsp;&nbsp;&nbsp; Grid Preset:"+c.gridwidth[c.curWinRange]+"x"+c.gridheight[c.curWinRange]+"</div>"),c.c.append('<div class="hglayerinfo"></div>'),m.append('<div class="tlhg"></div>')}void 0!==l&&c.layers[l]&&jQuery.each(c.layers[l],function(a,d){var e=jQuery(this);b.updateMarkup(this,c),b.prepareSingleCaption({caption:e,opt:c,offsetx:j,offsety:k,index:a,recall:f,preset:h}),h&&0!==i||b.buildFullTimeLine({caption:e,opt:c,offsetx:j,offsety:k,index:a,recall:f,preset:h,regenerate:0===i}),f&&"carousel"===c.sliderType&&"on"===c.carousel.showLayersAllTime&&b.animcompleted(e,c)}),c.layers.static&&jQuery.each(c.layers.static,function(a,d){var e=jQuery(this),g=e.data();!0!==g.hoveredstatus&&!0!==g.inhoveroutanimation?(b.updateMarkup(this,c),b.prepareSingleCaption({caption:e,opt:c,offsetx:j,offsety:k,index:a,recall:f,preset:h}),h&&0!==i||!0===g.veryfirstststic||(b.buildFullTimeLine({caption:e,opt:c,offsetx:j,offsety:k,index:a,recall:f,preset:h,regenerate:0===i}),g.veryfirstststic=!0),f&&"carousel"===c.sliderType&&"on"===c.carousel.showLayersAllTime&&b.animcompleted(e,c)):b.prepareSingleCaption({caption:e,opt:c,offsetx:j,offsety:k,index:a,recall:f,preset:h})});var n=-1===c.nextSlide||void 0===c.nextSlide?0:c.nextSlide;void 0!==c.rowzones&&(n=n>c.rowzones.length?c.rowzones.length:n),void 0!=c.rowzones&&c.rowzones.length>0&&void 0!=c.rowzones[n]&&n>=0&&n<=c.rowzones.length&&c.rowzones[n].length>0&&b.setSize(c),h||void 0!==i&&(void 0!==l&&jQuery.each(c.timelines[l].layers,function(a,d){var e=d.layer.data();"none"!==d.wrapper&&void 0!==d.wrapper||("keep"==d.triggerstate&&"on"===e.triggerstate?b.playAnimationFrame({caption:d.layer,opt:c,frame:"frame_0",triggerdirection:"in",triggerframein:"frame_0",triggerframeout:"frame_999"}):d.timeline.restart())}),c.timelines.staticlayers&&jQuery.each(c.timelines.staticlayers.layers,function(a,d){var e=d.layer.data(),f=n>=d.firstslide&&n<=d.lastslide,g=n<d.firstslide||n>d.lastslide,h=d.timeline.getLabelTime("slide_"+d.firstslide),i=d.timeline.getLabelTime("slide_"+d.lastslide),j=e.static_layer_timeline_time,k="in"===e.animdirection||"out"!==e.animdirection&&void 0,l="bytrigger"===e.frames[0].delay,o=(e.frames[e.frames.length-1].delay,e.triggered_startstatus),p=e.lasttriggerstate;!0!==e.hoveredstatus&&1!=e.inhoveroutanimation&&(void 0!==j&&k&&("keep"==p?(b.playAnimationFrame({caption:d.layer,opt:c,frame:"frame_0",triggerdirection:"in",triggerframein:"frame_0",triggerframeout:"frame_999"}),e.triggeredtimeline.time(j)):!0!==e.hoveredstatus&&d.timeline.time(j)),"reset"===p&&"hidden"===o&&(d.timeline.time(0),e.animdirection="out"),f?k?n===d.lastslide&&(d.timeline.play(i),e.animdirection="in"):(l||"in"===e.animdirection||d.timeline.play(h),("visible"==o&&"keep"!==p||"keep"===p&&!0===k||"visible"==o&&void 0===k)&&(d.timeline.play(h+.01),e.animdirection="in")):g&&k&&d.timeline.play("frame_999"))})),void 0!=g&&setTimeout(function(){g.resume()},30)},prepareSingleCaption:function(a){var c=a.caption,d=c.data(),e=a.opt,f=a.recall,g=a.recall,i=(a.preset,jQuery("body").hasClass("rtl"));if(d._pw=void 0===d._pw?c.closest(".tp-parallax-wrap"):d._pw,d._lw=void 0===d._lw?c.closest(".tp-loop-wrap"):d._lw,d._mw=void 0===d._mw?c.closest(".tp-mask-wrap"):d._mw,d._responsive=d.responsive||"on",d._respoffset=d.responsive_offset||"on",d._ba=d.basealign||"grid",d._gw="grid"===d._ba?e.width:e.ulw,d._gh="grid"===d._ba?e.height:e.ulh,d._lig=void 0===d._lig?c.hasClass("rev_layer_in_group")?c.closest(".rev_group"):c.hasClass("rev_layer_in_column")?c.closest(".rev_column_inner"):c.hasClass("rev_column_inner")?c.closest(".rev_row"):"none":d._lig,d._column=void 0===d._column?c.hasClass("rev_column_inner")?c.closest(".rev_column"):"none":d._column,d._row=void 0===d._row?c.hasClass("rev_column_inner")?c.closest(".rev_row"):"none":d._row,d._ingroup=void 0===d._ingroup?!(c.hasClass("rev_group")||!c.closest(".rev_group")):d._ingroup,d._isgroup=void 0===d._isgroup?!!c.hasClass("rev_group"):d._isgroup,d._nctype=d.type||"none",d._cbgc_auto=void 0===d._cbgc_auto?"column"===d._nctype&&d._pw.find(".rev_column_bg_auto_sized"):d._cbgc_auto,d._cbgc_man=void 0===d._cbgc_man?"column"===d._nctype&&d._pw.find(".rev_column_bg_man_sized"):d._cbgc_man,d._slideid=d._slideid||c.closest(".tp-revslider-slidesli").data("index"),d._id=void 0===d._id?c.data("id")||c.attr("id"):d._id,d._slidelink=void 0===d._slidelink?void 0!==c.hasClass("slidelink")&&c.hasClass("slidelink"):d._slidelink,void 0===d._li&&(c.hasClass("tp-static-layer")?(d._isstatic=!0,d._li=c.closest(".tp-static-layers"),d._slideid="staticlayers"):d._li=c.closest(".tp-revslider-slidesli")),d._row=void 0===d._row?"column"===d._nctype&&d._pw.closest(".rev_row"):d._row,void 0===d._togglelisteners&&c.find(".rs-toggled-content")?(d._togglelisteners=!0,void 0===d.actions&&c.click(function(){b.swaptoggleState(c)})):d._togglelisteners=!1,"fullscreen"==e.sliderLayout&&(a.offsety=d._gh/2-e.gridheight[e.curWinRange]*e.bh/2),("on"==e.autoHeight||void 0!=e.minHeight&&e.minHeight>0)&&(a.offsety=e.conh/2-e.gridheight[e.curWinRange]*e.bh/2),a.offsety<0&&(a.offsety=0),e.debugMode){c.closest("li").find(".helpgrid").css({top:a.offsety+"px",left:a.offsetx+"px"});var k=e.c.find(".hglayerinfo");c.on("hover, mouseenter",function(){var a="";c.data()&&jQuery.each(c.data(),function(b,c){"object"!=typeof c&&(a=a+'<span style="white-space:nowrap"><span style="color:#27ae60">'+b+":</span>"+c+"</span>&nbsp; &nbsp; ")}),k.html(a)})}if("off"===(void 0===d.visibility?"oon":v(d.visibility,e)[e.forcedWinRange]||v(d.visibility,e)||"ooon")||d._gw<e.hideCaptionAtLimit&&"on"==d.captionhidden||d._gw<e.hideAllCaptionAtLimit?d._pw.addClass("tp-hidden-caption"):d._pw.removeClass("tp-hidden-caption"),d.layertype="html",a.offsetx<0&&(a.offsetx=0),void 0!=d.thumbimage&&void 0==d.videoposter&&(d.videoposter=d.thumbimage),c.find("img").length>0){var n=c.find("img");d.layertype="image",0==n.width()&&n.css({width:"auto"}),0==n.height()&&n.css({height:"auto"}),void 0==n.data("ww")&&n.width()>0&&n.data("ww",n.width()),void 0==n.data("hh")&&n.height()>0&&n.data("hh",n.height());var o=n.data("ww"),p=n.data("hh"),q="slide"==d._ba?e.ulw:e.gridwidth[e.curWinRange],r="slide"==d._ba?e.ulh:e.gridheight[e.curWinRange];o=v(n.data("ww"),e)[e.curWinRange]||v(n.data("ww"),e)||"auto",p=v(n.data("hh"),e)[e.curWinRange]||v(n.data("hh"),e)||"auto";var s="full"===o||"full-proportional"===o,t="full"===p||"full-proportional"===p;if("full-proportional"===o){var u=n.data("owidth"),x=n.data("oheight");u/q<x/r?(o=q,p=x*(q/u)):(p=r,o=u*(r/x))}else o=s?q:!jQuery.isNumeric(o)&&o.indexOf("%")>0?o:parseFloat(o),p=t?r:!jQuery.isNumeric(p)&&p.indexOf("%")>0?p:parseFloat(p);o=void 0===o?0:o,p=void 0===p?0:p,"off"!==d._responsive?("grid"!=d._ba&&s?jQuery.isNumeric(o)?n.css({width:o+"px"}):n.css({width:o}):jQuery.isNumeric(o)?n.css({width:o*e.bw+"px"}):n.css({width:o}),"grid"!=d._ba&&t?jQuery.isNumeric(p)?n.css({height:p+"px"}):n.css({height:p}):jQuery.isNumeric(p)?n.css({height:p*e.bh+"px"}):n.css({height:p})):n.css({width:o,height:p}),d._ingroup&&"row"!==d._nctype&&(void 0!==o&&!jQuery.isNumeric(o)&&"string"===jQuery.type(o)&&o.indexOf("%")>0&&punchgs.TweenLite.set([d._lw,d._pw,d._mw],{minWidth:o}),void 0!==p&&!jQuery.isNumeric(p)&&"string"===jQuery.type(p)&&p.indexOf("%")>0&&punchgs.TweenLite.set([d._lw,d._pw,d._mw],{minHeight:p}))}if("slide"===d._ba)a.offsetx=0,a.offsety=0;else if(d._isstatic&&void 0!==e.carousel&&void 0!==e.carousel.horizontal_align&&"carousel"===e.sliderType){switch(e.carousel.horizontal_align){case"center":a.offsetx=0+(e.ulw-e.gridwidth[e.curWinRange]*e.bw)/2;break;case"left":break;case"right":a.offsetx=e.ulw-e.gridwidth[e.curWinRange]*e.bw}a.offsetx=a.offsetx<0?0:a.offsetx}var A="html5"==d.audio?"audio":"video";if(c.hasClass("tp-videolayer")||c.hasClass("tp-audiolayer")||c.find("iframe").length>0||c.find(A).length>0){if(d.layertype="video",b.manageVideoLayer&&b.manageVideoLayer(c,e,f,g),!f&&!g){d.videotype;b.resetVideo&&b.resetVideo(c,e,a.preset)}var D=d.aspectratio;void 0!=D&&D.split(":").length>1&&b.prepareCoveredVideo(D,e,c);var n=c.find("iframe")?c.find("iframe"):n=c.find(A),E=!c.find("iframe"),F=c.hasClass("coverscreenvideo");n.css({display:"block"}),void 0==c.data("videowidth")&&(c.data("videowidth",n.width()),c.data("videoheight",n.height()));var o=v(c.data("videowidth"),e)[e.curWinRange]||v(c.data("videowidth"),e)||"auto",p=v(c.data("videoheight"),e)[e.curWinRange]||v(c.data("videoheight"),e)||"auto";o="auto"===o||!jQuery.isNumeric(o)&&o.indexOf("%")>0?"auto"===o?"auto":"grid"===d._ba?e.gridwidth[e.curWinRange]*e.bw:d._gw:parseFloat(o)*e.bw+"px",p="auto"===p||!jQuery.isNumeric(p)&&p.indexOf("%")>0?"auto"===p?"auto":"grid"===d._ba?e.gridheight[e.curWinRange]*e.bw:d._gh:parseFloat(p)*e.bh+"px",d.cssobj=void 0===d.cssobj?y(c,0):d.cssobj;var G=z(d.cssobj,e);if("auto"==G.lineHeight&&(G.lineHeight=G.fontSize+4),c.hasClass("fullscreenvideo")||F){a.offsetx=0,a.offsety=0,c.data("x",0),c.data("y",0);var H=d._gh;"on"==e.autoHeight&&(H=e.conh),c.css({width:d._gw,height:H})}else punchgs.TweenLite.set(c,{paddingTop:Math.round(G.paddingTop*e.bh)+"px",paddingBottom:Math.round(G.paddingBottom*e.bh)+"px",paddingLeft:Math.round(G.paddingLeft*e.bw)+"px",paddingRight:Math.round(G.paddingRight*e.bw)+"px",marginTop:G.marginTop*e.bh+"px",marginBottom:G.marginBottom*e.bh+"px",marginLeft:G.marginLeft*e.bw+"px",marginRight:G.marginRight*e.bw+"px",borderTopWidth:Math.round(G.borderTopWidth*e.bh)+"px",borderBottomWidth:Math.round(G.borderBottomWidth*e.bh)+"px",borderLeftWidth:Math.round(G.borderLeftWidth*e.bw)+"px",borderRightWidth:Math.round(G.borderRightWidth*e.bw)+"px",width:o,height:p});(0==E&&!F||1!=d.forcecover&&!c.hasClass("fullscreenvideo")&&!F)&&(n.width(o),n.height(p)),d._ingroup&&null!==d.videowidth&&void 0!==d.videowidth&&!jQuery.isNumeric(d.videowidth)&&d.videowidth.indexOf("%")>0&&punchgs.TweenLite.set([d._lw,d._pw,d._mw],{minWidth:d.videowidth})}B(c,e,0,d._responsive),c.hasClass("tp-resizeme")&&c.find("*").each(function(){B(jQuery(this),e,"rekursive",d._responsive)});var I=c.outerHeight(),J=c.css("backgroundColor");w(c,".frontcorner","left","borderRight","borderTopColor",I,J),w(c,".frontcornertop","left","borderRight","borderBottomColor",I,J),w(c,".backcorner","right","borderLeft","borderBottomColor",I,J),w(c,".backcornertop","right","borderLeft","borderTopColor",I,J),"on"==e.fullScreenAlignForce&&(a.offsetx=0,a.offsety=0),"block"===d._sfx&&void 0===d._bmask&&(d._bmask=jQuery('<div class="tp-blockmask"></div>'),d._mw.append(d._bmask)),d.arrobj=new Object,d.arrobj.voa=v(d.voffset,e)[e.curWinRange]||v(d.voffset,e)[0],d.arrobj.hoa=v(d.hoffset,e)[e.curWinRange]||v(d.hoffset,e)[0],d.arrobj.elx=v(d.x,e)[e.curWinRange]||v(d.x,e)[0],d.arrobj.ely=v(d.y,e)[e.curWinRange]||v(d.y,e)[0];var K=0==d.arrobj.voa.length?0:d.arrobj.voa,L=0==d.arrobj.hoa.length?0:d.arrobj.hoa,M=0==d.arrobj.elx.length?0:d.arrobj.elx,N=0==d.arrobj.ely.length?0:d.arrobj.ely;d.eow=c.outerWidth(!0),d.eoh=c.outerHeight(!0),0==d.eow&&0==d.eoh&&(d.eow=e.ulw,d.eoh=e.ulh);var O="off"!==d._respoffset?parseInt(K,0)*e.bw:parseInt(K,0),P="off"!==d._respoffset?parseInt(L,0)*e.bw:parseInt(L,0),Q="grid"===d._ba?e.gridwidth[e.curWinRange]*e.bw:d._gw,R="grid"===d._ba?e.gridheight[e.curWinRange]*e.bw:d._gh;"on"==e.fullScreenAlignForce&&(Q=e.ulw,R=e.ulh),"none"!==d._lig&&void 0!=d._lig&&(Q=d._lig.width(),R=d._lig.height(),a.offsetx=0,a.offsety=0),M="center"===M||"middle"===M?Q/2-d.eow/2+P:"left"===M?P:"right"===M?Q-d.eow-P:"off"!==d._respoffset?M*e.bw:M,N="center"==N||"middle"==N?R/2-d.eoh/2+O:"top"==N?O:"bottom"==N?R-d.eoh-O:"off"!==d._respoffset?N*e.bw:N,i&&!d._slidelink&&(M+=d.eow),d._slidelink&&(M=0),d.calcx=parseInt(M,0)+a.offsetx,d.calcy=parseInt(N,0)+a.offsety;var S=c.css("z-Index");if("row"!==d._nctype&&"column"!==d._nctype)punchgs.TweenLite.set(d._pw,{zIndex:S,top:d.calcy,left:d.calcx,overwrite:"auto"});else if("row"!==d._nctype)punchgs.TweenLite.set(d._pw,{zIndex:S,width:d.columnwidth,top:0,left:0,overwrite:"auto"});else if("row"===d._nctype){var T="grid"===d._ba?Q+"px":"100%";punchgs.TweenLite.set(d._pw,{zIndex:S,width:T,top:0,left:a.offsetx,overwrite:"auto"})}if(void 0!==d.blendmode&&punchgs.TweenLite.set(d._pw,{mixBlendMode:d.blendmode}),"row"===d._nctype&&(d.columnbreak<=e.curWinRange?c.addClass("rev_break_columns"):c.removeClass("rev_break_columns")),"on"==d.loopanimation&&punchgs.TweenLite.set(d._lw,{minWidth:d.eow,minHeight:d.eoh}),"column"===d._nctype){var U=void 0!==c[0]._gsTransform?c[0]._gsTransform.y:0,V=parseInt(d._column[0].style.paddingTop,0);punchgs.TweenLite.set(c,{y:0}),punchgs.TweenLite.set(d._cbgc_man,{y:parseInt(V+d._column.offset().top-c.offset().top,0)}),punchgs.TweenLite.set(c,{y:U})}d._ingroup&&"row"!==d._nctype&&(void 0!==d._groupw&&!jQuery.isNumeric(d._groupw)&&d._groupw.indexOf("%")>0&&punchgs.TweenLite.set([d._lw,d._pw,d._mw],{minWidth:d._groupw}),void 0!==d._grouph&&!jQuery.isNumeric(d._grouph)&&d._grouph.indexOf("%")>0&&punchgs.TweenLite.set([d._lw,d._pw,d._mw],{minHeight:d._grouph}))},createTimelineStructure:function(a){function b(a,b,c,d){var f,e=new punchgs.TimelineLite({paused:!0});c=c||new Object,c[a.attr("id")]=c[a.attr("id")]||new Object,"staticlayers"===d&&(c[a.attr("id")].firstslide=a.data("startslide"),c[a.attr("id")].lastslide=a.data("endslide")),a.data("slideid",d),c[a.attr("id")].defclasses=f=a[0].className,c[a.attr("id")].wrapper=f.indexOf("rev_layer_in_column")>=0?a.closest(".rev_column_inner"):f.indexOf("rev_column_inner")>=0?a.closest(".rev_row"):f.indexOf("rev_layer_in_group")>=0?a.closest(".rev_group"):"none",c[a.attr("id")].timeline=e,c[a.attr("id")].layer=a,c[a.attr("id")].triggerstate=a.data("lasttriggerstate"),c[a.attr("id")].dchildren=f.indexOf("rev_row")>=0?a[0].getElementsByClassName("rev_column_inner"):f.indexOf("rev_column_inner")>=0?a[0].getElementsByClassName("tp-caption"):f.indexOf("rev_group")>=0?a[0].getElementsByClassName("rev_layer_in_group"):"none",a.data("timeline",e)}a.timelines=a.timelines||new Object,a.c.find(".tp-revslider-slidesli, .tp-static-layers").each(function(){var c=jQuery(this),d=c.data("index");a.timelines[d]=a.timelines[d]||{},a.timelines[d].layers=a.timelines[d].layers||new Object,c.find(".tp-caption").each(function(c){b(jQuery(this),a,a.timelines[d].layers,d)})})},buildFullTimeLine:function(a){var g,h,c=a.caption,d=c.data(),e=a.opt,f={},j=q();if(g=e.timelines[d._slideid].layers[d._id],!g.generated||!0===a.regenerate){if(h=g.timeline,g.generated=!0,void 0!==d.current_timeline&&!0!==a.regenerate?(d.current_timeline_pause=d.current_timeline.paused(),d.current_timeline_time=d.current_timeline.time(),d.current_is_nc_timeline=h===d.current_timeline,d.static_layer_timeline_time=d.current_timeline_time):(d.static_layer_timeline_time=d.current_timeline_time,d.current_timeline_time=0,d.current_timeline&&d.current_timeline.clear()),h.clear(),f.svg=void 0!=d.svg_src&&c.find("svg"),f.svg&&(d.idlesvg=o(d.svg_idle,n()),punchgs.TweenLite.set(f.svg,d.idlesvg.anim)),-1!==d.hoverframeindex&&void 0!==d.hoverframeindex&&!c.hasClass("rs-hover-ready")){if(c.addClass("rs-hover-ready"),d.hovertimelines={},d.hoveranim=t(j,d.frames[d.hoverframeindex].to),d.hoveranim=x(d.hoveranim,d.frames[d.hoverframeindex].style),f.svg){var l=o(d.svg_hover,n());void 0!=d.hoveranim.anim.color&&(l.anim.fill=d.hoveranim.anim.color,d.idlesvg.anim.css.fill=f.svg.css("fill")),d.hoversvg=l}c.hover(function(a){var b={caption:jQuery(a.currentTarget),opt:e,firstframe:"frame_0",lastframe:"frame_999"},d=(i(b),b.caption),g=d.data(),h=g.frames[g.hoverframeindex];g.forcehover=h.force,g.hovertimelines.item=punchgs.TweenLite.to(d,h.speed/1e3,g.hoveranim.anim),(g.hoverzIndex||g.hoveranim.anim&&g.hoveranim.anim.zIndex)&&(g.basiczindex=void 0===g.basiczindex?g.cssobj.zIndex:g.basiczindex,g.hoverzIndex=void 0===g.hoverzIndex?g.hoveranim.anim.zIndex:g.hoverzIndex,g.inhoverinanimation=!0,0===h.speed&&(g.inhoverinanimation=!1),g.hovertimelines.pwhoveranim=punchgs.TweenLite.to(g._pw,h.speed/1e3,{overwrite:"auto",zIndex:g.hoverzIndex}),g.hovertimelines.pwhoveranim.eventCallback("onComplete",function(a){a.inhoverinanimation=!1},[g])),f.svg&&(g.hovertimelines.svghoveranim=punchgs.TweenLite.to([f.svg,f.svg.find("path")],h.speed/1e3,g.hoversvg.anim)),g.hoveredstatus=!0},function(a){var b={caption:jQuery(a.currentTarget),opt:e,firstframe:"frame_0",lastframe:"frame_999"},d=(i(b),b.caption),g=d.data(),h=g.frames[g.hoverframeindex];g.hoveredstatus=!1,g.inhoveroutanimation=!0,g.hovertimelines.item.pause(),g.hovertimelines.item=punchgs.TweenLite.to(d,h.speed/1e3,jQuery.extend(!0,{},g._gsTransformTo)),0==h.speed&&(g.inhoveroutanimation=!1),g.hovertimelines.item.eventCallback("onComplete",function(a){a.inhoveroutanimation=!1},[g]),void 0!==g.hovertimelines.pwhoveranim&&(g.hovertimelines.pwhoveranim=punchgs.TweenLite.to(g._pw,h.speed/1e3,{overwrite:"auto",zIndex:g.basiczindex})),f.svg&&punchgs.TweenLite.to([f.svg,f.svg.find("path")],h.speed/1e3,g.idlesvg.anim)})}for(var m=0;m<d.frames.length;m++)if(m!==d.hoverframeindex){var p=m===d.inframeindex?"frame_0":m===d.outframeindex||"frame_999"===d.frames[m].frame?"frame_999":"frame_"+m;d.frames[m].framename=p,g[p]={},g[p].timeline=new punchgs.TimelineLite({align:"normal"});var r=d.frames[m].delay,u=(d.triggered_startstatus,void 0!==r?jQuery.inArray(r,["slideenter","bytrigger","wait"])>=0?r:parseInt(r,0)/1e3:"wait");void 0!==g.firstslide&&"frame_0"===p&&(h.addLabel("slide_"+g.firstslide+"_pause",0),h.addPause("slide_"+g.firstslide+"_pause"),h.addLabel("slide_"+g.firstslide,"+=0.005")),void 0!==g.lastslide&&"frame_999"===p&&(h.addLabel("slide_"+g.lastslide+"_pause","+=0.01"),h.addPause("slide_"+g.lastslide+"_pause"),h.addLabel("slide_"+g.lastslide,"+=0.005")),jQuery.isNumeric(u)?h.addLabel(p,"+="+u):(h.addLabel("pause_"+m,"+=0.01"),h.addPause("pause_"+m),h.addLabel(p,"+=0.01")),h=b.createFrameOnTimeline({caption:a.caption,timeline:h,label:p,frameindex:m,opt:e})}a.regenerate||(d.current_is_nc_timeline&&(d.current_timeline=h),d.current_timeline_pause?h.pause(d.current_timeline_time):h.time(d.current_timeline_time))}},createFrameOnTimeline:function(a){var b=a.caption,c=b.data(),d=a.label,e=a.timeline,i=a.frameindex,j=a.opt,n=b,o={},q=j.timelines[c._slideid].layers[c._id],r=c.frames.length-1,v=c.frames[i].split,w=c.frames[i].split_direction,x=c.frames[i].sfx_effect,y=!1;if(w=void 0===w?"forward":w,-1!==c.hoverframeindex&&c.hoverframeindex==r&&(r-=1),o.content=new punchgs.TimelineLite({align:"normal"}),o.mask=new punchgs.TimelineLite({align:"normal"}),void 0===e.vars.id&&(e.vars.id=Math.round(1e5*Math.random())),"column"===c._nctype&&(e.add(punchgs.TweenLite.set(c._cbgc_man,{visibility:"visible"}),d),e.add(punchgs.TweenLite.set(c._cbgc_auto,{visibility:"hidden"}),d)),c.splittext&&0===i){void 0!==c.mySplitText&&c.mySplitText.revert();var z=b.find("a").length>0?b.find("a"):b;c.mySplitText=new punchgs.SplitText(z,{type:"chars,words,lines",charsClass:"tp-splitted tp-charsplit",wordsClass:"tp-splitted tp-wordsplit",linesClass:"tp-splitted tp-linesplit"}),b.addClass("splitted")}void 0!==c.mySplitText&&v&&v.match(/chars|words|lines/g)&&(n=c.mySplitText[v],y=!0);var D,E,A=i!==c.outframeindex?t(m(),c.frames[i].to,void 0,y,n.length-1):void 0!==c.frames[i].to&&null===c.frames[i].to.match(/auto:auto/g)?t(p(),c.frames[i].to,1==j.sdir,y,n.length-1):t(p(),c.frames[c.inframeindex].from,0==j.sdir,y,n.length-1),B=void 0!==c.frames[i].from?t(A,c.frames[c.inframeindex].from,1==j.sdir,y,n.length-1):void 0,C=c.frames[i].splitdelay;if(0!==i||a.fromcurrentstate?E=u(c.frames[i].mask):D=u(c.frames[i].mask),A.anim.ease=void 0===c.frames[i].ease?punchgs.Power1.easeInOut:c.frames[i].ease,void 0!==B&&(B.anim.ease=void 0===c.frames[i].ease?punchgs.Power1.easeInOut:c.frames[i].ease,B.speed=void 0===c.frames[i].speed?B.speed:c.frames[i].speed,B.anim.x=B.anim.x*j.bw||s(B.anim.x,j,c.eow,c.eoh,c.calcy,c.calcx,"horizontal"),B.anim.y=B.anim.y*j.bw||s(B.anim.y,j,c.eow,c.eoh,c.calcy,c.calcx,"vertical")),void 0!==A&&(A.anim.ease=void 0===c.frames[i].ease?punchgs.Power1.easeInOut:c.frames[i].ease,A.speed=void 0===c.frames[i].speed?A.speed:c.frames[i].speed,A.anim.x=A.anim.x*j.bw||s(A.anim.x,j,c.eow,c.eoh,c.calcy,c.calcx,"horizontal"),A.anim.y=A.anim.y*j.bw||s(A.anim.y,j,c.eow,c.eoh,c.calcy,c.calcx,"vertical")),b.data("iframes")&&e.add(punchgs.TweenLite.set(b.find("iframe"),{autoAlpha:1}),d+"+=0.001"),i===c.outframeindex&&(c.frames[i].to&&c.frames[i].to.match(/auto:auto/g),A.speed=void 0===c.frames[i].speed||"inherit"===c.frames[i].speed?c.frames[c.inframeindex].speed:c.frames[i].speed,A.anim.ease=void 0===c.frames[i].ease||"inherit"===c.frames[i].ease?c.frames[c.inframeindex].ease:c.frames[i].ease,A.anim.overwrite="auto"),0!==i||a.fromcurrentstate)0===i&&a.fromcurrentstate&&(A.speed=B.speed);else{if(n!=b){var F=jQuery.extend({},A.anim,!0);e.add(punchgs.TweenLite.set(b,A.anim),d),A=m(),A.ease=F.ease,void 0!==F.filter&&(A.anim.filter=F.filter),void 0!==F["-webkit-filter"]&&(A.anim["-webkit-filter"]=F["-webkit-filter"])}B.anim.visibility="hidden",B.anim.immediateRender=!0,A.anim.visibility="visible"}a.fromcurrentstate&&(A.anim.immediateRender=!0);var G=-1;if(0===i&&!a.fromcurrentstate&&void 0!==c._bmask&&void 0!==x&&x.indexOf("block")>=0||i===c.outframeindex&&!a.fromcurrentstate&&void 0!==c._bmask&&void 0!==x&&x.indexOf("block")>=0){var H=0===i?B.speed/1e3/2:A.speed/1e3/2,I=[{scaleY:1,scaleX:0,transformOrigin:"0% 50%"},{scaleY:1,scaleX:1,ease:A.anim.ease}],J={scaleY:1,scaleX:0,transformOrigin:"100% 50%",ease:A.anim.ease};switch(G=void 0===C?H:C+H,x){case"blocktoleft":case"blockfromright":I[0].transformOrigin="100% 50%",J.transformOrigin="0% 50%";break;case"blockfromtop":case"blocktobottom":I=[{scaleX:1,scaleY:0,transformOrigin:"50% 0%"},{scaleX:1,scaleY:1,ease:A.anim.ease}],J={scaleX:1,scaleY:0,transformOrigin:"50% 100%",ease:A.anim.ease};break;case"blocktotop":case"blockfrombottom":I=[{scaleX:1,scaleY:0,transformOrigin:"50% 100%"},{scaleX:1,scaleY:1,ease:A.anim.ease}],J={scaleX:1,scaleY:0,transformOrigin:"50% 0%",ease:A.anim.ease}}I[0].background=c.frames[i].sfxcolor,e.add(o.mask.fromTo(c._bmask,H,I[0],I[1],C),d),e.add(o.mask.to(c._bmask,H,J,G),d)}if(y)var K=k(n.length-1,w);if(0!==i||a.fromcurrentstate)if("block"===c._sfx_out&&i===c.outframeindex)e.add(o.content.staggerTo(n,.001,{autoAlpha:0,delay:G}),d),e.add(o.content.staggerTo(n,A.speed/1e3/2-.001,{x:0,delay:G}),d+"+=0.001");else if(y&&void 0!==K){var L={to:l(A.anim)};for(var M in n){var O=jQuery.extend({},A.anim);for(var P in L.to)O[P]=parseInt(L.to[P].values[L.to[P].index],0),L.to[P].index=L.to[P].index<L.to[P].len?L.to[P].index+1:0;void 0!==c.frames[i].color&&(O.color=c.frames[i].color),void 0!==c.frames[i].bgcolor&&(O.backgroundColor=c.frames[i].bgcolor),e.add(o.content.to(n[K[M]],A.speed/1e3,O,C*M),d)}}else void 0!==c.frames[i].color&&(A.anim.color=c.frames[i].color),void 0!==c.frames[i].bgcolor&&(A.anim.backgroundColor=c.frames[i].bgcolor),e.add(o.content.staggerTo(n,A.speed/1e3,A.anim,C),d);else if("block"===c._sfx_in)e.add(o.content.staggerFromTo(n,.05,{x:0,y:0,autoAlpha:0},{x:0,y:0,autoAlpha:1,delay:G}),d);else if(y&&void 0!==K){var L={from:l(B.anim),to:l(A.anim)};for(var M in n){var N=jQuery.extend({},B.anim),O=jQuery.extend({},A.anim);for(var P in L.from)N[P]=parseInt(L.from[P].values[L.from[P].index],0),L.from[P].index=L.from[P].index<L.from[P].len?L.from[P].index+1:0;O.ease=N.ease,void 0!==c.frames[i].color&&(N.color=c.frames[i].color,O.color=c.cssobj.styleProps.color),void 0!==c.frames[i].bgcolor&&(N.backgroundColor=c.frames[i].bgcolor,O.backgroundColor=c.cssobj.styleProps["background-color"]),e.add(o.content.fromTo(n[K[M]],B.speed/1e3,N,O,C*M),d)}}else void 0!==c.frames[i].color&&(B.anim.color=c.frames[i].color,A.anim.color=c.cssobj.styleProps.color),void 0!==c.frames[i].bgcolor&&(B.anim.backgroundColor=c.frames[i].bgcolor,A.anim.backgroundColor=c.cssobj.styleProps["background-color"]),e.add(o.content.staggerFromTo(n,B.speed/1e3,B.anim,A.anim,C),d);return void 0===E||!1===E||0===i&&a.ignorefirstframe||(E.anim.ease=void 0===E.anim.ease||"inherit"===E.anim.ease?c.frames[0].ease:E.anim.ease,E.anim.overflow="hidden",E.anim.x=E.anim.x*j.bw||s(E.anim.x,j,c.eow,c.eoh,c.calcy,c.calcx,"horizontal"),E.anim.y=E.anim.y*j.bw||s(E.anim.y,j,c.eow,c.eoh,c.calcy,c.calcx,"vertical")),0===i&&D&&!1!==D&&!a.fromcurrentstate||0===i&&a.ignorefirstframe?(E=new Object,E.anim=new Object,E.anim.overwrite="auto",E.anim.ease=A.anim.ease,E.anim.x=E.anim.y=0,D&&!1!==D&&(D.anim.x=D.anim.x*j.bw||s(D.anim.x,j,c.eow,c.eoh,c.calcy,c.calcx,"horizontal"),D.anim.y=D.anim.y*j.bw||s(D.anim.y,j,c.eow,c.eoh,c.calcy,c.calcx,"vertical"),D.anim.overflow="hidden")):0===i&&e.add(o.mask.set(c._mw,{overflow:"visible"}),d),void 0!==D&&void 0!==E&&!1!==D&&!1!==E?e.add(o.mask.fromTo(c._mw,B.speed/1e3,D.anim,E.anim,C),d):void 0!==E&&!1!==E&&e.add(o.mask.to(c._mw,A.speed/1e3,E.anim,C),d),e.addLabel(d+"_end"),c._gsTransformTo&&i===r&&c.hoveredstatus&&(c.hovertimelines.item=punchgs.TweenLite.to(b,0,c._gsTransformTo)),c._gsTransformTo=!1,o.content.eventCallback("onStart",f,[i,q,c._pw,c,e,A.anim,b,a.updateStaticTimeline,j]),o.content.eventCallback("onUpdate",g,[d,c._id,c._pw,c,e,i,jQuery.extend(!0,{},A.anim),a.updateStaticTimeline,b,j]),o.content.eventCallback("onComplete",h,[i,c.frames.length,r,c._pw,c,e,a.updateStaticTimeline,b,j]),e},endMoveCaption:function(a){a.firstframe="frame_0",a.lastframe="frame_999";var c=i(a),d=a.caption.data();if(void 0!==a.frame?c.timeline.play(a.frame):(!c.static||a.currentslide>=c.removeonslide||a.currentslide<c.showonslide)&&(c.outnow=new punchgs.TimelineLite,c.timeline.pause(),!0===d.visibleelement&&b.createFrameOnTimeline({caption:a.caption,timeline:c.outnow,label:"outnow",frameindex:a.caption.data("outframeindex"),opt:a.opt,fromcurrentstate:!0}).play()),a.checkchildrens&&c.timeline_obj&&c.timeline_obj.dchildren&&"none"!==c.timeline_obj.dchildren&&c.timeline_obj.dchildren.length>0)for(var e=0;e<c.timeline_obj.dchildren.length;e++)b.endMoveCaption({caption:jQuery(c.timeline_obj.dchildren[e]),opt:a.opt})},playAnimationFrame:function(a){a.firstframe=a.triggerframein,a.lastframe=a.triggerframeout;var e,c=i(a),d=a.caption.data(),f=0;for(var g in d.frames)d.frames[g].framename===a.frame&&(e=f),f++;void 0!==d.triggeredtimeline&&d.triggeredtimeline.pause(),d.triggeredtimeline=new punchgs.TimelineLite,c.timeline.pause();var h=!0===d.visibleelement;d.triggeredtimeline=b.createFrameOnTimeline({caption:a.caption,timeline:d.triggeredtimeline,label:"triggered",frameindex:e,updateStaticTimeline:!0,opt:a.opt,ignorefirstframe:!0,fromcurrentstate:h}).play()},removeTheCaptions:function(a,c){if("stop"===b.compare_version(e).check)return!1;var f=a.data("index"),g=new Array;c.layers[f]&&jQuery.each(c.layers[f],function(a,b){g.push(b)});var h=b.currentSlideIndex(c);g&&jQuery.each(g,function(a){var d=jQuery(this);"carousel"===c.sliderType&&"on"===c.carousel.showLayersAllTime?(clearTimeout(d.data("videoplaywait")),b.stopVideo&&b.stopVideo(d,c),b.removeMediaFromList&&b.removeMediaFromList(d,c),c.lastplayedvideos=[]):(E(d),clearTimeout(d.data("videoplaywait")),b.endMoveCaption({caption:d,opt:c,currentslide:h}),b.removeMediaFromList&&b.removeMediaFromList(d,c),c.lastplayedvideos=[])})}});var f=function(a,c,d,e,f,g,h,i,j){var k={};if(k.layer=h,k.eventtype=0===a?"enterstage":a===e.outframeindex?"leavestage":"framestarted",k.layertype=h.data("layertype"),e.active=!0,k.frame_index=a,k.layersettings=h.data(),j.c.trigger("revolution.layeraction",[k]),"on"==e.loopanimation&&D(e._lw,j.bw),"enterstage"===k.eventtype&&(e.animdirection="in",e.visibleelement=!0,b.toggleState(e.layertoggledby)),"none"!==c.dchildren&&void 0!==c.dchildren&&c.dchildren.length>0)if(0===a)for(var l=0;l<c.dchildren.length;l++)jQuery(c.dchildren[l]).data("timeline").play(0);else if(a===e.outframeindex)for(var l=0;l<c.dchildren.length;l++)b.endMoveCaption({caption:jQuery(c.dchildren[l]),opt:j,checkchildrens:!0});punchgs.TweenLite.set(d,{visibility:"visible"}),e.current_frame=a,e.current_timeline=f,e.current_timeline_time=f.time(),i&&(e.static_layer_timeline_time=e.current_timeline_time),e.last_frame_started=a},g=function(a,b,c,d,e,f,g,h,i,j){"column"===d._nctype&&C(i,j),punchgs.TweenLite.set(c,{visibility:"visible"}),d.current_frame=f,d.current_timeline=e,d.current_timeline_time=e.time(),h&&(d.static_layer_timeline_time=d.current_timeline_time),void 0!==d.hoveranim&&!1===d._gsTransformTo&&(d._gsTransformTo=g,d._gsTransformTo&&d._gsTransformTo.startAt&&delete d._gsTransformTo.startAt,void 0===d.cssobj.styleProps.css?d._gsTransformTo=jQuery.extend(!0,{},d.cssobj.styleProps,d._gsTransformTo):d._gsTransformTo=jQuery.extend(!0,{},d.cssobj.styleProps.css,d._gsTransformTo)),d.visibleelement=!0},h=function(a,c,d,e,f,g,h,i,j){var k={};k.layer=i,k.eventtype=0===a?"enteredstage":a===c-1||a===d?"leftstage":"frameended",k.layertype=i.data("layertype"),k.layersettings=i.data(),j.c.trigger("revolution.layeraction",[k]),"leftstage"!==k.eventtype&&b.animcompleted(i,j),"leftstage"===k.eventtype&&b.stopVideo&&b.stopVideo(i,j),"column"===f._nctype&&(punchgs.TweenLite.to(f._cbgc_man,.01,{visibility:"hidden"}),punchgs.TweenLite.to(f._cbgc_auto,.01,{visibility:"visible"})),"leftstage"===k.eventtype&&(f.active=!1,punchgs.TweenLite.set(e,{visibility:"hidden",overwrite:"auto"}),f.animdirection="out",f.visibleelement=!1,b.unToggleState(f.layertoggledby)),f.current_frame=a,f.current_timeline=g,f.current_timeline_time=g.time(),h&&(f.static_layer_timeline_time=f.current_timeline_time)},i=function(a){var b={};return a.firstframe=void 0===a.firstframe?"frame_0":a.firstframe,a.lastframe=void 0===a.lastframe?"frame_999":a.lastframe,b.id=a.caption.data("id")||a.caption.attr("id"),b.slideid=a.caption.data("slideid")||a.caption.closest(".tp-revslider-slidesli").data("index"),b.timeline_obj=a.opt.timelines[b.slideid].layers[b.id],b.timeline=b.timeline_obj.timeline,b.ffs=b.timeline.getLabelTime(a.firstframe),b.ffe=b.timeline.getLabelTime(a.firstframe+"_end"),b.lfs=b.timeline.getLabelTime(a.lastframe),b.lfe=b.timeline.getLabelTime(a.lastframe+"_end"),b.ct=b.timeline.time(),b.static=void 0!=b.timeline_obj.firstslide||void 0!=b.timeline_obj.lastslide,b.static&&(b.showonslide=b.timeline_obj.firstslide,b.removeonslide=b.timeline_obj.lastslide),b},j=function(a){for(var c,d,b=a.length;0!==b;)d=Math.floor(Math.random()*b),b-=1,c=a[b],a[b]=a[d],a[d]=c;return a},k=function(a,b){var c=new Array;switch(b){case"forward":case"random":for(var d=0;d<=a;d++)c.push(d);"random"===b&&(c=j(c));break;case"backward":for(var d=0;d<=a;d++)c.push(a-d);break;case"middletoedge":var e=Math.ceil(a/2),f=e-1,g=e+1;c.push(e);for(var d=0;d<e;d++)f>=0&&c.push(f),g<=a&&c.push(g),f--,g++;break;case"edgetomiddle":for(var f=a,g=0,d=0;d<=Math.floor(a/2);d++)c.push(f),g<f&&c.push(g),f--,g++}return c},l=function(a){var b={};for(var c in a)"string"==typeof a[c]&&a[c].indexOf("|")>=0&&(void 0===b[c]&&(b[c]={index:0}),b[c].values=a[c].replace("[","").replace("]","").split("|"),b[c].len=b[c].values.length-1);return b},m=function(a){return a=void 0===a?new Object:a,a.anim=void 0===a.anim?new Object:a.anim,a.anim.x=void 0===a.anim.x?0:a.anim.x,a.anim.y=void 0===a.anim.y?0:a.anim.y,a.anim.z=void 0===a.anim.z?0:a.anim.z,a.anim.rotationX=void 0===a.anim.rotationX?0:a.anim.rotationX,a.anim.rotationY=void 0===a.anim.rotationY?0:a.anim.rotationY,a.anim.rotationZ=void 0===a.anim.rotationZ?0:a.anim.rotationZ,a.anim.scaleX=void 0===a.anim.scaleX?1:a.anim.scaleX,a.anim.scaleY=void 0===a.anim.scaleY?1:a.anim.scaleY,a.anim.skewX=void 0===a.anim.skewX?0:a.anim.skewX,a.anim.skewY=void 0===a.anim.skewY?0:a.anim.skewY,a.anim.opacity=void 0===a.anim.opacity?1:a.anim.opacity,a.anim.transformOrigin=void 0===a.anim.transformOrigin?"50% 50%":a.anim.transformOrigin,a.anim.transformPerspective=void 0===a.anim.transformPerspective?600:a.anim.transformPerspective,a.anim.rotation=void 0===a.anim.rotation?0:a.anim.rotation,a.anim.force3D=void 0===a.anim.force3D?"auto":a.anim.force3D,a.anim.autoAlpha=void 0===a.anim.autoAlpha?1:a.anim.autoAlpha,a.anim.visibility=void 0===a.anim.visibility?"visible":a.anim.visibility,a.anim.overwrite=void 0===a.anim.overwrite?"auto":a.anim.overwrite,a.speed=void 0===a.speed?.3:a.speed,a.filter=void 0===a.filter?"blur(0px) grayscale(0%) brightness(100%)":a.filter,a["-webkit-filter"]=void 0===a["-webkit-filter"]?"blur(0px) grayscale(0%) brightness(100%)":a["-webkit-filter"],a},n=function(){var a=new Object;return a.anim=new Object,a.anim.stroke="none",a.anim.strokeWidth=0,a.anim.strokeDasharray="none",a.anim.strokeDashoffset="0",a},o=function(a,b){var c=a.split(";");return c&&jQuery.each(c,function(a,c){var d=c.split(":"),e=d[0],f=d[1];"sc"==e&&(b.anim.stroke=f),"sw"==e&&(b.anim.strokeWidth=f),"sda"==e&&(b.anim.strokeDasharray=f),"sdo"==e&&(b.anim.strokeDashoffset=f)}),b},p=function(){var a=new Object;return a.anim=new Object,a.anim.x=0,a.anim.y=0,a.anim.z=0,a},q=function(){var a=new Object;return a.anim=new Object,a.speed=.2,a},r=function(a,b,c,d,e){if(e=void 0===e?"":e,jQuery.isNumeric(parseFloat(a)))return parseFloat(a)+e;if(void 0===a||"inherit"===a)return b+"ext";if(a.split("{").length>1){var f=a.split(","),g=parseFloat(f[1].split("}")[0]);if(f=parseFloat(f[0].split("{")[1]),void 0!==c&&void 0!==d){parseInt(Math.random()*(g-f),0),parseInt(f,0);for(var h=0;h<d;h++)a=a+"|"+(parseInt(Math.random()*(g-f),0)+parseInt(f,0))+e;a+="]"}else a=Math.random()*(g-f)+f}return a},s=function(a,b,c,d,e,f,g){return!jQuery.isNumeric(a)&&a.match(/%]/g)?(a=a.split("[")[1].split("]")[0],"horizontal"==g?a=(c+2)*parseInt(a,0)/100:"vertical"==g&&(a=(d+2)*parseInt(a,0)/100)):(a="layer_left"===a?0-c:"layer_right"===a?c:a,a="layer_top"===a?0-d:"layer_bottom"===a?d:a,a="left"===a||"stage_left"===a?0-c-f:"right"===a||"stage_right"===a?b.conw-f:"center"===a||"stage_center"===a?b.conw/2-c/2-f:a,a="top"===a||"stage_top"===a?0-d-e:"bottom"===a||"stage_bottom"===a?b.conh-e:"middle"===a||"stage_middle"===a?b.conh/2-d/2-e:a),a},t=function(a,b,c,d,e){var f=new Object;if(f=jQuery.extend(!0,{},f,a),void 0===b)return f;var g=b.split(";"),h="";return g&&jQuery.each(g,function(a,b){var g=b.split(":"),i=g[0],j=g[1];c&&"none"!==c&&void 0!=j&&j.length>0&&j.match(/\(R\)/)&&(j=j.replace("(R)",""),j="right"===j?"left":"left"===j?"right":"top"===j?"bottom":"bottom"===j?"top":j,"["===j[0]&&"-"===j[1]?j=j.replace("[-","["):"["===j[0]&&"-"!==j[1]?j=j.replace("[","[-"):"-"===j[0]?j=j.replace("-",""):j[0].match(/[1-9]/)&&(j="-"+j)),void 0!=j&&(j=j.replace(/\(R\)/,""),"rotationX"!=i&&"rX"!=i||(f.anim.rotationX=r(j,f.anim.rotationX,d,e,"deg")),"rotationY"!=i&&"rY"!=i||(f.anim.rotationY=r(j,f.anim.rotationY,d,e,"deg")),"rotationZ"!=i&&"rZ"!=i||(f.anim.rotation=r(j,f.anim.rotationZ,d,e,"deg")),"scaleX"!=i&&"sX"!=i||(f.anim.scaleX=r(j,f.anim.scaleX,d,e)),"scaleY"!=i&&"sY"!=i||(f.anim.scaleY=r(j,f.anim.scaleY,d,e)),"opacity"!=i&&"o"!=i||(f.anim.opacity=r(j,f.anim.opacity,d,e)),"fb"==i&&(h=""===h?"blur("+parseInt(j,0)+"px)":h+" blur("+parseInt(j,0)+"px)"),"fg"==i&&(h=""===h?"grayscale("+parseInt(j,0)+"%)":h+" grayscale("+parseInt(j,0)+"%)"),"fbr"==i&&(h=""===h?"brightness("+parseInt(j,0)+"%)":h+" brightness("+parseInt(j,0)+"%)"),0===f.anim.opacity&&(f.anim.autoAlpha=0),f.anim.opacity=0==f.anim.opacity?1e-4:f.anim.opacity,"skewX"!=i&&"skX"!=i||(f.anim.skewX=r(j,f.anim.skewX,d,e)),"skewY"!=i&&"skY"!=i||(f.anim.skewY=r(j,f.anim.skewY,d,e)),"x"==i&&(f.anim.x=r(j,f.anim.x,d,e)),"y"==i&&(f.anim.y=r(j,f.anim.y,d,e)),"z"==i&&(f.anim.z=r(j,f.anim.z,d,e)),"transformOrigin"!=i&&"tO"!=i||(f.anim.transformOrigin=j.toString()),"transformPerspective"!=i&&"tP"!=i||(f.anim.transformPerspective=parseInt(j,0)),"speed"!=i&&"s"!=i||(f.speed=parseFloat(j)))}),""!==h&&(f.anim["-webkit-filter"]=h,f.anim.filter=h),f},u=function(a){if(void 0===a)return!1;var b=new Object;b.anim=new Object;var c=a.split(";");return c&&jQuery.each(c,function(a,c){c=c.split(":");var d=c[0],e=c[1];"x"==d&&(b.anim.x=e),"y"==d&&(b.anim.y=e),"s"==d&&(b.speed=parseFloat(e)),"e"!=d&&"ease"!=d||(b.anim.ease=e)}),b},v=function(a,b,c){if(void 0==a&&(a=0),!jQuery.isArray(a)&&"string"===jQuery.type(a)&&(a.split(",").length>1||a.split("[").length>1)){a=a.replace("[",""),a=a.replace("]","");var d=a.match(/'/g)?a.split("',"):a.split(",");a=new Array,d&&jQuery.each(d,function(b,c){c=c.replace("'",""),c=c.replace("'",""),a.push(c)})}else{var e=a;jQuery.isArray(a)||(a=new Array,a.push(e))}var e=a[a.length-1];if(a.length<b.rle)for(var f=1;f<=b.curWinRange;f++)a.push(e);return a},x=function(a,b){if(void 0===b)return a;b=b.replace("c:","color:"),b=b.replace("bg:","background-color:"),b=b.replace("bw:","border-width:"),b=b.replace("bc:","border-color:"),b=b.replace("br:","borderRadius:"),b=b.replace("bs:","border-style:"),b=b.replace("td:","text-decoration:"),b=b.replace("zi:","zIndex:");var c=b.split(";");return c&&jQuery.each(c,function(b,c){var d=c.split(":");d[0].length>0&&("background-color"===d[0]&&d[1].indexOf("gradient")>=0&&(d[0]="background"),a.anim[d[0]]=d[1])}),a},y=function(a,b){var e,c=new Object,d=!1;if("rekursive"==b&&(e=a.closest(".tp-caption"))&&a.css("fontSize")===e.css("fontSize")&&a.css("fontWeight")===e.css("fontWeight")&&a.css("lineHeight")===e.css("lineHeight")&&(d=!0),c.basealign=a.data("basealign")||"grid",c.fontSize=d?void 0===e.data("fontsize")?parseInt(e.css("fontSize"),0)||0:e.data("fontsize"):void 0===a.data("fontsize")?parseInt(a.css("fontSize"),0)||0:a.data("fontsize"),c.fontWeight=d?void 0===e.data("fontweight")?parseInt(e.css("fontWeight"),0)||0:e.data("fontweight"):void 0===a.data("fontweight")?parseInt(a.css("fontWeight"),0)||0:a.data("fontweight"),c.whiteSpace=d?void 0===e.data("whitespace")?e.css("whitespace")||"normal":e.data("whitespace"):void 0===a.data("whitespace")?a.css("whitespace")||"normal":a.data("whitespace"),c.textAlign=d?void 0===e.data("textalign")?e.css("textalign")||"inherit":e.data("textalign"):void 0===a.data("textalign")?a.css("textalign")||"inherit":a.data("textalign"),c.zIndex=d?void 0===e.data("zIndex")?e.css("zIndex")||"inherit":e.data("zIndex"):void 0===a.data("zIndex")?a.css("zIndex")||"inherit":a.data("zIndex"),-1!==jQuery.inArray(a.data("layertype"),["video","image","audio"])||a.is("img")?c.lineHeight=0:c.lineHeight=d?void 0===e.data("lineheight")?parseInt(e.css("lineHeight"),0)||0:e.data("lineheight"):void 0===a.data("lineheight")?parseInt(a.css("lineHeight"),0)||0:a.data("lineheight"),c.letterSpacing=d?void 0===e.data("letterspacing")?parseFloat(e.css("letterSpacing"),0)||0:e.data("letterspacing"):void 0===a.data("letterspacing")?parseFloat(a.css("letterSpacing"))||0:a.data("letterspacing"),c.paddingTop=void 0===a.data("paddingtop")?parseInt(a.css("paddingTop"),0)||0:a.data("paddingtop"),c.paddingBottom=void 0===a.data("paddingbottom")?parseInt(a.css("paddingBottom"),0)||0:a.data("paddingbottom"),c.paddingLeft=void 0===a.data("paddingleft")?parseInt(a.css("paddingLeft"),0)||0:a.data("paddingleft"),c.paddingRight=void 0===a.data("paddingright")?parseInt(a.css("paddingRight"),0)||0:a.data("paddingright"),c.marginTop=void 0===a.data("margintop")?parseInt(a.css("marginTop"),0)||0:a.data("margintop"),c.marginBottom=void 0===a.data("marginbottom")?parseInt(a.css("marginBottom"),0)||0:a.data("marginbottom"),c.marginLeft=void 0===a.data("marginleft")?parseInt(a.css("marginLeft"),0)||0:a.data("marginleft"),c.marginRight=void 0===a.data("marginright")?parseInt(a.css("marginRight"),0)||0:a.data("marginright"),c.borderTopWidth=void 0===a.data("bordertopwidth")?parseInt(a.css("borderTopWidth"),0)||0:a.data("bordertopwidth"),c.borderBottomWidth=void 0===a.data("borderbottomwidth")?parseInt(a.css("borderBottomWidth"),0)||0:a.data("borderbottomwidth"),c.borderLeftWidth=void 0===a.data("borderleftwidth")?parseInt(a.css("borderLeftWidth"),0)||0:a.data("borderleftwidth"),c.borderRightWidth=void 0===a.data("borderrightwidth")?parseInt(a.css("borderRightWidth"),0)||0:a.data("borderrightwidth"),"rekursive"!=b){if(c.color=void 0===a.data("color")?"nopredefinedcolor":a.data("color"),c.whiteSpace=d?void 0===e.data("whitespace")?e.css("whiteSpace")||"nowrap":e.data("whitespace"):void 0===a.data("whitespace")?a.css("whiteSpace")||"nowrap":a.data("whitespace"),c.textAlign=d?void 0===e.data("textalign")?e.css("textalign")||"inherit":e.data("textalign"):void 0===a.data("textalign")?a.css("textalign")||"inherit":a.data("textalign"),c.fontWeight=d?void 0===e.data("fontweight")?parseInt(e.css("fontWeight"),0)||0:e.data("fontweight"):void 0===a.data("fontweight")?parseInt(a.css("fontWeight"),0)||0:a.data("fontweight"),c.minWidth=void 0===a.data("width")?parseInt(a.css("minWidth"),0)||0:a.data("width"),c.minHeight=void 0===a.data("height")?parseInt(a.css("minHeight"),0)||0:a.data("height"),void 0!=a.data("videowidth")&&void 0!=a.data("videoheight")){var f=a.data("videowidth"),g=a.data("videoheight");f="100%"===f?"none":f,g="100%"===g?"none":g,a.data("width",f),a.data("height",g)}c.maxWidth=void 0===a.data("width")?parseInt(a.css("maxWidth"),0)||"none":a.data("width"),c.maxHeight=-1!==jQuery.inArray(a.data("type"),["column","row"])?"none":void 0===a.data("height")?parseInt(a.css("maxHeight"),0)||"none":a.data("height"),c.wan=void 0===a.data("wan")?parseInt(a.css("-webkit-transition"),0)||"none":a.data("wan"),c.moan=void 0===a.data("moan")?parseInt(a.css("-moz-animation-transition"),0)||"none":a.data("moan"),c.man=void 0===a.data("man")?parseInt(a.css("-ms-animation-transition"),0)||"none":a.data("man"),c.ani=void 0===a.data("ani")?parseInt(a.css("transition"),0)||"none":a.data("ani")}return c.styleProps={borderTopLeftRadius:a[0].style.borderTopLeftRadius,borderTopRightRadius:a[0].style.borderTopRightRadius,borderBottomRightRadius:a[0].style.borderBottomRightRadius,borderBottomLeftRadius:a[0].style.borderBottomLeftRadius,background:a[0].style.background,boxShadow:a[0].style.boxShadow,"background-color":a[0].style["background-color"],"border-top-color":a[0].style["border-top-color"],"border-bottom-color":a[0].style["border-bottom-color"],"border-right-color":a[0].style["border-right-color"],"border-left-color":a[0].style["border-left-color"],"border-top-style":a[0].style["border-top-style"],"border-bottom-style":a[0].style["border-bottom-style"],"border-left-style":a[0].style["border-left-style"],"border-right-style":a[0].style["border-right-style"],"border-left-width":a[0].style["border-left-width"],"border-right-width":a[0].style["border-right-width"],"border-bottom-width":a[0].style["border-bottom-width"],"border-top-width":a[0].style["border-top-width"],color:a[0].style.color,"text-decoration":a[0].style["text-decoration"],"font-style":a[0].style["font-style"]},""!==c.styleProps.background&&void 0!==c.styleProps.background&&c.styleProps.background!==c.styleProps["background-color"]||delete c.styleProps.background,""==c.styleProps.color&&(c.styleProps.color=a.css("color")),c},z=function(a,b){var c=new Object;return a&&jQuery.each(a,function(d,e){var f=v(e,b)[b.curWinRange];c[d]=void 0!==f?f:a[d]}),c},A=function(a,b,c,d){return a=jQuery.isNumeric(a)?a*b+"px":a,a="full"===a?d:"auto"===a||"none"===a?c:a},B=function(a,b,c,d){var e=a.data();e=void 0===e?{}:e;try{if("BR"==a[0].nodeName||"br"==a[0].tagName)return!1}catch(a){}e.cssobj=void 0===e.cssobj?y(a,c):e.cssobj;var f=z(e.cssobj,b),g=b.bw,h=b.bh;"off"===d&&(g=1,h=1),"auto"==f.lineHeight&&(f.lineHeight=f.fontSize+4);var i={Top:f.marginTop,Bottom:f.marginBottom,Left:f.marginLeft,Right:f.marginRight};if("column"===e._nctype&&(punchgs.TweenLite.set(e._column,{paddingTop:Math.round(f.marginTop*h)+"px",paddingBottom:Math.round(f.marginBottom*h)+"px",paddingLeft:Math.round(f.marginLeft*g)+"px",paddingRight:Math.round(f.marginRight*g)+"px"}),i={Top:0,Bottom:0,Left:0,Right:0}),!a.hasClass("tp-splitted")){a.css("-webkit-transition","none"),a.css("-moz-transition","none"),a.css("-ms-transition","none"),a.css("transition","none");if((void 0!==a.data("transform_hover")||void 0!==a.data("style_hover"))&&punchgs.TweenLite.set(a,f.styleProps),punchgs.TweenLite.set(a,{fontSize:Math.round(f.fontSize*g)+"px",fontWeight:f.fontWeight,letterSpacing:Math.floor(f.letterSpacing*g)+"px",paddingTop:Math.round(f.paddingTop*h)+"px",paddingBottom:Math.round(f.paddingBottom*h)+"px",paddingLeft:Math.round(f.paddingLeft*g)+"px",paddingRight:Math.round(f.paddingRight*g)+"px",marginTop:i.Top*h+"px",marginBottom:i.Bottom*h+"px",marginLeft:i.Left*g+"px",marginRight:i.Right*g+"px",borderTopWidth:Math.round(f.borderTopWidth*h)+"px",borderBottomWidth:Math.round(f.borderBottomWidth*h)+"px",borderLeftWidth:Math.round(f.borderLeftWidth*g)+"px",borderRightWidth:Math.round(f.borderRightWidth*g)+"px",lineHeight:Math.round(f.lineHeight*h)+"px",textAlign:f.textAlign,overwrite:"auto"}),"rekursive"!=c){var k="slide"==f.basealign?b.ulw:b.gridwidth[b.curWinRange],l="slide"==f.basealign?b.ulh:b.gridheight[b.curWinRange],m=A(f.maxWidth,g,"none",k),n=A(f.maxHeight,h,"none",l),o=A(f.minWidth,g,"0px",k),p=A(f.minHeight,h,"0px",l);if(o=void 0===o?0:o,p=void 0===p?0:p,m=void 0===m?"none":m,n=void 0===n?"none":n,e._isgroup&&("#1/1#"===o&&(o=m=k),"#1/2#"===o&&(o=m=k/2),"#1/3#"===o&&(o=m=k/3),"#1/4#"===o&&(o=m=k/4),"#1/5#"===o&&(o=m=k/5),"#1/6#"===o&&(o=m=k/6),"#2/3#"===o&&(o=m=k/3*2),"#3/4#"===o&&(o=m=k/4*3),"#2/5#"===o&&(o=m=k/5*2),"#3/5#"===o&&(o=m=k/5*3),"#4/5#"===o&&(o=m=k/5*4),"#3/6#"===o&&(o=m=k/6*3),"#4/6#"===o&&(o=m=k/6*4),"#5/6#"===o&&(o=m=k/6*5)),e._ingroup&&(e._groupw=o,e._grouph=p),punchgs.TweenLite.set(a,{maxWidth:m,maxHeight:n,minWidth:o,minHeight:p,whiteSpace:f.whiteSpace,textAlign:f.textAlign,overwrite:"auto"}),"nopredefinedcolor"!=f.color&&punchgs.TweenLite.set(a,{color:f.color,overwrite:"auto"}),void 0!=e.svg_src){var q="nopredefinedcolor"!=f.color&&void 0!=f.color?f.color:void 0!=f.css&&"nopredefinedcolor"!=f.css.color&&void 0!=f.css.color?f.css.color:void 0!=f.styleProps.color?f.styleProps.color:void 0!=f.styleProps.css&&void 0!=f.styleProps.css.color&&f.styleProps.css.color;0!=q&&(punchgs.TweenLite.set(a.find("svg"),{fill:q,overwrite:"auto"}),punchgs.TweenLite.set(a.find("svg path"),{fill:q,overwrite:"auto"}))}}"column"===e._nctype&&(void 0===e._column_bg_set&&(e._column_bg_set=a.css("backgroundColor"),e._column_bg_image=a.css("backgroundImage"),e._column_bg_image_repeat=a.css("backgroundRepeat"),e._column_bg_image_position=a.css("backgroundPosition"),e._column_bg_image_size=a.css("backgroundSize"),e._column_bg_opacity=a.data("bgopacity"),e._column_bg_opacity=void 0===e._column_bg_opacity?1:e._column_bg_opacity,punchgs.TweenLite.set(a,{backgroundColor:"transparent",backgroundImage:""})),setTimeout(function(){C(a,b)},1),e._cbgc_auto&&e._cbgc_auto.length>0&&(e._cbgc_auto[0].style.backgroundSize=e._column_bg_image_size,jQuery.isArray(f.marginLeft)?punchgs.TweenLite.set(e._cbgc_auto,{borderTopWidth:f.marginTop[b.curWinRange]*h+"px",borderLeftWidth:f.marginLeft[b.curWinRange]*g+"px",borderRightWidth:f.marginRight[b.curWinRange]*g+"px",borderBottomWidth:f.marginBottom[b.curWinRange]*h+"px",backgroundColor:e._column_bg_set,backgroundImage:e._column_bg_image,backgroundRepeat:e._column_bg_image_repeat,backgroundPosition:e._column_bg_image_position,opacity:e._column_bg_opacity}):punchgs.TweenLite.set(e._cbgc_auto,{borderTopWidth:f.marginTop*h+"px",borderLeftWidth:f.marginLeft*g+"px",borderRightWidth:f.marginRight*g+"px",borderBottomWidth:f.marginBottom*h+"px",backgroundColor:e._column_bg_set,backgroundImage:e._column_bg_image,backgroundRepeat:e._column_bg_image_repeat,backgroundPosition:e._column_bg_image_position,opacity:e._column_bg_opacity}))),setTimeout(function(){a.css("-webkit-transition",a.data("wan")),a.css("-moz-transition",a.data("moan")),a.css("-ms-transition",a.data("man")),a.css("transition",a.data("ani"))},30)}},C=function(a,b){var c=a.data();if(c._cbgc_man&&c._cbgc_man.length>0){var e,f,h;jQuery.isArray(c.cssobj.marginLeft)?(c.cssobj.marginLeft[b.curWinRange]*b.bw,e=c.cssobj.marginTop[b.curWinRange]*b.bh,f=c.cssobj.marginBottom[b.curWinRange]*b.bh,c.cssobj.marginRight[b.curWinRange]*b.bw):(c.cssobj.marginLeft*b.bw,e=c.cssobj.marginTop*b.bh,f=c.cssobj.marginBottom*b.bh,c.cssobj.marginRight*b.bw),h=c._row.hasClass("rev_break_columns")?"100%":c._row.height()-(e+f)+"px",c._cbgc_man[0].style.backgroundSize=c._column_bg_image_size,punchgs.TweenLite.set(c._cbgc_man,{width:"100%",height:h,backgroundColor:c._column_bg_set,backgroundImage:c._column_bg_image,backgroundRepeat:c._column_bg_image_repeat,backgroundPosition:c._column_bg_image_position,overwrite:"auto",opacity:c._column_bg_opacity})}},D=function(a,b){var c=a.data();if(a.hasClass("rs-pendulum")&&void 0==c._loop_timeline){c._loop_timeline=new punchgs.TimelineLite;var d=void 0==a.data("startdeg")?-20:a.data("startdeg"),e=void 0==a.data("enddeg")?20:a.data("enddeg"),f=void 0==a.data("speed")?2:a.data("speed"),g=void 0==a.data("origin")?"50% 50%":a.data("origin"),h=void 0==a.data("easing")?punchgs.Power2.easeInOut:a.data("easing");d*=b,e*=b,c._loop_timeline.append(new punchgs.TweenLite.fromTo(a,f,{force3D:"auto",rotation:d,transformOrigin:g},{rotation:e,ease:h})),c._loop_timeline.append(new punchgs.TweenLite.fromTo(a,f,{force3D:"auto",rotation:e,transformOrigin:g},{rotation:d,ease:h,onComplete:function(){c._loop_timeline.restart()}}))}if(a.hasClass("rs-rotate")&&void 0==c._loop_timeline){c._loop_timeline=new punchgs.TimelineLite;var d=void 0==a.data("startdeg")?0:a.data("startdeg"),e=void 0==a.data("enddeg")?360:a.data("enddeg"),f=void 0==a.data("speed")?2:a.data("speed"),g=void 0==a.data("origin")?"50% 50%":a.data("origin"),h=void 0==a.data("easing")?punchgs.Power2.easeInOut:a.data("easing");d*=b,e*=b,c._loop_timeline.append(new punchgs.TweenLite.fromTo(a,f,{force3D:"auto",rotation:d,transformOrigin:g},{rotation:e,ease:h,onComplete:function(){c._loop_timeline.restart()}}))}if(a.hasClass("rs-slideloop")&&void 0==c._loop_timeline){c._loop_timeline=new punchgs.TimelineLite;var i=void 0==a.data("xs")?0:a.data("xs"),j=void 0==a.data("ys")?0:a.data("ys"),k=void 0==a.data("xe")?0:a.data("xe"),l=void 0==a.data("ye")?0:a.data("ye"),f=void 0==a.data("speed")?2:a.data("speed"),h=void 0==a.data("easing")?punchgs.Power2.easeInOut:a.data("easing");i*=b,j*=b,k*=b,l*=b,c._loop_timeline.append(new punchgs.TweenLite.fromTo(a,f,{force3D:"auto",x:i,y:j},{x:k,y:l,ease:h})),c._loop_timeline.append(new punchgs.TweenLite.fromTo(a,f,{force3D:"auto",x:k,y:l},{x:i,y:j,onComplete:function(){c._loop_timeline.restart()}}))}if(a.hasClass("rs-pulse")&&void 0==c._loop_timeline){c._loop_timeline=new punchgs.TimelineLite;var m=void 0==a.data("zoomstart")?0:a.data("zoomstart"),n=void 0==a.data("zoomend")?0:a.data("zoomend"),f=void 0==a.data("speed")?2:a.data("speed"),h=void 0==a.data("easing")?punchgs.Power2.easeInOut:a.data("easing");c._loop_timeline.append(new punchgs.TweenLite.fromTo(a,f,{force3D:"auto",scale:m},{scale:n,ease:h})),c._loop_timeline.append(new punchgs.TweenLite.fromTo(a,f,{force3D:"auto",scale:n},{scale:m,onComplete:function(){c._loop_timeline.restart()}}))}if(a.hasClass("rs-wave")&&void 0==c._loop_timeline){c._loop_timeline=new punchgs.TimelineLite;var o=void 0==a.data("angle")?10:parseInt(a.data("angle"),0),p=void 0==a.data("radius")?10:parseInt(a.data("radius"),0),f=void 0==a.data("speed")?-20:a.data("speed"),g=void 0==a.data("origin")?"50% 50%":a.data("origin"),q=g.split(" "),r=new Object;q.length>=1?(r.x=q[0],r.y=q[1]):(r.x="50%",r.y="50%"),p*=b;var s=(parseInt(r.x,0)/100-.5)*a.width(),t=(parseInt(r.y,0)/100-.5)*a.height(),u=-1*p+t,v=0+s,w={a:0,ang:o,element:a,unit:p,xoffset:v,yoffset:u},x=parseInt(o,0),y=new punchgs.TweenLite.fromTo(w,f,{a:0+x},{a:360+x,force3D:"auto",ease:punchgs.Linear.easeNone});y.eventCallback("onUpdate",function(a){var b=a.a*(Math.PI/180),c=a.yoffset+a.unit*(1-Math.sin(b)),d=a.xoffset+Math.cos(b)*a.unit;punchgs.TweenLite.to(a.element,.1,{force3D:"auto",x:d,y:c})},[w]),y.eventCallback("onComplete",function(a){a._loop_timeline.restart()},[c]),c._loop_timeline.append(y)}},E=function(a){a.closest(".rs-pendulum, .rs-slideloop, .rs-pulse, .rs-wave").each(function(){var a=this;void 0!=a._loop_timeline&&(a._loop_timeline.pause(),a._loop_timeline=null)})}}(jQuery);


// fin layer animation 

/********************************************
 * REVOLUTION 5.2 EXTENSION - NAVIGATION
 * @version: 1.3.5 (06.04.2017)
 * @requires jquery.themepunch.revolution.js
 * @author ThemePunch
*********************************************/
!function(a){"use strict";var b=jQuery.fn.revolution,c=b.is_mobile(),d={alias:"Navigation Min JS",name:"revolution.extensions.navigation.min.js",min_core:"5.4.0",version:"1.3.5"};jQuery.extend(!0,b,{hideUnHideNav:function(a){var b=a.c.width(),c=a.navigation.arrows,d=a.navigation.bullets,e=a.navigation.thumbnails,f=a.navigation.tabs;m(c)&&y(a.c.find(".tparrows"),c.hide_under,b,c.hide_over),m(d)&&y(a.c.find(".tp-bullets"),d.hide_under,b,d.hide_over),m(e)&&y(a.c.parent().find(".tp-thumbs"),e.hide_under,b,e.hide_over),m(f)&&y(a.c.parent().find(".tp-tabs"),f.hide_under,b,f.hide_over),x(a)},resizeThumbsTabs:function(a,b){if(a.navigation&&a.navigation.tabs.enable||a.navigation&&a.navigation.thumbnails.enable){var c=(jQuery(window).width()-480)/500,d=new punchgs.TimelineLite,e=a.navigation.tabs,g=a.navigation.thumbnails,h=a.navigation.bullets;if(d.pause(),c=c>1?1:c<0?0:c,m(e)&&(b||e.width>e.min_width)&&f(c,d,a.c,e,a.slideamount,"tab"),m(g)&&(b||g.width>g.min_width)&&f(c,d,a.c,g,a.slideamount,"thumb"),m(h)&&b){var i=a.c.find(".tp-bullets");i.find(".tp-bullet").each(function(a){var b=jQuery(this),c=a+1,d=b.outerWidth()+parseInt(void 0===h.space?0:h.space,0),e=b.outerHeight()+parseInt(void 0===h.space?0:h.space,0);"vertical"===h.direction?(b.css({top:(c-1)*e+"px",left:"0px"}),i.css({height:(c-1)*e+b.outerHeight(),width:b.outerWidth()})):(b.css({left:(c-1)*d+"px",top:"0px"}),i.css({width:(c-1)*d+b.outerWidth(),height:b.outerHeight()}))})}d.play(),x(a)}return!0},updateNavIndexes:function(a){function d(a){c.find(a).lenght>0&&c.find(a).each(function(a){jQuery(this).data("liindex",a)})}var c=a.c;d(".tp-tab"),d(".tp-bullet"),d(".tp-thumb"),b.resizeThumbsTabs(a,!0),b.manageNavigation(a)},manageNavigation:function(a){var c=b.getHorizontalOffset(a.c.parent(),"left"),d=b.getHorizontalOffset(a.c.parent(),"right");m(a.navigation.bullets)&&("fullscreen"!=a.sliderLayout&&"fullwidth"!=a.sliderLayout&&(a.navigation.bullets.h_offset_old=void 0===a.navigation.bullets.h_offset_old?a.navigation.bullets.h_offset:a.navigation.bullets.h_offset_old,a.navigation.bullets.h_offset="center"===a.navigation.bullets.h_align?a.navigation.bullets.h_offset_old+c/2-d/2:a.navigation.bullets.h_offset_old+c-d),t(a.c.find(".tp-bullets"),a.navigation.bullets,a)),m(a.navigation.thumbnails)&&t(a.c.parent().find(".tp-thumbs"),a.navigation.thumbnails,a),m(a.navigation.tabs)&&t(a.c.parent().find(".tp-tabs"),a.navigation.tabs,a),m(a.navigation.arrows)&&("fullscreen"!=a.sliderLayout&&"fullwidth"!=a.sliderLayout&&(a.navigation.arrows.left.h_offset_old=void 0===a.navigation.arrows.left.h_offset_old?a.navigation.arrows.left.h_offset:a.navigation.arrows.left.h_offset_old,a.navigation.arrows.left.h_offset="right"===a.navigation.arrows.left.h_align?a.navigation.arrows.left.h_offset_old+d:a.navigation.arrows.left.h_offset_old+c,a.navigation.arrows.right.h_offset_old=void 0===a.navigation.arrows.right.h_offset_old?a.navigation.arrows.right.h_offset:a.navigation.arrows.right.h_offset_old,a.navigation.arrows.right.h_offset="right"===a.navigation.arrows.right.h_align?a.navigation.arrows.right.h_offset_old+d:a.navigation.arrows.right.h_offset_old+c),t(a.c.find(".tp-leftarrow.tparrows"),a.navigation.arrows.left,a),t(a.c.find(".tp-rightarrow.tparrows"),a.navigation.arrows.right,a)),m(a.navigation.thumbnails)&&e(a.c.parent().find(".tp-thumbs"),a.navigation.thumbnails),m(a.navigation.tabs)&&e(a.c.parent().find(".tp-tabs"),a.navigation.tabs)},createNavigation:function(a,f){if("stop"===b.compare_version(d).check)return!1;var g=a.parent(),j=f.navigation.arrows,n=f.navigation.bullets,r=f.navigation.thumbnails,s=f.navigation.tabs,t=m(j),v=m(n),x=m(r),y=m(s);h(a,f),i(a,f),t&&q(a,j,f),f.li.each(function(b){var c=jQuery(f.li[f.li.length-1-b]),d=jQuery(this);v&&(f.navigation.bullets.rtl?u(a,n,c,f):u(a,n,d,f)),x&&(f.navigation.thumbnails.rtl?w(a,r,c,"tp-thumb",f):w(a,r,d,"tp-thumb",f)),y&&(f.navigation.tabs.rtl?w(a,s,c,"tp-tab",f):w(a,s,d,"tp-tab",f))}),a.bind("revolution.slide.onafterswap revolution.nextslide.waiting",function(){var b=0==a.find(".next-revslide").length?a.find(".active-revslide").data("index"):a.find(".next-revslide").data("index");a.find(".tp-bullet").each(function(){var a=jQuery(this);a.data("liref")===b?a.addClass("selected"):a.removeClass("selected")}),g.find(".tp-thumb, .tp-tab").each(function(){var a=jQuery(this);a.data("liref")===b?(a.addClass("selected"),a.hasClass("tp-tab")?e(g.find(".tp-tabs"),s):e(g.find(".tp-thumbs"),r)):a.removeClass("selected")});var c=0,d=!1;f.thumbs&&jQuery.each(f.thumbs,function(a,e){c=!1===d?a:c,d=e.id===b||a===b||d});var h=c>0?c-1:f.slideamount-1,i=c+1==f.slideamount?0:c+1;if(!0===j.enable){var k=j.tmp;if(void 0!=f.thumbs[h]&&jQuery.each(f.thumbs[h].params,function(a,b){k=k.replace(b.from,b.to)}),j.left.j.html(k),k=j.tmp,i>f.slideamount)return;jQuery.each(f.thumbs[i].params,function(a,b){k=k.replace(b.from,b.to)}),j.right.j.html(k),j.rtl?(punchgs.TweenLite.set(j.left.j.find(".tp-arr-imgholder"),{backgroundImage:"url("+f.thumbs[i].src+")"}),punchgs.TweenLite.set(j.right.j.find(".tp-arr-imgholder"),{backgroundImage:"url("+f.thumbs[h].src+")"})):(punchgs.TweenLite.set(j.left.j.find(".tp-arr-imgholder"),{backgroundImage:"url("+f.thumbs[h].src+")"}),punchgs.TweenLite.set(j.right.j.find(".tp-arr-imgholder"),{backgroundImage:"url("+f.thumbs[i].src+")"}))}}),l(j),l(n),l(r),l(s),g.on("mouseenter mousemove",function(){g.hasClass("tp-mouseover")||(g.addClass("tp-mouseover"),punchgs.TweenLite.killDelayedCallsTo(p),t&&j.hide_onleave&&p(g.find(".tparrows"),j,"show"),v&&n.hide_onleave&&p(g.find(".tp-bullets"),n,"show"),x&&r.hide_onleave&&p(g.find(".tp-thumbs"),r,"show"),y&&s.hide_onleave&&p(g.find(".tp-tabs"),s,"show"),c&&(g.removeClass("tp-mouseover"),o(a,f)))}),g.on("mouseleave",function(){g.removeClass("tp-mouseover"),o(a,f)}),t&&j.hide_onleave&&p(g.find(".tparrows"),j,"hide",0),v&&n.hide_onleave&&p(g.find(".tp-bullets"),n,"hide",0),x&&r.hide_onleave&&p(g.find(".tp-thumbs"),r,"hide",0),y&&s.hide_onleave&&p(g.find(".tp-tabs"),s,"hide",0),x&&k(g.find(".tp-thumbs"),f),y&&k(g.find(".tp-tabs"),f),"carousel"===f.sliderType&&k(a,f,!0),("on"===f.navigation.touch.touchOnDesktop||"on"==f.navigation.touch.touchenabled&&c)&&k(a,f,"swipebased")}});var e=function(a,b){var d=(a.hasClass("tp-thumbs"),a.hasClass("tp-thumbs")?".tp-thumb-mask":".tp-tab-mask"),e=a.hasClass("tp-thumbs")?".tp-thumbs-inner-wrapper":".tp-tabs-inner-wrapper",f=a.hasClass("tp-thumbs")?".tp-thumb":".tp-tab",g=a.find(d),h=g.find(e),i=b.direction,j="vertical"===i?g.find(f).first().outerHeight(!0)+b.space:g.find(f).first().outerWidth(!0)+b.space,k="vertical"===i?g.height():g.width(),l=parseInt(g.find(f+".selected").data("liindex"),0),m=k/j,n="vertical"===i?g.height():g.width(),o=0-l*j,p="vertical"===i?h.height():h.width(),q=o<0-(p-n)?0-(p-n):q>0?0:o,r=h.data("offset");m>2&&(q=o-(r+j)<=0?o-(r+j)<0-j?r:q+j:q,q=o-j+r+k<j&&o+(Math.round(m)-2)*j<r?o+(Math.round(m)-2)*j:q),q=q<0-(p-n)?0-(p-n):q>0?0:q,"vertical"!==i&&g.width()>=h.width()&&(q=0),"vertical"===i&&g.height()>=h.height()&&(q=0),a.hasClass("dragged")||("vertical"===i?h.data("tmmove",punchgs.TweenLite.to(h,.5,{top:q+"px",ease:punchgs.Power3.easeInOut})):h.data("tmmove",punchgs.TweenLite.to(h,.5,{left:q+"px",ease:punchgs.Power3.easeInOut})),h.data("offset",q))},f=function(a,b,c,d,e,f){var g=c.parent().find(".tp-"+f+"s"),h=g.find(".tp-"+f+"s-inner-wrapper"),i=g.find(".tp-"+f+"-mask"),j=d.width*a<d.min_width?d.min_width:Math.round(d.width*a),k=Math.round(j/d.width*d.height),l="vertical"===d.direction?j:j*e+d.space*(e-1),m="vertical"===d.direction?k*e+d.space*(e-1):k,n="vertical"===d.direction?{width:j+"px"}:{height:k+"px"};b.add(punchgs.TweenLite.set(g,n)),b.add(punchgs.TweenLite.set(h,{width:l+"px",height:m+"px"})),b.add(punchgs.TweenLite.set(i,{width:l+"px",height:m+"px"}));var o=h.find(".tp-"+f);return o&&jQuery.each(o,function(a,c){"vertical"===d.direction?b.add(punchgs.TweenLite.set(c,{top:a*(k+parseInt(void 0===d.space?0:d.space,0)),width:j+"px",height:k+"px"})):"horizontal"===d.direction&&b.add(punchgs.TweenLite.set(c,{left:a*(j+parseInt(void 0===d.space?0:d.space,0)),width:j+"px",height:k+"px"}))}),b},g=function(a){var b=0,c=0,d=0,e=0,f=1,g=1,h=1;return"detail"in a&&(c=a.detail),"wheelDelta"in a&&(c=-a.wheelDelta/120),"wheelDeltaY"in a&&(c=-a.wheelDeltaY/120),"wheelDeltaX"in a&&(b=-a.wheelDeltaX/120),"axis"in a&&a.axis===a.HORIZONTAL_AXIS&&(b=c,c=0),d=b*f,e=c*f,"deltaY"in a&&(e=a.deltaY),"deltaX"in a&&(d=a.deltaX),(d||e)&&a.deltaMode&&(1==a.deltaMode?(d*=g,e*=g):(d*=h,e*=h)),d&&!b&&(b=d<1?-1:1),e&&!c&&(c=e<1?-1:1),e=navigator.userAgent.match(/mozilla/i)?10*e:e,(e>300||e<-300)&&(e/=10),{spinX:b,spinY:c,pixelX:d,pixelY:e}},h=function(a,c){"on"===c.navigation.keyboardNavigation&&jQuery(document).keydown(function(d){("horizontal"==c.navigation.keyboard_direction&&39==d.keyCode||"vertical"==c.navigation.keyboard_direction&&40==d.keyCode)&&(c.sc_indicator="arrow",c.sc_indicator_dir=0,b.callingNewSlide(a,1)),("horizontal"==c.navigation.keyboard_direction&&37==d.keyCode||"vertical"==c.navigation.keyboard_direction&&38==d.keyCode)&&(c.sc_indicator="arrow",c.sc_indicator_dir=1,b.callingNewSlide(a,-1))})},i=function(a,c){if("on"===c.navigation.mouseScrollNavigation||"carousel"===c.navigation.mouseScrollNavigation){c.isIEEleven=!!navigator.userAgent.match(/Trident.*rv\:11\./),c.isSafari=!!navigator.userAgent.match(/safari/i),c.ischrome=!!navigator.userAgent.match(/chrome/i);var d=c.ischrome?-49:c.isIEEleven||c.isSafari?-9:navigator.userAgent.match(/mozilla/i)?-29:-49,e=c.ischrome?49:c.isIEEleven||c.isSafari?9:navigator.userAgent.match(/mozilla/i)?29:49;a.on("mousewheel DOMMouseScroll",function(f){var h=g(f.originalEvent),i=a.find(".tp-revslider-slidesli.active-revslide").index(),j=a.find(".tp-revslider-slidesli.processing-revslide").index(),k=-1!=i&&0==i||-1!=j&&0==j,l=-1!=i&&i==c.slideamount-1||1!=j&&j==c.slideamount-1,m=!0;"carousel"==c.navigation.mouseScrollNavigation&&(k=l=!1),-1==j?h.pixelY<d?(k||(c.sc_indicator="arrow","reverse"!==c.navigation.mouseScrollReverse&&(c.sc_indicator_dir=1,b.callingNewSlide(a,-1)),m=!1),l||(c.sc_indicator="arrow","reverse"===c.navigation.mouseScrollReverse&&(c.sc_indicator_dir=0,b.callingNewSlide(a,1)),m=!1)):h.pixelY>e&&(l||(c.sc_indicator="arrow","reverse"!==c.navigation.mouseScrollReverse&&(c.sc_indicator_dir=0,b.callingNewSlide(a,1)),m=!1),k||(c.sc_indicator="arrow","reverse"===c.navigation.mouseScrollReverse&&(c.sc_indicator_dir=1,b.callingNewSlide(a,-1)),m=!1)):m=!1;var n=c.c.offset().top-jQuery("body").scrollTop(),o=n+c.c.height();return"carousel"!=c.navigation.mouseScrollNavigation?("reverse"!==c.navigation.mouseScrollReverse&&(n>0&&h.pixelY>0||o<jQuery(window).height()&&h.pixelY<0)&&(m=!0),"reverse"===c.navigation.mouseScrollReverse&&(n<0&&h.pixelY<0||o>jQuery(window).height()&&h.pixelY>0)&&(m=!0)):m=!1,0==m?(f.preventDefault(f),!1):void 0})}},j=function(a,b,d){return a=c?jQuery(d.target).closest("."+a).length||jQuery(d.srcElement).closest("."+a).length:jQuery(d.toElement).closest("."+a).length||jQuery(d.originalTarget).closest("."+a).length,!0===a||1===a?1:0},k=function(a,d,e){var f=d.carousel;jQuery(".bullet, .bullets, .tp-bullets, .tparrows").addClass("noSwipe"),f.Limit="endless";var h=(c||b.get_browser(),a),i="vertical"===d.navigation.thumbnails.direction||"vertical"===d.navigation.tabs.direction?"none":"vertical",k=d.navigation.touch.swipe_direction||"horizontal";i="swipebased"==e&&"vertical"==k?"none":e?"vertical":i,jQuery.fn.swipetp||(jQuery.fn.swipetp=jQuery.fn.swipe),jQuery.fn.swipetp.defaults&&jQuery.fn.swipetp.defaults.excludedElements||jQuery.fn.swipetp.defaults||(jQuery.fn.swipetp.defaults=new Object),jQuery.fn.swipetp.defaults.excludedElements="label, button, input, select, textarea, .noSwipe",h.swipetp({allowPageScroll:i,triggerOnTouchLeave:!0,treshold:d.navigation.touch.swipe_treshold,fingers:d.navigation.touch.swipe_min_touches,excludeElements:jQuery.fn.swipetp.defaults.excludedElements,swipeStatus:function(e,g,h,i,l,m,n){var o=j("rev_slider_wrapper",a,e),p=j("tp-thumbs",a,e),q=j("tp-tabs",a,e),r=jQuery(this).attr("class"),s=!!r.match(/tp-tabs|tp-thumb/gi);if("carousel"===d.sliderType&&(("move"===g||"end"===g||"cancel"==g)&&d.dragStartedOverSlider&&!d.dragStartedOverThumbs&&!d.dragStartedOverTabs||"start"===g&&o>0&&0===p&&0===q)){if(c&&("up"===h||"down"===h))return;switch(d.dragStartedOverSlider=!0,i=h&&h.match(/left|up/g)?Math.round(-1*i):i=Math.round(1*i),g){case"start":void 0!==f.positionanim&&(f.positionanim.kill(),f.slide_globaloffset="off"===f.infinity?f.slide_offset:b.simp(f.slide_offset,f.maxwidth)),f.overpull="none",f.wrap.addClass("dragged");break;case"move":if(d.c.find(".tp-withaction").addClass("tp-temporarydisabled"),f.slide_offset="off"===f.infinity?f.slide_globaloffset+i:b.simp(f.slide_globaloffset+i,f.maxwidth),"off"===f.infinity){var t="center"===f.horizontal_align?(f.wrapwidth/2-f.slide_width/2-f.slide_offset)/f.slide_width:(0-f.slide_offset)/f.slide_width;"none"!==f.overpull&&0!==f.overpull||!(t<0||t>d.slideamount-1)?t>=0&&t<=d.slideamount-1&&(t>=0&&i>f.overpull||t<=d.slideamount-1&&i<f.overpull)&&(f.overpull=0):f.overpull=i,f.slide_offset=t<0?f.slide_offset+(f.overpull-i)/1.1+Math.sqrt(Math.abs((f.overpull-i)/1.1)):t>d.slideamount-1?f.slide_offset+(f.overpull-i)/1.1-Math.sqrt(Math.abs((f.overpull-i)/1.1)):f.slide_offset}b.organiseCarousel(d,h,!0,!0);break;case"end":case"cancel":f.slide_globaloffset=f.slide_offset,f.wrap.removeClass("dragged"),b.carouselToEvalPosition(d,h),d.dragStartedOverSlider=!1,d.dragStartedOverThumbs=!1,d.dragStartedOverTabs=!1,setTimeout(function(){d.c.find(".tp-withaction").removeClass("tp-temporarydisabled")},19)}}else{if(("move"!==g&&"end"!==g&&"cancel"!=g||d.dragStartedOverSlider||!d.dragStartedOverThumbs&&!d.dragStartedOverTabs)&&!("start"===g&&o>0&&(p>0||q>0))){if("end"==g&&!s){if(d.sc_indicator="arrow","horizontal"==k&&"left"==h||"vertical"==k&&"up"==h)return d.sc_indicator_dir=0,b.callingNewSlide(d.c,1),!1;if("horizontal"==k&&"right"==h||"vertical"==k&&"down"==h)return d.sc_indicator_dir=1,b.callingNewSlide(d.c,-1),!1}return d.dragStartedOverSlider=!1,d.dragStartedOverThumbs=!1,d.dragStartedOverTabs=!1,!0}p>0&&(d.dragStartedOverThumbs=!0),q>0&&(d.dragStartedOverTabs=!0);var u=d.dragStartedOverThumbs?".tp-thumbs":".tp-tabs",v=d.dragStartedOverThumbs?".tp-thumb-mask":".tp-tab-mask",w=d.dragStartedOverThumbs?".tp-thumbs-inner-wrapper":".tp-tabs-inner-wrapper",x=d.dragStartedOverThumbs?".tp-thumb":".tp-tab",y=d.dragStartedOverThumbs?d.navigation.thumbnails:d.navigation.tabs;i=h&&h.match(/left|up/g)?Math.round(-1*i):i=Math.round(1*i);var z=a.parent().find(v),A=z.find(w),B=y.direction,C="vertical"===B?A.height():A.width(),D="vertical"===B?z.height():z.width(),E="vertical"===B?z.find(x).first().outerHeight(!0)+y.space:z.find(x).first().outerWidth(!0)+y.space,F=void 0===A.data("offset")?0:parseInt(A.data("offset"),0),G=0;switch(g){case"start":a.parent().find(u).addClass("dragged"),F="vertical"===B?A.position().top:A.position().left,A.data("offset",F),A.data("tmmove")&&A.data("tmmove").pause();break;case"move":if(C<=D)return!1;G=F+i,G=G>0?"horizontal"===B?G-A.width()*(G/A.width()*G/A.width()):G-A.height()*(G/A.height()*G/A.height()):G;var H="vertical"===B?0-(A.height()-z.height()):0-(A.width()-z.width());G=G<H?"horizontal"===B?G+A.width()*(G-H)/A.width()*(G-H)/A.width():G+A.height()*(G-H)/A.height()*(G-H)/A.height():G,"vertical"===B?punchgs.TweenLite.set(A,{top:G+"px"}):punchgs.TweenLite.set(A,{left:G+"px"});break;case"end":case"cancel":if(s)return G=F+i,G="vertical"===B?G<0-(A.height()-z.height())?0-(A.height()-z.height()):G:G<0-(A.width()-z.width())?0-(A.width()-z.width()):G,G=G>0?0:G,G=Math.abs(i)>E/10?i<=0?Math.floor(G/E)*E:Math.ceil(G/E)*E:i<0?Math.ceil(G/E)*E:Math.floor(G/E)*E,G="vertical"===B?G<0-(A.height()-z.height())?0-(A.height()-z.height()):G:G<0-(A.width()-z.width())?0-(A.width()-z.width()):G,G=G>0?0:G,"vertical"===B?punchgs.TweenLite.to(A,.5,{top:G+"px",ease:punchgs.Power3.easeOut}):punchgs.TweenLite.to(A,.5,{left:G+"px",ease:punchgs.Power3.easeOut}),G=G||("vertical"===B?A.position().top:A.position().left),A.data("offset",G),A.data("distance",i),setTimeout(function(){d.dragStartedOverSlider=!1,d.dragStartedOverThumbs=!1,d.dragStartedOverTabs=!1},100),a.parent().find(u).removeClass("dragged"),!1}}}})},l=function(a){a.hide_delay=jQuery.isNumeric(parseInt(a.hide_delay,0))?a.hide_delay/1e3:.2,a.hide_delay_mobile=jQuery.isNumeric(parseInt(a.hide_delay_mobile,0))?a.hide_delay_mobile/1e3:.2},m=function(a){return a&&a.enable},n=function(a){return a&&a.enable&&!0===a.hide_onleave&&(void 0===a.position||!a.position.match(/outer/g))},o=function(a,b){var d=a.parent();n(b.navigation.arrows)&&punchgs.TweenLite.delayedCall(c?b.navigation.arrows.hide_delay_mobile:b.navigation.arrows.hide_delay,p,[d.find(".tparrows"),b.navigation.arrows,"hide"]),n(b.navigation.bullets)&&punchgs.TweenLite.delayedCall(c?b.navigation.bullets.hide_delay_mobile:b.navigation.bullets.hide_delay,p,[d.find(".tp-bullets"),b.navigation.bullets,"hide"]),n(b.navigation.thumbnails)&&punchgs.TweenLite.delayedCall(c?b.navigation.thumbnails.hide_delay_mobile:b.navigation.thumbnails.hide_delay,p,[d.find(".tp-thumbs"),b.navigation.thumbnails,"hide"]),n(b.navigation.tabs)&&punchgs.TweenLite.delayedCall(c?b.navigation.tabs.hide_delay_mobile:b.navigation.tabs.hide_delay,p,[d.find(".tp-tabs"),b.navigation.tabs,"hide"])},p=function(a,b,c,d){switch(d=void 0===d?.5:d,c){case"show":punchgs.TweenLite.to(a,d,{autoAlpha:1,ease:punchgs.Power3.easeInOut,overwrite:"auto"});break;case"hide":punchgs.TweenLite.to(a,d,{autoAlpha:0,ease:punchgs.Power3.easeInOu,overwrite:"auto"})}},q=function(a,b,c){b.style=void 0===b.style?"":b.style,b.left.style=void 0===b.left.style?"":b.left.style,b.right.style=void 0===b.right.style?"":b.right.style,0===a.find(".tp-leftarrow.tparrows").length&&a.append('<div class="tp-leftarrow tparrows '+b.style+" "+b.left.style+'">'+b.tmp+"</div>"),0===a.find(".tp-rightarrow.tparrows").length&&a.append('<div class="tp-rightarrow tparrows '+b.style+" "+b.right.style+'">'+b.tmp+"</div>");var d=a.find(".tp-leftarrow.tparrows"),e=a.find(".tp-rightarrow.tparrows");b.rtl?(d.click(function(){c.sc_indicator="arrow",c.sc_indicator_dir=0,a.revnext()}),e.click(function(){c.sc_indicator="arrow",c.sc_indicator_dir=1,a.revprev()})):(e.click(function(){c.sc_indicator="arrow",c.sc_indicator_dir=0,a.revnext()}),d.click(function(){c.sc_indicator="arrow",c.sc_indicator_dir=1,a.revprev()})),b.right.j=a.find(".tp-rightarrow.tparrows"),b.left.j=a.find(".tp-leftarrow.tparrows"),b.padding_top=parseInt(c.carousel.padding_top||0,0),b.padding_bottom=parseInt(c.carousel.padding_bottom||0,0),t(d,b.left,c),t(e,b.right,c),b.left.opt=c,b.right.opt=c,"outer-left"!=b.position&&"outer-right"!=b.position||(c.outernav=!0)},r=function(a,b,c){var d=a.outerHeight(!0),f=(a.outerWidth(!0),void 0==b.opt?0:0==c.conh?c.height:c.conh),g="layergrid"==b.container?"fullscreen"==c.sliderLayout?c.height/2-c.gridheight[c.curWinRange]*c.bh/2:"on"==c.autoHeight||void 0!=c.minHeight&&c.minHeight>0?f/2-c.gridheight[c.curWinRange]*c.bh/2:0:0,h="top"===b.v_align?{top:"0px",y:Math.round(b.v_offset+g)+"px"}:"center"===b.v_align?{top:"50%",y:Math.round(0-d/2+b.v_offset)+"px"}:{top:"100%",y:Math.round(0-(d+b.v_offset+g))+"px"};a.hasClass("outer-bottom")||punchgs.TweenLite.set(a,h)},s=function(a,b,c){var e=(a.outerHeight(!0),a.outerWidth(!0)),f="layergrid"==b.container?"carousel"===c.sliderType?0:c.width/2-c.gridwidth[c.curWinRange]*c.bw/2:0,g="left"===b.h_align?{left:"0px",x:Math.round(b.h_offset+f)+"px"}:"center"===b.h_align?{left:"50%",x:Math.round(0-e/2+b.h_offset)+"px"}:{left:"100%",x:Math.round(0-(e+b.h_offset+f))+"px"};punchgs.TweenLite.set(a,g)},t=function(a,b,c){var d=a.closest(".tp-simpleresponsive").length>0?a.closest(".tp-simpleresponsive"):a.closest(".tp-revslider-mainul").length>0?a.closest(".tp-revslider-mainul"):a.closest(".rev_slider_wrapper").length>0?a.closest(".rev_slider_wrapper"):a.parent().find(".tp-revslider-mainul"),e=d.width(),f=d.height();if(r(a,b,c),s(a,b,c),"outer-left"!==b.position||"fullwidth"!=b.sliderLayout&&"fullscreen"!=b.sliderLayout?"outer-right"!==b.position||"fullwidth"!=b.sliderLayout&&"fullscreen"!=b.sliderLayout||punchgs.TweenLite.set(a,{right:0-a.outerWidth()+"px",x:b.h_offset+"px"}):punchgs.TweenLite.set(a,{left:0-a.outerWidth()+"px",x:b.h_offset+"px"}),a.hasClass("tp-thumbs")||a.hasClass("tp-tabs")){var g=a.data("wr_padding"),h=a.data("maxw"),i=a.data("maxh"),j=a.hasClass("tp-thumbs")?a.find(".tp-thumb-mask"):a.find(".tp-tab-mask"),k=parseInt(b.padding_top||0,0),l=parseInt(b.padding_bottom||0,0);h>e&&"outer-left"!==b.position&&"outer-right"!==b.position?(punchgs.TweenLite.set(a,{left:"0px",x:0,maxWidth:e-2*g+"px"}),punchgs.TweenLite.set(j,{maxWidth:e-2*g+"px"})):(punchgs.TweenLite.set(a,{maxWidth:h+"px"}),punchgs.TweenLite.set(j,{maxWidth:h+"px"})),i+2*g>f&&"outer-bottom"!==b.position&&"outer-top"!==b.position?(punchgs.TweenLite.set(a,{top:"0px",y:0,maxHeight:k+l+(f-2*g)+"px"}),punchgs.TweenLite.set(j,{maxHeight:k+l+(f-2*g)+"px"})):(punchgs.TweenLite.set(a,{maxHeight:i+"px"}),punchgs.TweenLite.set(j,{maxHeight:i+"px"})),"outer-left"!==b.position&&"outer-right"!==b.position&&(k=0,l=0),!0===b.span&&"vertical"===b.direction?(punchgs.TweenLite.set(a,{maxHeight:k+l+(f-2*g)+"px",height:k+l+(f-2*g)+"px",top:0-k,y:0}),r(j,b,c)):!0===b.span&&"horizontal"===b.direction&&(punchgs.TweenLite.set(a,{maxWidth:"100%",width:e-2*g+"px",left:0,x:0}),s(j,b,c))}},u=function(a,b,c,d){0===a.find(".tp-bullets").length&&(b.style=void 0===b.style?"":b.style,a.append('<div class="tp-bullets '+b.style+" "+b.direction+'"></div>'));var e=a.find(".tp-bullets"),f=c.data("index"),g=b.tmp;jQuery.each(d.thumbs[c.index()].params,function(a,b){g=g.replace(b.from,b.to)}),e.append('<div class="justaddedbullet tp-bullet">'+g+"</div>");var h=a.find(".justaddedbullet"),i=a.find(".tp-bullet").length,j=h.outerWidth()+parseInt(void 0===b.space?0:b.space,0),k=h.outerHeight()+parseInt(void 0===b.space?0:b.space,0);"vertical"===b.direction?(h.css({top:(i-1)*k+"px",left:"0px"}),e.css({height:(i-1)*k+h.outerHeight(),width:h.outerWidth()})):(h.css({left:(i-1)*j+"px",top:"0px"}),e.css({width:(i-1)*j+h.outerWidth(),height:h.outerHeight()})),h.find(".tp-bullet-image").css({backgroundImage:"url("+d.thumbs[c.index()].src+")"}),h.data("liref",f),h.click(function(){d.sc_indicator="bullet",a.revcallslidewithid(f),a.find(".tp-bullet").removeClass("selected"),jQuery(this).addClass("selected")}),h.removeClass("justaddedbullet"),b.padding_top=parseInt(d.carousel.padding_top||0,0),b.padding_bottom=parseInt(d.carousel.padding_bottom||0,0),b.opt=d,"outer-left"!=b.position&&"outer-right"!=b.position||(d.outernav=!0),e.addClass("nav-pos-hor-"+b.h_align),e.addClass("nav-pos-ver-"+b.v_align),e.addClass("nav-dir-"+b.direction),t(e,b,d)},w=function(a,b,c,d,e){var f="tp-thumb"===d?".tp-thumbs":".tp-tabs",g="tp-thumb"===d?".tp-thumb-mask":".tp-tab-mask",h="tp-thumb"===d?".tp-thumbs-inner-wrapper":".tp-tabs-inner-wrapper",i="tp-thumb"===d?".tp-thumb":".tp-tab",j="tp-thumb"===d?".tp-thumb-image":".tp-tab-image";if(b.visibleAmount=b.visibleAmount>e.slideamount?e.slideamount:b.visibleAmount,b.sliderLayout=e.sliderLayout,0===a.parent().find(f).length){b.style=void 0===b.style?"":b.style;var k=!0===b.span?"tp-span-wrapper":"",l='<div class="'+d+"s "+k+" "+b.position+" "+b.style+'"><div class="'+d+'-mask"><div class="'+d+'s-inner-wrapper" style="position:relative;"></div></div></div>';"outer-top"===b.position?a.parent().prepend(l):"outer-bottom"===b.position?a.after(l):a.append(l),b.padding_top=parseInt(e.carousel.padding_top||0,0),b.padding_bottom=parseInt(e.carousel.padding_bottom||0,0),"outer-left"!=b.position&&"outer-right"!=b.position||(e.outernav=!0)}var m=c.data("index"),n=a.parent().find(f),o=n.find(g),p=o.find(h),q="horizontal"===b.direction?b.width*b.visibleAmount+b.space*(b.visibleAmount-1):b.width,r="horizontal"===b.direction?b.height:b.height*b.visibleAmount+b.space*(b.visibleAmount-1),s=b.tmp;jQuery.each(e.thumbs[c.index()].params,function(a,b){s=s.replace(b.from,b.to)}),p.append('<div data-liindex="'+c.index()+'" data-liref="'+m+'" class="justaddedthumb '+d+'" style="width:'+b.width+"px;height:"+b.height+'px;">'+s+"</div>");var u=n.find(".justaddedthumb"),v=n.find(i).length,w=u.outerWidth()+parseInt(void 0===b.space?0:b.space,0),x=u.outerHeight()+parseInt(void 0===b.space?0:b.space,0);u.find(j).css({backgroundImage:"url("+e.thumbs[c.index()].src+")"}),"vertical"===b.direction?(u.css({top:(v-1)*x+"px",left:"0px"}),p.css({height:(v-1)*x+u.outerHeight(),width:u.outerWidth()})):(u.css({left:(v-1)*w+"px",top:"0px"}),p.css({width:(v-1)*w+u.outerWidth(),height:u.outerHeight()})),n.data("maxw",q),n.data("maxh",r),n.data("wr_padding",b.wrapper_padding);var y="outer-top"===b.position||"outer-bottom"===b.position?"relative":"absolute";"outer-top"!==b.position&&"outer-bottom"!==b.position||b.h_align;o.css({maxWidth:q+"px",maxHeight:r+"px",overflow:"hidden",position:"relative"}),n.css({maxWidth:q+"px",maxHeight:r+"px",overflow:"visible",position:y,background:b.wrapper_color,padding:b.wrapper_padding+"px",boxSizing:"contet-box"}),u.click(function(){e.sc_indicator="bullet";var b=a.parent().find(h).data("distance");b=void 0===b?0:b,Math.abs(b)<10&&(a.revcallslidewithid(m),a.parent().find(f).removeClass("selected"),jQuery(this).addClass("selected"))}),u.removeClass("justaddedthumb"),b.opt=e,n.addClass("nav-pos-hor-"+b.h_align),n.addClass("nav-pos-ver-"+b.v_align),n.addClass("nav-dir-"+b.direction),t(n,b,e)},x=function(a){var b=a.c.parent().find(".outer-top"),c=a.c.parent().find(".outer-bottom");a.top_outer=b.hasClass("tp-forcenotvisible")?0:b.outerHeight()||0,a.bottom_outer=c.hasClass("tp-forcenotvisible")?0:c.outerHeight()||0},y=function(a,b,c,d){b>c||c>d?a.addClass("tp-forcenotvisible"):a.removeClass("tp-forcenotvisible")}}(jQuery);


// fin navigation 

/********************************************
 * REVOLUTION 5.4.5 EXTENSION - PARALLAX
 * @version: 2.2.3 (17.05.2017)
 * @requires jquery.themepunch.revolution.js
 * @author ThemePunch
*********************************************/
!function(a){"use strict";function e(a,b){a.lastscrolltop=b}var b=jQuery.fn.revolution,c=b.is_mobile(),d={alias:"Parallax Min JS",name:"revolution.extensions.parallax.min.js",min_core:"5.4.5",version:"2.2.3"};jQuery.extend(!0,b,{checkForParallax:function(a,e){function g(a){if("3D"==f.type||"3d"==f.type){a.find(".slotholder").wrapAll('<div class="dddwrapper" style="width:100%;height:100%;position:absolute;top:0px;left:0px;overflow:hidden"></div>'),a.find(".tp-parallax-wrap").wrapAll('<div class="dddwrapper-layer" style="width:100%;height:100%;position:absolute;top:0px;left:0px;z-index:5;overflow:'+f.ddd_layer_overflow+';"></div>'),a.find(".rs-parallaxlevel-tobggroup").closest(".tp-parallax-wrap").wrapAll('<div class="dddwrapper-layertobggroup" style="position:absolute;top:0px;left:0px;z-index:50;width:100%;height:100%"></div>');var b=a.find(".dddwrapper"),c=a.find(".dddwrapper-layer");a.find(".dddwrapper-layertobggroup").appendTo(b),"carousel"==e.sliderType&&("on"==f.ddd_shadow&&b.addClass("dddwrappershadow"),punchgs.TweenLite.set(b,{borderRadius:e.carousel.border_radius})),punchgs.TweenLite.set(a,{overflow:"visible",transformStyle:"preserve-3d",perspective:1600}),punchgs.TweenLite.set(b,{force3D:"auto",transformOrigin:"50% 50%"}),punchgs.TweenLite.set(c,{force3D:"auto",transformOrigin:"50% 50%",zIndex:5}),punchgs.TweenLite.set(e.ul,{transformStyle:"preserve-3d",transformPerspective:1600})}}if("stop"===b.compare_version(d).check)return!1;var f=e.parallax;if(!f.done){if(f.done=!0,c&&"on"==f.disable_onmobile)return!1;"3D"!=f.type&&"3d"!=f.type||(punchgs.TweenLite.set(e.c,{overflow:f.ddd_overflow}),punchgs.TweenLite.set(e.ul,{overflow:f.ddd_overflow}),"carousel"!=e.sliderType&&"on"==f.ddd_shadow&&(e.c.prepend('<div class="dddwrappershadow"></div>'),punchgs.TweenLite.set(e.c.find(".dddwrappershadow"),{force3D:"auto",transformPerspective:1600,transformOrigin:"50% 50%",width:"100%",height:"100%",position:"absolute",top:0,left:0,zIndex:0}))),e.li.each(function(){g(jQuery(this))}),("3D"==f.type||"3d"==f.type)&&e.c.find(".tp-static-layers").length>0&&(punchgs.TweenLite.set(e.c.find(".tp-static-layers"),{top:0,left:0,width:"100%",height:"100%"}),g(e.c.find(".tp-static-layers"))),f.pcontainers=new Array,f.pcontainer_depths=new Array,f.bgcontainers=new Array,f.bgcontainer_depths=new Array,e.c.find(".tp-revslider-slidesli .slotholder, .tp-revslider-slidesli .rs-background-video-layer").each(function(){var a=jQuery(this),b=a.data("bgparallax")||e.parallax.bgparallax;void 0!==(b="on"==b?1:b)&&"off"!==b&&(f.bgcontainers.push(a),f.bgcontainer_depths.push(e.parallax.levels[parseInt(b,0)-1]/100))});for(var h=1;h<=f.levels.length;h++)e.c.find(".rs-parallaxlevel-"+h).each(function(){var a=jQuery(this),b=a.closest(".tp-parallax-wrap");b.data("parallaxlevel",f.levels[h-1]),b.addClass("tp-parallax-container"),f.pcontainers.push(b),f.pcontainer_depths.push(f.levels[h-1])});"mouse"!=f.type&&"scroll+mouse"!=f.type&&"mouse+scroll"!=f.type&&"3D"!=f.type&&"3d"!=f.type||(a.mouseenter(function(b){var c=a.find(".active-revslide"),d=a.offset().top,e=a.offset().left,f=b.pageX-e,g=b.pageY-d;c.data("enterx",f),c.data("entery",g)}),a.on("mousemove.hoverdir, mouseleave.hoverdir, trigger3dpath",function(b,c){var d=c&&c.li?c.li:a.find(".active-revslide");if("enterpoint"==f.origo){var g=a.offset().top,h=a.offset().left;void 0==d.data("enterx")&&d.data("enterx",b.pageX-h),void 0==d.data("entery")&&d.data("entery",b.pageY-g);var i=d.data("enterx")||b.pageX-h,j=d.data("entery")||b.pageY-g,k=i-(b.pageX-h),l=j-(b.pageY-g),m=f.speed/1e3||.4}else var g=a.offset().top,h=a.offset().left,k=e.conw/2-(b.pageX-h),l=e.conh/2-(b.pageY-g),m=f.speed/1e3||3;"mouseleave"==b.type&&(k=f.ddd_lasth||0,l=f.ddd_lastv||0,m=1.5);for(var n=0;n<f.pcontainers.length;n++){var o=f.pcontainers[n],p=f.pcontainer_depths[n],q="3D"==f.type||"3d"==f.type?p/200:p/100,r=k*q,s=l*q;"scroll+mouse"==f.type||"mouse+scroll"==f.type?punchgs.TweenLite.to(o,m,{force3D:"auto",x:r,ease:punchgs.Power3.easeOut,overwrite:"all"}):punchgs.TweenLite.to(o,m,{force3D:"auto",x:r,y:s,ease:punchgs.Power3.easeOut,overwrite:"all"})}if("3D"==f.type||"3d"==f.type){var t=".tp-revslider-slidesli .dddwrapper, .dddwrappershadow, .tp-revslider-slidesli .dddwrapper-layer, .tp-static-layers .dddwrapper-layer";"carousel"===e.sliderType&&(t=".tp-revslider-slidesli .dddwrapper, .tp-revslider-slidesli .dddwrapper-layer, .tp-static-layers .dddwrapper-layer"),e.c.find(t).each(function(){var a=jQuery(this),c=f.levels[f.levels.length-1]/200,d=k*c,g=l*c,h=0==e.conw?0:Math.round(k/e.conw*c*100)||0,i=0==e.conh?0:Math.round(l/e.conh*c*100)||0,j=a.closest("li"),n=0,o=!1;a.hasClass("dddwrapper-layer")&&(n=f.ddd_z_correction||65,o=!0),a.hasClass("dddwrapper-layer")&&(d=0,g=0),j.hasClass("active-revslide")||"carousel"!=e.sliderType?"on"!=f.ddd_bgfreeze||o?punchgs.TweenLite.to(a,m,{rotationX:i,rotationY:-h,x:d,z:n,y:g,ease:punchgs.Power3.easeOut,overwrite:"all"}):punchgs.TweenLite.to(a,.5,{force3D:"auto",rotationY:0,rotationX:0,z:0,ease:punchgs.Power3.easeOut,overwrite:"all"}):punchgs.TweenLite.to(a,.5,{force3D:"auto",rotationY:0,x:0,y:0,rotationX:0,z:0,ease:punchgs.Power3.easeOut,overwrite:"all"}),"mouseleave"==b.type&&punchgs.TweenLite.to(jQuery(this),3.8,{z:0,ease:punchgs.Power3.easeOut})})}}),c&&(window.ondeviceorientation=function(b){var c=Math.round(b.beta||0)-70,d=Math.round(b.gamma||0),g=a.find(".active-revslide");if(jQuery(window).width()>jQuery(window).height()){var h=d;d=c,c=h}var i=a.width(),j=a.height(),k=360/i*d,l=180/j*c,m=f.speed/1e3||3,n=[];if(g.find(".tp-parallax-container").each(function(a){n.push(jQuery(this))}),a.find(".tp-static-layers .tp-parallax-container").each(function(){n.push(jQuery(this))}),jQuery.each(n,function(){var a=jQuery(this),b=parseInt(a.data("parallaxlevel"),0),c=b/100,d=k*c*2,e=l*c*4;punchgs.TweenLite.to(a,m,{force3D:"auto",x:d,y:e,ease:punchgs.Power3.easeOut,overwrite:"all"})}),"3D"==f.type||"3d"==f.type){var o=".tp-revslider-slidesli .dddwrapper, .dddwrappershadow, .tp-revslider-slidesli .dddwrapper-layer, .tp-static-layers .dddwrapper-layer";"carousel"===e.sliderType&&(o=".tp-revslider-slidesli .dddwrapper, .tp-revslider-slidesli .dddwrapper-layer, .tp-static-layers .dddwrapper-layer"),e.c.find(o).each(function(){var a=jQuery(this),c=f.levels[f.levels.length-1]/200,d=k*c,g=l*c*3,h=0==e.conw?0:Math.round(k/e.conw*c*500)||0,i=0==e.conh?0:Math.round(l/e.conh*c*700)||0,j=a.closest("li"),n=0,o=!1;a.hasClass("dddwrapper-layer")&&(n=f.ddd_z_correction||65,o=!0),a.hasClass("dddwrapper-layer")&&(d=0,g=0),j.hasClass("active-revslide")||"carousel"!=e.sliderType?"on"!=f.ddd_bgfreeze||o?punchgs.TweenLite.to(a,m,{rotationX:i,rotationY:-h,x:d,z:n,y:g,ease:punchgs.Power3.easeOut,overwrite:"all"}):punchgs.TweenLite.to(a,.5,{force3D:"auto",rotationY:0,rotationX:0,z:0,ease:punchgs.Power3.easeOut,overwrite:"all"}):punchgs.TweenLite.to(a,.5,{force3D:"auto",rotationY:0,z:0,x:0,y:0,rotationX:0,ease:punchgs.Power3.easeOut,overwrite:"all"}),"mouseleave"==b.type&&punchgs.TweenLite.to(jQuery(this),3.8,{z:0,ease:punchgs.Power3.easeOut})})}}));var i=e.scrolleffect;if(i.bgs=new Array,i.on){if("on"===i.on_slidebg)for(var h=0;h<e.allslotholder.length;h++)i.bgs.push(e.allslotholder[h]);i.multiplicator_layers=parseFloat(i.multiplicator_layers),i.multiplicator=parseFloat(i.multiplicator)}void 0!==i.layers&&0===i.layers.length&&(i.layers=!1),void 0!==i.bgs&&0===i.bgs.length&&(i.bgs=!1),b.scrollTicker(e,a)}},scrollTicker:function(a,d){1!=a.scrollTicker&&(a.scrollTicker=!0,c?(punchgs.TweenLite.ticker.fps(150),punchgs.TweenLite.ticker.addEventListener("tick",function(){b.scrollHandling(a)},d,!1,1)):document.addEventListener("scroll",function(c){b.scrollHandling(a,!0)},{passive:!0})),b.scrollHandling(a,!0)},scrollHandling:function(a,d,f){if(a.lastwindowheight=a.lastwindowheight||window.innerHeight,a.conh=0===a.conh||void 0===a.conh?a.infullscreenmode?a.minHeight:a.c.height():a.conh,a.lastscrolltop==window.scrollY&&!a.duringslidechange&&!d)return!1;punchgs.TweenLite.delayedCall(.2,e,[a,window.scrollY]);var g=a.c[0].getBoundingClientRect(),h=a.viewPort,i=a.parallax,j=g.top<0||g.height>a.lastwindowheight?g.top/g.height:g.bottom>a.lastwindowheight?(g.bottom-a.lastwindowheight)/g.height:0;if(a.scrollproc=j,b.callBackHandling&&b.callBackHandling(a,"parallax","start"),h.enable){var k=1-Math.abs(j);k=k<0?0:k,jQuery.isNumeric(h.visible_area)||-1!==h.visible_area.indexOf("%")&&(h.visible_area=parseInt(h.visible_area)/100),1-h.visible_area<=k?a.inviewport||(a.inviewport=!0,b.enterInViewPort(a)):a.inviewport&&(a.inviewport=!1,b.leaveViewPort(a))}if(c&&"on"==i.disable_onmobile)return!1;if("3d"!=i.type&&"3D"!=i.type){if(("scroll"==i.type||"scroll+mouse"==i.type||"mouse+scroll"==i.type)&&i.pcontainers)for(var l=0;l<i.pcontainers.length;l++)if(i.pcontainers[l].length>0){var m=i.pcontainers[l],n=i.pcontainer_depths[l]/100,o=Math.round(j*(-n*a.conh)*10)/10||0,p=void 0!==f?f:i.speedls/1e3||0;m.data("parallaxoffset",o),punchgs.TweenLite.to(m,p,{overwrite:"auto",force3D:"auto",y:o})}if(i.bgcontainers)for(var l=0;l<i.bgcontainers.length;l++){var q=i.bgcontainers[l],r=i.bgcontainer_depths[l],o=j*(-r*a.conh)||0,p=void 0!==f?f:i.speedbg/1e3||0;punchgs.TweenLite.to(q,p,{position:"absolute",top:"0px",left:"0px",backfaceVisibility:"hidden",force3D:"true",y:o+"px"})}}var s=a.scrolleffect;if(s.on&&("on"!==s.disable_on_mobile||!c)){var t=Math.abs(j)-s.tilt/100;if(t=t<0?0:t,!1!==s.layers){var u=1-t*s.multiplicator_layers,v={backfaceVisibility:"hidden",force3D:"true",z:.001,perspective:600};if("top"==s.direction&&j>=0&&(u=1),"bottom"==s.direction&&j<=0&&(u=1),u=u>1?1:u<0?0:u,"on"===s.fade&&(v.opacity=u),"on"===s.scale){var w=u;v.scale=1-w+1}if("on"===s.blur){var x=(1-u)*s.maxblur;v["-webkit-filter"]="blur("+x+"px)",v.filter="blur("+x+"px)"}if("on"===s.grayscale){var y=100*(1-u),z="grayscale("+y+"%)";v["-webkit-filter"]=void 0===v["-webkit-filter"]?z:v["-webkit-filter"]+" "+z,v.filter=void 0===v.filter?z:v.filter+" "+z}punchgs.TweenLite.set(s.layers,v)}if(!1!==s.bgs){var u=1-t*s.multiplicator,v={backfaceVisibility:"hidden",force3D:"true"};if("top"==s.direction&&j>=0&&(u=1),"bottom"==s.direction&&j<=0&&(u=1),u=u>1?1:u<0?0:u,"on"===s.fade&&(v.opacity=u),"on"===s.scale){var w=u;punchgs.TweenLite.set(jQuery(".tp-kbimg-wrap"),{transformOrigin:"50% 50%",scale:w,force3D:!0})}if("on"===s.blur){var x=(1-u)*s.maxblur;v["-webkit-filter"]="blur("+x+"px)",v.filter="blur("+x+"px)"}if("on"===s.grayscale){var y=100*(1-u),z="grayscale("+y+"%)";v["-webkit-filter"]=void 0===v["-webkit-filter"]?z:v["-webkit-filter"]+" "+z,v.filter=void 0===v.filter?z:v.filter+" "+z}punchgs.TweenLite.set(s.bgs,v)}}b.callBackHandling&&b.callBackHandling(a,"parallax","end")}})}(jQuery);


// fin parallax 


var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
var htmlDivCss = "";
if (htmlDiv) {
htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
} else {
var htmlDiv = document.createElement("div");
htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
}



var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
var htmlDivCss = "";
if (htmlDiv) {
htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
} else {
var htmlDiv = document.createElement("div");
htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
}




setREVStartSize({
c: jQuery('#rev_slider_1_1'),
responsiveLevels: [1240, 1024, 778, 480],
gridwidth: [1240, 1024, 778, 480],
gridheight: [741, 768, 960, 720],
sliderLayout: 'fullwidth'
});

var revapi1,
tpj = jQuery;

tpj(document).ready(function() {
if (tpj("#rev_slider_1_1").revolution == undefined) {
revslider_showDoubleJqueryError("#rev_slider_1_1");
} else {
revapi1 = tpj("#rev_slider_1_1").show().revolution({
sliderType: "standard",
jsFileLocation: "//codex-themes.com/thegem/agency-one-pager/wp-content/plugins/revslider/public/assets/js/",
sliderLayout: "fullwidth",
dottedOverlay: "none",
delay: 9000,
navigation: {
keyboardNavigation: "off",
keyboard_direction: "horizontal",
mouseScrollNavigation: "off",
mouseScrollReverse: "default",
onHoverStop: "off",
touch: {
touchenabled: "on",
touchOnDesktop: "off",
swipe_threshold: 75,
swipe_min_touches: 1,
swipe_direction: "horizontal",
drag_block_vertical: false
},
bullets: {
enable: true,
hide_onmobile: false,
style: "custom",
hide_onleave: false,
direction: "horizontal",
h_align: "right",
v_align: "bottom",
h_offset: 500,
v_offset: 50,
space: 20,
tmp: ''
}
},
responsiveLevels: [1240, 1024, 778, 480],
visibilityLevels: [1240, 1024, 778, 480],
gridwidth: [1240, 1024, 778, 480],
gridheight: [741, 768, 960, 720],
lazyType: "none",
parallax: {
type: "scroll",
origo: "slidercenter",
speed: 400,
speedbg: 0,
speedls: 0,
levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 46, 47, 48, 49, 50, 51, 55],
},
shadow: 0,
spinner: "off",
stopLoop: "off",
stopAfterLoops: -1,
stopAtSlide: -1,
shuffle: "off",
autoHeight: "off",
disableProgressBar: "on",
hideThumbsOnMobile: "off",
hideSliderAtLimit: 0,
hideCaptionAtLimit: 0,
hideAllCaptionAtLilmit: 0,
debugMode: false,
fallbacks: {
simplifyAll: "off",
nextSlideOnWindowFocus: "off",
disableFocusListener: false,
}
});
}

}); /*ready*/



var htmlDivCss = unescape("body%20.slideshow-preloader%20%7B%0A%09height%3A%20741px%3B%0A%7D%0A.fa-icon-chevron-right%3Abefore%20%7B%0A%20%20%20%20content%3A%20%22%5Ce638%22%20%21important%3B%0A%20%20%09font-family%3A%20thegem-icons%3B%0A%20%20%20%20font-size%3A%2020px%3B%0A%20%20%20%20margin-left%3A%2020px%3B%0A%20%20%20%20margin-top%3A%209px%3B%0A%20%20%20%20vertical-align%3A%20bottom%3B%0A%7D%0A.rev-btn%20%7B%0A%20%20%20%20%20background-color%3A%20#FFC107important%3B%0A%20%20%09-o-transition%3A%20all%200.2s%20linear%3B%0A%09-webkit-transition%3A%20all%200.2s%20linear%3B%0A%09transition%3A%20all%200.2s%20linear%3B%0Abox-shadow%3A%200%2012px%2040px%20rgba%28231%2C%20255%2C%20137%2C%200.3%29%3B%0A%20%20%7D%0A%20%20.rev-btn%3Ahover%20%7B%0A%20%20%20%20%20background-color%3A%20%23fff%20%21important%3B%0A%20%20%09-o-transition%3A%20all%200.2s%20linear%3B%0A%09-webkit-transition%3A%20all%200.2s%20linear%3B%0A%09transition%3A%20all%200.2s%20linear%3B%0Abox-shadow%3A%200%2012px%2040px%20rgba%28231%2C%20255%2C%20137%2C%200.3%29%3B%0A%20%20%7D%0A%20%20.custom%20.tp-bullet%2C%20%0A%09.custom%20.tp-bullet.selected%20%7B%0A%20%20%20%20%20margin-top%3A%20-10px%3B%0A%20%20%20%20%09margin-left%3A%205px%3B%0A%09padding-top%3A%2050px%3B%0A%20%20%20%20-moz-transform%3A%20rotate%2833deg%29%3B%20%2F%2A%20%D0%94%D0%BB%D1%8F%20Firefox%20%2A%2F%0A%20%20%20%20-ms-transform%3A%20rotate%2833deg%29%3B%20%2F%2A%20%D0%94%D0%BB%D1%8F%20IE%20%2A%2F%0A%20%20%20%20-webkit-transform%3A%20rotate%2833deg%29%3B%20%2F%2A%20%D0%94%D0%BB%D1%8F%20Safari%2C%20Chrome%2C%20iOS%20%2A%2F%0A%20%20%20%20-o-transform%3A%20rotate%2833deg%29%3B%20%2F%2A%20%D0%94%D0%BB%D1%8F%20Opera%20%2A%2F%0A%20%20%20%20transform%3A%20rotate%2833deg%29%3B%0A%20%20%20%7D%0A%0A%0A.custom%20.tp-bullet%3Ahover%2C%20.custom%20.tp-bullet.selected%20%7B%0A%20%20%20%20background%3A%20%23e7ff89%20%21important%3B%0A%09-o-transition%3A%20all%200.2s%20linear%3B%0A%09-webkit-transition%3A%20all%200.2s%20linear%3B%0A%09transition%3A%20all%200.2s%20linear%3B%0A%7D%0A.custom%20.tp-bullet%20%7B%0A%20%20%20%20background%3A%20%2356575f%20%21important%3B%0A%09-o-transition%3A%20all%200.2s%20linear%3B%0A%09-webkit-transition%3A%20all%200.2s%20linear%3B%0A%09transition%3A%20all%200.2s%20linear%3B%0A%7D%0A%20%20%0A%0A%20%0A%20%20%0A%20%20.tp-bullets.custom.nav-dir-horizontal%20%7B%0A%20%20%20%20overflow%3A%20hidden%3B%0A%20%20%20%20width%3A%20100px%20%21important%3B%0A%20%20%20%20height%3A%2028px%20%21important%3B%0A%20%20%20%20%20%20display%3A%20block%20%21important%3B%0A%7D");
var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
if (htmlDiv) {
htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
} else {
var htmlDiv = document.createElement('div');
htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
}



var htmlDivCss = unescape(".custom.tp-bullets%20%7B%0A%7D%0A.custom.tp-bullets%3Abefore%20%7B%0A%09content%3A%22%20%22%3B%0A%09position%3Aabsolute%3B%0A%09width%3A100%25%3B%0A%09height%3A100%25%3B%0A%09background%3Atransparent%3B%0A%09padding%3A10px%3B%0A%09margin-left%3A-10px%3Bmargin-top%3A-10px%3B%0A%09box-sizing%3Acontent-box%3B%0A%7D%0A.custom%20.tp-bullet%20%7B%0A%09width%3A12px%3B%0A%09height%3A12px%3B%0A%09position%3Aabsolute%3B%0A%09background%3A%23aaa%3B%0A%20%20%20%20background%3Argba%28125%2C125%2C125%2C0.5%29%3B%0A%09cursor%3A%20pointer%3B%0A%09box-sizing%3Acontent-box%3B%0A%7D%0A.custom%20.tp-bullet%3Ahover%2C%0A.custom%20.tp-bullet.selected%20%7B%0A%09background%3Argb%28125%2C125%2C125%29%3B%0A%7D%0A.custom%20.tp-bullet-image%20%7B%0A%7D%0A.custom%20.tp-bullet-title%20%7B%0A%7D%0A%0A");
var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
if (htmlDiv) {
htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
} else {
var htmlDiv = document.createElement('div');
htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
}



if (typeof(gem_fix_fullwidth_position) == "function") {
gem_fix_fullwidth_position(document.getElementById("fullwidth-block-5a1be031bccf0"));
}



if ( self !== top ) {
 setTimeout(function(){
 $('.defaultimg').css('transform', '');
  $('.defaultimg').css('opacity', 1);
  $('.defaultimg').css('visibility','visible');
 },1500);
}
