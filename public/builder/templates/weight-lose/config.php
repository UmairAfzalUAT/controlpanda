<?php 

return array(
	'name' => 'Weightloss',
	'color' => 'yellow',
	'category' => 'Weightloss Landing Page',
	'theme'  => 'Weightloss ',
	'libraries' => array('Classie', 'Animated Header', 'Bootstrap Validation', 'Jquery Easing'),
);