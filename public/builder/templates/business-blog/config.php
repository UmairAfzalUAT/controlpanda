<?php 

return array(
	'name' => 'Business Blog',
	'color' => 'blue',
	'category' => 'Business Blog',
	'theme'  => 'Business Blog',
	'libraries' => array('Classie', 'Animated Header', 'Bootstrap Validation', 'Jquery Easing'),
);