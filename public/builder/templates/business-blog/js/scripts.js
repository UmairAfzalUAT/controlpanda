if (function (t, e) {
        "object" == typeof module && "object" == typeof module.exports ? module.exports = t.document ? e(t, !0) : function (t) {
            if (!t.document) throw new Error("jQuery requires a window with a document");
            return e(t)
        } : e(t)
    }("undefined" != typeof window ? window : this, function (t, e) {
        function i(t) {
            var e = !!t && "length" in t && t.length,
                i = tt.type(t);
            return "function" !== i && !tt.isWindow(t) && ("array" === i || 0 === e || "number" == typeof e && e > 0 && e - 1 in t)
        }

        function n(t, e, i) {
            if (tt.isFunction(e)) return tt.grep(t, function (t, n) {
                return !!e.call(t, n, t) !== i
            });
            if (e.nodeType) return tt.grep(t, function (t) {
                return t === e !== i
            });
            if ("string" == typeof e) {
                if (dt.test(e)) return tt.filter(e, t, i);
                e = tt.filter(e, t)
            }
            return tt.grep(t, function (t) {
                return G.call(e, t) > -1 !== i
            })
        }

        function s(t, e) {
            for (;
                (t = t[e]) && 1 !== t.nodeType;);
            return t
        }

        function r() {
            F.removeEventListener("DOMContentLoaded", r), t.removeEventListener("load", r), tt.ready()
        }

        function o() {
            this.expando = tt.expando + o.uid++
        }

        function a(t, e, i) {
            var n;
            if (void 0 === i && 1 === t.nodeType)
                if (n = "data-" + e.replace(Ct, "-$&").toLowerCase(), "string" == typeof (i = t.getAttribute(n))) {
                    try {
                        i = "true" === i || "false" !== i && ("null" === i ? null : +i + "" === i ? +i : xt.test(i) ? tt.parseJSON(i) : i)
                    } catch (t) {}
                    wt.set(t, e, i)
                } else i = void 0;
            return i
        }

        function l(t, e, i, n) {
            var s, r = 1,
                o = 20,
                a = n ? function () {
                    return n.cur()
                } : function () {
                    return tt.css(t, e, "")
                },
                l = a(),
                h = i && i[3] || (tt.cssNumber[e] ? "" : "px"),
                d = (tt.cssNumber[e] || "px" !== h && +l) && Et.exec(tt.css(t, e));
            if (d && d[3] !== h) {
                h = h || d[3], i = i || [], d = +l || 1;
                do {
                    r = r || ".5", d /= r, tt.style(t, e, d + h)
                } while (r !== (r = a() / l) && 1 !== r && --o)
            }
            return i && (d = +d || +l || 0, s = i[1] ? d + (i[1] + 1) * i[2] : +i[2], n && (n.unit = h, n.start = d, n.end = s)), s
        }

        function h(t, e) {
            var i = void 0 !== t.getElementsByTagName ? t.getElementsByTagName(e || "*") : void 0 !== t.querySelectorAll ? t.querySelectorAll(e || "*") : [];
            return void 0 === e || e && tt.nodeName(t, e) ? tt.merge([t], i) : i
        }

        function d(t, e) {
            for (var i = 0, n = t.length; n > i; i++) bt.set(t[i], "globalEval", !e || bt.get(e[i], "globalEval"))
        }

        function c(t, e, i, n, s) {
            for (var r, o, a, l, c, u, p = e.createDocumentFragment(), f = [], m = 0, g = t.length; g > m; m++)
                if ((r = t[m]) || 0 === r)
                    if ("object" === tt.type(r)) tt.merge(f, r.nodeType ? [r] : r);
                    else if (Dt.test(r)) {
                for (o = o || p.appendChild(e.createElement("div")), a = ($t.exec(r) || ["", ""])[1].toLowerCase(), l = Mt[a] || Mt._default, o.innerHTML = l[1] + tt.htmlPrefilter(r) + l[2], u = l[0]; u--;) o = o.lastChild;
                tt.merge(f, o.childNodes), (o = p.firstChild).textContent = ""
            } else f.push(e.createTextNode(r));
            for (p.textContent = "", m = 0; r = f[m++];)
                if (n && tt.inArray(r, n) > -1) s && s.push(r);
                else if (c = tt.contains(r.ownerDocument, r), o = h(p.appendChild(r), "script"), c && d(o), i)
                for (u = 0; r = o[u++];) zt.test(r.type || "") && i.push(r);
            return p
        }

        function u() {
            return !0
        }

        function p() {
            return !1
        }

        function f() {
            try {
                return F.activeElement
            } catch (t) {}
        }

        function m(t, e, i, n, s, r) {
            var o, a;
            if ("object" == typeof e) {
                "string" != typeof i && (n = n || i, i = void 0);
                for (a in e) m(t, a, i, n, e[a], r);
                return t
            }
            if (null == n && null == s ? (s = i, n = i = void 0) : null == s && ("string" == typeof i ? (s = n, n = void 0) : (s = n, n = i, i = void 0)), !1 === s) s = p;
            else if (!s) return t;
            return 1 === r && (o = s, s = function (t) {
                return tt().off(t), o.apply(this, arguments)
            }, s.guid = o.guid || (o.guid = tt.guid++)), t.each(function () {
                tt.event.add(this, e, s, n, i)
            })
        }

        function g(t, e) {
            return tt.nodeName(t, "table") && tt.nodeName(11 !== e.nodeType ? e : e.firstChild, "tr") ? t.getElementsByTagName("tbody")[0] || t.appendChild(t.ownerDocument.createElement("tbody")) : t
        }

        function v(t) {
            return t.type = (null !== t.getAttribute("type")) + "/" + t.type, t
        }

        function y(t) {
            var e = jt.exec(t.type);
            return e ? t.type = e[1] : t.removeAttribute("type"), t
        }

        function b(t, e) {
            var i, n, s, r, o, a, l, h;
            if (1 === e.nodeType) {
                if (bt.hasData(t) && (r = bt.access(t), o = bt.set(e, r), h = r.events)) {
                    delete o.handle, o.events = {};
                    for (s in h)
                        for (i = 0, n = h[s].length; n > i; i++) tt.event.add(e, s, h[s][i])
                }
                wt.hasData(t) && (a = wt.access(t), l = tt.extend({}, a), wt.set(e, l))
            }
        }

        function w(t, e) {
            var i = e.nodeName.toLowerCase();
            "input" === i && kt.test(t.type) ? e.checked = t.checked : "input" !== i && "textarea" !== i || (e.defaultValue = t.defaultValue)
        }

        function x(t, e, i, n) {
            e = X.apply([], e);
            var s, r, o, a, l, d, u = 0,
                p = t.length,
                f = p - 1,
                m = e[0],
                g = tt.isFunction(m);
            if (g || p > 1 && "string" == typeof m && !Z.checkClone && At.test(m)) return t.each(function (s) {
                var r = t.eq(s);
                g && (e[0] = m.call(this, s, r.html())), x(r, e, i, n)
            });
            if (p && (s = c(e, t[0].ownerDocument, !1, t, n), r = s.firstChild, 1 === s.childNodes.length && (s = r), r || n)) {
                for (a = (o = tt.map(h(s, "script"), v)).length; p > u; u++) l = s, u !== f && (l = tt.clone(l, !0, !0), a && tt.merge(o, h(l, "script"))), i.call(t[u], l, u);
                if (a)
                    for (d = o[o.length - 1].ownerDocument, tt.map(o, y), u = 0; a > u; u++) l = o[u], zt.test(l.type || "") && !bt.access(l, "globalEval") && tt.contains(d, l) && (l.src ? tt._evalUrl && tt._evalUrl(l.src) : tt.globalEval(l.textContent.replace(Ht, "")))
            }
            return t
        }

        function C(t, e, i) {
            for (var n, s = e ? tt.filter(e, t) : t, r = 0; null != (n = s[r]); r++) i || 1 !== n.nodeType || tt.cleanData(h(n)), n.parentNode && (i && tt.contains(n.ownerDocument, n) && d(h(n, "script")), n.parentNode.removeChild(n));
            return t
        }

        function T(t, e) {
            var i = tt(e.createElement(t)).appendTo(e.body),
                n = tt.css(i[0], "display");
            return i.detach(), n
        }

        function E(t) {
            var e = F,
                i = Wt[t];
            return i || ("none" !== (i = T(t, e)) && i || (Rt = (Rt || tt("<iframe frameborder='0' width='0' height='0'/>")).appendTo(e.documentElement), (e = Rt[0].contentDocument).write(), e.close(), i = T(t, e), Rt.detach()), Wt[t] = i), i
        }

        function S(t, e, i) {
            var n, s, r, o, a = t.style;
            return i = i || Ft(t), "" !== (o = i ? i.getPropertyValue(e) || i[e] : void 0) && void 0 !== o || tt.contains(t.ownerDocument, t) || (o = tt.style(t, e)), i && !Z.pixelMarginRight() && qt.test(o) && Bt.test(e) && (n = a.width, s = a.minWidth, r = a.maxWidth, a.minWidth = a.maxWidth = a.width = o, o = i.width, a.width = n, a.minWidth = s, a.maxWidth = r), void 0 !== o ? o + "" : o
        }

        function _(t, e) {
            return {
                get: function () {
                    return t() ? void delete this.get : (this.get = e).apply(this, arguments)
                }
            }
        }

        function k(t) {
            if (t in Kt) return t;
            for (var e = t[0].toUpperCase() + t.slice(1), i = Qt.length; i--;)
                if ((t = Qt[i] + e) in Kt) return t
        }

        function $(t, e, i) {
            var n = Et.exec(e);
            return n ? Math.max(0, n[2] - (i || 0)) + (n[3] || "px") : e
        }

        function z(t, e, i, n, s) {
            for (var r = i === (n ? "border" : "content") ? 4 : "width" === e ? 1 : 0, o = 0; 4 > r; r += 2) "margin" === i && (o += tt.css(t, i + St[r], !0, s)), n ? ("content" === i && (o -= tt.css(t, "padding" + St[r], !0, s)), "margin" !== i && (o -= tt.css(t, "border" + St[r] + "Width", !0, s))) : (o += tt.css(t, "padding" + St[r], !0, s), "padding" !== i && (o += tt.css(t, "border" + St[r] + "Width", !0, s)));
            return o
        }

        function M(e, i, n) {
            var s = !0,
                r = "width" === i ? e.offsetWidth : e.offsetHeight,
                o = Ft(e),
                a = "border-box" === tt.css(e, "boxSizing", !1, o);
            if (F.msFullscreenElement && t.top !== t && e.getClientRects().length && (r = Math.round(100 * e.getBoundingClientRect()[i])), 0 >= r || null == r) {
                if ((0 > (r = S(e, i, o)) || null == r) && (r = e.style[i]), qt.test(r)) return r;
                s = a && (Z.boxSizingReliable() || r === e.style[i]), r = parseFloat(r) || 0
            }
            return r + z(e, i, n || (a ? "border" : "content"), s, o) + "px"
        }

        function D(t, e) {
            for (var i, n, s, r = [], o = 0, a = t.length; a > o; o++)(n = t[o]).style && (r[o] = bt.get(n, "olddisplay"), i = n.style.display, e ? (r[o] || "none" !== i || (n.style.display = ""), "" === n.style.display && _t(n) && (r[o] = bt.access(n, "olddisplay", E(n.nodeName)))) : (s = _t(n), "none" === i && s || bt.set(n, "olddisplay", s ? i : tt.css(n, "display"))));
            for (o = 0; a > o; o++)(n = t[o]).style && (e && "none" !== n.style.display && "" !== n.style.display || (n.style.display = e ? r[o] || "" : "none"));
            return t
        }

        function I(t, e, i, n, s) {
            return new I.prototype.init(t, e, i, n, s)
        }

        function P() {
            return t.setTimeout(function () {
                Zt = void 0
            }), Zt = tt.now()
        }

        function N(t, e) {
            var i, n = 0,
                s = {
                    height: t
                };
            for (e = e ? 1 : 0; 4 > n; n += 2 - e) i = St[n], s["margin" + i] = s["padding" + i] = t;
            return e && (s.opacity = s.width = t), s
        }

        function L(t, e, i) {
            for (var n, s = (O.tweeners[e] || []).concat(O.tweeners["*"]), r = 0, o = s.length; o > r; r++)
                if (n = s[r].call(i, e, t)) return n
        }

        function O(t, e, i) {
            var n, s, r = 0,
                o = O.prefilters.length,
                a = tt.Deferred().always(function () {
                    delete l.elem
                }),
                l = function () {
                    if (s) return !1;
                    for (var e = Zt || P(), i = Math.max(0, h.startTime + h.duration - e), n = 1 - (i / h.duration || 0), r = 0, o = h.tweens.length; o > r; r++) h.tweens[r].run(n);
                    return a.notifyWith(t, [h, n, i]), 1 > n && o ? i : (a.resolveWith(t, [h]), !1)
                },
                h = a.promise({
                    elem: t,
                    props: tt.extend({}, e),
                    opts: tt.extend(!0, {
                        specialEasing: {},
                        easing: tt.easing._default
                    }, i),
                    originalProperties: e,
                    originalOptions: i,
                    startTime: Zt || P(),
                    duration: i.duration,
                    tweens: [],
                    createTween: function (e, i) {
                        var n = tt.Tween(t, h.opts, e, i, h.opts.specialEasing[e] || h.opts.easing);
                        return h.tweens.push(n), n
                    },
                    stop: function (e) {
                        var i = 0,
                            n = e ? h.tweens.length : 0;
                        if (s) return this;
                        for (s = !0; n > i; i++) h.tweens[i].run(1);
                        return e ? (a.notifyWith(t, [h, 1, 0]), a.resolveWith(t, [h, e])) : a.rejectWith(t, [h, e]), this
                    }
                }),
                d = h.props;
            for (function (t, e) {
                    var i, n, s, r, o;
                    for (i in t)
                        if (n = tt.camelCase(i), s = e[n], r = t[i], tt.isArray(r) && (s = r[1], r = t[i] = r[0]), i !== n && (t[n] = r, delete t[i]), (o = tt.cssHooks[n]) && "expand" in o) {
                            r = o.expand(r), delete t[n];
                            for (i in r) i in t || (t[i] = r[i], e[i] = s)
                        } else e[n] = s
                }(d, h.opts.specialEasing); o > r; r++)
                if (n = O.prefilters[r].call(h, t, d, h.opts)) return tt.isFunction(n.stop) && (tt._queueHooks(h.elem, h.opts.queue).stop = tt.proxy(n.stop, n)), n;
            return tt.map(d, L, h), tt.isFunction(h.opts.start) && h.opts.start.call(t, h), tt.fx.timer(tt.extend(l, {
                elem: t,
                anim: h,
                queue: h.opts.queue
            })), h.progress(h.opts.progress).done(h.opts.done, h.opts.complete).fail(h.opts.fail).always(h.opts.always)
        }

        function A(t) {
            return t.getAttribute && t.getAttribute("class") || ""
        }

        function j(t) {
            return function (e, i) {
                "string" != typeof e && (i = e, e = "*");
                var n, s = 0,
                    r = e.toLowerCase().match(mt) || [];
                if (tt.isFunction(i))
                    for (; n = r[s++];) "+" === n[0] ? (n = n.slice(1) || "*", (t[n] = t[n] || []).unshift(i)) : (t[n] = t[n] || []).push(i)
            }
        }

        function H(t, e, i, n) {
            function s(a) {
                var l;
                return r[a] = !0, tt.each(t[a] || [], function (t, a) {
                    var h = a(e, i, n);
                    return "string" != typeof h || o || r[h] ? o ? !(l = h) : void 0 : (e.dataTypes.unshift(h), s(h), !1)
                }), l
            }
            var r = {},
                o = t === be;
            return s(e.dataTypes[0]) || !r["*"] && s("*")
        }

        function R(t, e) {
            var i, n, s = tt.ajaxSettings.flatOptions || {};
            for (i in e) void 0 !== e[i] && ((s[i] ? t : n || (n = {}))[i] = e[i]);
            return n && tt.extend(!0, t, n), t
        }

        function W(t, e, i, n) {
            var s;
            if (tt.isArray(e)) tt.each(e, function (e, s) {
                i || Te.test(t) ? n(t, s) : W(t + "[" + ("object" == typeof s && null != s ? e : "") + "]", s, i, n)
            });
            else if (i || "object" !== tt.type(e)) n(t, e);
            else
                for (s in e) W(t + "[" + s + "]", e[s], i, n)
        }

        function B(t) {
            return tt.isWindow(t) ? t : 9 === t.nodeType && t.defaultView
        }
        var q = [],
            F = t.document,
            U = q.slice,
            X = q.concat,
            Y = q.push,
            G = q.indexOf,
            V = {},
            Q = V.toString,
            K = V.hasOwnProperty,
            Z = {},
            J = "2.2.2",
            tt = function (t, e) {
                return new tt.fn.init(t, e)
            },
            et = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
            it = /^-ms-/,
            nt = /-([\da-z])/gi,
            st = function (t, e) {
                return e.toUpperCase()
            };
        tt.fn = tt.prototype = {
            jquery: J,
            constructor: tt,
            selector: "",
            length: 0,
            toArray: function () {
                return U.call(this)
            },
            get: function (t) {
                return null != t ? 0 > t ? this[t + this.length] : this[t] : U.call(this)
            },
            pushStack: function (t) {
                var e = tt.merge(this.constructor(), t);
                return e.prevObject = this, e.context = this.context, e
            },
            each: function (t) {
                return tt.each(this, t)
            },
            map: function (t) {
                return this.pushStack(tt.map(this, function (e, i) {
                    return t.call(e, i, e)
                }))
            },
            slice: function () {
                return this.pushStack(U.apply(this, arguments))
            },
            first: function () {
                return this.eq(0)
            },
            last: function () {
                return this.eq(-1)
            },
            eq: function (t) {
                var e = this.length,
                    i = +t + (0 > t ? e : 0);
                return this.pushStack(i >= 0 && e > i ? [this[i]] : [])
            },
            end: function () {
                return this.prevObject || this.constructor()
            },
            push: Y,
            sort: q.sort,
            splice: q.splice
        }, tt.extend = tt.fn.extend = function () {
            var t, e, i, n, s, r, o = arguments[0] || {},
                a = 1,
                l = arguments.length,
                h = !1;
            for ("boolean" == typeof o && (h = o, o = arguments[a] || {}, a++), "object" == typeof o || tt.isFunction(o) || (o = {}), a === l && (o = this, a--); l > a; a++)
                if (null != (t = arguments[a]))
                    for (e in t) i = o[e], n = t[e], o !== n && (h && n && (tt.isPlainObject(n) || (s = tt.isArray(n))) ? (s ? (s = !1, r = i && tt.isArray(i) ? i : []) : r = i && tt.isPlainObject(i) ? i : {}, o[e] = tt.extend(h, r, n)) : void 0 !== n && (o[e] = n));
            return o
        }, tt.extend({
            expando: "jQuery" + (J + Math.random()).replace(/\D/g, ""),
            isReady: !0,
            error: function (t) {
                throw new Error(t)
            },
            noop: function () {},
            isFunction: function (t) {
                return "function" === tt.type(t)
            },
            isArray: Array.isArray,
            isWindow: function (t) {
                return null != t && t === t.window
            },
            isNumeric: function (t) {
                var e = t && t.toString();
                return !tt.isArray(t) && e - parseFloat(e) + 1 >= 0
            },
            isPlainObject: function (t) {
                var e;
                if ("object" !== tt.type(t) || t.nodeType || tt.isWindow(t)) return !1;
                if (t.constructor && !K.call(t, "constructor") && !K.call(t.constructor.prototype || {}, "isPrototypeOf")) return !1;
                for (e in t);
                return void 0 === e || K.call(t, e)
            },
            isEmptyObject: function (t) {
                var e;
                for (e in t) return !1;
                return !0
            },
            type: function (t) {
                return null == t ? t + "" : "object" == typeof t || "function" == typeof t ? V[Q.call(t)] || "object" : typeof t
            },
            globalEval: function (t) {
                var e, i = eval;
                (t = tt.trim(t)) && (1 === t.indexOf("use strict") ? (e = F.createElement("script"), e.text = t, F.head.appendChild(e).parentNode.removeChild(e)) : i(t))
            },
            camelCase: function (t) {
                return t.replace(it, "ms-").replace(nt, st)
            },
            nodeName: function (t, e) {
                return t.nodeName && t.nodeName.toLowerCase() === e.toLowerCase()
            },
            each: function (t, e) {
                var n, s = 0;
                if (i(t))
                    for (n = t.length; n > s && !1 !== e.call(t[s], s, t[s]); s++);
                else
                    for (s in t)
                        if (!1 === e.call(t[s], s, t[s])) break;
                return t
            },
            trim: function (t) {
                return null == t ? "" : (t + "").replace(et, "")
            },
            makeArray: function (t, e) {
                var n = e || [];
                return null != t && (i(Object(t)) ? tt.merge(n, "string" == typeof t ? [t] : t) : Y.call(n, t)), n
            },
            inArray: function (t, e, i) {
                return null == e ? -1 : G.call(e, t, i)
            },
            merge: function (t, e) {
                for (var i = +e.length, n = 0, s = t.length; i > n; n++) t[s++] = e[n];
                return t.length = s, t
            },
            grep: function (t, e, i) {
                for (var n = [], s = 0, r = t.length, o = !i; r > s; s++) !e(t[s], s) !== o && n.push(t[s]);
                return n
            },
            map: function (t, e, n) {
                var s, r, o = 0,
                    a = [];
                if (i(t))
                    for (s = t.length; s > o; o++) null != (r = e(t[o], o, n)) && a.push(r);
                else
                    for (o in t) null != (r = e(t[o], o, n)) && a.push(r);
                return X.apply([], a)
            },
            guid: 1,
            proxy: function (t, e) {
                var i, n, s;
                return "string" == typeof e && (i = t[e], e = t, t = i), tt.isFunction(t) ? (n = U.call(arguments, 2), s = function () {
                    return t.apply(e || this, n.concat(U.call(arguments)))
                }, s.guid = t.guid = t.guid || tt.guid++, s) : void 0
            },
            now: Date.now,
            support: Z
        }), "function" == typeof Symbol && (tt.fn[Symbol.iterator] = q[Symbol.iterator]), tt.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function (t, e) {
            V["[object " + e + "]"] = e.toLowerCase()
        });
        var rt = function (t) {
            function e(t, e, i, n) {
                var s, r, o, a, l, h, c, p, f = e && e.ownerDocument,
                    m = e ? e.nodeType : 9;
                if (i = i || [], "string" != typeof t || !t || 1 !== m && 9 !== m && 11 !== m) return i;
                if (!n && ((e ? e.ownerDocument || e : H) !== D && M(e), e = e || D, P)) {
                    if (11 !== m && (h = gt.exec(t)))
                        if (s = h[1]) {
                            if (9 === m) {
                                if (!(o = e.getElementById(s))) return i;
                                if (o.id === s) return i.push(o), i
                            } else if (f && (o = f.getElementById(s)) && A(e, o) && o.id === s) return i.push(o), i
                        } else {
                            if (h[2]) return K.apply(i, e.getElementsByTagName(t)), i;
                            if ((s = h[3]) && w.getElementsByClassName && e.getElementsByClassName) return K.apply(i, e.getElementsByClassName(s)), i
                        }
                    if (w.qsa && !F[t + " "] && (!N || !N.test(t))) {
                        if (1 !== m) f = e, p = t;
                        else if ("object" !== e.nodeName.toLowerCase()) {
                            for ((a = e.getAttribute("id")) ? a = a.replace(yt, "\\$&") : e.setAttribute("id", a = j), r = (c = E(t)).length, l = ct.test(a) ? "#" + a : "[id='" + a + "']"; r--;) c[r] = l + " " + u(c[r]);
                            p = c.join(","), f = vt.test(t) && d(e.parentNode) || e
                        }
                        if (p) try {
                            return K.apply(i, f.querySelectorAll(p)), i
                        } catch (t) {} finally {
                            a === j && e.removeAttribute("id")
                        }
                    }
                }
                return _(t.replace(ot, "$1"), e, i, n)
            }

            function i() {
                function t(i, n) {
                    return e.push(i + " ") > x.cacheLength && delete t[e.shift()], t[i + " "] = n
                }
                var e = [];
                return t
            }

            function n(t) {
                return t[j] = !0, t
            }

            function s(t) {
                var e = D.createElement("div");
                try {
                    return !!t(e)
                } catch (t) {
                    return !1
                } finally {
                    e.parentNode && e.parentNode.removeChild(e), e = null
                }
            }

            function r(t, e) {
                for (var i = t.split("|"), n = i.length; n--;) x.attrHandle[i[n]] = e
            }

            function o(t, e) {
                var i = e && t,
                    n = i && 1 === t.nodeType && 1 === e.nodeType && (~e.sourceIndex || X) - (~t.sourceIndex || X);
                if (n) return n;
                if (i)
                    for (; i = i.nextSibling;)
                        if (i === e) return -1;
                return t ? 1 : -1
            }

            function a(t) {
                return function (e) {
                    return "input" === e.nodeName.toLowerCase() && e.type === t
                }
            }

            function l(t) {
                return function (e) {
                    var i = e.nodeName.toLowerCase();
                    return ("input" === i || "button" === i) && e.type === t
                }
            }

            function h(t) {
                return n(function (e) {
                    return e = +e, n(function (i, n) {
                        for (var s, r = t([], i.length, e), o = r.length; o--;) i[s = r[o]] && (i[s] = !(n[s] = i[s]))
                    })
                })
            }

            function d(t) {
                return t && void 0 !== t.getElementsByTagName && t
            }

            function c() {}

            function u(t) {
                for (var e = 0, i = t.length, n = ""; i > e; e++) n += t[e].value;
                return n
            }

            function p(t, e, i) {
                var n = e.dir,
                    s = i && "parentNode" === n,
                    r = W++;
                return e.first ? function (e, i, r) {
                    for (; e = e[n];)
                        if (1 === e.nodeType || s) return t(e, i, r)
                } : function (e, i, o) {
                    var a, l, h, d = [R, r];
                    if (o) {
                        for (; e = e[n];)
                            if ((1 === e.nodeType || s) && t(e, i, o)) return !0
                    } else
                        for (; e = e[n];)
                            if (1 === e.nodeType || s) {
                                if (h = e[j] || (e[j] = {}), l = h[e.uniqueID] || (h[e.uniqueID] = {}), (a = l[n]) && a[0] === R && a[1] === r) return d[2] = a[2];
                                if (l[n] = d, d[2] = t(e, i, o)) return !0
                            }
                }
            }

            function f(t) {
                return t.length > 1 ? function (e, i, n) {
                    for (var s = t.length; s--;)
                        if (!t[s](e, i, n)) return !1;
                    return !0
                } : t[0]
            }

            function m(t, e, i, n, s) {
                for (var r, o = [], a = 0, l = t.length, h = null != e; l > a; a++)(r = t[a]) && (i && !i(r, n, s) || (o.push(r), h && e.push(a)));
                return o
            }

            function g(t, i, s, r, o, a) {
                return r && !r[j] && (r = g(r)), o && !o[j] && (o = g(o, a)), n(function (n, a, l, h) {
                    var d, c, u, p = [],
                        f = [],
                        g = a.length,
                        v = n || function (t, i, n) {
                            for (var s = 0, r = i.length; r > s; s++) e(t, i[s], n);
                            return n
                        }(i || "*", l.nodeType ? [l] : l, []),
                        y = !t || !n && i ? v : m(v, p, t, l, h),
                        b = s ? o || (n ? t : g || r) ? [] : a : y;
                    if (s && s(y, b, l, h), r)
                        for (d = m(b, f), r(d, [], l, h), c = d.length; c--;)(u = d[c]) && (b[f[c]] = !(y[f[c]] = u));
                    if (n) {
                        if (o || t) {
                            if (o) {
                                for (d = [], c = b.length; c--;)(u = b[c]) && d.push(y[c] = u);
                                o(null, b = [], d, h)
                            }
                            for (c = b.length; c--;)(u = b[c]) && (d = o ? J(n, u) : p[c]) > -1 && (n[d] = !(a[d] = u))
                        }
                    } else b = m(b === a ? b.splice(g, b.length) : b), o ? o(null, a, b, h) : K.apply(a, b)
                })
            }

            function v(t) {
                for (var e, i, n, s = t.length, r = x.relative[t[0].type], o = r || x.relative[" "], a = r ? 1 : 0, l = p(function (t) {
                        return t === e
                    }, o, !0), h = p(function (t) {
                        return J(e, t) > -1
                    }, o, !0), d = [function (t, i, n) {
                        var s = !r && (n || i !== k) || ((e = i).nodeType ? l(t, i, n) : h(t, i, n));
                        return e = null, s
                    }]; s > a; a++)
                    if (i = x.relative[t[a].type]) d = [p(f(d), i)];
                    else {
                        if ((i = x.filter[t[a].type].apply(null, t[a].matches))[j]) {
                            for (n = ++a; s > n && !x.relative[t[n].type]; n++);
                            return g(a > 1 && f(d), a > 1 && u(t.slice(0, a - 1).concat({
                                value: " " === t[a - 2].type ? "*" : ""
                            })).replace(ot, "$1"), i, n > a && v(t.slice(a, n)), s > n && v(t = t.slice(n)), s > n && u(t))
                        }
                        d.push(i)
                    }
                return f(d)
            }

            function y(t, i) {
                var s = i.length > 0,
                    r = t.length > 0,
                    o = function (n, o, a, l, h) {
                        var d, c, u, p = 0,
                            f = "0",
                            g = n && [],
                            v = [],
                            y = k,
                            b = n || r && x.find.TAG("*", h),
                            w = R += null == y ? 1 : Math.random() || .1,
                            C = b.length;
                        for (h && (k = o === D || o || h); f !== C && null != (d = b[f]); f++) {
                            if (r && d) {
                                for (c = 0, o || d.ownerDocument === D || (M(d), a = !P); u = t[c++];)
                                    if (u(d, o || D, a)) {
                                        l.push(d);
                                        break
                                    }
                                h && (R = w)
                            }
                            s && ((d = !u && d) && p--, n && g.push(d))
                        }
                        if (p += f, s && f !== p) {
                            for (c = 0; u = i[c++];) u(g, v, o, a);
                            if (n) {
                                if (p > 0)
                                    for (; f--;) g[f] || v[f] || (v[f] = V.call(l));
                                v = m(v)
                            }
                            K.apply(l, v), h && !n && v.length > 0 && p + i.length > 1 && e.uniqueSort(l)
                        }
                        return h && (R = w, k = y), g
                    };
                return s ? n(o) : o
            }
            var b, w, x, C, T, E, S, _, k, $, z, M, D, I, P, N, L, O, A, j = "sizzle" + 1 * new Date,
                H = t.document,
                R = 0,
                W = 0,
                B = i(),
                q = i(),
                F = i(),
                U = function (t, e) {
                    return t === e && (z = !0), 0
                },
                X = 1 << 31,
                Y = {}.hasOwnProperty,
                G = [],
                V = G.pop,
                Q = G.push,
                K = G.push,
                Z = G.slice,
                J = function (t, e) {
                    for (var i = 0, n = t.length; n > i; i++)
                        if (t[i] === e) return i;
                    return -1
                },
                tt = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
                et = "[\\x20\\t\\r\\n\\f]",
                it = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",
                nt = "\\[" + et + "*(" + it + ")(?:" + et + "*([*^$|!~]?=)" + et + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + it + "))|)" + et + "*\\]",
                st = ":(" + it + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + nt + ")*)|.*)\\)|)",
                rt = new RegExp(et + "+", "g"),
                ot = new RegExp("^" + et + "+|((?:^|[^\\\\])(?:\\\\.)*)" + et + "+$", "g"),
                at = new RegExp("^" + et + "*," + et + "*"),
                lt = new RegExp("^" + et + "*([>+~]|" + et + ")" + et + "*"),
                ht = new RegExp("=" + et + "*([^\\]'\"]*?)" + et + "*\\]", "g"),
                dt = new RegExp(st),
                ct = new RegExp("^" + it + "$"),
                ut = {
                    ID: new RegExp("^#(" + it + ")"),
                    CLASS: new RegExp("^\\.(" + it + ")"),
                    TAG: new RegExp("^(" + it + "|[*])"),
                    ATTR: new RegExp("^" + nt),
                    PSEUDO: new RegExp("^" + st),
                    CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + et + "*(even|odd|(([+-]|)(\\d*)n|)" + et + "*(?:([+-]|)" + et + "*(\\d+)|))" + et + "*\\)|)", "i"),
                    bool: new RegExp("^(?:" + tt + ")$", "i"),
                    needsContext: new RegExp("^" + et + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + et + "*((?:-\\d)?\\d*)" + et + "*\\)|)(?=[^-]|$)", "i")
                },
                pt = /^(?:input|select|textarea|button)$/i,
                ft = /^h\d$/i,
                mt = /^[^{]+\{\s*\[native \w/,
                gt = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
                vt = /[+~]/,
                yt = /'|\\/g,
                bt = new RegExp("\\\\([\\da-f]{1,6}" + et + "?|(" + et + ")|.)", "ig"),
                wt = function (t, e, i) {
                    var n = "0x" + e - 65536;
                    return n != n || i ? e : 0 > n ? String.fromCharCode(n + 65536) : String.fromCharCode(n >> 10 | 55296, 1023 & n | 56320)
                },
                xt = function () {
                    M()
                };
            try {
                K.apply(G = Z.call(H.childNodes), H.childNodes), G[H.childNodes.length].nodeType
            } catch (t) {
                K = {
                    apply: G.length ? function (t, e) {
                        Q.apply(t, Z.call(e))
                    } : function (t, e) {
                        for (var i = t.length, n = 0; t[i++] = e[n++];);
                        t.length = i - 1
                    }
                }
            }
            w = e.support = {}, T = e.isXML = function (t) {
                var e = t && (t.ownerDocument || t).documentElement;
                return !!e && "HTML" !== e.nodeName
            }, M = e.setDocument = function (t) {
                var e, i, n = t ? t.ownerDocument || t : H;
                return n !== D && 9 === n.nodeType && n.documentElement ? (D = n, I = D.documentElement, P = !T(D), (i = D.defaultView) && i.top !== i && (i.addEventListener ? i.addEventListener("unload", xt, !1) : i.attachEvent && i.attachEvent("onunload", xt)), w.attributes = s(function (t) {
                    return t.className = "i", !t.getAttribute("className")
                }), w.getElementsByTagName = s(function (t) {
                    return t.appendChild(D.createComment("")), !t.getElementsByTagName("*").length
                }), w.getElementsByClassName = mt.test(D.getElementsByClassName), w.getById = s(function (t) {
                    return I.appendChild(t).id = j, !D.getElementsByName || !D.getElementsByName(j).length
                }), w.getById ? (x.find.ID = function (t, e) {
                    if (void 0 !== e.getElementById && P) {
                        var i = e.getElementById(t);
                        return i ? [i] : []
                    }
                }, x.filter.ID = function (t) {
                    var e = t.replace(bt, wt);
                    return function (t) {
                        return t.getAttribute("id") === e
                    }
                }) : (delete x.find.ID, x.filter.ID = function (t) {
                    var e = t.replace(bt, wt);
                    return function (t) {
                        var i = void 0 !== t.getAttributeNode && t.getAttributeNode("id");
                        return i && i.value === e
                    }
                }), x.find.TAG = w.getElementsByTagName ? function (t, e) {
                    return void 0 !== e.getElementsByTagName ? e.getElementsByTagName(t) : w.qsa ? e.querySelectorAll(t) : void 0
                } : function (t, e) {
                    var i, n = [],
                        s = 0,
                        r = e.getElementsByTagName(t);
                    if ("*" === t) {
                        for (; i = r[s++];) 1 === i.nodeType && n.push(i);
                        return n
                    }
                    return r
                }, x.find.CLASS = w.getElementsByClassName && function (t, e) {
                    return void 0 !== e.getElementsByClassName && P ? e.getElementsByClassName(t) : void 0
                }, L = [], N = [], (w.qsa = mt.test(D.querySelectorAll)) && (s(function (t) {
                    I.appendChild(t).innerHTML = "<a id='" + j + "'></a><select id='" + j + "-\r\\' msallowcapture=''><option selected=''></option></select>", t.querySelectorAll("[msallowcapture^='']").length && N.push("[*^$]=" + et + "*(?:''|\"\")"), t.querySelectorAll("[selected]").length || N.push("\\[" + et + "*(?:value|" + tt + ")"), t.querySelectorAll("[id~=" + j + "-]").length || N.push("~="), t.querySelectorAll(":checked").length || N.push(":checked"), t.querySelectorAll("a#" + j + "+*").length || N.push(".#.+[+~]")
                }), s(function (t) {
                    var e = D.createElement("input");
                    e.setAttribute("type", "hidden"), t.appendChild(e).setAttribute("name", "D"), t.querySelectorAll("[name=d]").length && N.push("name" + et + "*[*^$|!~]?="), t.querySelectorAll(":enabled").length || N.push(":enabled", ":disabled"), t.querySelectorAll("*,:x"), N.push(",.*:")
                })), (w.matchesSelector = mt.test(O = I.matches || I.webkitMatchesSelector || I.mozMatchesSelector || I.oMatchesSelector || I.msMatchesSelector)) && s(function (t) {
                    w.disconnectedMatch = O.call(t, "div"), O.call(t, "[s!='']:x"), L.push("!=", st)
                }), N = N.length && new RegExp(N.join("|")), L = L.length && new RegExp(L.join("|")), e = mt.test(I.compareDocumentPosition), A = e || mt.test(I.contains) ? function (t, e) {
                    var i = 9 === t.nodeType ? t.documentElement : t,
                        n = e && e.parentNode;
                    return t === n || !(!n || 1 !== n.nodeType || !(i.contains ? i.contains(n) : t.compareDocumentPosition && 16 & t.compareDocumentPosition(n)))
                } : function (t, e) {
                    if (e)
                        for (; e = e.parentNode;)
                            if (e === t) return !0;
                    return !1
                }, U = e ? function (t, e) {
                    if (t === e) return z = !0, 0;
                    var i = !t.compareDocumentPosition - !e.compareDocumentPosition;
                    return i || (1 & (i = (t.ownerDocument || t) === (e.ownerDocument || e) ? t.compareDocumentPosition(e) : 1) || !w.sortDetached && e.compareDocumentPosition(t) === i ? t === D || t.ownerDocument === H && A(H, t) ? -1 : e === D || e.ownerDocument === H && A(H, e) ? 1 : $ ? J($, t) - J($, e) : 0 : 4 & i ? -1 : 1)
                } : function (t, e) {
                    if (t === e) return z = !0, 0;
                    var i, n = 0,
                        s = t.parentNode,
                        r = e.parentNode,
                        a = [t],
                        l = [e];
                    if (!s || !r) return t === D ? -1 : e === D ? 1 : s ? -1 : r ? 1 : $ ? J($, t) - J($, e) : 0;
                    if (s === r) return o(t, e);
                    for (i = t; i = i.parentNode;) a.unshift(i);
                    for (i = e; i = i.parentNode;) l.unshift(i);
                    for (; a[n] === l[n];) n++;
                    return n ? o(a[n], l[n]) : a[n] === H ? -1 : l[n] === H ? 1 : 0
                }, D) : D
            }, e.matches = function (t, i) {
                return e(t, null, null, i)
            }, e.matchesSelector = function (t, i) {
                if ((t.ownerDocument || t) !== D && M(t), i = i.replace(ht, "='$1']"), w.matchesSelector && P && !F[i + " "] && (!L || !L.test(i)) && (!N || !N.test(i))) try {
                    var n = O.call(t, i);
                    if (n || w.disconnectedMatch || t.document && 11 !== t.document.nodeType) return n
                } catch (t) {}
                return e(i, D, null, [t]).length > 0
            }, e.contains = function (t, e) {
                return (t.ownerDocument || t) !== D && M(t), A(t, e)
            }, e.attr = function (t, e) {
                (t.ownerDocument || t) !== D && M(t);
                var i = x.attrHandle[e.toLowerCase()],
                    n = i && Y.call(x.attrHandle, e.toLowerCase()) ? i(t, e, !P) : void 0;
                return void 0 !== n ? n : w.attributes || !P ? t.getAttribute(e) : (n = t.getAttributeNode(e)) && n.specified ? n.value : null
            }, e.error = function (t) {
                throw new Error("Syntax error, unrecognized expression: " + t)
            }, e.uniqueSort = function (t) {
                var e, i = [],
                    n = 0,
                    s = 0;
                if (z = !w.detectDuplicates, $ = !w.sortStable && t.slice(0), t.sort(U), z) {
                    for (; e = t[s++];) e === t[s] && (n = i.push(s));
                    for (; n--;) t.splice(i[n], 1)
                }
                return $ = null, t
            }, C = e.getText = function (t) {
                var e, i = "",
                    n = 0,
                    s = t.nodeType;
                if (s) {
                    if (1 === s || 9 === s || 11 === s) {
                        if ("string" == typeof t.textContent) return t.textContent;
                        for (t = t.firstChild; t; t = t.nextSibling) i += C(t)
                    } else if (3 === s || 4 === s) return t.nodeValue
                } else
                    for (; e = t[n++];) i += C(e);
                return i
            }, (x = e.selectors = {
                cacheLength: 50,
                createPseudo: n,
                match: ut,
                attrHandle: {},
                find: {},
                relative: {
                    ">": {
                        dir: "parentNode",
                        first: !0
                    },
                    " ": {
                        dir: "parentNode"
                    },
                    "+": {
                        dir: "previousSibling",
                        first: !0
                    },
                    "~": {
                        dir: "previousSibling"
                    }
                },
                preFilter: {
                    ATTR: function (t) {
                        return t[1] = t[1].replace(bt, wt), t[3] = (t[3] || t[4] || t[5] || "").replace(bt, wt), "~=" === t[2] && (t[3] = " " + t[3] + " "), t.slice(0, 4)
                    },
                    CHILD: function (t) {
                        return t[1] = t[1].toLowerCase(), "nth" === t[1].slice(0, 3) ? (t[3] || e.error(t[0]), t[4] = +(t[4] ? t[5] + (t[6] || 1) : 2 * ("even" === t[3] || "odd" === t[3])), t[5] = +(t[7] + t[8] || "odd" === t[3])) : t[3] && e.error(t[0]), t
                    },
                    PSEUDO: function (t) {
                        var e, i = !t[6] && t[2];
                        return ut.CHILD.test(t[0]) ? null : (t[3] ? t[2] = t[4] || t[5] || "" : i && dt.test(i) && (e = E(i, !0)) && (e = i.indexOf(")", i.length - e) - i.length) && (t[0] = t[0].slice(0, e), t[2] = i.slice(0, e)), t.slice(0, 3))
                    }
                },
                filter: {
                    TAG: function (t) {
                        var e = t.replace(bt, wt).toLowerCase();
                        return "*" === t ? function () {
                            return !0
                        } : function (t) {
                            return t.nodeName && t.nodeName.toLowerCase() === e
                        }
                    },
                    CLASS: function (t) {
                        var e = B[t + " "];
                        return e || (e = new RegExp("(^|" + et + ")" + t + "(" + et + "|$)")) && B(t, function (t) {
                            return e.test("string" == typeof t.className && t.className || void 0 !== t.getAttribute && t.getAttribute("class") || "")
                        })
                    },
                    ATTR: function (t, i, n) {
                        return function (s) {
                            var r = e.attr(s, t);
                            return null == r ? "!=" === i : !i || (r += "", "=" === i ? r === n : "!=" === i ? r !== n : "^=" === i ? n && 0 === r.indexOf(n) : "*=" === i ? n && r.indexOf(n) > -1 : "$=" === i ? n && r.slice(-n.length) === n : "~=" === i ? (" " + r.replace(rt, " ") + " ").indexOf(n) > -1 : "|=" === i && (r === n || r.slice(0, n.length + 1) === n + "-"))
                        }
                    },
                    CHILD: function (t, e, i, n, s) {
                        var r = "nth" !== t.slice(0, 3),
                            o = "last" !== t.slice(-4),
                            a = "of-type" === e;
                        return 1 === n && 0 === s ? function (t) {
                            return !!t.parentNode
                        } : function (e, i, l) {
                            var h, d, c, u, p, f, m = r !== o ? "nextSibling" : "previousSibling",
                                g = e.parentNode,
                                v = a && e.nodeName.toLowerCase(),
                                y = !l && !a,
                                b = !1;
                            if (g) {
                                if (r) {
                                    for (; m;) {
                                        for (u = e; u = u[m];)
                                            if (a ? u.nodeName.toLowerCase() === v : 1 === u.nodeType) return !1;
                                        f = m = "only" === t && !f && "nextSibling"
                                    }
                                    return !0
                                }
                                if (f = [o ? g.firstChild : g.lastChild], o && y) {
                                    for (b = (p = (h = (d = (c = (u = g)[j] || (u[j] = {}))[u.uniqueID] || (c[u.uniqueID] = {}))[t] || [])[0] === R && h[1]) && h[2], u = p && g.childNodes[p]; u = ++p && u && u[m] || (b = p = 0) || f.pop();)
                                        if (1 === u.nodeType && ++b && u === e) {
                                            d[t] = [R, p, b];
                                            break
                                        }
                                } else if (y && (u = e, c = u[j] || (u[j] = {}), d = c[u.uniqueID] || (c[u.uniqueID] = {}), h = d[t] || [], p = h[0] === R && h[1], b = p), !1 === b)
                                    for (;
                                        (u = ++p && u && u[m] || (b = p = 0) || f.pop()) && ((a ? u.nodeName.toLowerCase() !== v : 1 !== u.nodeType) || !++b || (y && (c = u[j] || (u[j] = {}), d = c[u.uniqueID] || (c[u.uniqueID] = {}), d[t] = [R, b]), u !== e)););
                                return (b -= s) === n || b % n == 0 && b / n >= 0
                            }
                        }
                    },
                    PSEUDO: function (t, i) {
                        var s, r = x.pseudos[t] || x.setFilters[t.toLowerCase()] || e.error("unsupported pseudo: " + t);
                        return r[j] ? r(i) : r.length > 1 ? (s = [t, t, "", i], x.setFilters.hasOwnProperty(t.toLowerCase()) ? n(function (t, e) {
                            for (var n, s = r(t, i), o = s.length; o--;) n = J(t, s[o]), t[n] = !(e[n] = s[o])
                        }) : function (t) {
                            return r(t, 0, s)
                        }) : r
                    }
                },
                pseudos: {
                    not: n(function (t) {
                        var e = [],
                            i = [],
                            s = S(t.replace(ot, "$1"));
                        return s[j] ? n(function (t, e, i, n) {
                            for (var r, o = s(t, null, n, []), a = t.length; a--;)(r = o[a]) && (t[a] = !(e[a] = r))
                        }) : function (t, n, r) {
                            return e[0] = t, s(e, null, r, i), e[0] = null, !i.pop()
                        }
                    }),
                    has: n(function (t) {
                        return function (i) {
                            return e(t, i).length > 0
                        }
                    }),
                    contains: n(function (t) {
                        return t = t.replace(bt, wt),
                            function (e) {
                                return (e.textContent || e.innerText || C(e)).indexOf(t) > -1
                            }
                    }),
                    lang: n(function (t) {
                        return ct.test(t || "") || e.error("unsupported lang: " + t), t = t.replace(bt, wt).toLowerCase(),
                            function (e) {
                                var i;
                                do {
                                    if (i = P ? e.lang : e.getAttribute("xml:lang") || e.getAttribute("lang")) return (i = i.toLowerCase()) === t || 0 === i.indexOf(t + "-")
                                } while ((e = e.parentNode) && 1 === e.nodeType);
                                return !1
                            }
                    }),
                    target: function (e) {
                        var i = t.location && t.location.hash;
                        return i && i.slice(1) === e.id
                    },
                    root: function (t) {
                        return t === I
                    },
                    focus: function (t) {
                        return t === D.activeElement && (!D.hasFocus || D.hasFocus()) && !!(t.type || t.href || ~t.tabIndex)
                    },
                    enabled: function (t) {
                        return !1 === t.disabled
                    },
                    disabled: function (t) {
                        return !0 === t.disabled
                    },
                    checked: function (t) {
                        var e = t.nodeName.toLowerCase();
                        return "input" === e && !!t.checked || "option" === e && !!t.selected
                    },
                    selected: function (t) {
                        return t.parentNode && t.parentNode.selectedIndex, !0 === t.selected
                    },
                    empty: function (t) {
                        for (t = t.firstChild; t; t = t.nextSibling)
                            if (t.nodeType < 6) return !1;
                        return !0
                    },
                    parent: function (t) {
                        return !x.pseudos.empty(t)
                    },
                    header: function (t) {
                        return ft.test(t.nodeName)
                    },
                    input: function (t) {
                        return pt.test(t.nodeName)
                    },
                    button: function (t) {
                        var e = t.nodeName.toLowerCase();
                        return "input" === e && "button" === t.type || "button" === e
                    },
                    text: function (t) {
                        var e;
                        return "input" === t.nodeName.toLowerCase() && "text" === t.type && (null == (e = t.getAttribute("type")) || "text" === e.toLowerCase())
                    },
                    first: h(function () {
                        return [0]
                    }),
                    last: h(function (t, e) {
                        return [e - 1]
                    }),
                    eq: h(function (t, e, i) {
                        return [0 > i ? i + e : i]
                    }),
                    even: h(function (t, e) {
                        for (var i = 0; e > i; i += 2) t.push(i);
                        return t
                    }),
                    odd: h(function (t, e) {
                        for (var i = 1; e > i; i += 2) t.push(i);
                        return t
                    }),
                    lt: h(function (t, e, i) {
                        for (var n = 0 > i ? i + e : i; --n >= 0;) t.push(n);
                        return t
                    }),
                    gt: h(function (t, e, i) {
                        for (var n = 0 > i ? i + e : i; ++n < e;) t.push(n);
                        return t
                    })
                }
            }).pseudos.nth = x.pseudos.eq;
            for (b in {
                    radio: !0,
                    checkbox: !0,
                    file: !0,
                    password: !0,
                    image: !0
                }) x.pseudos[b] = a(b);
            for (b in {
                    submit: !0,
                    reset: !0
                }) x.pseudos[b] = l(b);
            return c.prototype = x.filters = x.pseudos, x.setFilters = new c, E = e.tokenize = function (t, i) {
                var n, s, r, o, a, l, h, d = q[t + " "];
                if (d) return i ? 0 : d.slice(0);
                for (a = t, l = [], h = x.preFilter; a;) {
                    n && !(s = at.exec(a)) || (s && (a = a.slice(s[0].length) || a), l.push(r = [])), n = !1, (s = lt.exec(a)) && (n = s.shift(), r.push({
                        value: n,
                        type: s[0].replace(ot, " ")
                    }), a = a.slice(n.length));
                    for (o in x.filter) !(s = ut[o].exec(a)) || h[o] && !(s = h[o](s)) || (n = s.shift(), r.push({
                        value: n,
                        type: o,
                        matches: s
                    }), a = a.slice(n.length));
                    if (!n) break
                }
                return i ? a.length : a ? e.error(t) : q(t, l).slice(0)
            }, S = e.compile = function (t, e) {
                var i, n = [],
                    s = [],
                    r = F[t + " "];
                if (!r) {
                    for (e || (e = E(t)), i = e.length; i--;)(r = v(e[i]))[j] ? n.push(r) : s.push(r);
                    (r = F(t, y(s, n))).selector = t
                }
                return r
            }, _ = e.select = function (t, e, i, n) {
                var s, r, o, a, l, h = "function" == typeof t && t,
                    c = !n && E(t = h.selector || t);
                if (i = i || [], 1 === c.length) {
                    if ((r = c[0] = c[0].slice(0)).length > 2 && "ID" === (o = r[0]).type && w.getById && 9 === e.nodeType && P && x.relative[r[1].type]) {
                        if (!(e = (x.find.ID(o.matches[0].replace(bt, wt), e) || [])[0])) return i;
                        h && (e = e.parentNode), t = t.slice(r.shift().value.length)
                    }
                    for (s = ut.needsContext.test(t) ? 0 : r.length; s-- && (o = r[s], !x.relative[a = o.type]);)
                        if ((l = x.find[a]) && (n = l(o.matches[0].replace(bt, wt), vt.test(r[0].type) && d(e.parentNode) || e))) {
                            if (r.splice(s, 1), !(t = n.length && u(r))) return K.apply(i, n), i;
                            break
                        }
                }
                return (h || S(t, c))(n, e, !P, i, !e || vt.test(t) && d(e.parentNode) || e), i
            }, w.sortStable = j.split("").sort(U).join("") === j, w.detectDuplicates = !!z, M(), w.sortDetached = s(function (t) {
                return 1 & t.compareDocumentPosition(D.createElement("div"))
            }), s(function (t) {
                return t.innerHTML = "<a href='#'></a>", "#" === t.firstChild.getAttribute("href")
            }) || r("type|href|height|width", function (t, e, i) {
                return i ? void 0 : t.getAttribute(e, "type" === e.toLowerCase() ? 1 : 2)
            }), w.attributes && s(function (t) {
                return t.innerHTML = "<input/>", t.firstChild.setAttribute("value", ""), "" === t.firstChild.getAttribute("value")
            }) || r("value", function (t, e, i) {
                return i || "input" !== t.nodeName.toLowerCase() ? void 0 : t.defaultValue
            }), s(function (t) {
                return null == t.getAttribute("disabled")
            }) || r(tt, function (t, e, i) {
                var n;
                return i ? void 0 : !0 === t[e] ? e.toLowerCase() : (n = t.getAttributeNode(e)) && n.specified ? n.value : null
            }), e
        }(t);
        tt.find = rt, tt.expr = rt.selectors, tt.expr[":"] = tt.expr.pseudos, tt.uniqueSort = tt.unique = rt.uniqueSort, tt.text = rt.getText, tt.isXMLDoc = rt.isXML, tt.contains = rt.contains;
        var ot = function (t, e, i) {
                for (var n = [], s = void 0 !== i;
                    (t = t[e]) && 9 !== t.nodeType;)
                    if (1 === t.nodeType) {
                        if (s && tt(t).is(i)) break;
                        n.push(t)
                    }
                return n
            },
            at = function (t, e) {
                for (var i = []; t; t = t.nextSibling) 1 === t.nodeType && t !== e && i.push(t);
                return i
            },
            lt = tt.expr.match.needsContext,
            ht = /^<([\w-]+)\s*\/?>(?:<\/\1>|)$/,
            dt = /^.[^:#\[\.,]*$/;
        tt.filter = function (t, e, i) {
            var n = e[0];
            return i && (t = ":not(" + t + ")"), 1 === e.length && 1 === n.nodeType ? tt.find.matchesSelector(n, t) ? [n] : [] : tt.find.matches(t, tt.grep(e, function (t) {
                return 1 === t.nodeType
            }))
        }, tt.fn.extend({
            find: function (t) {
                var e, i = this.length,
                    n = [],
                    s = this;
                if ("string" != typeof t) return this.pushStack(tt(t).filter(function () {
                    for (e = 0; i > e; e++)
                        if (tt.contains(s[e], this)) return !0
                }));
                for (e = 0; i > e; e++) tt.find(t, s[e], n);
                return n = this.pushStack(i > 1 ? tt.unique(n) : n), n.selector = this.selector ? this.selector + " " + t : t, n
            },
            filter: function (t) {
                return this.pushStack(n(this, t || [], !1))
            },
            not: function (t) {
                return this.pushStack(n(this, t || [], !0))
            },
            is: function (t) {
                return !!n(this, "string" == typeof t && lt.test(t) ? tt(t) : t || [], !1).length
            }
        });
        var ct, ut = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/;
        (tt.fn.init = function (t, e, i) {
            var n, s;
            if (!t) return this;
            if (i = i || ct, "string" == typeof t) {
                if (!(n = "<" === t[0] && ">" === t[t.length - 1] && t.length >= 3 ? [null, t, null] : ut.exec(t)) || !n[1] && e) return !e || e.jquery ? (e || i).find(t) : this.constructor(e).find(t);
                if (n[1]) {
                    if (e = e instanceof tt ? e[0] : e, tt.merge(this, tt.parseHTML(n[1], e && e.nodeType ? e.ownerDocument || e : F, !0)), ht.test(n[1]) && tt.isPlainObject(e))
                        for (n in e) tt.isFunction(this[n]) ? this[n](e[n]) : this.attr(n, e[n]);
                    return this
                }
                return (s = F.getElementById(n[2])) && s.parentNode && (this.length = 1, this[0] = s), this.context = F, this.selector = t, this
            }
            return t.nodeType ? (this.context = this[0] = t, this.length = 1, this) : tt.isFunction(t) ? void 0 !== i.ready ? i.ready(t) : t(tt) : (void 0 !== t.selector && (this.selector = t.selector, this.context = t.context), tt.makeArray(t, this))
        }).prototype = tt.fn, ct = tt(F);
        var pt = /^(?:parents|prev(?:Until|All))/,
            ft = {
                children: !0,
                contents: !0,
                next: !0,
                prev: !0
            };
        tt.fn.extend({
            has: function (t) {
                var e = tt(t, this),
                    i = e.length;
                return this.filter(function () {
                    for (var t = 0; i > t; t++)
                        if (tt.contains(this, e[t])) return !0
                })
            },
            closest: function (t, e) {
                for (var i, n = 0, s = this.length, r = [], o = lt.test(t) || "string" != typeof t ? tt(t, e || this.context) : 0; s > n; n++)
                    for (i = this[n]; i && i !== e; i = i.parentNode)
                        if (i.nodeType < 11 && (o ? o.index(i) > -1 : 1 === i.nodeType && tt.find.matchesSelector(i, t))) {
                            r.push(i);
                            break
                        }
                return this.pushStack(r.length > 1 ? tt.uniqueSort(r) : r)
            },
            index: function (t) {
                return t ? "string" == typeof t ? G.call(tt(t), this[0]) : G.call(this, t.jquery ? t[0] : t) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
            },
            add: function (t, e) {
                return this.pushStack(tt.uniqueSort(tt.merge(this.get(), tt(t, e))))
            },
            addBack: function (t) {
                return this.add(null == t ? this.prevObject : this.prevObject.filter(t))
            }
        }), tt.each({
            parent: function (t) {
                var e = t.parentNode;
                return e && 11 !== e.nodeType ? e : null
            },
            parents: function (t) {
                return ot(t, "parentNode")
            },
            parentsUntil: function (t, e, i) {
                return ot(t, "parentNode", i)
            },
            next: function (t) {
                return s(t, "nextSibling")
            },
            prev: function (t) {
                return s(t, "previousSibling")
            },
            nextAll: function (t) {
                return ot(t, "nextSibling")
            },
            prevAll: function (t) {
                return ot(t, "previousSibling")
            },
            nextUntil: function (t, e, i) {
                return ot(t, "nextSibling", i)
            },
            prevUntil: function (t, e, i) {
                return ot(t, "previousSibling", i)
            },
            siblings: function (t) {
                return at((t.parentNode || {}).firstChild, t)
            },
            children: function (t) {
                return at(t.firstChild)
            },
            contents: function (t) {
                return t.contentDocument || tt.merge([], t.childNodes)
            }
        }, function (t, e) {
            tt.fn[t] = function (i, n) {
                var s = tt.map(this, e, i);
                return "Until" !== t.slice(-5) && (n = i), n && "string" == typeof n && (s = tt.filter(n, s)), this.length > 1 && (ft[t] || tt.uniqueSort(s), pt.test(t) && s.reverse()), this.pushStack(s)
            }
        });
        var mt = /\S+/g;
        tt.Callbacks = function (t) {
            t = "string" == typeof t ? function (t) {
                var e = {};
                return tt.each(t.match(mt) || [], function (t, i) {
                    e[i] = !0
                }), e
            }(t) : tt.extend({}, t);
            var e, i, n, s, r = [],
                o = [],
                a = -1,
                l = function () {
                    for (s = t.once, n = e = !0; o.length; a = -1)
                        for (i = o.shift(); ++a < r.length;) !1 === r[a].apply(i[0], i[1]) && t.stopOnFalse && (a = r.length, i = !1);
                    t.memory || (i = !1), e = !1, s && (r = i ? [] : "")
                },
                h = {
                    add: function () {
                        return r && (i && !e && (a = r.length - 1, o.push(i)), function e(i) {
                            tt.each(i, function (i, n) {
                                tt.isFunction(n) ? t.unique && h.has(n) || r.push(n) : n && n.length && "string" !== tt.type(n) && e(n)
                            })
                        }(arguments), i && !e && l()), this
                    },
                    remove: function () {
                        return tt.each(arguments, function (t, e) {
                            for (var i;
                                (i = tt.inArray(e, r, i)) > -1;) r.splice(i, 1), a >= i && a--
                        }), this
                    },
                    has: function (t) {
                        return t ? tt.inArray(t, r) > -1 : r.length > 0
                    },
                    empty: function () {
                        return r && (r = []), this
                    },
                    disable: function () {
                        return s = o = [], r = i = "", this
                    },
                    disabled: function () {
                        return !r
                    },
                    lock: function () {
                        return s = o = [], i || (r = i = ""), this
                    },
                    locked: function () {
                        return !!s
                    },
                    fireWith: function (t, i) {
                        return s || (i = i || [], i = [t, i.slice ? i.slice() : i], o.push(i), e || l()), this
                    },
                    fire: function () {
                        return h.fireWith(this, arguments), this
                    },
                    fired: function () {
                        return !!n
                    }
                };
            return h
        }, tt.extend({
            Deferred: function (t) {
                var e = [["resolve", "done", tt.Callbacks("once memory"), "resolved"], ["reject", "fail", tt.Callbacks("once memory"), "rejected"], ["notify", "progress", tt.Callbacks("memory")]],
                    i = "pending",
                    n = {
                        state: function () {
                            return i
                        },
                        always: function () {
                            return s.done(arguments).fail(arguments), this
                        },
                        then: function () {
                            var t = arguments;
                            return tt.Deferred(function (i) {
                                tt.each(e, function (e, r) {
                                    var o = tt.isFunction(t[e]) && t[e];
                                    s[r[1]](function () {
                                        var t = o && o.apply(this, arguments);
                                        t && tt.isFunction(t.promise) ? t.promise().progress(i.notify).done(i.resolve).fail(i.reject) : i[r[0] + "With"](this === n ? i.promise() : this, o ? [t] : arguments)
                                    })
                                }), t = null
                            }).promise()
                        },
                        promise: function (t) {
                            return null != t ? tt.extend(t, n) : n
                        }
                    },
                    s = {};
                return n.pipe = n.then, tt.each(e, function (t, r) {
                    var o = r[2],
                        a = r[3];
                    n[r[1]] = o.add, a && o.add(function () {
                        i = a
                    }, e[1 ^ t][2].disable, e[2][2].lock), s[r[0]] = function () {
                        return s[r[0] + "With"](this === s ? n : this, arguments), this
                    }, s[r[0] + "With"] = o.fireWith
                }), n.promise(s), t && t.call(s, s), s
            },
            when: function (t) {
                var e, i, n, s = 0,
                    r = U.call(arguments),
                    o = r.length,
                    a = 1 !== o || t && tt.isFunction(t.promise) ? o : 0,
                    l = 1 === a ? t : tt.Deferred(),
                    h = function (t, i, n) {
                        return function (s) {
                            i[t] = this, n[t] = arguments.length > 1 ? U.call(arguments) : s, n === e ? l.notifyWith(i, n) : --a || l.resolveWith(i, n)
                        }
                    };
                if (o > 1)
                    for (e = new Array(o), i = new Array(o), n = new Array(o); o > s; s++) r[s] && tt.isFunction(r[s].promise) ? r[s].promise().progress(h(s, i, e)).done(h(s, n, r)).fail(l.reject) : --a;
                return a || l.resolveWith(n, r), l.promise()
            }
        });
        var gt;
        tt.fn.ready = function (t) {
            return tt.ready.promise().done(t), this
        }, tt.extend({
            isReady: !1,
            readyWait: 1,
            holdReady: function (t) {
                t ? tt.readyWait++ : tt.ready(!0)
            },
            ready: function (t) {
                (!0 === t ? --tt.readyWait : tt.isReady) || (tt.isReady = !0, !0 !== t && --tt.readyWait > 0 || (gt.resolveWith(F, [tt]), tt.fn.triggerHandler && (tt(F).triggerHandler("ready"), tt(F).off("ready"))))
            }
        }), tt.ready.promise = function (e) {
            return gt || (gt = tt.Deferred(), "complete" === F.readyState || "loading" !== F.readyState && !F.documentElement.doScroll ? t.setTimeout(tt.ready) : (F.addEventListener("DOMContentLoaded", r), t.addEventListener("load", r))), gt.promise(e)
        }, tt.ready.promise();
        var vt = function (t, e, i, n, s, r, o) {
                var a = 0,
                    l = t.length,
                    h = null == i;
                if ("object" === tt.type(i)) {
                    s = !0;
                    for (a in i) vt(t, e, a, i[a], !0, r, o)
                } else if (void 0 !== n && (s = !0, tt.isFunction(n) || (o = !0), h && (o ? (e.call(t, n), e = null) : (h = e, e = function (t, e, i) {
                        return h.call(tt(t), i)
                    })), e))
                    for (; l > a; a++) e(t[a], i, o ? n : n.call(t[a], a, e(t[a], i)));
                return s ? t : h ? e.call(t) : l ? e(t[0], i) : r
            },
            yt = function (t) {
                return 1 === t.nodeType || 9 === t.nodeType || !+t.nodeType
            };
        o.uid = 1, o.prototype = {
            register: function (t, e) {
                var i = e || {};
                return t.nodeType ? t[this.expando] = i : Object.defineProperty(t, this.expando, {
                    value: i,
                    writable: !0,
                    configurable: !0
                }), t[this.expando]
            },
            cache: function (t) {
                if (!yt(t)) return {};
                var e = t[this.expando];
                return e || (e = {}, yt(t) && (t.nodeType ? t[this.expando] = e : Object.defineProperty(t, this.expando, {
                    value: e,
                    configurable: !0
                }))), e
            },
            set: function (t, e, i) {
                var n, s = this.cache(t);
                if ("string" == typeof e) s[e] = i;
                else
                    for (n in e) s[n] = e[n];
                return s
            },
            get: function (t, e) {
                return void 0 === e ? this.cache(t) : t[this.expando] && t[this.expando][e]
            },
            access: function (t, e, i) {
                var n;
                return void 0 === e || e && "string" == typeof e && void 0 === i ? void 0 !== (n = this.get(t, e)) ? n : this.get(t, tt.camelCase(e)) : (this.set(t, e, i), void 0 !== i ? i : e)
            },
            remove: function (t, e) {
                var i, n, s, r = t[this.expando];
                if (void 0 !== r) {
                    if (void 0 === e) this.register(t);
                    else {
                        tt.isArray(e) ? n = e.concat(e.map(tt.camelCase)) : (s = tt.camelCase(e), e in r ? n = [e, s] : (n = s, n = n in r ? [n] : n.match(mt) || [])), i = n.length;
                        for (; i--;) delete r[n[i]]
                    }(void 0 === e || tt.isEmptyObject(r)) && (t.nodeType ? t[this.expando] = void 0 : delete t[this.expando])
                }
            },
            hasData: function (t) {
                var e = t[this.expando];
                return void 0 !== e && !tt.isEmptyObject(e)
            }
        };
        var bt = new o,
            wt = new o,
            xt = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
            Ct = /[A-Z]/g;
        tt.extend({
            hasData: function (t) {
                return wt.hasData(t) || bt.hasData(t)
            },
            data: function (t, e, i) {
                return wt.access(t, e, i)
            },
            removeData: function (t, e) {
                wt.remove(t, e)
            },
            _data: function (t, e, i) {
                return bt.access(t, e, i)
            },
            _removeData: function (t, e) {
                bt.remove(t, e)
            }
        }), tt.fn.extend({
            data: function (t, e) {
                var i, n, s, r = this[0],
                    o = r && r.attributes;
                if (void 0 === t) {
                    if (this.length && (s = wt.get(r), 1 === r.nodeType && !bt.get(r, "hasDataAttrs"))) {
                        for (i = o.length; i--;) o[i] && 0 === (n = o[i].name).indexOf("data-") && (n = tt.camelCase(n.slice(5)), a(r, n, s[n]));
                        bt.set(r, "hasDataAttrs", !0)
                    }
                    return s
                }
                return "object" == typeof t ? this.each(function () {
                    wt.set(this, t)
                }) : vt(this, function (e) {
                    var i, n;
                    if (r && void 0 === e) {
                        if (void 0 !== (i = wt.get(r, t) || wt.get(r, t.replace(Ct, "-$&").toLowerCase()))) return i;
                        if (n = tt.camelCase(t), void 0 !== (i = wt.get(r, n))) return i;
                        if (void 0 !== (i = a(r, n, void 0))) return i
                    } else n = tt.camelCase(t), this.each(function () {
                        var i = wt.get(this, n);
                        wt.set(this, n, e), t.indexOf("-") > -1 && void 0 !== i && wt.set(this, t, e)
                    })
                }, null, e, arguments.length > 1, null, !0)
            },
            removeData: function (t) {
                return this.each(function () {
                    wt.remove(this, t)
                })
            }
        }), tt.extend({
            queue: function (t, e, i) {
                var n;
                return t ? (e = (e || "fx") + "queue", n = bt.get(t, e), i && (!n || tt.isArray(i) ? n = bt.access(t, e, tt.makeArray(i)) : n.push(i)), n || []) : void 0
            },
            dequeue: function (t, e) {
                e = e || "fx";
                var i = tt.queue(t, e),
                    n = i.length,
                    s = i.shift(),
                    r = tt._queueHooks(t, e);
                "inprogress" === s && (s = i.shift(), n--), s && ("fx" === e && i.unshift("inprogress"), delete r.stop, s.call(t, function () {
                    tt.dequeue(t, e)
                }, r)), !n && r && r.empty.fire()
            },
            _queueHooks: function (t, e) {
                var i = e + "queueHooks";
                return bt.get(t, i) || bt.access(t, i, {
                    empty: tt.Callbacks("once memory").add(function () {
                        bt.remove(t, [e + "queue", i])
                    })
                })
            }
        }), tt.fn.extend({
            queue: function (t, e) {
                var i = 2;
                return "string" != typeof t && (e = t, t = "fx", i--), arguments.length < i ? tt.queue(this[0], t) : void 0 === e ? this : this.each(function () {
                    var i = tt.queue(this, t, e);
                    tt._queueHooks(this, t), "fx" === t && "inprogress" !== i[0] && tt.dequeue(this, t)
                })
            },
            dequeue: function (t) {
                return this.each(function () {
                    tt.dequeue(this, t)
                })
            },
            clearQueue: function (t) {
                return this.queue(t || "fx", [])
            },
            promise: function (t, e) {
                var i, n = 1,
                    s = tt.Deferred(),
                    r = this,
                    o = this.length,
                    a = function () {
                        --n || s.resolveWith(r, [r])
                    };
                for ("string" != typeof t && (e = t, t = void 0), t = t || "fx"; o--;)(i = bt.get(r[o], t + "queueHooks")) && i.empty && (n++, i.empty.add(a));
                return a(), s.promise(e)
            }
        });
        var Tt = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
            Et = new RegExp("^(?:([+-])=|)(" + Tt + ")([a-z%]*)$", "i"),
            St = ["Top", "Right", "Bottom", "Left"],
            _t = function (t, e) {
                return t = e || t, "none" === tt.css(t, "display") || !tt.contains(t.ownerDocument, t)
            },
            kt = /^(?:checkbox|radio)$/i,
            $t = /<([\w:-]+)/,
            zt = /^$|\/(?:java|ecma)script/i,
            Mt = {
                option: [1, "<select multiple='multiple'>", "</select>"],
                thead: [1, "<table>", "</table>"],
                col: [2, "<table><colgroup>", "</colgroup></table>"],
                tr: [2, "<table><tbody>", "</tbody></table>"],
                td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
                _default: [0, "", ""]
            };
        Mt.optgroup = Mt.option, Mt.tbody = Mt.tfoot = Mt.colgroup = Mt.caption = Mt.thead, Mt.th = Mt.td;
        var Dt = /<|&#?\w+;/;
        ! function () {
            var t = F.createDocumentFragment().appendChild(F.createElement("div")),
                e = F.createElement("input");
            e.setAttribute("type", "radio"), e.setAttribute("checked", "checked"), e.setAttribute("name", "t"), t.appendChild(e), Z.checkClone = t.cloneNode(!0).cloneNode(!0).lastChild.checked, t.innerHTML = "<textarea>x</textarea>", Z.noCloneChecked = !!t.cloneNode(!0).lastChild.defaultValue
        }();
        var It = /^key/,
            Pt = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
            Nt = /^([^.]*)(?:\.(.+)|)/;
        tt.event = {
            global: {},
            add: function (t, e, i, n, s) {
                var r, o, a, l, h, d, c, u, p, f, m, g = bt.get(t);
                if (g)
                    for (i.handler && (r = i, i = r.handler, s = r.selector), i.guid || (i.guid = tt.guid++), (l = g.events) || (l = g.events = {}), (o = g.handle) || (o = g.handle = function (e) {
                            return void 0 !== tt && tt.event.triggered !== e.type ? tt.event.dispatch.apply(t, arguments) : void 0
                        }), h = (e = (e || "").match(mt) || [""]).length; h--;) a = Nt.exec(e[h]) || [], p = m = a[1], f = (a[2] || "").split(".").sort(), p && (c = tt.event.special[p] || {}, p = (s ? c.delegateType : c.bindType) || p, c = tt.event.special[p] || {}, d = tt.extend({
                        type: p,
                        origType: m,
                        data: n,
                        handler: i,
                        guid: i.guid,
                        selector: s,
                        needsContext: s && tt.expr.match.needsContext.test(s),
                        namespace: f.join(".")
                    }, r), (u = l[p]) || (u = l[p] = [], u.delegateCount = 0, c.setup && !1 !== c.setup.call(t, n, f, o) || t.addEventListener && t.addEventListener(p, o)), c.add && (c.add.call(t, d), d.handler.guid || (d.handler.guid = i.guid)), s ? u.splice(u.delegateCount++, 0, d) : u.push(d), tt.event.global[p] = !0)
            },
            remove: function (t, e, i, n, s) {
                var r, o, a, l, h, d, c, u, p, f, m, g = bt.hasData(t) && bt.get(t);
                if (g && (l = g.events)) {
                    for (h = (e = (e || "").match(mt) || [""]).length; h--;)
                        if (a = Nt.exec(e[h]) || [], p = m = a[1], f = (a[2] || "").split(".").sort(), p) {
                            for (c = tt.event.special[p] || {}, u = l[p = (n ? c.delegateType : c.bindType) || p] || [], a = a[2] && new RegExp("(^|\\.)" + f.join("\\.(?:.*\\.|)") + "(\\.|$)"), o = r = u.length; r--;) d = u[r], !s && m !== d.origType || i && i.guid !== d.guid || a && !a.test(d.namespace) || n && n !== d.selector && ("**" !== n || !d.selector) || (u.splice(r, 1), d.selector && u.delegateCount--, c.remove && c.remove.call(t, d));
                            o && !u.length && (c.teardown && !1 !== c.teardown.call(t, f, g.handle) || tt.removeEvent(t, p, g.handle), delete l[p])
                        } else
                            for (p in l) tt.event.remove(t, p + e[h], i, n, !0);
                    tt.isEmptyObject(l) && bt.remove(t, "handle events")
                }
            },
            dispatch: function (t) {
                t = tt.event.fix(t);
                var e, i, n, s, r, o = [],
                    a = U.call(arguments),
                    l = (bt.get(this, "events") || {})[t.type] || [],
                    h = tt.event.special[t.type] || {};
                if (a[0] = t, t.delegateTarget = this, !h.preDispatch || !1 !== h.preDispatch.call(this, t)) {
                    for (o = tt.event.handlers.call(this, t, l), e = 0;
                        (s = o[e++]) && !t.isPropagationStopped();)
                        for (t.currentTarget = s.elem, i = 0;
                            (r = s.handlers[i++]) && !t.isImmediatePropagationStopped();) t.rnamespace && !t.rnamespace.test(r.namespace) || (t.handleObj = r, t.data = r.data, void 0 !== (n = ((tt.event.special[r.origType] || {}).handle || r.handler).apply(s.elem, a)) && !1 === (t.result = n) && (t.preventDefault(), t.stopPropagation()));
                    return h.postDispatch && h.postDispatch.call(this, t), t.result
                }
            },
            handlers: function (t, e) {
                var i, n, s, r, o = [],
                    a = e.delegateCount,
                    l = t.target;
                if (a && l.nodeType && ("click" !== t.type || isNaN(t.button) || t.button < 1))
                    for (; l !== this; l = l.parentNode || this)
                        if (1 === l.nodeType && (!0 !== l.disabled || "click" !== t.type)) {
                            for (n = [], i = 0; a > i; i++) r = e[i], s = r.selector + " ", void 0 === n[s] && (n[s] = r.needsContext ? tt(s, this).index(l) > -1 : tt.find(s, this, null, [l]).length), n[s] && n.push(r);
                            n.length && o.push({
                                elem: l,
                                handlers: n
                            })
                        }
                return a < e.length && o.push({
                    elem: this,
                    handlers: e.slice(a)
                }), o
            },
            props: "altKey bubbles cancelable ctrlKey currentTarget detail eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
            fixHooks: {},
            keyHooks: {
                props: "char charCode key keyCode".split(" "),
                filter: function (t, e) {
                    return null == t.which && (t.which = null != e.charCode ? e.charCode : e.keyCode), t
                }
            },
            mouseHooks: {
                props: "button buttons clientX clientY offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
                filter: function (t, e) {
                    var i, n, s, r = e.button;
                    return null == t.pageX && null != e.clientX && (i = t.target.ownerDocument || F, n = i.documentElement, s = i.body, t.pageX = e.clientX + (n && n.scrollLeft || s && s.scrollLeft || 0) - (n && n.clientLeft || s && s.clientLeft || 0), t.pageY = e.clientY + (n && n.scrollTop || s && s.scrollTop || 0) - (n && n.clientTop || s && s.clientTop || 0)), t.which || void 0 === r || (t.which = 1 & r ? 1 : 2 & r ? 3 : 4 & r ? 2 : 0), t
                }
            },
            fix: function (t) {
                if (t[tt.expando]) return t;
                var e, i, n, s = t.type,
                    r = t,
                    o = this.fixHooks[s];
                for (o || (this.fixHooks[s] = o = Pt.test(s) ? this.mouseHooks : It.test(s) ? this.keyHooks : {}), n = o.props ? this.props.concat(o.props) : this.props, t = new tt.Event(r), e = n.length; e--;) i = n[e], t[i] = r[i];
                return t.target || (t.target = F), 3 === t.target.nodeType && (t.target = t.target.parentNode), o.filter ? o.filter(t, r) : t
            },
            special: {
                load: {
                    noBubble: !0
                },
                focus: {
                    trigger: function () {
                        return this !== f() && this.focus ? (this.focus(), !1) : void 0
                    },
                    delegateType: "focusin"
                },
                blur: {
                    trigger: function () {
                        return this === f() && this.blur ? (this.blur(), !1) : void 0
                    },
                    delegateType: "focusout"
                },
                click: {
                    trigger: function () {
                        return "checkbox" === this.type && this.click && tt.nodeName(this, "input") ? (this.click(), !1) : void 0
                    },
                    _default: function (t) {
                        return tt.nodeName(t.target, "a")
                    }
                },
                beforeunload: {
                    postDispatch: function (t) {
                        void 0 !== t.result && t.originalEvent && (t.originalEvent.returnValue = t.result)
                    }
                }
            }
        }, tt.removeEvent = function (t, e, i) {
            t.removeEventListener && t.removeEventListener(e, i)
        }, tt.Event = function (t, e) {
            return this instanceof tt.Event ? (t && t.type ? (this.originalEvent = t, this.type = t.type, this.isDefaultPrevented = t.defaultPrevented || void 0 === t.defaultPrevented && !1 === t.returnValue ? u : p) : this.type = t, e && tt.extend(this, e), this.timeStamp = t && t.timeStamp || tt.now(), void(this[tt.expando] = !0)) : new tt.Event(t, e)
        }, tt.Event.prototype = {
            constructor: tt.Event,
            isDefaultPrevented: p,
            isPropagationStopped: p,
            isImmediatePropagationStopped: p,
            preventDefault: function () {
                var t = this.originalEvent;
                this.isDefaultPrevented = u, t && t.preventDefault()
            },
            stopPropagation: function () {
                var t = this.originalEvent;
                this.isPropagationStopped = u, t && t.stopPropagation()
            },
            stopImmediatePropagation: function () {
                var t = this.originalEvent;
                this.isImmediatePropagationStopped = u, t && t.stopImmediatePropagation(), this.stopPropagation()
            }
        }, tt.each({
            mouseenter: "mouseover",
            mouseleave: "mouseout",
            pointerenter: "pointerover",
            pointerleave: "pointerout"
        }, function (t, e) {
            tt.event.special[t] = {
                delegateType: e,
                bindType: e,
                handle: function (t) {
                    var i, n = t.relatedTarget,
                        s = t.handleObj;
                    return n && (n === this || tt.contains(this, n)) || (t.type = s.origType, i = s.handler.apply(this, arguments), t.type = e), i
                }
            }
        }), tt.fn.extend({
            on: function (t, e, i, n) {
                return m(this, t, e, i, n)
            },
            one: function (t, e, i, n) {
                return m(this, t, e, i, n, 1)
            },
            off: function (t, e, i) {
                var n, s;
                if (t && t.preventDefault && t.handleObj) return n = t.handleObj, tt(t.delegateTarget).off(n.namespace ? n.origType + "." + n.namespace : n.origType, n.selector, n.handler), this;
                if ("object" == typeof t) {
                    for (s in t) this.off(s, e, t[s]);
                    return this
                }
                return !1 !== e && "function" != typeof e || (i = e, e = void 0), !1 === i && (i = p), this.each(function () {
                    tt.event.remove(this, t, i, e)
                })
            }
        });
        var Lt = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:-]+)[^>]*)\/>/gi,
            Ot = /<script|<style|<link/i,
            At = /checked\s*(?:[^=]|=\s*.checked.)/i,
            jt = /^true\/(.*)/,
            Ht = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;
        tt.extend({
            htmlPrefilter: function (t) {
                return t.replace(Lt, "<$1></$2>")
            },
            clone: function (t, e, i) {
                var n, s, r, o, a = t.cloneNode(!0),
                    l = tt.contains(t.ownerDocument, t);
                if (!(Z.noCloneChecked || 1 !== t.nodeType && 11 !== t.nodeType || tt.isXMLDoc(t)))
                    for (o = h(a), r = h(t), n = 0, s = r.length; s > n; n++) w(r[n], o[n]);
                if (e)
                    if (i)
                        for (r = r || h(t), o = o || h(a), n = 0, s = r.length; s > n; n++) b(r[n], o[n]);
                    else b(t, a);
                return (o = h(a, "script")).length > 0 && d(o, !l && h(t, "script")), a
            },
            cleanData: function (t) {
                for (var e, i, n, s = tt.event.special, r = 0; void 0 !== (i = t[r]); r++)
                    if (yt(i)) {
                        if (e = i[bt.expando]) {
                            if (e.events)
                                for (n in e.events) s[n] ? tt.event.remove(i, n) : tt.removeEvent(i, n, e.handle);
                            i[bt.expando] = void 0
                        }
                        i[wt.expando] && (i[wt.expando] = void 0)
                    }
            }
        }), tt.fn.extend({
            domManip: x,
            detach: function (t) {
                return C(this, t, !0)
            },
            remove: function (t) {
                return C(this, t)
            },
            text: function (t) {
                return vt(this, function (t) {
                    return void 0 === t ? tt.text(this) : this.empty().each(function () {
                        1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || (this.textContent = t)
                    })
                }, null, t, arguments.length)
            },
            append: function () {
                return x(this, arguments, function (t) {
                    if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                        g(this, t).appendChild(t)
                    }
                })
            },
            prepend: function () {
                return x(this, arguments, function (t) {
                    if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                        var e = g(this, t);
                        e.insertBefore(t, e.firstChild)
                    }
                })
            },
            before: function () {
                return x(this, arguments, function (t) {
                    this.parentNode && this.parentNode.insertBefore(t, this)
                })
            },
            after: function () {
                return x(this, arguments, function (t) {
                    this.parentNode && this.parentNode.insertBefore(t, this.nextSibling)
                })
            },
            empty: function () {
                for (var t, e = 0; null != (t = this[e]); e++) 1 === t.nodeType && (tt.cleanData(h(t, !1)), t.textContent = "");
                return this
            },
            clone: function (t, e) {
                return t = null != t && t, e = null == e ? t : e, this.map(function () {
                    return tt.clone(this, t, e)
                })
            },
            html: function (t) {
                return vt(this, function (t) {
                    var e = this[0] || {},
                        i = 0,
                        n = this.length;
                    if (void 0 === t && 1 === e.nodeType) return e.innerHTML;
                    if ("string" == typeof t && !Ot.test(t) && !Mt[($t.exec(t) || ["", ""])[1].toLowerCase()]) {
                        t = tt.htmlPrefilter(t);
                        try {
                            for (; n > i; i++) 1 === (e = this[i] || {}).nodeType && (tt.cleanData(h(e, !1)), e.innerHTML = t);
                            e = 0
                        } catch (t) {}
                    }
                    e && this.empty().append(t)
                }, null, t, arguments.length)
            },
            replaceWith: function () {
                var t = [];
                return x(this, arguments, function (e) {
                    var i = this.parentNode;
                    tt.inArray(this, t) < 0 && (tt.cleanData(h(this)), i && i.replaceChild(e, this))
                }, t)
            }
        }), tt.each({
            appendTo: "append",
            prependTo: "prepend",
            insertBefore: "before",
            insertAfter: "after",
            replaceAll: "replaceWith"
        }, function (t, e) {
            tt.fn[t] = function (t) {
                for (var i, n = [], s = tt(t), r = s.length - 1, o = 0; r >= o; o++) i = o === r ? this : this.clone(!0), tt(s[o])[e](i), Y.apply(n, i.get());
                return this.pushStack(n)
            }
        });
        var Rt, Wt = {
                HTML: "block",
                BODY: "block"
            },
            Bt = /^margin/,
            qt = new RegExp("^(" + Tt + ")(?!px)[a-z%]+$", "i"),
            Ft = function (e) {
                var i = e.ownerDocument.defaultView;
                return i && i.opener || (i = t), i.getComputedStyle(e)
            },
            Ut = function (t, e, i, n) {
                var s, r, o = {};
                for (r in e) o[r] = t.style[r], t.style[r] = e[r];
                s = i.apply(t, n || []);
                for (r in e) t.style[r] = o[r];
                return s
            },
            Xt = F.documentElement;
        ! function () {
            function e() {
                a.style.cssText = "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%", a.innerHTML = "", Xt.appendChild(o);
                var e = t.getComputedStyle(a);
                i = "1%" !== e.top, r = "2px" === e.marginLeft, n = "4px" === e.width, a.style.marginRight = "50%", s = "4px" === e.marginRight, Xt.removeChild(o)
            }
            var i, n, s, r, o = F.createElement("div"),
                a = F.createElement("div");
            a.style && (a.style.backgroundClip = "content-box", a.cloneNode(!0).style.backgroundClip = "", Z.clearCloneStyle = "content-box" === a.style.backgroundClip, o.style.cssText = "border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute", o.appendChild(a), tt.extend(Z, {
                pixelPosition: function () {
                    return e(), i
                },
                boxSizingReliable: function () {
                    return null == n && e(), n
                },
                pixelMarginRight: function () {
                    return null == n && e(), s
                },
                reliableMarginLeft: function () {
                    return null == n && e(), r
                },
                reliableMarginRight: function () {
                    var e, i = a.appendChild(F.createElement("div"));
                    return i.style.cssText = a.style.cssText = "-webkit-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0", i.style.marginRight = i.style.width = "0", a.style.width = "1px", Xt.appendChild(o), e = !parseFloat(t.getComputedStyle(i).marginRight), Xt.removeChild(o), a.removeChild(i), e
                }
            }))
        }();
        var Yt = /^(none|table(?!-c[ea]).+)/,
            Gt = {
                position: "absolute",
                visibility: "hidden",
                display: "block"
            },
            Vt = {
                letterSpacing: "0",
                fontWeight: "400"
            },
            Qt = ["Webkit", "O", "Moz", "ms"],
            Kt = F.createElement("div").style;
        tt.extend({
            cssHooks: {
                opacity: {
                    get: function (t, e) {
                        if (e) {
                            var i = S(t, "opacity");
                            return "" === i ? "1" : i
                        }
                    }
                }
            },
            cssNumber: {
                animationIterationCount: !0,
                columnCount: !0,
                fillOpacity: !0,
                flexGrow: !0,
                flexShrink: !0,
                fontWeight: !0,
                lineHeight: !0,
                opacity: !0,
                order: !0,
                orphans: !0,
                widows: !0,
                zIndex: !0,
                zoom: !0
            },
            cssProps: {
                float: "cssFloat"
            },
            style: function (t, e, i, n) {
                if (t && 3 !== t.nodeType && 8 !== t.nodeType && t.style) {
                    var s, r, o, a = tt.camelCase(e),
                        h = t.style;
                    return e = tt.cssProps[a] || (tt.cssProps[a] = k(a) || a), o = tt.cssHooks[e] || tt.cssHooks[a], void 0 === i ? o && "get" in o && void 0 !== (s = o.get(t, !1, n)) ? s : h[e] : ("string" === (r = typeof i) && (s = Et.exec(i)) && s[1] && (i = l(t, e, s), r = "number"), void(null != i && i == i && ("number" === r && (i += s && s[3] || (tt.cssNumber[a] ? "" : "px")), Z.clearCloneStyle || "" !== i || 0 !== e.indexOf("background") || (h[e] = "inherit"), o && "set" in o && void 0 === (i = o.set(t, i, n)) || (h[e] = i))))
                }
            },
            css: function (t, e, i, n) {
                var s, r, o, a = tt.camelCase(e);
                return e = tt.cssProps[a] || (tt.cssProps[a] = k(a) || a), (o = tt.cssHooks[e] || tt.cssHooks[a]) && "get" in o && (s = o.get(t, !0, i)), void 0 === s && (s = S(t, e, n)), "normal" === s && e in Vt && (s = Vt[e]), "" === i || i ? (r = parseFloat(s), !0 === i || isFinite(r) ? r || 0 : s) : s
            }
        }), tt.each(["height", "width"], function (t, e) {
            tt.cssHooks[e] = {
                get: function (t, i, n) {
                    return i ? Yt.test(tt.css(t, "display")) && 0 === t.offsetWidth ? Ut(t, Gt, function () {
                        return M(t, e, n)
                    }) : M(t, e, n) : void 0
                },
                set: function (t, i, n) {
                    var s, r = n && Ft(t),
                        o = n && z(t, e, n, "border-box" === tt.css(t, "boxSizing", !1, r), r);
                    return o && (s = Et.exec(i)) && "px" !== (s[3] || "px") && (t.style[e] = i, i = tt.css(t, e)), $(0, i, o)
                }
            }
        }), tt.cssHooks.marginLeft = _(Z.reliableMarginLeft, function (t, e) {
            return e ? (parseFloat(S(t, "marginLeft")) || t.getBoundingClientRect().left - Ut(t, {
                marginLeft: 0
            }, function () {
                return t.getBoundingClientRect().left
            })) + "px" : void 0
        }), tt.cssHooks.marginRight = _(Z.reliableMarginRight, function (t, e) {
            return e ? Ut(t, {
                display: "inline-block"
            }, S, [t, "marginRight"]) : void 0
        }), tt.each({
            margin: "",
            padding: "",
            border: "Width"
        }, function (t, e) {
            tt.cssHooks[t + e] = {
                expand: function (i) {
                    for (var n = 0, s = {}, r = "string" == typeof i ? i.split(" ") : [i]; 4 > n; n++) s[t + St[n] + e] = r[n] || r[n - 2] || r[0];
                    return s
                }
            }, Bt.test(t) || (tt.cssHooks[t + e].set = $)
        }), tt.fn.extend({
            css: function (t, e) {
                return vt(this, function (t, e, i) {
                    var n, s, r = {},
                        o = 0;
                    if (tt.isArray(e)) {
                        for (n = Ft(t), s = e.length; s > o; o++) r[e[o]] = tt.css(t, e[o], !1, n);
                        return r
                    }
                    return void 0 !== i ? tt.style(t, e, i) : tt.css(t, e)
                }, t, e, arguments.length > 1)
            },
            show: function () {
                return D(this, !0)
            },
            hide: function () {
                return D(this)
            },
            toggle: function (t) {
                return "boolean" == typeof t ? t ? this.show() : this.hide() : this.each(function () {
                    _t(this) ? tt(this).show() : tt(this).hide()
                })
            }
        }), tt.Tween = I, (I.prototype = {
            constructor: I,
            init: function (t, e, i, n, s, r) {
                this.elem = t, this.prop = i, this.easing = s || tt.easing._default, this.options = e, this.start = this.now = this.cur(), this.end = n, this.unit = r || (tt.cssNumber[i] ? "" : "px")
            },
            cur: function () {
                var t = I.propHooks[this.prop];
                return t && t.get ? t.get(this) : I.propHooks._default.get(this)
            },
            run: function (t) {
                var e, i = I.propHooks[this.prop];
                return this.options.duration ? this.pos = e = tt.easing[this.easing](t, this.options.duration * t, 0, 1, this.options.duration) : this.pos = e = t, this.now = (this.end - this.start) * e + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), i && i.set ? i.set(this) : I.propHooks._default.set(this), this
            }
        }).init.prototype = I.prototype, (I.propHooks = {
            _default: {
                get: function (t) {
                    var e;
                    return 1 !== t.elem.nodeType || null != t.elem[t.prop] && null == t.elem.style[t.prop] ? t.elem[t.prop] : (e = tt.css(t.elem, t.prop, "")) && "auto" !== e ? e : 0
                },
                set: function (t) {
                    tt.fx.step[t.prop] ? tt.fx.step[t.prop](t) : 1 !== t.elem.nodeType || null == t.elem.style[tt.cssProps[t.prop]] && !tt.cssHooks[t.prop] ? t.elem[t.prop] = t.now : tt.style(t.elem, t.prop, t.now + t.unit)
                }
            }
        }).scrollTop = I.propHooks.scrollLeft = {
            set: function (t) {
                t.elem.nodeType && t.elem.parentNode && (t.elem[t.prop] = t.now)
            }
        }, tt.easing = {
            linear: function (t) {
                return t
            },
            swing: function (t) {
                return .5 - Math.cos(t * Math.PI) / 2
            },
            _default: "swing"
        }, tt.fx = I.prototype.init, tt.fx.step = {};
        var Zt, Jt, te = /^(?:toggle|show|hide)$/,
            ee = /queueHooks$/;
        tt.Animation = tt.extend(O, {
                tweeners: {
                    "*": [function (t, e) {
                        var i = this.createTween(t, e);
                        return l(i.elem, t, Et.exec(e), i), i
                    }]
                },
                tweener: function (t, e) {
                    tt.isFunction(t) ? (e = t, t = ["*"]) : t = t.match(mt);
                    for (var i, n = 0, s = t.length; s > n; n++) i = t[n], O.tweeners[i] = O.tweeners[i] || [], O.tweeners[i].unshift(e)
                },
                prefilters: [function (t, e, i) {
                    var n, s, r, o, a, l, h, d = this,
                        c = {},
                        u = t.style,
                        p = t.nodeType && _t(t),
                        f = bt.get(t, "fxshow");
                    i.queue || (null == (a = tt._queueHooks(t, "fx")).unqueued && (a.unqueued = 0, l = a.empty.fire, a.empty.fire = function () {
                        a.unqueued || l()
                    }), a.unqueued++, d.always(function () {
                        d.always(function () {
                            a.unqueued--, tt.queue(t, "fx").length || a.empty.fire()
                        })
                    })), 1 === t.nodeType && ("height" in e || "width" in e) && (i.overflow = [u.overflow, u.overflowX, u.overflowY], "inline" === ("none" === (h = tt.css(t, "display")) ? bt.get(t, "olddisplay") || E(t.nodeName) : h) && "none" === tt.css(t, "float") && (u.display = "inline-block")), i.overflow && (u.overflow = "hidden", d.always(function () {
                        u.overflow = i.overflow[0], u.overflowX = i.overflow[1], u.overflowY = i.overflow[2]
                    }));
                    for (n in e)
                        if (s = e[n], te.exec(s)) {
                            if (delete e[n], r = r || "toggle" === s, s === (p ? "hide" : "show")) {
                                if ("show" !== s || !f || void 0 === f[n]) continue;
                                p = !0
                            }
                            c[n] = f && f[n] || tt.style(t, n)
                        } else h = void 0;
                    if (tt.isEmptyObject(c)) "inline" === ("none" === h ? E(t.nodeName) : h) && (u.display = h);
                    else {
                        f ? "hidden" in f && (p = f.hidden) : f = bt.access(t, "fxshow", {}), r && (f.hidden = !p), p ? tt(t).show() : d.done(function () {
                            tt(t).hide()
                        }), d.done(function () {
                            var e;
                            bt.remove(t, "fxshow");
                            for (e in c) tt.style(t, e, c[e])
                        });
                        for (n in c) o = L(p ? f[n] : 0, n, d), n in f || (f[n] = o.start, p && (o.end = o.start, o.start = "width" === n || "height" === n ? 1 : 0))
                    }
                }],
                prefilter: function (t, e) {
                    e ? O.prefilters.unshift(t) : O.prefilters.push(t)
                }
            }), tt.speed = function (t, e, i) {
                var n = t && "object" == typeof t ? tt.extend({}, t) : {
                    complete: i || !i && e || tt.isFunction(t) && t,
                    duration: t,
                    easing: i && e || e && !tt.isFunction(e) && e
                };
                return n.duration = tt.fx.off ? 0 : "number" == typeof n.duration ? n.duration : n.duration in tt.fx.speeds ? tt.fx.speeds[n.duration] : tt.fx.speeds._default, null != n.queue && !0 !== n.queue || (n.queue = "fx"), n.old = n.complete, n.complete = function () {
                    tt.isFunction(n.old) && n.old.call(this), n.queue && tt.dequeue(this, n.queue)
                }, n
            }, tt.fn.extend({
                fadeTo: function (t, e, i, n) {
                    return this.filter(_t).css("opacity", 0).show().end().animate({
                        opacity: e
                    }, t, i, n)
                },
                animate: function (t, e, i, n) {
                    var s = tt.isEmptyObject(t),
                        r = tt.speed(e, i, n),
                        o = function () {
                            var e = O(this, tt.extend({}, t), r);
                            (s || bt.get(this, "finish")) && e.stop(!0)
                        };
                    return o.finish = o, s || !1 === r.queue ? this.each(o) : this.queue(r.queue, o)
                },
                stop: function (t, e, i) {
                    var n = function (t) {
                        var e = t.stop;
                        delete t.stop, e(i)
                    };
                    return "string" != typeof t && (i = e, e = t, t = void 0), e && !1 !== t && this.queue(t || "fx", []), this.each(function () {
                        var e = !0,
                            s = null != t && t + "queueHooks",
                            r = tt.timers,
                            o = bt.get(this);
                        if (s) o[s] && o[s].stop && n(o[s]);
                        else
                            for (s in o) o[s] && o[s].stop && ee.test(s) && n(o[s]);
                        for (s = r.length; s--;) r[s].elem !== this || null != t && r[s].queue !== t || (r[s].anim.stop(i), e = !1, r.splice(s, 1));
                        !e && i || tt.dequeue(this, t)
                    })
                },
                finish: function (t) {
                    return !1 !== t && (t = t || "fx"), this.each(function () {
                        var e, i = bt.get(this),
                            n = i[t + "queue"],
                            s = i[t + "queueHooks"],
                            r = tt.timers,
                            o = n ? n.length : 0;
                        for (i.finish = !0, tt.queue(this, t, []), s && s.stop && s.stop.call(this, !0), e = r.length; e--;) r[e].elem === this && r[e].queue === t && (r[e].anim.stop(!0), r.splice(e, 1));
                        for (e = 0; o > e; e++) n[e] && n[e].finish && n[e].finish.call(this);
                        delete i.finish
                    })
                }
            }), tt.each(["toggle", "show", "hide"], function (t, e) {
                var i = tt.fn[e];
                tt.fn[e] = function (t, n, s) {
                    return null == t || "boolean" == typeof t ? i.apply(this, arguments) : this.animate(N(e, !0), t, n, s)
                }
            }), tt.each({
                slideDown: N("show"),
                slideUp: N("hide"),
                slideToggle: N("toggle"),
                fadeIn: {
                    opacity: "show"
                },
                fadeOut: {
                    opacity: "hide"
                },
                fadeToggle: {
                    opacity: "toggle"
                }
            }, function (t, e) {
                tt.fn[t] = function (t, i, n) {
                    return this.animate(e, t, i, n)
                }
            }), tt.timers = [], tt.fx.tick = function () {
                var t, e = 0,
                    i = tt.timers;
                for (Zt = tt.now(); e < i.length; e++)(t = i[e])() || i[e] !== t || i.splice(e--, 1);
                i.length || tt.fx.stop(), Zt = void 0
            }, tt.fx.timer = function (t) {
                tt.timers.push(t), t() ? tt.fx.start() : tt.timers.pop()
            }, tt.fx.interval = 13, tt.fx.start = function () {
                Jt || (Jt = t.setInterval(tt.fx.tick, tt.fx.interval))
            }, tt.fx.stop = function () {
                t.clearInterval(Jt), Jt = null
            }, tt.fx.speeds = {
                slow: 600,
                fast: 200,
                _default: 400
            }, tt.fn.delay = function (e, i) {
                return e = tt.fx ? tt.fx.speeds[e] || e : e, i = i || "fx", this.queue(i, function (i, n) {
                    var s = t.setTimeout(i, e);
                    n.stop = function () {
                        t.clearTimeout(s)
                    }
                })
            },
            function () {
                var t = F.createElement("input"),
                    e = F.createElement("select"),
                    i = e.appendChild(F.createElement("option"));
                t.type = "checkbox", Z.checkOn = "" !== t.value, Z.optSelected = i.selected, e.disabled = !0, Z.optDisabled = !i.disabled, (t = F.createElement("input")).value = "t", t.type = "radio", Z.radioValue = "t" === t.value
            }();
        var ie, ne = tt.expr.attrHandle;
        tt.fn.extend({
            attr: function (t, e) {
                return vt(this, tt.attr, t, e, arguments.length > 1)
            },
            removeAttr: function (t) {
                return this.each(function () {
                    tt.removeAttr(this, t)
                })
            }
        }), tt.extend({
            attr: function (t, e, i) {
                var n, s, r = t.nodeType;
                if (3 !== r && 8 !== r && 2 !== r) return void 0 === t.getAttribute ? tt.prop(t, e, i) : (1 === r && tt.isXMLDoc(t) || (e = e.toLowerCase(), s = tt.attrHooks[e] || (tt.expr.match.bool.test(e) ? ie : void 0)), void 0 !== i ? null === i ? void tt.removeAttr(t, e) : s && "set" in s && void 0 !== (n = s.set(t, i, e)) ? n : (t.setAttribute(e, i + ""), i) : s && "get" in s && null !== (n = s.get(t, e)) ? n : null == (n = tt.find.attr(t, e)) ? void 0 : n)
            },
            attrHooks: {
                type: {
                    set: function (t, e) {
                        if (!Z.radioValue && "radio" === e && tt.nodeName(t, "input")) {
                            var i = t.value;
                            return t.setAttribute("type", e), i && (t.value = i), e
                        }
                    }
                }
            },
            removeAttr: function (t, e) {
                var i, n, s = 0,
                    r = e && e.match(mt);
                if (r && 1 === t.nodeType)
                    for (; i = r[s++];) n = tt.propFix[i] || i, tt.expr.match.bool.test(i) && (t[n] = !1), t.removeAttribute(i)
            }
        }), ie = {
            set: function (t, e, i) {
                return !1 === e ? tt.removeAttr(t, i) : t.setAttribute(i, i), i
            }
        }, tt.each(tt.expr.match.bool.source.match(/\w+/g), function (t, e) {
            var i = ne[e] || tt.find.attr;
            ne[e] = function (t, e, n) {
                var s, r;
                return n || (r = ne[e], ne[e] = s, s = null != i(t, e, n) ? e.toLowerCase() : null, ne[e] = r), s
            }
        });
        var se = /^(?:input|select|textarea|button)$/i,
            re = /^(?:a|area)$/i;
        tt.fn.extend({
            prop: function (t, e) {
                return vt(this, tt.prop, t, e, arguments.length > 1)
            },
            removeProp: function (t) {
                return this.each(function () {
                    delete this[tt.propFix[t] || t]
                })
            }
        }), tt.extend({
            prop: function (t, e, i) {
                var n, s, r = t.nodeType;
                if (3 !== r && 8 !== r && 2 !== r) return 1 === r && tt.isXMLDoc(t) || (e = tt.propFix[e] || e, s = tt.propHooks[e]), void 0 !== i ? s && "set" in s && void 0 !== (n = s.set(t, i, e)) ? n : t[e] = i : s && "get" in s && null !== (n = s.get(t, e)) ? n : t[e]
            },
            propHooks: {
                tabIndex: {
                    get: function (t) {
                        var e = tt.find.attr(t, "tabindex");
                        return e ? parseInt(e, 10) : se.test(t.nodeName) || re.test(t.nodeName) && t.href ? 0 : -1
                    }
                }
            },
            propFix: {
                for: "htmlFor",
                class: "className"
            }
        }), Z.optSelected || (tt.propHooks.selected = {
            get: function (t) {
                var e = t.parentNode;
                return e && e.parentNode && e.parentNode.selectedIndex, null
            },
            set: function (t) {
                var e = t.parentNode;
                e && (e.selectedIndex, e.parentNode && e.parentNode.selectedIndex)
            }
        }), tt.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function () {
            tt.propFix[this.toLowerCase()] = this
        });
        var oe = /[\t\r\n\f]/g;
        tt.fn.extend({
            addClass: function (t) {
                var e, i, n, s, r, o, a, l = 0;
                if (tt.isFunction(t)) return this.each(function (e) {
                    tt(this).addClass(t.call(this, e, A(this)))
                });
                if ("string" == typeof t && t)
                    for (e = t.match(mt) || []; i = this[l++];)
                        if (s = A(i), n = 1 === i.nodeType && (" " + s + " ").replace(oe, " ")) {
                            for (o = 0; r = e[o++];) n.indexOf(" " + r + " ") < 0 && (n += r + " ");
                            s !== (a = tt.trim(n)) && i.setAttribute("class", a)
                        }
                return this
            },
            removeClass: function (t) {
                var e, i, n, s, r, o, a, l = 0;
                if (tt.isFunction(t)) return this.each(function (e) {
                    tt(this).removeClass(t.call(this, e, A(this)))
                });
                if (!arguments.length) return this.attr("class", "");
                if ("string" == typeof t && t)
                    for (e = t.match(mt) || []; i = this[l++];)
                        if (s = A(i), n = 1 === i.nodeType && (" " + s + " ").replace(oe, " ")) {
                            for (o = 0; r = e[o++];)
                                for (; n.indexOf(" " + r + " ") > -1;) n = n.replace(" " + r + " ", " ");
                            s !== (a = tt.trim(n)) && i.setAttribute("class", a)
                        }
                return this
            },
            toggleClass: function (t, e) {
                var i = typeof t;
                return "boolean" == typeof e && "string" === i ? e ? this.addClass(t) : this.removeClass(t) : tt.isFunction(t) ? this.each(function (i) {
                    tt(this).toggleClass(t.call(this, i, A(this), e), e)
                }) : this.each(function () {
                    var e, n, s, r;
                    if ("string" === i)
                        for (n = 0, s = tt(this), r = t.match(mt) || []; e = r[n++];) s.hasClass(e) ? s.removeClass(e) : s.addClass(e);
                    else void 0 !== t && "boolean" !== i || ((e = A(this)) && bt.set(this, "__className__", e), this.setAttribute && this.setAttribute("class", e || !1 === t ? "" : bt.get(this, "__className__") || ""))
                })
            },
            hasClass: function (t) {
                var e, i, n = 0;
                for (e = " " + t + " "; i = this[n++];)
                    if (1 === i.nodeType && (" " + A(i) + " ").replace(oe, " ").indexOf(e) > -1) return !0;
                return !1
            }
        });
        var ae = /\r/g,
            le = /[\x20\t\r\n\f]+/g;
        tt.fn.extend({
            val: function (t) {
                var e, i, n, s = this[0];
                return arguments.length ? (n = tt.isFunction(t), this.each(function (i) {
                    var s;
                    1 === this.nodeType && (null == (s = n ? t.call(this, i, tt(this).val()) : t) ? s = "" : "number" == typeof s ? s += "" : tt.isArray(s) && (s = tt.map(s, function (t) {
                        return null == t ? "" : t + ""
                    })), (e = tt.valHooks[this.type] || tt.valHooks[this.nodeName.toLowerCase()]) && "set" in e && void 0 !== e.set(this, s, "value") || (this.value = s))
                })) : s ? (e = tt.valHooks[s.type] || tt.valHooks[s.nodeName.toLowerCase()]) && "get" in e && void 0 !== (i = e.get(s, "value")) ? i : "string" == typeof (i = s.value) ? i.replace(ae, "") : null == i ? "" : i : void 0
            }
        }), tt.extend({
            valHooks: {
                option: {
                    get: function (t) {
                        var e = tt.find.attr(t, "value");
                        return null != e ? e : tt.trim(tt.text(t)).replace(le, " ")
                    }
                },
                select: {
                    get: function (t) {
                        for (var e, i, n = t.options, s = t.selectedIndex, r = "select-one" === t.type || 0 > s, o = r ? null : [], a = r ? s + 1 : n.length, l = 0 > s ? a : r ? s : 0; a > l; l++)
                            if (((i = n[l]).selected || l === s) && (Z.optDisabled ? !i.disabled : null === i.getAttribute("disabled")) && (!i.parentNode.disabled || !tt.nodeName(i.parentNode, "optgroup"))) {
                                if (e = tt(i).val(), r) return e;
                                o.push(e)
                            }
                        return o
                    },
                    set: function (t, e) {
                        for (var i, n, s = t.options, r = tt.makeArray(e), o = s.length; o--;) n = s[o], (n.selected = tt.inArray(tt.valHooks.option.get(n), r) > -1) && (i = !0);
                        return i || (t.selectedIndex = -1), r
                    }
                }
            }
        }), tt.each(["radio", "checkbox"], function () {
            tt.valHooks[this] = {
                set: function (t, e) {
                    return tt.isArray(e) ? t.checked = tt.inArray(tt(t).val(), e) > -1 : void 0
                }
            }, Z.checkOn || (tt.valHooks[this].get = function (t) {
                return null === t.getAttribute("value") ? "on" : t.value
            })
        });
        var he = /^(?:focusinfocus|focusoutblur)$/;
        tt.extend(tt.event, {
            trigger: function (e, i, n, s) {
                var r, o, a, l, h, d, c, u = [n || F],
                    p = K.call(e, "type") ? e.type : e,
                    f = K.call(e, "namespace") ? e.namespace.split(".") : [];
                if (o = a = n = n || F, 3 !== n.nodeType && 8 !== n.nodeType && !he.test(p + tt.event.triggered) && (p.indexOf(".") > -1 && (f = p.split("."), p = f.shift(), f.sort()), h = p.indexOf(":") < 0 && "on" + p, e = e[tt.expando] ? e : new tt.Event(p, "object" == typeof e && e), e.isTrigger = s ? 2 : 3, e.namespace = f.join("."), e.rnamespace = e.namespace ? new RegExp("(^|\\.)" + f.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, e.result = void 0, e.target || (e.target = n), i = null == i ? [e] : tt.makeArray(i, [e]), c = tt.event.special[p] || {}, s || !c.trigger || !1 !== c.trigger.apply(n, i))) {
                    if (!s && !c.noBubble && !tt.isWindow(n)) {
                        for (l = c.delegateType || p, he.test(l + p) || (o = o.parentNode); o; o = o.parentNode) u.push(o), a = o;
                        a === (n.ownerDocument || F) && u.push(a.defaultView || a.parentWindow || t)
                    }
                    for (r = 0;
                        (o = u[r++]) && !e.isPropagationStopped();) e.type = r > 1 ? l : c.bindType || p, (d = (bt.get(o, "events") || {})[e.type] && bt.get(o, "handle")) && d.apply(o, i), (d = h && o[h]) && d.apply && yt(o) && (e.result = d.apply(o, i), !1 === e.result && e.preventDefault());
                    return e.type = p, s || e.isDefaultPrevented() || c._default && !1 !== c._default.apply(u.pop(), i) || !yt(n) || h && tt.isFunction(n[p]) && !tt.isWindow(n) && ((a = n[h]) && (n[h] = null), tt.event.triggered = p, n[p](), tt.event.triggered = void 0, a && (n[h] = a)), e.result
                }
            },
            simulate: function (t, e, i) {
                var n = tt.extend(new tt.Event, i, {
                    type: t,
                    isSimulated: !0
                });
                tt.event.trigger(n, null, e), n.isDefaultPrevented() && i.preventDefault()
            }
        }), tt.fn.extend({
            trigger: function (t, e) {
                return this.each(function () {
                    tt.event.trigger(t, e, this)
                })
            },
            triggerHandler: function (t, e) {
                var i = this[0];
                return i ? tt.event.trigger(t, e, i, !0) : void 0
            }
        }), tt.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function (t, e) {
            tt.fn[e] = function (t, i) {
                return arguments.length > 0 ? this.on(e, null, t, i) : this.trigger(e)
            }
        }), tt.fn.extend({
            hover: function (t, e) {
                return this.mouseenter(t).mouseleave(e || t)
            }
        }), Z.focusin = "onfocusin" in t, Z.focusin || tt.each({
            focus: "focusin",
            blur: "focusout"
        }, function (t, e) {
            var i = function (t) {
                tt.event.simulate(e, t.target, tt.event.fix(t))
            };
            tt.event.special[e] = {
                setup: function () {
                    var n = this.ownerDocument || this,
                        s = bt.access(n, e);
                    s || n.addEventListener(t, i, !0), bt.access(n, e, (s || 0) + 1)
                },
                teardown: function () {
                    var n = this.ownerDocument || this,
                        s = bt.access(n, e) - 1;
                    s ? bt.access(n, e, s) : (n.removeEventListener(t, i, !0), bt.remove(n, e))
                }
            }
        });
        var de = t.location,
            ce = tt.now(),
            ue = /\?/;
        tt.parseJSON = function (t) {
            return JSON.parse(t + "")
        }, tt.parseXML = function (e) {
            var i;
            if (!e || "string" != typeof e) return null;
            try {
                i = (new t.DOMParser).parseFromString(e, "text/xml")
            } catch (t) {
                i = void 0
            }
            return i && !i.getElementsByTagName("parsererror").length || tt.error("Invalid XML: " + e), i
        };
        var pe = /#.*$/,
            fe = /([?&])_=[^&]*/,
            me = /^(.*?):[ \t]*([^\r\n]*)$/gm,
            ge = /^(?:GET|HEAD)$/,
            ve = /^\/\//,
            ye = {},
            be = {},
            we = "*/".concat("*"),
            xe = F.createElement("a");
        xe.href = de.href, tt.extend({
            active: 0,
            lastModified: {},
            etag: {},
            ajaxSettings: {
                url: de.href,
                type: "GET",
                isLocal: /^(?:about|app|app-storage|.+-extension|file|res|widget):$/.test(de.protocol),
                global: !0,
                processData: !0,
                async: !0,
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                accepts: {
                    "*": we,
                    text: "text/plain",
                    html: "text/html",
                    xml: "application/xml, text/xml",
                    json: "application/json, text/javascript"
                },
                contents: {
                    xml: /\bxml\b/,
                    html: /\bhtml/,
                    json: /\bjson\b/
                },
                responseFields: {
                    xml: "responseXML",
                    text: "responseText",
                    json: "responseJSON"
                },
                converters: {
                    "* text": String,
                    "text html": !0,
                    "text json": tt.parseJSON,
                    "text xml": tt.parseXML
                },
                flatOptions: {
                    url: !0,
                    context: !0
                }
            },
            ajaxSetup: function (t, e) {
                return e ? R(R(t, tt.ajaxSettings), e) : R(tt.ajaxSettings, t)
            },
            ajaxPrefilter: j(ye),
            ajaxTransport: j(be),
            ajax: function (e, i) {
                function n(e, i, n, a) {
                    var h, c, y, b, x, T = i;
                    2 !== w && (w = 2, l && t.clearTimeout(l), s = void 0, o = a || "", C.readyState = e > 0 ? 4 : 0, h = e >= 200 && 300 > e || 304 === e, n && (b = function (t, e, i) {
                        for (var n, s, r, o, a = t.contents, l = t.dataTypes;
                            "*" === l[0];) l.shift(), void 0 === n && (n = t.mimeType || e.getResponseHeader("Content-Type"));
                        if (n)
                            for (s in a)
                                if (a[s] && a[s].test(n)) {
                                    l.unshift(s);
                                    break
                                }
                        if (l[0] in i) r = l[0];
                        else {
                            for (s in i) {
                                if (!l[0] || t.converters[s + " " + l[0]]) {
                                    r = s;
                                    break
                                }
                                o || (o = s)
                            }
                            r = r || o
                        }
                        return r ? (r !== l[0] && l.unshift(r), i[r]) : void 0
                    }(u, C, n)), b = function (t, e, i, n) {
                        var s, r, o, a, l, h = {},
                            d = t.dataTypes.slice();
                        if (d[1])
                            for (o in t.converters) h[o.toLowerCase()] = t.converters[o];
                        for (r = d.shift(); r;)
                            if (t.responseFields[r] && (i[t.responseFields[r]] = e), !l && n && t.dataFilter && (e = t.dataFilter(e, t.dataType)), l = r, r = d.shift())
                                if ("*" === r) r = l;
                                else if ("*" !== l && l !== r) {
                            if (!(o = h[l + " " + r] || h["* " + r]))
                                for (s in h)
                                    if ((a = s.split(" "))[1] === r && (o = h[l + " " + a[0]] || h["* " + a[0]])) {
                                        !0 === o ? o = h[s] : !0 !== h[s] && (r = a[0], d.unshift(a[1]));
                                        break
                                    }
                            if (!0 !== o)
                                if (o && t.throws) e = o(e);
                                else try {
                                    e = o(e)
                                } catch (t) {
                                    return {
                                        state: "parsererror",
                                        error: o ? t : "No conversion from " + l + " to " + r
                                    }
                                }
                        }
                        return {
                            state: "success",
                            data: e
                        }
                    }(u, b, C, h), h ? (u.ifModified && ((x = C.getResponseHeader("Last-Modified")) && (tt.lastModified[r] = x), (x = C.getResponseHeader("etag")) && (tt.etag[r] = x)), 204 === e || "HEAD" === u.type ? T = "nocontent" : 304 === e ? T = "notmodified" : (T = b.state, c = b.data, y = b.error, h = !y)) : (y = T, !e && T || (T = "error", 0 > e && (e = 0))), C.status = e, C.statusText = (i || T) + "", h ? m.resolveWith(p, [c, T, C]) : m.rejectWith(p, [C, T, y]), C.statusCode(v), v = void 0, d && f.trigger(h ? "ajaxSuccess" : "ajaxError", [C, u, h ? c : y]), g.fireWith(p, [C, T]), d && (f.trigger("ajaxComplete", [C, u]), --tt.active || tt.event.trigger("ajaxStop")))
                }
                "object" == typeof e && (i = e, e = void 0), i = i || {};
                var s, r, o, a, l, h, d, c, u = tt.ajaxSetup({}, i),
                    p = u.context || u,
                    f = u.context && (p.nodeType || p.jquery) ? tt(p) : tt.event,
                    m = tt.Deferred(),
                    g = tt.Callbacks("once memory"),
                    v = u.statusCode || {},
                    y = {},
                    b = {},
                    w = 0,
                    x = "canceled",
                    C = {
                        readyState: 0,
                        getResponseHeader: function (t) {
                            var e;
                            if (2 === w) {
                                if (!a)
                                    for (a = {}; e = me.exec(o);) a[e[1].toLowerCase()] = e[2];
                                e = a[t.toLowerCase()]
                            }
                            return null == e ? null : e
                        },
                        getAllResponseHeaders: function () {
                            return 2 === w ? o : null
                        },
                        setRequestHeader: function (t, e) {
                            var i = t.toLowerCase();
                            return w || (t = b[i] = b[i] || t, y[t] = e), this
                        },
                        overrideMimeType: function (t) {
                            return w || (u.mimeType = t), this
                        },
                        statusCode: function (t) {
                            var e;
                            if (t)
                                if (2 > w)
                                    for (e in t) v[e] = [v[e], t[e]];
                                else C.always(t[C.status]);
                            return this
                        },
                        abort: function (t) {
                            var e = t || x;
                            return s && s.abort(e), n(0, e), this
                        }
                    };
                if (m.promise(C).complete = g.add, C.success = C.done, C.error = C.fail, u.url = ((e || u.url || de.href) + "").replace(pe, "").replace(ve, de.protocol + "//"), u.type = i.method || i.type || u.method || u.type, u.dataTypes = tt.trim(u.dataType || "*").toLowerCase().match(mt) || [""], null == u.crossDomain) {
                    h = F.createElement("a");
                    try {
                        h.href = u.url, h.href = h.href, u.crossDomain = xe.protocol + "//" + xe.host != h.protocol + "//" + h.host
                    } catch (t) {
                        u.crossDomain = !0
                    }
                }
                if (u.data && u.processData && "string" != typeof u.data && (u.data = tt.param(u.data, u.traditional)), H(ye, u, i, C), 2 === w) return C;
                (d = tt.event && u.global) && 0 == tt.active++ && tt.event.trigger("ajaxStart"), u.type = u.type.toUpperCase(), u.hasContent = !ge.test(u.type), r = u.url, u.hasContent || (u.data && (r = u.url += (ue.test(r) ? "&" : "?") + u.data, delete u.data), !1 === u.cache && (u.url = fe.test(r) ? r.replace(fe, "$1_=" + ce++) : r + (ue.test(r) ? "&" : "?") + "_=" + ce++)), u.ifModified && (tt.lastModified[r] && C.setRequestHeader("If-Modified-Since", tt.lastModified[r]), tt.etag[r] && C.setRequestHeader("If-None-Match", tt.etag[r])), (u.data && u.hasContent && !1 !== u.contentType || i.contentType) && C.setRequestHeader("Content-Type", u.contentType), C.setRequestHeader("Accept", u.dataTypes[0] && u.accepts[u.dataTypes[0]] ? u.accepts[u.dataTypes[0]] + ("*" !== u.dataTypes[0] ? ", " + we + "; q=0.01" : "") : u.accepts["*"]);
                for (c in u.headers) C.setRequestHeader(c, u.headers[c]);
                if (u.beforeSend && (!1 === u.beforeSend.call(p, C, u) || 2 === w)) return C.abort();
                x = "abort";
                for (c in {
                        success: 1,
                        error: 1,
                        complete: 1
                    }) C[c](u[c]);
                if (s = H(be, u, i, C)) {
                    if (C.readyState = 1, d && f.trigger("ajaxSend", [C, u]), 2 === w) return C;
                    u.async && u.timeout > 0 && (l = t.setTimeout(function () {
                        C.abort("timeout")
                    }, u.timeout));
                    try {
                        w = 1, s.send(y, n)
                    } catch (t) {
                        if (!(2 > w)) throw t;
                        n(-1, t)
                    }
                } else n(-1, "No Transport");
                return C
            },
            getJSON: function (t, e, i) {
                return tt.get(t, e, i, "json")
            },
            getScript: function (t, e) {
                return tt.get(t, void 0, e, "script")
            }
        }), tt.each(["get", "post"], function (t, e) {
            tt[e] = function (t, i, n, s) {
                return tt.isFunction(i) && (s = s || n, n = i, i = void 0), tt.ajax(tt.extend({
                    url: t,
                    type: e,
                    dataType: s,
                    data: i,
                    success: n
                }, tt.isPlainObject(t) && t))
            }
        }), tt._evalUrl = function (t) {
            return tt.ajax({
                url: t,
                type: "GET",
                dataType: "script",
                async: !1,
                global: !1,
                throws: !0
            })
        }, tt.fn.extend({
            wrapAll: function (t) {
                var e;
                return tt.isFunction(t) ? this.each(function (e) {
                    tt(this).wrapAll(t.call(this, e))
                }) : (this[0] && (e = tt(t, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && e.insertBefore(this[0]), e.map(function () {
                    for (var t = this; t.firstElementChild;) t = t.firstElementChild;
                    return t
                }).append(this)), this)
            },
            wrapInner: function (t) {
                return tt.isFunction(t) ? this.each(function (e) {
                    tt(this).wrapInner(t.call(this, e))
                }) : this.each(function () {
                    var e = tt(this),
                        i = e.contents();
                    i.length ? i.wrapAll(t) : e.append(t)
                })
            },
            wrap: function (t) {
                var e = tt.isFunction(t);
                return this.each(function (i) {
                    tt(this).wrapAll(e ? t.call(this, i) : t)
                })
            },
            unwrap: function () {
                return this.parent().each(function () {
                    tt.nodeName(this, "body") || tt(this).replaceWith(this.childNodes)
                }).end()
            }
        }), tt.expr.filters.hidden = function (t) {
            return !tt.expr.filters.visible(t)
        }, tt.expr.filters.visible = function (t) {
            return t.offsetWidth > 0 || t.offsetHeight > 0 || t.getClientRects().length > 0
        };
        var Ce = /%20/g,
            Te = /\[\]$/,
            Ee = /\r?\n/g,
            Se = /^(?:submit|button|image|reset|file)$/i,
            _e = /^(?:input|select|textarea|keygen)/i;
        tt.param = function (t, e) {
            var i, n = [],
                s = function (t, e) {
                    e = tt.isFunction(e) ? e() : null == e ? "" : e, n[n.length] = encodeURIComponent(t) + "=" + encodeURIComponent(e)
                };
            if (void 0 === e && (e = tt.ajaxSettings && tt.ajaxSettings.traditional), tt.isArray(t) || t.jquery && !tt.isPlainObject(t)) tt.each(t, function () {
                s(this.name, this.value)
            });
            else
                for (i in t) W(i, t[i], e, s);
            return n.join("&").replace(Ce, "+")
        }, tt.fn.extend({
            serialize: function () {
                return tt.param(this.serializeArray())
            },
            serializeArray: function () {
                return this.map(function () {
                    var t = tt.prop(this, "elements");
                    return t ? tt.makeArray(t) : this
                }).filter(function () {
                    var t = this.type;
                    return this.name && !tt(this).is(":disabled") && _e.test(this.nodeName) && !Se.test(t) && (this.checked || !kt.test(t))
                }).map(function (t, e) {
                    var i = tt(this).val();
                    return null == i ? null : tt.isArray(i) ? tt.map(i, function (t) {
                        return {
                            name: e.name,
                            value: t.replace(Ee, "\r\n")
                        }
                    }) : {
                        name: e.name,
                        value: i.replace(Ee, "\r\n")
                    }
                }).get()
            }
        }), tt.ajaxSettings.xhr = function () {
            try {
                return new t.XMLHttpRequest
            } catch (t) {}
        };
        var ke = {
                0: 200,
                1223: 204
            },
            $e = tt.ajaxSettings.xhr();
        Z.cors = !!$e && "withCredentials" in $e, Z.ajax = $e = !!$e, tt.ajaxTransport(function (e) {
            var i, n;
            return Z.cors || $e && !e.crossDomain ? {
                send: function (s, r) {
                    var o, a = e.xhr();
                    if (a.open(e.type, e.url, e.async, e.username, e.password), e.xhrFields)
                        for (o in e.xhrFields) a[o] = e.xhrFields[o];
                    e.mimeType && a.overrideMimeType && a.overrideMimeType(e.mimeType), e.crossDomain || s["X-Requested-With"] || (s["X-Requested-With"] = "XMLHttpRequest");
                    for (o in s) a.setRequestHeader(o, s[o]);
                    i = function (t) {
                        return function () {
                            i && (i = n = a.onload = a.onerror = a.onabort = a.onreadystatechange = null, "abort" === t ? a.abort() : "error" === t ? "number" != typeof a.status ? r(0, "error") : r(a.status, a.statusText) : r(ke[a.status] || a.status, a.statusText, "text" !== (a.responseType || "text") || "string" != typeof a.responseText ? {
                                binary: a.response
                            } : {
                                text: a.responseText
                            }, a.getAllResponseHeaders()))
                        }
                    }, a.onload = i(), n = a.onerror = i("error"), void 0 !== a.onabort ? a.onabort = n : a.onreadystatechange = function () {
                        4 === a.readyState && t.setTimeout(function () {
                            i && n()
                        })
                    }, i = i("abort");
                    try {
                        a.send(e.hasContent && e.data || null)
                    } catch (t) {
                        if (i) throw t
                    }
                },
                abort: function () {
                    i && i()
                }
            } : void 0
        }), tt.ajaxSetup({
            accepts: {
                script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
            },
            contents: {
                script: /\b(?:java|ecma)script\b/
            },
            converters: {
                "text script": function (t) {
                    return tt.globalEval(t), t
                }
            }
        }), tt.ajaxPrefilter("script", function (t) {
            void 0 === t.cache && (t.cache = !1), t.crossDomain && (t.type = "GET")
        }), tt.ajaxTransport("script", function (t) {
            if (t.crossDomain) {
                var e, i;
                return {
                    send: function (n, s) {
                        e = tt("<script>").prop({
                            charset: t.scriptCharset,
                            src: t.url
                        }).on("load error", i = function (t) {
                            e.remove(), i = null, t && s("error" === t.type ? 404 : 200, t.type)
                        }), F.head.appendChild(e[0])
                    },
                    abort: function () {
                        i && i()
                    }
                }
            }
        });
        var ze = [],
            Me = /(=)\?(?=&|$)|\?\?/;
        tt.ajaxSetup({
            jsonp: "callback",
            jsonpCallback: function () {
                var t = ze.pop() || tt.expando + "_" + ce++;
                return this[t] = !0, t
            }
        }), tt.ajaxPrefilter("json jsonp", function (e, i, n) {
            var s, r, o, a = !1 !== e.jsonp && (Me.test(e.url) ? "url" : "string" == typeof e.data && 0 === (e.contentType || "").indexOf("application/x-www-form-urlencoded") && Me.test(e.data) && "data");
            return a || "jsonp" === e.dataTypes[0] ? (s = e.jsonpCallback = tt.isFunction(e.jsonpCallback) ? e.jsonpCallback() : e.jsonpCallback, a ? e[a] = e[a].replace(Me, "$1" + s) : !1 !== e.jsonp && (e.url += (ue.test(e.url) ? "&" : "?") + e.jsonp + "=" + s), e.converters["script json"] = function () {
                return o || tt.error(s + " was not called"), o[0]
            }, e.dataTypes[0] = "json", r = t[s], t[s] = function () {
                o = arguments
            }, n.always(function () {
                void 0 === r ? tt(t).removeProp(s) : t[s] = r, e[s] && (e.jsonpCallback = i.jsonpCallback, ze.push(s)), o && tt.isFunction(r) && r(o[0]), o = r = void 0
            }), "script") : void 0
        }), tt.parseHTML = function (t, e, i) {
            if (!t || "string" != typeof t) return null;
            "boolean" == typeof e && (i = e, e = !1), e = e || F;
            var n = ht.exec(t),
                s = !i && [];
            return n ? [e.createElement(n[1])] : (n = c([t], e, s), s && s.length && tt(s).remove(), tt.merge([], n.childNodes))
        };
        var De = tt.fn.load;
        tt.fn.load = function (t, e, i) {
            if ("string" != typeof t && De) return De.apply(this, arguments);
            var n, s, r, o = this,
                a = t.indexOf(" ");
            return a > -1 && (n = tt.trim(t.slice(a)), t = t.slice(0, a)), tt.isFunction(e) ? (i = e, e = void 0) : e && "object" == typeof e && (s = "POST"), o.length > 0 && tt.ajax({
                url: t,
                type: s || "GET",
                dataType: "html",
                data: e
            }).done(function (t) {
                r = arguments, o.html(n ? tt("<div>").append(tt.parseHTML(t)).find(n) : t)
            }).always(i && function (t, e) {
                o.each(function () {
                    i.apply(o, r || [t.responseText, e, t])
                })
            }), this
        }, tt.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function (t, e) {
            tt.fn[e] = function (t) {
                return this.on(e, t)
            }
        }), tt.expr.filters.animated = function (t) {
            return tt.grep(tt.timers, function (e) {
                return t === e.elem
            }).length
        }, tt.offset = {
            setOffset: function (t, e, i) {
                var n, s, r, o, a, l, h = tt.css(t, "position"),
                    d = tt(t),
                    c = {};
                "static" === h && (t.style.position = "relative"), a = d.offset(), r = tt.css(t, "top"), l = tt.css(t, "left"), ("absolute" === h || "fixed" === h) && (r + l).indexOf("auto") > -1 ? (n = d.position(), o = n.top, s = n.left) : (o = parseFloat(r) || 0, s = parseFloat(l) || 0), tt.isFunction(e) && (e = e.call(t, i, tt.extend({}, a))), null != e.top && (c.top = e.top - a.top + o), null != e.left && (c.left = e.left - a.left + s), "using" in e ? e.using.call(t, c) : d.css(c)
            }
        }, tt.fn.extend({
            offset: function (t) {
                if (arguments.length) return void 0 === t ? this : this.each(function (e) {
                    tt.offset.setOffset(this, t, e)
                });
                var e, i, n = this[0],
                    s = {
                        top: 0,
                        left: 0
                    },
                    r = n && n.ownerDocument;
                return r ? (e = r.documentElement, tt.contains(e, n) ? (s = n.getBoundingClientRect(), i = B(r), {
                    top: s.top + i.pageYOffset - e.clientTop,
                    left: s.left + i.pageXOffset - e.clientLeft
                }) : s) : void 0
            },
            position: function () {
                if (this[0]) {
                    var t, e, i = this[0],
                        n = {
                            top: 0,
                            left: 0
                        };
                    return "fixed" === tt.css(i, "position") ? e = i.getBoundingClientRect() : (t = this.offsetParent(), e = this.offset(), tt.nodeName(t[0], "html") || (n = t.offset()), n.top += tt.css(t[0], "borderTopWidth", !0), n.left += tt.css(t[0], "borderLeftWidth", !0)), {
                        top: e.top - n.top - tt.css(i, "marginTop", !0),
                        left: e.left - n.left - tt.css(i, "marginLeft", !0)
                    }
                }
            },
            offsetParent: function () {
                return this.map(function () {
                    for (var t = this.offsetParent; t && "static" === tt.css(t, "position");) t = t.offsetParent;
                    return t || Xt
                })
            }
        }), tt.each({
            scrollLeft: "pageXOffset",
            scrollTop: "pageYOffset"
        }, function (t, e) {
            var i = "pageYOffset" === e;
            tt.fn[t] = function (n) {
                return vt(this, function (t, n, s) {
                    var r = B(t);
                    return void 0 === s ? r ? r[e] : t[n] : void(r ? r.scrollTo(i ? r.pageXOffset : s, i ? s : r.pageYOffset) : t[n] = s)
                }, t, n, arguments.length)
            }
        }), tt.each(["top", "left"], function (t, e) {
            tt.cssHooks[e] = _(Z.pixelPosition, function (t, i) {
                return i ? (i = S(t, e), qt.test(i) ? tt(t).position()[e] + "px" : i) : void 0
            })
        }), tt.each({
            Height: "height",
            Width: "width"
        }, function (t, e) {
            tt.each({
                padding: "inner" + t,
                content: e,
                "": "outer" + t
            }, function (i, n) {
                tt.fn[n] = function (n, s) {
                    var r = arguments.length && (i || "boolean" != typeof n),
                        o = i || (!0 === n || !0 === s ? "margin" : "border");
                    return vt(this, function (e, i, n) {
                        var s;
                        return tt.isWindow(e) ? e.document.documentElement["client" + t] : 9 === e.nodeType ? (s = e.documentElement, Math.max(e.body["scroll" + t], s["scroll" + t], e.body["offset" + t], s["offset" + t], s["client" + t])) : void 0 === n ? tt.css(e, i, o) : tt.style(e, i, n, o)
                    }, e, r ? n : void 0, r, null)
                }
            })
        }), tt.fn.extend({
            bind: function (t, e, i) {
                return this.on(t, null, e, i)
            },
            unbind: function (t, e) {
                return this.off(t, null, e)
            },
            delegate: function (t, e, i, n) {
                return this.on(e, t, i, n)
            },
            undelegate: function (t, e, i) {
                return 1 === arguments.length ? this.off(t, "**") : this.off(e, t || "**", i)
            },
            size: function () {
                return this.length
            }
        }), tt.fn.andSelf = tt.fn.addBack, "function" == typeof define && define.amd && define("jquery", [], function () {
            return tt
        });
        var Ie = t.jQuery,
            Pe = t.$;
        return tt.noConflict = function (e) {
            return t.$ === tt && (t.$ = Pe), e && t.jQuery === tt && (t.jQuery = Ie), tt
        }, e || (t.jQuery = t.$ = tt), tt
    }), "undefined" == typeof jQuery) throw new Error("Bootstrap's JavaScript requires jQuery"); + function (t) {
    "use strict";
    var e = jQuery.fn.jquery.split(" ")[0].split(".");
    if (e[0] < 2 && e[1] < 9 || 1 == e[0] && 9 == e[1] && e[2] < 1 || e[0] > 3) throw new Error("Bootstrap's JavaScript requires jQuery version 1.9.1 or higher, but lower than version 4")
}(),
function (t) {
    "use strict";
    t.fn.emulateTransitionEnd = function (e) {
        var i = !1,
            n = this;
        t(this).one("bsTransitionEnd", function () {
            i = !0
        });
        return setTimeout(function () {
            i || t(n).trigger(t.support.transition.end)
        }, e), this
    }, t(function () {
        t.support.transition = function () {
            var t = document.createElement("bootstrap"),
                e = {
                    WebkitTransition: "webkitTransitionEnd",
                    MozTransition: "transitionend",
                    OTransition: "oTransitionEnd otransitionend",
                    transition: "transitionend"
                };
            for (var i in e)
                if (void 0 !== t.style[i]) return {
                    end: e[i]
                };
            return !1
        }(), t.support.transition && (t.event.special.bsTransitionEnd = {
            bindType: t.support.transition.end,
            delegateType: t.support.transition.end,
            handle: function (e) {
                if (t(e.target).is(this)) return e.handleObj.handler.apply(this, arguments)
            }
        })
    })
}(jQuery),
function (t) {
    "use strict";
    var e = '[data-dismiss="alert"]',
        i = function (i) {
            t(i).on("click", e, this.close)
        };
    i.VERSION = "3.3.7", i.TRANSITION_DURATION = 150, i.prototype.close = function (e) {
        function n() {
            o.detach().trigger("closed.bs.alert").remove()
        }
        var s = t(this),
            r = s.attr("data-target");
        r || (r = s.attr("href"), r = r && r.replace(/.*(?=#[^\s]*$)/, ""));
        var o = t("#" === r ? [] : r);
        e && e.preventDefault(), o.length || (o = s.closest(".alert")), o.trigger(e = t.Event("close.bs.alert")), e.isDefaultPrevented() || (o.removeClass("in"), t.support.transition && o.hasClass("fade") ? o.one("bsTransitionEnd", n).emulateTransitionEnd(i.TRANSITION_DURATION) : n())
    };
    var n = t.fn.alert;
    t.fn.alert = function (e) {
        return this.each(function () {
            var n = t(this),
                s = n.data("bs.alert");
            s || n.data("bs.alert", s = new i(this)), "string" == typeof e && s[e].call(n)
        })
    }, t.fn.alert.Constructor = i, t.fn.alert.noConflict = function () {
        return t.fn.alert = n, this
    }, t(document).on("click.bs.alert.data-api", e, i.prototype.close)
}(jQuery),
function (t) {
    "use strict";

    function e(e) {
        return this.each(function () {
            var n = t(this),
                s = n.data("bs.button"),
                r = "object" == typeof e && e;
            s || n.data("bs.button", s = new i(this, r)), "toggle" == e ? s.toggle() : e && s.setState(e)
        })
    }
    var i = function (e, n) {
        this.$element = t(e), this.options = t.extend({}, i.DEFAULTS, n), this.isLoading = !1
    };
    i.VERSION = "3.3.7", i.DEFAULTS = {
        loadingText: "loading..."
    }, i.prototype.setState = function (e) {
        var i = "disabled",
            n = this.$element,
            s = n.is("input") ? "val" : "html",
            r = n.data();
        e += "Text", null == r.resetText && n.data("resetText", n[s]()), setTimeout(t.proxy(function () {
            n[s](null == r[e] ? this.options[e] : r[e]), "loadingText" == e ? (this.isLoading = !0, n.addClass(i).attr(i, i).prop(i, !0)) : this.isLoading && (this.isLoading = !1, n.removeClass(i).removeAttr(i).prop(i, !1))
        }, this), 0)
    }, i.prototype.toggle = function () {
        var t = !0,
            e = this.$element.closest('[data-toggle="buttons"]');
        if (e.length) {
            var i = this.$element.find("input");
            "radio" == i.prop("type") ? (i.prop("checked") && (t = !1), e.find(".active").removeClass("active"), this.$element.addClass("active")) : "checkbox" == i.prop("type") && (i.prop("checked") !== this.$element.hasClass("active") && (t = !1), this.$element.toggleClass("active")), i.prop("checked", this.$element.hasClass("active")), t && i.trigger("change")
        } else this.$element.attr("aria-pressed", !this.$element.hasClass("active")), this.$element.toggleClass("active")
    };
    var n = t.fn.button;
    t.fn.button = e, t.fn.button.Constructor = i, t.fn.button.noConflict = function () {
        return t.fn.button = n, this
    }, t(document).on("click.bs.button.data-api", '[data-toggle^="button"]', function (i) {
        var n = t(i.target).closest(".btn");
        e.call(n, "toggle"), t(i.target).is('input[type="radio"], input[type="checkbox"]') || (i.preventDefault(), n.is("input,button") ? n.trigger("focus") : n.find("input:visible,button:visible").first().trigger("focus"))
    }).on("focus.bs.button.data-api blur.bs.button.data-api", '[data-toggle^="button"]', function (e) {
        t(e.target).closest(".btn").toggleClass("focus", /^focus(in)?$/.test(e.type))
    })
}(jQuery),
function (t) {
    "use strict";

    function e(e) {
        return this.each(function () {
            var n = t(this),
                s = n.data("bs.carousel"),
                r = t.extend({}, i.DEFAULTS, n.data(), "object" == typeof e && e),
                o = "string" == typeof e ? e : r.slide;
            s || n.data("bs.carousel", s = new i(this, r)), "number" == typeof e ? s.to(e) : o ? s[o]() : r.interval && s.pause().cycle()
        })
    }
    var i = function (e, i) {
        this.$element = t(e), this.$indicators = this.$element.find(".carousel-indicators"), this.options = i, this.paused = null, this.sliding = null, this.interval = null, this.$active = null, this.$items = null, this.options.keyboard && this.$element.on("keydown.bs.carousel", t.proxy(this.keydown, this)), "hover" == this.options.pause && !("ontouchstart" in document.documentElement) && this.$element.on("mouseenter.bs.carousel", t.proxy(this.pause, this)).on("mouseleave.bs.carousel", t.proxy(this.cycle, this))
    };
    i.VERSION = "3.3.7", i.TRANSITION_DURATION = 600, i.DEFAULTS = {
        interval: 5e3,
        pause: "hover",
        wrap: !0,
        keyboard: !0
    }, i.prototype.keydown = function (t) {
        if (!/input|textarea/i.test(t.target.tagName)) {
            switch (t.which) {
                case 37:
                    this.prev();
                    break;
                case 39:
                    this.next();
                    break;
                default:
                    return
            }
            t.preventDefault()
        }
    }, i.prototype.cycle = function (e) {
        return e || (this.paused = !1), this.interval && clearInterval(this.interval), this.options.interval && !this.paused && (this.interval = setInterval(t.proxy(this.next, this), this.options.interval)), this
    }, i.prototype.getItemIndex = function (t) {
        return this.$items = t.parent().children(".item"), this.$items.index(t || this.$active)
    }, i.prototype.getItemForDirection = function (t, e) {
        var i = this.getItemIndex(e);
        if (("prev" == t && 0 === i || "next" == t && i == this.$items.length - 1) && !this.options.wrap) return e;
        var n = (i + ("prev" == t ? -1 : 1)) % this.$items.length;
        return this.$items.eq(n)
    }, i.prototype.to = function (t) {
        var e = this,
            i = this.getItemIndex(this.$active = this.$element.find(".item.active"));
        if (!(t > this.$items.length - 1 || t < 0)) return this.sliding ? this.$element.one("slid.bs.carousel", function () {
            e.to(t)
        }) : i == t ? this.pause().cycle() : this.slide(t > i ? "next" : "prev", this.$items.eq(t))
    }, i.prototype.pause = function (e) {
        return e || (this.paused = !0), this.$element.find(".next, .prev").length && t.support.transition && (this.$element.trigger(t.support.transition.end), this.cycle(!0)), this.interval = clearInterval(this.interval), this
    }, i.prototype.next = function () {
        if (!this.sliding) return this.slide("next")
    }, i.prototype.prev = function () {
        if (!this.sliding) return this.slide("prev")
    }, i.prototype.slide = function (e, n) {
        var s = this.$element.find(".item.active"),
            r = n || this.getItemForDirection(e, s),
            o = this.interval,
            a = "next" == e ? "left" : "right",
            l = this;
        if (r.hasClass("active")) return this.sliding = !1;
        var h = r[0],
            d = t.Event("slide.bs.carousel", {
                relatedTarget: h,
                direction: a
            });
        if (this.$element.trigger(d), !d.isDefaultPrevented()) {
            if (this.sliding = !0, o && this.pause(), this.$indicators.length) {
                this.$indicators.find(".active").removeClass("active");
                var c = t(this.$indicators.children()[this.getItemIndex(r)]);
                c && c.addClass("active")
            }
            var u = t.Event("slid.bs.carousel", {
                relatedTarget: h,
                direction: a
            });
            return t.support.transition && this.$element.hasClass("slide") ? (r.addClass(e), r[0].offsetWidth, s.addClass(a), r.addClass(a), s.one("bsTransitionEnd", function () {
                r.removeClass([e, a].join(" ")).addClass("active"), s.removeClass(["active", a].join(" ")), l.sliding = !1, setTimeout(function () {
                    l.$element.trigger(u)
                }, 0)
            }).emulateTransitionEnd(i.TRANSITION_DURATION)) : (s.removeClass("active"), r.addClass("active"), this.sliding = !1, this.$element.trigger(u)), o && this.cycle(), this
        }
    };
    var n = t.fn.carousel;
    t.fn.carousel = e, t.fn.carousel.Constructor = i, t.fn.carousel.noConflict = function () {
        return t.fn.carousel = n, this
    };
    var s = function (i) {
        var n, s = t(this),
            r = t(s.attr("data-target") || (n = s.attr("href")) && n.replace(/.*(?=#[^\s]+$)/, ""));
        if (r.hasClass("carousel")) {
            var o = t.extend({}, r.data(), s.data()),
                a = s.attr("data-slide-to");
            a && (o.interval = !1), e.call(r, o), a && r.data("bs.carousel").to(a), i.preventDefault()
        }
    };
    t(document).on("click.bs.carousel.data-api", "[data-slide]", s).on("click.bs.carousel.data-api", "[data-slide-to]", s), t(window).on("load", function () {
        t('[data-ride="carousel"]').each(function () {
            var i = t(this);
            e.call(i, i.data())
        })
    })
}(jQuery),
function (t) {
    "use strict";

    function e(e) {
        var i, n = e.attr("data-target") || (i = e.attr("href")) && i.replace(/.*(?=#[^\s]+$)/, "");
        return t(n)
    }

    function i(e) {
        return this.each(function () {
            var i = t(this),
                s = i.data("bs.collapse"),
                r = t.extend({}, n.DEFAULTS, i.data(), "object" == typeof e && e);
            !s && r.toggle && /show|hide/.test(e) && (r.toggle = !1), s || i.data("bs.collapse", s = new n(this, r)), "string" == typeof e && s[e]()
        })
    }
    var n = function (e, i) {
        this.$element = t(e), this.options = t.extend({}, n.DEFAULTS, i), this.$trigger = t('[data-toggle="collapse"][href="#' + e.id + '"],[data-toggle="collapse"][data-target="#' + e.id + '"]'), this.transitioning = null, this.options.parent ? this.$parent = this.getParent() : this.addAriaAndCollapsedClass(this.$element, this.$trigger), this.options.toggle && this.toggle()
    };
    n.VERSION = "3.3.7", n.TRANSITION_DURATION = 350, n.DEFAULTS = {
        toggle: !0
    }, n.prototype.dimension = function () {
        return this.$element.hasClass("width") ? "width" : "height"
    }, n.prototype.show = function () {
        if (!this.transitioning && !this.$element.hasClass("in")) {
            var e, s = this.$parent && this.$parent.children(".panel").children(".in, .collapsing");
            if (!(s && s.length && (e = s.data("bs.collapse")) && e.transitioning)) {
                var r = t.Event("show.bs.collapse");
                if (this.$element.trigger(r), !r.isDefaultPrevented()) {
                    s && s.length && (i.call(s, "hide"), e || s.data("bs.collapse", null));
                    var o = this.dimension();
                    this.$element.removeClass("collapse").addClass("collapsing")[o](0).attr("aria-expanded", !0), this.$trigger.removeClass("collapsed").attr("aria-expanded", !0), this.transitioning = 1;
                    var a = function () {
                        this.$element.removeClass("collapsing").addClass("collapse in")[o](""), this.transitioning = 0, this.$element.trigger("shown.bs.collapse")
                    };
                    if (!t.support.transition) return a.call(this);
                    var l = t.camelCase(["scroll", o].join("-"));
                    this.$element.one("bsTransitionEnd", t.proxy(a, this)).emulateTransitionEnd(n.TRANSITION_DURATION)[o](this.$element[0][l])
                }
            }
        }
    }, n.prototype.hide = function () {
        if (!this.transitioning && this.$element.hasClass("in")) {
            var e = t.Event("hide.bs.collapse");
            if (this.$element.trigger(e), !e.isDefaultPrevented()) {
                var i = this.dimension();
                this.$element[i](this.$element[i]())[0].offsetHeight, this.$element.addClass("collapsing").removeClass("collapse in").attr("aria-expanded", !1), this.$trigger.addClass("collapsed").attr("aria-expanded", !1), this.transitioning = 1;
                var s = function () {
                    this.transitioning = 0, this.$element.removeClass("collapsing").addClass("collapse").trigger("hidden.bs.collapse")
                };
                return t.support.transition ? void this.$element[i](0).one("bsTransitionEnd", t.proxy(s, this)).emulateTransitionEnd(n.TRANSITION_DURATION) : s.call(this)
            }
        }
    }, n.prototype.toggle = function () {
        this[this.$element.hasClass("in") ? "hide" : "show"]()
    }, n.prototype.getParent = function () {
        return t(this.options.parent).find('[data-toggle="collapse"][data-parent="' + this.options.parent + '"]').each(t.proxy(function (i, n) {
            var s = t(n);
            this.addAriaAndCollapsedClass(e(s), s)
        }, this)).end()
    }, n.prototype.addAriaAndCollapsedClass = function (t, e) {
        var i = t.hasClass("in");
        t.attr("aria-expanded", i), e.toggleClass("collapsed", !i).attr("aria-expanded", i)
    };
    var s = t.fn.collapse;
    t.fn.collapse = i, t.fn.collapse.Constructor = n, t.fn.collapse.noConflict = function () {
        return t.fn.collapse = s, this
    }, t(document).on("click.bs.collapse.data-api", '[data-toggle="collapse"]', function (n) {
        var s = t(this);
        s.attr("data-target") || n.preventDefault();
        var r = e(s),
            o = r.data("bs.collapse") ? "toggle" : s.data();
        i.call(r, o)
    })
}(jQuery),
function (t) {
    "use strict";

    function e(e) {
        var i = e.attr("data-target");
        i || (i = e.attr("href"), i = i && /#[A-Za-z]/.test(i) && i.replace(/.*(?=#[^\s]*$)/, ""));
        var n = i && t(i);
        return n && n.length ? n : e.parent()
    }

    function i(i) {
        i && 3 === i.which || (t(n).remove(), t(s).each(function () {
            var n = t(this),
                s = e(n),
                r = {
                    relatedTarget: this
                };
            s.hasClass("open") && (i && "click" == i.type && /input|textarea/i.test(i.target.tagName) && t.contains(s[0], i.target) || (s.trigger(i = t.Event("hide.bs.dropdown", r)), i.isDefaultPrevented() || (n.attr("aria-expanded", "false"), s.removeClass("open").trigger(t.Event("hidden.bs.dropdown", r)))))
        }))
    }
    var n = ".dropdown-backdrop",
        s = '[data-toggle="dropdown"]',
        r = function (e) {
            t(e).on("click.bs.dropdown", this.toggle)
        };
    r.VERSION = "3.3.7", r.prototype.toggle = function (n) {
        var s = t(this);
        if (!s.is(".disabled, :disabled")) {
            var r = e(s),
                o = r.hasClass("open");
            if (i(), !o) {
                "ontouchstart" in document.documentElement && !r.closest(".navbar-nav").length && t(document.createElement("div")).addClass("dropdown-backdrop").insertAfter(t(this)).on("click", i);
                var a = {
                    relatedTarget: this
                };
                if (r.trigger(n = t.Event("show.bs.dropdown", a)), n.isDefaultPrevented()) return;
                s.trigger("focus").attr("aria-expanded", "true"), r.toggleClass("open").trigger(t.Event("shown.bs.dropdown", a))
            }
            return !1
        }
    }, r.prototype.keydown = function (i) {
        if (/(38|40|27|32)/.test(i.which) && !/input|textarea/i.test(i.target.tagName)) {
            var n = t(this);
            if (i.preventDefault(), i.stopPropagation(), !n.is(".disabled, :disabled")) {
                var r = e(n),
                    o = r.hasClass("open");
                if (!o && 27 != i.which || o && 27 == i.which) return 27 == i.which && r.find(s).trigger("focus"), n.trigger("click");
                var a = r.find(".dropdown-menu li:not(.disabled):visible a");
                if (a.length) {
                    var l = a.index(i.target);
                    38 == i.which && l > 0 && l--, 40 == i.which && l < a.length - 1 && l++, ~l || (l = 0), a.eq(l).trigger("focus")
                }
            }
        }
    };
    var o = t.fn.dropdown;
    t.fn.dropdown = function (e) {
        return this.each(function () {
            var i = t(this),
                n = i.data("bs.dropdown");
            n || i.data("bs.dropdown", n = new r(this)), "string" == typeof e && n[e].call(i)
        })
    }, t.fn.dropdown.Constructor = r, t.fn.dropdown.noConflict = function () {
        return t.fn.dropdown = o, this
    }, t(document).on("click.bs.dropdown.data-api", i).on("click.bs.dropdown.data-api", ".dropdown form", function (t) {
        t.stopPropagation()
    }).on("click.bs.dropdown.data-api", s, r.prototype.toggle).on("keydown.bs.dropdown.data-api", s, r.prototype.keydown).on("keydown.bs.dropdown.data-api", ".dropdown-menu", r.prototype.keydown)
}(jQuery),
function (t) {
    "use strict";

    function e(e, n) {
        return this.each(function () {
            var s = t(this),
                r = s.data("bs.modal"),
                o = t.extend({}, i.DEFAULTS, s.data(), "object" == typeof e && e);
            r || s.data("bs.modal", r = new i(this, o)), "string" == typeof e ? r[e](n) : o.show && r.show(n)
        })
    }
    var i = function (e, i) {
        this.options = i, this.$body = t(document.body), this.$element = t(e), this.$dialog = this.$element.find(".modal-dialog"), this.$backdrop = null, this.isShown = null, this.originalBodyPad = null, this.scrollbarWidth = 0, this.ignoreBackdropClick = !1, this.options.remote && this.$element.find(".modal-content").load(this.options.remote, t.proxy(function () {
            this.$element.trigger("loaded.bs.modal")
        }, this))
    };
    i.VERSION = "3.3.7", i.TRANSITION_DURATION = 300, i.BACKDROP_TRANSITION_DURATION = 150, i.DEFAULTS = {
        backdrop: !0,
        keyboard: !0,
        show: !0
    }, i.prototype.toggle = function (t) {
        return this.isShown ? this.hide() : this.show(t)
    }, i.prototype.show = function (e) {
        var n = this,
            s = t.Event("show.bs.modal", {
                relatedTarget: e
            });
        this.$element.trigger(s), this.isShown || s.isDefaultPrevented() || (this.isShown = !0, this.checkScrollbar(), this.setScrollbar(), this.$body.addClass("modal-open"), this.escape(), this.resize(), this.$element.on("click.dismiss.bs.modal", '[data-dismiss="modal"]', t.proxy(this.hide, this)), this.$dialog.on("mousedown.dismiss.bs.modal", function () {
            n.$element.one("mouseup.dismiss.bs.modal", function (e) {
                t(e.target).is(n.$element) && (n.ignoreBackdropClick = !0)
            })
        }), this.backdrop(function () {
            var s = t.support.transition && n.$element.hasClass("fade");
            n.$element.parent().length || n.$element.appendTo(n.$body), n.$element.show().scrollTop(0), n.adjustDialog(), s && n.$element[0].offsetWidth, n.$element.addClass("in"), n.enforceFocus();
            var r = t.Event("shown.bs.modal", {
                relatedTarget: e
            });
            s ? n.$dialog.one("bsTransitionEnd", function () {
                n.$element.trigger("focus").trigger(r)
            }).emulateTransitionEnd(i.TRANSITION_DURATION) : n.$element.trigger("focus").trigger(r)
        }))
    }, i.prototype.hide = function (e) {
        e && e.preventDefault(), e = t.Event("hide.bs.modal"), this.$element.trigger(e), this.isShown && !e.isDefaultPrevented() && (this.isShown = !1, this.escape(), this.resize(), t(document).off("focusin.bs.modal"), this.$element.removeClass("in").off("click.dismiss.bs.modal").off("mouseup.dismiss.bs.modal"), this.$dialog.off("mousedown.dismiss.bs.modal"), t.support.transition && this.$element.hasClass("fade") ? this.$element.one("bsTransitionEnd", t.proxy(this.hideModal, this)).emulateTransitionEnd(i.TRANSITION_DURATION) : this.hideModal())
    }, i.prototype.enforceFocus = function () {
        t(document).off("focusin.bs.modal").on("focusin.bs.modal", t.proxy(function (t) {
            document === t.target || this.$element[0] === t.target || this.$element.has(t.target).length || this.$element.trigger("focus")
        }, this))
    }, i.prototype.escape = function () {
        this.isShown && this.options.keyboard ? this.$element.on("keydown.dismiss.bs.modal", t.proxy(function (t) {
            27 == t.which && this.hide()
        }, this)) : this.isShown || this.$element.off("keydown.dismiss.bs.modal")
    }, i.prototype.resize = function () {
        this.isShown ? t(window).on("resize.bs.modal", t.proxy(this.handleUpdate, this)) : t(window).off("resize.bs.modal")
    }, i.prototype.hideModal = function () {
        var t = this;
        this.$element.hide(), this.backdrop(function () {
            t.$body.removeClass("modal-open"), t.resetAdjustments(), t.resetScrollbar(), t.$element.trigger("hidden.bs.modal")
        })
    }, i.prototype.removeBackdrop = function () {
        this.$backdrop && this.$backdrop.remove(), this.$backdrop = null
    }, i.prototype.backdrop = function (e) {
        var n = this,
            s = this.$element.hasClass("fade") ? "fade" : "";
        if (this.isShown && this.options.backdrop) {
            var r = t.support.transition && s;
            if (this.$backdrop = t(document.createElement("div")).addClass("modal-backdrop " + s).appendTo(this.$body), this.$element.on("click.dismiss.bs.modal", t.proxy(function (t) {
                    return this.ignoreBackdropClick ? void(this.ignoreBackdropClick = !1) : void(t.target === t.currentTarget && ("static" == this.options.backdrop ? this.$element[0].focus() : this.hide()))
                }, this)), r && this.$backdrop[0].offsetWidth, this.$backdrop.addClass("in"), !e) return;
            r ? this.$backdrop.one("bsTransitionEnd", e).emulateTransitionEnd(i.BACKDROP_TRANSITION_DURATION) : e()
        } else if (!this.isShown && this.$backdrop) {
            this.$backdrop.removeClass("in");
            var o = function () {
                n.removeBackdrop(), e && e()
            };
            t.support.transition && this.$element.hasClass("fade") ? this.$backdrop.one("bsTransitionEnd", o).emulateTransitionEnd(i.BACKDROP_TRANSITION_DURATION) : o()
        } else e && e()
    }, i.prototype.handleUpdate = function () {
        this.adjustDialog()
    }, i.prototype.adjustDialog = function () {
        var t = this.$element[0].scrollHeight > document.documentElement.clientHeight;
        this.$element.css({
            paddingLeft: !this.bodyIsOverflowing && t ? this.scrollbarWidth : "",
            paddingRight: this.bodyIsOverflowing && !t ? this.scrollbarWidth : ""
        })
    }, i.prototype.resetAdjustments = function () {
        this.$element.css({
            paddingLeft: "",
            paddingRight: ""
        })
    }, i.prototype.checkScrollbar = function () {
        var t = window.innerWidth;
        if (!t) {
            var e = document.documentElement.getBoundingClientRect();
            t = e.right - Math.abs(e.left)
        }
        this.bodyIsOverflowing = document.body.clientWidth < t, this.scrollbarWidth = this.measureScrollbar()
    }, i.prototype.setScrollbar = function () {
        var t = parseInt(this.$body.css("padding-right") || 0, 10);
        this.originalBodyPad = document.body.style.paddingRight || "", this.bodyIsOverflowing && this.$body.css("padding-right", t + this.scrollbarWidth)
    }, i.prototype.resetScrollbar = function () {
        this.$body.css("padding-right", this.originalBodyPad)
    }, i.prototype.measureScrollbar = function () {
        var t = document.createElement("div");
        t.className = "modal-scrollbar-measure", this.$body.append(t);
        var e = t.offsetWidth - t.clientWidth;
        return this.$body[0].removeChild(t), e
    };
    var n = t.fn.modal;
    t.fn.modal = e, t.fn.modal.Constructor = i, t.fn.modal.noConflict = function () {
        return t.fn.modal = n, this
    }, t(document).on("click.bs.modal.data-api", '[data-toggle="modal"]', function (i) {
        var n = t(this),
            s = n.attr("href"),
            r = t(n.attr("data-target") || s && s.replace(/.*(?=#[^\s]+$)/, "")),
            o = r.data("bs.modal") ? "toggle" : t.extend({
                remote: !/#/.test(s) && s
            }, r.data(), n.data());
        n.is("a") && i.preventDefault(), r.one("show.bs.modal", function (t) {
            t.isDefaultPrevented() || r.one("hidden.bs.modal", function () {
                n.is(":visible") && n.trigger("focus")
            })
        }), e.call(r, o, this)
    })
}(jQuery),
function (t) {
    "use strict";
    var e = function (t, e) {
        this.type = null, this.options = null, this.enabled = null, this.timeout = null, this.hoverState = null, this.$element = null, this.inState = null, this.init("tooltip", t, e)
    };
    e.VERSION = "3.3.7", e.TRANSITION_DURATION = 150, e.DEFAULTS = {
        animation: !0,
        placement: "top",
        selector: !1,
        template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
        trigger: "hover focus",
        title: "",
        delay: 0,
        html: !1,
        container: !1,
        viewport: {
            selector: "body",
            padding: 0
        }
    }, e.prototype.init = function (e, i, n) {
        if (this.enabled = !0, this.type = e, this.$element = t(i), this.options = this.getOptions(n), this.$viewport = this.options.viewport && t(t.isFunction(this.options.viewport) ? this.options.viewport.call(this, this.$element) : this.options.viewport.selector || this.options.viewport), this.inState = {
                click: !1,
                hover: !1,
                focus: !1
            }, this.$element[0] instanceof document.constructor && !this.options.selector) throw new Error("`selector` option must be specified when initializing " + this.type + " on the window.document object!");
        for (var s = this.options.trigger.split(" "), r = s.length; r--;) {
            var o = s[r];
            if ("click" == o) this.$element.on("click." + this.type, this.options.selector, t.proxy(this.toggle, this));
            else if ("manual" != o) {
                var a = "hover" == o ? "mouseenter" : "focusin",
                    l = "hover" == o ? "mouseleave" : "focusout";
                this.$element.on(a + "." + this.type, this.options.selector, t.proxy(this.enter, this)), this.$element.on(l + "." + this.type, this.options.selector, t.proxy(this.leave, this))
            }
        }
        this.options.selector ? this._options = t.extend({}, this.options, {
            trigger: "manual",
            selector: ""
        }) : this.fixTitle()
    }, e.prototype.getDefaults = function () {
        return e.DEFAULTS
    }, e.prototype.getOptions = function (e) {
        return (e = t.extend({}, this.getDefaults(), this.$element.data(), e)).delay && "number" == typeof e.delay && (e.delay = {
            show: e.delay,
            hide: e.delay
        }), e
    }, e.prototype.getDelegateOptions = function () {
        var e = {},
            i = this.getDefaults();
        return this._options && t.each(this._options, function (t, n) {
            i[t] != n && (e[t] = n)
        }), e
    }, e.prototype.enter = function (e) {
        var i = e instanceof this.constructor ? e : t(e.currentTarget).data("bs." + this.type);
        return i || (i = new this.constructor(e.currentTarget, this.getDelegateOptions()), t(e.currentTarget).data("bs." + this.type, i)), e instanceof t.Event && (i.inState["focusin" == e.type ? "focus" : "hover"] = !0), i.tip().hasClass("in") || "in" == i.hoverState ? void(i.hoverState = "in") : (clearTimeout(i.timeout), i.hoverState = "in", i.options.delay && i.options.delay.show ? void(i.timeout = setTimeout(function () {
            "in" == i.hoverState && i.show()
        }, i.options.delay.show)) : i.show())
    }, e.prototype.isInStateTrue = function () {
        for (var t in this.inState)
            if (this.inState[t]) return !0;
        return !1
    }, e.prototype.leave = function (e) {
        var i = e instanceof this.constructor ? e : t(e.currentTarget).data("bs." + this.type);
        if (i || (i = new this.constructor(e.currentTarget, this.getDelegateOptions()), t(e.currentTarget).data("bs." + this.type, i)), e instanceof t.Event && (i.inState["focusout" == e.type ? "focus" : "hover"] = !1), !i.isInStateTrue()) return clearTimeout(i.timeout), i.hoverState = "out", i.options.delay && i.options.delay.hide ? void(i.timeout = setTimeout(function () {
            "out" == i.hoverState && i.hide()
        }, i.options.delay.hide)) : i.hide()
    }, e.prototype.show = function () {
        var i = t.Event("show.bs." + this.type);
        if (this.hasContent() && this.enabled) {
            this.$element.trigger(i);
            var n = t.contains(this.$element[0].ownerDocument.documentElement, this.$element[0]);
            if (i.isDefaultPrevented() || !n) return;
            var s = this,
                r = this.tip(),
                o = this.getUID(this.type);
            this.setContent(), r.attr("id", o), this.$element.attr("aria-describedby", o), this.options.animation && r.addClass("fade");
            var a = "function" == typeof this.options.placement ? this.options.placement.call(this, r[0], this.$element[0]) : this.options.placement,
                l = /\s?auto?\s?/i,
                h = l.test(a);
            h && (a = a.replace(l, "") || "top"), r.detach().css({
                top: 0,
                left: 0,
                display: "block"
            }).addClass(a).data("bs." + this.type, this), this.options.container ? r.appendTo(this.options.container) : r.insertAfter(this.$element), this.$element.trigger("inserted.bs." + this.type);
            var d = this.getPosition(),
                c = r[0].offsetWidth,
                u = r[0].offsetHeight;
            if (h) {
                var p = a,
                    f = this.getPosition(this.$viewport);
                a = "bottom" == a && d.bottom + u > f.bottom ? "top" : "top" == a && d.top - u < f.top ? "bottom" : "right" == a && d.right + c > f.width ? "left" : "left" == a && d.left - c < f.left ? "right" : a, r.removeClass(p).addClass(a)
            }
            var m = this.getCalculatedOffset(a, d, c, u);
            this.applyPlacement(m, a);
            var g = function () {
                var t = s.hoverState;
                s.$element.trigger("shown.bs." + s.type), s.hoverState = null, "out" == t && s.leave(s)
            };
            t.support.transition && this.$tip.hasClass("fade") ? r.one("bsTransitionEnd", g).emulateTransitionEnd(e.TRANSITION_DURATION) : g()
        }
    }, e.prototype.applyPlacement = function (e, i) {
        var n = this.tip(),
            s = n[0].offsetWidth,
            r = n[0].offsetHeight,
            o = parseInt(n.css("margin-top"), 10),
            a = parseInt(n.css("margin-left"), 10);
        isNaN(o) && (o = 0), isNaN(a) && (a = 0), e.top += o, e.left += a, t.offset.setOffset(n[0], t.extend({
            using: function (t) {
                n.css({
                    top: Math.round(t.top),
                    left: Math.round(t.left)
                })
            }
        }, e), 0), n.addClass("in");
        var l = n[0].offsetWidth,
            h = n[0].offsetHeight;
        "top" == i && h != r && (e.top = e.top + r - h);
        var d = this.getViewportAdjustedDelta(i, e, l, h);
        d.left ? e.left += d.left : e.top += d.top;
        var c = /top|bottom/.test(i),
            u = c ? 2 * d.left - s + l : 2 * d.top - r + h,
            p = c ? "offsetWidth" : "offsetHeight";
        n.offset(e), this.replaceArrow(u, n[0][p], c)
    }, e.prototype.replaceArrow = function (t, e, i) {
        this.arrow().css(i ? "left" : "top", 50 * (1 - t / e) + "%").css(i ? "top" : "left", "")
    }, e.prototype.setContent = function () {
        var t = this.tip(),
            e = this.getTitle();
        t.find(".tooltip-inner")[this.options.html ? "html" : "text"](e), t.removeClass("fade in top bottom left right")
    }, e.prototype.hide = function (i) {
        function n() {
            "in" != s.hoverState && r.detach(), s.$element && s.$element.removeAttr("aria-describedby").trigger("hidden.bs." + s.type), i && i()
        }
        var s = this,
            r = t(this.$tip),
            o = t.Event("hide.bs." + this.type);
        if (this.$element.trigger(o), !o.isDefaultPrevented()) return r.removeClass("in"), t.support.transition && r.hasClass("fade") ? r.one("bsTransitionEnd", n).emulateTransitionEnd(e.TRANSITION_DURATION) : n(), this.hoverState = null, this
    }, e.prototype.fixTitle = function () {
        var t = this.$element;
        (t.attr("title") || "string" != typeof t.attr("data-original-title")) && t.attr("data-original-title", t.attr("title") || "").attr("title", "")
    }, e.prototype.hasContent = function () {
        return this.getTitle()
    }, e.prototype.getPosition = function (e) {
        var i = (e = e || this.$element)[0],
            n = "BODY" == i.tagName,
            s = i.getBoundingClientRect();
        null == s.width && (s = t.extend({}, s, {
            width: s.right - s.left,
            height: s.bottom - s.top
        }));
        var r = window.SVGElement && i instanceof window.SVGElement,
            o = n ? {
                top: 0,
                left: 0
            } : r ? null : e.offset(),
            a = {
                scroll: n ? document.documentElement.scrollTop || document.body.scrollTop : e.scrollTop()
            },
            l = n ? {
                width: t(window).width(),
                height: t(window).height()
            } : null;
        return t.extend({}, s, a, l, o)
    }, e.prototype.getCalculatedOffset = function (t, e, i, n) {
        return "bottom" == t ? {
            top: e.top + e.height,
            left: e.left + e.width / 2 - i / 2
        } : "top" == t ? {
            top: e.top - n,
            left: e.left + e.width / 2 - i / 2
        } : "left" == t ? {
            top: e.top + e.height / 2 - n / 2,
            left: e.left - i
        } : {
            top: e.top + e.height / 2 - n / 2,
            left: e.left + e.width
        }
    }, e.prototype.getViewportAdjustedDelta = function (t, e, i, n) {
        var s = {
            top: 0,
            left: 0
        };
        if (!this.$viewport) return s;
        var r = this.options.viewport && this.options.viewport.padding || 0,
            o = this.getPosition(this.$viewport);
        if (/right|left/.test(t)) {
            var a = e.top - r - o.scroll,
                l = e.top + r - o.scroll + n;
            a < o.top ? s.top = o.top - a : l > o.top + o.height && (s.top = o.top + o.height - l)
        } else {
            var h = e.left - r,
                d = e.left + r + i;
            h < o.left ? s.left = o.left - h : d > o.right && (s.left = o.left + o.width - d)
        }
        return s
    }, e.prototype.getTitle = function () {
        var t = this.$element,
            e = this.options;
        return t.attr("data-original-title") || ("function" == typeof e.title ? e.title.call(t[0]) : e.title)
    }, e.prototype.getUID = function (t) {
        do {
            t += ~~(1e6 * Math.random())
        } while (document.getElementById(t));
        return t
    }, e.prototype.tip = function () {
        if (!this.$tip && (this.$tip = t(this.options.template), 1 != this.$tip.length)) throw new Error(this.type + " `template` option must consist of exactly 1 top-level element!");
        return this.$tip
    }, e.prototype.arrow = function () {
        return this.$arrow = this.$arrow || this.tip().find(".tooltip-arrow")
    }, e.prototype.enable = function () {
        this.enabled = !0
    }, e.prototype.disable = function () {
        this.enabled = !1
    }, e.prototype.toggleEnabled = function () {
        this.enabled = !this.enabled
    }, e.prototype.toggle = function (e) {
        var i = this;
        e && ((i = t(e.currentTarget).data("bs." + this.type)) || (i = new this.constructor(e.currentTarget, this.getDelegateOptions()), t(e.currentTarget).data("bs." + this.type, i))), e ? (i.inState.click = !i.inState.click, i.isInStateTrue() ? i.enter(i) : i.leave(i)) : i.tip().hasClass("in") ? i.leave(i) : i.enter(i)
    }, e.prototype.destroy = function () {
        var t = this;
        clearTimeout(this.timeout), this.hide(function () {
            t.$element.off("." + t.type).removeData("bs." + t.type), t.$tip && t.$tip.detach(), t.$tip = null, t.$arrow = null, t.$viewport = null, t.$element = null
        })
    };
    var i = t.fn.tooltip;
    t.fn.tooltip = function (i) {
        return this.each(function () {
            var n = t(this),
                s = n.data("bs.tooltip"),
                r = "object" == typeof i && i;
            !s && /destroy|hide/.test(i) || (s || n.data("bs.tooltip", s = new e(this, r)), "string" == typeof i && s[i]())
        })
    }, t.fn.tooltip.Constructor = e, t.fn.tooltip.noConflict = function () {
        return t.fn.tooltip = i, this
    }
}(jQuery),
function (t) {
    "use strict";
    var e = function (t, e) {
        this.init("popover", t, e)
    };
    if (!t.fn.tooltip) throw new Error("Popover%20requires%20tooltip.html");
    e.VERSION = "3.3.7", e.DEFAULTS = t.extend({}, t.fn.tooltip.Constructor.DEFAULTS, {
        placement: "right",
        trigger: "click",
        content: "",
        template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
    }), e.prototype = t.extend({}, t.fn.tooltip.Constructor.prototype), e.prototype.constructor = e, e.prototype.getDefaults = function () {
        return e.DEFAULTS
    }, e.prototype.setContent = function () {
        var t = this.tip(),
            e = this.getTitle(),
            i = this.getContent();
        t.find(".popover-title")[this.options.html ? "html" : "text"](e), t.find(".popover-content").children().detach().end()[this.options.html ? "string" == typeof i ? "html" : "append" : "text"](i), t.removeClass("fade top bottom left right in"), t.find(".popover-title").html() || t.find(".popover-title").hide()
    }, e.prototype.hasContent = function () {
        return this.getTitle() || this.getContent()
    }, e.prototype.getContent = function () {
        var t = this.$element,
            e = this.options;
        return t.attr("data-content") || ("function" == typeof e.content ? e.content.call(t[0]) : e.content)
    }, e.prototype.arrow = function () {
        return this.$arrow = this.$arrow || this.tip().find(".arrow")
    };
    var i = t.fn.popover;
    t.fn.popover = function (i) {
        return this.each(function () {
            var n = t(this),
                s = n.data("bs.popover"),
                r = "object" == typeof i && i;
            !s && /destroy|hide/.test(i) || (s || n.data("bs.popover", s = new e(this, r)), "string" == typeof i && s[i]())
        })
    }, t.fn.popover.Constructor = e, t.fn.popover.noConflict = function () {
        return t.fn.popover = i, this
    }
}(jQuery),
function (t) {
    "use strict";

    function e(i, n) {
        this.$body = t(document.body), this.$scrollElement = t(t(i).is(document.body) ? window : i), this.options = t.extend({}, e.DEFAULTS, n), this.selector = (this.options.target || "") + " .nav li > a", this.offsets = [], this.targets = [], this.activeTarget = null, this.scrollHeight = 0, this.$scrollElement.on("scroll.bs.scrollspy", t.proxy(this.process, this)), this.refresh(), this.process()
    }

    function i(i) {
        return this.each(function () {
            var n = t(this),
                s = n.data("bs.scrollspy"),
                r = "object" == typeof i && i;
            s || n.data("bs.scrollspy", s = new e(this, r)), "string" == typeof i && s[i]()
        })
    }
    e.VERSION = "3.3.7", e.DEFAULTS = {
        offset: 10
    }, e.prototype.getScrollHeight = function () {
        return this.$scrollElement[0].scrollHeight || Math.max(this.$body[0].scrollHeight, document.documentElement.scrollHeight)
    }, e.prototype.refresh = function () {
        var e = this,
            i = "offset",
            n = 0;
        this.offsets = [], this.targets = [], this.scrollHeight = this.getScrollHeight(), t.isWindow(this.$scrollElement[0]) || (i = "position", n = this.$scrollElement.scrollTop()), this.$body.find(this.selector).map(function () {
            var e = t(this),
                s = e.data("target") || e.attr("href"),
                r = /^#./.test(s) && t(s);
            return r && r.length && r.is(":visible") && [[r[i]().top + n, s]] || null
        }).sort(function (t, e) {
            return t[0] - e[0]
        }).each(function () {
            e.offsets.push(this[0]), e.targets.push(this[1])
        })
    }, e.prototype.process = function () {
        var t, e = this.$scrollElement.scrollTop() + this.options.offset,
            i = this.getScrollHeight(),
            n = this.options.offset + i - this.$scrollElement.height(),
            s = this.offsets,
            r = this.targets,
            o = this.activeTarget;
        if (this.scrollHeight != i && this.refresh(), e >= n) return o != (t = r[r.length - 1]) && this.activate(t);
        if (o && e < s[0]) return this.activeTarget = null, this.clear();
        for (t = s.length; t--;) o != r[t] && e >= s[t] && (void 0 === s[t + 1] || e < s[t + 1]) && this.activate(r[t])
    }, e.prototype.activate = function (e) {
        this.activeTarget = e, this.clear();
        var i = this.selector + '[data-target="' + e + '"],' + this.selector + '[href="' + e + '"]',
            n = t(i).parents("li").addClass("active");
        n.parent(".dropdown-menu").length && (n = n.closest("li.dropdown").addClass("active")), n.trigger("activate.bs.scrollspy")
    }, e.prototype.clear = function () {
        t(this.selector).parentsUntil(this.options.target, ".active").removeClass("active")
    };
    var n = t.fn.scrollspy;
    t.fn.scrollspy = i, t.fn.scrollspy.Constructor = e, t.fn.scrollspy.noConflict = function () {
        return t.fn.scrollspy = n, this
    }, t(window).on("load.bs.scrollspy.data-api", function () {
        t('[data-spy="scroll"]').each(function () {
            var e = t(this);
            i.call(e, e.data())
        })
    })
}(jQuery),
function (t) {
    "use strict";

    function e(e) {
        return this.each(function () {
            var n = t(this),
                s = n.data("bs.tab");
            s || n.data("bs.tab", s = new i(this)), "string" == typeof e && s[e]()
        })
    }
    var i = function (e) {
        this.element = t(e)
    };
    i.VERSION = "3.3.7", i.TRANSITION_DURATION = 150, i.prototype.show = function () {
        var e = this.element,
            i = e.closest("ul:not(.dropdown-menu)"),
            n = e.data("target");
        if (n || (n = e.attr("href"), n = n && n.replace(/.*(?=#[^\s]*$)/, "")), !e.parent("li").hasClass("active")) {
            var s = i.find(".active:last a"),
                r = t.Event("hide.bs.tab", {
                    relatedTarget: e[0]
                }),
                o = t.Event("show.bs.tab", {
                    relatedTarget: s[0]
                });
            if (s.trigger(r), e.trigger(o), !o.isDefaultPrevented() && !r.isDefaultPrevented()) {
                var a = t(n);
                this.activate(e.closest("li"), i), this.activate(a, a.parent(), function () {
                    s.trigger({
                        type: "hidden.bs.tab",
                        relatedTarget: e[0]
                    }), e.trigger({
                        type: "shown.bs.tab",
                        relatedTarget: s[0]
                    })
                })
            }
        }
    }, i.prototype.activate = function (e, n, s) {
        function r() {
            o.removeClass("active").find("> .dropdown-menu > .active").removeClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded", !1), e.addClass("active").find('[data-toggle="tab"]').attr("aria-expanded", !0), a ? (e[0].offsetWidth, e.addClass("in")) : e.removeClass("fade"), e.parent(".dropdown-menu").length && e.closest("li.dropdown").addClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded", !0), s && s()
        }
        var o = n.find("> .active"),
            a = s && t.support.transition && (o.length && o.hasClass("fade") || !!n.find("> .fade").length);
        o.length && a ? o.one("bsTransitionEnd", r).emulateTransitionEnd(i.TRANSITION_DURATION) : r(), o.removeClass("in")
    };
    var n = t.fn.tab;
    t.fn.tab = e, t.fn.tab.Constructor = i, t.fn.tab.noConflict = function () {
        return t.fn.tab = n, this
    };
    var s = function (i) {
        i.preventDefault(), e.call(t(this), "show")
    };
    t(document).on("click.bs.tab.data-api", '[data-toggle="tab"]', s).on("click.bs.tab.data-api", '[data-toggle="pill"]', s)
}(jQuery),
function (t) {
    "use strict";

    function e(e) {
        return this.each(function () {
            var n = t(this),
                s = n.data("bs.affix"),
                r = "object" == typeof e && e;
            s || n.data("bs.affix", s = new i(this, r)), "string" == typeof e && s[e]()
        })
    }
    var i = function (e, n) {
        this.options = t.extend({}, i.DEFAULTS, n), this.$target = t(this.options.target).on("scroll.bs.affix.data-api", t.proxy(this.checkPosition, this)).on("click.bs.affix.data-api", t.proxy(this.checkPositionWithEventLoop, this)), this.$element = t(e), this.affixed = null, this.unpin = null, this.pinnedOffset = null, this.checkPosition()
    };
    i.VERSION = "3.3.7", i.RESET = "affix affix-top affix-bottom", i.DEFAULTS = {
        offset: 0,
        target: window
    }, i.prototype.getState = function (t, e, i, n) {
        var s = this.$target.scrollTop(),
            r = this.$element.offset(),
            o = this.$target.height();
        if (null != i && "top" == this.affixed) return s < i && "top";
        if ("bottom" == this.affixed) return null != i ? !(s + this.unpin <= r.top) && "bottom" : !(s + o <= t - n) && "bottom";
        var a = null == this.affixed,
            l = a ? s : r.top;
        return null != i && s <= i ? "top" : null != n && l + (a ? o : e) >= t - n && "bottom"
    }, i.prototype.getPinnedOffset = function () {
        if (this.pinnedOffset) return this.pinnedOffset;
        this.$element.removeClass(i.RESET).addClass("affix");
        var t = this.$target.scrollTop(),
            e = this.$element.offset();
        return this.pinnedOffset = e.top - t
    }, i.prototype.checkPositionWithEventLoop = function () {
        setTimeout(t.proxy(this.checkPosition, this), 1)
    }, i.prototype.checkPosition = function () {
        if (this.$element.is(":visible")) {
            var e = this.$element.height(),
                n = this.options.offset,
                s = n.top,
                r = n.bottom,
                o = Math.max(t(document).height(), t(document.body).height());
            "object" != typeof n && (r = s = n), "function" == typeof s && (s = n.top(this.$element)), "function" == typeof r && (r = n.bottom(this.$element));
            var a = this.getState(o, e, s, r);
            if (this.affixed != a) {
                null != this.unpin && this.$element.css("top", "");
                var l = "affix" + (a ? "-" + a : ""),
                    h = t.Event(l + ".bs.affix");
                if (this.$element.trigger(h), h.isDefaultPrevented()) return;
                this.affixed = a, this.unpin = "bottom" == a ? this.getPinnedOffset() : null, this.$element.removeClass(i.RESET).addClass(l).trigger(l.replace("affix", "affixed") + ".bs.affix")
            }
            "bottom" == a && this.$element.offset({
                top: o - e - r
            })
        }
    };
    var n = t.fn.affix;
    t.fn.affix = e, t.fn.affix.Constructor = i, t.fn.affix.noConflict = function () {
        return t.fn.affix = n, this
    }, t(window).on("load", function () {
        t('[data-spy="affix"]').each(function () {
            var i = t(this),
                n = i.data();
            n.offset = n.offset || {}, null != n.offsetBottom && (n.offset.bottom = n.offsetBottom), null != n.offsetTop && (n.offset.top = n.offsetTop), e.call(i, n)
        })
    })
}(jQuery),
function () {
    var t;
    t = function () {
            function t(t, e) {
                var i, n;
                if (this.options = {
                        target: "instafeed",
                        get: "popular",
                        resolution: "thumbnail",
                        sortBy: "none",
                        links: !0,
                        mock: !1,
                        useHttp: !1
                    }, "object" == typeof t)
                    for (i in t) n = t[i], this.options[i] = n;
                this.context = null != e ? e : this, this.unique = this._genKey()
            }
            return t.prototype.hasNext = function () {
                return "string" == typeof this.context.nextUrl && this.context.nextUrl.length > 0
            }, t.prototype.next = function () {
                return !!this.hasNext() && this.run(this.context.nextUrl)
            }, t.prototype.run = function (e) {
                var i, n;
                if ("string" != typeof this.options.clientId && "string" != typeof this.options.accessToken) throw new Error("Missing clientId or accessToken.");
                if ("string" != typeof this.options.accessToken && "string" != typeof this.options.clientId) throw new Error("Missing clientId or accessToken.");
                return null != this.options.before && "function" == typeof this.options.before && this.options.before.call(this), "undefined" != typeof document && null !== document && (n = document.createElement("script"), n.id = "instafeed-fetcher", n.src = e || this._buildUrl(), document.getElementsByTagName("head")[0].appendChild(n), i = "instafeedCache" + this.unique, window[i] = new t(this.options, this), window[i].unique = this.unique), !0
            }, t.prototype.parse = function (t) {
                var e, i, n, s, r, o, a, l, h, d, c, u, p, f, m, g, v, y, b, w, x, C, T, E, S, _, k, $;
                if ("object" != typeof t) {
                    if (null != this.options.error && "function" == typeof this.options.error) return this.options.error.call(this, "Invalid JSON data"), !1;
                    throw new Error("Invalid JSON response")
                }
                if (200 !== t.meta.code) {
                    if (null != this.options.error && "function" == typeof this.options.error) return this.options.error.call(this, t.meta.error_message), !1;
                    throw new Error("Error from Instagram: " + t.meta.error_message)
                }
                if (0 === t.data.length) {
                    if (null != this.options.error && "function" == typeof this.options.error) return this.options.error.call(this, "No images were returned from Instagram"), !1;
                    throw new Error("No images were returned from Instagram")
                }
                if (null != this.options.success && "function" == typeof this.options.success && this.options.success.call(this, t), this.context.nextUrl = "", null != t.pagination && (this.context.nextUrl = t.pagination.next_url), "none" !== this.options.sortBy) switch (_ = "random" === this.options.sortBy ? ["", "random"] : this.options.sortBy.split("-"), S = "least" === _[0], _[1]) {
                    case "random":
                        t.data.sort(function () {
                            return .5 - Math.random()
                        });
                        break;
                    case "recent":
                        t.data = this._sortBy(t.data, "created_time", S);
                        break;
                    case "liked":
                        t.data = this._sortBy(t.data, "likes.count", S);
                        break;
                    case "commented":
                        t.data = this._sortBy(t.data, "comments.count", S);
                        break;
                    default:
                        throw new Error("Invalid option for sortBy: '" + this.options.sortBy + "'.")
                }
                if ("undefined" != typeof document && null !== document && !1 === this.options.mock) {
                    if (u = t.data, E = parseInt(this.options.limit, 10), null != this.options.limit && u.length > E && (u = u.slice(0, E)), o = document.createDocumentFragment(), null != this.options.filter && "function" == typeof this.options.filter && (u = this._filter(u, this.options.filter)), null != this.options.template && "string" == typeof this.options.template) {
                        for (a = "", "", "", $ = document.createElement("div"), l = 0, w = u.length; l < w; l++) {
                            if (h = u[l], "object" != typeof (d = h.images[this.options.resolution])) throw r = "No image found for resolution: " + this.options.resolution + ".", new Error(r);
                            m = "square", (g = d.width) > (f = d.height) && (m = "landscape"), g < f && (m = "portrait"), c = d.url, window.location.protocol.indexOf("http") >= 0 && !this.options.useHttp && (c = c.replace(/https?:\/\//, "//")), a += this._makeTemplate(this.options.template, {
                                model: h,
                                id: h.id,
                                link: h.link,
                                type: h.type,
                                image: c,
                                width: g,
                                height: f,
                                orientation: m,
                                caption: this._getObjectProperty(h, "caption.text"),
                                likes: h.likes.count,
                                comments: h.comments.count,
                                location: this._getObjectProperty(h, "location.name")
                            })
                        }
                        for ($.innerHTML = a, s = [], n = 0, i = $.childNodes.length; n < i;) s.push($.childNodes[n]), n += 1;
                        for (y = 0, x = s.length; y < x; y++) T = s[y], o.appendChild(T)
                    } else
                        for (b = 0, C = u.length; b < C; b++) {
                            if (h = u[b], p = document.createElement("img"), "object" != typeof (d = h.images[this.options.resolution])) throw r = "No image found for resolution: " + this.options.resolution + ".", new Error(r);
                            c = d.url, window.location.protocol.indexOf("http") >= 0 && !this.options.useHttp && (c = c.replace(/https?:\/\//, "//")), p.src = c, !0 === this.options.links ? (e = document.createElement("a"), e.href = h.link, e.appendChild(p), o.appendChild(e)) : o.appendChild(p)
                        }
                    if ("string" == typeof (k = this.options.target) && (k = document.getElementById(k)), null == k) throw r = 'No element with id="' + this.options.target + '" on page.', new Error(r);
                    k.appendChild(o), document.getElementsByTagName("head")[0].removeChild(document.getElementById("instafeed-fetcher")), v = "instafeedCache" + this.unique, window[v] = void 0;
                    try {
                        delete window[v]
                    } catch (t) {
                        t
                    }
                }
                return null != this.options.after && "function" == typeof this.options.after && this.options.after.call(this), !0
            }, t.prototype._buildUrl = function () {
                var t, e, i;
                switch (t = "https://api.instagram.com/v1", this.options.get) {
                    case "popular":
                        e = "media/popular";
                        break;
                    case "tagged":
                        if (!this.options.tagName) throw new Error("No tag name specified. Use the 'tagName' option.");
                        e = "tags/" + this.options.tagName + "/media/recent";
                        break;
                    case "location":
                        if (!this.options.locationId) throw new Error("No location specified. Use the 'locationId' option.");
                        e = "locations/" + this.options.locationId + "/media/recent";
                        break;
                    case "user":
                        if (!this.options.userId) throw new Error("No user specified. Use the 'userId' option.");
                        e = "users/" + this.options.userId + "/media/recent";
                        break;
                    default:
                        throw new Error("Invalid option for get: '" + this.options.get + "'.")
                }
                return i = t + "/" + e, null != this.options.accessToken ? i += "?access_token=" + this.options.accessToken : i += "?client_id=" + this.options.clientId, null != this.options.limit && (i += "&count=" + this.options.limit), i += "&callback=instafeedCache" + this.unique + ".parse"
            }, t.prototype._genKey = function () {
                var t;
                return "" + (t = function () {
                    return (65536 * (1 + Math.random()) | 0).toString(16).substring(1)
                })() + t() + t() + t()
            }, t.prototype._makeTemplate = function (t, e) {
                var i, n, s, r, o;
                for (n = /(?:\{{2})([\w\[\]\.]+)(?:\}{2})/, i = t; n.test(i);) r = i.match(n)[1], o = null != (s = this._getObjectProperty(e, r)) ? s : "", i = i.replace(n, function () {
                    return "" + o
                });
                return i
            }, t.prototype._getObjectProperty = function (t, e) {
                var i, n;
                for (n = (e = e.replace(/\[(\w+)\]/g, ".$1")).split("."); n.length;) {
                    if (i = n.shift(), !(null != t && i in t)) return null;
                    t = t[i]
                }
                return t
            }, t.prototype._sortBy = function (t, e, i) {
                var n;
                return n = function (t, n) {
                    var s, r;
                    return s = this._getObjectProperty(t, e), r = this._getObjectProperty(n, e), i ? s > r ? 1 : -1 : s < r ? 1 : -1
                }, t.sort(n.bind(this)), t
            }, t.prototype._filter = function (t, e) {
                var i, n, s, r, o;
                for (i = [], n = function (t) {
                        if (e(t)) return i.push(t)
                    }, s = 0, o = t.length; s < o; s++) r = t[s], n(r);
                return i
            }, t
        }(),
        function (t, e) {
            "function" == typeof define && define.amd ? define([], e) : "object" == typeof module && module.exports ? module.exports = e() : t.Instafeed = e()
        }(this, function () {
            return t
        })
}.call(this),
    function (t, e, i) {
        function n(t, i) {
            var n = e(t);
            n.data(s, this), this._$element = n, this.shares = [], this._init(i), this._render()
        }
        var s = "JSSocials",
            r = function (t, i) {
                return e.isFunction(t) ? t.apply(i, e.makeArray(arguments).slice(2)) : t
            },
            o = /(\.(jpeg|png|gif|bmp|svg)$|^data:image\/(jpeg|png|gif|bmp|svg\+xml);base64)/i,
            a = /(&?[a-zA-Z0-9]+=)?\{([a-zA-Z0-9]+)\}/g,
            l = {
                G: 1e9,
                M: 1e6,
                K: 1e3
            },
            h = {};
        n.prototype = {
            url: "",
            text: "",
            shareIn: "blank",
            showLabel: function (t) {
                return !1 === this.showCount ? t > this.smallScreenWidth : t >= this.largeScreenWidth
            },
            showCount: function (t) {
                return !(t <= this.smallScreenWidth) || "inside"
            },
            smallScreenWidth: 640,
            largeScreenWidth: 1024,
            resizeTimeout: 200,
            elementClass: "jssocials",
            sharesClass: "jssocials-shares",
            shareClass: "jssocials-share",
            shareButtonClass: "jssocials-share-button",
            shareLinkClass: "jssocials-share-link",
            shareLogoClass: "jssocials-share-logo",
            shareLabelClass: "jssocials-share-label",
            shareLinkCountClass: "jssocials-share-link-count",
            shareCountBoxClass: "jssocials-share-count-box",
            shareCountClass: "jssocials-share-count",
            shareZeroCountClass: "jssocials-share-no-count",
            _init: function (t) {
                this._initDefaults(), e.extend(this, t), this._initShares(), this._attachWindowResizeCallback()
            },
            _initDefaults: function () {
                this.url = t.location.href, this.text = e.trim(e("meta[name=description]").attr("content") || e("title").text())
            },
            _initShares: function () {
                this.shares = e.map(this.shares, e.proxy(function (t) {
                    "string" == typeof t && (t = {
                        share: t
                    });
                    var i = t.share && h[t.share];
                    if (!i && !t.renderer) throw Error("Share '" + t.share + "' is not found");
                    return e.extend({
                        url: this.url,
                        text: this.text
                    }, i, t)
                }, this))
            },
            _attachWindowResizeCallback: function () {
                e(t).on("resize", e.proxy(this._windowResizeHandler, this))
            },
            _detachWindowResizeCallback: function () {
                e(t).off("resize", this._windowResizeHandler)
            },
            _windowResizeHandler: function () {
                (e.isFunction(this.showLabel) || e.isFunction(this.showCount)) && (t.clearTimeout(this._resizeTimer), this._resizeTimer = setTimeout(e.proxy(this.refresh, this), this.resizeTimeout))
            },
            _render: function () {
                this._clear(), this._defineOptionsByScreen(), this._$element.addClass(this.elementClass), this._$shares = e("<div>").addClass(this.sharesClass).appendTo(this._$element), this._renderShares()
            },
            _defineOptionsByScreen: function () {
                this._screenWidth = e(t).width(), this._showLabel = r(this.showLabel, this, this._screenWidth), this._showCount = r(this.showCount, this, this._screenWidth)
            },
            _renderShares: function () {
                e.each(this.shares, e.proxy(function (t, e) {
                    this._renderShare(e)
                }, this))
            },
            _renderShare: function (t) {
                (e.isFunction(t.renderer) ? e(t.renderer()) : this._createShare(t)).addClass(this.shareClass).addClass(t.share ? "jssocials-share-" + t.share : "").addClass(t.css).appendTo(this._$shares)
            },
            _createShare: function (t) {
                var i = e("<div>"),
                    n = this._createShareLink(t).appendTo(i);
                if (this._showCount) {
                    var s = "inside" === this._showCount,
                        r = s ? n : e("<div>").addClass(this.shareCountBoxClass).appendTo(i);
                    r.addClass(s ? this.shareLinkCountClass : this.shareCountBoxClass), this._renderShareCount(t, r)
                }
                return i
            },
            _createShareLink: function (t) {
                var i = this._getShareStrategy(t).call(t, {
                    shareUrl: this._getShareUrl(t)
                });
                return i.addClass(this.shareLinkClass).append(this._createShareLogo(t)), this._showLabel && i.append(this._createShareLabel(t)), e.each(this.on || {}, function (n, s) {
                    e.isFunction(s) && i.on(n, e.proxy(s, t))
                }), i
            },
            _getShareStrategy: function (t) {
                var e = d[t.shareIn || this.shareIn];
                if (!e) throw Error("Share strategy '" + this.shareIn + "' not found");
                return e
            },
            _getShareUrl: function (t) {
                var e = r(t.shareUrl, t);
                return this._formatShareUrl(e, t)
            },
            _createShareLogo: function (t) {
                var i = t.logo,
                    n = o.test(i) ? e("<img>").attr("src", t.logo) : e("<i>").addClass(i);
                return n.addClass(this.shareLogoClass), n
            },
            _createShareLabel: function (t) {
                return e("<span>").addClass(this.shareLabelClass).text(t.label)
            },
            _renderShareCount: function (t, i) {
                var n = e("<span>").addClass(this.shareCountClass);
                i.addClass(this.shareZeroCountClass).append(n), this._loadCount(t).done(e.proxy(function (t) {
                    t && (i.removeClass(this.shareZeroCountClass), n.text(t))
                }, this))
            },
            _loadCount: function (t) {
                var i = e.Deferred(),
                    n = this._getCountUrl(t);
                if (!n) return i.resolve(0).promise();
                var s = e.proxy(function (e) {
                    i.resolve(this._getCountValue(e, t))
                }, this);
                return e.getJSON(n).done(s).fail(function () {
                    e.get(n).done(s).fail(function () {
                        i.resolve(0)
                    })
                }), i.promise()
            },
            _getCountUrl: function (t) {
                var e = r(t.countUrl, t);
                return this._formatShareUrl(e, t)
            },
            _getCountValue: function (t, i) {
                var n = (e.isFunction(i.getCount) ? i.getCount(t) : t) || 0;
                return "string" == typeof n ? n : this._formatNumber(n)
            },
            _formatNumber: function (t) {
                return e.each(l, function (e, i) {
                    return t >= i ? (t = parseFloat((t / i).toFixed(2)) + e, !1) : void 0
                }), t
            },
            _formatShareUrl: function (e, i) {
                return e.replace(a, function (e, n, s) {
                    var r = i[s] || "";
                    return r ? (n || "") + t.encodeURIComponent(r) : ""
                })
            },
            _clear: function () {
                t.clearTimeout(this._resizeTimer), this._$element.empty()
            },
            _passOptionToShares: function (t, i) {
                var n = this.shares;
                e.each(["url", "text"], function (s, r) {
                    r === t && e.each(n, function (e, n) {
                        n[t] = i
                    })
                })
            },
            _normalizeShare: function (t) {
                return e.isNumeric(t) ? this.shares[t] : "string" == typeof t ? e.grep(this.shares, function (e) {
                    return e.share === t
                })[0] : t
            },
            refresh: function () {
                this._render()
            },
            destroy: function () {
                this._clear(), this._detachWindowResizeCallback(), this._$element.removeClass(this.elementClass).removeData(s)
            },
            option: function (t, e) {
                return 1 === arguments.length ? this[t] : (this[t] = e, this._passOptionToShares(t, e), void this.refresh())
            },
            shareOption: function (t, e, i) {
                return t = this._normalizeShare(t), 2 === arguments.length ? t[e] : (t[e] = i, void this.refresh())
            }
        }, e.fn.jsSocials = function (t) {
            var i = e.makeArray(arguments).slice(1),
                r = this;
            return this.each(function () {
                var o, a = e(this),
                    l = a.data(s);
                if (l)
                    if ("string" == typeof t) {
                        if (void 0 !== (o = l[t].apply(l, i)) && o !== l) return r = o, !1
                    } else l._detachWindowResizeCallback(), l._init(t), l._render();
                else new n(a, t)
            }), r
        };
        var d = {
            popup: function (i) {
                return e("<a>").attr("href", "#").on("click", function () {
                    return t.open(i.shareUrl, null, "width=600, height=400, location=0, menubar=0, resizeable=0, scrollbars=0, status=0, titlebar=0, toolbar=0"), !1
                })
            },
            blank: function (t) {
                return e("<a>").attr({
                    target: "_blank",
                    href: t.shareUrl
                })
            },
            self: function (t) {
                return e("<a>").attr({
                    target: "_self",
                    href: t.shareUrl
                })
            }
        };
        t.jsSocials = {
            Socials: n,
            shares: h,
            shareStrategies: d,
            setDefaults: function (t) {
                var i;
                e.isPlainObject(t) ? i = n.prototype : (i = h[t], t = arguments[1] || {}), e.extend(i, t)
            }
        }
    }(window, jQuery),
    function (t, e, i) {
        e.extend(i.shares, {
            email: {
                label: "E-mail",
                logo: "fa fa-at",
                shareUrl: "mailto:{to}?subject={text}&body={url}",
                countUrl: "",
                shareIn: "self"
            },
            twitter: {
                label: "Tweet",
                logo: "fa fa-twitter",
                shareUrl: "https://twitter.com/share?url={url}&text={text}&via={via}&hashtags={hashtags}",
                countUrl: ""
            },
            facebook: {
                label: "Like",
                logo: "fa fa-facebook",
                shareUrl: "https://facebook.com/sharer/sharer.php?u={url}",
                countUrl: "https://graph.facebook.com/?id={url}",
                getCount: function (t) {
                    return t.share && t.share.share_count || 0
                }
            },
            vkontakte: {
                label: "Like",
                logo: "fa fa-vk",
                shareUrl: "https://vk.com/share.php?url={url}&title={title}&description={text}",
                countUrl: "https://vk.com/share.php?act=count&index=1&url={url}",
                getCount: function (t) {
                    return parseInt(t.slice(15, -2).split(", ")[1])
                }
            },
            googleplus: {
                label: "+1",
                logo: "fa fa-google",
                shareUrl: "https://plus.google.com/share?url={url}",
                countUrl: ""
            },
            linkedin: {
                label: "Share",
                logo: "fa fa-linkedin",
                shareUrl: "https://www.linkedin.com/shareArticle?mini=true&url={url}",
                countUrl: "https://www.linkedin.com/countserv/count/share?format=jsonp&url={url}&callback=?",
                getCount: function (t) {
                    return t.count
                }
            },
            pinterest: {
                label: "Pin it",
                logo: "fa fa-pinterest",
                shareUrl: "https://pinterest.com/pin/create/bookmarklet/?media={media}&url={url}&description={text}",
                countUrl: "https://api.pinterest.com/v1/urls/count.json?&url={url}&callback=?",
                getCount: function (t) {
                    return t.count
                }
            },
            stumbleupon: {
                label: "Share",
                logo: "fa fa-stumbleupon",
                shareUrl: "http://www.stumbleupon.com/submit?url={url}&title={title}",
                countUrl: "https://cors-anywhere.herokuapp.com/https://www.stumbleupon.com/services/1.01/badge.getinfo?url={url}",
                getCount: function (t) {
                    return t.result.views
                }
            },
            telegram: {
                label: "Telegram",
                logo: "fa fa-paper-plane",
                shareUrl: "tg://msg?text={url} {text}",
                countUrl: "",
                shareIn: "self"
            },
            whatsapp: {
                label: "WhatsApp",
                logo: "fa fa-whatsapp",
                shareUrl: "whatsapp://send?text={url} {text}",
                countUrl: "",
                shareIn: "self"
            },
            line: {
                label: "LINE",
                logo: "fa fa-comment",
                shareUrl: "http://line.me/R/msg/text/?{text} {url}",
                countUrl: ""
            },
            viber: {
                label: "Viber",
                logo: "fa fa-volume-control-phone",
                shareUrl: "viber://forward?text={url} {text}",
                countUrl: "",
                shareIn: "self"
            },
            pocket: {
                label: "Pocket",
                logo: "fa fa-get-pocket",
                shareUrl: "https://getpocket.com/save?url={url}&title={title}",
                countUrl: ""
            },
            messenger: {
                label: "Share",
                logo: "fa fa-commenting",
                shareUrl: "fb-messenger://share?link={url}",
                countUrl: "",
                shareIn: "self"
            }
        })
    }(window, jQuery, window.jsSocials),
    function (t, e) {
        "function" == typeof define && define.amd ? define("jquery-bridget/jquery-bridget", ["jquery"], function (i) {
            return e(t, i)
        }) : "object" == typeof module && module.exports ? module.exports = e(t, require("jquery")) : t.jQueryBridget = e(t, t.jQuery)
    }(window, function (t, e) {
        "use strict";

        function i(i, r, a) {
            (a = a || e || t.jQuery) && (r.prototype.option || (r.prototype.option = function (t) {
                a.isPlainObject(t) && (this.options = a.extend(!0, this.options, t))
            }), a.fn[i] = function (t) {
                if ("string" == typeof t) {
                    return function (t, e, n) {
                        var s, r = "$()." + i + '("' + e + '")';
                        return t.each(function (t, l) {
                            var h = a.data(l, i);
                            if (h) {
                                var d = h[e];
                                if (d && "_" != e.charAt(0)) {
                                    var c = d.apply(h, n);
                                    s = void 0 === s ? c : s
                                } else o(r + " is not a valid method")
                            } else o(i + " not initialized. Cannot call methods, i.e. " + r)
                        }), void 0 !== s ? s : t
                    }(this, t, s.call(arguments, 1))
                }
                return function (t, e) {
                    t.each(function (t, n) {
                        var s = a.data(n, i);
                        s ? (s.option(e), s._init()) : (s = new r(n, e), a.data(n, i, s))
                    })
                }(this, t), this
            }, n(a))
        }

        function n(t) {
            !t || t && t.bridget || (t.bridget = i)
        }
        var s = Array.prototype.slice,
            r = t.console,
            o = void 0 === r ? function () {} : function (t) {
                r.error(t)
            };
        return n(e || t.jQuery), i
    }),
    function (t, e) {
        "function" == typeof define && define.amd ? define("ev-emitter/ev-emitter", e) : "object" == typeof module && module.exports ? module.exports = e() : t.EvEmitter = e()
    }("undefined" != typeof window ? window : this, function () {
        function t() {}
        var e = t.prototype;
        return e.on = function (t, e) {
            if (t && e) {
                var i = this._events = this._events || {},
                    n = i[t] = i[t] || [];
                return -1 == n.indexOf(e) && n.push(e), this
            }
        }, e.once = function (t, e) {
            if (t && e) {
                this.on(t, e);
                var i = this._onceEvents = this._onceEvents || {};
                return (i[t] = i[t] || {})[e] = !0, this
            }
        }, e.off = function (t, e) {
            var i = this._events && this._events[t];
            if (i && i.length) {
                var n = i.indexOf(e);
                return -1 != n && i.splice(n, 1), this
            }
        }, e.emitEvent = function (t, e) {
            var i = this._events && this._events[t];
            if (i && i.length) {
                i = i.slice(0), e = e || [];
                for (var n = this._onceEvents && this._onceEvents[t], s = 0; s < i.length; s++) {
                    var r = i[s];
                    n && n[r] && (this.off(t, r), delete n[r]), r.apply(this, e)
                }
                return this
            }
        }, e.allOff = function () {
            delete this._events, delete this._onceEvents
        }, t
    }),
    function (t, e) {
        "use strict";
        "function" == typeof define && define.amd ? define("get-size/get-size", [], function () {
            return e()
        }) : "object" == typeof module && module.exports ? module.exports = e() : t.getSize = e()
    }(window, function () {
        "use strict";

        function t(t) {
            var e = parseFloat(t);
            return -1 == t.indexOf("%") && !isNaN(e) && e
        }

        function e(t) {
            var e = getComputedStyle(t);
            return e || s("Style returned " + e + ". Are you running this code in a hidden iframe on Firefox? See http://bit.ly/getsizebug1"), e
        }

        function i(s) {
            if (function () {
                    if (!a) {
                        a = !0;
                        var s = document.createElement("div");
                        s.style.width = "200px", s.style.padding = "1px 2px 3px 4px", s.style.borderStyle = "solid", s.style.borderWidth = "1px 2px 3px 4px", s.style.boxSizing = "border-box";
                        var r = document.body || document.documentElement;
                        r.appendChild(s);
                        var o = e(s);
                        i.isBoxSizeOuter = n = 200 == t(o.width), r.removeChild(s)
                    }
                }(), "string" == typeof s && (s = document.querySelector(s)), s && "object" == typeof s && s.nodeType) {
                var l = e(s);
                if ("none" == l.display) return function () {
                    for (var t = {
                            width: 0,
                            height: 0,
                            innerWidth: 0,
                            innerHeight: 0,
                            outerWidth: 0,
                            outerHeight: 0
                        }, e = 0; e < o; e++) t[r[e]] = 0;
                    return t
                }();
                var h = {};
                h.width = s.offsetWidth, h.height = s.offsetHeight;
                for (var d = h.isBorderBox = "border-box" == l.boxSizing, c = 0; c < o; c++) {
                    var u = r[c],
                        p = l[u],
                        f = parseFloat(p);
                    h[u] = isNaN(f) ? 0 : f
                }
                var m = h.paddingLeft + h.paddingRight,
                    g = h.paddingTop + h.paddingBottom,
                    v = h.marginLeft + h.marginRight,
                    y = h.marginTop + h.marginBottom,
                    b = h.borderLeftWidth + h.borderRightWidth,
                    w = h.borderTopWidth + h.borderBottomWidth,
                    x = d && n,
                    C = t(l.width);
                !1 !== C && (h.width = C + (x ? 0 : m + b));
                var T = t(l.height);
                return !1 !== T && (h.height = T + (x ? 0 : g + w)), h.innerWidth = h.width - (m + b), h.innerHeight = h.height - (g + w), h.outerWidth = h.width + v, h.outerHeight = h.height + y, h
            }
        }
        var n, s = "undefined" == typeof console ? function () {} : function (t) {
                console.error(t)
            },
            r = ["paddingLeft", "paddingRight", "paddingTop", "paddingBottom", "marginLeft", "marginRight", "marginTop", "marginBottom", "borderLeftWidth", "borderRightWidth", "borderTopWidth", "borderBottomWidth"],
            o = r.length,
            a = !1;
        return i
    }),
    function (t, e) {
        "use strict";
        "function" == typeof define && define.amd ? define("desandro-matches-selector/matches-selector", e) : "object" == typeof module && module.exports ? module.exports = e() : t.matchesSelector = e()
    }(window, function () {
        "use strict";
        var t = function () {
            var t = window.Element.prototype;
            if (t.matches) return "matches";
            if (t.matchesSelector) return "matchesSelector";
            for (var e = ["webkit", "moz", "ms", "o"], i = 0; i < e.length; i++) {
                var n = e[i] + "MatchesSelector";
                if (t[n]) return n
            }
        }();
        return function (e, i) {
            return e[t](i)
        }
    }),
    function (t, e) {
        "function" == typeof define && define.amd ? define("fizzy-ui-utils/utils", ["desandro-matches-selector/matches-selector"], function (i) {
            return e(t, i)
        }) : "object" == typeof module && module.exports ? module.exports = e(t, require("desandro-matches-selector")) : t.fizzyUIUtils = e(t, t.matchesSelector)
    }(window, function (t, e) {
        var i = {};
        i.extend = function (t, e) {
            for (var i in e) t[i] = e[i];
            return t
        }, i.modulo = function (t, e) {
            return (t % e + e) % e
        }, i.makeArray = function (t) {
            var e = [];
            if (Array.isArray(t)) e = t;
            else if (t && "object" == typeof t && "number" == typeof t.length)
                for (var i = 0; i < t.length; i++) e.push(t[i]);
            else e.push(t);
            return e
        }, i.removeFrom = function (t, e) {
            var i = t.indexOf(e); - 1 != i && t.splice(i, 1)
        }, i.getParent = function (t, i) {
            for (; t.parentNode && t != document.body;)
                if (t = t.parentNode, e(t, i)) return t
        }, i.getQueryElement = function (t) {
            return "string" == typeof t ? document.querySelector(t) : t
        }, i.handleEvent = function (t) {
            var e = "on" + t.type;
            this[e] && this[e](t)
        }, i.filterFindElements = function (t, n) {
            var s = [];
            return (t = i.makeArray(t)).forEach(function (t) {
                if (t instanceof HTMLElement)
                    if (n) {
                        e(t, n) && s.push(t);
                        for (var i = t.querySelectorAll(n), r = 0; r < i.length; r++) s.push(i[r])
                    } else s.push(t)
            }), s
        }, i.debounceMethod = function (t, e, i) {
            var n = t.prototype[e],
                s = e + "Timeout";
            t.prototype[e] = function () {
                var t = this[s];
                t && clearTimeout(t);
                var e = arguments,
                    r = this;
                this[s] = setTimeout(function () {
                    n.apply(r, e), delete r[s]
                }, i || 100)
            }
        }, i.docReady = function (t) {
            var e = document.readyState;
            "complete" == e || "interactive" == e ? setTimeout(t) : document.addEventListener("DOMContentLoaded", t)
        }, i.toDashed = function (t) {
            return t.replace(/(.)([A-Z])/g, function (t, e, i) {
                return e + "-" + i
            }).toLowerCase()
        };
        var n = t.console;
        return i.htmlInit = function (e, s) {
            i.docReady(function () {
                var r = i.toDashed(s),
                    o = "data-" + r,
                    a = document.querySelectorAll("[" + o + "]"),
                    l = document.querySelectorAll(".js-" + r),
                    h = i.makeArray(a).concat(i.makeArray(l)),
                    d = o + "-options",
                    c = t.jQuery;
                h.forEach(function (t) {
                    var i, r = t.getAttribute(o) || t.getAttribute(d);
                    try {
                        i = r && JSON.parse(r)
                    } catch (e) {
                        return void(n && n.error("Error parsing " + o + " on " + t.className + ": " + e))
                    }
                    var a = new e(t, i);
                    c && c.data(t, s, a)
                })
            })
        }, i
    }),
    function (t, e) {
        "function" == typeof define && define.amd ? define("outlayer/item", ["ev-emitter/ev-emitter", "get-size/get-size"], e) : "object" == typeof module && module.exports ? module.exports = e(require("ev-emitter"), require("get-size")) : (t.Outlayer = {}, t.Outlayer.Item = e(t.EvEmitter, t.getSize))
    }(window, function (t, e) {
        "use strict";

        function i(t, e) {
            t && (this.element = t, this.layout = e, this.position = {
                x: 0,
                y: 0
            }, this._create())
        }
        var n = document.documentElement.style,
            s = "string" == typeof n.transition ? "transition" : "WebkitTransition",
            r = "string" == typeof n.transform ? "transform" : "WebkitTransform",
            o = {
                WebkitTransition: "webkitTransitionEnd",
                transition: "transitionend"
            }[s],
            a = {
                transform: r,
                transition: s,
                transitionDuration: s + "Duration",
                transitionProperty: s + "Property",
                transitionDelay: s + "Delay"
            },
            l = i.prototype = Object.create(t.prototype);
        l.constructor = i, l._create = function () {
            this._transn = {
                ingProperties: {},
                clean: {},
                onEnd: {}
            }, this.css({
                position: "absolute"
            })
        }, l.handleEvent = function (t) {
            var e = "on" + t.type;
            this[e] && this[e](t)
        }, l.getSize = function () {
            this.size = e(this.element)
        }, l.css = function (t) {
            var e = this.element.style;
            for (var i in t) {
                e[a[i] || i] = t[i]
            }
        }, l.getPosition = function () {
            var t = getComputedStyle(this.element),
                e = this.layout._getOption("originLeft"),
                i = this.layout._getOption("originTop"),
                n = t[e ? "left" : "right"],
                s = t[i ? "top" : "bottom"],
                r = this.layout.size,
                o = -1 != n.indexOf("%") ? parseFloat(n) / 100 * r.width : parseInt(n, 10),
                a = -1 != s.indexOf("%") ? parseFloat(s) / 100 * r.height : parseInt(s, 10);
            o = isNaN(o) ? 0 : o, a = isNaN(a) ? 0 : a, o -= e ? r.paddingLeft : r.paddingRight, a -= i ? r.paddingTop : r.paddingBottom, this.position.x = o, this.position.y = a
        }, l.layoutPosition = function () {
            var t = this.layout.size,
                e = {},
                i = this.layout._getOption("originLeft"),
                n = this.layout._getOption("originTop"),
                s = i ? "paddingLeft" : "paddingRight",
                r = i ? "left" : "right",
                o = i ? "right" : "left",
                a = this.position.x + t[s];
            e[r] = this.getXValue(a), e[o] = "";
            var l = n ? "paddingTop" : "paddingBottom",
                h = n ? "top" : "bottom",
                d = n ? "bottom" : "top",
                c = this.position.y + t[l];
            e[h] = this.getYValue(c), e[d] = "", this.css(e), this.emitEvent("layout", [this])
        }, l.getXValue = function (t) {
            var e = this.layout._getOption("horizontal");
            return this.layout.options.percentPosition && !e ? t / this.layout.size.width * 100 + "%" : t + "px"
        }, l.getYValue = function (t) {
            var e = this.layout._getOption("horizontal");
            return this.layout.options.percentPosition && e ? t / this.layout.size.height * 100 + "%" : t + "px"
        }, l._transitionTo = function (t, e) {
            this.getPosition();
            var i = this.position.x,
                n = this.position.y,
                s = parseInt(t, 10),
                r = parseInt(e, 10),
                o = s === this.position.x && r === this.position.y;
            if (this.setPosition(t, e), !o || this.isTransitioning) {
                var a = t - i,
                    l = e - n,
                    h = {};
                h.transform = this.getTranslate(a, l), this.transition({
                    to: h,
                    onTransitionEnd: {
                        transform: this.layoutPosition
                    },
                    isCleaning: !0
                })
            } else this.layoutPosition()
        }, l.getTranslate = function (t, e) {
            var i = this.layout._getOption("originLeft"),
                n = this.layout._getOption("originTop");
            return t = i ? t : -t, e = n ? e : -e, "translate3d(" + t + "px, " + e + "px, 0)"
        }, l.goTo = function (t, e) {
            this.setPosition(t, e), this.layoutPosition()
        }, l.moveTo = l._transitionTo, l.setPosition = function (t, e) {
            this.position.x = parseInt(t, 10), this.position.y = parseInt(e, 10)
        }, l._nonTransition = function (t) {
            this.css(t.to), t.isCleaning && this._removeStyles(t.to);
            for (var e in t.onTransitionEnd) t.onTransitionEnd[e].call(this)
        }, l.transition = function (t) {
            if (parseFloat(this.layout.options.transitionDuration)) {
                var e = this._transn;
                for (var i in t.onTransitionEnd) e.onEnd[i] = t.onTransitionEnd[i];
                for (i in t.to) e.ingProperties[i] = !0, t.isCleaning && (e.clean[i] = !0);
                if (t.from) {
                    this.css(t.from);
                    this.element.offsetHeight;
                    null
                }
                this.enableTransition(t.to), this.css(t.to), this.isTransitioning = !0
            } else this._nonTransition(t)
        };
        var h = "opacity," + function (t) {
            return t.replace(/([A-Z])/g, function (t) {
                return "-" + t.toLowerCase()
            })
        }(r);
        l.enableTransition = function () {
            if (!this.isTransitioning) {
                var t = this.layout.options.transitionDuration;
                t = "number" == typeof t ? t + "ms" : t, this.css({
                    transitionProperty: h,
                    transitionDuration: t,
                    transitionDelay: this.staggerDelay || 0
                }), this.element.addEventListener(o, this, !1)
            }
        }, l.onwebkitTransitionEnd = function (t) {
            this.ontransitionend(t)
        }, l.onotransitionend = function (t) {
            this.ontransitionend(t)
        };
        var d = {
            "-webkit-transform": "transform"
        };
        l.ontransitionend = function (t) {
            if (t.target === this.element) {
                var e = this._transn,
                    i = d[t.propertyName] || t.propertyName;
                if (delete e.ingProperties[i], function (t) {
                        for (var e in t) return !1;
                        return !0
                    }(e.ingProperties) && this.disableTransition(), i in e.clean && (this.element.style[t.propertyName] = "", delete e.clean[i]), i in e.onEnd) {
                    e.onEnd[i].call(this), delete e.onEnd[i]
                }
                this.emitEvent("transitionEnd", [this])
            }
        }, l.disableTransition = function () {
            this.removeTransitionStyles(), this.element.removeEventListener(o, this, !1), this.isTransitioning = !1
        }, l._removeStyles = function (t) {
            var e = {};
            for (var i in t) e[i] = "";
            this.css(e)
        };
        var c = {
            transitionProperty: "",
            transitionDuration: "",
            transitionDelay: ""
        };
        return l.removeTransitionStyles = function () {
            this.css(c)
        }, l.stagger = function (t) {
            t = isNaN(t) ? 0 : t, this.staggerDelay = t + "ms"
        }, l.removeElem = function () {
            this.element.parentNode.removeChild(this.element), this.css({
                display: ""
            }), this.emitEvent("remove", [this])
        }, l.remove = function () {
            s && parseFloat(this.layout.options.transitionDuration) ? (this.once("transitionEnd", function () {
                this.removeElem()
            }), this.hide()) : this.removeElem()
        }, l.reveal = function () {
            delete this.isHidden, this.css({
                display: ""
            });
            var t = this.layout.options,
                e = {};
            e[this.getHideRevealTransitionEndProperty("visibleStyle")] = this.onRevealTransitionEnd, this.transition({
                from: t.hiddenStyle,
                to: t.visibleStyle,
                isCleaning: !0,
                onTransitionEnd: e
            })
        }, l.onRevealTransitionEnd = function () {
            this.isHidden || this.emitEvent("reveal")
        }, l.getHideRevealTransitionEndProperty = function (t) {
            var e = this.layout.options[t];
            if (e.opacity) return "opacity";
            for (var i in e) return i
        }, l.hide = function () {
            this.isHidden = !0, this.css({
                display: ""
            });
            var t = this.layout.options,
                e = {};
            e[this.getHideRevealTransitionEndProperty("hiddenStyle")] = this.onHideTransitionEnd, this.transition({
                from: t.visibleStyle,
                to: t.hiddenStyle,
                isCleaning: !0,
                onTransitionEnd: e
            })
        }, l.onHideTransitionEnd = function () {
            this.isHidden && (this.css({
                display: "none"
            }), this.emitEvent("hide"))
        }, l.destroy = function () {
            this.css({
                position: "",
                left: "",
                right: "",
                top: "",
                bottom: "",
                transition: "",
                transform: ""
            })
        }, i
    }),
    function (t, e) {
        "use strict";
        "function" == typeof define && define.amd ? define("outlayer/outlayer", ["ev-emitter/ev-emitter", "get-size/get-size", "fizzy-ui-utils/utils", "./item"], function (i, n, s, r) {
            return e(t, i, n, s, r)
        }) : "object" == typeof module && module.exports ? module.exports = e(t, require("ev-emitter"), require("get-size"), require("fizzy-ui-utils"), require("./item")) : t.Outlayer = e(t, t.EvEmitter, t.getSize, t.fizzyUIUtils, t.Outlayer.Item)
    }(window, function (t, e, i, n, s) {
        "use strict";

        function r(t, e) {
            var i = n.getQueryElement(t);
            if (i) {
                this.element = i, l && (this.$element = l(this.element)), this.options = n.extend({}, this.constructor.defaults), this.option(e);
                var s = ++d;
                this.element.outlayerGUID = s, c[s] = this, this._create();
                this._getOption("initLayout") && this.layout()
            } else a && a.error("Bad element for " + this.constructor.namespace + ": " + (i || t))
        }

        function o(t) {
            function e() {
                t.apply(this, arguments)
            }
            return e.prototype = Object.create(t.prototype), e.prototype.constructor = e, e
        }
        var a = t.console,
            l = t.jQuery,
            h = function () {},
            d = 0,
            c = {};
        r.namespace = "outlayer", r.Item = s, r.defaults = {
            containerStyle: {
                position: "relative"
            },
            initLayout: !0,
            originLeft: !0,
            originTop: !0,
            resize: !0,
            resizeContainer: !0,
            transitionDuration: "0.4s",
            hiddenStyle: {
                opacity: 0,
                transform: "scale(0.001)"
            },
            visibleStyle: {
                opacity: 1,
                transform: "scale(1)"
            }
        };
        var u = r.prototype;
        n.extend(u, e.prototype), u.option = function (t) {
            n.extend(this.options, t)
        }, u._getOption = function (t) {
            var e = this.constructor.compatOptions[t];
            return e && void 0 !== this.options[e] ? this.options[e] : this.options[t]
        }, r.compatOptions = {
            initLayout: "isInitLayout",
            horizontal: "isHorizontal",
            layoutInstant: "isLayoutInstant",
            originLeft: "isOriginLeft",
            originTop: "isOriginTop",
            resize: "isResizeBound",
            resizeContainer: "isResizingContainer"
        }, u._create = function () {
            this.reloadItems(), this.stamps = [], this.stamp(this.options.stamp), n.extend(this.element.style, this.options.containerStyle);
            this._getOption("resize") && this.bindResize()
        }, u.reloadItems = function () {
            this.items = this._itemize(this.element.children)
        }, u._itemize = function (t) {
            for (var e = this._filterFindItemElements(t), i = this.constructor.Item, n = [], s = 0; s < e.length; s++) {
                var r = new i(e[s], this);
                n.push(r)
            }
            return n
        }, u._filterFindItemElements = function (t) {
            return n.filterFindElements(t, this.options.itemSelector)
        }, u.getItemElements = function () {
            return this.items.map(function (t) {
                return t.element
            })
        }, u.layout = function () {
            this._resetLayout(), this._manageStamps();
            var t = this._getOption("layoutInstant"),
                e = void 0 !== t ? t : !this._isLayoutInited;
            this.layoutItems(this.items, e), this._isLayoutInited = !0
        }, u._init = u.layout, u._resetLayout = function () {
            this.getSize()
        }, u.getSize = function () {
            this.size = i(this.element)
        }, u._getMeasurement = function (t, e) {
            var n, s = this.options[t];
            s ? ("string" == typeof s ? n = this.element.querySelector(s) : s instanceof HTMLElement && (n = s), this[t] = n ? i(n)[e] : s) : this[t] = 0
        }, u.layoutItems = function (t, e) {
            t = this._getItemsForLayout(t), this._layoutItems(t, e), this._postLayout()
        }, u._getItemsForLayout = function (t) {
            return t.filter(function (t) {
                return !t.isIgnored
            })
        }, u._layoutItems = function (t, e) {
            if (this._emitCompleteOnItems("layout", t), t && t.length) {
                var i = [];
                t.forEach(function (t) {
                    var n = this._getItemLayoutPosition(t);
                    n.item = t, n.isInstant = e || t.isLayoutInstant, i.push(n)
                }, this), this._processLayoutQueue(i)
            }
        }, u._getItemLayoutPosition = function () {
            return {
                x: 0,
                y: 0
            }
        }, u._processLayoutQueue = function (t) {
            this.updateStagger(), t.forEach(function (t, e) {
                this._positionItem(t.item, t.x, t.y, t.isInstant, e)
            }, this)
        }, u.updateStagger = function () {
            var t = this.options.stagger; {
                if (null !== t && void 0 !== t) return this.stagger = function (t) {
                    if ("number" == typeof t) return t;
                    var e = t.match(/(^\d*\.?\d*)(\w*)/),
                        i = e && e[1],
                        n = e && e[2];
                    return i.length ? (i = parseFloat(i)) * (p[n] || 1) : 0
                }(t), this.stagger;
                this.stagger = 0
            }
        }, u._positionItem = function (t, e, i, n, s) {
            n ? t.goTo(e, i) : (t.stagger(s * this.stagger), t.moveTo(e, i))
        }, u._postLayout = function () {
            this.resizeContainer()
        }, u.resizeContainer = function () {
            if (this._getOption("resizeContainer")) {
                var t = this._getContainerSize();
                t && (this._setContainerMeasure(t.width, !0), this._setContainerMeasure(t.height, !1))
            }
        }, u._getContainerSize = h, u._setContainerMeasure = function (t, e) {
            if (void 0 !== t) {
                var i = this.size;
                i.isBorderBox && (t += e ? i.paddingLeft + i.paddingRight + i.borderLeftWidth + i.borderRightWidth : i.paddingBottom + i.paddingTop + i.borderTopWidth + i.borderBottomWidth), t = Math.max(t, 0), this.element.style[e ? "width" : "height"] = t + "px"
            }
        }, u._emitCompleteOnItems = function (t, e) {
            function i() {
                s.dispatchEvent(t + "Complete", null, [e])
            }

            function n() {
                ++o == r && i()
            }
            var s = this,
                r = e.length;
            if (e && r) {
                var o = 0;
                e.forEach(function (e) {
                    e.once(t, n)
                })
            } else i()
        }, u.dispatchEvent = function (t, e, i) {
            var n = e ? [e].concat(i) : i;
            if (this.emitEvent(t, n), l)
                if (this.$element = this.$element || l(this.element), e) {
                    var s = l.Event(e);
                    s.type = t, this.$element.trigger(s, i)
                } else this.$element.trigger(t, i)
        }, u.ignore = function (t) {
            var e = this.getItem(t);
            e && (e.isIgnored = !0)
        }, u.unignore = function (t) {
            var e = this.getItem(t);
            e && delete e.isIgnored
        }, u.stamp = function (t) {
            (t = this._find(t)) && (this.stamps = this.stamps.concat(t), t.forEach(this.ignore, this))
        }, u.unstamp = function (t) {
            (t = this._find(t)) && t.forEach(function (t) {
                n.removeFrom(this.stamps, t), this.unignore(t)
            }, this)
        }, u._find = function (t) {
            if (t) return "string" == typeof t && (t = this.element.querySelectorAll(t)), t = n.makeArray(t)
        }, u._manageStamps = function () {
            this.stamps && this.stamps.length && (this._getBoundingRect(), this.stamps.forEach(this._manageStamp, this))
        }, u._getBoundingRect = function () {
            var t = this.element.getBoundingClientRect(),
                e = this.size;
            this._boundingRect = {
                left: t.left + e.paddingLeft + e.borderLeftWidth,
                top: t.top + e.paddingTop + e.borderTopWidth,
                right: t.right - (e.paddingRight + e.borderRightWidth),
                bottom: t.bottom - (e.paddingBottom + e.borderBottomWidth)
            }
        }, u._manageStamp = h, u._getElementOffset = function (t) {
            var e = t.getBoundingClientRect(),
                n = this._boundingRect,
                s = i(t);
            return {
                left: e.left - n.left - s.marginLeft,
                top: e.top - n.top - s.marginTop,
                right: n.right - e.right - s.marginRight,
                bottom: n.bottom - e.bottom - s.marginBottom
            }
        }, u.handleEvent = n.handleEvent, u.bindResize = function () {
            t.addEventListener("resize", this), this.isResizeBound = !0
        }, u.unbindResize = function () {
            t.removeEventListener("resize", this), this.isResizeBound = !1
        }, u.onresize = function () {
            this.resize()
        }, n.debounceMethod(r, "onresize", 100), u.resize = function () {
            this.isResizeBound && this.needsResizeLayout() && this.layout()
        }, u.needsResizeLayout = function () {
            var t = i(this.element);
            return this.size && t && t.innerWidth !== this.size.innerWidth
        }, u.addItems = function (t) {
            var e = this._itemize(t);
            return e.length && (this.items = this.items.concat(e)), e
        }, u.appended = function (t) {
            var e = this.addItems(t);
            e.length && (this.layoutItems(e, !0), this.reveal(e))
        }, u.prepended = function (t) {
            var e = this._itemize(t);
            if (e.length) {
                var i = this.items.slice(0);
                this.items = e.concat(i), this._resetLayout(), this._manageStamps(), this.layoutItems(e, !0), this.reveal(e), this.layoutItems(i)
            }
        }, u.reveal = function (t) {
            if (this._emitCompleteOnItems("reveal", t), t && t.length) {
                var e = this.updateStagger();
                t.forEach(function (t, i) {
                    t.stagger(i * e), t.reveal()
                })
            }
        }, u.hide = function (t) {
            if (this._emitCompleteOnItems("hide", t), t && t.length) {
                var e = this.updateStagger();
                t.forEach(function (t, i) {
                    t.stagger(i * e), t.hide()
                })
            }
        }, u.revealItemElements = function (t) {
            var e = this.getItems(t);
            this.reveal(e)
        }, u.hideItemElements = function (t) {
            var e = this.getItems(t);
            this.hide(e)
        }, u.getItem = function (t) {
            for (var e = 0; e < this.items.length; e++) {
                var i = this.items[e];
                if (i.element == t) return i
            }
        }, u.getItems = function (t) {
            var e = [];
            return (t = n.makeArray(t)).forEach(function (t) {
                var i = this.getItem(t);
                i && e.push(i)
            }, this), e
        }, u.remove = function (t) {
            var e = this.getItems(t);
            this._emitCompleteOnItems("remove", e), e && e.length && e.forEach(function (t) {
                t.remove(), n.removeFrom(this.items, t)
            }, this)
        }, u.destroy = function () {
            var t = this.element.style;
            t.height = "", t.position = "", t.width = "", this.items.forEach(function (t) {
                t.destroy()
            }), this.unbindResize();
            var e = this.element.outlayerGUID;
            delete c[e], delete this.element.outlayerGUID, l && l.removeData(this.element, this.constructor.namespace)
        }, r.data = function (t) {
            var e = (t = n.getQueryElement(t)) && t.outlayerGUID;
            return e && c[e]
        }, r.create = function (t, e) {
            var i = o(r);
            return i.defaults = n.extend({}, r.defaults), n.extend(i.defaults, e), i.compatOptions = n.extend({}, r.compatOptions), i.namespace = t, i.data = r.data, i.Item = o(s), n.htmlInit(i, t), l && l.bridget && l.bridget(t, i), i
        };
        var p = {
            ms: 1,
            s: 1e3
        };
        return r.Item = s, r
    }),
    function (t, e) {
        "function" == typeof define && define.amd ? define(["outlayer/outlayer", "get-size/get-size"], e) : "object" == typeof module && module.exports ? module.exports = e(require("outlayer"), require("get-size")) : t.Masonry = e(t.Outlayer, t.getSize)
    }(window, function (t, e) {
        var i = t.create("masonry");
        i.compatOptions.fitWidth = "isFitWidth";
        var n = i.prototype;
        return n._resetLayout = function () {
            this.getSize(), this._getMeasurement("columnWidth", "outerWidth"), this._getMeasurement("gutter", "outerWidth"), this.measureColumns(), this.colYs = [];
            for (var t = 0; t < this.cols; t++) this.colYs.push(0);
            this.maxY = 0, this.horizontalColIndex = 0
        }, n.measureColumns = function () {
            if (this.getContainerWidth(), !this.columnWidth) {
                var t = this.items[0],
                    i = t && t.element;
                this.columnWidth = i && e(i).outerWidth || this.containerWidth
            }
            var n = this.columnWidth += this.gutter,
                s = this.containerWidth + this.gutter,
                r = s / n,
                o = n - s % n,
                a = o && o < 1 ? "round" : "floor";
            r = Math[a](r), this.cols = Math.max(r, 1)
        }, n.getContainerWidth = function () {
            var t = this._getOption("fitWidth") ? this.element.parentNode : this.element,
                i = e(t);
            this.containerWidth = i && i.innerWidth
        }, n._getItemLayoutPosition = function (t) {
            t.getSize();
            var e = t.size.outerWidth % this.columnWidth,
                i = e && e < 1 ? "round" : "ceil",
                n = Math[i](t.size.outerWidth / this.columnWidth);
            n = Math.min(n, this.cols);
            for (var s = this[this.options.horizontalOrder ? "_getHorizontalColPosition" : "_getTopColPosition"](n, t), r = {
                    x: this.columnWidth * s.col,
                    y: s.y
                }, o = s.y + t.size.outerHeight, a = n + s.col, l = s.col; l < a; l++) this.colYs[l] = o;
            return r
        }, n._getTopColPosition = function (t) {
            var e = this._getTopColGroup(t),
                i = Math.min.apply(Math, e);
            return {
                col: e.indexOf(i),
                y: i
            }
        }, n._getTopColGroup = function (t) {
            if (t < 2) return this.colYs;
            for (var e = [], i = this.cols + 1 - t, n = 0; n < i; n++) e[n] = this._getColGroupY(n, t);
            return e
        }, n._getColGroupY = function (t, e) {
            if (e < 2) return this.colYs[t];
            var i = this.colYs.slice(t, t + e);
            return Math.max.apply(Math, i)
        }, n._getHorizontalColPosition = function (t, e) {
            var i = this.horizontalColIndex % this.cols;
            i = t > 1 && i + t > this.cols ? 0 : i;
            var n = e.size.outerWidth && e.size.outerHeight;
            return this.horizontalColIndex = n ? i + t : this.horizontalColIndex, {
                col: i,
                y: this._getColGroupY(i, t)
            }
        }, n._manageStamp = function (t) {
            var i = e(t),
                n = this._getElementOffset(t),
                s = this._getOption("originLeft") ? n.left : n.right,
                r = s + i.outerWidth,
                o = Math.floor(s / this.columnWidth);
            o = Math.max(0, o);
            var a = Math.floor(r / this.columnWidth);
            a -= r % this.columnWidth ? 0 : 1, a = Math.min(this.cols - 1, a);
            for (var l = (this._getOption("originTop") ? n.top : n.bottom) + i.outerHeight, h = o; h <= a; h++) this.colYs[h] = Math.max(l, this.colYs[h])
        }, n._getContainerSize = function () {
            this.maxY = Math.max.apply(Math, this.colYs);
            var t = {
                height: this.maxY
            };
            return this._getOption("fitWidth") && (t.width = this._getContainerFitWidth()), t
        }, n._getContainerFitWidth = function () {
            for (var t = 0, e = this.cols; --e && 0 === this.colYs[e];) t++;
            return (this.cols - t) * this.columnWidth - this.gutter
        }, n.needsResizeLayout = function () {
            var t = this.containerWidth;
            return this.getContainerWidth(), t != this.containerWidth
        }, i
    }),
    function (t, e, i, n) {
        function s(e, i) {
            this.settings = null, this.options = t.extend({}, s.Defaults, i), this.$element = t(e), this._handlers = {}, this._plugins = {}, this._supress = {}, this._current = null, this._speed = null, this._coordinates = [], this._breakpoint = null, this._width = null, this._items = [], this._clones = [], this._mergers = [], this._widths = [], this._invalidated = {}, this._pipe = [], this._drag = {
                time: null,
                target: null,
                pointer: null,
                stage: {
                    start: null,
                    current: null
                },
                direction: null
            }, this._states = {
                current: {},
                tags: {
                    initializing: ["busy"],
                    animating: ["busy"],
                    dragging: ["interacting"]
                }
            }, t.each(["onResize", "onThrottledResize"], t.proxy(function (e, i) {
                this._handlers[i] = t.proxy(this[i], this)
            }, this)), t.each(s.Plugins, t.proxy(function (t, e) {
                this._plugins[t.charAt(0).toLowerCase() + t.slice(1)] = new e(this)
            }, this)), t.each(s.Workers, t.proxy(function (e, i) {
                this._pipe.push({
                    filter: i.filter,
                    run: t.proxy(i.run, this)
                })
            }, this)), this.setup(), this.initialize()
        }
        s.Defaults = {
            items: 3,
            loop: !1,
            center: !1,
            rewind: !1,
            mouseDrag: !0,
            touchDrag: !0,
            pullDrag: !0,
            freeDrag: !1,
            margin: 0,
            stagePadding: 0,
            merge: !1,
            mergeFit: !0,
            autoWidth: !1,
            startPosition: 0,
            rtl: !1,
            smartSpeed: 250,
            fluidSpeed: !1,
            dragEndSpeed: !1,
            responsive: {},
            responsiveRefreshRate: 200,
            responsiveBaseElement: e,
            fallbackEasing: "swing",
            info: !1,
            nestedItemSelector: !1,
            itemElement: "div",
            stageElement: "div",
            refreshClass: "owl-refresh",
            loadedClass: "owl-loaded",
            loadingClass: "owl-loading",
            rtlClass: "owl-rtl",
            responsiveClass: "owl-responsive",
            dragClass: "owl-drag",
            itemClass: "owl-item",
            stageClass: "owl-stage",
            stageOuterClass: "owl-stage-outer",
            grabClass: "owl-grab"
        }, s.Width = {
            Default: "default",
            Inner: "inner",
            Outer: "outer"
        }, s.Type = {
            Event: "event",
            State: "state"
        }, s.Plugins = {}, s.Workers = [{
            filter: ["width", "settings"],
            run: function () {
                this._width = this.$element.width()
            }
        }, {
            filter: ["width", "items", "settings"],
            run: function (t) {
                t.current = this._items && this._items[this.relative(this._current)]
            }
        }, {
            filter: ["items", "settings"],
            run: function () {
                this.$stage.children(".cloned").remove()
            }
        }, {
            filter: ["width", "items", "settings"],
            run: function (t) {
                var e = this.settings.margin || "",
                    i = !this.settings.autoWidth,
                    n = this.settings.rtl,
                    s = {
                        width: "auto",
                        "margin-left": n ? e : "",
                        "margin-right": n ? "" : e
                    };
                !i && this.$stage.children().css(s), t.css = s
            }
        }, {
            filter: ["width", "items", "settings"],
            run: function (t) {
                var e = (this.width() / this.settings.items).toFixed(3) - this.settings.margin,
                    i = null,
                    n = this._items.length,
                    s = !this.settings.autoWidth,
                    r = [];
                for (t.items = {
                        merge: !1,
                        width: e
                    }; n--;) i = this._mergers[n], i = this.settings.mergeFit && Math.min(i, this.settings.items) || i, t.items.merge = i > 1 || t.items.merge, r[n] = s ? e * i : this._items[n].width();
                this._widths = r
            }
        }, {
            filter: ["items", "settings"],
            run: function () {
                var e = [],
                    i = this._items,
                    n = this.settings,
                    s = Math.max(2 * n.items, 4),
                    r = 2 * Math.ceil(i.length / 2),
                    o = n.loop && i.length ? n.rewind ? s : Math.max(s, r) : 0,
                    a = "",
                    l = "";
                for (o /= 2; o > 0;) e.push(this.normalize(e.length / 2, !0)), a += i[e[e.length - 1]][0].outerHTML, e.push(this.normalize(i.length - 1 - (e.length - 1) / 2, !0)), l = i[e[e.length - 1]][0].outerHTML + l, o -= 1;
                this._clones = e, t(a).addClass("cloned").appendTo(this.$stage), t(l).addClass("cloned").prependTo(this.$stage)
            }
        }, {
            filter: ["width", "items", "settings"],
            run: function () {
                for (var t = this.settings.rtl ? 1 : -1, e = this._clones.length + this._items.length, i = -1, n = 0, s = 0, r = []; ++i < e;) n = r[i - 1] || 0, s = this._widths[this.relative(i)] + this.settings.margin, r.push(n + s * t);
                this._coordinates = r
            }
        }, {
            filter: ["width", "items", "settings"],
            run: function () {
                var t = this.settings.stagePadding,
                    e = this._coordinates,
                    i = {
                        width: Math.ceil(Math.abs(e[e.length - 1])) + 2 * t,
                        "padding-left": t || "",
                        "padding-right": t || ""
                    };
                this.$stage.css(i)
            }
        }, {
            filter: ["width", "items", "settings"],
            run: function (t) {
                var e = this._coordinates.length,
                    i = !this.settings.autoWidth,
                    n = this.$stage.children();
                if (i && t.items.merge)
                    for (; e--;) t.css.width = this._widths[this.relative(e)], n.eq(e).css(t.css);
                else i && (t.css.width = t.items.width, n.css(t.css))
            }
        }, {
            filter: ["items"],
            run: function () {
                this._coordinates.length < 1 && this.$stage.removeAttr("style")
            }
        }, {
            filter: ["width", "items", "settings"],
            run: function (t) {
                t.current = t.current ? this.$stage.children().index(t.current) : 0, t.current = Math.max(this.minimum(), Math.min(this.maximum(), t.current)), this.reset(t.current)
            }
        }, {
            filter: ["position"],
            run: function () {
                this.animate(this.coordinates(this._current))
            }
        }, {
            filter: ["width", "position", "items", "settings"],
            run: function () {
                var t, e, i, n, s = this.settings.rtl ? 1 : -1,
                    r = 2 * this.settings.stagePadding,
                    o = this.coordinates(this.current()) + r,
                    a = o + this.width() * s,
                    l = [];
                for (i = 0, n = this._coordinates.length; i < n; i++) t = this._coordinates[i - 1] || 0, e = Math.abs(this._coordinates[i]) + r * s, (this.op(t, "<=", o) && this.op(t, ">", a) || this.op(e, "<", o) && this.op(e, ">", a)) && l.push(i);
                this.$stage.children(".active").removeClass("active"), this.$stage.children(":eq(" + l.join("), :eq(") + ")").addClass("active"), this.$stage.children(".center").removeClass("center"), this.settings.center && this.$stage.children().eq(this.current()).addClass("center")
            }
        }], s.prototype.initialize = function () {
            if (this.enter("initializing"), this.trigger("initialize"), this.$element.toggleClass(this.settings.rtlClass, this.settings.rtl), this.settings.autoWidth && !this.is("pre-loading")) {
                var e, i, n;
                e = this.$element.find("img"), i = this.settings.nestedItemSelector ? "." + this.settings.nestedItemSelector : void 0, n = this.$element.children(i).width(), e.length && n <= 0 && this.preloadAutoWidthImages(e)
            }
            this.$element.addClass(this.options.loadingClass), this.$stage = t("<" + this.settings.stageElement + ' class="' + this.settings.stageClass + '"/>').wrap('<div class="' + this.settings.stageOuterClass + '"/>'), this.$element.append(this.$stage.parent()), this.replace(this.$element.children().not(this.$stage.parent())), this.$element.is(":visible") ? this.refresh() : this.invalidate("width"), this.$element.removeClass(this.options.loadingClass).addClass(this.options.loadedClass), this.registerEventHandlers(), this.leave("initializing"), this.trigger("initialized")
        }, s.prototype.setup = function () {
            var e = this.viewport(),
                i = this.options.responsive,
                n = -1,
                s = null;
            i ? (t.each(i, function (t) {
                t <= e && t > n && (n = Number(t))
            }), "function" == typeof (s = t.extend({}, this.options, i[n])).stagePadding && (s.stagePadding = s.stagePadding()), delete s.responsive, s.responsiveClass && this.$element.attr("class", this.$element.attr("class").replace(new RegExp("(" + this.options.responsiveClass + "-)\\S+\\s", "g"), "$1" + n))) : s = t.extend({}, this.options), this.trigger("change", {
                property: {
                    name: "settings",
                    value: s
                }
            }), this._breakpoint = n, this.settings = s, this.invalidate("settings"), this.trigger("changed", {
                property: {
                    name: "settings",
                    value: this.settings
                }
            })
        }, s.prototype.optionsLogic = function () {
            this.settings.autoWidth && (this.settings.stagePadding = !1, this.settings.merge = !1)
        }, s.prototype.prepare = function (e) {
            var i = this.trigger("prepare", {
                content: e
            });
            return i.data || (i.data = t("<" + this.settings.itemElement + "/>").addClass(this.options.itemClass).append(e)), this.trigger("prepared", {
                content: i.data
            }), i.data
        }, s.prototype.update = function () {
            for (var e = 0, i = this._pipe.length, n = t.proxy(function (t) {
                    return this[t]
                }, this._invalidated), s = {}; e < i;)(this._invalidated.all || t.grep(this._pipe[e].filter, n).length > 0) && this._pipe[e].run(s), e++;
            this._invalidated = {}, !this.is("valid") && this.enter("valid")
        }, s.prototype.width = function (t) {
            switch (t = t || s.Width.Default) {
                case s.Width.Inner:
                case s.Width.Outer:
                    return this._width;
                default:
                    return this._width - 2 * this.settings.stagePadding + this.settings.margin
            }
        }, s.prototype.refresh = function () {
            this.enter("refreshing"), this.trigger("refresh"), this.setup(), this.optionsLogic(), this.$element.addClass(this.options.refreshClass), this.update(), this.$element.removeClass(this.options.refreshClass), this.leave("refreshing"), this.trigger("refreshed")
        }, s.prototype.onThrottledResize = function () {
            e.clearTimeout(this.resizeTimer), this.resizeTimer = e.setTimeout(this._handlers.onResize, this.settings.responsiveRefreshRate)
        }, s.prototype.onResize = function () {
            return !!this._items.length && (this._width !== this.$element.width() && (!!this.$element.is(":visible") && (this.enter("resizing"), this.trigger("resize").isDefaultPrevented() ? (this.leave("resizing"), !1) : (this.invalidate("width"), this.refresh(), this.leave("resizing"), void this.trigger("resized")))))
        }, s.prototype.registerEventHandlers = function () {
            t.support.transition && this.$stage.on(t.support.transition.end + ".owl.core", t.proxy(this.onTransitionEnd, this)), !1 !== this.settings.responsive && this.on(e, "resize", this._handlers.onThrottledResize), this.settings.mouseDrag && (this.$element.addClass(this.options.dragClass), this.$stage.on("mousedown.owl.core", t.proxy(this.onDragStart, this)), this.$stage.on("dragstart.owl.core selectstart.owl.core", function () {
                return !1
            })), this.settings.touchDrag && (this.$stage.on("touchstart.owl.core", t.proxy(this.onDragStart, this)), this.$stage.on("touchcancel.owl.core", t.proxy(this.onDragEnd, this)))
        }, s.prototype.onDragStart = function (e) {
            var n = null;
            3 !== e.which && (t.support.transform ? n = {
                x: (n = this.$stage.css("transform").replace(/.*\(|\)| /g, "").split(","))[16 === n.length ? 12 : 4],
                y: n[16 === n.length ? 13 : 5]
            } : (n = this.$stage.position(), n = {
                x: this.settings.rtl ? n.left + this.$stage.width() - this.width() + this.settings.margin : n.left,
                y: n.top
            }), this.is("animating") && (t.support.transform ? this.animate(n.x) : this.$stage.stop(), this.invalidate("position")), this.$element.toggleClass(this.options.grabClass, "mousedown" === e.type), this.speed(0), this._drag.time = (new Date).getTime(), this._drag.target = t(e.target), this._drag.stage.start = n, this._drag.stage.current = n, this._drag.pointer = this.pointer(e), t(i).on("mouseup.owl.core touchend.owl.core", t.proxy(this.onDragEnd, this)), t(i).one("mousemove.owl.core touchmove.owl.core", t.proxy(function (e) {
                var n = this.difference(this._drag.pointer, this.pointer(e));
                t(i).on("mousemove.owl.core touchmove.owl.core", t.proxy(this.onDragMove, this)), Math.abs(n.x) < Math.abs(n.y) && this.is("valid") || (e.preventDefault(), this.enter("dragging"), this.trigger("drag"))
            }, this)))
        }, s.prototype.onDragMove = function (t) {
            var e = null,
                i = null,
                n = null,
                s = this.difference(this._drag.pointer, this.pointer(t)),
                r = this.difference(this._drag.stage.start, s);
            this.is("dragging") && (t.preventDefault(), this.settings.loop ? (e = this.coordinates(this.minimum()), i = this.coordinates(this.maximum() + 1) - e, r.x = ((r.x - e) % i + i) % i + e) : (e = this.settings.rtl ? this.coordinates(this.maximum()) : this.coordinates(this.minimum()), i = this.settings.rtl ? this.coordinates(this.minimum()) : this.coordinates(this.maximum()), n = this.settings.pullDrag ? -1 * s.x / 5 : 0, r.x = Math.max(Math.min(r.x, e + n), i + n)), this._drag.stage.current = r, this.animate(r.x))
        }, s.prototype.onDragEnd = function (e) {
            var n = this.difference(this._drag.pointer, this.pointer(e)),
                s = this._drag.stage.current,
                r = n.x > 0 ^ this.settings.rtl ? "left" : "right";
            t(i).off(".owl.core"), this.$element.removeClass(this.options.grabClass), (0 !== n.x && this.is("dragging") || !this.is("valid")) && (this.speed(this.settings.dragEndSpeed || this.settings.smartSpeed), this.current(this.closest(s.x, 0 !== n.x ? r : this._drag.direction)), this.invalidate("position"), this.update(), this._drag.direction = r, (Math.abs(n.x) > 3 || (new Date).getTime() - this._drag.time > 300) && this._drag.target.one("click.owl.core", function () {
                return !1
            })), this.is("dragging") && (this.leave("dragging"), this.trigger("dragged"))
        }, s.prototype.closest = function (e, i) {
            var n = -1,
                s = this.width(),
                r = this.coordinates();
            return this.settings.freeDrag || t.each(r, t.proxy(function (t, o) {
                return "left" === i && e > o - 30 && e < o + 30 ? n = t : "right" === i && e > o - s - 30 && e < o - s + 30 ? n = t + 1 : this.op(e, "<", o) && this.op(e, ">", r[t + 1] || o - s) && (n = "left" === i ? t + 1 : t), -1 === n
            }, this)), this.settings.loop || (this.op(e, ">", r[this.minimum()]) ? n = e = this.minimum() : this.op(e, "<", r[this.maximum()]) && (n = e = this.maximum())), n
        }, s.prototype.animate = function (e) {
            var i = this.speed() > 0;
            this.is("animating") && this.onTransitionEnd(), i && (this.enter("animating"), this.trigger("translate")), t.support.transform3d && t.support.transition ? this.$stage.css({
                transform: "translate3d(" + e + "px,0px,0px)",
                transition: this.speed() / 1e3 + "s"
            }) : i ? this.$stage.animate({
                left: e + "px"
            }, this.speed(), this.settings.fallbackEasing, t.proxy(this.onTransitionEnd, this)) : this.$stage.css({
                left: e + "px"
            })
        }, s.prototype.is = function (t) {
            return this._states.current[t] && this._states.current[t] > 0
        }, s.prototype.current = function (t) {
            if (void 0 === t) return this._current;
            if (0 !== this._items.length) {
                if (t = this.normalize(t), this._current !== t) {
                    var e = this.trigger("change", {
                        property: {
                            name: "position",
                            value: t
                        }
                    });
                    void 0 !== e.data && (t = this.normalize(e.data)), this._current = t, this.invalidate("position"), this.trigger("changed", {
                        property: {
                            name: "position",
                            value: this._current
                        }
                    })
                }
                return this._current
            }
        }, s.prototype.invalidate = function (e) {
            return "string" === t.type(e) && (this._invalidated[e] = !0, this.is("valid") && this.leave("valid")), t.map(this._invalidated, function (t, e) {
                return e
            })
        }, s.prototype.reset = function (t) {
            void 0 !== (t = this.normalize(t)) && (this._speed = 0, this._current = t, this.suppress(["translate", "translated"]), this.animate(this.coordinates(t)), this.release(["translate", "translated"]))
        }, s.prototype.normalize = function (t, e) {
            var i = this._items.length,
                n = e ? 0 : this._clones.length;
            return !this.isNumeric(t) || i < 1 ? t = void 0 : (t < 0 || t >= i + n) && (t = ((t - n / 2) % i + i) % i + n / 2), t
        }, s.prototype.relative = function (t) {
            return t -= this._clones.length / 2, this.normalize(t, !0)
        }, s.prototype.maximum = function (t) {
            var e, i, n, s = this.settings,
                r = this._coordinates.length;
            if (s.loop) r = this._clones.length / 2 + this._items.length - 1;
            else if (s.autoWidth || s.merge) {
                if (e = this._items.length)
                    for (i = this._items[--e].width(), n = this.$element.width(); e-- && !((i += this._items[e].width() + this.settings.margin) > n););
                r = e + 1
            } else r = s.center ? this._items.length - 1 : this._items.length - s.items;
            return t && (r -= this._clones.length / 2), Math.max(r, 0)
        }, s.prototype.minimum = function (t) {
            return t ? 0 : this._clones.length / 2
        }, s.prototype.items = function (t) {
            return void 0 === t ? this._items.slice() : (t = this.normalize(t, !0), this._items[t])
        }, s.prototype.mergers = function (t) {
            return void 0 === t ? this._mergers.slice() : (t = this.normalize(t, !0), this._mergers[t])
        }, s.prototype.clones = function (e) {
            var i = this._clones.length / 2,
                n = i + this._items.length,
                s = function (t) {
                    return t % 2 == 0 ? n + t / 2 : i - (t + 1) / 2
                };
            return void 0 === e ? t.map(this._clones, function (t, e) {
                return s(e)
            }) : t.map(this._clones, function (t, i) {
                return t === e ? s(i) : null
            })
        }, s.prototype.speed = function (t) {
            return void 0 !== t && (this._speed = t), this._speed
        }, s.prototype.coordinates = function (e) {
            var i, n = 1,
                s = e - 1;
            return void 0 === e ? t.map(this._coordinates, t.proxy(function (t, e) {
                return this.coordinates(e)
            }, this)) : (this.settings.center ? (this.settings.rtl && (n = -1, s = e + 1), i = this._coordinates[e], i += (this.width() - i + (this._coordinates[s] || 0)) / 2 * n) : i = this._coordinates[s] || 0, i = Math.ceil(i))
        }, s.prototype.duration = function (t, e, i) {
            return 0 === i ? 0 : Math.min(Math.max(Math.abs(e - t), 1), 6) * Math.abs(i || this.settings.smartSpeed)
        }, s.prototype.to = function (t, e) {
            var i = this.current(),
                n = null,
                s = t - this.relative(i),
                r = (s > 0) - (s < 0),
                o = this._items.length,
                a = this.minimum(),
                l = this.maximum();
            this.settings.loop ? (!this.settings.rewind && Math.abs(s) > o / 2 && (s += -1 * r * o), (n = (((t = i + s) - a) % o + o) % o + a) !== t && n - s <= l && n - s > 0 && (i = n - s, t = n, this.reset(i))) : t = this.settings.rewind ? (t % (l += 1) + l) % l : Math.max(a, Math.min(l, t)), this.speed(this.duration(i, t, e)), this.current(t), this.$element.is(":visible") && this.update()
        }, s.prototype.next = function (t) {
            t = t || !1, this.to(this.relative(this.current()) + 1, t)
        }, s.prototype.prev = function (t) {
            t = t || !1, this.to(this.relative(this.current()) - 1, t)
        }, s.prototype.onTransitionEnd = function (t) {
            if (void 0 !== t && (t.stopPropagation(), (t.target || t.srcElement || t.originalTarget) !== this.$stage.get(0))) return !1;
            this.leave("animating"), this.trigger("translated")
        }, s.prototype.viewport = function () {
            var n;
            return this.options.responsiveBaseElement !== e ? n = t(this.options.responsiveBaseElement).width() : e.innerWidth ? n = e.innerWidth : i.documentElement && i.documentElement.clientWidth ? n = i.documentElement.clientWidth : console.warn("Can not detect viewport width."), n
        }, s.prototype.replace = function (e) {
            this.$stage.empty(), this._items = [], e && (e = e instanceof jQuery ? e : t(e)), this.settings.nestedItemSelector && (e = e.find("." + this.settings.nestedItemSelector)), e.filter(function () {
                return 1 === this.nodeType
            }).each(t.proxy(function (t, e) {
                e = this.prepare(e), this.$stage.append(e), this._items.push(e), this._mergers.push(1 * e.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1)
            }, this)), this.reset(this.isNumeric(this.settings.startPosition) ? this.settings.startPosition : 0), this.invalidate("items")
        }, s.prototype.add = function (e, i) {
            var n = this.relative(this._current);
            i = void 0 === i ? this._items.length : this.normalize(i, !0), e = e instanceof jQuery ? e : t(e), this.trigger("add", {
                content: e,
                position: i
            }), e = this.prepare(e), 0 === this._items.length || i === this._items.length ? (0 === this._items.length && this.$stage.append(e), 0 !== this._items.length && this._items[i - 1].after(e), this._items.push(e), this._mergers.push(1 * e.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1)) : (this._items[i].before(e), this._items.splice(i, 0, e), this._mergers.splice(i, 0, 1 * e.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1)), this._items[n] && this.reset(this._items[n].index()), this.invalidate("items"), this.trigger("added", {
                content: e,
                position: i
            })
        }, s.prototype.remove = function (t) {
            void 0 !== (t = this.normalize(t, !0)) && (this.trigger("remove", {
                content: this._items[t],
                position: t
            }), this._items[t].remove(), this._items.splice(t, 1), this._mergers.splice(t, 1), this.invalidate("items"), this.trigger("removed", {
                content: null,
                position: t
            }))
        }, s.prototype.preloadAutoWidthImages = function (e) {
            e.each(t.proxy(function (e, i) {
                this.enter("pre-loading"), i = t(i), t(new Image).one("load", t.proxy(function (t) {
                    i.attr("src", t.target.src), i.css("opacity", 1), this.leave("pre-loading"), !this.is("pre-loading") && !this.is("initializing") && this.refresh()
                }, this)).attr("src", i.attr("src") || i.attr("data-src") || i.attr("data-src-retina"))
            }, this))
        }, s.prototype.destroy = function () {
            this.$element.off(".owl.core"), this.$stage.off(".owl.core"), t(i).off(".owl.core"), !1 !== this.settings.responsive && (e.clearTimeout(this.resizeTimer), this.off(e, "resize", this._handlers.onThrottledResize));
            for (var n in this._plugins) this._plugins[n].destroy();
            this.$stage.children(".cloned").remove(), this.$stage.unwrap(), this.$stage.children().contents().unwrap(), this.$stage.children().unwrap(), this.$stage.remove(), this.$element.removeClass(this.options.refreshClass).removeClass(this.options.loadingClass).removeClass(this.options.loadedClass).removeClass(this.options.rtlClass).removeClass(this.options.dragClass).removeClass(this.options.grabClass).attr("class", this.$element.attr("class").replace(new RegExp(this.options.responsiveClass + "-\\S+\\s", "g"), "")).removeData("owl.carousel")
        }, s.prototype.op = function (t, e, i) {
            var n = this.settings.rtl;
            switch (e) {
                case "<":
                    return n ? t > i : t < i;
                case ">":
                    return n ? t < i : t > i;
                case ">=":
                    return n ? t <= i : t >= i;
                case "<=":
                    return n ? t >= i : t <= i
            }
        }, s.prototype.on = function (t, e, i, n) {
            t.addEventListener ? t.addEventListener(e, i, n) : t.attachEvent && t.attachEvent("on" + e, i)
        }, s.prototype.off = function (t, e, i, n) {
            t.removeEventListener ? t.removeEventListener(e, i, n) : t.detachEvent && t.detachEvent("on" + e, i)
        }, s.prototype.trigger = function (e, i, n, r, o) {
            var a = {
                    item: {
                        count: this._items.length,
                        index: this.current()
                    }
                },
                l = t.camelCase(t.grep(["on", e, n], function (t) {
                    return t
                }).join("-").toLowerCase()),
                h = t.Event([e, "owl", n || "carousel"].join(".").toLowerCase(), t.extend({
                    relatedTarget: this
                }, a, i));
            return this._supress[e] || (t.each(this._plugins, function (t, e) {
                e.onTrigger && e.onTrigger(h)
            }), this.register({
                type: s.Type.Event,
                name: e
            }), this.$element.trigger(h), this.settings && "function" == typeof this.settings[l] && this.settings[l].call(this, h)), h
        }, s.prototype.enter = function (e) {
            t.each([e].concat(this._states.tags[e] || []), t.proxy(function (t, e) {
                void 0 === this._states.current[e] && (this._states.current[e] = 0), this._states.current[e]++
            }, this))
        }, s.prototype.leave = function (e) {
            t.each([e].concat(this._states.tags[e] || []), t.proxy(function (t, e) {
                this._states.current[e]--
            }, this))
        }, s.prototype.register = function (e) {
            if (e.type === s.Type.Event) {
                if (t.event.special[e.name] || (t.event.special[e.name] = {}), !t.event.special[e.name].owl) {
                    var i = t.event.special[e.name]._default;
                    t.event.special[e.name]._default = function (t) {
                        return !i || !i.apply || t.namespace && -1 !== t.namespace.indexOf("owl") ? t.namespace && t.namespace.indexOf("owl") > -1 : i.apply(this, arguments)
                    }, t.event.special[e.name].owl = !0
                }
            } else e.type === s.Type.State && (this._states.tags[e.name] ? this._states.tags[e.name] = this._states.tags[e.name].concat(e.tags) : this._states.tags[e.name] = e.tags, this._states.tags[e.name] = t.grep(this._states.tags[e.name], t.proxy(function (i, n) {
                return t.inArray(i, this._states.tags[e.name]) === n
            }, this)))
        }, s.prototype.suppress = function (e) {
            t.each(e, t.proxy(function (t, e) {
                this._supress[e] = !0
            }, this))
        }, s.prototype.release = function (e) {
            t.each(e, t.proxy(function (t, e) {
                delete this._supress[e]
            }, this))
        }, s.prototype.pointer = function (t) {
            var i = {
                x: null,
                y: null
            };
            return t = t.originalEvent || t || e.event, (t = t.touches && t.touches.length ? t.touches[0] : t.changedTouches && t.changedTouches.length ? t.changedTouches[0] : t).pageX ? (i.x = t.pageX, i.y = t.pageY) : (i.x = t.clientX, i.y = t.clientY), i
        }, s.prototype.isNumeric = function (t) {
            return !isNaN(parseFloat(t))
        }, s.prototype.difference = function (t, e) {
            return {
                x: t.x - e.x,
                y: t.y - e.y
            }
        }, t.fn.owlCarousel = function (e) {
            var i = Array.prototype.slice.call(arguments, 1);
            return this.each(function () {
                var n = t(this),
                    r = n.data("owl.carousel");
                r || (r = new s(this, "object" == typeof e && e), n.data("owl.carousel", r), t.each(["next", "prev", "to", "destroy", "refresh", "replace", "add", "remove"], function (e, i) {
                    r.register({
                        type: s.Type.Event,
                        name: i
                    }), r.$element.on(i + ".owl.carousel.core", t.proxy(function (t) {
                        t.namespace && t.relatedTarget !== this && (this.suppress([i]), r[i].apply(this, [].slice.call(arguments, 1)), this.release([i]))
                    }, r))
                })), "string" == typeof e && "_" !== e.charAt(0) && r[e].apply(r, i)
            })
        }, t.fn.owlCarousel.Constructor = s
    }(window.Zepto || window.jQuery, window, document),
    function (t, e, i, n) {
        var s = function (e) {
            this._core = e, this._interval = null, this._visible = null, this._handlers = {
                "initialized.owl.carousel": t.proxy(function (t) {
                    t.namespace && this._core.settings.autoRefresh && this.watch()
                }, this)
            }, this._core.options = t.extend({}, s.Defaults, this._core.options), this._core.$element.on(this._handlers)
        };
        s.Defaults = {
            autoRefresh: !0,
            autoRefreshInterval: 500
        }, s.prototype.watch = function () {
            this._interval || (this._visible = this._core.$element.is(":visible"), this._interval = e.setInterval(t.proxy(this.refresh, this), this._core.settings.autoRefreshInterval))
        }, s.prototype.refresh = function () {
            this._core.$element.is(":visible") !== this._visible && (this._visible = !this._visible, this._core.$element.toggleClass("owl-hidden", !this._visible), this._visible && this._core.invalidate("width") && this._core.refresh())
        }, s.prototype.destroy = function () {
            var t, i;
            e.clearInterval(this._interval);
            for (t in this._handlers) this._core.$element.off(t, this._handlers[t]);
            for (i in Object.getOwnPropertyNames(this)) "function" != typeof this[i] && (this[i] = null)
        }, t.fn.owlCarousel.Constructor.Plugins.AutoRefresh = s
    }(window.Zepto || window.jQuery, window, document),
    function (t, e, i, n) {
        var s = function (e) {
            this._core = e, this._loaded = [], this._handlers = {
                "initialized.owl.carousel change.owl.carousel resized.owl.carousel": t.proxy(function (e) {
                    if (e.namespace && this._core.settings && this._core.settings.lazyLoad && (e.property && "position" == e.property.name || "initialized" == e.type))
                        for (var i = this._core.settings, n = i.center && Math.ceil(i.items / 2) || i.items, s = i.center && -1 * n || 0, r = (e.property && void 0 !== e.property.value ? e.property.value : this._core.current()) + s, o = this._core.clones().length, a = t.proxy(function (t, e) {
                                this.load(e)
                            }, this); s++ < n;) this.load(o / 2 + this._core.relative(r)), o && t.each(this._core.clones(this._core.relative(r)), a), r++
                }, this)
            }, this._core.options = t.extend({}, s.Defaults, this._core.options), this._core.$element.on(this._handlers)
        };
        s.Defaults = {
            lazyLoad: !1
        }, s.prototype.load = function (i) {
            var n = this._core.$stage.children().eq(i),
                s = n && n.find(".owl-lazy");
            !s || t.inArray(n.get(0), this._loaded) > -1 || (s.each(t.proxy(function (i, n) {
                var s, r = t(n),
                    o = e.devicePixelRatio > 1 && r.attr("data-src-retina") || r.attr("data-src");
                this._core.trigger("load", {
                    element: r,
                    url: o
                }, "lazy"), r.is("img") ? r.one("load.owl.lazy", t.proxy(function () {
                    r.css("opacity", 1), this._core.trigger("loaded", {
                        element: r,
                        url: o
                    }, "lazy")
                }, this)).attr("src", o) : ((s = new Image).onload = t.proxy(function () {
                    r.css({
                        "background-image": 'url("' + o + '")',
                        opacity: "1"
                    }), this._core.trigger("loaded", {
                        element: r,
                        url: o
                    }, "lazy")
                }, this), s.src = o)
            }, this)), this._loaded.push(n.get(0)))
        }, s.prototype.destroy = function () {
            var t, e;
            for (t in this.handlers) this._core.$element.off(t, this.handlers[t]);
            for (e in Object.getOwnPropertyNames(this)) "function" != typeof this[e] && (this[e] = null)
        }, t.fn.owlCarousel.Constructor.Plugins.Lazy = s
    }(window.Zepto || window.jQuery, window, document),
    function (t, e, i, n) {
        var s = function (e) {
            this._core = e, this._handlers = {
                "initialized.owl.carousel refreshed.owl.carousel": t.proxy(function (t) {
                    t.namespace && this._core.settings.autoHeight && this.update()
                }, this),
                "changed.owl.carousel": t.proxy(function (t) {
                    t.namespace && this._core.settings.autoHeight && "position" == t.property.name && this.update()
                }, this),
                "loaded.owl.lazy": t.proxy(function (t) {
                    t.namespace && this._core.settings.autoHeight && t.element.closest("." + this._core.settings.itemClass).index() === this._core.current() && this.update()
                }, this)
            }, this._core.options = t.extend({}, s.Defaults, this._core.options), this._core.$element.on(this._handlers)
        };
        s.Defaults = {
            autoHeight: !1,
            autoHeightClass: "owl-height"
        }, s.prototype.update = function () {
            var e = this._core._current,
                i = e + this._core.settings.items,
                n = this._core.$stage.children().toArray().slice(e, i),
                s = [],
                r = 0;
            t.each(n, function (e, i) {
                s.push(t(i).height())
            }), r = Math.max.apply(null, s), this._core.$stage.parent().height(r).addClass(this._core.settings.autoHeightClass)
        }, s.prototype.destroy = function () {
            var t, e;
            for (t in this._handlers) this._core.$element.off(t, this._handlers[t]);
            for (e in Object.getOwnPropertyNames(this)) "function" != typeof this[e] && (this[e] = null)
        }, t.fn.owlCarousel.Constructor.Plugins.AutoHeight = s
    }(window.Zepto || window.jQuery, window, document),
    function (t, e, i, n) {
        var s = function (e) {
            this._core = e, this._videos = {}, this._playing = null, this._handlers = {
                "initialized.owl.carousel": t.proxy(function (t) {
                    t.namespace && this._core.register({
                        type: "state",
                        name: "playing",
                        tags: ["interacting"]
                    })
                }, this),
                "resize.owl.carousel": t.proxy(function (t) {
                    t.namespace && this._core.settings.video && this.isInFullScreen() && t.preventDefault()
                }, this),
                "refreshed.owl.carousel": t.proxy(function (t) {
                    t.namespace && this._core.is("resizing") && this._core.$stage.find(".cloned .owl-video-frame").remove()
                }, this),
                "changed.owl.carousel": t.proxy(function (t) {
                    t.namespace && "position" === t.property.name && this._playing && this.stop()
                }, this),
                "prepared.owl.carousel": t.proxy(function (e) {
                    if (e.namespace) {
                        var i = t(e.content).find(".owl-video");
                        i.length && (i.css("display", "none"), this.fetch(i, t(e.content)))
                    }
                }, this)
            }, this._core.options = t.extend({}, s.Defaults, this._core.options), this._core.$element.on(this._handlers), this._core.$element.on("click.owl.video", ".owl-video-play-icon", t.proxy(function (t) {
                this.play(t)
            }, this))
        };
        s.Defaults = {
            video: !1,
            videoHeight: !1,
            videoWidth: !1
        }, s.prototype.fetch = function (t, e) {
            var i = t.attr("data-vimeo-id") ? "vimeo" : t.attr("data-vzaar-id") ? "vzaar" : "youtube",
                n = t.attr("data-vimeo-id") || t.attr("data-youtube-id") || t.attr("data-vzaar-id"),
                s = t.attr("data-width") || this._core.settings.videoWidth,
                r = t.attr("data-height") || this._core.settings.videoHeight,
                o = t.attr("href");
            if (!o) throw new Error("Missing video URL.");
            if ((n = o.match(/(http:|https:|)\/\/(player.|www.|app.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com)|vzaar\.com)\/(video\/|videos\/|embed\/|channels\/.+\/|groups\/.+\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/))[3].indexOf("youtu") > -1) i = "youtube";
            else if (n[3].indexOf("vimeo") > -1) i = "vimeo";
            else {
                if (!(n[3].indexOf("vzaar") > -1)) throw new Error("Video URL not supported.");
                i = "vzaar"
            }
            n = n[6], this._videos[o] = {
                type: i,
                id: n,
                width: s,
                height: r
            }, e.attr("data-video", o), this.thumbnail(t, this._videos[o])
        }, s.prototype.thumbnail = function (e, i) {
            var n, s, r, o = i.width && i.height ? 'style="width:' + i.width + "px;height:" + i.height + 'px;"' : "",
                a = e.find("img"),
                l = "src",
                h = "",
                d = this._core.settings,
                c = function (t) {
                    s = '<div class="owl-video-play-icon"></div>', n = d.lazyLoad ? '<div class="owl-video-tn ' + h + '" ' + l + '="' + t + '"></div>' : '<div class="owl-video-tn" style="opacity:1;background-image:url(' + t + ')"></div>', e.after(n), e.after(s)
                };
            if (e.wrap('<div class="owl-video-wrapper"' + o + "></div>"), this._core.settings.lazyLoad && (l = "data-src", h = "owl-lazy"), a.length) return c(a.attr(l)), a.remove(), !1;
            "youtube" === i.type ? (r = "//img.youtube.com/vi/" + i.id + "/hqdefault.jpg", c(r)) : "vimeo" === i.type ? t.ajax({
                type: "GET",
                url: "//vimeo.com/api/v2/video/" + i.id + ".json",
                jsonp: "callback",
                dataType: "jsonp",
                success: function (t) {
                    r = t[0].thumbnail_large, c(r)
                }
            }) : "vzaar" === i.type && t.ajax({
                type: "GET",
                url: "//vzaar.com/api/videos/" + i.id + ".json",
                jsonp: "callback",
                dataType: "jsonp",
                success: function (t) {
                    r = t.framegrab_url, c(r)
                }
            })
        }, s.prototype.stop = function () {
            this._core.trigger("stop", null, "video"), this._playing.find(".owl-video-frame").remove(), this._playing.removeClass("owl-video-playing"), this._playing = null, this._core.leave("playing"), this._core.trigger("stopped", null, "video")
        }, s.prototype.play = function (e) {
            var i, n = t(e.target).closest("." + this._core.settings.itemClass),
                s = this._videos[n.attr("data-video")],
                r = s.width || "100%",
                o = s.height || this._core.$stage.height();
            this._playing || (this._core.enter("playing"), this._core.trigger("play", null, "video"), n = this._core.items(this._core.relative(n.index())), this._core.reset(n.index()), "youtube" === s.type ? i = '<iframe width="' + r + '" height="' + o + '" src="//www.youtube.com/embed/' + s.id + "?autoplay=1&rel=0&v=" + s.id + '" frameborder="0" allowfullscreen></iframe>' : "vimeo" === s.type ? i = '<iframe src="//player.vimeo.com/video/' + s.id + '?autoplay=1" width="' + r + '" height="' + o + '" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>' : "vzaar" === s.type && (i = '<iframe frameborder="0"height="' + o + '"width="' + r + '" allowfullscreen mozallowfullscreen webkitAllowFullScreen src="//view.vzaar.com/' + s.id + '/player?autoplay=true"></iframe>'), t('<div class="owl-video-frame">' + i + "</div>").insertAfter(n.find(".owl-video")), this._playing = n.addClass("owl-video-playing"))
        }, s.prototype.isInFullScreen = function () {
            var e = i.fullscreenElement || i.mozFullScreenElement || i.webkitFullscreenElement;
            return e && t(e).parent().hasClass("owl-video-frame")
        }, s.prototype.destroy = function () {
            var t, e;
            this._core.$element.off("click.owl.video");
            for (t in this._handlers) this._core.$element.off(t, this._handlers[t]);
            for (e in Object.getOwnPropertyNames(this)) "function" != typeof this[e] && (this[e] = null)
        }, t.fn.owlCarousel.Constructor.Plugins.Video = s
    }(window.Zepto || window.jQuery, window, document),
    function (t, e, i, n) {
        var s = function (e) {
            this.core = e, this.core.options = t.extend({}, s.Defaults, this.core.options), this.swapping = !0, this.previous = void 0, this.next = void 0, this.handlers = {
                "change.owl.carousel": t.proxy(function (t) {
                    t.namespace && "position" == t.property.name && (this.previous = this.core.current(), this.next = t.property.value)
                }, this),
                "drag.owl.carousel dragged.owl.carousel translated.owl.carousel": t.proxy(function (t) {
                    t.namespace && (this.swapping = "translated" == t.type)
                }, this),
                "translate.owl.carousel": t.proxy(function (t) {
                    t.namespace && this.swapping && (this.core.options.animateOut || this.core.options.animateIn) && this.swap()
                }, this)
            }, this.core.$element.on(this.handlers)
        };
        s.Defaults = {
            animateOut: !1,
            animateIn: !1
        }, s.prototype.swap = function () {
            if (1 === this.core.settings.items && t.support.animation && t.support.transition) {
                this.core.speed(0);
                var e, i = t.proxy(this.clear, this),
                    n = this.core.$stage.children().eq(this.previous),
                    s = this.core.$stage.children().eq(this.next),
                    r = this.core.settings.animateIn,
                    o = this.core.settings.animateOut;
                this.core.current() !== this.previous && (o && (e = this.core.coordinates(this.previous) - this.core.coordinates(this.next), n.one(t.support.animation.end, i).css({
                    left: e + "px"
                }).addClass("animated owl-animated-out").addClass(o)), r && s.one(t.support.animation.end, i).addClass("animated owl-animated-in").addClass(r))
            }
        }, s.prototype.clear = function (e) {
            t(e.target).css({
                left: ""
            }).removeClass("animated owl-animated-out owl-animated-in").removeClass(this.core.settings.animateIn).removeClass(this.core.settings.animateOut), this.core.onTransitionEnd()
        }, s.prototype.destroy = function () {
            var t, e;
            for (t in this.handlers) this.core.$element.off(t, this.handlers[t]);
            for (e in Object.getOwnPropertyNames(this)) "function" != typeof this[e] && (this[e] = null)
        }, t.fn.owlCarousel.Constructor.Plugins.Animate = s
    }(window.Zepto || window.jQuery, window, document),
    function (t, e, i, n) {
        var s = function (e) {
            this._core = e, this._call = null, this._time = 0, this._timeout = 0, this._paused = !0, this._handlers = {
                "changed.owl.carousel": t.proxy(function (t) {
                    t.namespace && "settings" === t.property.name ? this._core.settings.autoplay ? this.play() : this.stop() : t.namespace && "position" === t.property.name && this._paused && (this._time = 0)
                }, this),
                "initialized.owl.carousel": t.proxy(function (t) {
                    t.namespace && this._core.settings.autoplay && this.play()
                }, this),
                "play.owl.autoplay": t.proxy(function (t, e, i) {
                    t.namespace && this.play(e, i)
                }, this),
                "stop.owl.autoplay": t.proxy(function (t) {
                    t.namespace && this.stop()
                }, this),
                "mouseover.owl.autoplay": t.proxy(function () {
                    this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.pause()
                }, this),
                "mouseleave.owl.autoplay": t.proxy(function () {
                    this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.play()
                }, this),
                "touchstart.owl.core": t.proxy(function () {
                    this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.pause()
                }, this),
                "touchend.owl.core": t.proxy(function () {
                    this._core.settings.autoplayHoverPause && this.play()
                }, this)
            }, this._core.$element.on(this._handlers), this._core.options = t.extend({}, s.Defaults, this._core.options)
        };
        s.Defaults = {
            autoplay: !1,
            autoplayTimeout: 5e3,
            autoplayHoverPause: !1,
            autoplaySpeed: !1
        }, s.prototype._next = function (n) {
            this._call = e.setTimeout(t.proxy(this._next, this, n), this._timeout * (Math.round(this.read() / this._timeout) + 1) - this.read()), this._core.is("busy") || this._core.is("interacting") || i.hidden || this._core.next(n || this._core.settings.autoplaySpeed)
        }, s.prototype.read = function () {
            return (new Date).getTime() - this._time
        }, s.prototype.play = function (i, n) {
            var s;
            this._core.is("rotating") || this._core.enter("rotating"), i = i || this._core.settings.autoplayTimeout, s = Math.min(this._time % (this._timeout || i), i), this._paused ? (this._time = this.read(), this._paused = !1) : e.clearTimeout(this._call), this._time += this.read() % i - s, this._timeout = i, this._call = e.setTimeout(t.proxy(this._next, this, n), i - s)
        }, s.prototype.stop = function () {
            this._core.is("rotating") && (this._time = 0, this._paused = !0, e.clearTimeout(this._call), this._core.leave("rotating"))
        }, s.prototype.pause = function () {
            this._core.is("rotating") && !this._paused && (this._time = this.read(), this._paused = !0, e.clearTimeout(this._call))
        }, s.prototype.destroy = function () {
            var t, e;
            this.stop();
            for (t in this._handlers) this._core.$element.off(t, this._handlers[t]);
            for (e in Object.getOwnPropertyNames(this)) "function" != typeof this[e] && (this[e] = null)
        }, t.fn.owlCarousel.Constructor.Plugins.autoplay = s
    }(window.Zepto || window.jQuery, window, document),
    function (t, e, i, n) {
        "use strict";
        var s = function (e) {
            this._core = e, this._initialized = !1, this._pages = [], this._controls = {}, this._templates = [], this.$element = this._core.$element, this._overrides = {
                next: this._core.next,
                prev: this._core.prev,
                to: this._core.to
            }, this._handlers = {
                "prepared.owl.carousel": t.proxy(function (e) {
                    e.namespace && this._core.settings.dotsData && this._templates.push('<div class="' + this._core.settings.dotClass + '">' + t(e.content).find("[data-dot]").addBack("[data-dot]").attr("data-dot") + "</div>")
                }, this),
                "added.owl.carousel": t.proxy(function (t) {
                    t.namespace && this._core.settings.dotsData && this._templates.splice(t.position, 0, this._templates.pop())
                }, this),
                "remove.owl.carousel": t.proxy(function (t) {
                    t.namespace && this._core.settings.dotsData && this._templates.splice(t.position, 1)
                }, this),
                "changed.owl.carousel": t.proxy(function (t) {
                    t.namespace && "position" == t.property.name && this.draw()
                }, this),
                "initialized.owl.carousel": t.proxy(function (t) {
                    t.namespace && !this._initialized && (this._core.trigger("initialize", null, "navigation"), this.initialize(), this.update(), this.draw(), this._initialized = !0, this._core.trigger("initialized", null, "navigation"))
                }, this),
                "refreshed.owl.carousel": t.proxy(function (t) {
                    t.namespace && this._initialized && (this._core.trigger("refresh", null, "navigation"), this.update(), this.draw(), this._core.trigger("refreshed", null, "navigation"))
                }, this)
            }, this._core.options = t.extend({}, s.Defaults, this._core.options), this.$element.on(this._handlers)
        };
        s.Defaults = {
            nav: !1,
            navText: ['<span aria-label="prev">&#x2039;</span>', '<span aria-label="next">&#x203a;</span>'],
            navSpeed: !1,
            navElement: 'button role="presentation"',
            navContainer: !1,
            navContainerClass: "owl-nav",
            navClass: ["owl-prev", "owl-next"],
            slideBy: 1,
            dotClass: "owl-dot",
            dotsClass: "owl-dots",
            dots: !0,
            dotsEach: !1,
            dotsData: !1,
            dotsSpeed: !1,
            dotsContainer: !1
        }, s.prototype.initialize = function () {
            var e, i = this._core.settings;
            this._controls.$relative = (i.navContainer ? t(i.navContainer) : t("<div>").addClass(i.navContainerClass).appendTo(this.$element)).addClass("disabled"), this._controls.$previous = t("<" + i.navElement + ">").addClass(i.navClass[0]).html(i.navText[0]).prependTo(this._controls.$relative).on("click", t.proxy(function (t) {
                this.prev(i.navSpeed)
            }, this)), this._controls.$next = t("<" + i.navElement + ">").addClass(i.navClass[1]).html(i.navText[1]).appendTo(this._controls.$relative).on("click", t.proxy(function (t) {
                this.next(i.navSpeed)
            }, this)), i.dotsData || (this._templates = [t("<button>").addClass(i.dotClass).append(t("<span>")).prop("outerHTML")]), this._controls.$absolute = (i.dotsContainer ? t(i.dotsContainer) : t("<div>").addClass(i.dotsClass).appendTo(this.$element)).addClass("disabled"), this._controls.$absolute.on("click", "button", t.proxy(function (e) {
                var n = t(e.target).parent().is(this._controls.$absolute) ? t(e.target).index() : t(e.target).parent().index();
                e.preventDefault(), this.to(n, i.dotsSpeed)
            }, this));
            for (e in this._overrides) this._core[e] = t.proxy(this[e], this)
        }, s.prototype.destroy = function () {
            var t, e, i, n;
            for (t in this._handlers) this.$element.off(t, this._handlers[t]);
            for (e in this._controls) "$relative" === e && settings.navContainer ? this._controls[e].html("") : this._controls[e].remove();
            for (n in this.overides) this._core[n] = this._overrides[n];
            for (i in Object.getOwnPropertyNames(this)) "function" != typeof this[i] && (this[i] = null)
        }, s.prototype.update = function () {
            var t, e, i = this._core.clones().length / 2,
                n = i + this._core.items().length,
                s = this._core.maximum(!0),
                r = this._core.settings,
                o = r.center || r.autoWidth || r.dotsData ? 1 : r.dotsEach || r.items;
            if ("page" !== r.slideBy && (r.slideBy = Math.min(r.slideBy, r.items)), r.dots || "page" == r.slideBy)
                for (this._pages = [], t = i, e = 0, 0; t < n; t++) {
                    if (e >= o || 0 === e) {
                        if (this._pages.push({
                                start: Math.min(s, t - i),
                                end: t - i + o - 1
                            }), Math.min(s, t - i) === s) break;
                        e = 0, 0
                    }
                    e += this._core.mergers(this._core.relative(t))
                }
        }, s.prototype.draw = function () {
            var e, i = this._core.settings,
                n = this._core.items().length <= i.items,
                s = this._core.relative(this._core.current()),
                r = i.loop || i.rewind;
            this._controls.$relative.toggleClass("disabled", !i.nav || n), i.nav && (this._controls.$previous.toggleClass("disabled", !r && s <= this._core.minimum(!0)), this._controls.$next.toggleClass("disabled", !r && s >= this._core.maximum(!0))), this._controls.$absolute.toggleClass("disabled", !i.dots || n), i.dots && (e = this._pages.length - this._controls.$absolute.children().length, i.dotsData && 0 !== e ? this._controls.$absolute.html(this._templates.join("")) : e > 0 ? this._controls.$absolute.append(new Array(e + 1).join(this._templates[0])) : e < 0 && this._controls.$absolute.children().slice(e).remove(), this._controls.$absolute.find(".active").removeClass("active"), this._controls.$absolute.children().eq(t.inArray(this.current(), this._pages)).addClass("active"))
        }, s.prototype.onTrigger = function (e) {
            var i = this._core.settings;
            e.page = {
                index: t.inArray(this.current(), this._pages),
                count: this._pages.length,
                size: i && (i.center || i.autoWidth || i.dotsData ? 1 : i.dotsEach || i.items)
            }
        }, s.prototype.current = function () {
            var e = this._core.relative(this._core.current());
            return t.grep(this._pages, t.proxy(function (t, i) {
                return t.start <= e && t.end >= e
            }, this)).pop()
        }, s.prototype.getPosition = function (e) {
            var i, n, s = this._core.settings;
            return "page" == s.slideBy ? (i = t.inArray(this.current(), this._pages), n = this._pages.length, e ? ++i : --i, i = this._pages[(i % n + n) % n].start) : (i = this._core.relative(this._core.current()), n = this._core.items().length, e ? i += s.slideBy : i -= s.slideBy), i
        }, s.prototype.next = function (e) {
            t.proxy(this._overrides.to, this._core)(this.getPosition(!0), e)
        }, s.prototype.prev = function (e) {
            t.proxy(this._overrides.to, this._core)(this.getPosition(!1), e)
        }, s.prototype.to = function (e, i, n) {
            var s;
            !n && this._pages.length ? (s = this._pages.length, t.proxy(this._overrides.to, this._core)(this._pages[(e % s + s) % s].start, i)) : t.proxy(this._overrides.to, this._core)(e, i)
        }, t.fn.owlCarousel.Constructor.Plugins.Navigation = s
    }(window.Zepto || window.jQuery, window, document),
    function (t, e, i, n) {
        "use strict";
        var s = function (i) {
            this._core = i, this._hashes = {}, this.$element = this._core.$element, this._handlers = {
                "initialized.owl.carousel": t.proxy(function (i) {
                    i.namespace && "URLHash" === this._core.settings.startPosition && t(e).trigger("hashchange.owl.navigation")
                }, this),
                "prepared.owl.carousel": t.proxy(function (e) {
                    if (e.namespace) {
                        var i = t(e.content).find("[data-hash]").addBack("[data-hash]").attr("data-hash");
                        if (!i) return;
                        this._hashes[i] = e.content
                    }
                }, this),
                "changed.owl.carousel": t.proxy(function (i) {
                    if (i.namespace && "position" === i.property.name) {
                        var n = this._core.items(this._core.relative(this._core.current())),
                            s = t.map(this._hashes, function (t, e) {
                                return t === n ? e : null
                            }).join();
                        if (!s || e.location.hash.slice(1) === s) return;
                        e.location.hash = s
                    }
                }, this)
            }, this._core.options = t.extend({}, s.Defaults, this._core.options), this.$element.on(this._handlers), t(e).on("hashchange.owl.navigation", t.proxy(function (t) {
                var i = e.location.hash.substring(1),
                    n = this._core.$stage.children(),
                    s = this._hashes[i] && n.index(this._hashes[i]);
                void 0 !== s && s !== this._core.current() && this._core.to(this._core.relative(s), !1, !0)
            }, this))
        };
        s.Defaults = {
            URLhashListener: !1
        }, s.prototype.destroy = function () {
            var i, n;
            t(e).off("hashchange.owl.navigation");
            for (i in this._handlers) this._core.$element.off(i, this._handlers[i]);
            for (n in Object.getOwnPropertyNames(this)) "function" != typeof this[n] && (this[n] = null)
        }, t.fn.owlCarousel.Constructor.Plugins.Hash = s
    }(window.Zepto || window.jQuery, window, document),
    function (t, e, i, n) {
        function s(e, i) {
            var s = !1,
                r = e.charAt(0).toUpperCase() + e.slice(1);
            return t.each((e + " " + a.join(r + " ") + r).split(" "), function (t, e) {
                if (o[e] !== n) return s = !i || e, !1
            }), s
        }

        function r(t) {
            return s(t, !0)
        }
        var o = t("<support>").get(0).style,
            a = "Webkit Moz O ms".split(" "),
            l = {
                transition: {
                    end: {
                        WebkitTransition: "webkitTransitionEnd",
                        MozTransition: "transitionend",
                        OTransition: "oTransitionEnd",
                        transition: "transitionend"
                    }
                },
                animation: {
                    end: {
                        WebkitAnimation: "webkitAnimationEnd",
                        MozAnimation: "animationend",
                        OAnimation: "oAnimationEnd",
                        animation: "animationend"
                    }
                }
            },
            h = {
                csstransforms: function () {
                    return !!s("transform")
                },
                csstransforms3d: function () {
                    return !!s("perspective")
                },
                csstransitions: function () {
                    return !!s("transition")
                },
                cssanimations: function () {
                    return !!s("animation")
                }
            };
        h.csstransitions() && (t.support.transition = new String(r("transition")), t.support.transition.end = l.transition.end[t.support.transition]), h.cssanimations() && (t.support.animation = new String(r("animation")), t.support.animation.end = l.animation.end[t.support.animation]), h.csstransforms() && (t.support.transform = new String(r("transform")), t.support.transform3d = h.csstransforms3d())
    }(window.Zepto || window.jQuery, window, document),
    function (t) {
        t.fn.stellarNav = function (e, i, n) {
            nav = t(this), i = t(window).width();
            var s = t.extend({
                theme: "plain",
                breakpoint: 768,
                phoneBtn: !1,
                locationBtn: !1,
                sticky: !1,
                position: "static",
                showArrows: !0,
                closeBtn: !1,
                scrollbarFix: !1
            }, e);
            return this.each(function () {
                function e() {
                    window.innerWidth <= n ? (d(), nav.addClass("mobile"), nav.removeClass("desktop"), !nav.hasClass("active") && nav.find("ul:first").is(":visible") && nav.find("ul:first").hide()) : (nav.addClass("desktop"), nav.removeClass("mobile"), nav.hasClass("active") && nav.removeClass("active"), !nav.hasClass("active") && nav.find("ul:first").is(":hidden") && nav.find("ul:first").show(), t("li.open").removeClass("open").find("ul:visible").hide(), d(), c())
                }
                if ("light" != s.theme && "dark" != s.theme || nav.addClass(s.theme), s.breakpoint && (n = s.breakpoint), s.phoneBtn && s.locationBtn) r = "third";
                else if (s.phoneBtn || s.locationBtn) r = "half";
                else var r = "full";
                if (nav.prepend('<a href="#" class="menu-toggle ' + r + '"><i class="fa fa-bars"></i> Menu</a>'), s.phoneBtn && "right" != s.position && "left" != s.position) {
                    o = '<a href="tel:' + s.phoneBtn + '" class="call-btn-mobile ' + r + '"><i class="fa fa-phone"></i> <span>Call us</span></a>';
                    nav.find("a.menu-toggle").after(o)
                }
                if (s.locationBtn && "right" != s.position && "left" != s.position) {
                    var o = '<a href="' + s.locationBtn + '" class="location-btn-mobile ' + r + '" target="_blank"><i class="fa fa-map-marker"></i> <span>Location</span></a>';
                    nav.find("a.menu-toggle").after(o)
                }
                if (s.sticky && (navPos = nav.offset().top, i >= n && t(window).bind("scroll", function () {
                        t(window).scrollTop() > navPos ? nav.addClass("fixed") : nav.removeClass("fixed")
                    })), "top" == s.position && nav.addClass("top"), "left" == s.position || "right" == s.position) {
                    var a = '<a href="#" class="close-menu ' + r + '"><i class="fa fa-close"></i> <span>Close</span></a>',
                        l = '<a href="tel:' + s.phoneBtn + '" class="call-btn-mobile ' + r + '"><i class="fa fa-phone"></i></a>',
                        h = '<a href="' + s.locationBtn + '" class="location-btn-mobile ' + r + '" target="_blank"><i class="fa fa-map-marker"></i></a>';
                    nav.find("ul:first").prepend(a), s.locationBtn && nav.find("ul:first").prepend(h), s.phoneBtn && nav.find("ul:first").prepend(l)
                }
                "right" == s.position && nav.addClass("right"), "left" == s.position && nav.addClass("left"), s.showArrows || nav.addClass("hide-arrows"), s.closeBtn && "right" != s.position && "left" != s.position && nav.find("ul:first").append('<li><a href="#" class="close-menu"><i class="fa fa-close"></i> Close Menu</a></li>'), s.scrollbarFix && t("body").addClass("stellarnav-noscroll-x"), t(".menu-toggle").on("click", function (e) {
                    e.stopPropagation(), "left" == s.position || "right" == s.position ? (nav.find("ul:first").stop(!0, !0).fadeToggle(250), nav.toggleClass("active"), nav.hasClass("active") && nav.hasClass("mobile") && t(document).on("click", function (e) {
                        nav.hasClass("mobile") && (t(e.target).closest(nav).length || (nav.find("ul:first").stop(!0, !0).fadeOut(250), nav.removeClass("active")))
                    })) : (nav.find("ul:first").stop(!0, !0).slideToggle(250), nav.toggleClass("active"))
                }), t(".close-menu").click(function () {
                    nav.removeClass("active"), "left" == s.position || "right" == s.position ? nav.find("ul:first").stop(!0, !0).fadeToggle(250) : nav.find("ul:first").stop(!0, !0).slideUp(250).toggleClass("active")
                }), nav.find("li a").each(function () {
                    t(this).next().length > 0 && t(this).parent("li").addClass("has-sub").append('<a class="dd-toggle" href="#"><i class="fa fa-plus"></i></a>')
                }), nav.find("li .dd-toggle").on("click", function (e) {
                    e.preventDefault(), t(this).parent("li").children("ul").stop(!0, !0).slideToggle(250), t(this).parent("li").toggleClass("open")
                });
                var d = function () {
                        nav.find("li").unbind("mouseenter"), nav.find("li").unbind("mouseleave")
                    },
                    c = function () {
                        nav.find("li").on("mouseenter", function () {
                            t(this).addClass("hover"), t(this).children("ul").stop(!0, !0).slideDown(250)
                        }), nav.find("li").on("mouseleave", function () {
                            t(this).removeClass("hover"), t(this).children("ul").stop(!0, !0).slideUp(250)
                        })
                    };
                e(), t(window).on("resize", function () {
                    e()
                })
            })
        }
    }(jQuery),
    function (t) {
        "function" == typeof define && define.amd ? define(["jquery"], t) : "object" == typeof module && module.exports ? module.exports = t(require("jquery")) : t(jQuery)
    }(function (t) {
        var e = Array.prototype.slice,
            i = Array.prototype.splice,
            n = {
                topSpacing: 0,
                bottomSpacing: 0,
                className: "is-sticky",
                wrapperClassName: "sticky-wrapper",
                center: !1,
                getWidthFrom: "",
                widthFromWrapper: !0,
                responsiveWidth: !1,
                zIndex: "inherit"
            },
            s = t(window),
            r = t(document),
            o = [],
            a = s.height(),
            l = function () {
                for (var e = s.scrollTop(), i = r.height(), n = i - a, l = e > n ? n - e : 0, h = 0, d = o.length; h < d; h++) {
                    var c = o[h],
                        u = c.stickyWrapper.offset().top - c.topSpacing - l;
                    if (c.stickyWrapper.css("height", c.stickyElement.outerHeight()), e <= u) null !== c.currentTop && (c.stickyElement.css({
                        width: "",
                        position: "",
                        top: "",
                        "z-index": ""
                    }), c.stickyElement.parent().removeClass(c.className), c.stickyElement.trigger("sticky-end", [c]), c.currentTop = null);
                    else {
                        var p = i - c.stickyElement.outerHeight() - c.topSpacing - c.bottomSpacing - e - l;
                        if (p < 0 ? p += c.topSpacing : p = c.topSpacing, c.currentTop !== p) {
                            var f;
                            c.getWidthFrom ? (padding = c.stickyElement.innerWidth() - c.stickyElement.width(), f = t(c.getWidthFrom).width() - padding || null) : c.widthFromWrapper && (f = c.stickyWrapper.width()), null == f && (f = c.stickyElement.width()), c.stickyElement.css("width", f).css("position", "fixed").css("top", p).css("z-index", c.zIndex), c.stickyElement.parent().addClass(c.className), null === c.currentTop ? c.stickyElement.trigger("sticky-start", [c]) : c.stickyElement.trigger("sticky-update", [c]), c.currentTop === c.topSpacing && c.currentTop > p || null === c.currentTop && p < c.topSpacing ? c.stickyElement.trigger("sticky-bottom-reached", [c]) : null !== c.currentTop && p === c.topSpacing && c.currentTop < p && c.stickyElement.trigger("sticky-bottom-unreached", [c]), c.currentTop = p
                        }
                        var m = c.stickyWrapper.parent();
                        c.stickyElement.offset().top + c.stickyElement.outerHeight() >= m.offset().top + m.outerHeight() && c.stickyElement.offset().top <= c.topSpacing ? c.stickyElement.css("position", "absolute").css("top", "").css("bottom", 0).css("z-index", "") : c.stickyElement.css("position", "fixed").css("top", p).css("bottom", "").css("z-index", c.zIndex)
                    }
                }
            },
            h = function () {
                a = s.height();
                for (var e = 0, i = o.length; e < i; e++) {
                    var n = o[e],
                        r = null;
                    n.getWidthFrom ? n.responsiveWidth && (r = t(n.getWidthFrom).width()) : n.widthFromWrapper && (r = n.stickyWrapper.width()), null != r && n.stickyElement.css("width", r)
                }
            },
            d = {
                init: function (e) {
                    return this.each(function () {
                        var i = t.extend({}, n, e),
                            s = t(this),
                            r = s.attr("id"),
                            a = r ? r + "-" + n.wrapperClassName : n.wrapperClassName,
                            l = t("<div></div>").attr("id", a).addClass(i.wrapperClassName);
                        s.wrapAll(function () {
                            if (0 == t(this).parent("#" + a).length) return l
                        });
                        var h = s.parent();
                        i.center && h.css({
                            width: s.outerWidth(),
                            marginLeft: "auto",
                            marginRight: "auto"
                        }), "right" === s.css("float") && s.css({
                            float: "none"
                        }).parent().css({
                            float: "right"
                        }), i.stickyElement = s, i.stickyWrapper = h, i.currentTop = null, o.push(i), d.setWrapperHeight(this), d.setupChangeListeners(this)
                    })
                },
                setWrapperHeight: function (e) {
                    var i = t(e),
                        n = i.parent();
                    n && n.css("height", i.outerHeight())
                },
                setupChangeListeners: function (t) {
                    if (window.MutationObserver) {
                        new window.MutationObserver(function (e) {
                            (e[0].addedNodes.length || e[0].removedNodes.length) && d.setWrapperHeight(t)
                        }).observe(t, {
                            subtree: !0,
                            childList: !0
                        })
                    } else window.addEventListener ? (t.addEventListener("DOMNodeInserted", function () {
                        d.setWrapperHeight(t)
                    }, !1), t.addEventListener("DOMNodeRemoved", function () {
                        d.setWrapperHeight(t)
                    }, !1)) : window.attachEvent && (t.attachEvent("onDOMNodeInserted", function () {
                        d.setWrapperHeight(t)
                    }), t.attachEvent("onDOMNodeRemoved", function () {
                        d.setWrapperHeight(t)
                    }))
                },
                update: l,
                unstick: function (e) {
                    return this.each(function () {
                        for (var e = t(this), n = -1, s = o.length; s-- > 0;) o[s].stickyElement.get(0) === this && (i.call(o, s, 1), n = s); - 1 !== n && (e.unwrap(), e.css({
                            width: "",
                            position: "",
                            top: "",
                            float: "",
                            "z-index": ""
                        }))
                    })
                }
            };
        window.addEventListener ? (window.addEventListener("scroll", l, !1), window.addEventListener("resize", h, !1)) : window.attachEvent && (window.attachEvent("onscroll", l), window.attachEvent("onresize", h)), t.fn.sticky = function (i) {
            return d[i] ? d[i].apply(this, e.call(arguments, 1)) : "object" != typeof i && i ? void t.error("Method " + i + " does not exist on jQuery.sticky") : d.init.apply(this, arguments)
        }, t.fn.unstick = function (i) {
            return d[i] ? d[i].apply(this, e.call(arguments, 1)) : "object" != typeof i && i ? void t.error("Method " + i + " does not exist on jQuery.sticky") : d.unstick.apply(this, arguments)
        }, t(function () {
            setTimeout(l, 0)
        })
    }),
    function (t, e) {
        "object" == typeof exports && "undefined" != typeof module ? module.exports = e() : "function" == typeof define && define.amd ? define(e) : t.Swiper = e()
    }(this, function () {
        "use strict";

        function t(t, e) {
            var i = [],
                n = 0;
            if (t && !e && t instanceof s) return t;
            if (t)
                if ("string" == typeof t) {
                    var r, o, a = t.trim();
                    if (a.indexOf("<") >= 0 && a.indexOf(">") >= 0) {
                        var l = "div";
                        for (0 === a.indexOf("<li") && (l = "ul"), 0 === a.indexOf("<tr") && (l = "tbody"), 0 !== a.indexOf("<td") && 0 !== a.indexOf("<th") || (l = "tr"), 0 === a.indexOf("<tbody") && (l = "table"), 0 === a.indexOf("<option") && (l = "select"), (o = document.createElement(l)).innerHTML = a, n = 0; n < o.childNodes.length; n += 1) i.push(o.childNodes[n])
                    } else
                        for (r = e || "#" !== t[0] || t.match(/[ .<>:~]/) ? (e || document).querySelectorAll(t.trim()) : [document.getElementById(t.trim().split("#")[1])], n = 0; n < r.length; n += 1) r[n] && i.push(r[n])
                } else if (t.nodeType || t === window || t === document) i.push(t);
            else if (t.length > 0 && t[0].nodeType)
                for (n = 0; n < t.length; n += 1) i.push(t[n]);
            return new s(i)
        }

        function e(t) {
            for (var e = [], i = 0; i < t.length; i += 1) - 1 === e.indexOf(t[i]) && e.push(t[i]);
            return e
        }
        var i, n = i = "undefined" == typeof window ? {
                navigator: {
                    userAgent: ""
                },
                location: {},
                history: {},
                addEventListener: function () {},
                removeEventListener: function () {},
                getComputedStyle: function () {
                    return {}
                },
                Image: function () {},
                Date: function () {},
                screen: {}
            } : window,
            s = function (t) {
                for (var e = 0; e < t.length; e += 1) this[e] = t[e];
                return this.length = t.length, this
            };
        t.fn = s.prototype, t.Class = s, t.Dom7 = s;
        "resize scroll".split(" ");
        var r = {
            addClass: function (t) {
                if (void 0 === t) return this;
                for (var e = t.split(" "), i = 0; i < e.length; i += 1)
                    for (var n = 0; n < this.length; n += 1) void 0 !== this[n].classList && this[n].classList.add(e[i]);
                return this
            },
            removeClass: function (t) {
                for (var e = t.split(" "), i = 0; i < e.length; i += 1)
                    for (var n = 0; n < this.length; n += 1) void 0 !== this[n].classList && this[n].classList.remove(e[i]);
                return this
            },
            hasClass: function (t) {
                return !!this[0] && this[0].classList.contains(t)
            },
            toggleClass: function (t) {
                for (var e = t.split(" "), i = 0; i < e.length; i += 1)
                    for (var n = 0; n < this.length; n += 1) void 0 !== this[n].classList && this[n].classList.toggle(e[i]);
                return this
            },
            attr: function (t, e) {
                var i = arguments;
                if (1 !== arguments.length || "string" != typeof t) {
                    for (var n = 0; n < this.length; n += 1)
                        if (2 === i.length) this[n].setAttribute(t, e);
                        else
                            for (var s in t) this[n][s] = t[s], this[n].setAttribute(s, t[s]);
                    return this
                }
                if (this[0]) return this[0].getAttribute(t)
            },
            removeAttr: function (t) {
                for (var e = 0; e < this.length; e += 1) this[e].removeAttribute(t);
                return this
            },
            data: function (t, e) {
                var i;
                if (void 0 !== e) {
                    for (var n = 0; n < this.length; n += 1)(i = this[n]).dom7ElementDataStorage || (i.dom7ElementDataStorage = {}), i.dom7ElementDataStorage[t] = e;
                    return this
                }
                if (i = this[0]) {
                    if (i.dom7ElementDataStorage && t in i.dom7ElementDataStorage) return i.dom7ElementDataStorage[t];
                    var s = i.getAttribute("data-" + t);
                    if (s) return s
                }
            },
            transform: function (t) {
                for (var e = 0; e < this.length; e += 1) {
                    var i = this[e].style;
                    i.webkitTransform = t, i.transform = t
                }
                return this
            },
            transition: function (t) {
                "string" != typeof t && (t += "ms");
                for (var e = 0; e < this.length; e += 1) {
                    var i = this[e].style;
                    i.webkitTransitionDuration = t, i.transitionDuration = t
                }
                return this
            },
            on: function () {
                function e(e) {
                    var i = e.target;
                    if (i) {
                        var n = e.target.dom7EventData || [];
                        if (n.unshift(e), t(i).is(o)) a.apply(i, n);
                        else
                            for (var s = t(i).parents(), r = 0; r < s.length; r += 1) t(s[r]).is(o) && a.apply(s[r], n)
                    }
                }

                function i(t) {
                    var e = t && t.target ? t.target.dom7EventData || [] : [];
                    e.unshift(t), a.apply(this, e)
                }
                for (var n = [], s = arguments.length; s--;) n[s] = arguments[s];
                var r = n[0],
                    o = n[1],
                    a = n[2],
                    l = n[3];
                if ("function" == typeof n[1]) {
                    var h;
                    r = (h = n)[0], a = h[1], l = h[2], o = void 0
                }
                l || (l = !1);
                for (var d, c = r.split(" "), u = 0; u < this.length; u += 1) {
                    var p = this[u];
                    if (o)
                        for (d = 0; d < c.length; d += 1) p.dom7LiveListeners || (p.dom7LiveListeners = []), p.dom7LiveListeners.push({
                            type: r,
                            listener: a,
                            proxyListener: e
                        }), p.addEventListener(c[d], e, l);
                    else
                        for (d = 0; d < c.length; d += 1) p.dom7Listeners || (p.dom7Listeners = []), p.dom7Listeners.push({
                            type: r,
                            listener: a,
                            proxyListener: i
                        }), p.addEventListener(c[d], i, l)
                }
                return this
            },
            off: function () {
                for (var t = [], e = arguments.length; e--;) t[e] = arguments[e];
                var i = t[0],
                    n = t[1],
                    s = t[2],
                    r = t[3];
                if ("function" == typeof t[1]) {
                    var o;
                    i = (o = t)[0], s = o[1], r = o[2], n = void 0
                }
                r || (r = !1);
                for (var a = i.split(" "), l = 0; l < a.length; l += 1)
                    for (var h = 0; h < this.length; h += 1) {
                        var d = this[h];
                        if (n) {
                            if (d.dom7LiveListeners)
                                for (var c = 0; c < d.dom7LiveListeners.length; c += 1) s ? d.dom7LiveListeners[c].listener === s && d.removeEventListener(a[l], d.dom7LiveListeners[c].proxyListener, r) : d.dom7LiveListeners[c].type === a[l] && d.removeEventListener(a[l], d.dom7LiveListeners[c].proxyListener, r)
                        } else if (d.dom7Listeners)
                            for (var u = 0; u < d.dom7Listeners.length; u += 1) s ? d.dom7Listeners[u].listener === s && d.removeEventListener(a[l], d.dom7Listeners[u].proxyListener, r) : d.dom7Listeners[u].type === a[l] && d.removeEventListener(a[l], d.dom7Listeners[u].proxyListener, r)
                    }
                return this
            },
            trigger: function () {
                for (var t = [], e = arguments.length; e--;) t[e] = arguments[e];
                for (var i = t[0].split(" "), n = t[1], s = 0; s < i.length; s += 1)
                    for (var r = 0; r < this.length; r += 1) {
                        var o = void 0;
                        try {
                            o = new window.CustomEvent(i[s], {
                                detail: n,
                                bubbles: !0,
                                cancelable: !0
                            })
                        } catch (t) {
                            (o = document.createEvent("Event")).initEvent(i[s], !0, !0), o.detail = n
                        }
                        this[r].dom7EventData = t.filter(function (t, e) {
                            return e > 0
                        }), this[r].dispatchEvent(o), this[r].dom7EventData = [], delete this[r].dom7EventData
                    }
                return this
            },
            transitionEnd: function (t) {
                function e(r) {
                    if (r.target === this)
                        for (t.call(this, r), i = 0; i < n.length; i += 1) s.off(n[i], e)
                }
                var i, n = ["webkitTransitionEnd", "transitionend"],
                    s = this;
                if (t)
                    for (i = 0; i < n.length; i += 1) s.on(n[i], e);
                return this
            },
            outerWidth: function (t) {
                if (this.length > 0) {
                    if (t) {
                        var e = this.styles();
                        return this[0].offsetWidth + parseFloat(e.getPropertyValue("margin-right")) + parseFloat(e.getPropertyValue("margin-left"))
                    }
                    return this[0].offsetWidth
                }
                return null
            },
            outerHeight: function (t) {
                if (this.length > 0) {
                    if (t) {
                        var e = this.styles();
                        return this[0].offsetHeight + parseFloat(e.getPropertyValue("margin-top")) + parseFloat(e.getPropertyValue("margin-bottom"))
                    }
                    return this[0].offsetHeight
                }
                return null
            },
            offset: function () {
                if (this.length > 0) {
                    var t = this[0],
                        e = t.getBoundingClientRect(),
                        i = document.body,
                        n = t.clientTop || i.clientTop || 0,
                        s = t.clientLeft || i.clientLeft || 0,
                        r = t === window ? window.scrollY : t.scrollTop,
                        o = t === window ? window.scrollX : t.scrollLeft;
                    return {
                        top: e.top + r - n,
                        left: e.left + o - s
                    }
                }
                return null
            },
            css: function (t, e) {
                var i;
                if (1 === arguments.length) {
                    if ("string" != typeof t) {
                        for (i = 0; i < this.length; i += 1)
                            for (var n in t) this[i].style[n] = t[n];
                        return this
                    }
                    if (this[0]) return window.getComputedStyle(this[0], null).getPropertyValue(t)
                }
                if (2 === arguments.length && "string" == typeof t) {
                    for (i = 0; i < this.length; i += 1) this[i].style[t] = e;
                    return this
                }
                return this
            },
            each: function (t) {
                if (!t) return this;
                for (var e = 0; e < this.length; e += 1)
                    if (!1 === t.call(this[e], e, this[e])) return this;
                return this
            },
            html: function (t) {
                if (void 0 === t) return this[0] ? this[0].innerHTML : void 0;
                for (var e = 0; e < this.length; e += 1) this[e].innerHTML = t;
                return this
            },
            text: function (t) {
                if (void 0 === t) return this[0] ? this[0].textContent.trim() : null;
                for (var e = 0; e < this.length; e += 1) this[e].textContent = t;
                return this
            },
            is: function (e) {
                var i, n, r = this[0];
                if (!r || void 0 === e) return !1;
                if ("string" == typeof e) {
                    if (r.matches) return r.matches(e);
                    if (r.webkitMatchesSelector) return r.webkitMatchesSelector(e);
                    if (r.msMatchesSelector) return r.msMatchesSelector(e);
                    for (i = t(e), n = 0; n < i.length; n += 1)
                        if (i[n] === r) return !0;
                    return !1
                }
                if (e === document) return r === document;
                if (e === window) return r === window;
                if (e.nodeType || e instanceof s) {
                    for (i = e.nodeType ? [e] : e, n = 0; n < i.length; n += 1)
                        if (i[n] === r) return !0;
                    return !1
                }
                return !1
            },
            index: function () {
                var t, e = this[0];
                if (e) {
                    for (t = 0; null !== (e = e.previousSibling);) 1 === e.nodeType && (t += 1);
                    return t
                }
            },
            eq: function (t) {
                if (void 0 === t) return this;
                var e, i = this.length;
                return t > i - 1 ? new s([]) : t < 0 ? (e = i + t, new s(e < 0 ? [] : [this[e]])) : new s([this[t]])
            },
            append: function () {
                for (var t = [], e = arguments.length; e--;) t[e] = arguments[e];
                for (var i, n = 0; n < t.length; n += 1) {
                    i = t[n];
                    for (var r = 0; r < this.length; r += 1)
                        if ("string" == typeof i) {
                            var o = document.createElement("div");
                            for (o.innerHTML = i; o.firstChild;) this[r].appendChild(o.firstChild)
                        } else if (i instanceof s)
                        for (var a = 0; a < i.length; a += 1) this[r].appendChild(i[a]);
                    else this[r].appendChild(i)
                }
                return this
            },
            prepend: function (t) {
                var e, i;
                for (e = 0; e < this.length; e += 1)
                    if ("string" == typeof t) {
                        var n = document.createElement("div");
                        for (n.innerHTML = t, i = n.childNodes.length - 1; i >= 0; i -= 1) this[e].insertBefore(n.childNodes[i], this[e].childNodes[0])
                    } else if (t instanceof s)
                    for (i = 0; i < t.length; i += 1) this[e].insertBefore(t[i], this[e].childNodes[0]);
                else this[e].insertBefore(t, this[e].childNodes[0]);
                return this
            },
            next: function (e) {
                return new s(this.length > 0 ? e ? this[0].nextElementSibling && t(this[0].nextElementSibling).is(e) ? [this[0].nextElementSibling] : [] : this[0].nextElementSibling ? [this[0].nextElementSibling] : [] : [])
            },
            nextAll: function (e) {
                var i = [],
                    n = this[0];
                if (!n) return new s([]);
                for (; n.nextElementSibling;) {
                    var r = n.nextElementSibling;
                    e ? t(r).is(e) && i.push(r) : i.push(r), n = r
                }
                return new s(i)
            },
            prev: function (e) {
                if (this.length > 0) {
                    var i = this[0];
                    return new s(e ? i.previousElementSibling && t(i.previousElementSibling).is(e) ? [i.previousElementSibling] : [] : i.previousElementSibling ? [i.previousElementSibling] : [])
                }
                return new s([])
            },
            prevAll: function (e) {
                var i = [],
                    n = this[0];
                if (!n) return new s([]);
                for (; n.previousElementSibling;) {
                    var r = n.previousElementSibling;
                    e ? t(r).is(e) && i.push(r) : i.push(r), n = r
                }
                return new s(i)
            },
            parent: function (i) {
                for (var n = [], s = 0; s < this.length; s += 1) null !== this[s].parentNode && (i ? t(this[s].parentNode).is(i) && n.push(this[s].parentNode) : n.push(this[s].parentNode));
                return t(e(n))
            },
            parents: function (i) {
                for (var n = [], s = 0; s < this.length; s += 1)
                    for (var r = this[s].parentNode; r;) i ? t(r).is(i) && n.push(r) : n.push(r), r = r.parentNode;
                return t(e(n))
            },
            closest: function (t) {
                var e = this;
                return void 0 === t ? new s([]) : (e.is(t) || (e = e.parents(t).eq(0)), e)
            },
            find: function (t) {
                for (var e = [], i = 0; i < this.length; i += 1)
                    for (var n = this[i].querySelectorAll(t), r = 0; r < n.length; r += 1) e.push(n[r]);
                return new s(e)
            },
            children: function (i) {
                for (var n = [], r = 0; r < this.length; r += 1)
                    for (var o = this[r].childNodes, a = 0; a < o.length; a += 1) i ? 1 === o[a].nodeType && t(o[a]).is(i) && n.push(o[a]) : 1 === o[a].nodeType && n.push(o[a]);
                return new s(e(n))
            },
            remove: function () {
                for (var t = 0; t < this.length; t += 1) this[t].parentNode && this[t].parentNode.removeChild(this[t]);
                return this
            },
            add: function () {
                for (var e = [], i = arguments.length; i--;) e[i] = arguments[i];
                var n, s;
                for (n = 0; n < e.length; n += 1) {
                    var r = t(e[n]);
                    for (s = 0; s < r.length; s += 1) this[this.length] = r[s], this.length += 1
                }
                return this
            },
            styles: function () {
                return this[0] ? window.getComputedStyle(this[0], null) : {}
            }
        };
        Object.keys(r).forEach(function (e) {
            t.fn[e] = r[e]
        });
        var o, a = {
                deleteProps: function (t) {
                    var e = t;
                    Object.keys(e).forEach(function (t) {
                        try {
                            e[t] = null
                        } catch (t) {}
                        try {
                            delete e[t]
                        } catch (t) {}
                    })
                },
                nextTick: function (t, e) {
                    return void 0 === e && (e = 0), setTimeout(t, e)
                },
                now: function () {
                    return Date.now()
                },
                getTranslate: function (t, e) {
                    void 0 === e && (e = "x");
                    var i, s, r, o = n.getComputedStyle(t, null);
                    return n.WebKitCSSMatrix ? ((s = o.transform || o.webkitTransform).split(",").length > 6 && (s = s.split(", ").map(function (t) {
                        return t.replace(",", ".")
                    }).join(", ")), r = new n.WebKitCSSMatrix("none" === s ? "" : s)) : i = (r = o.MozTransform || o.OTransform || o.MsTransform || o.msTransform || o.transform || o.getPropertyValue("transform").replace("translate(", "matrix(1, 0, 0, 1,")).toString().split(","), "x" === e && (s = n.WebKitCSSMatrix ? r.m41 : 16 === i.length ? parseFloat(i[12]) : parseFloat(i[4])), "y" === e && (s = n.WebKitCSSMatrix ? r.m42 : 16 === i.length ? parseFloat(i[13]) : parseFloat(i[5])), s || 0
                },
                parseUrlQuery: function (t) {
                    var e, i, s, r, o = {},
                        a = t || n.location.href;
                    if ("string" == typeof a && a.length)
                        for (r = (i = (a = a.indexOf("?") > -1 ? a.replace(/\S*\?/, "") : "").split("&").filter(function (t) {
                                return "" !== t
                            })).length, e = 0; e < r; e += 1) s = i[e].replace(/#\S+/g, "").split("="), o[decodeURIComponent(s[0])] = void 0 === s[1] ? void 0 : decodeURIComponent(s[1]) || "";
                    return o
                },
                isObject: function (t) {
                    return "object" == typeof t && null !== t && t.constructor && t.constructor === Object
                },
                extend: function () {
                    for (var t = [], e = arguments.length; e--;) t[e] = arguments[e];
                    for (var i = Object(t[0]), n = 1; n < t.length; n += 1) {
                        var s = t[n];
                        if (void 0 !== s && null !== s)
                            for (var r = Object.keys(Object(s)), o = 0, l = r.length; o < l; o += 1) {
                                var h = r[o],
                                    d = Object.getOwnPropertyDescriptor(s, h);
                                void 0 !== d && d.enumerable && (a.isObject(i[h]) && a.isObject(s[h]) ? a.extend(i[h], s[h]) : !a.isObject(i[h]) && a.isObject(s[h]) ? (i[h] = {}, a.extend(i[h], s[h])) : i[h] = s[h])
                            }
                    }
                    return i
                }
            },
            l = o = "undefined" == typeof document ? {
                addEventListener: function () {},
                removeEventListener: function () {},
                activeElement: {
                    blur: function () {},
                    nodeName: ""
                },
                querySelector: function () {
                    return {}
                },
                querySelectorAll: function () {
                    return []
                },
                createElement: function () {
                    return {
                        style: {},
                        setAttribute: function () {},
                        getElementsByTagName: function () {
                            return []
                        }
                    }
                },
                location: {
                    hash: ""
                }
            } : document,
            h = {
                touch: n.Modernizr && !0 === n.Modernizr.touch || !!("ontouchstart" in n || n.DocumentTouch && l instanceof n.DocumentTouch),
                transforms3d: n.Modernizr && !0 === n.Modernizr.csstransforms3d || function () {
                    var t = l.createElement("div").style;
                    return "webkitPerspective" in t || "MozPerspective" in t || "OPerspective" in t || "MsPerspective" in t || "perspective" in t
                }(),
                flexbox: function () {
                    for (var t = l.createElement("div").style, e = "alignItems webkitAlignItems webkitBoxAlign msFlexAlign mozBoxAlign webkitFlexDirection msFlexDirection mozBoxDirection mozBoxOrient webkitBoxDirection webkitBoxOrient".split(" "), i = 0; i < e.length; i += 1)
                        if (e[i] in t) return !0;
                    return !1
                }(),
                observer: "MutationObserver" in n || "WebkitMutationObserver" in n,
                passiveListener: function () {
                    var t = !1;
                    try {
                        var e = Object.defineProperty({}, "passive", {
                            get: function () {
                                t = !0
                            }
                        });
                        n.addEventListener("testPassiveListener", null, e)
                    } catch (t) {}
                    return t
                }(),
                gestures: "ongesturestart" in n
            },
            d = function (t) {
                void 0 === t && (t = {});
                var e = this;
                e.params = t, e.eventsListeners = {}, e.params && e.params.on && Object.keys(e.params.on).forEach(function (t) {
                    e.on(t, e.params.on[t])
                })
            },
            c = {
                components: {}
            };
        d.prototype.on = function (t, e) {
            var i = this;
            return "function" != typeof e ? i : (t.split(" ").forEach(function (t) {
                i.eventsListeners[t] || (i.eventsListeners[t] = []), i.eventsListeners[t].push(e)
            }), i)
        }, d.prototype.once = function (t, e) {
            function i() {
                for (var s = [], r = arguments.length; r--;) s[r] = arguments[r];
                e.apply(n, s), n.off(t, i)
            }
            var n = this;
            return "function" != typeof e ? n : n.on(t, i)
        }, d.prototype.off = function (t, e) {
            var i = this;
            return t.split(" ").forEach(function (t) {
                void 0 === e ? i.eventsListeners[t] = [] : i.eventsListeners[t].forEach(function (n, s) {
                    n === e && i.eventsListeners[t].splice(s, 1)
                })
            }), i
        }, d.prototype.emit = function () {
            for (var t = [], e = arguments.length; e--;) t[e] = arguments[e];
            var i = this;
            if (!i.eventsListeners) return i;
            var n, s, r;
            "string" == typeof t[0] || Array.isArray(t[0]) ? (n = t[0], s = t.slice(1, t.length), r = i) : (n = t[0].events, s = t[0].data, r = t[0].context || i);
            return (Array.isArray(n) ? n : n.split(" ")).forEach(function (t) {
                if (i.eventsListeners[t]) {
                    var e = [];
                    i.eventsListeners[t].forEach(function (t) {
                        e.push(t)
                    }), e.forEach(function (t) {
                        t.apply(r, s)
                    })
                }
            }), i
        }, d.prototype.useModulesParams = function (t) {
            var e = this;
            e.modules && Object.keys(e.modules).forEach(function (i) {
                var n = e.modules[i];
                n.params && a.extend(t, n.params)
            })
        }, d.prototype.useModules = function (t) {
            void 0 === t && (t = {});
            var e = this;
            e.modules && Object.keys(e.modules).forEach(function (i) {
                var n = e.modules[i],
                    s = t[i] || {};
                n.instance && Object.keys(n.instance).forEach(function (t) {
                    var i = n.instance[t];
                    e[t] = "function" == typeof i ? i.bind(e) : i
                }), n.on && e.on && Object.keys(n.on).forEach(function (t) {
                    e.on(t, n.on[t])
                }), n.create && n.create.bind(e)(s)
            })
        }, c.components.set = function (t) {
            this.use && this.use(t)
        }, d.installModule = function (t) {
            for (var e = [], i = arguments.length - 1; i-- > 0;) e[i] = arguments[i + 1];
            var n = this;
            n.prototype.modules || (n.prototype.modules = {});
            var s = t.name || Object.keys(n.prototype.modules).length + "_" + a.now();
            return n.prototype.modules[s] = t, t.proto && Object.keys(t.proto).forEach(function (e) {
                n.prototype[e] = t.proto[e]
            }), t.static && Object.keys(t.static).forEach(function (e) {
                n[e] = t.static[e]
            }), t.install && t.install.apply(n, e), n
        }, d.use = function (t) {
            for (var e = [], i = arguments.length - 1; i-- > 0;) e[i] = arguments[i + 1];
            var n = this;
            return Array.isArray(t) ? (t.forEach(function (t) {
                return n.installModule(t)
            }), n) : n.installModule.apply(n, [t].concat(e))
        }, Object.defineProperties(d, c);
        var u = {
                updateSize: function () {
                    var t, e, i = this.$el;
                    t = void 0 !== this.params.width ? this.params.width : i[0].clientWidth, e = void 0 !== this.params.height ? this.params.height : i[0].clientHeight, 0 === t && this.isHorizontal() || 0 === e && this.isVertical() || (t = t - parseInt(i.css("padding-left"), 10) - parseInt(i.css("padding-right"), 10), e = e - parseInt(i.css("padding-top"), 10) - parseInt(i.css("padding-bottom"), 10), a.extend(this, {
                        width: t,
                        height: e,
                        size: this.isHorizontal() ? t : e
                    }))
                },
                updateSlides: function () {
                    var t = this.params,
                        e = this.$wrapperEl,
                        i = this.size,
                        n = this.rtl,
                        s = this.wrongRTL,
                        r = e.children("." + this.params.slideClass),
                        o = this.virtual && t.virtual.enabled ? this.virtual.slides.length : r.length,
                        l = [],
                        d = [],
                        c = [],
                        u = t.slidesOffsetBefore;
                    "function" == typeof u && (u = t.slidesOffsetBefore.call(this));
                    var p = t.slidesOffsetAfter;
                    "function" == typeof p && (p = t.slidesOffsetAfter.call(this));
                    var f = o,
                        m = this.snapGrid.length,
                        g = this.snapGrid.length,
                        v = t.spaceBetween,
                        y = -u,
                        b = 0,
                        w = 0;
                    if (void 0 !== i) {
                        "string" == typeof v && v.indexOf("%") >= 0 && (v = parseFloat(v.replace("%", "")) / 100 * i), this.virtualSize = -v, n ? r.css({
                            marginLeft: "",
                            marginTop: ""
                        }) : r.css({
                            marginRight: "",
                            marginBottom: ""
                        });
                        var x;
                        t.slidesPerColumn > 1 && (x = Math.floor(o / t.slidesPerColumn) === o / this.params.slidesPerColumn ? o : Math.ceil(o / t.slidesPerColumn) * t.slidesPerColumn, "auto" !== t.slidesPerView && "row" === t.slidesPerColumnFill && (x = Math.max(x, t.slidesPerView * t.slidesPerColumn)));
                        for (var C, T = t.slidesPerColumn, E = x / T, S = E - (t.slidesPerColumn * E - o), _ = 0; _ < o; _ += 1) {
                            C = 0;
                            var k = r.eq(_);
                            if (t.slidesPerColumn > 1) {
                                var $ = void 0,
                                    z = void 0,
                                    M = void 0;
                                "column" === t.slidesPerColumnFill ? (M = _ - (z = Math.floor(_ / T)) * T, (z > S || z === S && M === T - 1) && (M += 1) >= T && (M = 0, z += 1), $ = z + M * x / T, k.css({
                                    "-webkit-box-ordinal-group": $,
                                    "-moz-box-ordinal-group": $,
                                    "-ms-flex-order": $,
                                    "-webkit-order": $,
                                    order: $
                                })) : z = _ - (M = Math.floor(_ / E)) * E, k.css("margin-" + (this.isHorizontal() ? "top" : "left"), 0 !== M && t.spaceBetween && t.spaceBetween + "px").attr("data-swiper-column", z).attr("data-swiper-row", M)
                            }
                            "none" !== k.css("display") && ("auto" === t.slidesPerView ? (C = this.isHorizontal() ? k.outerWidth(!0) : k.outerHeight(!0), t.roundLengths && (C = Math.floor(C))) : (C = (i - (t.slidesPerView - 1) * v) / t.slidesPerView, t.roundLengths && (C = Math.floor(C)), r[_] && (this.isHorizontal() ? r[_].style.width = C + "px" : r[_].style.height = C + "px")), r[_] && (r[_].swiperSlideSize = C), c.push(C), t.centeredSlides ? (y = y + C / 2 + b / 2 + v, 0 === b && 0 !== _ && (y = y - i / 2 - v), 0 === _ && (y = y - i / 2 - v), Math.abs(y) < .001 && (y = 0), w % t.slidesPerGroup == 0 && l.push(y), d.push(y)) : (w % t.slidesPerGroup == 0 && l.push(y), d.push(y), y = y + C + v), this.virtualSize += C + v, b = C, w += 1)
                        }
                        this.virtualSize = Math.max(this.virtualSize, i) + p;
                        var D;
                        if (n && s && ("slide" === t.effect || "coverflow" === t.effect) && e.css({
                                width: this.virtualSize + t.spaceBetween + "px"
                            }), h.flexbox && !t.setWrapperSize || (this.isHorizontal() ? e.css({
                                width: this.virtualSize + t.spaceBetween + "px"
                            }) : e.css({
                                height: this.virtualSize + t.spaceBetween + "px"
                            })), t.slidesPerColumn > 1 && (this.virtualSize = (C + t.spaceBetween) * x, this.virtualSize = Math.ceil(this.virtualSize / t.slidesPerColumn) - t.spaceBetween, this.isHorizontal() ? e.css({
                                width: this.virtualSize + t.spaceBetween + "px"
                            }) : e.css({
                                height: this.virtualSize + t.spaceBetween + "px"
                            }), t.centeredSlides)) {
                            D = [];
                            for (var I = 0; I < l.length; I += 1) l[I] < this.virtualSize + l[0] && D.push(l[I]);
                            l = D
                        }
                        if (!t.centeredSlides) {
                            D = [];
                            for (var P = 0; P < l.length; P += 1) l[P] <= this.virtualSize - i && D.push(l[P]);
                            l = D, Math.floor(this.virtualSize - i) - Math.floor(l[l.length - 1]) > 1 && l.push(this.virtualSize - i)
                        }
                        0 === l.length && (l = [0]), 0 !== t.spaceBetween && (this.isHorizontal() ? n ? r.css({
                            marginLeft: v + "px"
                        }) : r.css({
                            marginRight: v + "px"
                        }) : r.css({
                            marginBottom: v + "px"
                        })), a.extend(this, {
                            slides: r,
                            snapGrid: l,
                            slidesGrid: d,
                            slidesSizesGrid: c
                        }), o !== f && this.emit("slidesLengthChange"), l.length !== m && this.emit("snapGridLengthChange"), d.length !== g && this.emit("slidesGridLengthChange"), (t.watchSlidesProgress || t.watchSlidesVisibility) && this.updateSlidesOffset()
                    }
                },
                updateAutoHeight: function () {
                    var t, e = [],
                        i = 0;
                    if ("auto" !== this.params.slidesPerView && this.params.slidesPerView > 1)
                        for (t = 0; t < Math.ceil(this.params.slidesPerView); t += 1) {
                            var n = this.activeIndex + t;
                            if (n > this.slides.length) break;
                            e.push(this.slides.eq(n)[0])
                        } else e.push(this.slides.eq(this.activeIndex)[0]);
                    for (t = 0; t < e.length; t += 1)
                        if (void 0 !== e[t]) {
                            var s = e[t].offsetHeight;
                            i = s > i ? s : i
                        }
                    i && this.$wrapperEl.css("height", i + "px")
                },
                updateSlidesOffset: function () {
                    for (var t = this.slides, e = 0; e < t.length; e += 1) t[e].swiperSlideOffset = this.isHorizontal() ? t[e].offsetLeft : t[e].offsetTop
                },
                updateSlidesProgress: function (t) {
                    void 0 === t && (t = this.translate || 0);
                    var e = this.params,
                        i = this.slides,
                        n = this.rtl;
                    if (0 !== i.length) {
                        void 0 === i[0].swiperSlideOffset && this.updateSlidesOffset();
                        var s = -t;
                        n && (s = t), i.removeClass(e.slideVisibleClass);
                        for (var r = 0; r < i.length; r += 1) {
                            var o = i[r],
                                a = (s + (e.centeredSlides ? this.minTranslate() : 0) - o.swiperSlideOffset) / (o.swiperSlideSize + e.spaceBetween);
                            if (e.watchSlidesVisibility) {
                                var l = -(s - o.swiperSlideOffset),
                                    h = l + this.slidesSizesGrid[r];
                                (l >= 0 && l < this.size || h > 0 && h <= this.size || l <= 0 && h >= this.size) && i.eq(r).addClass(e.slideVisibleClass)
                            }
                            o.progress = n ? -a : a
                        }
                    }
                },
                updateProgress: function (t) {
                    void 0 === t && (t = this.translate || 0);
                    var e = this.params,
                        i = this.maxTranslate() - this.minTranslate(),
                        n = this.progress,
                        s = this.isBeginning,
                        r = this.isEnd,
                        o = s,
                        l = r;
                    0 === i ? (n = 0, s = !0, r = !0) : (s = (n = (t - this.minTranslate()) / i) <= 0, r = n >= 1), a.extend(this, {
                        progress: n,
                        isBeginning: s,
                        isEnd: r
                    }), (e.watchSlidesProgress || e.watchSlidesVisibility) && this.updateSlidesProgress(t), s && !o && this.emit("reachBeginning toEdge"), r && !l && this.emit("reachEnd toEdge"), (o && !s || l && !r) && this.emit("fromEdge"), this.emit("progress", n)
                },
                updateSlidesClasses: function () {
                    var t = this.slides,
                        e = this.params,
                        i = this.$wrapperEl,
                        n = this.activeIndex,
                        s = this.realIndex,
                        r = this.virtual && e.virtual.enabled;
                    t.removeClass(e.slideActiveClass + " " + e.slideNextClass + " " + e.slidePrevClass + " " + e.slideDuplicateActiveClass + " " + e.slideDuplicateNextClass + " " + e.slideDuplicatePrevClass);
                    var o;
                    (o = r ? this.$wrapperEl.find("." + e.slideClass + '[data-swiper-slide-index="' + n + '"]') : t.eq(n)).addClass(e.slideActiveClass), e.loop && (o.hasClass(e.slideDuplicateClass) ? i.children("." + e.slideClass + ":not(." + e.slideDuplicateClass + ')[data-swiper-slide-index="' + s + '"]').addClass(e.slideDuplicateActiveClass) : i.children("." + e.slideClass + "." + e.slideDuplicateClass + '[data-swiper-slide-index="' + s + '"]').addClass(e.slideDuplicateActiveClass));
                    var a = o.nextAll("." + e.slideClass).eq(0).addClass(e.slideNextClass);
                    e.loop && 0 === a.length && (a = t.eq(0)).addClass(e.slideNextClass);
                    var l = o.prevAll("." + e.slideClass).eq(0).addClass(e.slidePrevClass);
                    e.loop && 0 === l.length && (l = t.eq(-1)).addClass(e.slidePrevClass), e.loop && (a.hasClass(e.slideDuplicateClass) ? i.children("." + e.slideClass + ":not(." + e.slideDuplicateClass + ')[data-swiper-slide-index="' + a.attr("data-swiper-slide-index") + '"]').addClass(e.slideDuplicateNextClass) : i.children("." + e.slideClass + "." + e.slideDuplicateClass + '[data-swiper-slide-index="' + a.attr("data-swiper-slide-index") + '"]').addClass(e.slideDuplicateNextClass), l.hasClass(e.slideDuplicateClass) ? i.children("." + e.slideClass + ":not(." + e.slideDuplicateClass + ')[data-swiper-slide-index="' + l.attr("data-swiper-slide-index") + '"]').addClass(e.slideDuplicatePrevClass) : i.children("." + e.slideClass + "." + e.slideDuplicateClass + '[data-swiper-slide-index="' + l.attr("data-swiper-slide-index") + '"]').addClass(e.slideDuplicatePrevClass))
                },
                updateActiveIndex: function (t) {
                    var e, i = this.rtl ? this.translate : -this.translate,
                        n = this.slidesGrid,
                        s = this.snapGrid,
                        r = this.params,
                        o = this.activeIndex,
                        l = this.realIndex,
                        h = this.snapIndex,
                        d = t;
                    if (void 0 === d) {
                        for (var c = 0; c < n.length; c += 1) void 0 !== n[c + 1] ? i >= n[c] && i < n[c + 1] - (n[c + 1] - n[c]) / 2 ? d = c : i >= n[c] && i < n[c + 1] && (d = c + 1) : i >= n[c] && (d = c);
                        r.normalizeSlideIndex && (d < 0 || void 0 === d) && (d = 0)
                    }
                    if ((e = s.indexOf(i) >= 0 ? s.indexOf(i) : Math.floor(d / r.slidesPerGroup)) >= s.length && (e = s.length - 1), d !== o) {
                        var u = parseInt(this.slides.eq(d).attr("data-swiper-slide-index") || d, 10);
                        a.extend(this, {
                            snapIndex: e,
                            realIndex: u,
                            previousIndex: o,
                            activeIndex: d
                        }), this.emit("activeIndexChange"), this.emit("snapIndexChange"), l !== u && this.emit("realIndexChange"), this.emit("slideChange")
                    } else e !== h && (this.snapIndex = e, this.emit("snapIndexChange"))
                },
                updateClickedSlide: function (e) {
                    var i = this.params,
                        n = t(e.target).closest("." + i.slideClass)[0],
                        s = !1;
                    if (n)
                        for (var r = 0; r < this.slides.length; r += 1) this.slides[r] === n && (s = !0);
                    if (!n || !s) return this.clickedSlide = void 0, void(this.clickedIndex = void 0);
                    this.clickedSlide = n, this.virtual && this.params.virtual.enabled ? this.clickedIndex = parseInt(t(n).attr("data-swiper-slide-index"), 10) : this.clickedIndex = t(n).index(), i.slideToClickedSlide && void 0 !== this.clickedIndex && this.clickedIndex !== this.activeIndex && this.slideToClickedSlide()
                }
            },
            p = {
                getTranslate: function (t) {
                    void 0 === t && (t = this.isHorizontal() ? "x" : "y");
                    var e = this.params,
                        i = this.rtl,
                        n = this.translate,
                        s = this.$wrapperEl;
                    if (e.virtualTranslate) return i ? -n : n;
                    var r = a.getTranslate(s[0], t);
                    return i && (r = -r), r || 0
                },
                setTranslate: function (t, e) {
                    var i = this.rtl,
                        n = this.params,
                        s = this.$wrapperEl,
                        r = this.progress,
                        o = 0,
                        a = 0;
                    this.isHorizontal() ? o = i ? -t : t : a = t, n.roundLengths && (o = Math.floor(o), a = Math.floor(a)), n.virtualTranslate || (h.transforms3d ? s.transform("translate3d(" + o + "px, " + a + "px, 0px)") : s.transform("translate(" + o + "px, " + a + "px)")), this.translate = this.isHorizontal() ? o : a;
                    var l = this.maxTranslate() - this.minTranslate();
                    (0 === l ? 0 : (t - this.minTranslate()) / l) !== r && this.updateProgress(t), this.emit("setTranslate", this.translate, e)
                },
                minTranslate: function () {
                    return -this.snapGrid[0]
                },
                maxTranslate: function () {
                    return -this.snapGrid[this.snapGrid.length - 1]
                }
            },
            f = {
                setTransition: function (t, e) {
                    this.$wrapperEl.transition(t), this.emit("setTransition", t, e)
                },
                transitionStart: function (t) {
                    void 0 === t && (t = !0);
                    var e = this.activeIndex,
                        i = this.params,
                        n = this.previousIndex;
                    i.autoHeight && this.updateAutoHeight(), this.emit("transitionStart"), t && e !== n && (this.emit("slideChangeTransitionStart"), e > n ? this.emit("slideNextTransitionStart") : this.emit("slidePrevTransitionStart"))
                },
                transitionEnd: function (t) {
                    void 0 === t && (t = !0);
                    var e = this.activeIndex,
                        i = this.previousIndex;
                    this.animating = !1, this.setTransition(0), this.emit("transitionEnd"), t && e !== i && (this.emit("slideChangeTransitionEnd"), e > i ? this.emit("slideNextTransitionEnd") : this.emit("slidePrevTransitionEnd"))
                }
            },
            m = function () {
                return {
                    isSafari: function () {
                        var t = n.navigator.userAgent.toLowerCase();
                        return t.indexOf("safari") >= 0 && t.indexOf("chrome") < 0 && t.indexOf("android") < 0
                    }(),
                    isUiWebView: /(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(n.navigator.userAgent),
                    ie: n.navigator.pointerEnabled || n.navigator.msPointerEnabled,
                    ieTouch: n.navigator.msPointerEnabled && n.navigator.msMaxTouchPoints > 1 || n.navigator.pointerEnabled && n.navigator.maxTouchPoints > 1,
                    lteIE9: function () {
                        var t = l.createElement("div");
                        return t.innerHTML = "\x3c!--[if lte IE 9]><i></i><![endif]--\x3e", 1 === t.getElementsByTagName("i").length
                    }()
                }
            }(),
            g = {
                slideTo: function (t, e, i, n) {
                    void 0 === t && (t = 0), void 0 === e && (e = this.params.speed), void 0 === i && (i = !0);
                    var s = this,
                        r = t;
                    r < 0 && (r = 0);
                    var o = s.params,
                        a = s.snapGrid,
                        l = s.slidesGrid,
                        h = s.previousIndex,
                        d = s.activeIndex,
                        c = s.rtl,
                        u = s.$wrapperEl,
                        p = Math.floor(r / o.slidesPerGroup);
                    p >= a.length && (p = a.length - 1), (d || o.initialSlide || 0) === (h || 0) && i && s.emit("beforeSlideChangeStart");
                    var f = -a[p];
                    if (s.updateProgress(f), o.normalizeSlideIndex)
                        for (var g = 0; g < l.length; g += 1) - Math.floor(100 * f) >= Math.floor(100 * l[g]) && (r = g);
                    return !(!s.allowSlideNext && f < s.translate && f < s.minTranslate() || !s.allowSlidePrev && f > s.translate && f > s.maxTranslate() && (d || 0) !== r || (c && -f === s.translate || !c && f === s.translate ? (s.updateActiveIndex(r), o.autoHeight && s.updateAutoHeight(), s.updateSlidesClasses(), "slide" !== o.effect && s.setTranslate(f), 1) : (0 === e || m.lteIE9 ? (s.setTransition(0), s.setTranslate(f), s.updateActiveIndex(r), s.updateSlidesClasses(), s.emit("beforeTransitionStart", e, n), s.transitionStart(i), s.transitionEnd(i)) : (s.setTransition(e), s.setTranslate(f), s.updateActiveIndex(r), s.updateSlidesClasses(), s.emit("beforeTransitionStart", e, n), s.transitionStart(i), s.animating || (s.animating = !0, u.transitionEnd(function () {
                        s && !s.destroyed && s.transitionEnd(i)
                    }))), 0)))
                },
                slideNext: function (t, e, i) {
                    void 0 === t && (t = this.params.speed), void 0 === e && (e = !0);
                    var n = this.params,
                        s = this.animating;
                    return n.loop ? !s && (this.loopFix(), this._clientLeft = this.$wrapperEl[0].clientLeft, this.slideTo(this.activeIndex + n.slidesPerGroup, t, e, i)) : this.slideTo(this.activeIndex + n.slidesPerGroup, t, e, i)
                },
                slidePrev: function (t, e, i) {
                    void 0 === t && (t = this.params.speed), void 0 === e && (e = !0);
                    var n = this.params,
                        s = this.animating;
                    return n.loop ? !s && (this.loopFix(), this._clientLeft = this.$wrapperEl[0].clientLeft, this.slideTo(this.activeIndex - 1, t, e, i)) : this.slideTo(this.activeIndex - 1, t, e, i)
                },
                slideReset: function (t, e, i) {
                    void 0 === t && (t = this.params.speed), void 0 === e && (e = !0);
                    return this.slideTo(this.activeIndex, t, e, i)
                },
                slideToClickedSlide: function () {
                    var e, i = this,
                        n = i.params,
                        s = i.$wrapperEl,
                        r = "auto" === n.slidesPerView ? i.slidesPerViewDynamic() : n.slidesPerView,
                        o = i.clickedIndex;
                    if (n.loop) {
                        if (i.animating) return;
                        e = parseInt(t(i.clickedSlide).attr("data-swiper-slide-index"), 10), n.centeredSlides ? o < i.loopedSlides - r / 2 || o > i.slides.length - i.loopedSlides + r / 2 ? (i.loopFix(), o = s.children("." + n.slideClass + '[data-swiper-slide-index="' + e + '"]:not(.' + n.slideDuplicateClass + ")").eq(0).index(), a.nextTick(function () {
                            i.slideTo(o)
                        })) : i.slideTo(o) : o > i.slides.length - r ? (i.loopFix(), o = s.children("." + n.slideClass + '[data-swiper-slide-index="' + e + '"]:not(.' + n.slideDuplicateClass + ")").eq(0).index(), a.nextTick(function () {
                            i.slideTo(o)
                        })) : i.slideTo(o)
                    } else i.slideTo(o)
                }
            },
            v = {
                loopCreate: function () {
                    var e = this,
                        i = e.params,
                        n = e.$wrapperEl;
                    n.children("." + i.slideClass + "." + i.slideDuplicateClass).remove();
                    var s = n.children("." + i.slideClass);
                    if (i.loopFillGroupWithBlank) {
                        var r = i.slidesPerGroup - s.length % i.slidesPerGroup;
                        if (r !== i.slidesPerGroup) {
                            for (var o = 0; o < r; o += 1) {
                                var a = t(l.createElement("div")).addClass(i.slideClass + " " + i.slideBlankClass);
                                n.append(a)
                            }
                            s = n.children("." + i.slideClass)
                        }
                    }
                    "auto" !== i.slidesPerView || i.loopedSlides || (i.loopedSlides = s.length), e.loopedSlides = parseInt(i.loopedSlides || i.slidesPerView, 10), e.loopedSlides += i.loopAdditionalSlides, e.loopedSlides > s.length && (e.loopedSlides = s.length);
                    var h = [],
                        d = [];
                    s.each(function (i, n) {
                        var r = t(n);
                        i < e.loopedSlides && d.push(n), i < s.length && i >= s.length - e.loopedSlides && h.push(n), r.attr("data-swiper-slide-index", i)
                    });
                    for (var c = 0; c < d.length; c += 1) n.append(t(d[c].cloneNode(!0)).addClass(i.slideDuplicateClass));
                    for (var u = h.length - 1; u >= 0; u -= 1) n.prepend(t(h[u].cloneNode(!0)).addClass(i.slideDuplicateClass))
                },
                loopFix: function () {
                    var t, e = this.params,
                        i = this.activeIndex,
                        n = this.slides,
                        s = this.loopedSlides,
                        r = this.allowSlidePrev,
                        o = this.allowSlideNext;
                    this.allowSlidePrev = !0, this.allowSlideNext = !0, i < s ? (t = n.length - 3 * s + i, t += s, this.slideTo(t, 0, !1, !0)) : ("auto" === e.slidesPerView && i >= 2 * s || i > n.length - 2 * e.slidesPerView) && (t = -n.length + i + s, t += s, this.slideTo(t, 0, !1, !0)), this.allowSlidePrev = r, this.allowSlideNext = o
                },
                loopDestroy: function () {
                    var t = this.$wrapperEl,
                        e = this.params,
                        i = this.slides;
                    t.children("." + e.slideClass + "." + e.slideDuplicateClass).remove(), i.removeAttr("data-swiper-slide-index")
                }
            },
            y = {
                setGrabCursor: function (t) {
                    if (!h.touch && this.params.simulateTouch) {
                        var e = this.el;
                        e.style.cursor = "move", e.style.cursor = t ? "-webkit-grabbing" : "-webkit-grab", e.style.cursor = t ? "-moz-grabbin" : "-moz-grab", e.style.cursor = t ? "grabbing" : "grab"
                    }
                },
                unsetGrabCursor: function () {
                    h.touch || (this.el.style.cursor = "")
                }
            },
            b = {
                appendSlide: function (t) {
                    var e = this.$wrapperEl,
                        i = this.params;
                    if (i.loop && this.loopDestroy(), "object" == typeof t && "length" in t)
                        for (var n = 0; n < t.length; n += 1) t[n] && e.append(t[n]);
                    else e.append(t);
                    i.loop && this.loopCreate(), i.observer && h.observer || this.update()
                },
                prependSlide: function (t) {
                    var e = this.params,
                        i = this.$wrapperEl,
                        n = this.activeIndex;
                    e.loop && this.loopDestroy();
                    var s = n + 1;
                    if ("object" == typeof t && "length" in t) {
                        for (var r = 0; r < t.length; r += 1) t[r] && i.prepend(t[r]);
                        s = n + t.length
                    } else i.prepend(t);
                    e.loop && this.loopCreate(), e.observer && h.observer || this.update(), this.slideTo(s, 0, !1)
                },
                removeSlide: function (t) {
                    var e = this.params,
                        i = this.$wrapperEl,
                        n = this.activeIndex;
                    e.loop && (this.loopDestroy(), this.slides = i.children("." + e.slideClass));
                    var s, r = n;
                    if ("object" == typeof t && "length" in t) {
                        for (var o = 0; o < t.length; o += 1) s = t[o], this.slides[s] && this.slides.eq(s).remove(), s < r && (r -= 1);
                        r = Math.max(r, 0)
                    } else s = t, this.slides[s] && this.slides.eq(s).remove(), s < r && (r -= 1), r = Math.max(r, 0);
                    e.loop && this.loopCreate(), e.observer && h.observer || this.update(), e.loop ? this.slideTo(r + this.loopedSlides, 0, !1) : this.slideTo(r, 0, !1)
                },
                removeAllSlides: function () {
                    for (var t = [], e = 0; e < this.slides.length; e += 1) t.push(e);
                    this.removeSlide(t)
                }
            },
            w = function () {
                var t = n.navigator.userAgent,
                    e = {
                        ios: !1,
                        android: !1,
                        androidChrome: !1,
                        desktop: !1,
                        windows: !1,
                        iphone: !1,
                        ipod: !1,
                        ipad: !1,
                        cordova: n.cordova || n.phonegap,
                        phonegap: n.cordova || n.phonegap
                    },
                    i = t.match(/(Windows Phone);?[\s\/]+([\d.]+)?/),
                    s = t.match(/(Android);?[\s\/]+([\d.]+)?/),
                    r = t.match(/(iPad).*OS\s([\d_]+)/),
                    o = t.match(/(iPod)(.*OS\s([\d_]+))?/),
                    a = !r && t.match(/(iPhone\sOS|iOS)\s([\d_]+)/);
                if (i && (e.os = "windows", e.osVersion = i[2], e.windows = !0), s && !i && (e.os = "android", e.osVersion = s[2], e.android = !0, e.androidChrome = t.toLowerCase().indexOf("chrome") >= 0), (r || a || o) && (e.os = "ios", e.ios = !0), a && !o && (e.osVersion = a[2].replace(/_/g, "."), e.iphone = !0), r && (e.osVersion = r[2].replace(/_/g, "."), e.ipad = !0), o && (e.osVersion = o[3] ? o[3].replace(/_/g, ".") : null, e.iphone = !0), e.ios && e.osVersion && t.indexOf("Version/index.html") >= 0 && "10" === e.osVersion.split(".")[0] && (e.osVersion = t.toLowerCase().split("version/index-2.html")[1].split(" ")[0]), e.desktop = !(e.os || e.android || e.webView), e.webView = (a || r || o) && t.match(/.*AppleWebKit(?!.*Safari)/i), e.os && "ios" === e.os) {
                    var h = e.osVersion.split("."),
                        d = l.querySelector('meta[name="viewport"]');
                    e.minimalUi = !e.webView && (o || a) && (1 * h[0] == 7 ? 1 * h[1] >= 1 : 1 * h[0] > 7) && d && d.getAttribute("content").indexOf("minimal-ui") >= 0
                }
                return e.pixelRatio = n.devicePixelRatio || 1, e
            }(),
            x = function (e) {
                var i = this.touchEventsData,
                    n = this.params,
                    s = this.touches,
                    r = e;
                if (r.originalEvent && (r = r.originalEvent), i.isTouchEvent = "touchstart" === r.type, (i.isTouchEvent || !("which" in r) || 3 !== r.which) && (!i.isTouched || !i.isMoved))
                    if (n.noSwiping && t(r.target).closest("." + n.noSwipingClass)[0]) this.allowClick = !0;
                    else if (!n.swipeHandler || t(r).closest(n.swipeHandler)[0]) {
                    s.currentX = "touchstart" === r.type ? r.targetTouches[0].pageX : r.pageX, s.currentY = "touchstart" === r.type ? r.targetTouches[0].pageY : r.pageY;
                    var o = s.currentX,
                        h = s.currentY;
                    if (!(w.ios && !w.cordova && n.iOSEdgeSwipeDetection && o <= n.iOSEdgeSwipeThreshold && o >= window.screen.width - n.iOSEdgeSwipeThreshold)) {
                        if (a.extend(i, {
                                isTouched: !0,
                                isMoved: !1,
                                allowTouchCallbacks: !0,
                                isScrolling: void 0,
                                startMoving: void 0
                            }), s.startX = o, s.startY = h, i.touchStartTime = a.now(), this.allowClick = !0, this.updateSize(), this.swipeDirection = void 0, n.threshold > 0 && (i.allowThresholdMove = !1), "touchstart" !== r.type) {
                            var d = !0;
                            t(r.target).is(i.formElements) && (d = !1), l.activeElement && t(l.activeElement).is(i.formElements) && l.activeElement.blur(), d && this.allowTouchMove && r.preventDefault()
                        }
                        this.emit("touchStart", r)
                    }
                }
            },
            C = function (e) {
                var i = this.touchEventsData,
                    n = this.params,
                    s = this.touches,
                    r = this.rtl,
                    o = e;
                if (o.originalEvent && (o = o.originalEvent), !i.isTouchEvent || "mousemove" !== o.type) {
                    var h = "touchmove" === o.type ? o.targetTouches[0].pageX : o.pageX,
                        d = "touchmove" === o.type ? o.targetTouches[0].pageY : o.pageY;
                    if (o.preventedByNestedSwiper) return s.startX = h, void(s.startY = d);
                    if (!this.allowTouchMove) return this.allowClick = !1, void(i.isTouched && (a.extend(s, {
                        startX: h,
                        startY: d,
                        currentX: h,
                        currentY: d
                    }), i.touchStartTime = a.now()));
                    if (i.isTouchEvent && n.touchReleaseOnEdges && !n.loop)
                        if (this.isVertical()) {
                            if (d < s.startY && this.translate <= this.maxTranslate() || d > s.startY && this.translate >= this.minTranslate()) return i.isTouched = !1, void(i.isMoved = !1)
                        } else if (h < s.startX && this.translate <= this.maxTranslate() || h > s.startX && this.translate >= this.minTranslate()) return;
                    if (i.isTouchEvent && l.activeElement && o.target === l.activeElement && t(o.target).is(i.formElements)) return i.isMoved = !0, void(this.allowClick = !1);
                    if (i.allowTouchCallbacks && this.emit("touchMove", o), !(o.targetTouches && o.targetTouches.length > 1)) {
                        s.currentX = h, s.currentY = d;
                        var c = s.currentX - s.startX,
                            u = s.currentY - s.startY;
                        if (void 0 === i.isScrolling) {
                            var p;
                            this.isHorizontal() && s.currentY === s.startY || this.isVertical() && s.currentX === s.startX ? i.isScrolling = !1 : c * c + u * u >= 25 && (p = 180 * Math.atan2(Math.abs(u), Math.abs(c)) / Math.PI, i.isScrolling = this.isHorizontal() ? p > n.touchAngle : 90 - p > n.touchAngle)
                        }
                        if (i.isScrolling && this.emit("touchMoveOpposite", o), "undefined" == typeof startMoving && (s.currentX === s.startX && s.currentY === s.startY || (i.startMoving = !0)), i.isTouched)
                            if (i.isScrolling) i.isTouched = !1;
                            else if (i.startMoving) {
                            this.allowClick = !1, o.preventDefault(), n.touchMoveStopPropagation && !n.nested && o.stopPropagation(), i.isMoved || (n.loop && this.loopFix(), i.startTranslate = this.getTranslate(), this.setTransition(0), this.animating && this.$wrapperEl.trigger("webkitTransitionEnd transitionend"), i.allowMomentumBounce = !1, !n.grabCursor || !0 !== this.allowSlideNext && !0 !== this.allowSlidePrev || this.setGrabCursor(!0), this.emit("sliderFirstMove", o)), this.emit("sliderMove", o), i.isMoved = !0;
                            var f = this.isHorizontal() ? c : u;
                            s.diff = f, f *= n.touchRatio, r && (f = -f), this.swipeDirection = f > 0 ? "prev" : "next", i.currentTranslate = f + i.startTranslate;
                            var m = !0,
                                g = n.resistanceRatio;
                            if (n.touchReleaseOnEdges && (g = 0), f > 0 && i.currentTranslate > this.minTranslate() ? (m = !1, n.resistance && (i.currentTranslate = this.minTranslate() - 1 + Math.pow(-this.minTranslate() + i.startTranslate + f, g))) : f < 0 && i.currentTranslate < this.maxTranslate() && (m = !1, n.resistance && (i.currentTranslate = this.maxTranslate() + 1 - Math.pow(this.maxTranslate() - i.startTranslate - f, g))), m && (o.preventedByNestedSwiper = !0), !this.allowSlideNext && "next" === this.swipeDirection && i.currentTranslate < i.startTranslate && (i.currentTranslate = i.startTranslate), !this.allowSlidePrev && "prev" === this.swipeDirection && i.currentTranslate > i.startTranslate && (i.currentTranslate = i.startTranslate), n.threshold > 0) {
                                if (!(Math.abs(f) > n.threshold || i.allowThresholdMove)) return void(i.currentTranslate = i.startTranslate);
                                if (!i.allowThresholdMove) return i.allowThresholdMove = !0, s.startX = s.currentX, s.startY = s.currentY, i.currentTranslate = i.startTranslate, void(s.diff = this.isHorizontal() ? s.currentX - s.startX : s.currentY - s.startY)
                            }
                            n.followFinger && ((n.freeMode || n.watchSlidesProgress || n.watchSlidesVisibility) && (this.updateActiveIndex(), this.updateSlidesClasses()), n.freeMode && (0 === i.velocities.length && i.velocities.push({
                                position: s[this.isHorizontal() ? "startX" : "startY"],
                                time: i.touchStartTime
                            }), i.velocities.push({
                                position: s[this.isHorizontal() ? "currentX" : "currentY"],
                                time: a.now()
                            })), this.updateProgress(i.currentTranslate), this.setTranslate(i.currentTranslate))
                        }
                    }
                }
            },
            T = function (t) {
                var e = this,
                    i = e.touchEventsData,
                    n = e.params,
                    s = e.touches,
                    r = e.rtl,
                    o = e.$wrapperEl,
                    l = e.slidesGrid,
                    h = e.snapGrid,
                    d = t;
                if (d.originalEvent && (d = d.originalEvent), i.allowTouchCallbacks && e.emit("touchEnd", d), i.allowTouchCallbacks = !1, i.isTouched) {
                    n.grabCursor && i.isMoved && i.isTouched && (!0 === e.allowSlideNext || !0 === e.allowSlidePrev) && e.setGrabCursor(!1);
                    var c = a.now(),
                        u = c - i.touchStartTime;
                    if (e.allowClick && (e.updateClickedSlide(d), e.emit("tap", d), u < 300 && c - i.lastClickTime > 300 && (i.clickTimeout && clearTimeout(i.clickTimeout), i.clickTimeout = a.nextTick(function () {
                            e && !e.destroyed && e.emit("click", d)
                        }, 300)), u < 300 && c - i.lastClickTime < 300 && (i.clickTimeout && clearTimeout(i.clickTimeout), e.emit("doubleTap", d))), i.lastClickTime = a.now(), a.nextTick(function () {
                            e.destroyed || (e.allowClick = !0)
                        }), !i.isTouched || !i.isMoved || !e.swipeDirection || 0 === s.diff || i.currentTranslate === i.startTranslate) return i.isTouched = !1, void(i.isMoved = !1);
                    i.isTouched = !1, i.isMoved = !1;
                    var p;
                    if (p = n.followFinger ? r ? e.translate : -e.translate : -i.currentTranslate, n.freeMode) {
                        if (p < -e.minTranslate()) return void e.slideTo(e.activeIndex);
                        if (p > -e.maxTranslate()) return void(e.slides.length < h.length ? e.slideTo(h.length - 1) : e.slideTo(e.slides.length - 1));
                        if (n.freeModeMomentum) {
                            if (i.velocities.length > 1) {
                                var f = i.velocities.pop(),
                                    m = i.velocities.pop(),
                                    g = f.position - m.position,
                                    v = f.time - m.time;
                                e.velocity = g / v, e.velocity /= 2, Math.abs(e.velocity) < n.freeModeMinimumVelocity && (e.velocity = 0), (v > 150 || a.now() - f.time > 300) && (e.velocity = 0)
                            } else e.velocity = 0;
                            e.velocity *= n.freeModeMomentumVelocityRatio, i.velocities.length = 0;
                            var y = 1e3 * n.freeModeMomentumRatio,
                                b = e.velocity * y,
                                w = e.translate + b;
                            r && (w = -w);
                            var x, C = !1,
                                T = 20 * Math.abs(e.velocity) * n.freeModeMomentumBounceRatio;
                            if (w < e.maxTranslate()) n.freeModeMomentumBounce ? (w + e.maxTranslate() < -T && (w = e.maxTranslate() - T), x = e.maxTranslate(), C = !0, i.allowMomentumBounce = !0) : w = e.maxTranslate();
                            else if (w > e.minTranslate()) n.freeModeMomentumBounce ? (w - e.minTranslate() > T && (w = e.minTranslate() + T), x = e.minTranslate(), C = !0, i.allowMomentumBounce = !0) : w = e.minTranslate();
                            else if (n.freeModeSticky) {
                                for (var E, S = 0; S < h.length; S += 1)
                                    if (h[S] > -w) {
                                        E = S;
                                        break
                                    }
                                w = -(w = Math.abs(h[E] - w) < Math.abs(h[E - 1] - w) || "next" === e.swipeDirection ? h[E] : h[E - 1])
                            }
                            if (0 !== e.velocity) y = r ? Math.abs((-w - e.translate) / e.velocity) : Math.abs((w - e.translate) / e.velocity);
                            else if (n.freeModeSticky) return void e.slideReset();
                            n.freeModeMomentumBounce && C ? (e.updateProgress(x), e.setTransition(y), e.setTranslate(w), e.transitionStart(), e.animating = !0, o.transitionEnd(function () {
                                e && !e.destroyed && i.allowMomentumBounce && (e.emit("momentumBounce"), e.setTransition(n.speed), e.setTranslate(x), o.transitionEnd(function () {
                                    e && !e.destroyed && e.transitionEnd()
                                }))
                            })) : e.velocity ? (e.updateProgress(w), e.setTransition(y), e.setTranslate(w), e.transitionStart(), e.animating || (e.animating = !0, o.transitionEnd(function () {
                                e && !e.destroyed && e.transitionEnd()
                            }))) : e.updateProgress(w), e.updateActiveIndex(), e.updateSlidesClasses()
                        }(!n.freeModeMomentum || u >= n.longSwipesMs) && (e.updateProgress(), e.updateActiveIndex(), e.updateSlidesClasses())
                    } else {
                        for (var _ = 0, k = e.slidesSizesGrid[0], $ = 0; $ < l.length; $ += n.slidesPerGroup) void 0 !== l[$ + n.slidesPerGroup] ? p >= l[$] && p < l[$ + n.slidesPerGroup] && (_ = $, k = l[$ + n.slidesPerGroup] - l[$]) : p >= l[$] && (_ = $, k = l[l.length - 1] - l[l.length - 2]);
                        var z = (p - l[_]) / k;
                        if (u > n.longSwipesMs) {
                            if (!n.longSwipes) return void e.slideTo(e.activeIndex);
                            "next" === e.swipeDirection && (z >= n.longSwipesRatio ? e.slideTo(_ + n.slidesPerGroup) : e.slideTo(_)), "prev" === e.swipeDirection && (z > 1 - n.longSwipesRatio ? e.slideTo(_ + n.slidesPerGroup) : e.slideTo(_))
                        } else {
                            if (!n.shortSwipes) return void e.slideTo(e.activeIndex);
                            "next" === e.swipeDirection && e.slideTo(_ + n.slidesPerGroup), "prev" === e.swipeDirection && e.slideTo(_)
                        }
                    }
                }
            },
            E = function () {
                var t = this.params,
                    e = this.el;
                if (!e || 0 !== e.offsetWidth) {
                    t.breakpoints && this.setBreakpoint();
                    var i = this.allowSlideNext,
                        n = this.allowSlidePrev;
                    if (this.allowSlideNext = !0, this.allowSlidePrev = !0, this.updateSize(), this.updateSlides(), t.freeMode) {
                        var s = Math.min(Math.max(this.translate, this.maxTranslate()), this.minTranslate());
                        this.setTranslate(s), this.updateActiveIndex(), this.updateSlidesClasses(), t.autoHeight && this.updateAutoHeight()
                    } else this.updateSlidesClasses(), ("auto" === t.slidesPerView || t.slidesPerView > 1) && this.isEnd && !this.params.centeredSlides ? this.slideTo(this.slides.length - 1, 0, !1, !0) : this.slideTo(this.activeIndex, 0, !1, !0);
                    this.allowSlidePrev = n, this.allowSlideNext = i
                }
            },
            S = function (t) {
                this.allowClick || (this.params.preventClicks && t.preventDefault(), this.params.preventClicksPropagation && this.animating && (t.stopPropagation(), t.stopImmediatePropagation()))
            },
            _ = {
                init: !0,
                direction: "horizontal",
                touchEventsTarget: "container",
                initialSlide: 0,
                speed: 300,
                iOSEdgeSwipeDetection: !1,
                iOSEdgeSwipeThreshold: 20,
                freeMode: !1,
                freeModeMomentum: !0,
                freeModeMomentumRatio: 1,
                freeModeMomentumBounce: !0,
                freeModeMomentumBounceRatio: 1,
                freeModeMomentumVelocityRatio: 1,
                freeModeSticky: !1,
                freeModeMinimumVelocity: .02,
                autoHeight: !1,
                setWrapperSize: !1,
                virtualTranslate: !1,
                effect: "slide",
                breakpoints: void 0,
                spaceBetween: 0,
                slidesPerView: 1,
                slidesPerColumn: 1,
                slidesPerColumnFill: "column",
                slidesPerGroup: 1,
                centeredSlides: !1,
                slidesOffsetBefore: 0,
                slidesOffsetAfter: 0,
                normalizeSlideIndex: !0,
                roundLengths: !1,
                touchRatio: 1,
                touchAngle: 45,
                simulateTouch: !0,
                shortSwipes: !0,
                longSwipes: !0,
                longSwipesRatio: .5,
                longSwipesMs: 300,
                followFinger: !0,
                allowTouchMove: !0,
                threshold: 0,
                touchMoveStopPropagation: !0,
                touchReleaseOnEdges: !1,
                uniqueNavElements: !0,
                resistance: !0,
                resistanceRatio: .85,
                watchSlidesProgress: !1,
                watchSlidesVisibility: !1,
                grabCursor: !1,
                preventClicks: !0,
                preventClicksPropagation: !0,
                slideToClickedSlide: !1,
                preloadImages: !0,
                updateOnImagesReady: !0,
                loop: !1,
                loopAdditionalSlides: 0,
                loopedSlides: null,
                loopFillGroupWithBlank: !1,
                allowSlidePrev: !0,
                allowSlideNext: !0,
                swipeHandler: null,
                noSwiping: !0,
                noSwipingClass: "swiper-no-swiping",
                passiveListeners: !0,
                containerModifierClass: "swiper-container-",
                slideClass: "swiper-slide",
                slideBlankClass: "swiper-slide-invisible-blank",
                slideActiveClass: "swiper-slide-active",
                slideDuplicateActiveClass: "swiper-slide-duplicate-active",
                slideVisibleClass: "swiper-slide-visible",
                slideDuplicateClass: "swiper-slide-duplicate",
                slideNextClass: "swiper-slide-next",
                slideDuplicateNextClass: "swiper-slide-duplicate-next",
                slidePrevClass: "swiper-slide-prev",
                slideDuplicatePrevClass: "swiper-slide-duplicate-prev",
                wrapperClass: "swiper-wrapper",
                runCallbacksOnInit: !0
            },
            k = {
                update: u,
                translate: p,
                transition: f,
                slide: g,
                loop: v,
                grabCursor: y,
                manipulation: b,
                events: {
                    attachEvents: function () {
                        var t = this.params,
                            e = this.touchEvents,
                            i = this.el,
                            n = this.wrapperEl;
                        this.onTouchStart = x.bind(this), this.onTouchMove = C.bind(this), this.onTouchEnd = T.bind(this), this.onClick = S.bind(this);
                        var s = "container" === t.touchEventsTarget ? i : n,
                            r = !!t.nested;
                        if (m.ie) s.addEventListener(e.start, this.onTouchStart, !1), (h.touch ? s : l).addEventListener(e.move, this.onTouchMove, r), (h.touch ? s : l).addEventListener(e.end, this.onTouchEnd, !1);
                        else {
                            if (h.touch) {
                                var o = !("touchstart" !== e.start || !h.passiveListener || !t.passiveListeners) && {
                                    passive: !0,
                                    capture: !1
                                };
                                s.addEventListener(e.start, this.onTouchStart, o), s.addEventListener(e.move, this.onTouchMove, h.passiveListener ? {
                                    passive: !1,
                                    capture: r
                                } : r), s.addEventListener(e.end, this.onTouchEnd, o)
                            }(t.simulateTouch && !w.ios && !w.android || t.simulateTouch && !h.touch && w.ios) && (s.addEventListener("mousedown", this.onTouchStart, !1), l.addEventListener("mousemove", this.onTouchMove, r), l.addEventListener("mouseup", this.onTouchEnd, !1))
                        }(t.preventClicks || t.preventClicksPropagation) && s.addEventListener("click", this.onClick, !0), this.on("resize observerUpdate", E)
                    },
                    detachEvents: function () {
                        var t = this.params,
                            e = this.touchEvents,
                            i = this.el,
                            n = this.wrapperEl,
                            s = "container" === t.touchEventsTarget ? i : n,
                            r = !!t.nested;
                        if (m.ie) s.removeEventListener(e.start, this.onTouchStart, !1), (h.touch ? s : l).removeEventListener(e.move, this.onTouchMove, r), (h.touch ? s : l).removeEventListener(e.end, this.onTouchEnd, !1);
                        else {
                            if (h.touch) {
                                var o = !("onTouchStart" !== e.start || !h.passiveListener || !t.passiveListeners) && {
                                    passive: !0,
                                    capture: !1
                                };
                                s.removeEventListener(e.start, this.onTouchStart, o), s.removeEventListener(e.move, this.onTouchMove, r), s.removeEventListener(e.end, this.onTouchEnd, o)
                            }(t.simulateTouch && !w.ios && !w.android || t.simulateTouch && !h.touch && w.ios) && (s.removeEventListener("mousedown", this.onTouchStart, !1), l.removeEventListener("mousemove", this.onTouchMove, r), l.removeEventListener("mouseup", this.onTouchEnd, !1))
                        }(t.preventClicks || t.preventClicksPropagation) && s.removeEventListener("click", this.onClick, !0), this.off("resize observerUpdate", E)
                    }
                },
                breakpoints: {
                    setBreakpoint: function () {
                        var t = this.activeIndex,
                            e = this.loopedSlides;
                        void 0 === e && (e = 0);
                        var i = this.params,
                            n = i.breakpoints;
                        if (n && (!n || 0 !== Object.keys(n).length)) {
                            var s = this.getBreakpoint(n);
                            if (s && this.currentBreakpoint !== s) {
                                var r = s in n ? n[s] : this.originalParams,
                                    o = i.loop && r.slidesPerView !== i.slidesPerView;
                                a.extend(this.params, r), a.extend(this, {
                                    allowTouchMove: this.params.allowTouchMove,
                                    allowSlideNext: this.params.allowSlideNext,
                                    allowSlidePrev: this.params.allowSlidePrev
                                }), this.currentBreakpoint = s, o && (this.loopDestroy(), this.loopCreate(), this.updateSlides(), this.slideTo(t - e + this.loopedSlides, 0, !1)), this.emit("breakpoint", r)
                            }
                        }
                    },
                    getBreakpoint: function (t) {
                        if (t) {
                            var e = !1,
                                i = [];
                            Object.keys(t).forEach(function (t) {
                                i.push(t)
                            }), i.sort(function (t, e) {
                                return parseInt(t, 10) - parseInt(e, 10)
                            });
                            for (var s = 0; s < i.length; s += 1) {
                                var r = i[s];
                                r >= n.innerWidth && !e && (e = r)
                            }
                            return e || "max"
                        }
                    }
                },
                classes: {
                    addClasses: function () {
                        var t = this.classNames,
                            e = this.params,
                            i = this.rtl,
                            s = this.$el,
                            r = [];
                        r.push(e.direction), e.freeMode && r.push("free-mode"), h.flexbox || r.push("no-flexbox"), e.autoHeight && r.push("autoheight"), i && r.push("rtl"), e.slidesPerColumn > 1 && r.push("multirow"), w.android && r.push("android"), w.ios && r.push("ios"), (n.navigator.pointerEnabled || n.navigator.msPointerEnabled) && r.push("wp8-" + e.direction), r.forEach(function (i) {
                            t.push(e.containerModifierClass + i)
                        }), s.addClass(t.join(" "))
                    },
                    removeClasses: function () {
                        var t = this.$el,
                            e = this.classNames;
                        t.removeClass(e.join(" "))
                    }
                },
                images: {
                    loadImage: function (t, e, i, s, r, o) {
                        function a() {
                            o && o()
                        }
                        var l;
                        t.complete && r ? a() : e ? ((l = new n.Image).onload = a, l.onerror = a, s && (l.sizes = s), i && (l.srcset = i), e && (l.src = e)) : a()
                    },
                    preloadImages: function () {
                        function t() {
                            void 0 !== e && null !== e && e && !e.destroyed && (void 0 !== e.imagesLoaded && (e.imagesLoaded += 1), e.imagesLoaded === e.imagesToLoad.length && (e.params.updateOnImagesReady && e.update(), e.emit("imagesReady")))
                        }
                        var e = this;
                        e.imagesToLoad = e.$el.find("img");
                        for (var i = 0; i < e.imagesToLoad.length; i += 1) {
                            var n = e.imagesToLoad[i];
                            e.loadImage(n, n.currentSrc || n.getAttribute("src"), n.srcset || n.getAttribute("srcset"), n.sizes || n.getAttribute("sizes"), !0, t)
                        }
                    }
                }
            },
            $ = {},
            z = function (e) {
                function i() {
                    for (var s = [], r = arguments.length; r--;) s[r] = arguments[r];
                    var o, l;
                    if (1 === s.length && s[0].constructor && s[0].constructor === Object) l = s[0];
                    else {
                        var d;
                        o = (d = s)[0], l = d[1]
                    }
                    l || (l = {}), l = a.extend({}, l), o && !l.el && (l.el = o), e.call(this, l), Object.keys(k).forEach(function (t) {
                        Object.keys(k[t]).forEach(function (e) {
                            i.prototype[e] || (i.prototype[e] = k[t][e])
                        })
                    });
                    var c = this;
                    void 0 === c.modules && (c.modules = {}), Object.keys(c.modules).forEach(function (t) {
                        var e = c.modules[t];
                        if (e.params) {
                            var i = Object.keys(e.params)[0],
                                n = e.params[i];
                            if ("object" != typeof n) return;
                            if (!(i in l && "enabled" in n)) return;
                            !0 === l[i] && (l[i] = {
                                enabled: !0
                            }), "object" != typeof l[i] || "enabled" in l[i] || (l[i].enabled = !0), l[i] || (l[i] = {
                                enabled: !1
                            })
                        }
                    });
                    var u = a.extend({}, _);
                    c.useModulesParams(u), c.params = a.extend({}, u, $, l), c.originalParams = a.extend({}, c.params), c.passedParams = a.extend({}, l);
                    var p = t(c.params.el);
                    if (o = p[0]) {
                        if (p.length > 1) {
                            var f = [];
                            return p.each(function (t, e) {
                                var n = a.extend({}, l, {
                                    el: e
                                });
                                f.push(new i(n))
                            }), f
                        }
                        o.swiper = c, p.data("swiper", c);
                        var m = p.children("." + c.params.wrapperClass);
                        return a.extend(c, {
                            $el: p,
                            el: o,
                            $wrapperEl: m,
                            wrapperEl: m[0],
                            classNames: [],
                            slides: t(),
                            slidesGrid: [],
                            snapGrid: [],
                            slidesSizesGrid: [],
                            isHorizontal: function () {
                                return "horizontal" === c.params.direction
                            },
                            isVertical: function () {
                                return "vertical" === c.params.direction
                            },
                            rtl: "horizontal" === c.params.direction && ("rtl" === o.dir.toLowerCase() || "rtl" === p.css("direction")),
                            wrongRTL: "-webkit-box" === m.css("display"),
                            activeIndex: 0,
                            realIndex: 0,
                            isBeginning: !0,
                            isEnd: !1,
                            translate: 0,
                            progress: 0,
                            velocity: 0,
                            animating: !1,
                            allowSlideNext: c.params.allowSlideNext,
                            allowSlidePrev: c.params.allowSlidePrev,
                            touchEvents: function () {
                                var t = ["touchstart", "touchmove", "touchend"],
                                    e = ["mousedown", "mousemove", "mouseup"];
                                return n.navigator.pointerEnabled ? e = ["pointerdown", "pointermove", "pointerup"] : n.navigator.msPointerEnabled && (e = ["MSPointerDown", "MsPointerMove", "MsPointerUp"]), {
                                    start: h.touch || !c.params.simulateTouch ? t[0] : e[0],
                                    move: h.touch || !c.params.simulateTouch ? t[1] : e[1],
                                    end: h.touch || !c.params.simulateTouch ? t[2] : e[2]
                                }
                            }(),
                            touchEventsData: {
                                isTouched: void 0,
                                isMoved: void 0,
                                allowTouchCallbacks: void 0,
                                touchStartTime: void 0,
                                isScrolling: void 0,
                                currentTranslate: void 0,
                                startTranslate: void 0,
                                allowThresholdMove: void 0,
                                formElements: "input, select, option, textarea, button, video",
                                lastClickTime: a.now(),
                                clickTimeout: void 0,
                                velocities: [],
                                allowMomentumBounce: void 0,
                                isTouchEvent: void 0,
                                startMoving: void 0
                            },
                            allowClick: !0,
                            allowTouchMove: c.params.allowTouchMove,
                            touches: {
                                startX: 0,
                                startY: 0,
                                currentX: 0,
                                currentY: 0,
                                diff: 0
                            },
                            imagesToLoad: [],
                            imagesLoaded: 0
                        }), c.useModules(), c.params.init && c.init(), c
                    }
                }
                e && (i.__proto__ = e), (i.prototype = Object.create(e && e.prototype)).constructor = i;
                var s = {
                    extendedDefaults: {},
                    defaults: {},
                    Class: {},
                    $: {}
                };
                return i.prototype.slidesPerViewDynamic = function () {
                    var t = this.params,
                        e = this.slides,
                        i = this.slidesGrid,
                        n = this.size,
                        s = this.activeIndex,
                        r = 1;
                    if (t.centeredSlides) {
                        for (var o, a = e[s].swiperSlideSize, l = s + 1; l < e.length; l += 1) e[l] && !o && (r += 1, (a += e[l].swiperSlideSize) > n && (o = !0));
                        for (var h = s - 1; h >= 0; h -= 1) e[h] && !o && (r += 1, (a += e[h].swiperSlideSize) > n && (o = !0))
                    } else
                        for (var d = s + 1; d < e.length; d += 1) i[d] - i[s] < n && (r += 1);
                    return r
                }, i.prototype.update = function () {
                    function t() {
                        i = Math.min(Math.max(e.translate, e.maxTranslate()), e.minTranslate()), e.setTranslate(i), e.updateActiveIndex(), e.updateSlidesClasses()
                    }
                    var e = this;
                    if (e && !e.destroyed) {
                        e.updateSize(), e.updateSlides(), e.updateProgress(), e.updateSlidesClasses();
                        var i;
                        e.params.freeMode ? (t(), e.params.autoHeight && e.updateAutoHeight()) : (("auto" === e.params.slidesPerView || e.params.slidesPerView > 1) && e.isEnd && !e.params.centeredSlides ? e.slideTo(e.slides.length - 1, 0, !1, !0) : e.slideTo(e.activeIndex, 0, !1, !0)) || t(), e.emit("update")
                    }
                }, i.prototype.init = function () {
                    this.initialized || (this.emit("beforeInit"), this.params.breakpoints && this.setBreakpoint(), this.addClasses(), this.params.loop && this.loopCreate(), this.updateSize(), this.updateSlides(), this.params.grabCursor && this.setGrabCursor(), this.params.preloadImages && this.preloadImages(), this.params.loop ? this.slideTo(this.params.initialSlide + this.loopedSlides, 0, this.params.runCallbacksOnInit) : this.slideTo(this.params.initialSlide, 0, this.params.runCallbacksOnInit), this.attachEvents(), this.initialized = !0, this.emit("init"))
                }, i.prototype.destroy = function (t, e) {
                    void 0 === t && (t = !0), void 0 === e && (e = !0);
                    var i = this,
                        n = i.params,
                        s = i.$el,
                        r = i.$wrapperEl,
                        o = i.slides;
                    i.emit("beforeDestroy"), i.initialized = !1, i.detachEvents(), n.loop && i.loopDestroy(), e && (i.removeClasses(), s.removeAttr("style"), r.removeAttr("style"), o && o.length && o.removeClass([n.slideVisibleClass, n.slideActiveClass, n.slideNextClass, n.slidePrevClass].join(" ")).removeAttr("style").removeAttr("data-swiper-slide-index").removeAttr("data-swiper-column").removeAttr("data-swiper-row")), i.emit("destroy"), Object.keys(i.eventsListeners).forEach(function (t) {
                        i.off(t)
                    }), !1 !== t && (i.$el[0].swiper = null, i.$el.data("swiper", null), a.deleteProps(i)), i.destroyed = !0
                }, i.extendDefaults = function (t) {
                    a.extend($, t)
                }, s.extendedDefaults.get = function () {
                    return $
                }, s.defaults.get = function () {
                    return _
                }, s.Class.get = function () {
                    return e
                }, s.$.get = function () {
                    return t
                }, Object.defineProperties(i, s), i
            }(d),
            M = {
                name: "device",
                proto: {
                    device: w
                },
                static: {
                    device: w
                }
            },
            D = {
                name: "support",
                proto: {
                    support: h
                },
                static: {
                    support: h
                }
            },
            I = {
                name: "browser",
                proto: {
                    browser: m
                },
                static: {
                    browser: m
                }
            },
            P = {
                name: "resize",
                create: function () {
                    var t = this;
                    a.extend(t, {
                        resize: {
                            resizeHandler: function () {
                                t && !t.destroyed && t.initialized && (t.emit("beforeResize"), t.emit("resize"))
                            },
                            orientationChangeHandler: function () {
                                t && !t.destroyed && t.initialized && t.emit("orientationchange")
                            }
                        }
                    })
                },
                on: {
                    init: function () {
                        n.addEventListener("resize", this.resize.resizeHandler), n.addEventListener("orientationchange", this.resize.orientationChangeHandler)
                    },
                    destroy: function () {
                        n.removeEventListener("resize", this.resize.resizeHandler), n.removeEventListener("orientationchange", this.resize.orientationChangeHandler)
                    }
                }
            },
            N = {
                func: n.MutationObserver || n.WebkitMutationObserver,
                attach: function (t, e) {
                    void 0 === e && (e = {});
                    var i = this,
                        n = new(0, N.func)(function (t) {
                            t.forEach(function (t) {
                                i.emit("observerUpdate", t)
                            })
                        });
                    n.observe(t, {
                        attributes: void 0 === e.attributes || e.attributes,
                        childList: void 0 === e.childList || e.childList,
                        characterData: void 0 === e.characterData || e.characterData
                    }), i.observer.observers.push(n)
                },
                init: function () {
                    if (h.observer && this.params.observer) {
                        if (this.params.observeParents)
                            for (var t = this.$el.parents(), e = 0; e < t.length; e += 1) this.observer.attach(t[e]);
                        this.observer.attach(this.$el[0], {
                            childList: !1
                        }), this.observer.attach(this.$wrapperEl[0], {
                            attributes: !1
                        })
                    }
                },
                destroy: function () {
                    this.observer.observers.forEach(function (t) {
                        t.disconnect()
                    }), this.observer.observers = []
                }
            },
            L = {
                name: "observer",
                params: {
                    observer: !1,
                    observeParents: !1
                },
                create: function () {
                    a.extend(this, {
                        observer: {
                            init: N.init.bind(this),
                            attach: N.attach.bind(this),
                            destroy: N.destroy.bind(this),
                            observers: []
                        }
                    })
                },
                on: {
                    init: function () {
                        this.observer.init()
                    },
                    destroy: function () {
                        this.observer.destroy()
                    }
                }
            },
            O = {
                update: function (t) {
                    function e() {
                        i.updateSlides(), i.updateProgress(), i.updateSlidesClasses(), i.lazy && i.params.lazy.enabled && i.lazy.load()
                    }
                    var i = this,
                        n = i.params,
                        s = n.slidesPerView,
                        r = n.slidesPerGroup,
                        o = n.centeredSlides,
                        l = i.virtual,
                        h = l.from,
                        d = l.to,
                        c = l.slides,
                        u = l.slidesGrid,
                        p = l.renderSlide,
                        f = l.offset;
                    i.updateActiveIndex();
                    var m, g = i.activeIndex || 0;
                    m = i.rtl && i.isHorizontal() ? "right" : i.isHorizontal() ? "left" : "top";
                    var v, y;
                    o ? (v = Math.floor(s / 2) + r, y = Math.floor(s / 2) + r) : (v = s + (r - 1), y = r);
                    var b = Math.max((g || 0) - y, 0),
                        w = Math.min((g || 0) + v, c.length - 1),
                        x = (i.slidesGrid[b] || 0) - (i.slidesGrid[0] || 0);
                    if (a.extend(i.virtual, {
                            from: b,
                            to: w,
                            offset: x,
                            slidesGrid: i.slidesGrid
                        }), h === b && d === w && !t) return i.slidesGrid !== u && x !== f && i.slides.css(m, x + "px"), void i.updateProgress();
                    if (i.params.virtual.renderExternal) return i.params.virtual.renderExternal.call(i, {
                        offset: x,
                        from: b,
                        to: w,
                        slides: function () {
                            for (var t = [], e = b; e <= w; e += 1) t.push(c[e]);
                            return t
                        }()
                    }), void e();
                    var C = [],
                        T = [];
                    if (t) i.$wrapperEl.find("." + i.params.slideClass).remove();
                    else
                        for (var E = h; E <= d; E += 1)(E < b || E > w) && i.$wrapperEl.find("." + i.params.slideClass + '[data-swiper-slide-index="' + E + '"]').remove();
                    for (var S = 0; S < c.length; S += 1) S >= b && S <= w && (void 0 === d || t ? T.push(S) : (S > d && T.push(S), S < h && C.push(S)));
                    T.forEach(function (t) {
                        i.$wrapperEl.append(p(c[t], t))
                    }), C.sort(function (t, e) {
                        return t < e
                    }).forEach(function (t) {
                        i.$wrapperEl.prepend(p(c[t], t))
                    }), i.$wrapperEl.children(".swiper-slide").css(m, x + "px"), e()
                },
                renderSlide: function (e, i) {
                    var n = this.params.virtual;
                    if (n.cache && this.virtual.cache[i]) return this.virtual.cache[i];
                    var s = t(n.renderSlide ? n.renderSlide.call(this, e, i) : '<div class="' + this.params.slideClass + '" data-swiper-slide-index="' + i + '">' + e + "</div>");
                    return s.attr("data-swiper-slide-index") || s.attr("data-swiper-slide-index", i), n.cache && (this.virtual.cache[i] = s), s
                },
                appendSlide: function (t) {
                    this.virtual.slides.push(t), this.virtual.update(!0)
                },
                prependSlide: function (t) {
                    if (this.virtual.slides.unshift(t), this.params.virtual.cache) {
                        var e = this.virtual.cache,
                            i = {};
                        Object.keys(e).forEach(function (t) {
                            i[t + 1] = e[t]
                        }), this.virtual.cache = i
                    }
                    this.virtual.update(!0), this.slideNext(0)
                }
            },
            A = {
                name: "virtual",
                params: {
                    virtual: {
                        enabled: !1,
                        slides: [],
                        cache: !0,
                        renderSlide: null,
                        renderExternal: null
                    }
                },
                create: function () {
                    a.extend(this, {
                        virtual: {
                            update: O.update.bind(this),
                            appendSlide: O.appendSlide.bind(this),
                            prependSlide: O.prependSlide.bind(this),
                            renderSlide: O.renderSlide.bind(this),
                            slides: this.params.virtual.slides,
                            cache: {}
                        }
                    })
                },
                on: {
                    beforeInit: function () {
                        if (this.params.virtual.enabled) {
                            this.classNames.push(this.params.containerModifierClass + "virtual");
                            var t = {
                                watchSlidesProgress: !0
                            };
                            a.extend(this.params, t), a.extend(this.originalParams, t), this.virtual.update()
                        }
                    },
                    setTranslate: function () {
                        this.params.virtual.enabled && this.virtual.update()
                    }
                }
            },
            j = {
                handle: function (t) {
                    var e = t;
                    e.originalEvent && (e = e.originalEvent);
                    var i = e.keyCode || e.charCode;
                    if (!this.allowSlideNext && (this.isHorizontal() && 39 === i || this.isVertical() && 40 === i)) return !1;
                    if (!this.allowSlidePrev && (this.isHorizontal() && 37 === i || this.isVertical() && 38 === i)) return !1;
                    if (!(e.shiftKey || e.altKey || e.ctrlKey || e.metaKey || l.activeElement && l.activeElement.nodeName && ("input" === l.activeElement.nodeName.toLowerCase() || "textarea" === l.activeElement.nodeName.toLowerCase()))) {
                        if (37 === i || 39 === i || 38 === i || 40 === i) {
                            var s = !1;
                            if (this.$el.parents("." + this.params.slideClass).length > 0 && 0 === this.$el.parents("." + this.params.slideActiveClass).length) return;
                            var r = {
                                    left: n.pageXOffset,
                                    top: n.pageYOffset
                                },
                                o = n.innerWidth,
                                a = n.innerHeight,
                                h = this.$el.offset();
                            this.rtl && (h.left -= this.$el[0].scrollLeft);
                            for (var d = [[h.left, h.top], [h.left + this.width, h.top], [h.left, h.top + this.height], [h.left + this.width, h.top + this.height]], c = 0; c < d.length; c += 1) {
                                var u = d[c];
                                u[0] >= r.left && u[0] <= r.left + o && u[1] >= r.top && u[1] <= r.top + a && (s = !0)
                            }
                            if (!s) return
                        }
                        this.isHorizontal() ? (37 !== i && 39 !== i || (e.preventDefault ? e.preventDefault() : e.returnValue = !1), (39 === i && !this.rtl || 37 === i && this.rtl) && this.slideNext(), (37 === i && !this.rtl || 39 === i && this.rtl) && this.slidePrev()) : (38 !== i && 40 !== i || (e.preventDefault ? e.preventDefault() : e.returnValue = !1), 40 === i && this.slideNext(), 38 === i && this.slidePrev()), this.emit("keyPress", i)
                    }
                },
                enable: function () {
                    this.keyboard.enabled || (t(l).on("keydown", this.keyboard.handle), this.keyboard.enabled = !0)
                },
                disable: function () {
                    this.keyboard.enabled && (t(l).off("keydown", this.keyboard.handle), this.keyboard.enabled = !1)
                }
            },
            H = {
                name: "keyboard",
                params: {
                    keyboard: {
                        enabled: !1
                    }
                },
                create: function () {
                    a.extend(this, {
                        keyboard: {
                            enabled: !1,
                            enable: j.enable.bind(this),
                            disable: j.disable.bind(this),
                            handle: j.handle.bind(this)
                        }
                    })
                },
                on: {
                    init: function () {
                        this.params.keyboard.enabled && this.keyboard.enable()
                    },
                    destroy: function () {
                        this.keyboard.enabled && this.keyboard.disable()
                    }
                }
            },
            R = {
                lastScrollTime: a.now(),
                event: n.navigator.userAgent.indexOf("firefox") > -1 ? "DOMMouseScroll" : function () {
                    var t = "onwheel" in l;
                    if (!t) {
                        var e = l.createElement("div");
                        e.setAttribute("onwheel", "return;"), t = "function" == typeof e.onwheel
                    }
                    return !t && l.implementation && l.implementation.hasFeature && !0 !== l.implementation.hasFeature("", "") && (t = l.implementation.hasFeature("Events.wheel", "3.0")), t
                }() ? "wheel" : "mousewheel",
                normalize: function (t) {
                    var e = 0,
                        i = 0,
                        n = 0,
                        s = 0;
                    return "detail" in t && (i = t.detail), "wheelDelta" in t && (i = -t.wheelDelta / 120), "wheelDeltaY" in t && (i = -t.wheelDeltaY / 120), "wheelDeltaX" in t && (e = -t.wheelDeltaX / 120), "axis" in t && t.axis === t.HORIZONTAL_AXIS && (e = i, i = 0), n = 10 * e, s = 10 * i, "deltaY" in t && (s = t.deltaY), "deltaX" in t && (n = t.deltaX), (n || s) && t.deltaMode && (1 === t.deltaMode ? (n *= 40, s *= 40) : (n *= 800, s *= 800)), n && !e && (e = n < 1 ? -1 : 1), s && !i && (i = s < 1 ? -1 : 1), {
                        spinX: e,
                        spinY: i,
                        pixelX: n,
                        pixelY: s
                    }
                },
                handle: function (t) {
                    var e = t,
                        i = this,
                        s = i.params.mousewheel;
                    e.originalEvent && (e = e.originalEvent);
                    var r = 0,
                        o = i.rtl ? -1 : 1,
                        l = R.normalize(e);
                    if (s.forceToAxis)
                        if (i.isHorizontal()) {
                            if (!(Math.abs(l.pixelX) > Math.abs(l.pixelY))) return !0;
                            r = l.pixelX * o
                        } else {
                            if (!(Math.abs(l.pixelY) > Math.abs(l.pixelX))) return !0;
                            r = l.pixelY
                        }
                    else r = Math.abs(l.pixelX) > Math.abs(l.pixelY) ? -l.pixelX * o : -l.pixelY;
                    if (0 === r) return !0;
                    if (s.invert && (r = -r), i.params.freeMode) {
                        var h = i.getTranslate() + r * s.sensitivity,
                            d = i.isBeginning,
                            c = i.isEnd;
                        if (h >= i.minTranslate() && (h = i.minTranslate()), h <= i.maxTranslate() && (h = i.maxTranslate()), i.setTransition(0), i.setTranslate(h), i.updateProgress(), i.updateActiveIndex(), i.updateSlidesClasses(), (!d && i.isBeginning || !c && i.isEnd) && i.updateSlidesClasses(), i.params.freeModeSticky && (clearTimeout(i.mousewheel.timeout), i.mousewheel.timeout = a.nextTick(function () {
                                i.slideReset()
                            }, 300)), i.emit("scroll", e), i.params.autoplay && i.params.autoplayDisableOnInteraction && i.stopAutoplay(), 0 === h || h === i.maxTranslate()) return !0
                    } else {
                        if (a.now() - i.mousewheel.lastScrollTime > 60)
                            if (r < 0)
                                if (i.isEnd && !i.params.loop || i.animating) {
                                    if (s.releaseOnEdges) return !0
                                } else i.slideNext(), i.emit("scroll", e);
                        else if (i.isBeginning && !i.params.loop || i.animating) {
                            if (s.releaseOnEdges) return !0
                        } else i.slidePrev(), i.emit("scroll", e);
                        i.mousewheel.lastScrollTime = (new n.Date).getTime()
                    }
                    return e.preventDefault ? e.preventDefault() : e.returnValue = !1, !1
                },
                enable: function () {
                    if (!R.event) return !1;
                    if (this.mousewheel.enabled) return !1;
                    var e = this.$el;
                    return "container" !== this.params.mousewheel.eventsTarged && (e = t(this.params.mousewheel.eventsTarged)), e.on(R.event, this.mousewheel.handle), this.mousewheel.enabled = !0, !0
                },
                disable: function () {
                    if (!R.event) return !1;
                    if (!this.mousewheel.enabled) return !1;
                    var e = this.$el;
                    return "container" !== this.params.mousewheel.eventsTarged && (e = t(this.params.mousewheel.eventsTarged)), e.off(R.event, this.mousewheel.handle), this.mousewheel.enabled = !1, !0
                }
            },
            W = {
                name: "mousewheel",
                params: {
                    mousewheel: {
                        enabled: !1,
                        releaseOnEdges: !1,
                        invert: !1,
                        forceToAxis: !1,
                        sensitivity: 1,
                        eventsTarged: "container"
                    }
                },
                create: function () {
                    a.extend(this, {
                        mousewheel: {
                            enabled: !1,
                            enable: R.enable.bind(this),
                            disable: R.disable.bind(this),
                            handle: R.handle.bind(this),
                            lastScrollTime: a.now()
                        }
                    })
                },
                on: {
                    init: function () {
                        this.params.mousewheel.enabled && this.mousewheel.enable()
                    },
                    destroy: function () {
                        this.mousewheel.enabled && this.mousewheel.disable()
                    }
                }
            },
            B = {
                update: function () {
                    var t = this.params.navigation;
                    if (!this.params.loop) {
                        var e = this.navigation,
                            i = e.$nextEl,
                            n = e.$prevEl;
                        n && n.length > 0 && (this.isBeginning ? n.addClass(t.disabledClass) : n.removeClass(t.disabledClass)), i && i.length > 0 && (this.isEnd ? i.addClass(t.disabledClass) : i.removeClass(t.disabledClass))
                    }
                },
                init: function () {
                    var e = this,
                        i = e.params.navigation;
                    if (i.nextEl || i.prevEl) {
                        var n, s;
                        i.nextEl && (n = t(i.nextEl), e.params.uniqueNavElements && "string" == typeof i.nextEl && n.length > 1 && 1 === e.$el.find(i.nextEl).length && (n = e.$el.find(i.nextEl))), i.prevEl && (s = t(i.prevEl), e.params.uniqueNavElements && "string" == typeof i.prevEl && s.length > 1 && 1 === e.$el.find(i.prevEl).length && (s = e.$el.find(i.prevEl))), n && n.length > 0 && n.on("click", function (t) {
                            t.preventDefault(), e.isEnd && !e.params.loop || e.slideNext()
                        }), s && s.length > 0 && s.on("click", function (t) {
                            t.preventDefault(), e.isBeginning && !e.params.loop || e.slidePrev()
                        }), a.extend(e.navigation, {
                            $nextEl: n,
                            nextEl: n && n[0],
                            $prevEl: s,
                            prevEl: s && s[0]
                        })
                    }
                },
                destroy: function () {
                    var t = this.navigation,
                        e = t.$nextEl,
                        i = t.$prevEl;
                    e && e.length && (e.off("click"), e.removeClass(this.params.navigation.disabledClass)), i && i.length && (i.off("click"), i.removeClass(this.params.navigation.disabledClass))
                }
            },
            q = {
                name: "navigation",
                params: {
                    navigation: {
                        nextEl: null,
                        prevEl: null,
                        hideOnClick: !1,
                        disabledClass: "swiper-button-disabled",
                        hiddenClass: "swiper-button-hidden"
                    }
                },
                create: function () {
                    a.extend(this, {
                        navigation: {
                            init: B.init.bind(this),
                            update: B.update.bind(this),
                            destroy: B.destroy.bind(this)
                        }
                    })
                },
                on: {
                    init: function () {
                        this.navigation.init(), this.navigation.update()
                    },
                    toEdge: function () {
                        this.navigation.update()
                    },
                    fromEdge: function () {
                        this.navigation.update()
                    },
                    destroy: function () {
                        this.navigation.destroy()
                    },
                    click: function (e) {
                        var i = this.navigation,
                            n = i.$nextEl,
                            s = i.$prevEl;
                        !this.params.navigation.hideOnClick || t(e.target).is(s) || t(e.target).is(n) || (n && n.toggleClass(this.params.navigation.hiddenClass), s && s.toggleClass(this.params.navigation.hiddenClass))
                    }
                }
            },
            F = {
                update: function () {
                    var e = this.rtl,
                        i = this.params.pagination;
                    if (i.el && this.pagination.el && this.pagination.$el && 0 !== this.pagination.$el.length) {
                        var n, s = this.virtual && this.params.virtual.enabled ? this.virtual.slides.length : this.slides.length,
                            r = this.pagination.$el,
                            o = this.params.loop ? Math.ceil((s - 2 * this.loopedSlides) / this.params.slidesPerGroup) : this.snapGrid.length;
                        if (this.params.loop ? ((n = Math.ceil((this.activeIndex - this.loopedSlides) / this.params.slidesPerGroup)) > s - 1 - 2 * this.loopedSlides && (n -= s - 2 * this.loopedSlides), n > o - 1 && (n -= o), n < 0 && "bullets" !== this.params.paginationType && (n = o + n)) : n = void 0 !== this.snapIndex ? this.snapIndex : this.activeIndex || 0, "bullets" === i.type && this.pagination.bullets && this.pagination.bullets.length > 0) {
                            var a = this.pagination.bullets;
                            if (i.dynamicBullets && (this.pagination.bulletSize = a.eq(0)[this.isHorizontal() ? "outerWidth" : "outerHeight"](!0), r.css(this.isHorizontal() ? "width" : "height", 5 * this.pagination.bulletSize + "px")), a.removeClass(i.bulletActiveClass + " " + i.bulletActiveClass + "-next " + i.bulletActiveClass + "-next-next " + i.bulletActiveClass + "-prev " + i.bulletActiveClass + "-prev-prev"), r.length > 1) a.each(function (e, s) {
                                var r = t(s);
                                r.index() === n && (r.addClass(i.bulletActiveClass), i.dynamicBullets && (r.prev().addClass(i.bulletActiveClass + "-prev").prev().addClass(i.bulletActiveClass + "-prev-prev"), r.next().addClass(i.bulletActiveClass + "-next").next().addClass(i.bulletActiveClass + "-next-next")))
                            });
                            else {
                                var l = a.eq(n);
                                l.addClass(i.bulletActiveClass), i.dynamicBullets && (l.prev().addClass(i.bulletActiveClass + "-prev").prev().addClass(i.bulletActiveClass + "-prev-prev"), l.next().addClass(i.bulletActiveClass + "-next").next().addClass(i.bulletActiveClass + "-next-next"))
                            }
                            if (i.dynamicBullets) {
                                var h = Math.min(a.length, 5),
                                    d = (this.pagination.bulletSize * h - this.pagination.bulletSize) / 2 - n * this.pagination.bulletSize,
                                    c = e ? "right" : "left";
                                a.css(this.isHorizontal() ? c : "top", d + "px")
                            }
                        }
                        if ("fraction" === i.type && (r.find("." + i.currentClass).text(n + 1), r.find("." + i.totalClass).text(o)), "progressbar" === i.type) {
                            var u = (n + 1) / o,
                                p = u,
                                f = 1;
                            this.isHorizontal() || (f = u, p = 1), r.find("." + i.progressbarFillClass).transform("translate3d(0,0,0) scaleX(" + p + ") scaleY(" + f + ")").transition(this.params.speed)
                        }
                        "custom" === i.type && i.renderCustom ? (r.html(i.renderCustom(this, n + 1, o)), this.emit("paginationRender", this, r[0])) : this.emit("paginationUpdate", this, r[0])
                    }
                },
                render: function () {
                    var t = this.params.pagination;
                    if (t.el && this.pagination.el && this.pagination.$el && 0 !== this.pagination.$el.length) {
                        var e = this.virtual && this.params.virtual.enabled ? this.virtual.slides.length : this.slides.length,
                            i = this.pagination.$el,
                            n = "";
                        if ("bullets" === t.type) {
                            for (var s = this.params.loop ? Math.ceil((e - 2 * this.loopedSlides) / this.params.slidesPerGroup) : this.snapGrid.length, r = 0; r < s; r += 1) t.renderBullet ? n += t.renderBullet.call(this, r, t.bulletClass) : n += "<" + t.bulletElement + ' class="' + t.bulletClass + '"></' + t.bulletElement + ">";
                            i.html(n), this.pagination.bullets = i.find("." + t.bulletClass)
                        }
                        "fraction" === t.type && (n = t.renderFraction ? t.renderFraction.call(this, t.currentClass, t.totalClass) : '<span class="' + t.currentClass + '"></span> / <span class="' + t.totalClass + '"></span>', i.html(n)), "progressbar" === t.type && (n = t.renderProgressbar ? t.renderProgressbar.call(this, t.progressbarFillClass) : '<span class="' + t.progressbarFillClass + '"></span>', i.html(n)), "custom" !== t.type && this.emit("paginationRender", this.pagination.$el[0])
                    }
                },
                init: function () {
                    var e = this,
                        i = e.params.pagination;
                    if (i.el) {
                        var n = t(i.el);
                        0 !== n.length && (e.params.uniqueNavElements && "string" == typeof i.el && n.length > 1 && 1 === e.$el.find(i.el).length && (n = e.$el.find(i.el)), "bullets" === i.type && i.clickable && n.addClass(i.clickableClass), n.addClass(i.modifierClass + i.type), "bullets" === i.type && i.dynamicBullets && n.addClass("" + i.modifierClass + i.type + "-dynamic"), i.clickable && n.on("click", "." + i.bulletClass, function (i) {
                            i.preventDefault();
                            var n = t(this).index() * e.params.slidesPerGroup;
                            e.params.loop && (n += e.loopedSlides), e.slideTo(n)
                        }), a.extend(e.pagination, {
                            $el: n,
                            el: n[0]
                        }))
                    }
                },
                destroy: function () {
                    var t = this.params.pagination;
                    if (t.el && this.pagination.el && this.pagination.$el && 0 !== this.pagination.$el.length) {
                        var e = this.pagination.$el;
                        e.removeClass(t.hiddenClass), e.removeClass(t.modifierClass + t.type), this.pagination.bullets && this.pagination.bullets.removeClass(t.bulletActiveClass), t.clickable && e.off("click", "." + t.bulletClass)
                    }
                }
            },
            U = {
                name: "pagination",
                params: {
                    pagination: {
                        el: null,
                        bulletElement: "span",
                        clickable: !1,
                        hideOnClick: !1,
                        renderBullet: null,
                        renderProgressbar: null,
                        renderFraction: null,
                        renderCustom: null,
                        type: "bullets",
                        dynamicBullets: !1,
                        bulletClass: "swiper-pagination-bullet",
                        bulletActiveClass: "swiper-pagination-bullet-active",
                        modifierClass: "swiper-pagination-",
                        currentClass: "swiper-pagination-current",
                        totalClass: "swiper-pagination-total",
                        hiddenClass: "swiper-pagination-hidden",
                        progressbarFillClass: "swiper-pagination-progressbar-fill",
                        clickableClass: "swiper-pagination-clickable"
                    }
                },
                create: function () {
                    a.extend(this, {
                        pagination: {
                            init: F.init.bind(this),
                            render: F.render.bind(this),
                            update: F.update.bind(this),
                            destroy: F.destroy.bind(this)
                        }
                    })
                },
                on: {
                    init: function () {
                        this.pagination.init(), this.pagination.render(), this.pagination.update()
                    },
                    activeIndexChange: function () {
                        this.params.loop ? this.pagination.update() : void 0 === this.snapIndex && this.pagination.update()
                    },
                    snapIndexChange: function () {
                        this.params.loop || this.pagination.update()
                    },
                    slidesLengthChange: function () {
                        this.params.loop && (this.pagination.render(), this.pagination.update())
                    },
                    snapGridLengthChange: function () {
                        this.params.loop || (this.pagination.render(), this.pagination.update())
                    },
                    destroy: function () {
                        this.pagination.destroy()
                    },
                    click: function (e) {
                        this.params.pagination.el && this.params.pagination.hideOnClick && this.pagination.$el.length > 0 && !t(e.target).hasClass(this.params.pagination.bulletClass) && this.pagination.$el.toggleClass(this.params.pagination.hiddenClass)
                    }
                }
            },
            X = {
                setTranslate: function () {
                    if (this.params.scrollbar.el && this.scrollbar.el) {
                        var t = this.scrollbar,
                            e = this.rtl,
                            i = this.progress,
                            n = t.dragSize,
                            s = t.trackSize,
                            r = t.$dragEl,
                            o = t.$el,
                            a = this.params.scrollbar,
                            l = n,
                            d = (s - n) * i;
                        e && this.isHorizontal() ? (d = -d) > 0 ? (l = n - d, d = 0) : -d + n > s && (l = s + d) : d < 0 ? (l = n + d, d = 0) : d + n > s && (l = s - d), this.isHorizontal() ? (h.transforms3d ? r.transform("translate3d(" + d + "px, 0, 0)") : r.transform("translateX(" + d + "px)"), r[0].style.width = l + "px") : (h.transforms3d ? r.transform("translate3d(0px, " + d + "px, 0)") : r.transform("translateY(" + d + "px)"), r[0].style.height = l + "px"), a.hide && (clearTimeout(this.scrollbar.timeout), o[0].style.opacity = 1, this.scrollbar.timeout = setTimeout(function () {
                            o[0].style.opacity = 0, o.transition(400)
                        }, 1e3))
                    }
                },
                setTransition: function (t) {
                    this.params.scrollbar.el && this.scrollbar.el && this.scrollbar.$dragEl.transition(t)
                },
                updateSize: function () {
                    if (this.params.scrollbar.el && this.scrollbar.el) {
                        var t = this.scrollbar,
                            e = t.$dragEl,
                            i = t.$el;
                        e[0].style.width = "", e[0].style.height = "";
                        var n, s = this.isHorizontal() ? i[0].offsetWidth : i[0].offsetHeight,
                            r = this.size / this.virtualSize,
                            o = r * (s / this.size);
                        n = "auto" === this.params.scrollbar.dragSize ? s * r : parseInt(this.params.scrollbar.dragSize, 10), this.isHorizontal() ? e[0].style.width = n + "px" : e[0].style.height = n + "px", i[0].style.display = r >= 1 ? "none" : "", this.params.scrollbarHide && (i[0].style.opacity = 0), a.extend(t, {
                            trackSize: s,
                            divider: r,
                            moveDivider: o,
                            dragSize: n
                        })
                    }
                },
                setDragPosition: function (t) {
                    var e, i = this.scrollbar,
                        n = i.$el,
                        s = i.dragSize,
                        r = i.trackSize;
                    e = ((this.isHorizontal() ? "touchstart" === t.type || "touchmove" === t.type ? t.targetTouches[0].pageX : t.pageX || t.clientX : "touchstart" === t.type || "touchmove" === t.type ? t.targetTouches[0].pageY : t.pageY || t.clientY) - n.offset()[this.isHorizontal() ? "left" : "top"] - s / 2) / (r - s), e = Math.max(Math.min(e, 1), 0), this.rtl && (e = 1 - e);
                    var o = this.minTranslate() + (this.maxTranslate() - this.minTranslate()) * e;
                    this.updateProgress(o), this.setTranslate(o), this.updateActiveIndex(), this.updateSlidesClasses()
                },
                onDragStart: function (t) {
                    var e = this.params.scrollbar,
                        i = this.scrollbar,
                        n = this.$wrapperEl,
                        s = i.$el,
                        r = i.$dragEl;
                    this.scrollbar.isTouched = !0, t.preventDefault(), t.stopPropagation(), n.transition(100), r.transition(100), i.setDragPosition(t), clearTimeout(this.scrollbar.dragTimeout), s.transition(0), e.hide && s.css("opacity", 1), this.emit("scrollbarDragStart", t)
                },
                onDragMove: function (t) {
                    var e = this.scrollbar,
                        i = this.$wrapperEl,
                        n = e.$el,
                        s = e.$dragEl;
                    this.scrollbar.isTouched && (t.preventDefault ? t.preventDefault() : t.returnValue = !1, e.setDragPosition(t), i.transition(0), n.transition(0), s.transition(0), this.emit("scrollbarDragMove", t))
                },
                onDragEnd: function (t) {
                    var e = this.params.scrollbar,
                        i = this.scrollbar.$el;
                    this.scrollbar.isTouched && (this.scrollbar.isTouched = !1, e.hide && (clearTimeout(this.scrollbar.dragTimeout), this.scrollbar.dragTimeout = a.nextTick(function () {
                        i.css("opacity", 0), i.transition(400)
                    }, 1e3)), this.emit("scrollbarDragEnd", t), e.snapOnRelease && this.slideReset())
                },
                enableDraggable: function () {
                    if (this.params.scrollbar.el) {
                        var e = this.scrollbar.$el,
                            i = h.touch ? e[0] : document;
                        e.on(this.scrollbar.dragEvents.start, this.scrollbar.onDragStart), t(i).on(this.scrollbar.dragEvents.move, this.scrollbar.onDragMove), t(i).on(this.scrollbar.dragEvents.end, this.scrollbar.onDragEnd)
                    }
                },
                disableDraggable: function () {
                    if (this.params.scrollbar.el) {
                        var e = this.scrollbar.$el,
                            i = h.touch ? e[0] : document;
                        e.off(this.scrollbar.dragEvents.start), t(i).off(this.scrollbar.dragEvents.move), t(i).off(this.scrollbar.dragEvents.end)
                    }
                },
                init: function () {
                    var e = this;
                    if (e.params.scrollbar.el) {
                        var i = e.scrollbar,
                            n = e.$el,
                            s = e.touchEvents,
                            r = e.params.scrollbar,
                            o = t(r.el);
                        e.params.uniqueNavElements && "string" == typeof r.el && o.length > 1 && 1 === n.find(r.el).length && (o = n.find(r.el));
                        var l = o.find(".swiper-scrollbar-drag");
                        0 === l.length && (l = t('<div class="swiper-scrollbar-drag"></div>'), o.append(l)), e.scrollbar.dragEvents = !1 !== e.params.simulateTouch || h.touch ? s : {
                            start: "mousedown",
                            move: "mousemove",
                            end: "mouseup"
                        }, a.extend(i, {
                            $el: o,
                            el: o[0],
                            $dragEl: l,
                            dragEl: l[0]
                        }), r.draggable && i.enableDraggable()
                    }
                },
                destroy: function () {
                    this.scrollbar.disableDraggable()
                }
            },
            Y = {
                name: "scrollbar",
                params: {
                    scrollbar: {
                        el: null,
                        dragSize: "auto",
                        hide: !1,
                        draggable: !1,
                        snapOnRelease: !0
                    }
                },
                create: function () {
                    a.extend(this, {
                        scrollbar: {
                            init: X.init.bind(this),
                            destroy: X.destroy.bind(this),
                            updateSize: X.updateSize.bind(this),
                            setTranslate: X.setTranslate.bind(this),
                            setTransition: X.setTransition.bind(this),
                            enableDraggable: X.enableDraggable.bind(this),
                            disableDraggable: X.disableDraggable.bind(this),
                            setDragPosition: X.setDragPosition.bind(this),
                            onDragStart: X.onDragStart.bind(this),
                            onDragMove: X.onDragMove.bind(this),
                            onDragEnd: X.onDragEnd.bind(this),
                            isTouched: !1,
                            timeout: null,
                            dragTimeout: null
                        }
                    })
                },
                on: {
                    init: function () {
                        this.scrollbar.init(), this.scrollbar.updateSize(), this.scrollbar.setTranslate()
                    },
                    update: function () {
                        this.scrollbar.updateSize()
                    },
                    resize: function () {
                        this.scrollbar.updateSize()
                    },
                    observerUpdate: function () {
                        this.scrollbar.updateSize()
                    },
                    setTranslate: function () {
                        this.scrollbar.setTranslate()
                    },
                    setTransition: function (t) {
                        this.scrollbar.setTransition(t)
                    },
                    destroy: function () {
                        this.scrollbar.destroy()
                    }
                }
            },
            G = {
                setTransform: function (e, i) {
                    var n = this.rtl,
                        s = t(e),
                        r = n ? -1 : 1,
                        o = s.attr("data-swiper-parallax") || "0",
                        a = s.attr("data-swiper-parallax-x"),
                        l = s.attr("data-swiper-parallax-y"),
                        h = s.attr("data-swiper-parallax-scale"),
                        d = s.attr("data-swiper-parallax-opacity");
                    if (a || l ? (a = a || "0", l = l || "0") : this.isHorizontal() ? (a = o, l = "0") : (l = o, a = "0"), a = a.indexOf("%") >= 0 ? parseInt(a, 10) * i * r + "%" : a * i * r + "px", l = l.indexOf("%") >= 0 ? parseInt(l, 10) * i + "%" : l * i + "px", void 0 !== d && null !== d) {
                        var c = d - (d - 1) * (1 - Math.abs(i));
                        s[0].style.opacity = c
                    }
                    if (void 0 === h || null === h) s.transform("translate3d(" + a + ", " + l + ", 0px)");
                    else {
                        var u = h - (h - 1) * (1 - Math.abs(i));
                        s.transform("translate3d(" + a + ", " + l + ", 0px) scale(" + u + ")")
                    }
                },
                setTranslate: function () {
                    var e = this,
                        i = e.$el,
                        n = e.slides,
                        s = e.progress,
                        r = e.snapGrid;
                    i.children("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function (t, i) {
                        e.parallax.setTransform(i, s)
                    }), n.each(function (i, n) {
                        var o = n.progress;
                        e.params.slidesPerGroup > 1 && "auto" !== e.params.slidesPerView && (o += Math.ceil(i / 2) - s * (r.length - 1)), o = Math.min(Math.max(o, -1), 1), t(n).find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function (t, i) {
                            e.parallax.setTransform(i, o)
                        })
                    })
                },
                setTransition: function (e) {
                    void 0 === e && (e = this.params.speed);
                    this.$el.find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function (i, n) {
                        var s = t(n),
                            r = parseInt(s.attr("data-swiper-parallax-duration"), 10) || e;
                        0 === e && (r = 0), s.transition(r)
                    })
                }
            },
            V = {
                name: "parallax",
                params: {
                    parallax: {
                        enabled: !1
                    }
                },
                create: function () {
                    a.extend(this, {
                        parallax: {
                            setTransform: G.setTransform.bind(this),
                            setTranslate: G.setTranslate.bind(this),
                            setTransition: G.setTransition.bind(this)
                        }
                    })
                },
                on: {
                    beforeInit: function () {
                        this.params.watchSlidesProgress = !0
                    },
                    init: function () {
                        this.params.parallax && this.parallax.setTranslate()
                    },
                    setTranslate: function () {
                        this.params.parallax && this.parallax.setTranslate()
                    },
                    setTransition: function (t) {
                        this.params.parallax && this.parallax.setTransition(t)
                    }
                }
            },
            Q = {
                getDistanceBetweenTouches: function (t) {
                    if (t.targetTouches.length < 2) return 1;
                    var e = t.targetTouches[0].pageX,
                        i = t.targetTouches[0].pageY,
                        n = t.targetTouches[1].pageX,
                        s = t.targetTouches[1].pageY;
                    return Math.sqrt(Math.pow(n - e, 2) + Math.pow(s - i, 2))
                },
                onGestureStart: function (e) {
                    var i = this.params.zoom,
                        n = this.zoom,
                        s = n.gesture;
                    if (n.fakeGestureTouched = !1, n.fakeGestureMoved = !1, !h.gestures) {
                        if ("touchstart" !== e.type || "touchstart" === e.type && e.targetTouches.length < 2) return;
                        n.fakeGestureTouched = !0, s.scaleStart = Q.getDistanceBetweenTouches(e)
                    }
                    s.$slideEl && s.$slideEl.length || (s.$slideEl = t(this), 0 === s.$slideEl.length && (s.$slideEl = this.slides.eq(this.activeIndex)), s.$imageEl = s.$slideEl.find("img, svg, canvas"), s.$imageWrapEl = s.$imageEl.parent("." + i.containerClass), s.maxRatio = s.$imageWrapEl.attr("data-swiper-zoom") || i.maxRatio, 0 !== s.$imageWrapEl.length) ? (s.$imageEl.transition(0), this.zoom.isScaling = !0) : s.$imageEl = void 0
                },
                onGestureChange: function (t) {
                    var e = this.params.zoom,
                        i = this.zoom,
                        n = i.gesture;
                    if (!h.gestures) {
                        if ("touchmove" !== t.type || "touchmove" === t.type && t.targetTouches.length < 2) return;
                        i.fakeGestureMoved = !0, n.scaleMove = Q.getDistanceBetweenTouches(t)
                    }
                    n.$imageEl && 0 !== n.$imageEl.length && (h.gestures ? this.zoom.scale = t.scale * i.currentScale : i.scale = n.scaleMove / n.scaleStart * i.currentScale, i.scale > n.maxRatio && (i.scale = n.maxRatio - 1 + Math.pow(i.scale - n.maxRatio + 1, .5)), i.scale < e.minRatio && (i.scale = e.minRatio + 1 - Math.pow(e.minRatio - i.scale + 1, .5)), n.$imageEl.transform("translate3d(0,0,0) scale(" + i.scale + ")"))
                },
                onGestureEnd: function (t) {
                    var e = this.params.zoom,
                        i = this.zoom,
                        n = i.gesture;
                    if (!h.gestures) {
                        if (!i.fakeGestureTouched || !i.fakeGestureMoved) return;
                        if ("touchend" !== t.type || "touchend" === t.type && t.changedTouches.length < 2 && !w.android) return;
                        i.fakeGestureTouched = !1, i.fakeGestureMoved = !1
                    }
                    n.$imageEl && 0 !== n.$imageEl.length && (i.scale = Math.max(Math.min(i.scale, n.maxRatio), e.minRatio), n.$imageEl.transition(this.params.speed).transform("translate3d(0,0,0) scale(" + i.scale + ")"), i.currentScale = i.scale, i.isScaling = !1, 1 === i.scale && (n.$slideEl = void 0))
                },
                onTouchStart: function (t) {
                    var e = this.zoom,
                        i = e.gesture,
                        n = e.image;
                    i.$imageEl && 0 !== i.$imageEl.length && (n.isTouched || (w.android && t.preventDefault(), n.isTouched = !0, n.touchesStart.x = "touchstart" === t.type ? t.targetTouches[0].pageX : t.pageX, n.touchesStart.y = "touchstart" === t.type ? t.targetTouches[0].pageY : t.pageY))
                },
                onTouchMove: function (t) {
                    var e = this.zoom,
                        i = e.gesture,
                        n = e.image,
                        s = e.velocity;
                    if (i.$imageEl && 0 !== i.$imageEl.length && (this.allowClick = !1, n.isTouched && i.$slideEl)) {
                        n.isMoved || (n.width = i.$imageEl[0].offsetWidth, n.height = i.$imageEl[0].offsetHeight, n.startX = a.getTranslate(i.$imageWrapEl[0], "x") || 0, n.startY = a.getTranslate(i.$imageWrapEl[0], "y") || 0, i.slideWidth = i.$slideEl[0].offsetWidth, i.slideHeight = i.$slideEl[0].offsetHeight, i.$imageWrapEl.transition(0), this.rtl && (n.startX = -n.startX), this.rtl && (n.startY = -n.startY));
                        var r = n.width * e.scale,
                            o = n.height * e.scale;
                        if (!(r < i.slideWidth && o < i.slideHeight)) {
                            if (n.minX = Math.min(i.slideWidth / 2 - r / 2, 0), n.maxX = -n.minX, n.minY = Math.min(i.slideHeight / 2 - o / 2, 0), n.maxY = -n.minY, n.touchesCurrent.x = "touchmove" === t.type ? t.targetTouches[0].pageX : t.pageX, n.touchesCurrent.y = "touchmove" === t.type ? t.targetTouches[0].pageY : t.pageY, !n.isMoved && !e.isScaling) {
                                if (this.isHorizontal() && (Math.floor(n.minX) === Math.floor(n.startX) && n.touchesCurrent.x < n.touchesStart.x || Math.floor(n.maxX) === Math.floor(n.startX) && n.touchesCurrent.x > n.touchesStart.x)) return void(n.isTouched = !1);
                                if (!this.isHorizontal() && (Math.floor(n.minY) === Math.floor(n.startY) && n.touchesCurrent.y < n.touchesStart.y || Math.floor(n.maxY) === Math.floor(n.startY) && n.touchesCurrent.y > n.touchesStart.y)) return void(n.isTouched = !1)
                            }
                            t.preventDefault(), t.stopPropagation(), n.isMoved = !0, n.currentX = n.touchesCurrent.x - n.touchesStart.x + n.startX, n.currentY = n.touchesCurrent.y - n.touchesStart.y + n.startY, n.currentX < n.minX && (n.currentX = n.minX + 1 - Math.pow(n.minX - n.currentX + 1, .8)), n.currentX > n.maxX && (n.currentX = n.maxX - 1 + Math.pow(n.currentX - n.maxX + 1, .8)), n.currentY < n.minY && (n.currentY = n.minY + 1 - Math.pow(n.minY - n.currentY + 1, .8)), n.currentY > n.maxY && (n.currentY = n.maxY - 1 + Math.pow(n.currentY - n.maxY + 1, .8)), s.prevPositionX || (s.prevPositionX = n.touchesCurrent.x), s.prevPositionY || (s.prevPositionY = n.touchesCurrent.y), s.prevTime || (s.prevTime = Date.now()), s.x = (n.touchesCurrent.x - s.prevPositionX) / (Date.now() - s.prevTime) / 2, s.y = (n.touchesCurrent.y - s.prevPositionY) / (Date.now() - s.prevTime) / 2, Math.abs(n.touchesCurrent.x - s.prevPositionX) < 2 && (s.x = 0), Math.abs(n.touchesCurrent.y - s.prevPositionY) < 2 && (s.y = 0), s.prevPositionX = n.touchesCurrent.x, s.prevPositionY = n.touchesCurrent.y, s.prevTime = Date.now(), i.$imageWrapEl.transform("translate3d(" + n.currentX + "px, " + n.currentY + "px,0)")
                        }
                    }
                },
                onTouchEnd: function () {
                    var t = this.zoom,
                        e = t.gesture,
                        i = t.image,
                        n = t.velocity;
                    if (e.$imageEl && 0 !== e.$imageEl.length) {
                        if (!i.isTouched || !i.isMoved) return i.isTouched = !1, void(i.isMoved = !1);
                        i.isTouched = !1, i.isMoved = !1;
                        var s = 300,
                            r = 300,
                            o = n.x * s,
                            a = i.currentX + o,
                            l = n.y * r,
                            h = i.currentY + l;
                        0 !== n.x && (s = Math.abs((a - i.currentX) / n.x)), 0 !== n.y && (r = Math.abs((h - i.currentY) / n.y));
                        var d = Math.max(s, r);
                        i.currentX = a, i.currentY = h;
                        var c = i.width * t.scale,
                            u = i.height * t.scale;
                        i.minX = Math.min(e.slideWidth / 2 - c / 2, 0), i.maxX = -i.minX, i.minY = Math.min(e.slideHeight / 2 - u / 2, 0), i.maxY = -i.minY, i.currentX = Math.max(Math.min(i.currentX, i.maxX), i.minX), i.currentY = Math.max(Math.min(i.currentY, i.maxY), i.minY), e.$imageWrapEl.transition(d).transform("translate3d(" + i.currentX + "px, " + i.currentY + "px,0)")
                    }
                },
                onTransitionEnd: function () {
                    var t = this.zoom,
                        e = t.gesture;
                    e.$slideEl && this.previousIndex !== this.activeIndex && (e.$imageEl.transform("translate3d(0,0,0) scale(1)"), e.$imageWrapEl.transform("translate3d(0,0,0)"), e.$slideEl = void 0, e.$imageEl = void 0, e.$imageWrapEl = void 0, t.scale = 1, t.currentScale = 1)
                },
                toggle: function (t) {
                    var e = this.zoom;
                    e.scale && 1 !== e.scale ? e.out() : e.in(t)
                },
                in: function (e) {
                    var i = this.zoom,
                        n = this.params.zoom,
                        s = i.gesture,
                        r = i.image;
                    if (s.$slideEl || (s.$slideEl = this.clickedSlide ? t(this.clickedSlide) : this.slides.eq(this.activeIndex), s.$imageEl = s.$slideEl.find("img, svg, canvas"), s.$imageWrapEl = s.$imageEl.parent("." + n.containerClass)), s.$imageEl && 0 !== s.$imageEl.length) {
                        s.$slideEl.addClass("" + n.zoomedSlideClass);
                        var o, a, l, h, d, c, u, p, f, m, g, v, y, b, w, x;
                        void 0 === r.touchesStart.x && e ? (o = "touchend" === e.type ? e.changedTouches[0].pageX : e.pageX, a = "touchend" === e.type ? e.changedTouches[0].pageY : e.pageY) : (o = r.touchesStart.x, a = r.touchesStart.y), i.scale = s.$imageWrapEl.attr("data-swiper-zoom") || n.maxRatio, i.currentScale = s.$imageWrapEl.attr("data-swiper-zoom") || n.maxRatio, e ? (w = s.$slideEl[0].offsetWidth, x = s.$slideEl[0].offsetHeight, l = s.$slideEl.offset().left + w / 2 - o, h = s.$slideEl.offset().top + x / 2 - a, u = s.$imageEl[0].offsetWidth, p = s.$imageEl[0].offsetHeight, f = u * i.scale, m = p * i.scale, y = -(g = Math.min(w / 2 - f / 2, 0)), b = -(v = Math.min(x / 2 - m / 2, 0)), d = l * i.scale, c = h * i.scale, d < g && (d = g), d > y && (d = y), c < v && (c = v), c > b && (c = b)) : (d = 0, c = 0), s.$imageWrapEl.transition(300).transform("translate3d(" + d + "px, " + c + "px,0)"), s.$imageEl.transition(300).transform("translate3d(0,0,0) scale(" + i.scale + ")")
                    }
                },
                out: function () {
                    var e = this.zoom,
                        i = this.params.zoom,
                        n = e.gesture;
                    n.$slideEl || (n.$slideEl = this.clickedSlide ? t(this.clickedSlide) : this.slides.eq(this.activeIndex), n.$imageEl = n.$slideEl.find("img, svg, canvas"), n.$imageWrapEl = n.$imageEl.parent("." + i.containerClass)), n.$imageEl && 0 !== n.$imageEl.length && (e.scale = 1, e.currentScale = 1, n.$imageWrapEl.transition(300).transform("translate3d(0,0,0)"), n.$imageEl.transition(300).transform("translate3d(0,0,0) scale(1)"), n.$slideEl.removeClass("" + i.zoomedSlideClass), n.$slideEl = void 0)
                },
                enable: function () {
                    var e = this,
                        i = e.zoom;
                    if (!i.enabled) {
                        i.enabled = !0;
                        var n = e.slides,
                            s = !("touchstart" !== e.touchEvents.start || !h.passiveListener || !e.params.passiveListeners) && {
                                passive: !0,
                                capture: !1
                            };
                        h.gestures ? (n.on("gesturestart", i.onGestureStart, s), n.on("gesturechange", i.onGestureChange, s), n.on("gestureend", i.onGestureEnd, s)) : "touchstart" === e.touchEvents.start && (n.on(e.touchEvents.start, i.onGestureStart, s), n.on(e.touchEvents.move, i.onGestureChange, s), n.on(e.touchEvents.end, i.onGestureEnd, s)), e.slides.each(function (n, s) {
                            var r = t(s);
                            r.find("." + e.params.zoom.containerClass).length > 0 && r.on(e.touchEvents.move, i.onTouchMove)
                        })
                    }
                },
                disable: function () {
                    var e = this,
                        i = e.zoom;
                    if (i.enabled) {
                        e.zoom.enabled = !1;
                        var n = e.slides,
                            s = !("touchstart" !== e.touchEvents.start || !h.passiveListener || !e.params.passiveListeners) && {
                                passive: !0,
                                capture: !1
                            };
                        h.gestures ? (n.off("gesturestart", i.onGestureStart, s), n.off("gesturechange", i.onGestureChange, s), n.off("gestureend", i.onGestureEnd, s)) : "touchstart" === e.touchEvents.start && (n.off(e.touchEvents.start, i.onGestureStart, s), n.off(e.touchEvents.move, i.onGestureChange, s), n.off(e.touchEvents.end, i.onGestureEnd, s)), e.slides.each(function (n, s) {
                            var r = t(s);
                            r.find("." + e.params.zoom.containerClass).length > 0 && r.off(e.touchEvents.move, i.onTouchMove)
                        })
                    }
                }
            },
            K = {
                name: "zoom",
                params: {
                    zoom: {
                        enabled: !1,
                        maxRatio: 3,
                        minRatio: 1,
                        toggle: !0,
                        containerClass: "swiper-zoom-container",
                        zoomedSlideClass: "swiper-slide-zoomed"
                    }
                },
                create: function () {
                    var t = this,
                        e = {
                            enabled: !1,
                            scale: 1,
                            currentScale: 1,
                            isScaling: !1,
                            gesture: {
                                $slideEl: void 0,
                                slideWidth: void 0,
                                slideHeight: void 0,
                                $imageEl: void 0,
                                $imageWrapEl: void 0,
                                maxRatio: 3
                            },
                            image: {
                                isTouched: void 0,
                                isMoved: void 0,
                                currentX: void 0,
                                currentY: void 0,
                                minX: void 0,
                                minY: void 0,
                                maxX: void 0,
                                maxY: void 0,
                                width: void 0,
                                height: void 0,
                                startX: void 0,
                                startY: void 0,
                                touchesStart: {},
                                touchesCurrent: {}
                            },
                            velocity: {
                                x: void 0,
                                y: void 0,
                                prevPositionX: void 0,
                                prevPositionY: void 0,
                                prevTime: void 0
                            }
                        };
                    "onGestureStart onGestureChange onGestureEnd onTouchStart onTouchMove onTouchEnd onTransitionEnd toggle enable disable in out".split(" ").forEach(function (i) {
                        e[i] = Q[i].bind(t)
                    }), a.extend(t, {
                        zoom: e
                    })
                },
                on: {
                    init: function () {
                        this.params.zoom.enabled && this.zoom.enable()
                    },
                    destroy: function () {
                        this.zoom.disable()
                    },
                    touchStart: function (t) {
                        this.zoom.enabled && this.zoom.onTouchStart(t)
                    },
                    touchEnd: function (t) {
                        this.zoom.enabled && this.zoom.onTouchEnd(t)
                    },
                    doubleTap: function (t) {
                        this.params.zoom.enabled && this.zoom.enabled && this.params.zoom.toggle && this.zoom.toggle(t)
                    },
                    transitionEnd: function () {
                        this.zoom.enabled && this.params.zoom.enabled && this.zoom.onTransitionEnd()
                    }
                }
            },
            Z = {
                loadInSlide: function (e, i) {
                    void 0 === i && (i = !0);
                    var n = this,
                        s = n.params.lazy;
                    if (void 0 !== e && 0 !== n.slides.length) {
                        var r = n.virtual && n.params.virtual.enabled ? n.$wrapperEl.children("." + n.params.slideClass + '[data-swiper-slide-index="' + e + '"]') : n.slides.eq(e),
                            o = r.find("." + s.elementClass + ":not(." + s.loadedClass + "):not(." + s.loadingClass + ")");
                        !r.hasClass(s.elementClass) || r.hasClass(s.loadedClass) || r.hasClass(s.loadingClass) || (o = o.add(r[0])), 0 !== o.length && o.each(function (e, o) {
                            var a = t(o);
                            a.addClass(s.loadingClass);
                            var l = a.attr("data-background"),
                                h = a.attr("data-src"),
                                d = a.attr("data-srcset"),
                                c = a.attr("data-sizes");
                            n.loadImage(a[0], h || l, d, c, !1, function () {
                                if (void 0 !== n && null !== n && n && (!n || n.params) && !n.destroyed) {
                                    if (l ? (a.css("background-image", 'url("' + l + '")'), a.removeAttr("data-background")) : (d && (a.attr("srcset", d), a.removeAttr("data-srcset")), c && (a.attr("sizes", c), a.removeAttr("data-sizes")), h && (a.attr("src", h), a.removeAttr("data-src"))), a.addClass(s.loadedClass).removeClass(s.loadingClass), r.find("." + s.preloaderClass).remove(), n.params.loop && i) {
                                        var t = r.attr("data-swiper-slide-index");
                                        if (r.hasClass(n.params.slideDuplicateClass)) {
                                            var e = n.$wrapperEl.children('[data-swiper-slide-index="' + t + '"]:not(.' + n.params.slideDuplicateClass + ")");
                                            n.lazy.loadInSlide(e.index(), !1)
                                        } else {
                                            var o = n.$wrapperEl.children("." + n.params.slideDuplicateClass + '[data-swiper-slide-index="' + t + '"]');
                                            n.lazy.loadInSlide(o.index(), !1)
                                        }
                                    }
                                    n.emit("lazyImageReady", r[0], a[0])
                                }
                            }), n.emit("lazyImageLoad", r[0], a[0])
                        })
                    }
                },
                load: function () {
                    function e(t) {
                        if (l) {
                            if (s.children("." + r.slideClass + '[data-swiper-slide-index="' + t + '"]').length) return !0
                        } else if (o[t]) return !0;
                        return !1
                    }

                    function i(e) {
                        return l ? t(e).attr("data-swiper-slide-index") : t(e).index()
                    }
                    var n = this,
                        s = n.$wrapperEl,
                        r = n.params,
                        o = n.slides,
                        a = n.activeIndex,
                        l = n.virtual && r.virtual.enabled,
                        h = r.lazy,
                        d = r.slidesPerView;
                    if ("auto" === d && (d = 0), n.lazy.initialImageLoaded || (n.lazy.initialImageLoaded = !0), n.params.watchSlidesVisibility) s.children("." + r.slideVisibleClass).each(function (e, i) {
                        var s = l ? t(i).attr("data-swiper-slide-index") : t(i).index();
                        n.lazy.loadInSlide(s)
                    });
                    else if (d > 1)
                        for (var c = a; c < a + d; c += 1) e(c) && n.lazy.loadInSlide(c);
                    else n.lazy.loadInSlide(a);
                    if (h.loadPrevNext)
                        if (d > 1 || h.loadPrevNextAmount && h.loadPrevNextAmount > 1) {
                            for (var u = h.loadPrevNextAmount, p = d, f = Math.min(a + p + Math.max(u, p), o.length), m = Math.max(a - Math.max(p, u), 0), g = a + d; g < f; g += 1) e(g) && n.lazy.loadInSlide(g);
                            for (var v = m; v < a; v += 1) e(v) && n.lazy.loadInSlide(v)
                        } else {
                            var y = s.children("." + r.slideNextClass);
                            y.length > 0 && n.lazy.loadInSlide(i(y));
                            var b = s.children("." + r.slidePrevClass);
                            b.length > 0 && n.lazy.loadInSlide(i(b))
                        }
                }
            },
            J = {
                name: "lazy",
                params: {
                    lazy: {
                        enabled: !1,
                        loadPrevNext: !1,
                        loadPrevNextAmount: 1,
                        loadOnTransitionStart: !1,
                        elementClass: "swiper-lazy",
                        loadingClass: "swiper-lazy-loading",
                        loadedClass: "swiper-lazy-loaded",
                        preloaderClass: "swiper-lazy-preloader"
                    }
                },
                create: function () {
                    a.extend(this, {
                        lazy: {
                            initialImageLoaded: !1,
                            load: Z.load.bind(this),
                            loadInSlide: Z.loadInSlide.bind(this)
                        }
                    })
                },
                on: {
                    beforeInit: function () {
                        this.params.lazy.enabled && this.params.preloadImages && (this.params.preloadImages = !1)
                    },
                    init: function () {
                        this.params.lazy.enabled && !this.params.loop && 0 === this.params.initialSlide && this.lazy.load()
                    },
                    scroll: function () {
                        this.params.freeMode && !this.params.freeModeSticky && this.lazy.load()
                    },
                    resize: function () {
                        this.params.lazy.enabled && this.lazy.load()
                    },
                    scrollbarDragMove: function () {
                        this.params.lazy.enabled && this.lazy.load()
                    },
                    transitionStart: function () {
                        this.params.lazy.enabled && (this.params.lazy.loadOnTransitionStart || !this.params.lazy.loadOnTransitionStart && !this.lazy.initialImageLoaded) && this.lazy.load()
                    },
                    transitionEnd: function () {
                        this.params.lazy.enabled && !this.params.lazy.loadOnTransitionStart && this.lazy.load()
                    }
                }
            },
            tt = {
                LinearSpline: function (t, e) {
                    var i = function () {
                        var t, e, i;
                        return function (n, s) {
                            for (e = -1, t = n.length; t - e > 1;) n[i = t + e >> 1] <= s ? e = i : t = i;
                            return t
                        }
                    }();
                    this.x = t, this.y = e, this.lastIndex = t.length - 1;
                    var n, s;
                    return this.interpolate = function (t) {
                        return t ? (s = i(this.x, t), n = s - 1, (t - this.x[n]) * (this.y[s] - this.y[n]) / (this.x[s] - this.x[n]) + this.y[n]) : 0
                    }, this
                },
                getInterpolateFunction: function (t) {
                    this.controller.spline || (this.controller.spline = this.params.loop ? new tt.LinearSpline(this.slidesGrid, t.slidesGrid) : new tt.LinearSpline(this.snapGrid, t.snapGrid))
                },
                setTranslate: function (t, e) {
                    function i(t) {
                        var e = t.rtl && "horizontal" === t.params.direction ? -r.translate : r.translate;
                        "slide" === r.params.controller.by && (r.controller.getInterpolateFunction(t), s = -r.controller.spline.interpolate(-e)), s && "container" !== r.params.controller.by || (n = (t.maxTranslate() - t.minTranslate()) / (r.maxTranslate() - r.minTranslate()), s = (e - r.minTranslate()) * n + t.minTranslate()), r.params.controller.inverse && (s = t.maxTranslate() - s), t.updateProgress(s), t.setTranslate(s, r), t.updateActiveIndex(), t.updateSlidesClasses()
                    }
                    var n, s, r = this,
                        o = r.controller.control;
                    if (Array.isArray(o))
                        for (var a = 0; a < o.length; a += 1) o[a] !== e && o[a] instanceof z && i(o[a]);
                    else o instanceof z && e !== o && i(o)
                },
                setTransition: function (t, e) {
                    function i(e) {
                        e.setTransition(t, s), 0 !== t && (e.transitionStart(), e.$wrapperEl.transitionEnd(function () {
                            r && (e.params.loop && "slide" === s.params.controller.by && e.loopFix(), e.transitionEnd())
                        }))
                    }
                    var n, s = this,
                        r = s.controller.control;
                    if (Array.isArray(r))
                        for (n = 0; n < r.length; n += 1) r[n] !== e && r[n] instanceof z && i(r[n]);
                    else r instanceof z && e !== r && i(r)
                }
            },
            et = {
                name: "controller",
                params: {
                    controller: {
                        control: void 0,
                        inverse: !1,
                        by: "slide"
                    }
                },
                create: function () {
                    a.extend(this, {
                        controller: {
                            control: this.params.controller.control,
                            getInterpolateFunction: tt.getInterpolateFunction.bind(this),
                            setTranslate: tt.setTranslate.bind(this),
                            setTransition: tt.setTransition.bind(this)
                        }
                    })
                },
                on: {
                    update: function () {
                        this.controller.control && this.controller.spline && (this.controller.spline = void 0, delete this.controller.spline)
                    },
                    resize: function () {
                        this.controller.control && this.controller.spline && (this.controller.spline = void 0, delete this.controller.spline)
                    },
                    observerUpdate: function () {
                        this.controller.control && this.controller.spline && (this.controller.spline = void 0, delete this.controller.spline)
                    },
                    setTranslate: function (t, e) {
                        this.controller.control && this.controller.setTranslate(t, e)
                    },
                    setTransition: function (t, e) {
                        this.controller.control && this.controller.setTransition(t, e)
                    }
                }
            },
            it = {
                makeElFocusable: function (t) {
                    return t.attr("tabIndex", "0"), t
                },
                addElRole: function (t, e) {
                    return t.attr("role", e), t
                },
                addElLabel: function (t, e) {
                    return t.attr("aria-label", e), t
                },
                disableEl: function (t) {
                    return t.attr("aria-disabled", !0), t
                },
                enableEl: function (t) {
                    return t.attr("aria-disabled", !1), t
                },
                onEnterKey: function (e) {
                    var i = this.params.a11y;
                    if (13 === e.keyCode) {
                        var n = t(e.target);
                        this.navigation && this.navigation.$nextEl && n.is(this.navigation.$nextEl) && (this.isEnd && !this.params.loop || this.slideNext(), this.isEnd ? this.a11y.notify(i.lastSlideMessage) : this.a11y.notify(i.nextSlideMessage)), this.navigation && this.navigation.$prevEl && n.is(this.navigation.$prevEl) && (this.isBeginning && !this.params.loop || this.slidePrev(), this.isBeginning ? this.a11y.notify(i.firstSlideMessage) : this.a11y.notify(i.prevSlideMessage)), this.pagination && n.is("." + this.params.pagination.bulletClass) && n[0].click()
                    }
                },
                notify: function (t) {
                    var e = this.a11y.liveRegion;
                    0 !== e.length && (e.html(""), e.html(t))
                },
                updateNavigation: function () {
                    if (!this.params.loop) {
                        var t = this.navigation,
                            e = t.$nextEl,
                            i = t.$prevEl;
                        i && i.length > 0 && (this.isBeginning ? this.a11y.disableEl(i) : this.a11y.enableEl(i)), e && e.length > 0 && (this.isEnd ? this.a11y.disableEl(e) : this.a11y.enableEl(e))
                    }
                },
                updatePagination: function () {
                    var e = this,
                        i = e.params.a11y;
                    e.pagination && e.params.pagination.clickable && e.pagination.bullets && e.pagination.bullets.length && e.pagination.bullets.each(function (n, s) {
                        var r = t(s);
                        e.a11y.makeElFocusable(r), e.a11y.addElRole(r, "button"), e.a11y.addElLabel(r, i.paginationBulletMessage.replace(/{{index}}/, r.index() + 1))
                    })
                },
                init: function () {
                    this.$el.append(this.a11y.liveRegion);
                    var t, e, i = this.params.a11y;
                    this.navigation && this.navigation.$nextEl && (t = this.navigation.$nextEl), this.navigation && this.navigation.$prevEl && (e = this.navigation.$prevEl), t && (this.a11y.makeElFocusable(t), this.a11y.addElRole(t, "button"), this.a11y.addElLabel(t, i.nextSlideMessage), t.on("keydown", this.a11y.onEnterKey)), e && (this.a11y.makeElFocusable(e), this.a11y.addElRole(e, "button"), this.a11y.addElLabel(e, i.prevSlideMessage), e.on("keydown", this.a11y.onEnterKey)), this.pagination && this.params.pagination.clickable && this.pagination.bullets && this.pagination.bullets.length && this.pagination.$el.on("keydown", "." + this.params.pagination.bulletClass, this.a11y.onEnterKey)
                },
                destroy: function () {
                    this.a11y.liveRegion && this.a11y.liveRegion.length > 0 && this.a11y.liveRegion.remove();
                    var t, e;
                    this.navigation && this.navigation.$nextEl && (t = this.navigation.$nextEl), this.navigation && this.navigation.$prevEl && (e = this.navigation.$prevEl), t && t.off("keydown", this.a11y.onEnterKey), e && e.off("keydown", this.a11y.onEnterKey), this.pagination && this.params.pagination.clickable && this.pagination.bullets && this.pagination.bullets.length && this.pagination.$el.off("keydown", "." + this.params.pagination.bulletClass, this.a11y.onEnterKey)
                }
            },
            nt = {
                name: "a11y",
                params: {
                    a11y: {
                        enabled: !1,
                        notificationClass: "swiper-notification",
                        prevSlideMessage: "Previous slide",
                        nextSlideMessage: "Next slide",
                        firstSlideMessage: "This is the first slide",
                        lastSlideMessage: "This is the last slide",
                        paginationBulletMessage: "Go to slide {{index}}"
                    }
                },
                create: function () {
                    var e = this;
                    a.extend(e, {
                        a11y: {
                            liveRegion: t('<span class="' + e.params.a11y.notificationClass + '" aria-live="assertive" aria-atomic="true"></span>')
                        }
                    }), Object.keys(it).forEach(function (t) {
                        e.a11y[t] = it[t].bind(e)
                    })
                },
                on: {
                    init: function () {
                        this.params.a11y.enabled && (this.a11y.init(), this.a11y.updateNavigation())
                    },
                    toEdge: function () {
                        this.params.a11y.enabled && this.a11y.updateNavigation()
                    },
                    fromEdge: function () {
                        this.params.a11y.enabled && this.a11y.updateNavigation()
                    },
                    paginationUpdate: function () {
                        this.params.a11y.enabled && this.a11y.updatePagination()
                    },
                    destroy: function () {
                        this.params.a11y.enabled && this.a11y.destroy()
                    }
                }
            },
            st = {
                init: function () {
                    if (this.params.history) {
                        if (!n.history || !n.history.pushState) return this.params.history.enabled = !1, void(this.params.hashNavigation.enabled = !0);
                        var t = this.history;
                        t.initialized = !0, t.paths = st.getPathValues(), (t.paths.key || t.paths.value) && (t.scrollToSlide(0, t.paths.value, this.params.runCallbacksOnInit), this.params.history.replaceState || n.addEventListener("popstate", this.history.setHistoryPopState))
                    }
                },
                destroy: function () {
                    this.params.history.replaceState || n.removeEventListener("popstate", this.history.setHistoryPopState)
                },
                setHistoryPopState: function () {
                    this.history.paths = st.getPathValues(), this.history.scrollToSlide(this.params.speed, this.history.paths.value, !1)
                },
                getPathValues: function () {
                    var t = n.location.pathname.slice(1).split("https://offshorethemes.com/").filter(function (t) {
                            return "" !== t
                        }),
                        e = t.length;
                    return {
                        key: t[e - 2],
                        value: t[e - 1]
                    }
                },
                setHistory: function (t, e) {
                    if (this.history.initialized && this.params.history.enabled) {
                        var i = this.slides.eq(e),
                            s = st.slugify(i.attr("data-history"));
                        n.location.pathname.includes(t) || (s = t + "/" + s);
                        var r = n.history.state;
                        r && r.value === s || (this.params.history.replaceState ? n.history.replaceState({
                            value: s
                        }, null, s) : n.history.pushState({
                            value: s
                        }, null, s))
                    }
                },
                slugify: function (t) {
                    return t.toString().toLowerCase().replace(/\s+/g, "-").replace(/[^\w-]+/g, "").replace(/--+/g, "-").replace(/^-+/, "").replace(/-+$/, "")
                },
                scrollToSlide: function (t, e, i) {
                    if (e)
                        for (var n = 0, s = this.slides.length; n < s; n += 1) {
                            var r = this.slides.eq(n);
                            if (st.slugify(r.attr("data-history")) === e && !r.hasClass(this.params.slideDuplicateClass)) {
                                var o = r.index();
                                this.slideTo(o, t, i)
                            }
                        } else this.slideTo(0, t, i)
                }
            },
            rt = {
                name: "history",
                params: {
                    history: {
                        enabled: !1,
                        replaceState: !1,
                        key: "slides"
                    }
                },
                create: function () {
                    a.extend(this, {
                        history: {
                            init: st.init.bind(this),
                            setHistory: st.setHistory.bind(this),
                            setHistoryPopState: st.setHistoryPopState.bind(this),
                            scrollToSlide: st.scrollToSlide.bind(this),
                            destroy: st.destroy.bind(this)
                        }
                    })
                },
                on: {
                    init: function () {
                        this.params.history.enabled && this.history.init()
                    },
                    destroy: function () {
                        this.params.history.enabled && this.history.destroy()
                    },
                    transitionEnd: function () {
                        this.history.initialized && this.history.setHistory(this.params.history.key, this.activeIndex)
                    }
                }
            },
            ot = {
                onHashCange: function () {
                    var t = l.location.hash.replace("#", "");
                    t !== this.slides.eq(this.activeIndex).attr("data-hash") && this.slideTo(this.$wrapperEl.children("." + this.params.slideClass + '[data-hash="' + t + '"]').index())
                },
                setHash: function () {
                    if (this.hashNavigation.initialized && this.params.hashNavigation.enabled)
                        if (this.params.hashNavigation.replaceState && n.history && n.history.replaceState) n.history.replaceState(null, null, "#" + this.slides.eq(this.activeIndex).attr("data-hash") || "");
                        else {
                            var t = this.slides.eq(this.activeIndex),
                                e = t.attr("data-hash") || t.attr("data-history");
                            l.location.hash = e || ""
                        }
                },
                init: function () {
                    if (!(!this.params.hashNavigation.enabled || this.params.history && this.params.history.enabled)) {
                        this.hashNavigation.initialized = !0;
                        var e = l.location.hash.replace("#", "");
                        if (e)
                            for (var i = 0, s = this.slides.length; i < s; i += 1) {
                                var r = this.slides.eq(i);
                                if ((r.attr("data-hash") || r.attr("data-history")) === e && !r.hasClass(this.params.slideDuplicateClass)) {
                                    var o = r.index();
                                    this.slideTo(o, 0, this.params.runCallbacksOnInit, !0)
                                }
                            }
                        this.params.hashNavigation.watchState && t(n).on("hashchange", this.hashNavigation.onHashCange)
                    }
                },
                destroy: function () {
                    this.params.hashNavigation.watchState && t(n).off("hashchange", this.hashNavigation.onHashCange)
                }
            },
            at = {
                name: "hash-navigation",
                params: {
                    hashNavigation: {
                        enabled: !1,
                        replaceState: !1,
                        watchState: !1
                    }
                },
                create: function () {
                    a.extend(this, {
                        hashNavigation: {
                            initialized: !1,
                            init: ot.init.bind(this),
                            destroy: ot.destroy.bind(this),
                            setHash: ot.setHash.bind(this),
                            onHashCange: ot.onHashCange.bind(this)
                        }
                    })
                },
                on: {
                    init: function () {
                        this.params.hashNavigation.enabled && this.hashNavigation.init()
                    },
                    destroy: function () {
                        this.params.hashNavigation.enabled && this.hashNavigation.destroy()
                    },
                    transitionEnd: function () {
                        this.hashNavigation.initialized && this.hashNavigation.setHash()
                    }
                }
            },
            lt = {
                run: function () {
                    var t = this,
                        e = t.slides.eq(t.activeIndex),
                        i = t.params.autoplay.delay;
                    e.attr("data-swiper-autoplay") && (i = e.attr("data-swiper-autoplay") || t.params.autoplay.delay), t.autoplay.timeout = a.nextTick(function () {
                        t.params.loop ? (t.loopFix(), t.slideNext(t.params.speed, !0, !0), t.emit("autoplay")) : t.isEnd ? t.params.autoplay.stopOnLastSlide ? t.autoplay.stop() : (t.slideTo(0, t.params.speed, !0, !0), t.emit("autoplay")) : (t.slideNext(t.params.speed, !0, !0), t.emit("autoplay"))
                    }, i)
                },
                start: function () {
                    return void 0 === this.autoplay.timeout && (!this.autoplay.running && (this.autoplay.running = !0, this.emit("autoplayStart"), this.autoplay.run(), !0))
                },
                stop: function () {
                    return !!this.autoplay.running && (void 0 !== this.autoplay.timeout && (this.autoplay.timeout && (clearTimeout(this.autoplay.timeout), this.autoplay.timeout = void 0), this.autoplay.running = !1, this.emit("autoplayStop"), !0))
                },
                pause: function (t) {
                    var e = this;
                    e.autoplay.running && (e.autoplay.paused || (e.autoplay.timeout && clearTimeout(e.autoplay.timeout), e.autoplay.paused = !0, 0 === t ? (e.autoplay.paused = !1, e.autoplay.run()) : e.$wrapperEl.transitionEnd(function () {
                        e && !e.destroyed && (e.autoplay.paused = !1, e.autoplay.running ? e.autoplay.run() : e.autoplay.stop())
                    })))
                }
            },
            ht = {
                name: "autoplay",
                params: {
                    autoplay: {
                        enabled: !1,
                        delay: 3e3,
                        disableOnInteraction: !0,
                        stopOnLastSlide: !1
                    }
                },
                create: function () {
                    a.extend(this, {
                        autoplay: {
                            running: !1,
                            paused: !1,
                            run: lt.run.bind(this),
                            start: lt.start.bind(this),
                            stop: lt.stop.bind(this),
                            pause: lt.pause.bind(this)
                        }
                    })
                },
                on: {
                    init: function () {
                        this.params.autoplay.enabled && this.autoplay.start()
                    },
                    beforeTransitionStart: function (t, e) {
                        this.autoplay.running && (e || !this.params.autoplay.disableOnInteraction ? this.autoplay.pause(t) : this.autoplay.stop())
                    },
                    sliderFirstMove: function () {
                        this.autoplay.running && (this.params.autoplay.disableOnInteraction ? this.autoplay.stop() : this.autoplay.pause())
                    },
                    destroy: function () {
                        this.autoplay.running && this.autoplay.stop()
                    }
                }
            },
            dt = {
                setTranslate: function () {
                    for (var t = this.slides, e = 0; e < t.length; e += 1) {
                        var i = this.slides.eq(e),
                            n = -i[0].swiperSlideOffset;
                        this.params.virtualTranslate || (n -= this.translate);
                        var s = 0;
                        this.isHorizontal() || (s = n, n = 0);
                        var r = this.params.fadeEffect.crossFade ? Math.max(1 - Math.abs(i[0].progress), 0) : 1 + Math.min(Math.max(i[0].progress, -1), 0);
                        i.css({
                            opacity: r
                        }).transform("translate3d(" + n + "px, " + s + "px, 0px)")
                    }
                },
                setTransition: function (t) {
                    var e = this,
                        i = e.slides,
                        n = e.$wrapperEl;
                    if (i.transition(t), e.params.virtualTranslate && 0 !== t) {
                        var s = !1;
                        i.transitionEnd(function () {
                            if (!s && e && !e.destroyed) {
                                s = !0, e.animating = !1;
                                for (var t = ["webkitTransitionEnd", "transitionend"], i = 0; i < t.length; i += 1) n.trigger(t[i])
                            }
                        })
                    }
                }
            },
            ct = {
                name: "effect-fade",
                params: {
                    fadeEffect: {
                        crossFade: !1
                    }
                },
                create: function () {
                    a.extend(this, {
                        fadeEffect: {
                            setTranslate: dt.setTranslate.bind(this),
                            setTransition: dt.setTransition.bind(this)
                        }
                    })
                },
                on: {
                    beforeInit: function () {
                        if ("fade" === this.params.effect) {
                            this.classNames.push(this.params.containerModifierClass + "fade");
                            var t = {
                                slidesPerView: 1,
                                slidesPerColumn: 1,
                                slidesPerGroup: 1,
                                watchSlidesProgress: !0,
                                spaceBetween: 0,
                                virtualTranslate: !0
                            };
                            a.extend(this.params, t), a.extend(this.originalParams, t)
                        }
                    },
                    setTranslate: function () {
                        "fade" === this.params.effect && this.fadeEffect.setTranslate()
                    },
                    setTransition: function (t) {
                        "fade" === this.params.effect && this.fadeEffect.setTransition(t)
                    }
                }
            },
            ut = {
                setTranslate: function () {
                    var e, i = this.$el,
                        n = this.$wrapperEl,
                        s = this.slides,
                        r = this.width,
                        o = this.height,
                        a = this.rtl,
                        l = this.size,
                        h = this.params.cubeEffect,
                        d = this.isHorizontal(),
                        c = this.virtual && this.params.virtual.enabled,
                        u = 0;
                    h.shadow && (d ? (0 === (e = n.find(".swiper-cube-shadow")).length && (e = t('<div class="swiper-cube-shadow"></div>'), n.append(e)), e.css({
                        height: r + "px"
                    })) : 0 === (e = i.find(".swiper-cube-shadow")).length && (e = t('<div class="swiper-cube-shadow"></div>'), i.append(e)));
                    for (var p = 0; p < s.length; p += 1) {
                        var f = s.eq(p),
                            g = p;
                        c && (g = parseInt(f.attr("data-swiper-slide-index"), 10));
                        var v = 90 * g,
                            y = Math.floor(v / 360);
                        a && (v = -v, y = Math.floor(-v / 360));
                        var b = Math.max(Math.min(f[0].progress, 1), -1),
                            w = 0,
                            x = 0,
                            C = 0;
                        g % 4 == 0 ? (w = 4 * -y * l, C = 0) : (g - 1) % 4 == 0 ? (w = 0, C = 4 * -y * l) : (g - 2) % 4 == 0 ? (w = l + 4 * y * l, C = l) : (g - 3) % 4 == 0 && (w = -l, C = 3 * l + 4 * l * y), a && (w = -w), d || (x = w, w = 0);
                        var T = "rotateX(" + (d ? 0 : -v) + "deg) rotateY(" + (d ? v : 0) + "deg) translate3d(" + w + "px, " + x + "px, " + C + "px)";
                        if (b <= 1 && b > -1 && (u = 90 * g + 90 * b, a && (u = 90 * -g - 90 * b)), f.transform(T), h.slideShadows) {
                            var E = d ? f.find(".swiper-slide-shadow-left") : f.find(".swiper-slide-shadow-top"),
                                S = d ? f.find(".swiper-slide-shadow-right") : f.find(".swiper-slide-shadow-bottom");
                            0 === E.length && (E = t('<div class="swiper-slide-shadow-' + (d ? "left" : "top") + '"></div>'), f.append(E)), 0 === S.length && (S = t('<div class="swiper-slide-shadow-' + (d ? "right" : "bottom") + '"></div>'), f.append(S)), E.length && (E[0].style.opacity = Math.max(-b, 0)), S.length && (S[0].style.opacity = Math.max(b, 0))
                        }
                    }
                    if (n.css({
                            "-webkit-transform-origin": "50% 50% -" + l / 2 + "px",
                            "-moz-transform-origin": "50% 50% -" + l / 2 + "px",
                            "-ms-transform-origin": "50% 50% -" + l / 2 + "px",
                            "transform-origin": "50% 50% -" + l / 2 + "px"
                        }), h.shadow)
                        if (d) e.transform("translate3d(0px, " + (r / 2 + h.shadowOffset) + "px, " + -r / 2 + "px) rotateX(90deg) rotateZ(0deg) scale(" + h.shadowScale + ")");
                        else {
                            var _ = Math.abs(u) - 90 * Math.floor(Math.abs(u) / 90),
                                k = 1.5 - (Math.sin(2 * _ * Math.PI / 360) / 2 + Math.cos(2 * _ * Math.PI / 360) / 2),
                                $ = h.shadowScale,
                                z = h.shadowScale / k,
                                M = h.shadowOffset;
                            e.transform("scale3d(" + $ + ", 1, " + z + ") translate3d(0px, " + (o / 2 + M) + "px, " + -o / 2 / z + "px) rotateX(-90deg)")
                        }
                    var D = m.isSafari || m.isUiWebView ? -l / 2 : 0;
                    n.transform("translate3d(0px,0," + D + "px) rotateX(" + (this.isHorizontal() ? 0 : u) + "deg) rotateY(" + (this.isHorizontal() ? -u : 0) + "deg)")
                },
                setTransition: function (t) {
                    var e = this.$el;
                    this.slides.transition(t).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(t), this.params.cubeEffect.shadow && !this.isHorizontal() && e.find(".swiper-cube-shadow").transition(t)
                }
            },
            pt = {
                name: "effect-cube",
                params: {
                    cubeEffect: {
                        slideShadows: !0,
                        shadow: !0,
                        shadowOffset: 20,
                        shadowScale: .94
                    }
                },
                create: function () {
                    a.extend(this, {
                        cubeEffect: {
                            setTranslate: ut.setTranslate.bind(this),
                            setTransition: ut.setTransition.bind(this)
                        }
                    })
                },
                on: {
                    beforeInit: function () {
                        if ("cube" === this.params.effect) {
                            this.classNames.push(this.params.containerModifierClass + "cube"), this.classNames.push(this.params.containerModifierClass + "3d");
                            var t = {
                                slidesPerView: 1,
                                slidesPerColumn: 1,
                                slidesPerGroup: 1,
                                watchSlidesProgress: !0,
                                resistanceRatio: 0,
                                spaceBetween: 0,
                                centeredSlides: !1,
                                virtualTranslate: !0
                            };
                            a.extend(this.params, t), a.extend(this.originalParams, t)
                        }
                    },
                    setTranslate: function () {
                        "cube" === this.params.effect && this.cubeEffect.setTranslate()
                    },
                    setTransition: function (t) {
                        "cube" === this.params.effect && this.cubeEffect.setTransition(t)
                    }
                }
            },
            ft = {
                setTranslate: function () {
                    for (var e = this.slides, i = 0; i < e.length; i += 1) {
                        var n = e.eq(i),
                            s = n[0].progress;
                        this.params.flipEffect.limitRotation && (s = Math.max(Math.min(n[0].progress, 1), -1));
                        var r = -180 * s,
                            o = 0,
                            a = -n[0].swiperSlideOffset,
                            l = 0;
                        if (this.isHorizontal() ? this.rtl && (r = -r) : (l = a, a = 0, o = -r, r = 0), n[0].style.zIndex = -Math.abs(Math.round(s)) + e.length, this.params.flipEffect.slideShadows) {
                            var h = this.isHorizontal() ? n.find(".swiper-slide-shadow-left") : n.find(".swiper-slide-shadow-top"),
                                d = this.isHorizontal() ? n.find(".swiper-slide-shadow-right") : n.find(".swiper-slide-shadow-bottom");
                            0 === h.length && (h = t('<div class="swiper-slide-shadow-' + (this.isHorizontal() ? "left" : "top") + '"></div>'), n.append(h)), 0 === d.length && (d = t('<div class="swiper-slide-shadow-' + (this.isHorizontal() ? "right" : "bottom") + '"></div>'), n.append(d)), h.length && (h[0].style.opacity = Math.max(-s, 0)), d.length && (d[0].style.opacity = Math.max(s, 0))
                        }
                        n.transform("translate3d(" + a + "px, " + l + "px, 0px) rotateX(" + o + "deg) rotateY(" + r + "deg)")
                    }
                },
                setTransition: function (t) {
                    var e = this,
                        i = e.slides,
                        n = e.activeIndex,
                        s = e.$wrapperEl;
                    if (i.transition(t).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(t), e.params.virtualTranslate && 0 !== t) {
                        var r = !1;
                        i.eq(n).transitionEnd(function () {
                            if (!r && e && !e.destroyed) {
                                r = !0, e.animating = !1;
                                for (var t = ["webkitTransitionEnd", "transitionend"], i = 0; i < t.length; i += 1) s.trigger(t[i])
                            }
                        })
                    }
                }
            },
            mt = {
                name: "effect-flip",
                params: {
                    flipEffect: {
                        slideShadows: !0,
                        limitRotation: !0
                    }
                },
                create: function () {
                    a.extend(this, {
                        flipEffect: {
                            setTranslate: ft.setTranslate.bind(this),
                            setTransition: ft.setTransition.bind(this)
                        }
                    })
                },
                on: {
                    beforeInit: function () {
                        if ("flip" === this.params.effect) {
                            this.classNames.push(this.params.containerModifierClass + "flip"), this.classNames.push(this.params.containerModifierClass + "3d");
                            var t = {
                                slidesPerView: 1,
                                slidesPerColumn: 1,
                                slidesPerGroup: 1,
                                watchSlidesProgress: !0,
                                spaceBetween: 0,
                                virtualTranslate: !0
                            };
                            a.extend(this.params, t), a.extend(this.originalParams, t)
                        }
                    },
                    setTranslate: function () {
                        "flip" === this.params.effect && this.flipEffect.setTranslate()
                    },
                    setTransition: function (t) {
                        "flip" === this.params.effect && this.flipEffect.setTransition(t)
                    }
                }
            },
            gt = {
                setTranslate: function () {
                    for (var e = this.width, i = this.height, n = this.slides, s = this.$wrapperEl, r = this.slidesSizesGrid, o = this.params.coverflowEffect, a = this.isHorizontal(), l = this.translate, h = a ? e / 2 - l : i / 2 - l, d = a ? o.rotate : -o.rotate, c = o.depth, u = 0, p = n.length; u < p; u += 1) {
                        var f = n.eq(u),
                            g = r[u],
                            v = (h - f[0].swiperSlideOffset - g / 2) / g * o.modifier,
                            y = a ? d * v : 0,
                            b = a ? 0 : d * v,
                            w = -c * Math.abs(v),
                            x = a ? 0 : o.stretch * v,
                            C = a ? o.stretch * v : 0;
                        Math.abs(C) < .001 && (C = 0), Math.abs(x) < .001 && (x = 0), Math.abs(w) < .001 && (w = 0), Math.abs(y) < .001 && (y = 0), Math.abs(b) < .001 && (b = 0);
                        var T = "translate3d(" + C + "px," + x + "px," + w + "px)  rotateX(" + b + "deg) rotateY(" + y + "deg)";
                        if (f.transform(T), f[0].style.zIndex = 1 - Math.abs(Math.round(v)), o.slideShadows) {
                            var E = a ? f.find(".swiper-slide-shadow-left") : f.find(".swiper-slide-shadow-top"),
                                S = a ? f.find(".swiper-slide-shadow-right") : f.find(".swiper-slide-shadow-bottom");
                            0 === E.length && (E = t('<div class="swiper-slide-shadow-' + (a ? "left" : "top") + '"></div>'), f.append(E)), 0 === S.length && (S = t('<div class="swiper-slide-shadow-' + (a ? "right" : "bottom") + '"></div>'), f.append(S)), E.length && (E[0].style.opacity = v > 0 ? v : 0), S.length && (S[0].style.opacity = -v > 0 ? -v : 0)
                        }
                    }
                    if (m.ie) {
                        s[0].style.perspectiveOrigin = h + "px 50%"
                    }
                },
                setTransition: function (t) {
                    this.slides.transition(t).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(t)
                }
            },
            vt = {
                name: "effect-coverflow",
                params: {
                    coverflowEffect: {
                        rotate: 50,
                        stretch: 0,
                        depth: 100,
                        modifier: 1,
                        slideShadows: !0
                    }
                },
                create: function () {
                    a.extend(this, {
                        coverflowEffect: {
                            setTranslate: gt.setTranslate.bind(this),
                            setTransition: gt.setTransition.bind(this)
                        }
                    })
                },
                on: {
                    beforeInit: function () {
                        "coverflow" === this.params.effect && (this.classNames.push(this.params.containerModifierClass + "coverflow"), this.classNames.push(this.params.containerModifierClass + "3d"), this.params.watchSlidesProgress = !0, this.originalParams.watchSlidesProgress = !0)
                    },
                    setTranslate: function () {
                        "coverflow" === this.params.effect && this.coverflowEffect.setTranslate()
                    },
                    setTransition: function (t) {
                        "coverflow" === this.params.effect && this.coverflowEffect.setTransition(t)
                    }
                }
            };
        return z.use([M, D, I, P, L, A, H, W, q, U, Y, V, K, J, et, nt, rt, at, ht, ct, pt, mt, vt]), z
    });
var coverVid = function (t, e, i) {
    function n() {
        var n = t.parentNode.offsetHeight,
            s = t.parentNode.offsetWidth;
        s / e > n / i ? (t.style.height = "auto", t.style.width = s + "px") : (t.style.height = n + "px", t.style.width = "auto")
    }
    n(), window.addEventListener("resize", function (t, e) {
        var i = null;
        return function () {
            var n = this,
                s = arguments;
            window.clearTimeout(i), i = window.setTimeout(function () {
                t.apply(n, s)
            }, e)
        }
    }(n, 50)), t.style.position = "absolute", t.style.top = "50%", t.style.left = "50%", t.style["-webkit-transform"] = "translate(-50%, -50%)", t.style["-ms-transform"] = "translate(-50%, -50%)", t.style.transform = "translate(-50%, -50%)", t.parentNode.style.overflow = "hidden";
    var s = t.getAttribute("poster");
    t.removeAttribute("poster"), t.parentNode.style.backgroundImage = "url(" + s + ")", t.parentNode.style.backgroundSize = "cover", t.parentNode.style.backgroundPosition = "center center";
    var r = void 0 !== t.canPlayType,
        o = !1;
    (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) && (o = !0), r && !o || t && t.parentNode && t.parentNode.removeChild(t)
};
window.jQuery && jQuery.fn.extend({
    coverVid: function () {
        return coverVid(this[0], arguments[0], arguments[1]), this
    }
});


// fin bundle
// Init primary naviagtion





jQuery(document).ready(function($) {

    jQuery('.stellarnav').stellarNav({

        theme: 'light',

        sticky: false,

        position:'left',

        closeBtn:false,

        showArrows:true,

    });

});









// init sticky js for nav in layout three



$(document).ready(function() {

    $(".fixed-nav").sticky({ topSpacing: 0 });

});













// Init swiper for layout one banner



var swiper = new Swiper('.swiper-container', {

    slidesPerView: 3,

    spaceBetween: 0,

    freeMode: false,

    nav: true,

    loop: true,



    autoplay: {

        delay: 3000,

    },



    navigation: {

        nextEl: '.swiper-button-next',

        prevEl: '.swiper-button-prev',

    },



    pagination: {

        el: '.swiper-pagination',

        clickable: true,

    },



    breakpoints: {

        1024: {

          slidesPerView: 2,

          spaceBetween: 0,

        },

        768: {

          slidesPerView: 2,

          spaceBetween: 0,

        },

        640: {

          slidesPerView: 2,

          spaceBetween: 0,

        },

        450: {

          slidesPerView: 1,

          spaceBetween: 0,

        }

      },

});







//  init swiper for layout two



var swiper = new Swiper('.banner-style-two-container', {

    slidesPerView: 1,

    spaceBetween: 0,

    freeMode: true,

    nav: true,

    loop: true,



    autoplay: {

        delay: 5000,

    },



    navigation: {

        nextEl: '.swiper-button-next',

        prevEl: '.swiper-button-prev',

    },



    pagination: {

        el: '.swiper-pagination',

        clickable: true,

    },

});





// init swiper for inpost gallery 



var swiper = new Swiper('.inpostgallery-container', {

    slidesPerView: 'auto',

    centeredSlides: false,

    spaceBetween: 0,

    nav: true,

    slidesPerView: 1,

    effect: 'slide',

    autoplay: {

        delay: 2000,

    },

    pagination: false,



    navigation: {

        nextEl: '.swiper-button-next',

        prevEl: '.swiper-button-prev',

    },



});





// init swiper for sidebar recent post gallery



var swiper = new Swiper('.widget-rpag-gallery-container', {

    slidesPerView: 1,

    centeredSlides: false,

    spaceBetween: 0,

    nav: false,

    slidesPerView: 1,

    effect: 'slide',

    pagination: false,



    pagination: {

        el: '.swiper-pagination',

        clickable: true,

    },



});





// init social sharing network 



$(".share").jsSocials({

    shares: ["facebook", "twitter", "email", "pinterest", "pocket", "googleplus", "linkedin"],

    showLabel: false,

    showCount: false,

    shareIn: "popup",

});





// Init masonary for home layout four





$('.masonry-grid').masonry({

    itemSelector: '.grid-item',

});

setTimeout(function() {

    $('.masonry-grid').masonry({

        itemSelector: '.grid-item',

    });

}, 5000);





// init slider ( owl 2 ) for home layout four 



var owl2 = $('.banner-style-four-container');

owl2.owlCarousel({

    items: 3,

    loop: true,

    margin: 10,

    nav: true,

    rtl: false,

    dots: false,

    autoplay: true,

    autoplayTimeout: 3000,

    autoplayHoverPause: true,

    navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],

    responsive: {

        0: {

            items: 1

        },

        768: {

            items: 2

        },

        992: {

            items: 2

        },

        1024: {



            items: 2

        },

        1200: {

            items: 3

        }

    }

});





// init owl for related post layout one



var owl3 = $('.related-post-carousel');

owl3.owlCarousel({

    items: 3,

    loop: true,

    margin: 10,

    nav: true,

    dots: false,

    autoplay: true,

    autoplayTimeout: 4000,

    autoplayHoverPause: true,

    navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],

    responsive: {

        0: {

            items: 1

        },

        768: {

            items: 2

        },

        992: {

            items: 3

        },

        1200: {

            items: 3

        }

    }

});



// Owl for related post with 3 items for full layout



var owl4 = $('.related-post-carousel-three-cards');

owl4.owlCarousel({

    items: 3,

    loop: true,

    margin: 40,

    nav: true,

    dots: false,

    autoplay: true,

    autoplayTimeout: 4000,

    autoplayHoverPause: true,

    navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],

    responsive: {

        0: {

            items: 1

        },

        768: {

            items: 2

        },

        992: {

            items: 3

        },

        1200: {

            items: 3

        }

    }

});





// instagram js init



jQuery(document).ready(function($) {

    var feed = new Instafeed({

        get: 'user',

        userId: '6688021399',

        accessToken: '6688021399.1677ed0.442755678e994865b8d8d15e70ccc131',

        resolution: 'standard_resolution',



        template: '<div class="item"><a href="{{link}}"><img src="{{image}}"><span><i class="fa fa-instagram" aria-hidden="true"></i></span></a></div>',

        target: 'instafeed',

        after: function() {

            $('.feed-carousel').owlCarousel({

                items: 6,

                loop: true,

                margin: 0,

                autoplay: true,

                autoplayTimeout: 3000,

                nav: false,



                responsive: {

                    0: {

                        items: 1

                    },

                    768: {

                        items: 3

                    },

                    992: {

                        items: 6

                    },

                    1200: {

                        items: 6

                    }

                }

            });



        }

    });

    feed.run();

});




// Comment box init



window.onload = (function() {

    var form = document.querySelector('.comments__form');



    // form.addEventListener('submit', function(e) {

    //     e.preventDefault();

    // });



})();

// fin scripts 

(function(global) {
  "use strict";

  /* Set up a RequestAnimationFrame shim so we can animate efficiently FOR
   * GREAT JUSTICE. */
  var requestInterval, cancelInterval;

  (function() {
    var raf = global.requestAnimationFrame       ||
              global.webkitRequestAnimationFrame ||
              global.mozRequestAnimationFrame    ||
              global.oRequestAnimationFrame      ||
              global.msRequestAnimationFrame     ,
        caf = global.cancelAnimationFrame        ||
              global.webkitCancelAnimationFrame  ||
              global.mozCancelAnimationFrame     ||
              global.oCancelAnimationFrame       ||
              global.msCancelAnimationFrame      ;

    if(raf && caf) {
      requestInterval = function(fn, delay) {
        var handle = {value: null};

        function loop() {
          handle.value = raf(loop);
          fn();
        }

        loop();
        return handle;
      };

      cancelInterval = function(handle) {
        caf(handle.value);
      };
    }

    else {
      requestInterval = setInterval;
      cancelInterval = clearInterval;
    }
  }());
  /* Define skycon things. */
  /* FIXME: I'm *really really* sorry that this code is so gross. Really, I am.
   * I'll try to clean it up eventually! Promise! */
  var KEYFRAME = 500,
      STROKE = 0.08,
      TAU = 2.0 * Math.PI,
      TWO_OVER_SQRT_2 = 2.0 / Math.sqrt(2);

  function circle(ctx, x, y, r) {
    ctx.beginPath();
    ctx.arc(x, y, r, 0, TAU, false);
    ctx.fill();
  }

  function line(ctx, ax, ay, bx, by) {
    ctx.beginPath();
    ctx.moveTo(ax, ay);
    ctx.lineTo(bx, by);
    ctx.stroke();
  }

  function puff(ctx, t, cx, cy, rx, ry, rmin, rmax) {
    var c = Math.cos(t * TAU),
        s = Math.sin(t * TAU);

    rmax -= rmin;

    circle(
      ctx,
      cx - s * rx,
      cy + c * ry + rmax * 0.5,
      rmin + (1 - c * 0.5) * rmax
    );
  }

  function puffs(ctx, t, cx, cy, rx, ry, rmin, rmax) {
    var i;

    for(i = 5; i--; )
      puff(ctx, t + i / 5, cx, cy, rx, ry, rmin, rmax);
  }

  function cloud(ctx, t, cx, cy, cw, s, color) {
    t /= 30000;

    var a = cw * 0.21,
        b = cw * 0.12,
        c = cw * 0.24,
        d = cw * 0.28;

    ctx.fillStyle = color;
    puffs(ctx, t, cx, cy, a, b, c, d);

    ctx.globalCompositeOperation = 'destination-out';
    puffs(ctx, t, cx, cy, a, b, c - s, d - s);
    ctx.globalCompositeOperation = 'source-over';
  }

  function sun(ctx, t, cx, cy, cw, s, color) {
    t /= 40000;

    var a = cw * 0.25 - s * 0.5,
        b = cw * 0.32 + s * 0.5,
        c = cw * 0.50 - s * 0.5,
        i, p, cos, sin;

    ctx.strokeStyle = color;
    ctx.lineWidth = s;
    ctx.lineCap = "round";
    ctx.lineJoin = "round";

    ctx.beginPath();
    ctx.arc(cx, cy, a, 0, TAU, false);
    ctx.stroke();

    for(i = 8; i--; ) {
      p = (t + i / 8) * TAU;
      cos = Math.cos(p);
      sin = Math.sin(p);
      line(ctx, cx + cos * b, cy + sin * b, cx + cos * c, cy + sin * c);
    }
  }

  function moon(ctx, t, cx, cy, cw, s, color) {
    t /= 15000;

    var a = cw * 0.29 - s * 0.5,
        b = cw * 0.05,
        c = Math.cos(t * TAU),
        p = c * TAU / -16;

    ctx.strokeStyle = color;
    ctx.lineWidth = s;
    ctx.lineCap = "round";
    ctx.lineJoin = "round";

    cx += c * b;

    ctx.beginPath();
    ctx.arc(cx, cy, a, p + TAU / 8, p + TAU * 7 / 8, false);
    ctx.arc(cx + Math.cos(p) * a * TWO_OVER_SQRT_2, cy + Math.sin(p) * a * TWO_OVER_SQRT_2, a, p + TAU * 5 / 8, p + TAU * 3 / 8, true);
    ctx.closePath();
    ctx.stroke();
  }

  function rain(ctx, t, cx, cy, cw, s, color) {
    t /= 1350;

    var a = cw * 0.16,
        b = TAU * 11 / 12,
        c = TAU *  7 / 12,
        i, p, x, y;

    ctx.fillStyle = color;

    for(i = 4; i--; ) {
      p = (t + i / 4) % 1;
      x = cx + ((i - 1.5) / 1.5) * (i === 1 || i === 2 ? -1 : 1) * a;
      y = cy + p * p * cw;
      ctx.beginPath();
      ctx.moveTo(x, y - s * 1.5);
      ctx.arc(x, y, s * 0.75, b, c, false);
      ctx.fill();
    }
  }

  function sleet(ctx, t, cx, cy, cw, s, color) {
    t /= 750;

    var a = cw * 0.1875,
        b = TAU * 11 / 12,
        c = TAU *  7 / 12,
        i, p, x, y;

    ctx.strokeStyle = color;
    ctx.lineWidth = s * 0.5;
    ctx.lineCap = "round";
    ctx.lineJoin = "round";

    for(i = 4; i--; ) {
      p = (t + i / 4) % 1;
      x = Math.floor(cx + ((i - 1.5) / 1.5) * (i === 1 || i === 2 ? -1 : 1) * a) + 0.5;
      y = cy + p * cw;
      line(ctx, x, y - s * 1.5, x, y + s * 1.5);
    }
  }

  function snow(ctx, t, cx, cy, cw, s, color) {
    t /= 3000;

    var a  = cw * 0.16,
        b  = s * 0.75,
        u  = t * TAU * 0.7,
        ux = Math.cos(u) * b,
        uy = Math.sin(u) * b,
        v  = u + TAU / 3,
        vx = Math.cos(v) * b,
        vy = Math.sin(v) * b,
        w  = u + TAU * 2 / 3,
        wx = Math.cos(w) * b,
        wy = Math.sin(w) * b,
        i, p, x, y;

    ctx.strokeStyle = color;
    ctx.lineWidth = s * 0.5;
    ctx.lineCap = "round";
    ctx.lineJoin = "round";

    for(i = 4; i--; ) {
      p = (t + i / 4) % 1;
      x = cx + Math.sin((p + i / 4) * TAU) * a;
      y = cy + p * cw;

      line(ctx, x - ux, y - uy, x + ux, y + uy);
      line(ctx, x - vx, y - vy, x + vx, y + vy);
      line(ctx, x - wx, y - wy, x + wx, y + wy);
    }
  }

  function fogbank(ctx, t, cx, cy, cw, s, color) {
    t /= 30000;

    var a = cw * 0.21,
        b = cw * 0.06,
        c = cw * 0.21,
        d = cw * 0.28;

    ctx.fillStyle = color;
    puffs(ctx, t, cx, cy, a, b, c, d);

    ctx.globalCompositeOperation = 'destination-out';
    puffs(ctx, t, cx, cy, a, b, c - s, d - s);
    ctx.globalCompositeOperation = 'source-over';
  }

  /*
  var WIND_PATHS = [
        downsample(63, upsample(8, [
          -1.00, -0.28,
          -0.75, -0.18,
          -0.50,  0.12,
          -0.20,  0.12,
          -0.04, -0.04,
          -0.07, -0.18,
          -0.19, -0.18,
          -0.23, -0.05,
          -0.12,  0.11,
           0.02,  0.16,
           0.20,  0.15,
           0.50,  0.07,
           0.75,  0.18,
           1.00,  0.28
        ])),
        downsample(31, upsample(16, [
          -1.00, -0.10,
          -0.75,  0.00,
          -0.50,  0.10,
          -0.25,  0.14,
           0.00,  0.10,
           0.25,  0.00,
           0.50, -0.10,
           0.75, -0.14,
           1.00, -0.10
        ]))
      ];
  */

  var WIND_PATHS = [
        [
          -0.7500, -0.1800, -0.7219, -0.1527, -0.6971, -0.1225,
          -0.6739, -0.0910, -0.6516, -0.0588, -0.6298, -0.0262,
          -0.6083,  0.0065, -0.5868,  0.0396, -0.5643,  0.0731,
          -0.5372,  0.1041, -0.5033,  0.1259, -0.4662,  0.1406,
          -0.4275,  0.1493, -0.3881,  0.1530, -0.3487,  0.1526,
          -0.3095,  0.1488, -0.2708,  0.1421, -0.2319,  0.1342,
          -0.1943,  0.1217, -0.1600,  0.1025, -0.1290,  0.0785,
          -0.1012,  0.0509, -0.0764,  0.0206, -0.0547, -0.0120,
          -0.0378, -0.0472, -0.0324, -0.0857, -0.0389, -0.1241,
          -0.0546, -0.1599, -0.0814, -0.1876, -0.1193, -0.1964,
          -0.1582, -0.1935, -0.1931, -0.1769, -0.2157, -0.1453,
          -0.2290, -0.1085, -0.2327, -0.0697, -0.2240, -0.0317,
          -0.2064,  0.0033, -0.1853,  0.0362, -0.1613,  0.0672,
          -0.1350,  0.0961, -0.1051,  0.1213, -0.0706,  0.1397,
          -0.0332,  0.1512,  0.0053,  0.1580,  0.0442,  0.1624,
           0.0833,  0.1636,  0.1224,  0.1615,  0.1613,  0.1565,
           0.1999,  0.1500,  0.2378,  0.1402,  0.2749,  0.1279,
           0.3118,  0.1147,  0.3487,  0.1015,  0.3858,  0.0892,
           0.4236,  0.0787,  0.4621,  0.0715,  0.5012,  0.0702,
           0.5398,  0.0766,  0.5768,  0.0890,  0.6123,  0.1055,
           0.6466,  0.1244,  0.6805,  0.1440,  0.7147,  0.1630,
           0.7500,  0.1800
        ],
        [
          -0.7500,  0.0000, -0.7033,  0.0195, -0.6569,  0.0399,
          -0.6104,  0.0600, -0.5634,  0.0789, -0.5155,  0.0954,
          -0.4667,  0.1089, -0.4174,  0.1206, -0.3676,  0.1299,
          -0.3174,  0.1365, -0.2669,  0.1398, -0.2162,  0.1391,
          -0.1658,  0.1347, -0.1157,  0.1271, -0.0661,  0.1169,
          -0.0170,  0.1046,  0.0316,  0.0903,  0.0791,  0.0728,
           0.1259,  0.0534,  0.1723,  0.0331,  0.2188,  0.0129,
           0.2656, -0.0064,  0.3122, -0.0263,  0.3586, -0.0466,
           0.4052, -0.0665,  0.4525, -0.0847,  0.5007, -0.1002,
           0.5497, -0.1130,  0.5991, -0.1240,  0.6491, -0.1325,
           0.6994, -0.1380,  0.7500, -0.1400
        ]
      ],
      WIND_OFFSETS = [
        {start: 0.36, end: 0.11},
        {start: 0.56, end: 0.16}
      ];

  function leaf(ctx, t, x, y, cw, s, color) {
    var a = cw / 8,
        b = a / 3,
        c = 2 * b,
        d = (t % 1) * TAU,
        e = Math.cos(d),
        f = Math.sin(d);

    ctx.fillStyle = color;
    ctx.strokeStyle = color;
    ctx.lineWidth = s;
    ctx.lineCap = "round";
    ctx.lineJoin = "round";

    ctx.beginPath();
    ctx.arc(x        , y        , a, d          , d + Math.PI, false);
    ctx.arc(x - b * e, y - b * f, c, d + Math.PI, d          , false);
    ctx.arc(x + c * e, y + c * f, b, d + Math.PI, d          , true );
    ctx.globalCompositeOperation = 'destination-out';
    ctx.fill();
    ctx.globalCompositeOperation = 'source-over';
    ctx.stroke();
  }

  function swoosh(ctx, t, cx, cy, cw, s, index, total, color) {
    t /= 2500;

    var path = WIND_PATHS[index],
        a = (t + index - WIND_OFFSETS[index].start) % total,
        c = (t + index - WIND_OFFSETS[index].end  ) % total,
        e = (t + index                            ) % total,
        b, d, f, i;

    ctx.strokeStyle = color;
    ctx.lineWidth = s;
    ctx.lineCap = "round";
    ctx.lineJoin = "round";

    if(a < 1) {
      ctx.beginPath();

      a *= path.length / 2 - 1;
      b  = Math.floor(a);
      a -= b;
      b *= 2;
      b += 2;

      ctx.moveTo(
        cx + (path[b - 2] * (1 - a) + path[b    ] * a) * cw,
        cy + (path[b - 1] * (1 - a) + path[b + 1] * a) * cw
      );

      if(c < 1) {
        c *= path.length / 2 - 1;
        d  = Math.floor(c);
        c -= d;
        d *= 2;
        d += 2;

        for(i = b; i !== d; i += 2)
          ctx.lineTo(cx + path[i] * cw, cy + path[i + 1] * cw);

        ctx.lineTo(
          cx + (path[d - 2] * (1 - c) + path[d    ] * c) * cw,
          cy + (path[d - 1] * (1 - c) + path[d + 1] * c) * cw
        );
      }

      else
        for(i = b; i !== path.length; i += 2)
          ctx.lineTo(cx + path[i] * cw, cy + path[i + 1] * cw);

      ctx.stroke();
    }

    else if(c < 1) {
      ctx.beginPath();

      c *= path.length / 2 - 1;
      d  = Math.floor(c);
      c -= d;
      d *= 2;
      d += 2;

      ctx.moveTo(cx + path[0] * cw, cy + path[1] * cw);

      for(i = 2; i !== d; i += 2)
        ctx.lineTo(cx + path[i] * cw, cy + path[i + 1] * cw);

      ctx.lineTo(
        cx + (path[d - 2] * (1 - c) + path[d    ] * c) * cw,
        cy + (path[d - 1] * (1 - c) + path[d + 1] * c) * cw
      );

      ctx.stroke();
    }

    if(e < 1) {
      e *= path.length / 2 - 1;
      f  = Math.floor(e);
      e -= f;
      f *= 2;
      f += 2;

      leaf(
        ctx,
        t,
        cx + (path[f - 2] * (1 - e) + path[f    ] * e) * cw,
        cy + (path[f - 1] * (1 - e) + path[f + 1] * e) * cw,
        cw,
        s,
        color
      );
    }
  }

  var Skycons = function(opts) {
        this.list        = [];
        this.interval    = null;
        this.color       = opts && opts.color ? opts.color : "black";
        this.resizeClear = !!(opts && opts.resizeClear);
      };

  Skycons.CLEAR_DAY = function(ctx, t, color) {
    var w = ctx.canvas.width,
        h = ctx.canvas.height,
        s = Math.min(w, h);

    sun(ctx, t, w * 0.5, h * 0.5, s, s * STROKE, color);
  };

  Skycons.CLEAR_NIGHT = function(ctx, t, color) {
    var w = ctx.canvas.width,
        h = ctx.canvas.height,
        s = Math.min(w, h);

    moon(ctx, t, w * 0.5, h * 0.5, s, s * STROKE, color);
  };

  Skycons.PARTLY_CLOUDY_DAY = function(ctx, t, color) {
    var w = ctx.canvas.width,
        h = ctx.canvas.height,
        s = Math.min(w, h);

    sun(ctx, t, w * 0.625, h * 0.375, s * 0.75, s * STROKE, color);
    cloud(ctx, t, w * 0.375, h * 0.625, s * 0.75, s * STROKE, color);
  };

  Skycons.PARTLY_CLOUDY_NIGHT = function(ctx, t, color) {
    var w = ctx.canvas.width,
        h = ctx.canvas.height,
        s = Math.min(w, h);

    moon(ctx, t, w * 0.667, h * 0.375, s * 0.75, s * STROKE, color);
    cloud(ctx, t, w * 0.375, h * 0.625, s * 0.75, s * STROKE, color);
  };

  Skycons.CLOUDY = function(ctx, t, color) {
    var w = ctx.canvas.width,
        h = ctx.canvas.height,
        s = Math.min(w, h);

    cloud(ctx, t, w * 0.5, h * 0.5, s, s * STROKE, color);
  };

  Skycons.RAIN = function(ctx, t, color) {
    var w = ctx.canvas.width,
        h = ctx.canvas.height,
        s = Math.min(w, h);

    rain(ctx, t, w * 0.5, h * 0.37, s * 0.9, s * STROKE, color);
    cloud(ctx, t, w * 0.5, h * 0.37, s * 0.9, s * STROKE, color);
  };

  Skycons.SLEET = function(ctx, t, color) {
    var w = ctx.canvas.width,
        h = ctx.canvas.height,
        s = Math.min(w, h);

    sleet(ctx, t, w * 0.5, h * 0.37, s * 0.9, s * STROKE, color);
    cloud(ctx, t, w * 0.5, h * 0.37, s * 0.9, s * STROKE, color);
  };

  Skycons.SNOW = function(ctx, t, color) {
    var w = ctx.canvas.width,
        h = ctx.canvas.height,
        s = Math.min(w, h);

    snow(ctx, t, w * 0.5, h * 0.37, s * 0.9, s * STROKE, color);
    cloud(ctx, t, w * 0.5, h * 0.37, s * 0.9, s * STROKE, color);
  };

  Skycons.WIND = function(ctx, t, color) {
    var w = ctx.canvas.width,
        h = ctx.canvas.height,
        s = Math.min(w, h);

    swoosh(ctx, t, w * 0.5, h * 0.5, s, s * STROKE, 0, 2, color);
    swoosh(ctx, t, w * 0.5, h * 0.5, s, s * STROKE, 1, 2, color);
  };

  Skycons.FOG = function(ctx, t, color) {
    var w = ctx.canvas.width,
        h = ctx.canvas.height,
        s = Math.min(w, h),
        k = s * STROKE;

    fogbank(ctx, t, w * 0.5, h * 0.32, s * 0.75, k, color);

    t /= 5000;

    var a = Math.cos((t       ) * TAU) * s * 0.02,
        b = Math.cos((t + 0.25) * TAU) * s * 0.02,
        c = Math.cos((t + 0.50) * TAU) * s * 0.02,
        d = Math.cos((t + 0.75) * TAU) * s * 0.02,
        n = h * 0.936,
        e = Math.floor(n - k * 0.5) + 0.5,
        f = Math.floor(n - k * 2.5) + 0.5;

    ctx.strokeStyle = color;
    ctx.lineWidth = k;
    ctx.lineCap = "round";
    ctx.lineJoin = "round";

    line(ctx, a + w * 0.2 + k * 0.5, e, b + w * 0.8 - k * 0.5, e);
    line(ctx, c + w * 0.2 + k * 0.5, f, d + w * 0.8 - k * 0.5, f);
  };

  Skycons.prototype = {
    _determineDrawingFunction: function(draw) {
      if(typeof draw === "string")
        draw = Skycons[draw.toUpperCase().replace(/-/g, "_")] || null;

      return draw;
    },
    add: function(el, draw) {
      var obj;

      if(typeof el === "string")
        el = document.getElementById(el);

      // Does nothing if canvas name doesn't exists
      if(el === null)
        return;

      draw = this._determineDrawingFunction(draw);

      // Does nothing if the draw function isn't actually a function
      if(typeof draw !== "function")
        return;

      obj = {
        element: el,
        context: el.getContext("2d"),
        drawing: draw
      };

      this.list.push(obj);
      this.draw(obj, KEYFRAME);
    },
    set: function(el, draw) {
      var i;

      if(typeof el === "string")
        el = document.getElementById(el);

      for(i = this.list.length; i--; )
        if(this.list[i].element === el) {
          this.list[i].drawing = this._determineDrawingFunction(draw);
          this.draw(this.list[i], KEYFRAME);
          return;
        }

      this.add(el, draw);
    },
    remove: function(el) {
      var i;

      if(typeof el === "string")
        el = document.getElementById(el);

      for(i = this.list.length; i--; )
        if(this.list[i].element === el) {
          this.list.splice(i, 1);
          return;
        }
    },
    draw: function(obj, time) {
      var canvas = obj.context.canvas;

      if(this.resizeClear)
        canvas.width = canvas.width;

      else
        obj.context.clearRect(0, 0, canvas.width, canvas.height);

      obj.drawing(obj.context, time, this.color);
    },
    play: function() {
      var self = this;

      this.pause();
      this.interval = requestInterval(function() {
        var now = Date.now(),
            i;

        for(i = self.list.length; i--; )
          self.draw(self.list[i], now);
      }, 1000 / 60);
    },
    pause: function() {
      var i;

      if(this.interval) {
        cancelInterval(this.interval);
        this.interval = null;
      }
    }
  };

  global.Skycons = Skycons;
}(this));


// fin skycons 