<?php 

return array(
	'name' => 'Newspaper   ',
	'color' => 'Blue',
	'category' => 'Newspaper Blog',
	'theme'  => 'Newspaper',
	'libraries' => array('Classie', 'Animated Header', 'Bootstrap Validation', 'Jquery Easing'),
);