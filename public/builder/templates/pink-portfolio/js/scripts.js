
(function( jQuery, window, undefined ) {
// See http://bugs.jquery.com/ticket/13335
// "use strict";


var warnedAbout = {};

// List of warnings already given; public read only
jQuery.migrateWarnings = [];

// Set to true to prevent console output; migrateWarnings still maintained
// jQuery.migrateMute = false;

// Show a message on the console so devs know we're active
if ( !jQuery.migrateMute && window.console && window.console.log ) {
  window.console.log("JQMIGRATE: Logging is active");
}

// Set to false to disable traces that appear with warnings
if ( jQuery.migrateTrace === undefined ) {
  jQuery.migrateTrace = true;
}

// Forget any warnings we've already given; public
jQuery.migrateReset = function() {
  warnedAbout = {};
  jQuery.migrateWarnings.length = 0;
};

function migrateWarn( msg) {
  var console = window.console;
  if ( !warnedAbout[ msg ] ) {
    warnedAbout[ msg ] = true;
    jQuery.migrateWarnings.push( msg );
    if ( console && console.warn && !jQuery.migrateMute ) {
      console.warn( "JQMIGRATE: " + msg );
      if ( jQuery.migrateTrace && console.trace ) {
        console.trace();
      }
    }
  }
}

function migrateWarnProp( obj, prop, value, msg ) {
  if ( Object.defineProperty ) {
    // On ES5 browsers (non-oldIE), warn if the code tries to get prop;
    // allow property to be overwritten in case some other plugin wants it
    try {
      Object.defineProperty( obj, prop, {
        configurable: true,
        enumerable: true,
        get: function() {
          migrateWarn( msg );
          return value;
        },
        set: function( newValue ) {
          migrateWarn( msg );
          value = newValue;
        }
      });
      return;
    } catch( err ) {
      // IE8 is a dope about Object.defineProperty, can't warn there
    }
  }

  // Non-ES5 (or broken) browser; just set the property
  jQuery._definePropertyBroken = true;
  obj[ prop ] = value;
}

if ( document.compatMode === "BackCompat" ) {
  // jQuery has never supported or tested Quirks Mode
  migrateWarn( "jQuery is not compatible with Quirks Mode" );
}


var attrFn = jQuery( "<input/>", { size: 1 } ).attr("size") && jQuery.attrFn,
  oldAttr = jQuery.attr,
  valueAttrGet = jQuery.attrHooks.value && jQuery.attrHooks.value.get ||
    function() { return null; },
  valueAttrSet = jQuery.attrHooks.value && jQuery.attrHooks.value.set ||
    function() { return undefined; },
  rnoType = /^(?:input|button)$/i,
  rnoAttrNodeType = /^[238]$/,
  rboolean = /^(?:autofocus|autoplay|async|checked|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped|selected)$/i,
  ruseDefault = /^(?:checked|selected)$/i;

// jQuery.attrFn
migrateWarnProp( jQuery, "attrFn", attrFn || {}, "jQuery.attrFn is deprecated" );

jQuery.attr = function( elem, name, value, pass ) {
  var lowerName = name.toLowerCase(),
    nType = elem && elem.nodeType;

  if ( pass ) {
    // Since pass is used internally, we only warn for new jQuery
    // versions where there isn't a pass arg in the formal params
    if ( oldAttr.length < 4 ) {
      migrateWarn("jQuery.fn.attr( props, pass ) is deprecated");
    }
    if ( elem && !rnoAttrNodeType.test( nType ) &&
      (attrFn ? name in attrFn : jQuery.isFunction(jQuery.fn[name])) ) {
      return jQuery( elem )[ name ]( value );
    }
  }

  // Warn if user tries to set `type`, since it breaks on IE 6/7/8; by checking
  // for disconnected elements we don't warn on $( "<button>", { type: "button" } ).
  if ( name === "type" && value !== undefined && rnoType.test( elem.nodeName ) && elem.parentNode ) {
    migrateWarn("Can't change the 'type' of an input or button in IE 6/7/8");
  }

  // Restore boolHook for boolean property/attribute synchronization
  if ( !jQuery.attrHooks[ lowerName ] && rboolean.test( lowerName ) ) {
    jQuery.attrHooks[ lowerName ] = {
      get: function( elem, name ) {
        // Align boolean attributes with corresponding properties
        // Fall back to attribute presence where some booleans are not supported
        var attrNode,
          property = jQuery.prop( elem, name );
        return property === true || typeof property !== "boolean" &&
          ( attrNode = elem.getAttributeNode(name) ) && attrNode.nodeValue !== false ?

          name.toLowerCase() :
          undefined;
      },
      set: function( elem, value, name ) {
        var propName;
        if ( value === false ) {
          // Remove boolean attributes when set to false
          jQuery.removeAttr( elem, name );
        } else {
          // value is true since we know at this point it's type boolean and not false
          // Set boolean attributes to the same name and set the DOM property
          propName = jQuery.propFix[ name ] || name;
          if ( propName in elem ) {
            // Only set the IDL specifically if it already exists on the element
            elem[ propName ] = true;
          }

          elem.setAttribute( name, name.toLowerCase() );
        }
        return name;
      }
    };

    // Warn only for attributes that can remain distinct from their properties post-1.9
    if ( ruseDefault.test( lowerName ) ) {
      migrateWarn( "jQuery.fn.attr('" + lowerName + "') may use property instead of attribute" );
    }
  }

  return oldAttr.call( jQuery, elem, name, value );
};

// attrHooks: value
jQuery.attrHooks.value = {
  get: function( elem, name ) {
    var nodeName = ( elem.nodeName || "" ).toLowerCase();
    if ( nodeName === "button" ) {
      return valueAttrGet.apply( this, arguments );
    }
    if ( nodeName !== "input" && nodeName !== "option" ) {
      migrateWarn("jQuery.fn.attr('value') no longer gets properties");
    }
    return name in elem ?
      elem.value :
      null;
  },
  set: function( elem, value ) {
    var nodeName = ( elem.nodeName || "" ).toLowerCase();
    if ( nodeName === "button" ) {
      return valueAttrSet.apply( this, arguments );
    }
    if ( nodeName !== "input" && nodeName !== "option" ) {
      migrateWarn("jQuery.fn.attr('value', val) no longer sets properties");
    }
    // Does not return so that setAttribute is also used
    elem.value = value;
  }
};


var matched, browser,
  oldInit = jQuery.fn.init,
  oldParseJSON = jQuery.parseJSON,
  // Note: XSS check is done below after string is trimmed
  rquickExpr = /^([^<]*)(<[\w\W]+>)([^>]*)$/;

// $(html) "looks like html" rule change
jQuery.fn.init = function( selector, context, rootjQuery ) {
  var match;

  if ( selector && typeof selector === "string" && !jQuery.isPlainObject( context ) &&
      (match = rquickExpr.exec( jQuery.trim( selector ) )) && match[ 0 ] ) {
    // This is an HTML string according to the "old" rules; is it still?
    if ( selector.charAt( 0 ) !== "<" ) {
      migrateWarn("$(html) HTML strings must start with '<' character");
    }
    if ( match[ 3 ] ) {
      migrateWarn("$(html) HTML text after last tag is ignored");
    }
    // Consistently reject any HTML-like string starting with a hash (#9521)
    // Note that this may break jQuery 1.6.x code that otherwise would work.
    if ( match[ 0 ].charAt( 0 ) === "#" ) {
      migrateWarn("HTML string cannot start with a '#' character");
      jQuery.error("JQMIGRATE: Invalid selector string (XSS)");
    }
    // Now process using loose rules; let pre-1.8 play too
    if ( context && context.context ) {
      // jQuery object as context; parseHTML expects a DOM object
      context = context.context;
    }
    if ( jQuery.parseHTML ) {
      return oldInit.call( this, jQuery.parseHTML( match[ 2 ], context, true ),
          context, rootjQuery );
    }
  }
  return oldInit.apply( this, arguments );
};
jQuery.fn.init.prototype = jQuery.fn;

// Let $.parseJSON(falsy_value) return null
jQuery.parseJSON = function( json ) {
  if ( !json && json !== null ) {
    migrateWarn("jQuery.parseJSON requires a valid JSON string");
    return null;
  }
  return oldParseJSON.apply( this, arguments );
};

jQuery.uaMatch = function( ua ) {
  ua = ua.toLowerCase();

  var match = /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
    /(webkit)[ \/]([\w.]+)/.exec( ua ) ||
    /(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
    /(msie) ([\w.]+)/.exec( ua ) ||
    ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
    [];

  return {
    browser: match[ 1 ] || "",
    version: match[ 2 ] || "0"
  };
};

// Don't clobber any existing jQuery.browser in case it's different
if ( !jQuery.browser ) {
  matched = jQuery.uaMatch( navigator.userAgent );
  browser = {};

  if ( matched.browser ) {
    browser[ matched.browser ] = true;
    browser.version = matched.version;
  }

  // Chrome is Webkit, but Webkit is also Safari.
  if ( browser.chrome ) {
    browser.webkit = true;
  } else if ( browser.webkit ) {
    browser.safari = true;
  }

  jQuery.browser = browser;
}

// Warn if the code tries to get jQuery.browser
migrateWarnProp( jQuery, "browser", jQuery.browser, "jQuery.browser is deprecated" );

jQuery.sub = function() {
  function jQuerySub( selector, context ) {
    return new jQuerySub.fn.init( selector, context );
  }
  jQuery.extend( true, jQuerySub, this );
  jQuerySub.superclass = this;
  jQuerySub.fn = jQuerySub.prototype = this();
  jQuerySub.fn.constructor = jQuerySub;
  jQuerySub.sub = this.sub;
  jQuerySub.fn.init = function init( selector, context ) {
    if ( context && context instanceof jQuery && !(context instanceof jQuerySub) ) {
      context = jQuerySub( context );
    }

    return jQuery.fn.init.call( this, selector, context, rootjQuerySub );
  };
  jQuerySub.fn.init.prototype = jQuerySub.fn;
  var rootjQuerySub = jQuerySub(document);
  migrateWarn( "jQuery.sub() is deprecated" );
  return jQuerySub;
};


// Ensure that $.ajax gets the new parseJSON defined in core.js
jQuery.ajaxSetup({
  converters: {
    "text json": jQuery.parseJSON
  }
});


var oldFnData = jQuery.fn.data;

jQuery.fn.data = function( name ) {
  var ret, evt,
    elem = this[0];

  // Handles 1.7 which has this behavior and 1.8 which doesn't
  if ( elem && name === "events" && arguments.length === 1 ) {
    ret = jQuery.data( elem, name );
    evt = jQuery._data( elem, name );
    if ( ( ret === undefined || ret === evt ) && evt !== undefined ) {
      migrateWarn("Use of jQuery.fn.data('events') is deprecated");
      return evt;
    }
  }
  return oldFnData.apply( this, arguments );
};


var rscriptType = /\/(java|ecma)script/i,
  oldSelf = jQuery.fn.andSelf || jQuery.fn.addBack;

jQuery.fn.andSelf = function() {
  migrateWarn("jQuery.fn.andSelf() replaced by jQuery.fn.addBack()");
  return oldSelf.apply( this, arguments );
};

// Since jQuery.clean is used internally on older versions, we only shim if it's missing
if ( !jQuery.clean ) {
  jQuery.clean = function( elems, context, fragment, scripts ) {
    // Set context per 1.8 logic
    context = context || document;
    context = !context.nodeType && context[0] || context;
    context = context.ownerDocument || context;

    migrateWarn("jQuery.clean() is deprecated");

    var i, elem, handleScript, jsTags,
      ret = [];

    jQuery.merge( ret, jQuery.buildFragment( elems, context ).childNodes );

    // Complex logic lifted directly from jQuery 1.8
    if ( fragment ) {
      // Special handling of each script element
      handleScript = function( elem ) {
        // Check if we consider it executable
        if ( !elem.type || rscriptType.test( elem.type ) ) {
          // Detach the script and store it in the scripts array (if provided) or the fragment
          // Return truthy to indicate that it has been handled
          return scripts ?
            scripts.push( elem.parentNode ? elem.parentNode.removeChild( elem ) : elem ) :
            fragment.appendChild( elem );
        }
      };

      for ( i = 0; (elem = ret[i]) != null; i++ ) {
        // Check if we're done after handling an executable script
        if ( !( jQuery.nodeName( elem, "script" ) && handleScript( elem ) ) ) {
          // Append to fragment and handle embedded scripts
          fragment.appendChild( elem );
          if ( typeof elem.getElementsByTagName !== "undefined" ) {
            // handleScript alters the DOM, so use jQuery.merge to ensure snapshot iteration
            jsTags = jQuery.grep( jQuery.merge( [], elem.getElementsByTagName("script") ), handleScript );

            // Splice the scripts into ret after their former ancestor and advance our index beyond them
            ret.splice.apply( ret, [i + 1, 0].concat( jsTags ) );
            i += jsTags.length;
          }
        }
      }
    }

    return ret;
  };
}

var eventAdd = jQuery.event.add,
  eventRemove = jQuery.event.remove,
  eventTrigger = jQuery.event.trigger,
  oldToggle = jQuery.fn.toggle,
  oldLive = jQuery.fn.live,
  oldDie = jQuery.fn.die,
  ajaxEvents = "ajaxStart|ajaxStop|ajaxSend|ajaxComplete|ajaxError|ajaxSuccess",
  rajaxEvent = new RegExp( "\\b(?:" + ajaxEvents + ")\\b" ),
  rhoverHack = /(?:^|\s)hover(\.\S+|)\b/,
  hoverHack = function( events ) {
    if ( typeof( events ) !== "string" || jQuery.event.special.hover ) {
      return events;
    }
    if ( rhoverHack.test( events ) ) {
      migrateWarn("'hover' pseudo-event is deprecated, use 'mouseenter mouseleave'");
    }
    return events && events.replace( rhoverHack, "mouseenter$1 mouseleave$1" );
  };

// Event props removed in 1.9, put them back if needed; no practical way to warn them
if ( jQuery.event.props && jQuery.event.props[ 0 ] !== "attrChange" ) {
  jQuery.event.props.unshift( "attrChange", "attrName", "relatedNode", "srcElement" );
}

// Undocumented jQuery.event.handle was "deprecated" in jQuery 1.7
if ( jQuery.event.dispatch ) {
  migrateWarnProp( jQuery.event, "handle", jQuery.event.dispatch, "jQuery.event.handle is undocumented and deprecated" );
}

// Support for 'hover' pseudo-event and ajax event warnings
jQuery.event.add = function( elem, types, handler, data, selector ){
  if ( elem !== document && rajaxEvent.test( types ) ) {
    migrateWarn( "AJAX events should be attached to document: " + types );
  }
  eventAdd.call( this, elem, hoverHack( types || "" ), handler, data, selector );
};
jQuery.event.remove = function( elem, types, handler, selector, mappedTypes ){
  eventRemove.call( this, elem, hoverHack( types ) || "", handler, selector, mappedTypes );
};

jQuery.fn.error = function() {
  var args = Array.prototype.slice.call( arguments, 0);
  migrateWarn("jQuery.fn.error() is deprecated");
  args.splice( 0, 0, "error" );
  if ( arguments.length ) {
    return this.bind.apply( this, args );
  }
  // error event should not bubble to window, although it does pre-1.7
  this.triggerHandler.apply( this, args );
  return this;
};

jQuery.fn.toggle = function( fn, fn2 ) {

  // Don't mess with animation or css toggles
  if ( !jQuery.isFunction( fn ) || !jQuery.isFunction( fn2 ) ) {
    return oldToggle.apply( this, arguments );
  }
  migrateWarn("jQuery.fn.toggle(handler, handler...) is deprecated");

  // Save reference to arguments for access in closure
  var args = arguments,
    guid = fn.guid || jQuery.guid++,
    i = 0,
    toggler = function( event ) {
      // Figure out which function to execute
      var lastToggle = ( jQuery._data( this, "lastToggle" + fn.guid ) || 0 ) % i;
      jQuery._data( this, "lastToggle" + fn.guid, lastToggle + 1 );

      // Make sure that clicks stop
      event.preventDefault();

      // and execute the function
      return args[ lastToggle ].apply( this, arguments ) || false;
    };

  // link all the functions, so any of them can unbind this click handler
  toggler.guid = guid;
  while ( i < args.length ) {
    args[ i++ ].guid = guid;
  }

  return this.click( toggler );
};

jQuery.fn.live = function( types, data, fn ) {
  migrateWarn("jQuery.fn.live() is deprecated");
  if ( oldLive ) {
    return oldLive.apply( this, arguments );
  }
  jQuery( this.context ).on( types, this.selector, data, fn );
  return this;
};

jQuery.fn.die = function( types, fn ) {
  migrateWarn("jQuery.fn.die() is deprecated");
  if ( oldDie ) {
    return oldDie.apply( this, arguments );
  }
  jQuery( this.context ).off( types, this.selector || "**", fn );
  return this;
};

// Turn global events into document-triggered events
jQuery.event.trigger = function( event, data, elem, onlyHandlers  ){
  if ( !elem && !rajaxEvent.test( event ) ) {
    migrateWarn( "Global events are undocumented and deprecated" );
  }
  return eventTrigger.call( this,  event, data, elem || document, onlyHandlers  );
};
jQuery.each( ajaxEvents.split("|"),
  function( _, name ) {
    jQuery.event.special[ name ] = {
      setup: function() {
        var elem = this;

        // The document needs no shimming; must be !== for oldIE
        if ( elem !== document ) {
          jQuery.event.add( document, name + "." + jQuery.guid, function() {
            jQuery.event.trigger( name, null, elem, true );
          });
          jQuery._data( this, name, jQuery.guid++ );
        }
        return false;
      },
      teardown: function() {
        if ( this !== document ) {
          jQuery.event.remove( document, name + "." + jQuery._data( this, name ) );
        }
        return false;
      }
    };
  }
);


})( jQuery, window );


// fin jq migrate 

// Smooth scroll blocking
document.addEventListener( 'DOMContentLoaded', function() {
	if ( 'onwheel' in document ) {
		window.onwheel = function( event ) {
			if( typeof( this.RDSmoothScroll ) !== undefined ) {
				try { window.removeEventListener( 'DOMMouseScroll', this.RDSmoothScroll.prototype.onWheel ); } catch( error ) {}
				event.stopPropagation();
			}
		};
	} else if ( 'onmousewheel' in document ) {
		window.onmousewheel= function( event ) {
			if( typeof( this.RDSmoothScroll ) !== undefined ) {
				try { window.removeEventListener( 'onmousewheel', this.RDSmoothScroll.prototype.onWheel ); } catch( error ) {}
				event.stopPropagation();
			}
		};
	}

	try { $('body').unmousewheel(); } catch( error ) {}
});
$(function(){
// IPad/IPhone
  var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
  ua = navigator.userAgent,

  gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

  scaleFix = function () {
    if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
      viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
      document.addEventListener("gesturestart", gestureStart, false);
    }
  };
  
  scaleFix();
  // Menu Android
  if(window.orientation!=undefined){
  var regM = /ipod|ipad|iphone/gi,
   result = ua.match(regM)
  if(!result) {
   $('.sf-menu li').each(function(){
    if($(">ul", this)[0]){
     $(">a", this).toggle(
      function(){
       return false;
      },
      function(){
       window.location.href = $(this).attr("href");
      }
     );
    } 
   })
  }
 }
});
var ua=navigator.userAgent.toLocaleLowerCase(),
 regV = /ipod|ipad|iphone/gi,
 result = ua.match(regV),
 userScale="";
if(!result){
 userScale=",user-scalable=0"
}
document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0'+userScale+'">')

var currentYear = (new Date).getFullYear();
  $(document).ready(function() {
  $("#copyright-year").text( (new Date).getFullYear() );
  });

  $(function(){
  $('.sf-menu').superfish({autoArrows: true})
})

  // fin scripts 

  

;(function ($) {
  "use strict";

  var methods = (function () {
    // private properties and methods go here
    var c = {
        bcClass: 'sf-breadcrumb',
        menuClass: 'sf-js-enabled',
        anchorClass: 'sf-with-ul',
        menuArrowClass: 'sf-arrows'
      },
      ios = (function () {
        var ios = /iPhone|iPad|iPod/i.test(navigator.userAgent);
        if (ios) {
          // iOS clicks only bubble as far as body children
          $(window).load(function () {
            $('body').children().on('click', $.noop);
          });
        }
        return ios;
      })(),
      wp7 = (function () {
        var style = document.documentElement.style;
        return ('behavior' in style && 'fill' in style && /iemobile/i.test(navigator.userAgent));
      })(),
      toggleMenuClasses = function ($menu, o) {
        var classes = c.menuClass;
        if (o.cssArrows) {
          classes += ' ' + c.menuArrowClass;
        }
        $menu.toggleClass(classes);
      },
      setPathToCurrent = function ($menu, o) {
        return $menu.find('li.' + o.pathClass).slice(0, o.pathLevels)
          .addClass(o.hoverClass + ' ' + c.bcClass)
            .filter(function () {
              return ($(this).children(o.popUpSelector).hide().show().length);
            }).removeClass(o.pathClass);
      },
      toggleAnchorClass = function ($li) {
        $li.children('a').toggleClass(c.anchorClass);
      },
      toggleTouchAction = function ($menu) {
        var touchAction = $menu.css('ms-touch-action');
        touchAction = (touchAction === 'pan-y') ? 'auto' : 'pan-y';
        $menu.css('ms-touch-action', touchAction);
      },
      applyHandlers = function ($menu, o) {
        var targets = 'li:has(' + o.popUpSelector + ')';
        if ($.fn.hoverIntent && !o.disableHI) {
          $menu.hoverIntent(over, out, targets);
        }
        else {
          $menu
            .on('mouseenter.superfish', targets, over)
            .on('mouseleave.superfish', targets, out);
        }
        var touchevent = 'MSPointerDown.superfish';
        if (!ios) {
          touchevent += ' touchend.superfish';
        }
        if (wp7) {
          touchevent += ' mousedown.superfish';
        }
        $menu
          .on('focusin.superfish', 'li', over)
          .on('focusout.superfish', 'li', out)
          .on(touchevent, 'a', o, touchHandler);
      },
      touchHandler = function (e) {
        var $this = $(this),
          $ul = $this.siblings(e.data.popUpSelector);

        if ($ul.length > 0 && $ul.is(':hidden')) {
          $this.one('click.superfish', false);
          if (e.type === 'MSPointerDown') {
            $this.trigger('focus');
          } else {
            $.proxy(over, $this.parent('li'))();
          }
        }
      },
      over = function () {
        var $this = $(this),
          o = getOptions($this);
        clearTimeout(o.sfTimer);
        $this.siblings().superfish('hide').end().superfish('show');
      },
      out = function () {
        var $this = $(this),
          o = getOptions($this);
        if (ios) {
          $.proxy(close, $this, o)();
        }
        else {
          clearTimeout(o.sfTimer);
          o.sfTimer = setTimeout($.proxy(close, $this, o), o.delay);
        }
      },
      close = function (o) {
        o.retainPath = ($.inArray(this[0], o.$path) > -1);
        this.superfish('hide');

        if (!this.parents('.' + o.hoverClass).length) {
          o.onIdle.call(getMenu(this));
          if (o.$path.length) {
            $.proxy(over, o.$path)();
          }
        }
      },
      getMenu = function ($el) {
        return $el.closest('.' + c.menuClass);
      },
      getOptions = function ($el) {
        return getMenu($el).data('sf-options');
      };

    return {
      // public methods
      hide: function (instant) {
        if (this.length) {
          var $this = this,
            o = getOptions($this);
          if (!o) {
            return this;
          }
          var not = (o.retainPath === true) ? o.$path : '',
            $ul = $this.find('li.' + o.hoverClass).add(this).not(not).removeClass(o.hoverClass).children(o.popUpSelector),
            speed = o.speedOut;

          if (instant) {
            $ul.show();
            speed = 0;
          }
          o.retainPath = false;
          o.onBeforeHide.call($ul);
          $ul.stop(true, true).animate(o.animationOut, speed, function () {
            var $this = $(this);
            o.onHide.call($this);
          });
        }
        return this;
      },
      show: function () {
        var o = getOptions(this);
        if (!o) {
          return this;
        }
        var $this = this.addClass(o.hoverClass),
          $ul = $this.children(o.popUpSelector);

        o.onBeforeShow.call($ul);
        $ul.stop(true, true).animate(o.animation, o.speed, function () {
          o.onShow.call($ul);
        });
        return this;
      },
      destroy: function () {
        return this.each(function () {
          var $this = $(this),
            o = $this.data('sf-options'),
            $hasPopUp;
          if (!o) {
            return false;
          }
          $hasPopUp = $this.find(o.popUpSelector).parent('li');
          clearTimeout(o.sfTimer);
          toggleMenuClasses($this, o);
          toggleAnchorClass($hasPopUp);
          toggleTouchAction($this);
          // remove event handlers
          $this.off('.superfish').off('.hoverIntent');
          // clear animation's inline display style
          $hasPopUp.children(o.popUpSelector).attr('style', function (i, style) {
            return style.replace(/display[^;]+;?/g, '');
          });
          // reset 'current' path classes
          o.$path.removeClass(o.hoverClass + ' ' + c.bcClass).addClass(o.pathClass);
          $this.find('.' + o.hoverClass).removeClass(o.hoverClass);
          o.onDestroy.call($this);
          $this.removeData('sf-options');
        });
      },
      init: function (op) {
        return this.each(function () {
          var $this = $(this);
          if ($this.data('sf-options')) {
            return false;
          }
          var o = $.extend({}, $.fn.superfish.defaults, op),
            $hasPopUp = $this.find(o.popUpSelector).parent('li');
          o.$path = setPathToCurrent($this, o);

          $this.data('sf-options', o);

          toggleMenuClasses($this, o);
          toggleAnchorClass($hasPopUp);
          toggleTouchAction($this);
          applyHandlers($this, o);

          $hasPopUp.not('.' + c.bcClass).superfish('hide', true);

          o.onInit.call(this);
        });
      }
    };
  })();

  $.fn.superfish = function (method, args) {
    if (methods[method]) {
      return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
    }
    else if (typeof method === 'object' || ! method) {
      return methods.init.apply(this, arguments);
    }
    else {
      return $.error('Method ' +  method + ' does not exist on jQuery.fn.superfish');
    }
  };

  $.fn.superfish.defaults = {
    popUpSelector: 'ul,.sf-mega', // within menu context
    hoverClass: 'sfHover',
    pathClass: 'overrideThisToUse',
    pathLevels: 1,
    delay: 800,
    animation: {opacity: 'show'},
    animationOut: {opacity: 'hide'},
    speed: 'normal',
    speedOut: 'fast',
    cssArrows: true,
    disableHI: false,
    onInit: $.noop,
    onBeforeShow: $.noop,
    onShow: $.noop,
    onBeforeHide: $.noop,
    onHide: $.noop,
    onIdle: $.noop,
    onDestroy: $.noop
  };

  // soon to be deprecated
  $.fn.extend({
    hideSuperfishUl: methods.hide,
    showSuperfishUl: methods.show
  });

})(jQuery);


// fin superfish 

/*parsed HTML*/
      
$(function(){
  $(".maxheight").each(function(){
    $(this).contents().wrapAll("<div class='box_inner'></div>");
  })
  $(".maxheight1").each(function(){
    $(this).contents().wrapAll("<div class='box_inner'></div>");
  })
  $(".maxheight2").each(function(){
    $(this).contents().wrapAll("<div class='box_inner'></div>");
  })
})
/*add event*/
$(window).bind("resize", height_handler).bind("load", height_handler)
function height_handler(){
  if($(window).width()>767){
    $(".maxheight").equalHeights();
  }else{
    $(".maxheight").css({'height':'auto'});
  }
  if($(window).width()>767){
    $(".maxheight1").equalHeights();
  }else{
    $(".maxheight1").css({'height':'auto'});
  }
  if($(window).width()>767){
    $(".maxheight2").equalHeights();
  }else{
    $(".maxheight2").css({'height':'auto'});
  }
}
/*glob function*/
(function($){
  $.fn.equalHeights=function(minHeight,maxHeight){
    tallest=(minHeight)?minHeight:0;
    this.each(function(){
      if($(">.box_inner", this).outerHeight()>tallest){
        tallest=$(">.box_inner", this).outerHeight()
      }
    });
    if((maxHeight)&&tallest>maxHeight) tallest=maxHeight;
    return this.each(function(){$(this).height(tallest)})
  }
})(jQuery)


// fin equal heights 

/**
 * jQuery Mobile Menu 
 * Turn unordered list menu into dropdown select menu
 * version 1.1(27-JULY-2013)
 * 
 * Built on top of the jQuery library
 *   http://jquery.com
 * 
 * Documentation
 *   http://github.com/mambows/mobilemenu
 */
;(function($){
$.fn.mobileMenu = function(options) {
  
  var defaults = {
      defaultText: 'Navigate to...',
      className: 'select-menu',
      subMenuClass: 'sub-menu',
      subMenuDash: '&ndash;'
    },
    settings = $.extend( defaults, options ),
    el = $(this);
  
  this.each(function(){
    var $el = $(this),
      $select_menu;

    // ad class to submenu list
    $el.find('ul').addClass(settings.subMenuClass);

    // Create base menu
    var $select_menu = $('<select />',{
      'class' : settings.className + ' ' + el.get(0).className
    }).insertAfter( $el );

    // Create default option
    $('<option />', {
      "value"   : '#',
      "text"    : settings.defaultText
    }).appendTo( $select_menu );

    // Create select option from menu
    $el.find('a').each(function(){
      var $this   = $(this),
        optText = '&nbsp;' + $this.text(),
        optSub  = $this.parents( '.' + settings.subMenuClass ),
        len   = optSub.length,
        dash;
      
      // if menu has sub menu
      if( $this.parents('ul').hasClass( settings.subMenuClass ) ) {
        dash = Array( len+1 ).join( settings.subMenuDash );
        optText = dash + optText;
      }

      // Now build menu and append it
      $('<option />', {
        "value" : this.href,
        "html"  : optText,
        "selected" : (this.href == window.location.href)
      }).appendTo( $select_menu );

    }); // End el.find('a').each

    // Change event on select element
    $select_menu.change(function(){
      var locations = $(this).val();
      if( locations !== '#' ) {
        window.location.href = $(this).val();
      };
    });
    $('.select-menu').show();
  }); // End this.each

  return this;

};
})(jQuery);
$(document).ready(function(){
  $('.sf-menu').mobileMenu();
});

// fin mobile menu .js 

/*
 * jQuery Easing v1.3 - http://gsgd.co.uk/sandbox/jquery/easing/
 *
 * Uses the built in easing capabilities added In jQuery 1.1
 * to offer multiple easing options
 *
 * TERMS OF USE - jQuery Easing
 * 
 * Open source under the BSD License. 
 * 
 * Copyright © 2008 George McGinley Smith
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 
 * Redistributions of source code must retain the above copyright notice, this list of 
 * conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list 
 * of conditions and the following disclaimer in the documentation and/or other materials 
 * provided with the distribution.
 * 
 * Neither the name of the author nor the names of contributors may be used to endorse 
 * or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED 
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE. 
 *
*/

// t: current time, b: begInnIng value, c: change In value, d: duration
jQuery.easing['jswing'] = jQuery.easing['swing'];

jQuery.extend( jQuery.easing,
{
  def: 'easeOutQuad',
  swing: function (x, t, b, c, d) {
    //alert(jQuery.easing.default);
    return jQuery.easing[jQuery.easing.def](x, t, b, c, d);
  },
  easeInQuad: function (x, t, b, c, d) {
    return c*(t/=d)*t + b;
  },
  easeOutQuad: function (x, t, b, c, d) {
    return -c *(t/=d)*(t-2) + b;
  },
  easeInOutQuad: function (x, t, b, c, d) {
    if ((t/=d/2) < 1) return c/2*t*t + b;
    return -c/2 * ((--t)*(t-2) - 1) + b;
  },
  easeInCubic: function (x, t, b, c, d) {
    return c*(t/=d)*t*t + b;
  },
  easeOutCubic: function (x, t, b, c, d) {
    return c*((t=t/d-1)*t*t + 1) + b;
  },
  easeInOutCubic: function (x, t, b, c, d) {
    if ((t/=d/2) < 1) return c/2*t*t*t + b;
    return c/2*((t-=2)*t*t + 2) + b;
  },
  easeInQuart: function (x, t, b, c, d) {
    return c*(t/=d)*t*t*t + b;
  },
  easeOutQuart: function (x, t, b, c, d) {
    return -c * ((t=t/d-1)*t*t*t - 1) + b;
  },
  easeInOutQuart: function (x, t, b, c, d) {
    if ((t/=d/2) < 1) return c/2*t*t*t*t + b;
    return -c/2 * ((t-=2)*t*t*t - 2) + b;
  },
  easeInQuint: function (x, t, b, c, d) {
    return c*(t/=d)*t*t*t*t + b;
  },
  easeOutQuint: function (x, t, b, c, d) {
    return c*((t=t/d-1)*t*t*t*t + 1) + b;
  },
  easeInOutQuint: function (x, t, b, c, d) {
    if ((t/=d/2) < 1) return c/2*t*t*t*t*t + b;
    return c/2*((t-=2)*t*t*t*t + 2) + b;
  },
  easeInSine: function (x, t, b, c, d) {
    return -c * Math.cos(t/d * (Math.PI/2)) + c + b;
  },
  easeOutSine: function (x, t, b, c, d) {
    return c * Math.sin(t/d * (Math.PI/2)) + b;
  },
  easeInOutSine: function (x, t, b, c, d) {
    return -c/2 * (Math.cos(Math.PI*t/d) - 1) + b;
  },
  easeInExpo: function (x, t, b, c, d) {
    return (t==0) ? b : c * Math.pow(2, 10 * (t/d - 1)) + b;
  },
  easeOutExpo: function (x, t, b, c, d) {
    return (t==d) ? b+c : c * (-Math.pow(2, -10 * t/d) + 1) + b;
  },
  easeInOutExpo: function (x, t, b, c, d) {
    if (t==0) return b;
    if (t==d) return b+c;
    if ((t/=d/2) < 1) return c/2 * Math.pow(2, 10 * (t - 1)) + b;
    return c/2 * (-Math.pow(2, -10 * --t) + 2) + b;
  },
  easeInCirc: function (x, t, b, c, d) {
    return -c * (Math.sqrt(1 - (t/=d)*t) - 1) + b;
  },
  easeOutCirc: function (x, t, b, c, d) {
    return c * Math.sqrt(1 - (t=t/d-1)*t) + b;
  },
  easeInOutCirc: function (x, t, b, c, d) {
    if ((t/=d/2) < 1) return -c/2 * (Math.sqrt(1 - t*t) - 1) + b;
    return c/2 * (Math.sqrt(1 - (t-=2)*t) + 1) + b;
  },
  easeInElastic: function (x, t, b, c, d) {
    var s=1.70158;var p=0;var a=c;
    if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
    if (a < Math.abs(c)) { a=c; var s=p/4; }
    else var s = p/(2*Math.PI) * Math.asin (c/a);
    return -(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
  },
  easeOutElastic: function (x, t, b, c, d) {
    var s=1.70158;var p=0;var a=c;
    if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
    if (a < Math.abs(c)) { a=c; var s=p/4; }
    else var s = p/(2*Math.PI) * Math.asin (c/a);
    return a*Math.pow(2,-10*t) * Math.sin( (t*d-s)*(2*Math.PI)/p ) + c + b;
  },
  easeInOutElastic: function (x, t, b, c, d) {
    var s=1.70158;var p=0;var a=c;
    if (t==0) return b;  if ((t/=d/2)==2) return b+c;  if (!p) p=d*(.3*1.5);
    if (a < Math.abs(c)) { a=c; var s=p/4; }
    else var s = p/(2*Math.PI) * Math.asin (c/a);
    if (t < 1) return -.5*(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
    return a*Math.pow(2,-10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )*.5 + c + b;
  },
  easeInBack: function (x, t, b, c, d, s) {
    if (s == undefined) s = 1.70158;
    return c*(t/=d)*t*((s+1)*t - s) + b;
  },
  easeOutBack: function (x, t, b, c, d, s) {
    if (s == undefined) s = 1.70158;
    return c*((t=t/d-1)*t*((s+1)*t + s) + 1) + b;
  },
  easeInOutBack: function (x, t, b, c, d, s) {
    if (s == undefined) s = 1.70158; 
    if ((t/=d/2) < 1) return c/2*(t*t*(((s*=(1.525))+1)*t - s)) + b;
    return c/2*((t-=2)*t*(((s*=(1.525))+1)*t + s) + 2) + b;
  },
  easeInBounce: function (x, t, b, c, d) {
    return c - jQuery.easing.easeOutBounce (x, d-t, 0, c, d) + b;
  },
  easeOutBounce: function (x, t, b, c, d) {
    if ((t/=d) < (1/2.75)) {
      return c*(7.5625*t*t) + b;
    } else if (t < (2/2.75)) {
      return c*(7.5625*(t-=(1.5/2.75))*t + .75) + b;
    } else if (t < (2.5/2.75)) {
      return c*(7.5625*(t-=(2.25/2.75))*t + .9375) + b;
    } else {
      return c*(7.5625*(t-=(2.625/2.75))*t + .984375) + b;
    }
  },
  easeInOutBounce: function (x, t, b, c, d) {
    if (t < d/2) return jQuery.easing.easeInBounce (x, t*2, 0, c, d) * .5 + b;
    return jQuery.easing.easeOutBounce (x, t*2-d, 0, c, d) * .5 + c*.5 + b;
  }
});

/*
 *
 * TERMS OF USE - EASING EQUATIONS
 * 
 * Open source under the BSD License. 
 * 
 * Copyright © 2001 Robert Penner
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 
 * Redistributions of source code must retain the above copyright notice, this list of 
 * conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list 
 * of conditions and the following disclaimer in the documentation and/or other materials 
 * provided with the distribution.
 * 
 * Neither the name of the author nor the names of contributors may be used to endorse 
 * or promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED 
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE. 
 *
 */

 // fin jquery easing 

 (function($){
  $.fn.tmStickUp=function(options){ 
    
    var getOptions = {
      corectionValue: 0
    }
    $.extend(getOptions, options); 

    var
      _this = $(this)
    , _window = $(window)
    , _document = $(document)
    , thisOffsetTop = 0
    , thisOuterHeight = 0
    , thisMarginTop = 0
    , documentScroll = 0
    , pseudoBlock
    , lastScrollValue = 0
    , scrollDir = ''
    , tmpScrolled
    , gridWidth = 0
    , tmpTop = 0
    ;

    init();
    function init(){
      parentContainer = _this.parent();
      thisOffsetTop = parseInt(_this.offset().top);
      thisMarginTop = parseInt(_this.css("margin-top"));
      thisOuterHeight = parseInt(_this.outerHeight(true));
      gridWidth = parseInt($('.container').width());

      $('<div class="pseudoStickyBlock"></div>').insertAfter(_this);
      pseudoBlock = $('.pseudoStickyBlock');
      pseudoBlock.css({"position":"relative", "display":"block"});

      _this.on("rePosition",
        function(e,d){
          tmpTop = d;
          _document.trigger('scroll');
        }
      )

      addEventsFunction();
    }//end init

    function addEventsFunction(){
      _document.on('scroll', function() {

        tmpScrolled = $(this).scrollTop();
          if (tmpScrolled > lastScrollValue){
            scrollDir = 'down';
          } else {
            scrollDir = 'up';
          }
        lastScrollValue = tmpScrolled;

        documentScroll = parseInt(_document.scrollTop());
        if(thisOffsetTop-thisMarginTop < documentScroll){
          _this.addClass('isStuck');
          _this.css({position:"fixed",top:tmpTop, zIndex:999, left:0, right:0, margin:"0 auto"})
          pseudoBlock.css({"height":thisOuterHeight});
        }else{
          _this.removeClass('isStuck');
          _this.css({position:"relative", top: 0});
          pseudoBlock.css({"height":0});
        }
        
      }).trigger('scroll');
    }
  }//end tmStickUp function
})(jQuery)


// fin tmstick up 

;(function($){
  $.fn.UItoTop = function(options) {

    var defaults = {
      text: '',
      min: 200,     
      scrollSpeed: 800,
        containerID: 'toTop',
      containerHoverID: 'toTopHover',
      easingType: 'linear',
      min_width:parseInt($('body').css("min-width"),10),
      main_width:parseInt($('body').css("min-width"),10)/2
          
    };

    var settings = $.extend(defaults, options);
    var containerIDhash = '#' + settings.containerID;
    var containerHoverIDHash = '#'+settings.containerHoverID;
      
    $('body').append('<a href="#" id="'+settings.containerID+'">'+settings.text+'</a>');
    
    var button_width = parseInt($(containerIDhash).css("width"))+90
    var button_width_1 = parseInt($(containerIDhash).css("width"))+20
    var max_width = defaults.min_width+button_width;
    var margin_right_1 = -(defaults.main_width+button_width_1)
    var margin_right_2 = -(defaults.main_width-20)
    
    function top(){
      if(($(window).width()<=max_width)&&($(window).width()>=defaults.min_width))$(containerIDhash).stop().animate({marginRight:margin_right_2,right:'50%'})
      else if($(window).width()<=defaults.min_width)$(containerIDhash).stop().css({marginRight:0,right:10})
      else $(containerIDhash).stop().animate({marginRight:margin_right_1,right:'50%'})
    }
    top()
    $(containerIDhash).hide().click(function(){     
      $('html, body').stop().animate({scrollTop:0}, settings.scrollSpeed, settings.easingType);
      $('#'+settings.containerHoverID, this).stop().animate({'opacity': 0 }, settings.inDelay, settings.easingType);
      return false;
    })
    
    .prepend('<span id="'+settings.containerHoverID+'"></span>')
    .hover(function() {
        $(containerHoverIDHash, this).stop().animate({
          'opacity': 1
        }, 600, 'linear');
      }, function() { 
        $(containerHoverIDHash, this).stop().animate({
          'opacity': 0
        }, 700, 'linear');
      });
                
    $(window).scroll(function() {
      var sd = $(window).scrollTop();
      if(typeof document.body.style.maxHeight === "undefined") {
        $(containerIDhash).css({
          'position': 'absolute',
          'top': $(window).scrollTop() + $(window).height() - 50
        });
      }
      if ( sd > settings.min ) 
        $(containerIDhash).css({display: 'block'});
      else 
        $(containerIDhash).css({display: 'none'});
    });
    $(window).resize(function(){top()})
};
})(jQuery);



// fin jq ui to top



/**
 * @name    jQuery touchTouch plugin
 * @author    Martin Angelov
 * @version   1.0
 * @url     http://tutorialzine.com/2012/04/mobile-touch-gallery/
 * @license   MIT License
 */


(function(){

  /* Private variables */
  
  var overlay = $('<div id="galleryOverlay">'),
    slider = $('<div id="gallerySlider">'),
    prevArrow = $('<a id="prevArrow"></a>'),
    nextArrow = $('<a id="nextArrow"></a>'),
    overlayVisible = false;
    
    
  /* Creating the plugin */
  
  $.fn.touchTouch = function(){

    var placeholders = $([]),
      index = 0,
      allitems = this,
      items = allitems;
    
    // Appending the markup to the page
    overlay.hide().appendTo('body');
    slider.appendTo(overlay);
    
    // Creating a placeholder for each image
    items.each(function(){

      placeholders = placeholders.add($('<div class="placeholder">'));
    });
  
    // Hide the gallery if the background is touched / clicked
    slider.append(placeholders).on('click',function(e){

      if(!$(e.target).is('img')){
        hideOverlay();
      }
    });
    
    // Listen for touch events on the body and check if they
    // originated in #gallerySlider img - the images in the slider.
    $('body').on('touchstart', '#gallerySlider img', function(e){
      
      var touch = e.originalEvent,
        startX = touch.changedTouches[0].pageX;
  
      slider.on('touchmove',function(e){
        
        e.preventDefault();
        
        touch = e.originalEvent.touches[0] ||
            e.originalEvent.changedTouches[0];
        
        if(touch.pageX - startX > 10){

          slider.off('touchmove');
          showPrevious();
        }

        else if (touch.pageX - startX < -10){

          slider.off('touchmove');
          showNext();
        }
      });

      // Return false to prevent image 
      // highlighting on Android
      return false;
      
    }).on('touchend',function(){

      slider.off('touchmove');

    });
    
    // Listening for clicks on the thumbnails
    items.on('click', function(e){

      e.preventDefault();

      var $this = $(this),
        galleryName,
        selectorType,
        $closestGallery = $this.parent().closest('[data-gallery]');

      // Find gallery name and change items object to only have 
      // that gallery

      //If gallery name given to each item
      if ($this.attr('data-gallery')) {

        galleryName = $this.attr('data-gallery');
        selectorType = 'item';

      //If gallery name given to some ancestor
      } else if ($closestGallery.length) {

        galleryName = $closestGallery.attr('data-gallery');
        selectorType = 'ancestor';

      }

      //These statements kept seperate in case elements have data-gallery on both
      //items and ancestor. Ancestor will always win because of above statments.
      if (galleryName && selectorType == 'item') {

        items = $('[data-gallery='+galleryName+']');

      } else if (galleryName && selectorType == 'ancestor') {

        //Filter to check if item has an ancestory with data-gallery attribute
        items = items.filter(function(){

                return $(this).parent().closest('[data-gallery]').length;    
                
              });

      }

      // Find the position of this image
      // in the collection
      index = items.index(this);
      showOverlay(index);
      showImage(index);
      
      // Preload the next image
      preload(index+1);
      
      // Preload the previous
      preload(index-1);
      
    });
    
    // If the browser does not have support 
    // for touch, display the arrows
    if ( !("ontouchstart" in window) ){
      overlay.append(prevArrow).append(nextArrow);
      
      prevArrow.click(function(e){
        e.preventDefault();
        showPrevious();
      });
      
      nextArrow.click(function(e){
        e.preventDefault();
        showNext();
      });
    }
    
    // Listen for arrow keys
    $(window).bind('keydown', function(e){
    
      if (e.keyCode == 37) {
        showPrevious();
      }

      else if (e.keyCode==39) {
        showNext();
      }
  
    });
    
    
    /* Private functions */
    
  
    function showOverlay(index){
      // If the overlay is already shown, exit
      if (overlayVisible){
        return false;
      }
      
      // Show the overlay
      overlay.show();
      
      setTimeout(function(){
        // Trigger the opacity CSS transition
        overlay.addClass('visible');
      }, 100);
  
      // Move the slider to the correct image
      offsetSlider(index);
      
      // Raise the visible flag
      overlayVisible = true;
    }
  
    function hideOverlay(){

      // If the overlay is not shown, exit
      if(!overlayVisible){
        return false;
      }
      
      // Hide the overlay
      overlay.hide().removeClass('visible');
      overlayVisible = false;

      //Clear preloaded items
      $('.placeholder').empty();

      //Reset possibly filtered items
      items = allitems;
    }
  
    function offsetSlider(index){

      // This will trigger a smooth css transition
      slider.css('left',(-index*100)+'%');
    }
  
    // Preload an image by its index in the items array
    function preload(index){

      setTimeout(function(){
        showImage(index);
      }, 1000);
    }
    
    // Show image in the slider
    function showImage(index){
  
      // If the index is outside the bonds of the array
      if(index < 0 || index >= items.length){
        return false;
      }
      
      // Call the load function with the href attribute of the item
      loadImage(items.eq(index).attr('href'), function(){
        placeholders.eq(index).html(this);
      });
    }
    
    // Load the image and execute a callback function.
    // Returns a jQuery object
    
    function loadImage(src, callback){

      var img = $('<img>').on('load', function(){
        callback.call(img);
      });
      
      img.attr('src',src);
    }
    
    function showNext(){
      
      // If this is not the last image
      if(index+1 < items.length){
        index++;
        offsetSlider(index);
        preload(index+1);
      }

      else{
        // Trigger the spring animation
        slider.addClass('rightSpring');
        setTimeout(function(){
          slider.removeClass('rightSpring');
        },500);
      }
    }
    
    function showPrevious(){
      
      // If this is not the first image
      if(index>0){
        index--;
        offsetSlider(index);
        preload(index-1);
      }

      else{
        // Trigger the spring animation
        slider.addClass('leftSpring');
        setTimeout(function(){
          slider.removeClass('leftSpring');
        },500);
      }
    }
  };
  
})(jQuery);

// fin touch.js 

/*
 *  jQuery OwlCarousel v1.22
 *
 *  Copyright (c) 2013 Bartosz Wojciechowski
 *  http://www.owlgraphic.com/owlcarousel
 *
 *  Licensed under MIT
 *
 */


// Object.create function
if ( typeof Object.create !== "function" ) {
  Object.create = function( obj ) {
    function F() {};
    F.prototype = obj;
    return new F();
  };
}
(function( $, window, document, undefined ) {

  var Carousel = {
    init :function(options, el){
      var base = this;
      base.options = $.extend({}, $.fn.owlCarousel.options, options);
      var elem = el;
      var $elem = $(el);
      base.$elem = $elem;
      base.loadContent()
    },

    loadContent : function(){
      var base = this;

      if (typeof base.options.beforeInit === "function") {
        base.options.beforeInit.apply(this,[base.$elem]);
      }

      if (typeof base.options.jsonPath === "string") {
        var url = base.options.jsonPath;

        function getData(data) {
          if (typeof base.options.jsonSuccess === "function") {
            base.options.jsonSuccess.apply(this,[data]);
          } else {
            var content = "";
            for(var i in data["owl"]){
              content += data["owl"][i]["item"];
            }
            base.$elem.html(content);
          }
          base.logIn();
        }
        $.getJSON(url,getData);
      } else {
        base.logIn();
      }
    },

    logIn : function(){
      var base = this;

      base.baseClass();

      base.$elem
      .css({opacity: 0})

      base.checkTouch();
      base.eventTypes();
      base.support3d();


      base.wrapperWidth = 0;
      base.currentSlide = 0; //Starting Position

      base.userItems = base.$elem.children();
      base.itemsAmount = base.userItems.length;
      base.wrapItems();

      base.owlItems = base.$elem.find(".owl-item");
      base.owlWrapper = base.$elem.find(".owl-wrapper");

      base.orignalItems = base.options.items;
      base.playDirection = "next";

      base.checkVisible;

      //setTimeout(function(){
      base.onStartup();
      //},0);
      base.customEvents();

    },

    onStartup : function(){
      var base = this;
      base.updateItems();
      base.calculateAll();
      base.buildControls();
      base.updateControls();
      base.response();
      base.moveEvents();
      base.stopOnHover();
      if(base.options.autoPlay === true){
        base.options.autoPlay = 5000;
      }
      base.play();
      base.$elem.find(".owl-wrapper").css("display","block")

      if(!base.$elem.is(":visible")){
        base.watchVisibility();
      } else {
        setTimeout(function(){
          base.$elem.animate({opacity: 1},200);
        },10);
      }
      base.onstartup = false;
      base.eachMoveUpdate();
      if (typeof base.options.afterInit === "function") {
        base.options.afterInit.apply(this,[base.$elem]);
      }
    },

    eachMoveUpdate : function(){
      var base = this;
      if(base.options.lazyLoad === true){
        base.lazyLoad();
      }
      if(base.options.autoHeight === true){
        base.autoHeight();
      }
      if(base.options.addClassActive === true){
        base.addClassActive();
      }
      if (typeof base.options.afterAction === "function") {
        base.options.afterAction.apply(this,[base.$elem]);
      }
    },

    updateVars : function(){
      var base = this;
      base.watchVisibility();
      base.updateItems();
      base.calculateAll();
      base.updatePosition();
      base.updateControls();
      base.eachMoveUpdate();
    },

    reload : function(elements){
      var base = this;
      setTimeout(function(){
        base.updateVars();
      },0)
    },

    watchVisibility : function(){
      var base = this;
      clearInterval(base.checkVisible);
      if(!base.$elem.is(":visible")){
        base.$elem.css({opacity: 0});
        clearInterval(base.autoPlaySpeed);
      } else {
        return false;
      }
      base.checkVisible = setInterval(function(){
        if (base.$elem.is(":visible")) {
          base.reload();
          base.$elem.animate({opacity: 1},200);
          clearInterval(base.checkVisible);
        }
      }, 500);
    },

    wrapItems : function(){
      var base = this;
      base.userItems.wrapAll("<div class=\"owl-wrapper\">").wrap("<div class=\"owl-item\"></div>");
      base.$elem.find(".owl-wrapper").wrap("<div class=\"owl-wrapper-outer\">");
      base.wrapperOuter = base.$elem.find(".owl-wrapper-outer");
      base.$elem.css("display","block");
    },

    baseClass : function(){
      var base = this;
      var hasBaseClass = base.$elem.hasClass(base.options.baseClass);
      var hasThemeClass = base.$elem.hasClass(base.options.theme);

      if(!hasBaseClass){
        base.$elem.addClass(base.options.baseClass);
      }

      if(!hasThemeClass){
        base.$elem.addClass(base.options.theme);
      }
    },

    updateItems : function(){
      var base = this;

      if(base.options.responsive === false){
        return false;
      }

      if(base.options.singleItem === true){
        base.options.items = base.orignalItems = 1;
        base.options.itemsDesktop = false;
        base.options.itemsDesktopSmall = false;
        base.options.itemsTablet = false;
        base.options.itemsMobile = false;
        return false;
      }

      var width = $(window).width();

      if(width > (base.options.itemsDesktop[0] || base.orignalItems) ){
         base.options.items = base.orignalItems
      }

      if(width <= base.options.itemsDesktop[0] && base.options.itemsDesktop !== false){
        base.options.items = base.options.itemsDesktop[1];
      }

      if(width <= base.options.itemsDesktopSmall[0] && base.options.itemsDesktopSmall !== false){
        base.options.items = base.options.itemsDesktopSmall[1];
      }

      if(width <= base.options.itemsTablet[0]  && base.options.itemsTablet !== false){
        base.options.items = base.options.itemsTablet[1];
      }

      if(width <= base.options.itemsMobile[0] && base.options.itemsMobile !== false){
        base.options.items = base.options.itemsMobile[1];
      }

      //if number of items is less than declared
      if(base.options.items > base.itemsAmount){
        base.options.items = base.itemsAmount;
      }
    },

    response : function(){
      var base = this,
        smallDelay;
      if(base.options.responsive !== true){
        return false
      }
      var lastWindowWidth = $(window).width();
      $(window).resize(function(){
        if($(window).width() !== lastWindowWidth){
          if(base.options.autoPlay !== false){
            clearInterval(base.autoPlaySpeed);
          }
          clearTimeout(smallDelay);
          smallDelay = setTimeout(function(){
            lastWindowWidth = $(window).width();
            base.updateVars();
          },base.options.responsiveRefreshRate);
        }
      })
    },

    updatePosition : function(){
      var base = this;

      if(base.support3d === true){
        if(base.positionsInArray[base.currentSlide] > base.maximumPixels){
          base.transition3d(base.positionsInArray[base.currentSlide]);
        } else {
          base.transition3d(0);
          base.currentSlide = 0;
        }
      } else{
        if(base.positionsInArray[base.currentSlide] > base.maximumPixels){
          base.css2slide(base.positionsInArray[base.currentSlide]);
        } else {
          base.css2slide(0);
          base.currentSlide = 0;
        }
      }
      if(base.options.autoPlay !== false){
        base.checkAp();
      }
    },

    appendItemsSizes : function(){
      var base = this;

      var roundPages = 0;
      var lastItem = base.itemsAmount - base.options.items;

      base.owlItems.each(function(index){
        $(this)
        .css({"width": base.itemWidth})
        .data("owl-item",Number(index));

        if(index % base.options.items === 0 || index === lastItem){
          if(!(index > lastItem)){
            roundPages +=1;
          }
        }
        $(this).data("owl-roundPages",roundPages);
      });
    },

    appendWrapperSizes : function(){
      var base = this;
      var width = 0;

      var width = base.owlItems.length * base.itemWidth;

      base.owlWrapper.css({
        "width": width*2,
        "left": 0
      });
      base.appendItemsSizes();
    },

    calculateAll : function(){
      var base = this;
      base.calculateWidth();
      base.appendWrapperSizes();
      base.loops();
      base.max();
    },

    calculateWidth : function(){
      var base = this;
      base.itemWidth = Math.round(base.$elem.width()/base.options.items)
    },

    max : function(){
      var base = this;
      base.maximumSlide = base.itemsAmount - base.options.items;
      var maximum = (base.itemsAmount * base.itemWidth) - base.options.items * base.itemWidth;
        maximum = maximum * -1
      base.maximumPixels = maximum;
      return maximum;
    },

    min : function(){
      return 0;
    },

    loops : function(){
      var base = this;

      base.positionsInArray = [0];
      var elWidth = 0;

      for(var i = 0; i<base.itemsAmount; i++){
        elWidth += base.itemWidth;
        base.positionsInArray.push(-elWidth)
      }
    },

    buildControls : function(){
      var base = this;
      if(base.options.navigation === true || base.options.pagination === true){
        base.owlControls = $("<div class=\"owl-controls\"/>").toggleClass("clickable", !base.isTouch).appendTo(base.$elem);
      }
      if(base.options.pagination === true){
        base.buildPagination();
      }
      if(base.options.navigation === true){
        base.buildButtons();
      }
    },

    buildButtons : function(){
      var base = this;
      var buttonsWrapper = $("<div class=\"owl-buttons\"/>")
      base.owlControls.append(buttonsWrapper);

      base.buttonPrev = $("<div/>",{
        "class" : "owl-prev",
        "html" : base.options.navigationText[0] || ""
        });

      base.buttonNext = $("<div/>",{
        "class" : "owl-next",
        "html" : base.options.navigationText[1] || ""
        });

      buttonsWrapper
      .append(base.buttonPrev)
      .append(base.buttonNext);

      buttonsWrapper.on("touchend.owlControls mouseup.owlControls", "div[class^=\"owl\"]", function(event){
        event.preventDefault();
        if($(this).hasClass("owl-next")){
          base.next();
        } else{
          base.prev();
        }
      })
    },

    buildPagination : function(){
      var base = this;

      base.paginationWrapper = $("<div class=\"owl-pagination\"/>");
      base.owlControls.append(base.paginationWrapper);

      base.paginationWrapper.on("touchend.owlControls mouseup.owlControls", ".owl-page", function(event){
        event.preventDefault();
        if(Number($(this).data("owl-page")) !== base.currentSlide){
          base.goTo( Number($(this).data("owl-page")), true);
        }
      });
    },

    updatePagination : function(){
      var base = this;
      if(base.options.pagination === false){
        return false;
      }

      base.paginationWrapper.html("");

      var counter = 0;
      var lastPage = base.itemsAmount - base.itemsAmount % base.options.items;

      for(var i = 0; i<base.itemsAmount; i++){
        if(i % base.options.items === 0){
          counter +=1;
          if(lastPage === i){
            var lastItem = base.itemsAmount - base.options.items;
          }
          var paginationButton = $("<div/>",{
            "class" : "owl-page"
            });
          var paginationButtonInner = $("<span></span>",{
            "text": base.options.paginationNumbers === true ? counter : "",
            "class": base.options.paginationNumbers === true ? "owl-numbers" : ""
          });
          paginationButton.append(paginationButtonInner);

          paginationButton.data("owl-page",lastPage === i ? lastItem : i);
          paginationButton.data("owl-roundPages",counter);

          base.paginationWrapper.append(paginationButton);
        }
      }
      base.checkPagination();
    },
    checkPagination : function(){
      var base = this;

      base.paginationWrapper.find(".owl-page").each(function(i,v){
        if($(this).data("owl-roundPages") === $(base.owlItems[base.currentSlide]).data("owl-roundPages") ){
          base.paginationWrapper
            .find(".owl-page")
            .removeClass("active");
          $(this).addClass("active");
        }
      });
    },

    checkNavigation : function(){
      var base = this;

      if(base.options.navigation === false){
        return false;
      }
      if(base.options.goToFirstNav === false){
        if(base.currentSlide === 0 && base.maximumSlide === 0){
          base.buttonPrev.addClass("disabled");
          base.buttonNext.addClass("disabled");
        } else if(base.currentSlide === 0 && base.maximumSlide !== 0){
          base.buttonPrev.addClass("disabled");
          base.buttonNext.removeClass("disabled");
        } else if (base.currentSlide === base.maximumSlide){
          base.buttonPrev.removeClass("disabled");
          base.buttonNext.addClass("disabled");
        } else if(base.currentSlide !== 0 && base.currentSlide !== base.maximumSlide){
          base.buttonPrev.removeClass("disabled");
          base.buttonNext.removeClass("disabled");
        }
      }
    },

    updateControls : function(){
      var base = this;
      base.updatePagination();
      base.checkNavigation();
      if(base.owlControls){
        if(base.options.items === base.itemsAmount){
          base.owlControls.hide();
        } else {
          base.owlControls.show();
        }
      }
    },

    destroyControls : function(){
      var base = this;
      if(base.owlControls){
        base.owlControls.remove();
      }
    },

    next : function(speed){
      var base = this;
      base.currentSlide += base.options.scrollPerPage === true ? base.options.items : 1;
      if(base.currentSlide > base.maximumSlide + (base.options.scrollPerPage == true ? (base.options.items - 1) : 0)){
        if(base.options.goToFirstNav === true){
          base.currentSlide = 0;
          speed = "goToFirst";
        } else {
          base.currentSlide = base.maximumSlide;
          return false;
        }
      }
      base.goTo(base.currentSlide,speed);
    },

    prev : function(speed){
      var base = this;
      if(base.options.scrollPerPage === true && base.currentSlide > 0 && base.currentSlide < base.options.items){
        base.currentSlide = 0
      } else {
      base.currentSlide -= base.options.scrollPerPage === true ? base.options.items : 1;
      }
      if(base.currentSlide < 0){
        if(base.options.goToFirstNav === true){
          base.currentSlide = base.maximumSlide;
          speed = "goToFirst"
        } else {
          base.currentSlide =0;
          return false;
        }
      }
      base.goTo(base.currentSlide,speed);
    },

    goTo : function(position,pagination){
      var base = this;

      if(typeof base.options.beforeMove === "function") {
        base.options.beforeMove.apply(this,[base.$elem]);
      }
      if(position >= base.maximumSlide){
        position = base.maximumSlide;
      }
      else if( position <= 0 ){
        position = 0;
      }
      base.currentSlide = position;

      var goToPixel = base.positionsInArray[position];

      if(base.support3d === true){
        base.isCss3Finish = false;

        if(pagination === true){
          base.swapTransitionSpeed("paginationSpeed");
          setTimeout(function() {
            base.isCss3Finish = true;
          }, base.options.paginationSpeed);

        } else if(pagination === "goToFirst" ){
          base.swapTransitionSpeed(base.options.goToFirstSpeed);
          setTimeout(function() {
            base.isCss3Finish = true;
          }, base.options.goToFirstSpeed);

        } else {
          base.swapTransitionSpeed("slideSpeed");
          setTimeout(function() {
            base.isCss3Finish = true;
          }, base.options.slideSpeed);
        }
        base.transition3d(goToPixel);
      } else {
        if(pagination === true){
          base.css2slide(goToPixel, base.options.paginationSpeed);
        } else if(pagination === "goToFirst" ){
          base.css2slide(goToPixel, base.options.goToFirstSpeed);
        } else {
          base.css2slide(goToPixel, base.options.slideSpeed);
        }
      }
      if(base.options.pagination === true){
        base.checkPagination();
      }
      if(base.options.navigation === true){
        base.checkNavigation();
      }
      if(base.options.autoPlay !== false){
        base.checkAp();
      }
      base.eachMoveUpdate();
      if(typeof base.options.afterMove === "function") {
        base.options.afterMove.apply(this,[base.$elem]);
      }
    },

    stop: function(){
      var base = this;
      base.apStatus = "stop";
      clearInterval(base.autoPlaySpeed);
    },

    checkAp : function(){
      var base = this;
      if(base.apStatus !== "stop"){
        base.play();
      }
    },

    play : function(){
      var base = this;
      base.apStatus = "play";
      if(base.options.autoPlay === false){
        return false;
      }
      clearInterval(base.autoPlaySpeed);
      base.autoPlaySpeed = setInterval(function(){
        if(base.currentSlide < base.maximumSlide && base.playDirection === "next"){
          base.next(true);
        } else if(base.currentSlide === base.maximumSlide){
          if(base.options.goToFirst === true){
            base.goTo(0,"goToFirst");
          } else{
            base.playDirection = "prev";
            base.prev(true);
          }
        } else if(base.playDirection === "prev" && base.currentSlide > 0){
          base.prev(true);
        } else if(base.playDirection === "prev" && base.currentSlide === 0){
          base.playDirection = "next";
          base.next(true);
        }
      },base.options.autoPlay);
    },

    swapTransitionSpeed : function(action){
      var base = this;
      if(action === "slideSpeed"){
        base.owlWrapper.css(base.addTransition(base.options.slideSpeed));
      } else if(action === "paginationSpeed" ){
        base.owlWrapper.css(base.addTransition(base.options.paginationSpeed));
      } else if(typeof action !== "string"){
        base.owlWrapper.css(base.addTransition(action));
      }
    },

    addTransition : function(speed){
      var base = this;
      return {
        "-webkit-transition": "all "+ speed +"ms ease",
        "-moz-transition": "all "+ speed +"ms ease",
        "-o-transition": "all "+ speed +"ms ease",
        "transition": "all "+ speed +"ms ease"
      };
    },

    removeTransition : function(){
      return {
        "-webkit-transition": "",
        "-moz-transition": "",
        "-o-transition": "",
        "transition": ""
      };
    },

    doTranslate : function(pixels){
      return {
        "-webkit-transform": "translate3d("+pixels+"px, 0px, 0px)",
        "-moz-transform": "translate3d("+pixels+"px, 0px, 0px)",
        "-o-transform": "translate3d("+pixels+"px, 0px, 0px)",
        "-ms-transform": "translate3d("+pixels+"px, 0px, 0px)",
        "transform": "translate3d("+pixels+"px, 0px,0px)"
      };
    },

    transition3d : function(value){
      var base = this;
      base.owlWrapper.css(base.doTranslate(value));
    },

    css2move : function(value){
      var base = this;
      base.owlWrapper.css({"left" : value})
    },

    css2slide : function(value,speed){
      var base = this;

      base.isCssFinish = false;
      base.owlWrapper.stop(true,true).animate({
        "left" : value
      }, {
        duration : speed || base.options.slideSpeed ,
        complete : function(){
          base.isCssFinish = true;
        }
      });
    },

    support3d : function(){
        var base = this;

        var sTranslate3D = "translate3d(0px, 0px, 0px)";
         var eTemp = document.createElement("div");
        eTemp.style.cssText = "  -moz-transform:"    + sTranslate3D +
                    "; -ms-transform:"     + sTranslate3D +
                    "; -o-transform:"      + sTranslate3D +
                    "; -webkit-transform:" + sTranslate3D +
                    "; transform:"         + sTranslate3D;
        var rxTranslate = /translate3d\(0px, 0px, 0px\)/g;
        var asSupport = eTemp.style.cssText.match(rxTranslate);
        var bHasSupport = (asSupport !== null && asSupport.length === 1);
        base.support3d = bHasSupport
        return bHasSupport;
    },

    checkTouch : function(){
      var base = this;
      base.isTouch = 'ontouchstart' in window || navigator.msMaxTouchPoints;
    },

    moveEvents : function(){
      var base = this;
      if(base.options.mouseDrag !== false || base.options.touchDrag !== false){
        base.gestures();
        base.disabledEvents();
      }
    },

    eventTypes : function(){
    var base = this;
    var types = ["s","e","x"];

    base.ev_types = {};

    if(base.options.mouseDrag === true && base.options.touchDrag === true){
      types = [
        "touchstart.owl mousedown.owl",
        "touchmove.owl mousemove.owl",
        "touchend.owl touchcancel.owl mouseup.owl"
      ];
    } else if(base.options.mouseDrag === false && base.options.touchDrag === true){
      types = [
        "touchstart.owl",
        "touchmove.owl",
        "touchend.owl touchcancel.owl"
      ];
    } else if(base.options.mouseDrag === true && base.options.touchDrag === false){
      types = [
        "mousedown.owl",
        "mousemove.owl",
        "mouseup.owl"
      ];
    }

    base.ev_types["start"] = types[0];
    base.ev_types["move"] = types[1];
    base.ev_types["end"] = types[2];
    },

    disabledEvents :  function(){
      var base = this;
      base.$elem.on("dragstart.owl","img", function(event) { event.preventDefault();});
      base.$elem.bind("mousedown.disableTextSelect", function() {return false;});
    },

    gestures : function(){
      var base = this;

      var locals = {
        offsetX : 0,
        offsetY : 0,
        baseElWidth : 0,
        relativePos : 0,
        position: null,
        minSwipe : null,
        maxSwipe: null,
        sliding : null,
        dargging: null,
        targetElement : null
      }

      base.isCssFinish = true;

      function getTouches(event){
        if(event.touches){
          return {
            x : event.touches[0].pageX,
            y : event.touches[0].pageY
          }
        } else {
          if(event.pageX !== undefined){
            return {
              x : event.pageX,
              y : event.pageY
            }
          } else {
            return {
              x : event.clientX,
              y : event.clientY
            }
          }
        }
      }

      function swapEvents(type){
        if(type === "on"){
          $(document).on(base.ev_types["move"], dragMove);
          $(document).on(base.ev_types["end"], dragEnd);
        } else if(type === "off"){
          $(document).off(base.ev_types["move"]);
          $(document).off(base.ev_types["end"]);
        }
      }
      function dragStart(event) {
        var event = event.originalEvent || event || window.event;

        if(base.isCssFinish === false){
          return false;
        }
        if(base.isCss3Finish === false){
          return false;
        }

        if(base.options.autoPlay !== false){
          clearInterval(base.autoPlaySpeed);
        }

        if(base.isTouch !== true && !base.owlWrapper.hasClass("grabbing")){
          base.owlWrapper.addClass("grabbing")
        }

        base.newPosX = 0;
        base.newRelativeX = 0;

        $(this).css(base.removeTransition());

        var position = $(this).position();
        locals.relativePos = position.left;
        
        locals.offsetX = getTouches(event).x - position.left;
        locals.offsetY = getTouches(event).y - position.top;

        swapEvents("on");

        locals.sliding = false;
        locals.targetElement = event.target || event.srcElement;
      }

      function dragMove(event){
        var event = event.originalEvent || event || window.event;

        base.newPosX = getTouches(event).x- locals.offsetX;
        base.newPosY = getTouches(event).y - locals.offsetY;
        base.newRelativeX = base.newPosX - locals.relativePos;  

        if (typeof base.options.startDragging === "function" && locals.dragging !== true && base.newPosX !== 0) {
          locals.dragging = true;
          base.options.startDragging.apply(this);
        }     

        if(base.newRelativeX > 8 || base.newRelativeX < -8 && base.isTouch === true){
          event.preventDefault ? event.preventDefault() : event.returnValue = false;
          locals.sliding = true;
        }

        if((base.newPosY > 10 || base.newPosY < -10) && locals.sliding === false){
          $(document).off("touchmove.owl");
        }

        var minSwipe = function(){
          return  base.newRelativeX / 5;
        }
        var maxSwipe = function(){
          return  base.maximumPixels + base.newRelativeX / 5;
        }

        base.newPosX = Math.max(Math.min( base.newPosX, minSwipe() ), maxSwipe() );
        if(base.support3d === true){
          base.transition3d(base.newPosX);
        } else {
          base.css2move(base.newPosX);
        }
      }

      function dragEnd(event){
        var event = event.originalEvent || event || window.event;
        event.target = event.target || event.srcElement;

        locals.dragging = false;

        if(base.isTouch !== true){
          base.owlWrapper.removeClass("grabbing");
        }

        if(base.newPosX !== 0){
          var newPosition = base.getNewPosition();
          base.goTo(newPosition);
          if(locals.targetElement === event.target && base.isTouch !== true){
            $(event.target).on("click.disable", function(ev){
              ev.stopImmediatePropagation()
              ev.stopPropagation();
              ev.preventDefault();
              $(event.target).off("click.disable");
            });
          var handlers = $._data(event.target, "events")["click"];
          var owlStopEvent = handlers.pop();
          handlers.splice(0, 0, owlStopEvent);
          }
        }
        swapEvents("off");
      }
      base.$elem.on(base.ev_types["start"], ".owl-wrapper", dragStart); 
    },

    clearEvents : function(){
      var base = this;
      base.$elem.off(".owl");
      $(document).off(".owl");
    },

    getNewPosition : function(){
      var base = this,
        newPosition;

      var newPosition = base.improveClosest();

      if(newPosition>base.maximumSlide){
        base.currentSlide = base.maximumSlide;
        newPosition  = base.maximumSlide;
      } else if( base.newPosX >=0 ){
        newPosition = 0;
        base.currentSlide = 0;
      }
      return newPosition;
    },

    improveClosest : function(){
      var base = this;
      var array = base.positionsInArray;
      var goal = base.newPosX;
      var closest = null;
      $.each(array, function(i,v){
        if( goal - (base.itemWidth/20) > array[i+1] && goal - (base.itemWidth/20)< v && base.moveDirection() === "left") {
          closest = v;
          base.currentSlide = i;
        } 
        else if (goal + (base.itemWidth/20) < v && goal + (base.itemWidth/20) > array[i+1] && base.moveDirection() === "right"){
          closest = array[i+1];
          base.currentSlide = i+1;
        }
      });
      return base.currentSlide;
    },

    moveDirection : function(){
      var base = this,
        direction;
      if(base.newRelativeX < 0 ){
        direction = "right"
        base.playDirection = "next"
      } else {
        direction = "left"
        base.playDirection = "prev"
      }
      return direction
    },

    customEvents : function(){
      var base = this;
      base.$elem.on("owl.next",function(){
        base.next();
      });
      base.$elem.on("owl.prev",function(){
        base.prev();
      });
      base.$elem.on("owl.play",function(event,speed){
        base.options.autoPlay = speed;
        base.play();
        base.hoverStatus = "play";
      });
      base.$elem.on("owl.stop",function(){
        base.stop();
        base.hoverStatus = "stop";
      });
    },
    
    stopOnHover : function(){
      var base = this;
      if(base.options.stopOnHover === true && base.isTouch !== true && base.options.autoPlay !== false){
        base.$elem.on("mouseover", function(){
          base.stop();
        });
        base.$elem.on("mouseout", function(){
          if(base.hoverStatus !== "stop"){
            base.play();
          }
        });
      }
    },

    lazyLoad : function(){
      var base = this;

      if(base.options.lazyLoad === false){
        return false;
      }

      for(var i=0; i<base.itemsAmount; i++){
        var item = $(base.owlItems[i]),
          itemNumber = item.data("owl-item"),
          lazyImg = item.find(".lazyOwl"),
          follow;

        if(item.data("owl-loaded") === undefined){
          lazyImg.hide();
          item.addClass("loading").data("owl-loaded","checked");
        } else if(item.data("owl-loaded") === "loaded"){
          continue;
        }

        if(base.options.lazyFollow === true){
          follow = itemNumber >= base.currentSlide;
        }else {
          follow = true;
        }

        if(follow && itemNumber < base.currentSlide + base.options.items){
          item.data("owl-loaded", "loaded");

          var link = lazyImg.data("src");
          if(link){
            lazyImg[0].src = link;
            lazyImg.removeAttr("data-src");
          }
          lazyImg.fadeIn(200);
          item.removeClass("loading");
        }
      }
    },

    autoHeight : function(){
      var base = this;
      var $currentimg = $(base.owlItems[base.currentSlide]).find('img');

      if($currentimg.get(0) !== undefined ){
        var iterations = 0;
        checkImage();
      } else {
        addHeight();
      }
      function checkImage(){
        iterations += 1;
        if ($currentimg.get(0).complete) {
          addHeight();
        } else if(iterations <= 50){ //if image loads in less than 10 seconds 
          setTimeout(checkImage,200);
        } else {
          base.wrapperOuter.css("height", ""); //Else remove height attribute
        }
      }
      function addHeight(){
        var $currentSlide = $(base.owlItems[base.currentSlide]).height();
        base.wrapperOuter.css("height",$currentSlide+"px");
        if(!base.wrapperOuter.hasClass("autoHeight")){
          setTimeout(function(){
            base.wrapperOuter.addClass("autoHeight");
          },0);
        }
      }
    },

    addClassActive : function(){
      var base = this;
      $(base.owlItems).removeClass('active');
      for(var i=base.currentSlide; i<base.currentSlide + base.options.items; i++){
        $(base.owlItems[i]).addClass('active');
      }
    }
  };


  $.fn.owlCarousel = function( options ){
    return this.each(function() {
      var carousel = Object.create( Carousel );
      carousel.init( options, this );
      $.data( this, "owlCarousel", carousel );
    });
  };

  $.fn.owlCarousel.options = {

    items : 5,
    itemsDesktop : [1199,4],
    itemsDesktopSmall : [979,3],
    itemsTablet: [768,2],
    itemsMobile : [479,1],
    singleItem:false,

    slideSpeed : 200,
    paginationSpeed : 800,

    autoPlay : false,
    stopOnHover : false,
    goToFirst : true,
    goToFirstSpeed : 1000,

    navigation : false,
    navigationText : ["prev","next"],
    goToFirstNav : true,
    scrollPerPage : false,

    pagination : true,
    paginationNumbers: false,

    responsive: true,
    responsiveRefreshRate : 200,

    baseClass : "owl-carousel",
    theme : "owl-theme",

    lazyLoad : false,
    lazyFollow : true,

    autoHeight : false,

    jsonPath : false,
    jsonSuccess : false,

    mouseDrag : false,
    touchDrag : true,

    beforeInit : false,
    afterInit : false,
    beforeMove: false,
    afterMove: false,
    afterAction : false,
    startDragging : false,

    addClassActive : false
  };
})( jQuery, window, document );

// fin owl casousel 

;(function($){
  function init(form,o){
    var name=$('.name>input',form)
      ,email=$('.email>input',form)
      ,submit=$('a[data-type="submit"]',form)
      ,msg_success=$('.success',form).hide()
      ,bl,vl
      
    o=$.extend({
      ownerEmail:'#'
      ,mailHandlerURL:'bat/MailHandler-sub.php'
    },o)
    
    submit.click(function(){
      vl=true
      name.add(email).trigger('keyup')
      if(!$('.invalid',form).length)
        sendRQ()
      return false
    })
        
    form[form.on?'on':'bind']('keyup',function(e){
      if(e.keyCode===13){
        name.add(email).data({wtch:true}).trigger('keyup')
        if(!$('.invalid',form).length){         
          sendRQ()
          return false
        }else{
          $('.invalid',form).focus()
        }
      }
    })
    
    name.add(email)
      .data({wtch:true})
      .each(function(){
        var th=$(this)
          ,val=th.val()
        th
          .data({defVal:val})
          .focus(function(){
            if(th.val()==val)
              th.val('')
              ,th.parents('label').removeClass('invalid')
              ,th.data({wtch:false})
          })
          .blur(function(){
            if(th.val()=='')
              th.val(val)             
              //,th.parents('label').removeClass('invalid')
            //else
              th.data({wtch:true}).trigger('keyup')
          })
      })
      [name.on?'on':'bind']('keyup',function(e){
        var th=$(this)
        if(th.data('wtch'))
          th.parents('label')[validate(th)?'removeClass':'addClass']('invalid')
      })
  
    function validate(el){
      var rx={
          "text":/^[a-zA-Z'][a-zA-Z-' ]+[a-zA-Z']?$/
          ,"email":/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
        }
        ,val=el.val()
      return rx[el.attr('type')].test(val)&&val!==el.data('defVal')
    }
    
    function sendRQ(){
      if(bl)
        return false
      bl=true
      $.ajax({
        type:"POST"
        ,url:o.mailHandlerURL
        ,data:{
          name:name.length?name.val():email.val().split('@')[0]
          ,email:email.val()
          ,owner_email:o.ownerEmail         
          ,sitename:o.sitename||o.ownerEmail.split('@')[1]
        }
        ,success:function(e){
          bl=false
          msg_success
            .slideDown(400,function(){
              submit.focus()
              form.trigger('reset')             
              name.add(email).data({wtch:true})
              setTimeout(function(){
                msg_success.fadeOut(400)
              },1000)
            })          
        }
      })
    }   
  } 

  $.fn.sForm=function(o){
    return this.each(function(){
      init($(this),o)
    })
  }
  
})(jQuery)
$(window).load(function(){
  $('#newsletter').sForm({      
    ownerEmail:'#'
    ,sitename:'sitename.link'
  })
})

// fin sforms 

;(function($){$.fn.camera = function(opts, callback) {
  
  var defaults = {
    alignment     : 'topCenter', //topLeft, topCenter, topRight, centerLeft, center, centerRight, bottomLeft, bottomCenter, bottomRight
    
    autoAdvance     : true, //true, false
    
    mobileAutoAdvance : true, //true, false. Auto-advancing for mobile devices
    
    barDirection    : 'leftToRight',  //'leftToRight', 'rightToLeft', 'topToBottom', 'bottomToTop'
    
    barPosition     : 'bottom', //'bottom', 'left', 'top', 'right'
    
    cols        : 6,
    
    easing        : 'easeInOutExpo',  //for the complete list http://jqueryui.com/demos/effect/easing.html
    
    mobileEasing    : '', //leave empty if you want to display the same easing on mobile devices and on desktop etc.
    
    fx          : 'random', //'random','simpleFade', 'curtainTopLeft', 'curtainTopRight', 'curtainBottomLeft', 'curtainBottomRight', 'curtainSliceLeft', 'curtainSliceRight', 'blindCurtainTopLeft', 'blindCurtainTopRight', 'blindCurtainBottomLeft', 'blindCurtainBottomRight', 'blindCurtainSliceBottom', 'blindCurtainSliceTop', 'stampede', 'mosaic', 'mosaicReverse', 'mosaicRandom', 'mosaicSpiral', 'mosaicSpiralReverse', 'topLeftBottomRight', 'bottomRightTopLeft', 'bottomLeftTopRight', 'bottomLeftTopRight'
                    //you can also use more than one effect, just separate them with commas: 'simpleFade, scrollRight, scrollBottom'

    mobileFx      : '', //leave empty if you want to display the same effect on mobile devices and on desktop etc.

    gridDifference    : 250,  //to make the grid blocks slower than the slices, this value must be smaller than transPeriod
    
    height        : 'auto', //here you can type pixels (for instance '300px'), a percentage (relative to the width of the slideshow, for instance '50%') or 'auto'
    
    imagePath     : 'images/',  //he path to the image folder (it serves for the blank.gif, when you want to display videos)
    
    hover       : true, //true, false. Puase on state hover. Not available for mobile devices
        
    loader        : 'pie',  //pie, bar, none (even if you choose "pie", old browsers like IE8- can't display it... they will display always a loading bar)
    
    loaderColor     : '#eeeeee', 
    
    loaderBgColor   : '#222222', 
    
    loaderOpacity   : .8, //0, .1, .2, .3, .4, .5, .6, .7, .8, .9, 1
    
    loaderPadding   : 2,  //how many empty pixels you want to display between the loader and its background
    
    loaderStroke    : 7,  //the thickness both of the pie loader and of the bar loader. Remember: for the pie, the loader thickness must be less than a half of the pie diameter
        
    minHeight     : '000',  //you can also leave it blank
    
    navigation      : true, //true or false, to display or not the navigation buttons
    
    navigationHover   : true, //if true the navigation button (prev, next and play/stop buttons) will be visible on hover state only, if false they will be visible always
    
    mobileNavHover    : true, //same as above, but only for mobile devices
    
    opacityOnGrid   : false,  //true, false. Decide to apply a fade effect to blocks and slices: if your slideshow is fullscreen or simply big, I recommend to set it false to have a smoother effect 
    
    overlayer     : true, //a layer on the images to prevent the users grab them simply by clicking the right button of their mouse (.camera_overlayer)
    
    pagination      : true,
    
    playPause     : true, //true or false, to display or not the play/pause buttons
    
    pauseOnClick    : true, //true, false. It stops the slideshow when you click the sliders.
    
    pieDiameter     : 38,
    
    piePosition     : 'center', //'rightTop', 'leftTop', 'leftBottom', 'rightBottom'
    
    portrait      : false, //true, false. Select true if you don't want that your images are cropped
    
    rows        : 4,
    
    slicedCols      : 12, //if 0 the same value of cols
    
    slicedRows      : 8,  //if 0 the same value of rows
    
    slideOn       : 'random', //next, prev, random: decide if the transition effect will be applied to the current (prev) or the next slide
    
    thumbnails      : false,
    
    time        : 7000, //milliseconds between the end of the sliding effect and the start of the nex one
    
    transPeriod     : 1000, //lenght of the sliding effect in milliseconds
    
////////callbacks

    onEndTransition   : function() {  },  //this callback is invoked when the transition effect ends

    onLoaded      : function() {  },  //this callback is invoked when the image on a slide has completely loaded
    
    onStartLoading    : function() {  },  //this callback is invoked when the image on a slide start loading
    
    onStartTransition : function() {  } //this callback is invoked when the transition effect starts

    };
  
  
  function isMobile() {
    if( navigator.userAgent.match(/Android/i) ||
      navigator.userAgent.match(/webOS/i) ||
      navigator.userAgent.match(/iPad/i) ||
      navigator.userAgent.match(/iPhone/i) ||
      navigator.userAgent.match(/iPod/i)
      ){
        return true;
    }
  }

  var opts = $.extend({}, defaults, opts);
  
  var wrap = $(this).addClass('camera_wrap');
  
  wrap.wrapInner(
        '<div class="camera_src" />'
    ).wrapInner(
      '<div class="camera_fakehover" />'
    );
    
  var fakeHover = $('.camera_fakehover',wrap);
  
  fakeHover.append(
    '<div class="camera_target"></div>'
    );
  if(opts.overlayer == true){
    fakeHover.append(
      '<div class="camera_overlayer"></div>'
      )
  }
    fakeHover.append(
        '<div class="camera_target_content"></div>'
    );
    
  var loader;
  
  if(opts.loader=='pie' && $.browser.msie && $.browser.version < 9){
    loader = 'bar';
  } else {
    loader = opts.loader;
  }
  
  if(loader == 'pie'){
    fakeHover.append(
      '<div class="camera_pie"></div>'
      )
  } else if (loader == 'bar') {
    fakeHover.append(
      '<div class="camera_bar"></div>'
      )
  } else {
    fakeHover.append(
      '<div class="camera_bar" style="display:none"></div>'
      )
  }
  
  if(opts.playPause==true){
    fakeHover.append(
        '<div class="camera_commands"></div>'
    )
  }
    
  if(opts.navigation==true){
    fakeHover.append(
      '<div class="camera_prev"><span></span></div>'
      ).append(
      '<div class="camera_next"><span></span></div>'
      );
  }
    
  if(opts.thumbnails==true){
    wrap.append(
      '<div class="camera_thumbs_cont" />'
      );
  }
  
  if(opts.thumbnails==true && opts.pagination!=true){
    $('.camera_thumbs_cont',wrap).wrap(
      '<div />'
      ).wrap(
        '<div class="camera_thumbs" />'
      ).wrap(
        '<div />'
      ).wrap(
        '<div class="camera_command_wrap" />'
      );
  }
    
  if(opts.pagination==true){
    wrap.append(
      '<div class="camera_pag"></div>'
      );
  }
    
  wrap.append(
    '<div class="camera_loader"></div>'
    );
    
  $('.camera_caption',wrap).each(function(){
    $(this).wrapInner('<div />');
  });
    
                
  var pieID = 'pie_'+wrap.index(),
    elem = $('.camera_src',wrap),
    target = $('.camera_target',wrap),
    content = $('.camera_target_content',wrap),
    pieContainer = $('.camera_pie',wrap),
    barContainer = $('.camera_bar',wrap),
    prevNav = $('.camera_prev',wrap),
    nextNav = $('.camera_next',wrap),
    commands = $('.camera_commands',wrap),
    pagination = $('.camera_pag',wrap),
    thumbs = $('.camera_thumbs_cont',wrap); 

  
  var w,
    h;


  var allImg = new Array();
  $('> div', elem).each( function() { 
    allImg.push($(this).attr('data-src'));
  });
  
  var allLinks = new Array();
  $('> div', elem).each( function() {
    if($(this).attr('data-link')){
      allLinks.push($(this).attr('data-link'));
    } else {
      allLinks.push('');
    }
  });
  
  var allTargets = new Array();
  $('> div', elem).each( function() {
    if($(this).attr('data-target')){
      allTargets.push($(this).attr('data-target'));
    } else {
      allTargets.push('');
    }
  });
  
  var allPor = new Array();
  $('> div', elem).each( function() {
    if($(this).attr('data-portrait')){
      allPor.push($(this).attr('data-portrait'));
    } else {
      allPor.push('');
    }
  });
  
  var allAlign= new Array();
  $('> div', elem).each( function() { 
    if($(this).attr('data-alignment')){
      allAlign.push($(this).attr('data-alignment'));
    } else {
      allAlign.push('');
    }
  });
  
    
  var allThumbs = new Array();
  $('> div', elem).each( function() { 
    if($(this).attr('data-thumb')){
      allThumbs.push($(this).attr('data-thumb'));
    } else {
      allThumbs.push('');
    }
  });
  
  var amountSlide = allImg.length;

  $(content).append('<div class="cameraContents" />');
  var loopMove;
  for (loopMove=0;loopMove<amountSlide;loopMove++)
  {
    $('.cameraContents',content).append('<div class="cameraContent" />');
    if(allLinks[loopMove]!=''){
      //only for Wordpress plugin
      var dataBox = $('> div ',elem).eq(loopMove).attr('data-box');
      if(typeof dataBox !== 'undefined' && dataBox !== false && dataBox != '') {
        dataBox = 'data-box="'+$('> div ',elem).eq(loopMove).attr('data-box')+'"';
      } else {
        dataBox = '';
      }
      //
      $('.camera_target_content .cameraContent:eq('+loopMove+')',wrap).append('<a class="camera_link" href="'+allLinks[loopMove]+'" '+dataBox+' target="'+allTargets[loopMove]+'"></a>');
    }

  }
  $('.camera_caption',wrap).each(function(){
    var ind = $(this).parent().index(),
      cont = wrap.find('.cameraContent').eq(ind);
    $(this).appendTo(cont);
  });
  
  target.append('<div class="cameraCont" />');
  var cameraCont = $('.cameraCont',wrap);
  

  
  var loop;
  for (loop=0;loop<amountSlide;loop++)
  {
    cameraCont.append('<div class="cameraSlide cameraSlide_'+loop+'" />');
    var div = $('> div:eq('+loop+')',elem);
    target.find('.cameraSlide_'+loop).clone(div);
  }
  
  
  function thumbnailVisible() {
    var wTh = $(thumbs).width();
    $('li', thumbs).removeClass('camera_visThumb');
    $('li', thumbs).each(function(){
      var pos = $(this).position(),
        ulW = $('ul', thumbs).outerWidth(),
        offUl = $('ul', thumbs).offset().left,
        offDiv = $('> div',thumbs).offset().left,
        ulLeft = offDiv-offUl;
        if(ulLeft>0){
          $('.camera_prevThumbs',camera_thumbs_wrap).removeClass('hideNav');
        } else {
          $('.camera_prevThumbs',camera_thumbs_wrap).addClass('hideNav');
        }
        if((ulW-ulLeft)>wTh){
          $('.camera_nextThumbs',camera_thumbs_wrap).removeClass('hideNav');
        } else {
          $('.camera_nextThumbs',camera_thumbs_wrap).addClass('hideNav');
        }
        var left = pos.left,
          right = pos.left+($(this).width());
        if(right-ulLeft<=wTh && left-ulLeft>=0){
          $(this).addClass('camera_visThumb');
        }
    });
  }
  
  $(window).bind('load resize pageshow',function(){
    thumbnailPos();
    thumbnailVisible();
  });


  cameraCont.append('<div class="cameraSlide cameraSlide_'+loop+'" />');
  
  
  var started;
  
  wrap.show();
  var w = target.width();
  var h = target.height();
  
  var setPause;
    
  $(window).bind('resize pageshow',function(){
    if(started == true) {
      resizeImage();
    }
    $('ul', thumbs).animate({'margin-top':0},0,thumbnailPos);
    if(!elem.hasClass('paused')){
      elem.addClass('paused');
      if($('.camera_stop',camera_thumbs_wrap).length){
        $('.camera_stop',camera_thumbs_wrap).hide()
        $('.camera_play',camera_thumbs_wrap).show();
        if(loader!='none'){
          $('#'+pieID).hide();
        }
      } else {
        if(loader!='none'){
          $('#'+pieID).hide();
        }
      }
      clearTimeout(setPause);
      setPause = setTimeout(function(){
        elem.removeClass('paused');
        if($('.camera_play',camera_thumbs_wrap).length){
          $('.camera_play',camera_thumbs_wrap).hide();
          $('.camera_stop',camera_thumbs_wrap).show();
          if(loader!='none'){
            $('#'+pieID).fadeIn();
          }
        } else {
          if(loader!='none'){
            $('#'+pieID).fadeIn();
          }
        }
      },1500);
    }
  });
  
  function resizeImage(){ 
    var res;
    function resizeImageWork(){
      w = wrap.width();
      if(opts.height.indexOf('%')!=-1) {
        var startH = Math.round(w / (100/parseFloat(opts.height)));
        if(opts.minHeight != '' && startH < parseFloat(opts.minHeight)){
          h = parseFloat(opts.minHeight);
        } else {
          h = startH;
        }
        wrap.css({height:h});
      } else if (opts.height=='auto') {
        h = wrap.height();
      } else {
        h = parseFloat(opts.height);
        wrap.css({height:h});
      }
      $('.camerarelative',target).css({'width':w,'height':h});
      $('.imgLoaded',target).each(function(){
        var t = $(this),
          wT = t.attr('width'),
          hT = t.attr('height'),
          imgLoadIn = t.index(),
          mTop,
          mLeft,
          alignment = t.attr('data-alignment'),
          portrait =  t.attr('data-portrait');
          
          if(typeof alignment === 'undefined' || alignment === false || alignment === ''){
            alignment = opts.alignment;
          }
          
          if(typeof portrait === 'undefined' || portrait === false || portrait === ''){
            portrait = opts.portrait;
          }
                    
          if(portrait==false||portrait=='false'){
            if((wT/hT)<(w/h)) {
              var r = w / wT;
              var d = (Math.abs(h - (hT*r)))*0.5;
              switch(alignment){
                case 'topLeft':
                  mTop = 0;
                  break;
                case 'topCenter':
                  mTop = 0;
                  break;
                case 'topRight':
                  mTop = 0;
                  break;
                case 'centerLeft':
                  mTop = '-'+d+'px';
                  break;
                case 'center':
                  mTop = '-'+d+'px';
                  break;
                case 'centerRight':
                  mTop = '-'+d+'px';
                  break;
                case 'bottomLeft':
                  mTop = '-'+d*2+'px';
                  break;
                case 'bottomCenter':
                  mTop = '-'+d*2+'px';
                  break;
                case 'bottomRight':
                  mTop = '-'+d*2+'px';
                  break;
              }
              t.css({
                'height' : hT*r,
                'margin-left' : 0,
                'margin-top' : mTop,
                'position' : 'absolute',
                'visibility' : 'visible',
                'width' : w
              });
            }
            else {
              var r = h / hT;
              var d = (Math.abs(w - (wT*r)))*0.5;
              switch(alignment){
                case 'topLeft':
                  mLeft = 0;
                  break;
                case 'topCenter':
                  mLeft = '-'+d+'px';
                  break;
                case 'topRight':
                  mLeft = '-'+d*2+'px';
                  break;
                case 'centerLeft':
                  mLeft = 0;
                  break;
                case 'center':
                  mLeft = '-'+d+'px';
                  break;
                case 'centerRight':
                  mLeft = '-'+d*2+'px';
                  break;
                case 'bottomLeft':
                  mLeft = 0;
                  break;
                case 'bottomCenter':
                  mLeft = '-'+d+'px';
                  break;
                case 'bottomRight':
                  mLeft = '-'+d*2+'px';
                  break;
              }
              t.css({
                'height' : h,
                'margin-left' : mLeft,
                'margin-top' : 0,
                'position' : 'absolute',
                'visibility' : 'visible',
                'width' : wT*r
              });
            }
          } else {
            if((wT/hT)<(w/h)) {
              var r = h / hT;
              var d = (Math.abs(w - (wT*r)))*0.5;
              switch(alignment){
                case 'topLeft':
                  mLeft = 0;
                  break;
                case 'topCenter':
                  mLeft = d+'px';
                  break;
                case 'topRight':
                  mLeft = d*2+'px';
                  break;
                case 'centerLeft':
                  mLeft = 0;
                  break;
                case 'center':
                  mLeft = d+'px';
                  break;
                case 'centerRight':
                  mLeft = d*2+'px';
                  break;
                case 'bottomLeft':
                  mLeft = 0;
                  break;
                case 'bottomCenter':
                  mLeft = d+'px';
                  break;
                case 'bottomRight':
                  mLeft = d*2+'px';
                  break;
              }
              t.css({
                'height' : h,
                'margin-left' : mLeft,
                'margin-top' : 0,
                'position' : 'absolute',
                'visibility' : 'visible',
                'width' : wT*r
              });
            }
            else {
              var r = w / wT;
              var d = (Math.abs(h - (hT*r)))*0.5;
              switch(alignment){
                case 'topLeft':
                  mTop = 0;
                  break;
                case 'topCenter':
                  mTop = 0;
                  break;
                case 'topRight':
                  mTop = 0;
                  break;
                case 'centerLeft':
                  mTop = d+'px';
                  break;
                case 'center':
                  mTop = d+'px';
                  break;
                case 'centerRight':
                  mTop = d+'px';
                  break;
                case 'bottomLeft':
                  mTop = d*2+'px';
                  break;
                case 'bottomCenter':
                  mTop = d*2+'px';
                  break;
                case 'bottomRight':
                  mTop = d*2+'px';
                  break;
              }
              t.css({
                'height' : hT*r,
                'margin-left' : 0,
                'margin-top' : mTop,
                'position' : 'absolute',
                'visibility' : 'visible',
                'width' : w
              });
            }
          }
      });
    }
    if (started == true) {
      clearTimeout(res);
      res = setTimeout(resizeImageWork,200);
    } else {
      resizeImageWork();
    }
    
    started = true;
  }
  
  
  var u,
    setT;

  var clickEv,
    autoAdv,
    navHover,
    commands,
    pagination;

  var videoHover,
    videoPresent;
    
  if(isMobile() && opts.mobileAutoAdvance!=''){
    autoAdv = opts.mobileAutoAdvance;
  } else {
    autoAdv = opts.autoAdvance;
  }
  
  if(autoAdv==false){
    elem.addClass('paused');
  }

  if(isMobile() && opts.mobileNavHover!=''){
    navHover = opts.mobileNavHover;
  } else {
    navHover = opts.navigationHover;
  }

  if(elem.length!=0){
      
    var selector = $('.cameraSlide',target);
    selector.wrapInner('<div class="camerarelative" />');
    
    var navSlide;
      
    var barDirection = opts.barDirection;
  
    var camera_thumbs_wrap = wrap;


    $('iframe',fakeHover).each(function(){
      var t = $(this);
      var src = t.attr('src');
      t.attr('data-src',src);
      var divInd = t.parent().index('.camera_src > div');
      $('.camera_target_content .cameraContent:eq('+divInd+')',wrap).append(t);
    });
    function imgFake() {
        $('iframe',fakeHover).each(function(){
          $('.camera_caption',fakeHover).show();
          var t = $(this);
          var cloneSrc = t.attr('data-src');
          t.attr('src',cloneSrc);
          var imgFakeUrl = opts.imagePath+'blank.gif';
          var imgFake = new Image();
          imgFake.src = imgFakeUrl;
          if(opts.height.indexOf('%')!=-1) {
            var startH = Math.round(w / (100/parseFloat(opts.height)));
            if(opts.minHeight != '' && startH < parseFloat(opts.minHeight)){
              h = parseFloat(opts.minHeight);
            } else {
              h = startH;
            }
          } else if (opts.height=='auto') {
            h = wrap.height();
          } else {
            h = parseFloat(opts.height);
          }
          t.after($(imgFake).attr({'class':'imgFake','width':w,'height':h}));
          var clone = t.clone();
          t.remove();
          $(imgFake).bind('click',function(){
            if($(this).css('position')=='absolute') {
              $(this).remove();
              if(cloneSrc.indexOf('vimeo') != -1 || cloneSrc.indexOf('youtube') != -1) {
                if(cloneSrc.indexOf('?') != -1){
                  autoplay = '&autoplay=1';
                } else {
                  autoplay = '?autoplay=1';
                }
              } else if(cloneSrc.indexOf('dailymotion') != -1) {
                if(cloneSrc.indexOf('?') != -1){
                  autoplay = '&autoPlay=1';
                } else {
                  autoplay = '?autoPlay=1';
                }
              }
              clone.attr('src',cloneSrc+autoplay);
              videoPresent = true;
            } else {
              $(this).css({position:'absolute',top:0,left:0,zIndex:10}).after(clone);
              clone.css({position:'absolute',top:0,left:0,zIndex:9});
            }
          });
        });
    }
    
    imgFake();
    
    
    if(opts.hover==true){
      if(!isMobile()){
        fakeHover.hover(function(){
          elem.addClass('hovered');
        },function(){
          elem.removeClass('hovered');
        });
      }
    }

    
    
  
    $('.camera_stop',camera_thumbs_wrap).live('click',function(){
      autoAdv = false;
      elem.addClass('paused');
      if($('.camera_stop',camera_thumbs_wrap).length){
        $('.camera_stop',camera_thumbs_wrap).hide()
        $('.camera_play',camera_thumbs_wrap).show();
        if(loader!='none'){
          $('#'+pieID).hide();
        }
      } else {
        if(loader!='none'){
          $('#'+pieID).hide();
        }
      }
    });
  
    $('.camera_play',camera_thumbs_wrap).live('click',function(){
      autoAdv = true;
      elem.removeClass('paused');
      if($('.camera_play',camera_thumbs_wrap).length){
        $('.camera_play',camera_thumbs_wrap).hide();
        $('.camera_stop',camera_thumbs_wrap).show();
        if(loader!='none'){
          $('#'+pieID).show();
        }
      } else {
        if(loader!='none'){
          $('#'+pieID).show();
        }
      }
    });
  
    if(opts.pauseOnClick==true){
      $('.camera_target_content',fakeHover).mouseup(function(){
        autoAdv = false;
        elem.addClass('paused');
        $('.camera_stop',camera_thumbs_wrap).hide()
        $('.camera_play',camera_thumbs_wrap).show();
        $('#'+pieID).hide();
      });
    }
    $('.cameraContent, .imgFake',fakeHover).hover(function(){
      videoHover = true;
    },function(){
      videoHover = false;
    });
    
    $('.cameraContent, .imgFake',fakeHover).bind('click',function(){
      if(videoPresent == true && videoHover == true) {
        autoAdv = false;
        $('.camera_caption',fakeHover).hide();
        elem.addClass('paused');
        $('.camera_stop',camera_thumbs_wrap).hide()
        $('.camera_play',camera_thumbs_wrap).show();
        $('#'+pieID).hide();
      }
    });
    
    
  }
  
  
    function shuffle(arr) {
      for(
        var j, x, i = arr.length; i;
        j = parseInt(Math.random() * i),
        x = arr[--i], arr[i] = arr[j], arr[j] = x
      );
      return arr;
    }
  
    function isInteger(s) {
      return Math.ceil(s) == Math.floor(s);
    } 
  
    if (loader != 'pie') {
      barContainer.append('<span class="camera_bar_cont" />');
      $('.camera_bar_cont',barContainer)
        .animate({opacity:opts.loaderOpacity},0)
        .css({'position':'absolute', 'left':0, 'right':0, 'top':0, 'bottom':0, 'background-color':opts.loaderBgColor})
        .append('<span id="'+pieID+'" />');
      $('#'+pieID).animate({opacity:0},0);
      var canvas = $('#'+pieID);
      canvas.css({'position':'absolute', 'background-color':opts.loaderColor});
      switch(opts.barPosition){
        case 'left':
          barContainer.css({right:'auto',width:opts.loaderStroke});
          break;
        case 'right':
          barContainer.css({left:'auto',width:opts.loaderStroke});
          break;
        case 'top':
          barContainer.css({bottom:'auto',height:opts.loaderStroke});
          break;
        case 'bottom':
          barContainer.css({top:'auto',height:opts.loaderStroke});
          break;
      }
      switch(barDirection){
        case 'leftToRight':
          canvas.css({'left':0, 'right':0, 'top':opts.loaderPadding, 'bottom':opts.loaderPadding});
          break;
        case 'rightToLeft':
          canvas.css({'left':0, 'right':0, 'top':opts.loaderPadding, 'bottom':opts.loaderPadding});
          break;
        case 'topToBottom':
          canvas.css({'left':opts.loaderPadding, 'right':opts.loaderPadding, 'top':0, 'bottom':0});
          break;
        case 'bottomToTop':
          canvas.css({'left':opts.loaderPadding, 'right':opts.loaderPadding, 'top':0, 'bottom':0});
          break;
      }
    } else {
      pieContainer.append('<canvas id="'+pieID+'"></canvas>');
      var G_vmlCanvasManager;
      var canvas = document.getElementById(pieID);
      canvas.setAttribute("width", opts.pieDiameter);
      canvas.setAttribute("height", opts.pieDiameter);
      var piePosition;
      switch(opts.piePosition){
        case 'leftTop' :
          piePosition = 'left:0; top:0;';
          break;
        case 'rightTop' :
          piePosition = 'right:0; top:0;';
          break;
        case 'leftBottom' :
          piePosition = 'left:0; bottom:0;';
          break;
        case 'rightBottom' :
          piePosition = 'right:0; bottom:0;';
          break;
      }
      canvas.setAttribute("style", "position:absolute; z-index:1002; "+piePosition);
      var rad;
      var radNew;
  
      if (canvas && canvas.getContext) {
        var ctx = canvas.getContext("2d");
        ctx.rotate(Math.PI*(3/2));
        ctx.translate(-opts.pieDiameter,0);
      }
    
    }
    if(loader=='none' || autoAdv==false) {
      $('#'+pieID).hide();
      $('.camera_canvas_wrap',camera_thumbs_wrap).hide();
    }
    
    if($(pagination).length) {
      $(pagination).append('<ul class="camera_pag_ul" />');
      var li;
      for (li = 0; li < amountSlide; li++){
        $('.camera_pag_ul',wrap).append('<li class="pag_nav_'+li+'" style="position:relative; z-index:1002"><span><span>'+li+'</span></span></li>');
      }
      $('.camera_pag_ul li',wrap).hover(function(){
        $(this).addClass('camera_hover');
        if($('.camera_thumb',this).length){
          var wTh = $('.camera_thumb',this).outerWidth(),
          hTh = $('.camera_thumb',this).outerHeight(),
          wTt = $(this).outerWidth();
          $('.camera_thumb',this).show().css({'top':'-'+hTh+'px','left':'-'+(wTh-wTt)/2+'px'}).animate({'opacity':1,'margin-top':'-3px'},200);
          $('.thumb_arrow',this).show().animate({'opacity':1,'margin-top':'-3px'},200);
        }
      },function(){
        $(this).removeClass('camera_hover');
        $('.camera_thumb',this).animate({'margin-top':'-20px','opacity':0},200,function(){
          $(this).css({marginTop:'5px'}).hide();
        });
        $('.thumb_arrow',this).animate({'margin-top':'-20px','opacity':0},200,function(){
          $(this).css({marginTop:'5px'}).hide();
        });
      });
    }
      
  
  
    if($(thumbs).length) {
      var thumbUrl;
      if(!$(pagination).length) {
        $(thumbs).append('<div />');
        $(thumbs).before('<div class="camera_prevThumbs hideNav"><div></div></div>').before('<div class="camera_nextThumbs hideNav"><div></div></div>');
        $('> div',thumbs).append('<ul />');
        $.each(allThumbs, function(i, val) {
          if($('> div', elem).eq(i).attr('data-thumb')!='') {
            var thumbUrl = $('> div', elem).eq(i).attr('data-thumb'),
              newImg = new Image();
            newImg.src = thumbUrl;
            $('ul',thumbs).append('<li class="pix_thumb pix_thumb_'+i+'" />');
            $('li.pix_thumb_'+i,thumbs).append($(newImg).attr('class','camera_thumb'));
          }
        });
      } else {
        $.each(allThumbs, function(i, val) {
          if($('> div', elem).eq(i).attr('data-thumb')!='') {
            var thumbUrl = $('> div', elem).eq(i).attr('data-thumb'),
              newImg = new Image();
            newImg.src = thumbUrl;
            $('li.pag_nav_'+i,pagination).append($(newImg).attr('class','camera_thumb').css({'position':'absolute'}).animate({opacity:0},0));
            $('li.pag_nav_'+i+' > img',pagination).after('<div class="thumb_arrow" />');
            $('li.pag_nav_'+i+' > .thumb_arrow',pagination).animate({opacity:0},0);
          }
        });
        wrap.css({marginBottom:$(pagination).outerHeight()});
      }
    } else if(!$(thumbs).length && $(pagination).length) {
      wrap.css({marginBottom:$(pagination).outerHeight()});
    }

  
    var firstPos = true;

    function thumbnailPos() {
      if($(thumbs).length && !$(pagination).length) {
        var wTh = $(thumbs).outerWidth(),
          owTh = $('ul > li',thumbs).outerWidth(),
          pos = $('li.cameracurrent', thumbs).length ? $('li.cameracurrent', thumbs).position() : '',
          ulW = ($('ul > li', thumbs).length * $('ul > li', thumbs).outerWidth()),
          offUl = $('ul', thumbs).offset().left,
          offDiv = $('> div', thumbs).offset().left,
          ulLeft;

          if(offUl<0){
            ulLeft = '-'+ (offDiv-offUl);
          } else {
            ulLeft = offDiv-offUl;
          }
          
          
          
        if(firstPos == true) {
          $('ul', thumbs).width($('ul > li', thumbs).length * $('ul > li', thumbs).outerWidth());
          if($(thumbs).length && !$(pagination).lenght) {
            wrap.css({marginBottom:$(thumbs).outerHeight()});
          }
          thumbnailVisible();
          /*I repeat this two lines because of a problem with iPhones*/
          $('ul', thumbs).width($('ul > li', thumbs).length * $('ul > li', thumbs).outerWidth());
          if($(thumbs).length && !$(pagination).lenght) {
            wrap.css({marginBottom:$(thumbs).outerHeight()});
          }
          /*...*/
        }
        firstPos = false;
        
          var left = $('li.cameracurrent', thumbs).length ? pos.left : '',
            right = $('li.cameracurrent', thumbs).length ? pos.left+($('li.cameracurrent', thumbs).outerWidth()) : '';
          if(left<$('li.cameracurrent', thumbs).outerWidth()) {
            left = 0;
          }
          if(right-ulLeft>wTh){
            if((left+wTh)<ulW){
              $('ul', thumbs).animate({'margin-left':'-'+(left)+'px'},500,thumbnailVisible);
            } else {
              $('ul', thumbs).animate({'margin-left':'-'+($('ul', thumbs).outerWidth()-wTh)+'px'},500,thumbnailVisible);
            }
          } else if(left-ulLeft<0) {
            $('ul', thumbs).animate({'margin-left':'-'+(left)+'px'},500,thumbnailVisible);
          } else {
            $('ul', thumbs).css({'margin-left':'auto', 'margin-right':'auto'});
            setTimeout(thumbnailVisible,100);
          }
          
      }
    }

    if($(commands).length) {
      $(commands).append('<div class="camera_play"></div>').append('<div class="camera_stop"></div>');
      if(autoAdv==true){
        $('.camera_play',camera_thumbs_wrap).hide();
        $('.camera_stop',camera_thumbs_wrap).show();
      } else {
        $('.camera_stop',camera_thumbs_wrap).hide();
        $('.camera_play',camera_thumbs_wrap).show();
      }
      
    }
      
      
    function canvasLoader() {
      rad = 0;
      var barWidth = $('.camera_bar_cont',camera_thumbs_wrap).width(),
        barHeight = $('.camera_bar_cont',camera_thumbs_wrap).height();

      if (loader != 'pie') {
        switch(barDirection){
          case 'leftToRight':
            $('#'+pieID).css({'right':barWidth});
            break;
          case 'rightToLeft':
            $('#'+pieID).css({'left':barWidth});
            break;
          case 'topToBottom':
            $('#'+pieID).css({'bottom':barHeight});
            break;
          case 'bottomToTop':
            $('#'+pieID).css({'top':barHeight});
            break;
        }
      } else {
        ctx.clearRect(0,0,opts.pieDiameter,opts.pieDiameter); 
      }
    }
    
    
    canvasLoader();
    
    
    $('.moveFromLeft, .moveFromRight, .moveFromTop, .moveFromBottom, .fadeIn, .fadeFromLeft, .fadeFromRight, .fadeFromTop, .fadeFromBottom',fakeHover).each(function(){
      $(this).css('visibility','hidden');
    });
    
    opts.onStartLoading.call(this);
    
    nextSlide();
    
  
  /*************************** FUNCTION nextSlide() ***************************/
  
  function nextSlide(navSlide){ 
    elem.addClass('camerasliding');
    
    videoPresent = false;
    var vis = parseFloat($('div.cameraSlide.cameracurrent',target).index());

    if(navSlide>0){ 
      var slideI = navSlide-1;
    } else if (vis == amountSlide-1) { 
      var slideI = 0;
    } else {
      var slideI = vis+1;
    }
    
        
    var slide = $('.cameraSlide:eq('+slideI+')',target);
    var slideNext = $('.cameraSlide:eq('+(slideI+1)+')',target).addClass('cameranext');
    if( vis != slideI+1 ) {
      slideNext.hide();
    }
    $('.cameraContent',fakeHover).fadeOut(600);
    $('.camera_caption',fakeHover).show();
    
    $('.camerarelative',slide).append($('> div ',elem).eq(slideI).find('> div.camera_effected'));

    $('.camera_target_content .cameraContent:eq('+slideI+')',wrap).append($('> div ',elem).eq(slideI).find('> div'));
    
    if(!$('.imgLoaded',slide).length){
      var imgUrl = allImg[slideI];
      var imgLoaded = new Image();
      imgLoaded.src = imgUrl +"?"+ new Date().getTime();
      slide.css('visibility','hidden');
      slide.prepend($(imgLoaded).attr('class','imgLoaded').css('visibility','hidden'));
      var wT, hT;
      if (!$(imgLoaded).get(0).complete || wT == '0' || hT == '0' || typeof wT === 'undefined' || wT === false || typeof hT === 'undefined' || hT === false) {
        $('.camera_loader',wrap).delay(500).fadeIn(400);
        imgLoaded.onload = function() {
          wT = imgLoaded.naturalWidth;
          hT = imgLoaded.naturalHeight;
          $(imgLoaded).attr('data-alignment',allAlign[slideI]).attr('data-portrait',allPor[slideI]);
          $(imgLoaded).attr('width',wT);
          $(imgLoaded).attr('height',hT);
          target.find('.cameraSlide_'+slideI).hide().css('visibility','visible');
          resizeImage();
          nextSlide(slideI+1);
        };
      }
    } else {
      if( allImg.length > (slideI+1) && !$('.imgLoaded',slideNext).length ){
        var imgUrl2 = allImg[(slideI+1)];
        var imgLoaded2 = new Image();
        imgLoaded2.src = imgUrl2 +"?"+ new Date().getTime();
        slideNext.prepend($(imgLoaded2).attr('class','imgLoaded').css('visibility','hidden'));
        imgLoaded2.onload = function() {
          wT = imgLoaded2.naturalWidth;
          hT = imgLoaded2.naturalHeight;
          $(imgLoaded2).attr('data-alignment',allAlign[slideI+1]).attr('data-portrait',allPor[slideI+1]);
          $(imgLoaded2).attr('width',wT);
          $(imgLoaded2).attr('height',hT);
          resizeImage();
        };
      }
      opts.onLoaded.call(this);
      if($('.camera_loader',wrap).is(':visible')){
        $('.camera_loader',wrap).fadeOut(400);
      } else {
        $('.camera_loader',wrap).css({'visibility':'hidden'});
        $('.camera_loader',wrap).fadeOut(400,function(){
          $('.camera_loader',wrap).css({'visibility':'visible'});
        });
      }
      var rows = opts.rows,
        cols = opts.cols,
        couples = 1,
        difference = 0,
        dataSlideOn,
        time,
        transPeriod,
        fx,
        easing,
        randomFx = new Array('simpleFade','curtainTopLeft','curtainTopRight','curtainBottomLeft','curtainBottomRight','curtainSliceLeft','curtainSliceRight','blindCurtainTopLeft','blindCurtainTopRight','blindCurtainBottomLeft','blindCurtainBottomRight','blindCurtainSliceBottom','blindCurtainSliceTop','stampede','mosaic','mosaicReverse','mosaicRandom','mosaicSpiral','mosaicSpiralReverse','topLeftBottomRight','bottomRightTopLeft','bottomLeftTopRight','topRightBottomLeft','scrollLeft','scrollRight','scrollTop','scrollBottom','scrollHorz');
        marginLeft = 0,
        marginTop = 0,
        opacityOnGrid = 0;
        
        if(opts.opacityOnGrid==true){
          opacityOnGrid = 0;
        } else {
          opacityOnGrid = 1;
        }
 
      
      
      var dataFx = $(' > div',elem).eq(slideI).attr('data-fx');
        
      if(isMobile()&&opts.mobileFx!=''&&opts.mobileFx!='default'){
        fx = opts.mobileFx;
      } else {
        if(typeof dataFx !== 'undefined' && dataFx!== false && dataFx!== 'default'){
          fx = dataFx;
        } else {
          fx = opts.fx;
        }
      }
      
      if(fx=='random') {
        fx = shuffle(randomFx);
        fx = fx[0];
      } else {
        fx = fx;
        if(fx.indexOf(',')>0){
          fx = fx.replace(/ /g,'');
          fx = fx.split(',');
          fx = shuffle(fx);
          fx = fx[0];
        }
      }
      
      dataEasing = $(' > div',elem).eq(slideI).attr('data-easing');
      mobileEasing = $(' > div',elem).eq(slideI).attr('data-mobileEasing');

      if(isMobile()&&opts.mobileEasing!=''&&opts.mobileEasing!='default'){
        if(typeof mobileEasing !== 'undefined' && mobileEasing!== false && mobileEasing!== 'default') {
          easing = mobileEasing;
        } else {
          easing = opts.mobileEasing;
        }
      } else {
        if(typeof dataEasing !== 'undefined' && dataEasing!== false && dataEasing!== 'default') {
          easing = dataEasing;
        } else {
          easing = opts.easing;
        }
      }
  
      dataSlideOn = $(' > div',elem).eq(slideI).attr('data-slideOn');
      if(typeof dataSlideOn !== 'undefined' && dataSlideOn!== false){
        slideOn = dataSlideOn;
      } else {
        if(opts.slideOn=='random'){
          var slideOn = new Array('next','prev');
          slideOn = shuffle(slideOn);
          slideOn = slideOn[0];
        } else {
          slideOn = opts.slideOn;
        }
      }
        
      var dataTime = $(' > div',elem).eq(slideI).attr('data-time');
      if(typeof dataTime !== 'undefined' && dataTime!== false && dataTime!== ''){
        time = parseFloat(dataTime);
      } else {
        time = opts.time;
      }
        
      var dataTransPeriod = $(' > div',elem).eq(slideI).attr('data-transPeriod');
      if(typeof dataTransPeriod !== 'undefined' && dataTransPeriod!== false && dataTransPeriod!== ''){
        transPeriod = parseFloat(dataTransPeriod);
      } else {
        transPeriod = opts.transPeriod;
      }
        
      if(!$(elem).hasClass('camerastarted')){
        fx = 'simpleFade';
        slideOn = 'next';
        easing = '';
        transPeriod = 400;
        $(elem).addClass('camerastarted')
      }
  
      switch(fx){
        case 'simpleFade':
          cols = 1;
          rows = 1;
            break;
        case 'curtainTopLeft':
          if(opts.slicedCols == 0) {
            cols = opts.cols;
          } else {
            cols = opts.slicedCols;
          }
          rows = 1;
            break;
        case 'curtainTopRight':
          if(opts.slicedCols == 0) {
            cols = opts.cols;
          } else {
            cols = opts.slicedCols;
          }
          rows = 1;
            break;
        case 'curtainBottomLeft':
          if(opts.slicedCols == 0) {
            cols = opts.cols;
          } else {
            cols = opts.slicedCols;
          }
          rows = 1;
            break;
        case 'curtainBottomRight':
          if(opts.slicedCols == 0) {
            cols = opts.cols;
          } else {
            cols = opts.slicedCols;
          }
          rows = 1;
            break;
        case 'curtainSliceLeft':
          if(opts.slicedCols == 0) {
            cols = opts.cols;
          } else {
            cols = opts.slicedCols;
          }
          rows = 1;
            break;
        case 'curtainSliceRight':
          if(opts.slicedCols == 0) {
            cols = opts.cols;
          } else {
            cols = opts.slicedCols;
          }
          rows = 1;
            break;
        case 'blindCurtainTopLeft':
          if(opts.slicedRows == 0) {
            rows = opts.rows;
          } else {
            rows = opts.slicedRows;
          }
          cols = 1;
            break;
        case 'blindCurtainTopRight':
          if(opts.slicedRows == 0) {
            rows = opts.rows;
          } else {
            rows = opts.slicedRows;
          }
          cols = 1;
            break;
        case 'blindCurtainBottomLeft':
          if(opts.slicedRows == 0) {
            rows = opts.rows;
          } else {
            rows = opts.slicedRows;
          }
          cols = 1;
            break;
        case 'blindCurtainBottomRight':
          if(opts.slicedRows == 0) {
            rows = opts.rows;
          } else {
            rows = opts.slicedRows;
          }
          cols = 1;
            break;
        case 'blindCurtainSliceTop':
          if(opts.slicedRows == 0) {
            rows = opts.rows;
          } else {
            rows = opts.slicedRows;
          }
          cols = 1;
            break;
        case 'blindCurtainSliceBottom':
          if(opts.slicedRows == 0) {
            rows = opts.rows;
          } else {
            rows = opts.slicedRows;
          }
          cols = 1;
            break;
        case 'stampede':
          difference = '-'+transPeriod;
            break;
        case 'mosaic':
          difference = opts.gridDifference;
            break;
        case 'mosaicReverse':
          difference = opts.gridDifference;
            break;
        case 'mosaicRandom':
            break;
        case 'mosaicSpiral':
          difference = opts.gridDifference;
          couples = 1.7;
            break;
        case 'mosaicSpiralReverse':
          difference = opts.gridDifference;
          couples = 1.7;
            break;
        case 'topLeftBottomRight':
          difference = opts.gridDifference;
          couples = 6;
            break;
        case 'bottomRightTopLeft':
          difference = opts.gridDifference;
          couples = 6;
            break;
        case 'bottomLeftTopRight':
          difference = opts.gridDifference;
          couples = 6;
            break;
        case 'topRightBottomLeft':
          difference = opts.gridDifference;
          couples = 6;
            break;
        case 'scrollLeft':
          cols = 1;
          rows = 1;
            break;
        case 'scrollRight':
          cols = 1;
          rows = 1;
            break;
        case 'scrollTop':
          cols = 1;
          rows = 1;
            break;
        case 'scrollBottom':
          cols = 1;
          rows = 1;
            break;
        case 'scrollHorz':
          cols = 1;
          rows = 1;
            break;
      }
      
      var cycle = 0;
      var blocks = rows*cols;
      var leftScrap = w-(Math.floor(w/cols)*cols);
      var topScrap = h-(Math.floor(h/rows)*rows);
      var addLeft;
      var addTop;
      var tAppW = 0;  
      var tAppH = 0;
      var arr = new Array();
      var delay = new Array();
      var order = new Array();
      while(cycle < blocks){
        arr.push(cycle);
        delay.push(cycle);
        cameraCont.append('<div class="cameraappended" style="display:none; overflow:hidden; position:absolute; z-index:1000" />');
        var tApp = $('.cameraappended:eq('+cycle+')',target);
        if(fx=='scrollLeft' || fx=='scrollRight' || fx=='scrollTop' || fx=='scrollBottom' || fx=='scrollHorz'){
          selector.eq(slideI).clone().show().appendTo(tApp);
        } else {
          if(slideOn=='next'){
            selector.eq(slideI).clone().show().appendTo(tApp);
          } else {
            selector.eq(vis).clone().show().appendTo(tApp);
          }
        }

        if(cycle%cols<leftScrap){
          addLeft = 1;
        } else {
          addLeft = 0;
        }
        if(cycle%cols==0){
          tAppW = 0;
        }
        if(Math.floor(cycle/cols)<topScrap){
          addTop = 1;
        } else {
          addTop = 0;
        }
        tApp.css({
          'height': Math.floor((h/rows)+addTop+1),
          'left': tAppW,
          'top': tAppH,
          'width': Math.floor((w/cols)+addLeft+1)
        });
        $('> .cameraSlide', tApp).css({
          'height': h,
          'margin-left': '-'+tAppW+'px',
          'margin-top': '-'+tAppH+'px',
          'width': w
        });
        tAppW = tAppW+tApp.width()-1;
        if(cycle%cols==cols-1){
          tAppH = tAppH + tApp.height() - 1;
        }
        cycle++;
      }
      

      
      switch(fx){
        case 'curtainTopLeft':
            break;
        case 'curtainBottomLeft':
            break;
        case 'curtainSliceLeft':
            break;
        case 'curtainTopRight':
          arr = arr.reverse();
            break;
        case 'curtainBottomRight':
          arr = arr.reverse();
            break;
        case 'curtainSliceRight':
          arr = arr.reverse();
            break;
        case 'blindCurtainTopLeft':
            break;
        case 'blindCurtainBottomLeft':
          arr = arr.reverse();
            break;
        case 'blindCurtainSliceTop':
            break;
        case 'blindCurtainTopRight':
            break;
        case 'blindCurtainBottomRight':
          arr = arr.reverse();
            break;
        case 'blindCurtainSliceBottom':
          arr = arr.reverse();
            break;
        case 'stampede':
          arr = shuffle(arr);
            break;
        case 'mosaic':
            break;
        case 'mosaicReverse':
          arr = arr.reverse();
            break;
        case 'mosaicRandom':
          arr = shuffle(arr);
            break;
        case 'mosaicSpiral':
          var rows2 = rows/2, x, y, z, n=0;
            for (z = 0; z < rows2; z++){
              y = z;
              for (x = z; x < cols - z - 1; x++) {
                order[n++] = y * cols + x;
              }
              x = cols - z - 1;
              for (y = z; y < rows - z - 1; y++) {
                order[n++] = y * cols + x;
              }
              y = rows - z - 1;
              for (x = cols - z - 1; x > z; x--) {
                order[n++] = y * cols + x;
              }
              x = z;
              for (y = rows - z - 1; y > z; y--) {
                order[n++] = y * cols + x;
              }
            }
            
            arr = order;

            break;
        case 'mosaicSpiralReverse':
          var rows2 = rows/2, x, y, z, n=blocks-1;
            for (z = 0; z < rows2; z++){
              y = z;
              for (x = z; x < cols - z - 1; x++) {
                order[n--] = y * cols + x;
              }
              x = cols - z - 1;
              for (y = z; y < rows - z - 1; y++) {
                order[n--] = y * cols + x;
              }
              y = rows - z - 1;
              for (x = cols - z - 1; x > z; x--) {
                order[n--] = y * cols + x;
              }
              x = z;
              for (y = rows - z - 1; y > z; y--) {
                order[n--] = y * cols + x;
              }
            }

            arr = order;
            
            break;
        case 'topLeftBottomRight':
          for (var y = 0; y < rows; y++)
          for (var x = 0; x < cols; x++) {
            order.push(x + y);
          }
            delay = order;
            break;
        case 'bottomRightTopLeft':
          for (var y = 0; y < rows; y++)
          for (var x = 0; x < cols; x++) {
            order.push(x + y);
          }
            delay = order.reverse();
            break;
        case 'bottomLeftTopRight':
          for (var y = rows; y > 0; y--)
          for (var x = 0; x < cols; x++) {
            order.push(x + y);
          }
            delay = order;
            break;
        case 'topRightBottomLeft':
          for (var y = 0; y < rows; y++)
          for (var x = cols; x > 0; x--) {
            order.push(x + y);
          }
            delay = order;
            break;
      }
      
      
            
      $.each(arr, function(index, value) {

        if(value%cols<leftScrap){
          addLeft = 1;
        } else {
          addLeft = 0;
        }
        if(value%cols==0){
          tAppW = 0;
        }
        if(Math.floor(value/cols)<topScrap){
          addTop = 1;
        } else {
          addTop = 0;
        }
              
        switch(fx){
          case 'simpleFade':
            height = h;
            width = w;
            opacityOnGrid = 0;
              break;
          case 'curtainTopLeft':
            height = 0,
            width = Math.floor((w/cols)+addLeft+1),
            marginTop = '-'+Math.floor((h/rows)+addTop+1)+'px';
              break;
          case 'curtainTopRight':
            height = 0,
            width = Math.floor((w/cols)+addLeft+1),
            marginTop = '-'+Math.floor((h/rows)+addTop+1)+'px';
              break;
          case 'curtainBottomLeft':
            height = 0,
            width = Math.floor((w/cols)+addLeft+1),
            marginTop = Math.floor((h/rows)+addTop+1)+'px';
              break;
          case 'curtainBottomRight':
            height = 0,
            width = Math.floor((w/cols)+addLeft+1),
            marginTop = Math.floor((h/rows)+addTop+1)+'px';
              break;
          case 'curtainSliceLeft':
            height = 0,
            width = Math.floor((w/cols)+addLeft+1);
            if(value%2==0){
              marginTop = Math.floor((h/rows)+addTop+1)+'px';         
            } else {
              marginTop = '-'+Math.floor((h/rows)+addTop+1)+'px';         
            }
              break;
          case 'curtainSliceRight':
            height = 0,
            width = Math.floor((w/cols)+addLeft+1);
            if(value%2==0){
              marginTop = Math.floor((h/rows)+addTop+1)+'px';         
            } else {
              marginTop = '-'+Math.floor((h/rows)+addTop+1)+'px';         
            }
              break;
          case 'blindCurtainTopLeft':
            height = Math.floor((h/rows)+addTop+1),
            width = 0,
            marginLeft = '-'+Math.floor((w/cols)+addLeft+1)+'px';
              break;
          case 'blindCurtainTopRight':
            height = Math.floor((h/rows)+addTop+1),
            width = 0,
            marginLeft = Math.floor((w/cols)+addLeft+1)+'px';
              break;
          case 'blindCurtainBottomLeft':
            height = Math.floor((h/rows)+addTop+1),
            width = 0,
            marginLeft = '-'+Math.floor((w/cols)+addLeft+1)+'px';
              break;
          case 'blindCurtainBottomRight':
            height = Math.floor((h/rows)+addTop+1),
            width = 0,
            marginLeft = Math.floor((w/cols)+addLeft+1)+'px';
              break;
          case 'blindCurtainSliceBottom':
            height = Math.floor((h/rows)+addTop+1),
            width = 0;
            if(value%2==0){
              marginLeft = '-'+Math.floor((w/cols)+addLeft+1)+'px';
            } else {
              marginLeft = Math.floor((w/cols)+addLeft+1)+'px';
            }
              break;
          case 'blindCurtainSliceTop':
            height = Math.floor((h/rows)+addTop+1),
            width = 0;
            if(value%2==0){
              marginLeft = '-'+Math.floor((w/cols)+addLeft+1)+'px';
            } else {
              marginLeft = Math.floor((w/cols)+addLeft+1)+'px';
            }
              break;
          case 'stampede':
            height = 0;
            width = 0;          
            marginLeft = (w*0.2)*(((index)%cols)-(cols-(Math.floor(cols/2))))+'px';         
            marginTop = (h*0.2)*((Math.floor(index/cols)+1)-(rows-(Math.floor(rows/2))))+'px';  
              break;
          case 'mosaic':
            height = 0;
            width = 0;          
              break;
          case 'mosaicReverse':
            height = 0;
            width = 0;          
            marginLeft = Math.floor((w/cols)+addLeft+1)+'px';         
            marginTop = Math.floor((h/rows)+addTop+1)+'px';         
              break;
          case 'mosaicRandom':
            height = 0;
            width = 0;          
            marginLeft = Math.floor((w/cols)+addLeft+1)*0.5+'px';         
            marginTop = Math.floor((h/rows)+addTop+1)*0.5+'px';         
              break;
          case 'mosaicSpiral':
            height = 0;
            width = 0;
            marginLeft = Math.floor((w/cols)+addLeft+1)*0.5+'px';         
            marginTop = Math.floor((h/rows)+addTop+1)*0.5+'px';         
              break;
          case 'mosaicSpiralReverse':
            height = 0;
            width = 0;
            marginLeft = Math.floor((w/cols)+addLeft+1)*0.5+'px';         
            marginTop = Math.floor((h/rows)+addTop+1)*0.5+'px';         
              break;
          case 'topLeftBottomRight':
            height = 0;
            width = 0;          
              break;
          case 'bottomRightTopLeft':
            height = 0;
            width = 0;          
            marginLeft = Math.floor((w/cols)+addLeft+1)+'px';         
            marginTop = Math.floor((h/rows)+addTop+1)+'px';         
              break;
          case 'bottomLeftTopRight':
            height = 0;
            width = 0;          
            marginLeft = 0;         
            marginTop = Math.floor((h/rows)+addTop+1)+'px';         
              break;
          case 'topRightBottomLeft':
            height = 0;
            width = 0;          
            marginLeft = Math.floor((w/cols)+addLeft+1)+'px';         
            marginTop = 0;          
              break;
          case 'scrollRight':
            height = h;
            width = w;
            marginLeft = -w;          
              break;
          case 'scrollLeft':
            height = h;
            width = w;
            marginLeft = w;         
              break;
          case 'scrollTop':
            height = h;
            width = w;
            marginTop = h;          
              break;
          case 'scrollBottom':
            height = h;
            width = w;
            marginTop = -h;         
              break;
          case 'scrollHorz':
            height = h;
            width = w;
            if(vis==0 && slideI==amountSlide-1) {
              marginLeft = -w;  
            } else if(vis<slideI  || (vis==amountSlide-1 && slideI==0)) {
              marginLeft = w; 
            } else {
              marginLeft = -w;  
            }
              break;
          }
          
      
        var tApp = $('.cameraappended:eq('+value+')',target);
                
        if(typeof u !== 'undefined'){
          clearInterval(u);
          clearTimeout(setT);
          setT = setTimeout(canvasLoader,transPeriod+difference);
        }
        
        
        if($(pagination).length){
          $('.camera_pag li',wrap).removeClass('cameracurrent');
          $('.camera_pag li',wrap).eq(slideI).addClass('cameracurrent');
        }
            
        if($(thumbs).length){
          $('li', thumbs).removeClass('cameracurrent');
          $('li', thumbs).eq(slideI).addClass('cameracurrent');
          $('li', thumbs).not('.cameracurrent').find('img').animate({opacity:.5},0);
          $('li.cameracurrent img', thumbs).animate({opacity:1},0);
          $('li', thumbs).hover(function(){
            $('img',this).stop(true,false).animate({opacity:1},150);
          },function(){
            if(!$(this).hasClass('cameracurrent')){
              $('img',this).stop(true,false).animate({opacity:.5},150);
            }
          });
        }
                
            
        var easedTime = parseFloat(transPeriod)+parseFloat(difference);
        
        function cameraeased() {

          $(this).addClass('cameraeased');
          if($('.cameraeased',target).length>=0){
            $(thumbs).css({visibility:'visible'});
          }
          if($('.cameraeased',target).length==blocks){
            
            thumbnailPos();
            
            $('.moveFromLeft, .moveFromRight, .moveFromTop, .moveFromBottom, .fadeIn, .fadeFromLeft, .fadeFromRight, .fadeFromTop, .fadeFromBottom',fakeHover).each(function(){
              $(this).css('visibility','hidden');
            });
    
            selector.eq(slideI).show().css('z-index','999').removeClass('cameranext').addClass('cameracurrent');
            selector.eq(vis).css('z-index','1').removeClass('cameracurrent');
            $('.cameraContent',fakeHover).eq(slideI).addClass('cameracurrent');
            if (vis >= 0) {
              $('.cameraContent',fakeHover).eq(vis).removeClass('cameracurrent');
            }
            
            opts.onEndTransition.call(this);
            
            if($('> div', elem).eq(slideI).attr('data-video')!='hide' && $('.cameraContent.cameracurrent .imgFake',fakeHover).length ){
              $('.cameraContent.cameracurrent .imgFake',fakeHover).click();
            }

            
            var lMoveIn = selector.eq(slideI).find('.fadeIn').length;
            var lMoveInContent = $('.cameraContent',fakeHover).eq(slideI).find('.moveFromLeft, .moveFromRight, .moveFromTop, .moveFromBottom, .fadeIn, .fadeFromLeft, .fadeFromRight, .fadeFromTop, .fadeFromBottom').length;
            
            if (lMoveIn!=0){
              $('.cameraSlide.cameracurrent .fadeIn',fakeHover).each(function(){
                if($(this).attr('data-easing')!=''){
                  var easeMove = $(this).attr('data-easing');
                } else {
                  var easeMove = easing;
                }
                var t = $(this);
                if(typeof t.attr('data-outerWidth') === 'undefined' || t.attr('data-outerWidth') === false || t.attr('data-outerWidth') === '') {
                  var wMoveIn = t.outerWidth();
                  t.attr('data-outerWidth',wMoveIn);
                } else {
                  var wMoveIn = t.attr('data-outerWidth');
                }
                if(typeof t.attr('data-outerHeight') === 'undefined' || t.attr('data-outerHeight') === false || t.attr('data-outerHeight') === '') {
                  var hMoveIn = t.outerHeight();
                  t.attr('data-outerHeight',hMoveIn);
                } else {
                  var hMoveIn = t.attr('data-outerHeight');
                }
                //t.css('width',wMoveIn);
                var pos = t.position();
                var left = pos.left;
                var top = pos.top;
                var tClass = t.attr('class');
                var ind = t.index();
                var hRel = t.parents('.camerarelative').outerHeight();
                var wRel = t.parents('.camerarelative').outerWidth();
                if(tClass.indexOf("fadeIn") != -1) {
                  t.animate({opacity:0},0).css('visibility','visible').delay((time/lMoveIn)*(0.1*(ind-1))).animate({opacity:1},(time/lMoveIn)*0.15,easeMove);
                } else {
                  t.css('visibility','visible');
                }
              });
            }

            $('.cameraContent.cameracurrent',fakeHover).show();
            if (lMoveInContent!=0){
              
              $('.cameraContent.cameracurrent .moveFromLeft, .cameraContent.cameracurrent .moveFromRight, .cameraContent.cameracurrent .moveFromTop, .cameraContent.cameracurrent .moveFromBottom, .cameraContent.cameracurrent .fadeIn, .cameraContent.cameracurrent .fadeFromLeft, .cameraContent.cameracurrent .fadeFromRight, .cameraContent.cameracurrent .fadeFromTop, .cameraContent.cameracurrent .fadeFromBottom',fakeHover).each(function(){
                if($(this).attr('data-easing')!=''){
                  var easeMove = $(this).attr('data-easing');
                } else {
                  var easeMove = easing;
                }
                var t = $(this);
                var pos = t.position();
                var left = pos.left;
                var top = pos.top;
                var tClass = t.attr('class');
                var ind = t.index();
                var thisH = t.outerHeight();
                if(tClass.indexOf("moveFromLeft") != -1) {
                  t.css({'left':'-'+(w)+'px','right':'auto'});
                  t.css('visibility','visible').delay((time/lMoveInContent)*(0.1*(ind-1))).animate({'left':pos.left},(time/lMoveInContent)*0.15,easeMove);
                } else if(tClass.indexOf("moveFromRight") != -1) {
                  t.css({'left':w+'px','right':'auto'});
                  t.css('visibility','visible').delay((time/lMoveInContent)*(0.1*(ind-1))).animate({'left':pos.left},(time/lMoveInContent)*0.15,easeMove);
                } else if(tClass.indexOf("moveFromTop") != -1) {
                  t.css({'top':'-'+h+'px','bottom':'auto'});
                  t.css('visibility','visible').delay((time/lMoveInContent)*(0.1*(ind-1))).animate({'top':pos.top},(time/lMoveInContent)*0.15,easeMove,function(){
                    t.css({top:'auto',bottom:0});
                  });
                } else if(tClass.indexOf("moveFromBottom") != -1) {
                  t.css({'top':h+'px','bottom':'auto'});
                  t.css('visibility','visible').delay((time/lMoveInContent)*(0.1*(ind-1))).animate({'top':pos.top},(time/lMoveInContent)*0.15,easeMove);
                } else if(tClass.indexOf("fadeFromLeft") != -1) {
                  t.animate({opacity:0},0).css({'left':'-'+(w)+'px','right':'auto'});
                  t.css('visibility','visible').delay((time/lMoveInContent)*(0.1*(ind-1))).animate({'left':pos.left,opacity:1},(time/lMoveInContent)*0.15,easeMove);
                } else if(tClass.indexOf("fadeFromRight") != -1) {
                  t.animate({opacity:0},0).css({'left':(w)+'px','right':'auto'});
                  t.css('visibility','visible').delay((time/lMoveInContent)*(0.1*(ind-1))).animate({'left':pos.left,opacity:1},(time/lMoveInContent)*0.15,easeMove);
                } else if(tClass.indexOf("fadeFromTop") != -1) {
                  t.animate({opacity:0},0).css({'top':'-'+(h)+'px','bottom':'auto'});
                  t.css('visibility','visible').delay((time/lMoveInContent)*(0.1*(ind-1))).animate({'top':pos.top,opacity:1},(time/lMoveInContent)*0.15,easeMove,function(){
                    t.css({top:'auto',bottom:0});
                  });
                } else if(tClass.indexOf("fadeFromBottom") != -1) {
                  t.animate({opacity:0},0).css({'bottom':'-'+thisH+'px'});
                  t.css('visibility','visible').delay((time/lMoveInContent)*(0.1*(ind-1))).animate({'bottom':'0',opacity:1},(time/lMoveInContent)*0.15,easeMove);
                } else if(tClass.indexOf("fadeIn") != -1) {
                  t.animate({opacity:0},0).css('visibility','visible').delay((time/lMoveInContent)*(0.1*(ind-1))).animate({opacity:1},(time/lMoveInContent)*0.15,easeMove);
                } else {
                  t.css('visibility','visible');
                }
              });
            }

            
            $('.cameraappended',target).remove();
            elem.removeClass('camerasliding');  
              selector.eq(vis).hide();
              var barWidth = $('.camera_bar_cont',camera_thumbs_wrap).width(),
                barHeight = $('.camera_bar_cont',camera_thumbs_wrap).height(),
                radSum;
              if (loader != 'pie') {
                radSum = 0.05;
              } else {
                radSum = 0.005;
              }
              $('#'+pieID).animate({opacity:opts.loaderOpacity},200);
              u = setInterval(
                function(){
                  if(elem.hasClass('stopped')){
                    clearInterval(u);
                  }
                  if (loader != 'pie') {
                    if(rad<=1.002 && !elem.hasClass('stopped') && !elem.hasClass('paused') && !elem.hasClass('hovered')){
                      rad = (rad+radSum);
                    } else if (rad<=1 && (elem.hasClass('stopped') || elem.hasClass('paused') || elem.hasClass('stopped') || elem.hasClass('hovered'))){
                      rad = rad;
                    } else {
                      if(!elem.hasClass('stopped') && !elem.hasClass('paused') && !elem.hasClass('hovered')) {
                        clearInterval(u);
                        imgFake();
                        $('#'+pieID).animate({opacity:0},200,function(){
                          clearTimeout(setT);
                          setT = setTimeout(canvasLoader,easedTime);
                          nextSlide();
                          opts.onStartLoading.call(this);
                        });
                      }
                    }
                    switch(barDirection){
                      case 'leftToRight':
                        $('#'+pieID).animate({'right':barWidth-(barWidth*rad)},(time*radSum),'linear');
                        break;
                      case 'rightToLeft':
                        $('#'+pieID).animate({'left':barWidth-(barWidth*rad)},(time*radSum),'linear');
                        break;
                      case 'topToBottom':
                        $('#'+pieID).animate({'bottom':barHeight-(barHeight*rad)},(time*radSum),'linear');
                        break;
                      case 'bottomToTop':
                        $('#'+pieID).animate({'bottom':barHeight-(barHeight*rad)},(time*radSum),'linear');
                        break;
                    }
                    
                  } else {
                    radNew = rad;
                    ctx.clearRect(0,0,opts.pieDiameter,opts.pieDiameter);
                    ctx.globalCompositeOperation = 'destination-over';
                    ctx.beginPath();
                    ctx.arc((opts.pieDiameter)/2, (opts.pieDiameter)/2, (opts.pieDiameter)/2-opts.loaderStroke,0,Math.PI*2,false);
                    ctx.lineWidth = opts.loaderStroke;
                    ctx.strokeStyle = opts.loaderBgColor;
                    ctx.stroke();
                    ctx.closePath();
                    ctx.globalCompositeOperation = 'source-over';
                    ctx.beginPath();
                    ctx.arc((opts.pieDiameter)/2, (opts.pieDiameter)/2, (opts.pieDiameter)/2-opts.loaderStroke,0,Math.PI*2*radNew,false);
                    ctx.lineWidth = opts.loaderStroke-(opts.loaderPadding*2);
                    ctx.strokeStyle = opts.loaderColor;
                    ctx.stroke();
                    ctx.closePath();
                        
                    if(rad<=1.002 && !elem.hasClass('stopped') && !elem.hasClass('paused') && !elem.hasClass('hovered')){
                      rad = (rad+radSum);
                    } else if (rad<=1 && (elem.hasClass('stopped') || elem.hasClass('paused') || elem.hasClass('hovered'))){
                      rad = rad;
                    } else {
                      if(!elem.hasClass('stopped') && !elem.hasClass('paused') && !elem.hasClass('hovered')) {
                        clearInterval(u);
                        imgFake();
                        $('#'+pieID+', .camera_canvas_wrap',camera_thumbs_wrap).animate({opacity:0},200,function(){
                          clearTimeout(setT);
                          setT = setTimeout(canvasLoader,easedTime);
                          nextSlide();
                          opts.onStartLoading.call(this);
                        });
                      }
                    }
                  }
                },time*radSum
              );
            }

        }


        
        if(fx=='scrollLeft' || fx=='scrollRight' || fx=='scrollTop' || fx=='scrollBottom' || fx=='scrollHorz'){
          opts.onStartTransition.call(this);
          easedTime = 0;
          tApp.delay((((transPeriod+difference)/blocks)*delay[index]*couples)*0.5).css({
              'display' : 'block',
              'height': height,
              'margin-left': marginLeft,
              'margin-top': marginTop,
              'width': width
            }).animate({
              'height': Math.floor((h/rows)+addTop+1),
              'margin-top' : 0,
              'margin-left' : 0,
              'width' : Math.floor((w/cols)+addLeft+1)
            },(transPeriod-difference),easing,cameraeased);
          selector.eq(vis).delay((((transPeriod+difference)/blocks)*delay[index]*couples)*0.5).animate({
              'margin-left': marginLeft*(-1),
              'margin-top': marginTop*(-1)
            },(transPeriod-difference),easing,function(){
              $(this).css({'margin-top' : 0,'margin-left' : 0});
            });
        } else {
          opts.onStartTransition.call(this);
          easedTime = parseFloat(transPeriod)+parseFloat(difference);
          if(slideOn=='next'){
            tApp.delay((((transPeriod+difference)/blocks)*delay[index]*couples)*0.5).css({
                'display' : 'block',
                'height': height,
                'margin-left': marginLeft,
                'margin-top': marginTop,
                'width': width,
                'opacity' : opacityOnGrid
              }).animate({
                'height': Math.floor((h/rows)+addTop+1),
                'margin-top' : 0,
                'margin-left' : 0,
                'opacity' : 1,
                'width' : Math.floor((w/cols)+addLeft+1)
              },(transPeriod-difference),easing,cameraeased);
          } else {
            selector.eq(slideI).show().css('z-index','999').addClass('cameracurrent');
            selector.eq(vis).css('z-index','1').removeClass('cameracurrent');
            $('.cameraContent',fakeHover).eq(slideI).addClass('cameracurrent');
            $('.cameraContent',fakeHover).eq(vis).removeClass('cameracurrent');
            tApp.delay((((transPeriod+difference)/blocks)*delay[index]*couples)*0.5).css({
                'display' : 'block',
                'height': Math.floor((h/rows)+addTop+1),
                'margin-top' : 0,
                'margin-left' : 0,
                'opacity' : 1,
                'width' : Math.floor((w/cols)+addLeft+1)
              }).animate({
                'height': height,
                'margin-left': marginLeft,
                'margin-top': marginTop,
                'width': width,
                'opacity' : opacityOnGrid
              },(transPeriod-difference),easing,cameraeased);
          }
        }





      });
        
        
        
   
    }
  }


        if($(prevNav).length){
          $(prevNav).click(function(){
            if(!elem.hasClass('camerasliding')){
              var idNum = parseFloat($('.cameraSlide.cameracurrent',target).index());
              clearInterval(u);
              imgFake();
              $('#'+pieID+', .camera_canvas_wrap',wrap).animate({opacity:0},0);
              canvasLoader();
              if(idNum!=0){
                nextSlide(idNum);
              } else {
                nextSlide(amountSlide);
               }
               opts.onStartLoading.call(this);
            }
          });
        }
      
        if($(nextNav).length){
          $(nextNav).click(function(){
            if(!elem.hasClass('camerasliding')){
              var idNum = parseFloat($('.cameraSlide.cameracurrent',target).index()); 
              clearInterval(u);
              imgFake();
              $('#'+pieID+', .camera_canvas_wrap',camera_thumbs_wrap).animate({opacity:0},0);
              canvasLoader();
              if(idNum==amountSlide-1){
                nextSlide(1);
              } else {
                nextSlide(idNum+2);
               }
               opts.onStartLoading.call(this);
            }
          });
        }


        if(isMobile()){
          fakeHover.bind('swipeleft',function(event){
            if(!elem.hasClass('camerasliding')){
              var idNum = parseFloat($('.cameraSlide.cameracurrent',target).index()); 
              clearInterval(u);
              imgFake();
              $('#'+pieID+', .camera_canvas_wrap',camera_thumbs_wrap).animate({opacity:0},0);
              canvasLoader();
              if(idNum==amountSlide-1){
                nextSlide(1);
              } else {
                nextSlide(idNum+2);
               }
               opts.onStartLoading.call(this);
            }
          });
          fakeHover.bind('swiperight',function(event){
            if(!elem.hasClass('camerasliding')){
              var idNum = parseFloat($('.cameraSlide.cameracurrent',target).index());
              clearInterval(u);
              imgFake();
              $('#'+pieID+', .camera_canvas_wrap',camera_thumbs_wrap).animate({opacity:0},0);
              canvasLoader();
              if(idNum!=0){
                nextSlide(idNum);
              } else {
                nextSlide(amountSlide);
               }
               opts.onStartLoading.call(this);
            }
          });
        }

        if($(pagination).length){
          $('.camera_pag li',wrap).click(function(){
            if(!elem.hasClass('camerasliding')){
              var idNum = parseFloat($(this).index());
              var curNum = parseFloat($('.cameraSlide.cameracurrent',target).index());
              if(idNum!=curNum) {
                clearInterval(u);
                imgFake();
                $('#'+pieID+', .camera_canvas_wrap',camera_thumbs_wrap).animate({opacity:0},0);
                canvasLoader();
                nextSlide(idNum+1);
                opts.onStartLoading.call(this);
              }
            }
          });
        }

        if($(thumbs).length) {

          $('.pix_thumb img',thumbs).click(function(){
            if(!elem.hasClass('camerasliding')){
              var idNum = parseFloat($(this).parents('li').index());
              var curNum = parseFloat($('.cameracurrent',target).index());
              if(idNum!=curNum) {
                clearInterval(u);
                imgFake();
                $('#'+pieID+', .camera_canvas_wrap',camera_thumbs_wrap).animate({opacity:0},0);
                $('.pix_thumb',thumbs).removeClass('cameracurrent');
                $(this).parents('li').addClass('cameracurrent');
                canvasLoader();
                nextSlide(idNum+1);
                thumbnailPos();
                opts.onStartLoading.call(this);
              }
            }
          });

          $('.camera_thumbs_cont .camera_prevThumbs',camera_thumbs_wrap).hover(function(){
            $(this).stop(true,false).animate({opacity:1},250);
          },function(){
            $(this).stop(true,false).animate({opacity:.7},250);
          });
          $('.camera_prevThumbs',camera_thumbs_wrap).click(function(){
            var sum = 0,
              wTh = $(thumbs).outerWidth(),
              offUl = $('ul', thumbs).offset().left,
              offDiv = $('> div', thumbs).offset().left,
              ulLeft = offDiv-offUl;
              $('.camera_visThumb',thumbs).each(function(){
                var tW = $(this).outerWidth();
                sum = sum+tW;
              });
              if(ulLeft-sum>0){
                $('ul', thumbs).animate({'margin-left':'-'+(ulLeft-sum)+'px'},500,thumbnailVisible);
              } else {
                $('ul', thumbs).animate({'margin-left':0},500,thumbnailVisible);
              }
          });

          $('.camera_thumbs_cont .camera_nextThumbs',camera_thumbs_wrap).hover(function(){
            $(this).stop(true,false).animate({opacity:1},250);
          },function(){
            $(this).stop(true,false).animate({opacity:.7},250);
          });
          $('.camera_nextThumbs',camera_thumbs_wrap).click(function(){
            var sum = 0,
              wTh = $(thumbs).outerWidth(),
              ulW = $('ul', thumbs).outerWidth(),
              offUl = $('ul', thumbs).offset().left,
              offDiv = $('> div', thumbs).offset().left,
              ulLeft = offDiv-offUl;
              $('.camera_visThumb',thumbs).each(function(){
                var tW = $(this).outerWidth();
                sum = sum+tW;
              });
              if(ulLeft+sum+sum<ulW){
                $('ul', thumbs).animate({'margin-left':'-'+(ulLeft+sum)+'px'},500,thumbnailVisible);
              } else {
                $('ul', thumbs).animate({'margin-left':'-'+(ulW-wTh)+'px'},500,thumbnailVisible);
              }
          });

        }
    
    
  
}

})(jQuery);

;(function($){$.fn.cameraStop = function() {
  var wrap = $(this),
    elem = $('.camera_src',wrap),
    pieID = 'pie_'+wrap.index();
  elem.addClass('stopped');
  if($('.camera_showcommands').length) {
    var camera_thumbs_wrap = $('.camera_thumbs_wrap',wrap);
  } else {
    var camera_thumbs_wrap = wrap;
  }
}
})(jQuery);

;(function($){$.fn.cameraPause = function() {
  var wrap = $(this);
  var elem = $('.camera_src',wrap);
  elem.addClass('paused');
}
})(jQuery);

;(function($){$.fn.cameraResume = function() {
  var wrap = $(this);
  var elem = $('.camera_src',wrap);
  if(typeof autoAdv === 'undefined' || autoAdv!==true){
    elem.removeClass('paused');
  }
}
})(jQuery);

// fin camra 
//  jQuery Mobile framework customized for Camera slideshow, made by
//  'jquery.mobile.define.js',
//  'jquery.ui.widget.js',
//  'jquery.mobile.widget.js',
//  'jquery.mobile.media.js',
//  'jquery.mobile.support.js',
//  'jquery.mobile.vmouse.js',
//  'jquery.mobile.event.js',
//  'jquery.mobile.core.js'
window.define=function(){Array.prototype.slice.call(arguments).pop()(window.jQuery)};define(["jquery"],function(a){(function(a,b){if(a.cleanData){var c=a.cleanData;a.cleanData=function(b){for(var d=0,e;(e=b[d])!=null;d++){a(e).triggerHandler("remove")}c(b)}}else{var d=a.fn.remove;a.fn.remove=function(b,c){return this.each(function(){if(!c){if(!b||a.filter(b,[this]).length){a("*",this).add([this]).each(function(){a(this).triggerHandler("remove")})}}return d.call(a(this),b,c)})}}a.widget=function(b,c,d){var e=b.split(".")[0],f;b=b.split(".")[1];f=e+"-"+b;if(!d){d=c;c=a.Widget}a.expr[":"][f]=function(c){return!!a.data(c,b)};a[e]=a[e]||{};a[e][b]=function(a,b){if(arguments.length){this._createWidget(a,b)}};var g=new c;g.options=a.extend(true,{},g.options);a[e][b].prototype=a.extend(true,g,{namespace:e,widgetName:b,widgetEventPrefix:a[e][b].prototype.widgetEventPrefix||b,widgetBaseClass:f},d);a.widget.bridge(b,a[e][b])};a.widget.bridge=function(c,d){a.fn[c]=function(e){var f=typeof e==="string",g=Array.prototype.slice.call(arguments,1),h=this;e=!f&&g.length?a.extend.apply(null,[true,e].concat(g)):e;if(f&&e.charAt(0)==="_"){return h}if(f){this.each(function(){var d=a.data(this,c);if(!d){throw"cannot call methods on "+c+" prior to initialization; "+"attempted to call method '"+e+"'"}if(!a.isFunction(d[e])){throw"no such method '"+e+"' for "+c+" widget instance"}var f=d[e].apply(d,g);if(f!==d&&f!==b){h=f;return false}})}else{this.each(function(){var b=a.data(this,c);if(b){b.option(e||{})._init()}else{a.data(this,c,new d(e,this))}})}return h}};a.Widget=function(a,b){if(arguments.length){this._createWidget(a,b)}};a.Widget.prototype={widgetName:"widget",widgetEventPrefix:"",options:{disabled:false},_createWidget:function(b,c){a.data(c,this.widgetName,this);this.element=a(c);this.options=a.extend(true,{},this.options,this._getCreateOptions(),b);var d=this;this.element.bind("remove."+this.widgetName,function(){d.destroy()});this._create();this._trigger("create");this._init()},_getCreateOptions:function(){var b={};if(a.metadata){b=a.metadata.get(element)[this.widgetName]}return b},_create:function(){},_init:function(){},destroy:function(){this.element.unbind("."+this.widgetName).removeData(this.widgetName);this.widget().unbind("."+this.widgetName).removeAttr("aria-disabled").removeClass(this.widgetBaseClass+"-disabled "+"ui-state-disabled")},widget:function(){return this.element},option:function(c,d){var e=c;if(arguments.length===0){return a.extend({},this.options)}if(typeof c==="string"){if(d===b){return this.options[c]}e={};e[c]=d}this._setOptions(e);return this},_setOptions:function(b){var c=this;a.each(b,function(a,b){c._setOption(a,b)});return this},_setOption:function(a,b){this.options[a]=b;if(a==="disabled"){this.widget()[b?"addClass":"removeClass"](this.widgetBaseClass+"-disabled"+" "+"ui-state-disabled").attr("aria-disabled",b)}return this},enable:function(){return this._setOption("disabled",false)},disable:function(){return this._setOption("disabled",true)},_trigger:function(b,c,d){var e=this.options[b];c=a.Event(c);c.type=(b===this.widgetEventPrefix?b:this.widgetEventPrefix+b).toLowerCase();d=d||{};if(c.originalEvent){for(var f=a.event.props.length,g;f;){g=a.event.props[--f];c[g]=c.originalEvent[g]}}this.element.trigger(c,d);return!(a.isFunction(e)&&e.call(this.element[0],c,d)===false||c.isDefaultPrevented())}}})(jQuery)});define(["jquery","./jquery.ui.widget"],function(a){(function(a,b){a.widget("mobile.widget",{_createWidget:function(){a.Widget.prototype._createWidget.apply(this,arguments);this._trigger("init")},_getCreateOptions:function(){var c=this.element,d={};a.each(this.options,function(a){var e=c.jqmData(a.replace(/[A-Z]/g,function(a){return"-"+a.toLowerCase()}));if(e!==b){d[a]=e}});return d},enhanceWithin:function(b){var c=a.mobile.closestPageData(a(b)),d=c&&c.keepNativeSelector()||"";a(this.options.initSelector,b).not(d)[this.widgetName]()}})})(jQuery)});define(["jquery","./jquery.mobile.core"],function(a){(function(a,b){var c=a(window),d=a("html");a.mobile.media=function(){var b={},c=a("<div id='jquery-mediatest'>"),e=a("<body>").append(c);return function(a){if(!(a in b)){var f=document.createElement("style"),g="@media "+a+" { #jquery-mediatest { position:absolute; } }";f.type="text/css";if(f.styleSheet){f.styleSheet.cssText=g}else{f.appendChild(document.createTextNode(g))}d.prepend(e).prepend(f);b[a]=c.css("position")==="absolute";e.add(f).remove()}return b[a]}}()})(jQuery)});define(["jquery","./jquery.mobile.media"],function(a){(function(a,b){function m(){var b=location.protocol+"//"+location.host+location.pathname+"ui-dir/",d=a("head base"),e=null,f="",g,h;if(!d.length){d=e=a("<base>",{href:b}).appendTo("head")}else{f=d.attr("href")}g=a("<a href='testurl' />").prependTo(c);h=g[0].href;d[0].href=f||location.pathname;if(e){e.remove()}return h.indexOf(b)===0}function l(){var b="transform-3d";return k("perspective","10px","moz")||a.mobile.media("(-"+e.join("-"+b+"),(-")+"-"+b+"),("+b+")")}function k(a,b,c){var d=document.createElement("div"),f=function(a){return a.charAt(0).toUpperCase()+a.substr(1)},g=function(a){return"-"+a.charAt(0).toLowerCase()+a.substr(1)+"-"},h=function(c){var e=g(c)+a+": "+b+";",h=f(c),i=h+f(a);d.setAttribute("style",e);if(!!d.style[i]){k=true}},j=c?[c]:e,k;for(i=0;i<j.length;i++){h(j[i])}return!!k}function j(a){var c=a.charAt(0).toUpperCase()+a.substr(1),f=(a+" "+e.join(c+" ")+c).split(" ");for(var g in f){if(d[f[g]]!==b){return true}}}var c=a("<body>").prependTo("html"),d=c[0].style,e=["Webkit","Moz","O"],f="palmGetResource"in window,g=window.operamini&&{}.toString.call(window.operamini)==="[object OperaMini]",h=window.blackberry;a.extend(a.mobile,{browser:{}});a.mobile.browser.ie=function(){var a=3,b=document.createElement("div"),c=b.all||[];while(b.innerHTML="<!--[if gt IE "+ ++a+"]><br><![endif]-->",c[0]){}return a>4?a:!a}();a.extend(a.support,{orientation:"orientation"in window&&"onorientationchange"in window,touch:"ontouchend"in document,cssTransitions:"WebKitTransitionEvent"in window||k("transition","height 100ms linear"),pushState:"pushState"in history&&"replaceState"in history,mediaquery:a.mobile.media("only all"),cssPseudoElement:!!j("content"),touchOverflow:!!j("overflowScrolling"),cssTransform3d:l(),boxShadow:!!j("boxShadow")&&!h,scrollTop:("pageXOffset"in window||"scrollTop"in document.documentElement||"scrollTop"in c[0])&&!f&&!g,dynamicBaseTag:m()});c.remove();var n=function(){var a=window.navigator.userAgent;return a.indexOf("Nokia")>-1&&(a.indexOf("Symbian/3")>-1||a.indexOf("Series60/5")>-1)&&a.indexOf("AppleWebKit")>-1&&a.match(/(BrowserNG|NokiaBrowser)\/7\.[0-3]/)}();a.mobile.ajaxBlacklist=window.blackberry&&!window.WebKitPoint||g||n;if(n){a(function(){a("head link[rel='stylesheet']").attr("rel","alternate stylesheet").attr("rel","stylesheet")})}if(!a.support.boxShadow){a("html").addClass("ui-mobile-nosupport-boxshadow")}})(jQuery)});define(["jquery"],function(a){(function(a,b,c,d){function O(b){var c=b.substr(1);return{setup:function(d,f){if(!M(this)){a.data(this,e,{})}var g=a.data(this,e);g[b]=true;k[b]=(k[b]||0)+1;if(k[b]===1){t.bind(c,H)}a(this).bind(c,N);if(s){k["touchstart"]=(k["touchstart"]||0)+1;if(k["touchstart"]===1){t.bind("touchstart",I).bind("touchend",L).bind("touchmove",K).bind("scroll",J)}}},teardown:function(d,f){--k[b];if(!k[b]){t.unbind(c,H)}if(s){--k["touchstart"];if(!k["touchstart"]){t.unbind("touchstart",I).unbind("touchmove",K).unbind("touchend",L).unbind("scroll",J)}}var g=a(this),h=a.data(this,e);if(h){h[b]=false}g.unbind(c,N);if(!M(this)){g.removeData(e)}}}}function N(){}function M(b){var c=a.data(b,e),d;if(c){for(d in c){if(c[d]){return true}}}return false}function L(a){if(r){return}B();var b=y(a.target),c;G("vmouseup",a,b);if(!o){var d=G("vclick",a,b);if(d&&d.isDefaultPrevented()){c=w(a).changedTouches[0];p.push({touchID:v,x:c.clientX,y:c.clientY});q=true}}G("vmouseout",a,b);o=false;E()}function K(b){if(r){return}var c=w(b).touches[0],d=o,e=a.vmouse.moveDistanceThreshold;o=o||Math.abs(c.pageX-m)>e||Math.abs(c.pageY-n)>e,flags=y(b.target);if(o&&!d){G("vmousecancel",b,flags)}G("vmousemove",b,flags);E()}function J(a){if(r){return}if(!o){G("vmousecancel",a,y(a.target))}o=true;E()}function I(b){var c=w(b).touches,d,e;if(c&&c.length===1){d=b.target;e=y(d);if(e.hasVirtualBinding){v=u++;a.data(d,f,v);F();D();o=false;var g=w(b).touches[0];m=g.pageX;n=g.pageY;G("vmouseover",b,e);G("vmousedown",b,e)}}}function H(b){var c=a.data(b.target,f);if(!q&&(!v||v!==c)){var d=G("v"+b.type,b);if(d){if(d.isDefaultPrevented()){b.preventDefault()}if(d.isPropagationStopped()){b.stopPropagation()}if(d.isImmediatePropagationStopped()){b.stopImmediatePropagation()}}}}function G(b,c,d){var e;if(d&&d[b]||!d&&z(c.target,b)){e=x(c,b);a(c.target).trigger(e)}return e}function F(){if(l){clearTimeout(l);l=0}}function E(){F();l=setTimeout(function(){l=0;C()},a.vmouse.resetTimerDuration)}function D(){A()}function C(){v=0;p.length=0;q=false;B()}function B(){r=true}function A(){r=false}function z(b,c){var d;while(b){d=a.data(b,e);if(d&&(!c||d[c])){return b}b=b.parentNode}return null}function y(b){var c={},d,f;while(b){d=a.data(b,e);for(f in d){if(d[f]){c[f]=c.hasVirtualBinding=true}}b=b.parentNode}return c}function x(b,c){var e=b.type,f,g,i,k,l,m,n,o;b=a.Event(b);b.type=c;f=b.originalEvent;g=a.event.props;if(e.search(/mouse/)>-1){g=j}if(f){for(n=g.length,k;n;){k=g[--n];b[k]=f[k]}}if(e.search(/mouse(down|up)|click/)>-1&&!b.which){b.which=1}if(e.search(/^touch/)!==-1){i=w(f);e=i.touches;l=i.changedTouches;m=e&&e.length?e[0]:l&&l.length?l[0]:d;if(m){for(o=0,len=h.length;o<len;o++){k=h[o];b[k]=m[k]}}}return b}function w(a){while(a&&typeof a.originalEvent!=="undefined"){a=a.originalEvent}return a}var e="virtualMouseBindings",f="virtualTouchID",g="vmouseover vmousedown vmousemove vmouseup vclick vmouseout vmousecancel".split(" "),h="clientX clientY pageX pageY screenX screenY".split(" "),i=a.event.mouseHooks?a.event.mouseHooks.props:[],j=a.event.props.concat(i),k={},l=0,m=0,n=0,o=false,p=[],q=false,r=false,s="addEventListener"in c,t=a(c),u=1,v=0;a.vmouse={moveDistanceThreshold:10,clickDistanceThreshold:10,resetTimerDuration:1500};for(var P=0;P<g.length;P++){a.event.special[g[P]]=O(g[P])}if(s){c.addEventListener("click",function(b){var c=p.length,d=b.target,e,g,h,i,j,k;if(c){e=b.clientX;g=b.clientY;threshold=a.vmouse.clickDistanceThreshold;h=d;while(h){for(i=0;i<c;i++){j=p[i];k=0;if(h===d&&Math.abs(j.x-e)<threshold&&Math.abs(j.y-g)<threshold||a.data(h,f)===j.touchID){b.preventDefault();b.stopPropagation();return}}h=h.parentNode}}},true)}})(jQuery,window,document)});define(["jquery","./jquery.mobile.core","./jquery.mobile.media","./jquery.mobile.support","./jquery.mobile.vmouse"],function(a){(function(a,b,c){function i(b,c,d){var e=d.type;d.type=c;a.event.handle.call(b,d);d.type=e}a.each(("touchstart touchmove touchend orientationchange throttledresize "+"tap taphold swipe swipeleft swiperight scrollstart scrollstop").split(" "),function(b,c){a.fn[c]=function(a){return a?this.bind(c,a):this.trigger(c)};a.attrFn[c]=true});var d=a.support.touch,e="touchmove scroll",f=d?"touchstart":"mousedown",g=d?"touchend":"mouseup",h=d?"touchmove":"mousemove";a.event.special.scrollstart={enabled:true,setup:function(){function g(a,c){d=c;i(b,d?"scrollstart":"scrollstop",a)}var b=this,c=a(b),d,f;c.bind(e,function(b){if(!a.event.special.scrollstart.enabled){return}if(!d){g(b,true)}clearTimeout(f);f=setTimeout(function(){g(b,false)},50)})}};a.event.special.tap={setup:function(){var b=this,c=a(b);c.bind("vmousedown",function(d){function k(a){j();if(e==a.target){i(b,"tap",a)}}function j(){h();c.unbind("vclick",k).unbind("vmouseup",h);a(document).unbind("vmousecancel",j)}function h(){clearTimeout(g)}if(d.which&&d.which!==1){return false}var e=d.target,f=d.originalEvent,g;c.bind("vmouseup",h).bind("vclick",k);a(document).bind("vmousecancel",j);g=setTimeout(function(){i(b,"taphold",a.Event("taphold"))},750)})}};a.event.special.swipe={scrollSupressionThreshold:10,durationThreshold:1e3,horizontalDistanceThreshold:30,verticalDistanceThreshold:75,setup:function(){var b=this,d=a(b);d.bind(f,function(b){function j(b){if(!f){return}var c=b.originalEvent.touches?b.originalEvent.touches[0]:b;i={time:(new Date).getTime(),coords:[c.pageX,c.pageY]};if(Math.abs(f.coords[0]-i.coords[0])>a.event.special.swipe.scrollSupressionThreshold){b.preventDefault()}}var e=b.originalEvent.touches?b.originalEvent.touches[0]:b,f={time:(new Date).getTime(),coords:[e.pageX,e.pageY],origin:a(b.target)},i;d.bind(h,j).one(g,function(b){d.unbind(h,j);if(f&&i){if(i.time-f.time<a.event.special.swipe.durationThreshold&&Math.abs(f.coords[0]-i.coords[0])>a.event.special.swipe.horizontalDistanceThreshold&&Math.abs(f.coords[1]-i.coords[1])<a.event.special.swipe.verticalDistanceThreshold){f.origin.trigger("swipe").trigger(f.coords[0]>i.coords[0]?"swipeleft":"swiperight")}}f=i=c})})}};(function(a,b){function j(){var a=e();if(a!==f){f=a;c.trigger("orientationchange")}}var c=a(b),d,e,f,g,h,i={0:true,180:true};if(a.support.orientation){g=a.mobile.media("all and (orientation: landscape)");h=i[b.orientation];if(g&&h||!g&&!h){i={"-90":true,90:true}}}a.event.special.orientationchange=d={setup:function(){if(a.support.orientation&&a.mobile.orientationChangeEnabled){return false}f=e();c.bind("throttledresize",j)},teardown:function(){if(a.support.orientation&&a.mobile.orientationChangeEnabled){return false}c.unbind("throttledresize",j)},add:function(a){var b=a.handler;a.handler=function(a){a.orientation=e();return b.apply(this,arguments)}}};a.event.special.orientationchange.orientation=e=function(){var c=true,d=document.documentElement;if(a.support.orientation){c=i[b.orientation]}else{c=d&&d.clientWidth/d.clientHeight<1.1}return c?"portrait":"landscape"}})(jQuery,b);(function(){a.event.special.throttledresize={setup:function(){a(this).bind("resize",c)},teardown:function(){a(this).unbind("resize",c)}};var b=250,c=function(){f=(new Date).getTime();g=f-d;if(g>=b){d=f;a(this).trigger("throttledresize")}else{if(e){clearTimeout(e)}e=setTimeout(c,b-g)}},d=0,e,f,g})();a.each({scrollstop:"scrollstart",taphold:"tap",swipeleft:"swipe",swiperight:"swipe"},function(b,c){a.event.special[b]={setup:function(){a(this).bind(c,a.noop)}}})})(jQuery,this)});define(["jquery","../external/requirejs/text!../version.txt","./jquery.mobile.widget"],function(a,b){(function(a,c,d){var e={};a.mobile=a.extend({},{version:b,ns:"",subPageUrlKey:"ui-page",activePageClass:"ui-page-active",activeBtnClass:"ui-btn-active",focusClass:"ui-focus",ajaxEnabled:true,hashListeningEnabled:true,linkBindingEnabled:true,defaultPageTransition:"fade",maxTransitionWidth:false,minScrollBack:10,touchOverflowEnabled:false,defaultDialogTransition:"pop",loadingMessage:"loading",pageLoadErrorMessage:"Error Loading Page",loadingMessageTextVisible:false,loadingMessageTheme:"a",pageLoadErrorMessageTheme:"e",autoInitializePage:true,pushStateEnabled:true,orientationChangeEnabled:true,gradeA:function(){return a.support.mediaquery||a.mobile.browser.ie&&a.mobile.browser.ie>=7},keyCode:{ALT:18,BACKSPACE:8,CAPS_LOCK:20,COMMA:188,COMMAND:91,COMMAND_LEFT:91,COMMAND_RIGHT:93,CONTROL:17,DELETE:46,DOWN:40,END:35,ENTER:13,ESCAPE:27,HOME:36,INSERT:45,LEFT:37,MENU:93,NUMPAD_ADD:107,NUMPAD_DECIMAL:110,NUMPAD_DIVIDE:111,NUMPAD_ENTER:108,NUMPAD_MULTIPLY:106,NUMPAD_SUBTRACT:109,PAGE_DOWN:34,PAGE_UP:33,PERIOD:190,RIGHT:39,SHIFT:16,SPACE:32,TAB:9,UP:38,WINDOWS:91},silentScroll:function(b){if(a.type(b)!=="number"){b=a.mobile.defaultHomeScroll}a.event.special.scrollstart.enabled=false;setTimeout(function(){c.scrollTo(0,b);a(document).trigger("silentscroll",{x:0,y:b})},20);setTimeout(function(){a.event.special.scrollstart.enabled=true},150)},nsNormalizeDict:e,nsNormalize:function(b){if(!b){return}return e[b]||(e[b]=a.camelCase(a.mobile.ns+b))},getInheritedTheme:function(a,b){var c=a[0],d="",e=/ui-(bar|body)-([a-z])\b/,f,g;while(c){var f=c.className||"";if((g=e.exec(f))&&(d=g[2])){break}c=c.parentNode}return d||b||"a"},closestPageData:function(a){return a.closest(':jqmData(role="page"), :jqmData(role="dialog")').data("page")}},a.mobile);a.fn.jqmData=function(b,c){var d;if(typeof b!="undefined"){d=this.data(b?a.mobile.nsNormalize(b):b,c)}return d};a.jqmData=function(b,c,d){var e;if(typeof c!="undefined"){e=a.data(b,c?a.mobile.nsNormalize(c):c,d)}return e};a.fn.jqmRemoveData=function(b){return this.removeData(a.mobile.nsNormalize(b))};a.jqmRemoveData=function(b,c){return a.removeData(b,a.mobile.nsNormalize(c))};a.fn.removeWithDependents=function(){a.removeWithDependents(this)};a.removeWithDependents=function(b){var c=a(b);(c.jqmData("dependents")||a()).remove();c.remove()};a.fn.addDependents=function(b){a.addDependents(a(this),b)};a.addDependents=function(b,c){var d=a(b).jqmData("dependents")||a();a(b).jqmData("dependents",a.merge(d,c))};a.fn.getEncodedText=function(){return a("<div/>").text(a(this).text()).html()};var f=a.find,g=/:jqmData\(([^)]*)\)/g;a.find=function(b,c,d,e){b=b.replace(g,"[data-"+(a.mobile.ns||"")+"$1]");return f.call(this,b,c,d,e)};a.extend(a.find,f);a.find.matches=function(b,c){return a.find(b,null,null,c)};a.find.matchesSelector=function(b,c){return a.find(c,null,null,[b]).length>0}})(jQuery,this)})


// fin jq mobile customize .min .js 

            $(document).ready(function () {
                jQuery('#camera_wrap').camera({
                    loader: false,
                    pagination: false,
                    minHeight: '200',
                    thumbnails: false,
                    height: '39,0625%',
                    caption: true,
                    navigation: true,
                    fx: 'mosaic'
                });
                var owl = $("#owl");
                owl.owlCarousel({
                    items: 7, //10 items above 1000px browser width
                    itemsDesktop: [995, 5], //5 items between 1000px and 901px
                    itemsDesktopSmall: [767, 3], // betweem 900px and 601px
                    itemsTablet: [700, 3], //2 items between 600 and 0
                    itemsMobile: [479, 2], // itemsMobile disabled - inherit from itemsTablet option
                    navigation: true,
                });
                $().UItoTop({easingType: 'easeOutQuart'});
                $('#stuck_container').tmStickUp({});
                $('.gallery a.gal_item').touchTouch();
            });



