<?php 

return array(
	'name' => 'Pets Clinic',
	'color' => 'blue',
	'category' => 'Pets',
	'theme'  => 'Pets Care',
	'libraries' => array('Classie', 'Animated Header', 'Bootstrap Validation', 'Jquery Easing'),
);