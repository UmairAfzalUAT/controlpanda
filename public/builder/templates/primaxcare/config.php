<?php 

return array(
	'name' => 'Primax',
	'color' => 'Blue',
	'category' => 'Medical 2',
	'theme'  => 'Health Services',
	'libraries' => array('Classie', 'Animated Header', 'Bootstrap Validation', 'Jquery Easing'),
);