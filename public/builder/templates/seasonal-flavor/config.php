<?php 

return array(
	'name' => 'Restaurantes  ',
	'color' => 'Golden',
	'category' => 'Seasonal Flavours ',
	'theme'  => 'Food Point',
	'libraries' => array('Classie', 'Animated Header', 'Bootstrap Validation', 'Jquery Easing'),
);