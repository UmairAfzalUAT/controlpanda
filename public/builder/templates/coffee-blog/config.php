<?php 

return array(
	'name' => 'FliCode',
	'color' => 'Gray',
	'category' => 'Landing Page',
	'theme'  => 'Yeti',
	'libraries' => array('Classie', 'Animated Header', 'Bootstrap Validation', 'Jquery Easing'),
);