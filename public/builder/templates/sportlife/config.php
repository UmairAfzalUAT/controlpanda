<?php 

return array(
	'name' => 'Sport Life',
	'color' => 'Orange',
	'category' => 'Gym - Fitness',
	'theme'  => 'Gym/Fitness',
	'libraries' => array('Classie', 'Animated Header', 'Bootstrap Validation', 'Jquery Easing'),
);