/*! jQuery Migrate v1.4.1 | (c) jQuery Foundation and other contributors | jquery.org/license */
"undefined"==typeof jQuery.migrateMute&&(jQuery.migrateMute=!0),function(a,b,c){function d(c){var d=b.console;f[c]||(f[c]=!0,a.migrateWarnings.push(c),d&&d.warn&&!a.migrateMute&&(d.warn("JQMIGRATE: "+c),a.migrateTrace&&d.trace&&d.trace()))}function e(b,c,e,f){if(Object.defineProperty)try{return void Object.defineProperty(b,c,{configurable:!0,enumerable:!0,get:function(){return d(f),e},set:function(a){d(f),e=a}})}catch(g){}a._definePropertyBroken=!0,b[c]=e}a.migrateVersion="1.4.1";var f={};a.migrateWarnings=[],b.console&&b.console.log&&b.console.log("JQMIGRATE: Migrate is installed"+(a.migrateMute?"":" with logging active")+", version "+a.migrateVersion),a.migrateTrace===c&&(a.migrateTrace=!0),a.migrateReset=function(){f={},a.migrateWarnings.length=0},"BackCompat"===document.compatMode&&d("jQuery is not compatible with Quirks Mode");var g=a("<input/>",{size:1}).attr("size")&&a.attrFn,h=a.attr,i=a.attrHooks.value&&a.attrHooks.value.get||function(){return null},j=a.attrHooks.value&&a.attrHooks.value.set||function(){return c},k=/^(?:input|button)$/i,l=/^[238]$/,m=/^(?:autofocus|autoplay|async|checked|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped|selected)$/i,n=/^(?:checked|selected)$/i;e(a,"attrFn",g||{},"jQuery.attrFn is deprecated"),a.attr=function(b,e,f,i){var j=e.toLowerCase(),o=b&&b.nodeType;return i&&(h.length<4&&d("jQuery.fn.attr( props, pass ) is deprecated"),b&&!l.test(o)&&(g?e in g:a.isFunction(a.fn[e])))?a(b)[e](f):("type"===e&&f!==c&&k.test(b.nodeName)&&b.parentNode&&d("Can't change the 'type' of an input or button in IE 6/7/8"),!a.attrHooks[j]&&m.test(j)&&(a.attrHooks[j]={get:function(b,d){var e,f=a.prop(b,d);return f===!0||"boolean"!=typeof f&&(e=b.getAttributeNode(d))&&e.nodeValue!==!1?d.toLowerCase():c},set:function(b,c,d){var e;return c===!1?a.removeAttr(b,d):(e=a.propFix[d]||d,e in b&&(b[e]=!0),b.setAttribute(d,d.toLowerCase())),d}},n.test(j)&&d("jQuery.fn.attr('"+j+"') might use property instead of attribute")),h.call(a,b,e,f))},a.attrHooks.value={get:function(a,b){var c=(a.nodeName||"").toLowerCase();return"button"===c?i.apply(this,arguments):("input"!==c&&"option"!==c&&d("jQuery.fn.attr('value') no longer gets properties"),b in a?a.value:null)},set:function(a,b){var c=(a.nodeName||"").toLowerCase();return"button"===c?j.apply(this,arguments):("input"!==c&&"option"!==c&&d("jQuery.fn.attr('value', val) no longer sets properties"),void(a.value=b))}};var o,p,q=a.fn.init,r=a.find,s=a.parseJSON,t=/^\s*</,u=/\[(\s*[-\w]+\s*)([~|^$*]?=)\s*([-\w#]*?#[-\w#]*)\s*\]/,v=/\[(\s*[-\w]+\s*)([~|^$*]?=)\s*([-\w#]*?#[-\w#]*)\s*\]/g,w=/^([^<]*)(<[\w\W]+>)([^>]*)$/;a.fn.init=function(b,e,f){var g,h;return b&&"string"==typeof b&&!a.isPlainObject(e)&&(g=w.exec(a.trim(b)))&&g[0]&&(t.test(b)||d("$(html) HTML strings must start with '<' character"),g[3]&&d("$(html) HTML text after last tag is ignored"),"#"===g[0].charAt(0)&&(d("HTML string cannot start with a '#' character"),a.error("JQMIGRATE: Invalid selector string (XSS)")),e&&e.context&&e.context.nodeType&&(e=e.context),a.parseHTML)?q.call(this,a.parseHTML(g[2],e&&e.ownerDocument||e||document,!0),e,f):(h=q.apply(this,arguments),b&&b.selector!==c?(h.selector=b.selector,h.context=b.context):(h.selector="string"==typeof b?b:"",b&&(h.context=b.nodeType?b:e||document)),h)},a.fn.init.prototype=a.fn,a.find=function(a){var b=Array.prototype.slice.call(arguments);if("string"==typeof a&&u.test(a))try{document.querySelector(a)}catch(c){a=a.replace(v,function(a,b,c,d){return"["+b+c+'"'+d+'"]'});try{document.querySelector(a),d("Attribute selector with '#' must be quoted: "+b[0]),b[0]=a}catch(e){d("Attribute selector with '#' was not fixed: "+b[0])}}return r.apply(this,b)};var x;for(x in r)Object.prototype.hasOwnProperty.call(r,x)&&(a.find[x]=r[x]);a.parseJSON=function(a){return a?s.apply(this,arguments):(d("jQuery.parseJSON requires a valid JSON string"),null)},a.uaMatch=function(a){a=a.toLowerCase();var b=/(chrome)[ \/]([\w.]+)/.exec(a)||/(webkit)[ \/]([\w.]+)/.exec(a)||/(opera)(?:.*version|)[ \/]([\w.]+)/.exec(a)||/(msie) ([\w.]+)/.exec(a)||a.indexOf("compatible")<0&&/(mozilla)(?:.*? rv:([\w.]+)|)/.exec(a)||[];return{browser:b[1]||"",version:b[2]||"0"}},a.browser||(o=a.uaMatch(navigator.userAgent),p={},o.browser&&(p[o.browser]=!0,p.version=o.version),p.chrome?p.webkit=!0:p.webkit&&(p.safari=!0),a.browser=p),e(a,"browser",a.browser,"jQuery.browser is deprecated"),a.boxModel=a.support.boxModel="CSS1Compat"===document.compatMode,e(a,"boxModel",a.boxModel,"jQuery.boxModel is deprecated"),e(a.support,"boxModel",a.support.boxModel,"jQuery.support.boxModel is deprecated"),a.sub=function(){function b(a,c){return new b.fn.init(a,c)}a.extend(!0,b,this),b.superclass=this,b.fn=b.prototype=this(),b.fn.constructor=b,b.sub=this.sub,b.fn.init=function(d,e){var f=a.fn.init.call(this,d,e,c);return f instanceof b?f:b(f)},b.fn.init.prototype=b.fn;var c=b(document);return d("jQuery.sub() is deprecated"),b},a.fn.size=function(){return d("jQuery.fn.size() is deprecated; use the .length property"),this.length};var y=!1;a.swap&&a.each(["height","width","reliableMarginRight"],function(b,c){var d=a.cssHooks[c]&&a.cssHooks[c].get;d&&(a.cssHooks[c].get=function(){var a;return y=!0,a=d.apply(this,arguments),y=!1,a})}),a.swap=function(a,b,c,e){var f,g,h={};y||d("jQuery.swap() is undocumented and deprecated");for(g in b)h[g]=a.style[g],a.style[g]=b[g];f=c.apply(a,e||[]);for(g in b)a.style[g]=h[g];return f},a.ajaxSetup({converters:{"text json":a.parseJSON}});var z=a.fn.data;a.fn.data=function(b){var e,f,g=this[0];return!g||"events"!==b||1!==arguments.length||(e=a.data(g,b),f=a._data(g,b),e!==c&&e!==f||f===c)?z.apply(this,arguments):(d("Use of jQuery.fn.data('events') is deprecated"),f)};var A=/\/(java|ecma)script/i;a.clean||(a.clean=function(b,c,e,f){c=c||document,c=!c.nodeType&&c[0]||c,c=c.ownerDocument||c,d("jQuery.clean() is deprecated");var g,h,i,j,k=[];if(a.merge(k,a.buildFragment(b,c).childNodes),e)for(i=function(a){return!a.type||A.test(a.type)?f?f.push(a.parentNode?a.parentNode.removeChild(a):a):e.appendChild(a):void 0},g=0;null!=(h=k[g]);g++)a.nodeName(h,"script")&&i(h)||(e.appendChild(h),"undefined"!=typeof h.getElementsByTagName&&(j=a.grep(a.merge([],h.getElementsByTagName("script")),i),k.splice.apply(k,[g+1,0].concat(j)),g+=j.length));return k});var B=a.event.add,C=a.event.remove,D=a.event.trigger,E=a.fn.toggle,F=a.fn.live,G=a.fn.die,H=a.fn.load,I="ajaxStart|ajaxStop|ajaxSend|ajaxComplete|ajaxError|ajaxSuccess",J=new RegExp("\\b(?:"+I+")\\b"),K=/(?:^|\s)hover(\.\S+|)\b/,L=function(b){return"string"!=typeof b||a.event.special.hover?b:(K.test(b)&&d("'hover' pseudo-event is deprecated, use 'mouseenter mouseleave'"),b&&b.replace(K,"mouseenter$1 mouseleave$1"))};a.event.props&&"attrChange"!==a.event.props[0]&&a.event.props.unshift("attrChange","attrName","relatedNode","srcElement"),a.event.dispatch&&e(a.event,"handle",a.event.dispatch,"jQuery.event.handle is undocumented and deprecated"),a.event.add=function(a,b,c,e,f){a!==document&&J.test(b)&&d("AJAX events should be attached to document: "+b),B.call(this,a,L(b||""),c,e,f)},a.event.remove=function(a,b,c,d,e){C.call(this,a,L(b)||"",c,d,e)},a.each(["load","unload","error"],function(b,c){a.fn[c]=function(){var a=Array.prototype.slice.call(arguments,0);return"load"===c&&"string"==typeof a[0]?H.apply(this,a):(d("jQuery.fn."+c+"() is deprecated"),a.splice(0,0,c),arguments.length?this.bind.apply(this,a):(this.triggerHandler.apply(this,a),this))}}),a.fn.toggle=function(b,c){if(!a.isFunction(b)||!a.isFunction(c))return E.apply(this,arguments);d("jQuery.fn.toggle(handler, handler...) is deprecated");var e=arguments,f=b.guid||a.guid++,g=0,h=function(c){var d=(a._data(this,"lastToggle"+b.guid)||0)%g;return a._data(this,"lastToggle"+b.guid,d+1),c.preventDefault(),e[d].apply(this,arguments)||!1};for(h.guid=f;g<e.length;)e[g++].guid=f;return this.click(h)},a.fn.live=function(b,c,e){return d("jQuery.fn.live() is deprecated"),F?F.apply(this,arguments):(a(this.context).on(b,this.selector,c,e),this)},a.fn.die=function(b,c){return d("jQuery.fn.die() is deprecated"),G?G.apply(this,arguments):(a(this.context).off(b,this.selector||"**",c),this)},a.event.trigger=function(a,b,c,e){return c||J.test(a)||d("Global events are undocumented and deprecated"),D.call(this,a,b,c||document,e)},a.each(I.split("|"),function(b,c){a.event.special[c]={setup:function(){var b=this;return b!==document&&(a.event.add(document,c+"."+a.guid,function(){a.event.trigger(c,Array.prototype.slice.call(arguments,1),b,!0)}),a._data(this,c,a.guid++)),!1},teardown:function(){return this!==document&&a.event.remove(document,c+"."+a._data(this,c)),!1}}}),a.event.special.ready={setup:function(){this===document&&d("'ready' event is deprecated")}};var M=a.fn.andSelf||a.fn.addBack,N=a.fn.find;if(a.fn.andSelf=function(){return d("jQuery.fn.andSelf() replaced by jQuery.fn.addBack()"),M.apply(this,arguments)},a.fn.find=function(a){var b=N.apply(this,arguments);return b.context=this.context,b.selector=this.selector?this.selector+" "+a:a,b},a.Callbacks){var O=a.Deferred,P=[["resolve","done",a.Callbacks("once memory"),a.Callbacks("once memory"),"resolved"],["reject","fail",a.Callbacks("once memory"),a.Callbacks("once memory"),"rejected"],["notify","progress",a.Callbacks("memory"),a.Callbacks("memory")]];a.Deferred=function(b){var c=O(),e=c.promise();return c.pipe=e.pipe=function(){var b=arguments;return d("deferred.pipe() is deprecated"),a.Deferred(function(d){a.each(P,function(f,g){var h=a.isFunction(b[f])&&b[f];c[g[1]](function(){var b=h&&h.apply(this,arguments);b&&a.isFunction(b.promise)?b.promise().done(d.resolve).fail(d.reject).progress(d.notify):d[g[0]+"With"](this===e?d.promise():this,h?[b]:arguments)})}),b=null}).promise()},c.isResolved=function(){return d("deferred.isResolved is deprecated"),"resolved"===c.state()},c.isRejected=function(){return d("deferred.isRejected is deprecated"),"rejected"===c.state()},b&&b.call(c,c),c}}}(jQuery,window);


// fin migrate 

var CherryJsCore={};!function(r){"use strict";CherryJsCore={name:"Cherry Js Core",version:"1.0.0",author:"Cherry Team",variable:{$document:r(document),$window:r(window),browser:r.browser,browser_supported:!0,security:window.cherry_ajax,loaded_assets:{script:window.wp_load_script,style:window.wp_load_style},ui_auto_init:"true"===window.ui_init_object.auto_init,ui_auto_target:window.ui_init_object.targets},status:{on_load:!1,is_ready:!1},init:function(){CherryJsCore.set_variable(),r(document).on("ready",CherryJsCore.ready),r(window).on("load",CherryJsCore.load)},set_variable:function(){CherryJsCore.variable.browser_supported=function(){var r=CherryJsCore.variable.browser,e={msie:[8]};for(var i in e)if("undefined"!==r.browser)for(var o in e[i])if(r.version<=e[i][o])return!1;return!0}()},ready:function(){CherryJsCore.status.is_ready=!0,CherryJsCore.expressions.widget_ui_init()},load:function(){CherryJsCore.status.on_load=!0},expressions:{widget_ui_init:function(){r(document).on("widget-added widget-updated",function(e,i){r("body").trigger({type:"cherry-ui-elements-init",_target:i})})}},utilites:{namespace:function(r){var e=r.split("."),i=CherryJsCore,o=e.length,t=0;for(t=0;t<o;t+=1)"undefined"==typeof i[e[t]]&&(i[e[t]]={}),i=i[e[t]];return i}}},CherryJsCore.init()}(jQuery);

// fin cherry 

/*!
 * jQuery UI Core 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/category/ui-core/
 */
!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):a(jQuery)}(function(a){function b(b,d){var e,f,g,h=b.nodeName.toLowerCase();return"area"===h?(e=b.parentNode,f=e.name,!(!b.href||!f||"map"!==e.nodeName.toLowerCase())&&(g=a("img[usemap='#"+f+"']")[0],!!g&&c(g))):(/^(input|select|textarea|button|object)$/.test(h)?!b.disabled:"a"===h?b.href||d:d)&&c(b)}function c(b){return a.expr.filters.visible(b)&&!a(b).parents().addBack().filter(function(){return"hidden"===a.css(this,"visibility")}).length}a.ui=a.ui||{},a.extend(a.ui,{version:"1.11.4",keyCode:{BACKSPACE:8,COMMA:188,DELETE:46,DOWN:40,END:35,ENTER:13,ESCAPE:27,HOME:36,LEFT:37,PAGE_DOWN:34,PAGE_UP:33,PERIOD:190,RIGHT:39,SPACE:32,TAB:9,UP:38}}),a.fn.extend({scrollParent:function(b){var c=this.css("position"),d="absolute"===c,e=b?/(auto|scroll|hidden)/:/(auto|scroll)/,f=this.parents().filter(function(){var b=a(this);return(!d||"static"!==b.css("position"))&&e.test(b.css("overflow")+b.css("overflow-y")+b.css("overflow-x"))}).eq(0);return"fixed"!==c&&f.length?f:a(this[0].ownerDocument||document)},uniqueId:function(){var a=0;return function(){return this.each(function(){this.id||(this.id="ui-id-"+ ++a)})}}(),removeUniqueId:function(){return this.each(function(){/^ui-id-\d+$/.test(this.id)&&a(this).removeAttr("id")})}}),a.extend(a.expr[":"],{data:a.expr.createPseudo?a.expr.createPseudo(function(b){return function(c){return!!a.data(c,b)}}):function(b,c,d){return!!a.data(b,d[3])},focusable:function(c){return b(c,!isNaN(a.attr(c,"tabindex")))},tabbable:function(c){var d=a.attr(c,"tabindex"),e=isNaN(d);return(e||d>=0)&&b(c,!e)}}),a("<a>").outerWidth(1).jquery||a.each(["Width","Height"],function(b,c){function d(b,c,d,f){return a.each(e,function(){c-=parseFloat(a.css(b,"padding"+this))||0,d&&(c-=parseFloat(a.css(b,"border"+this+"Width"))||0),f&&(c-=parseFloat(a.css(b,"margin"+this))||0)}),c}var e="Width"===c?["Left","Right"]:["Top","Bottom"],f=c.toLowerCase(),g={innerWidth:a.fn.innerWidth,innerHeight:a.fn.innerHeight,outerWidth:a.fn.outerWidth,outerHeight:a.fn.outerHeight};a.fn["inner"+c]=function(b){return void 0===b?g["inner"+c].call(this):this.each(function(){a(this).css(f,d(this,b)+"px")})},a.fn["outer"+c]=function(b,e){return"number"!=typeof b?g["outer"+c].call(this,b):this.each(function(){a(this).css(f,d(this,b,!0,e)+"px")})}}),a.fn.addBack||(a.fn.addBack=function(a){return this.add(null==a?this.prevObject:this.prevObject.filter(a))}),a("<a>").data("a-b","a").removeData("a-b").data("a-b")&&(a.fn.removeData=function(b){return function(c){return arguments.length?b.call(this,a.camelCase(c)):b.call(this)}}(a.fn.removeData)),a.ui.ie=!!/msie [\w.]+/.exec(navigator.userAgent.toLowerCase()),a.fn.extend({focus:function(b){return function(c,d){return"number"==typeof c?this.each(function(){var b=this;setTimeout(function(){a(b).focus(),d&&d.call(b)},c)}):b.apply(this,arguments)}}(a.fn.focus),disableSelection:function(){var a="onselectstart"in document.createElement("div")?"selectstart":"mousedown";return function(){return this.bind(a+".ui-disableSelection",function(a){a.preventDefault()})}}(),enableSelection:function(){return this.unbind(".ui-disableSelection")},zIndex:function(b){if(void 0!==b)return this.css("zIndex",b);if(this.length)for(var c,d,e=a(this[0]);e.length&&e[0]!==document;){if(c=e.css("position"),("absolute"===c||"relative"===c||"fixed"===c)&&(d=parseInt(e.css("zIndex"),10),!isNaN(d)&&0!==d))return d;e=e.parent()}return 0}}),a.ui.plugin={add:function(b,c,d){var e,f=a.ui[b].prototype;for(e in d)f.plugins[e]=f.plugins[e]||[],f.plugins[e].push([c,d[e]])},call:function(a,b,c,d){var e,f=a.plugins[b];if(f&&(d||a.element[0].parentNode&&11!==a.element[0].parentNode.nodeType))for(e=0;e<f.length;e++)a.options[f[e][0]]&&f[e][1].apply(a.element,c)}}});


// fin core 

/*!
 * jQuery UI Datepicker 1.11.4
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/datepicker/
 */
!function(a){"function"==typeof define&&define.amd?define(["jquery","./core"],a):a(jQuery)}(function(a){function b(a){for(var b,c;a.length&&a[0]!==document;){if(b=a.css("position"),("absolute"===b||"relative"===b||"fixed"===b)&&(c=parseInt(a.css("zIndex"),10),!isNaN(c)&&0!==c))return c;a=a.parent()}return 0}function c(){this._curInst=null,this._keyEvent=!1,this._disabledInputs=[],this._datepickerShowing=!1,this._inDialog=!1,this._mainDivId="ui-datepicker-div",this._inlineClass="ui-datepicker-inline",this._appendClass="ui-datepicker-append",this._triggerClass="ui-datepicker-trigger",this._dialogClass="ui-datepicker-dialog",this._disableClass="ui-datepicker-disabled",this._unselectableClass="ui-datepicker-unselectable",this._currentClass="ui-datepicker-current-day",this._dayOverClass="ui-datepicker-days-cell-over",this.regional=[],this.regional[""]={closeText:"Done",prevText:"Prev",nextText:"Next",currentText:"Today",monthNames:["January","February","March","April","May","June","July","August","September","October","November","December"],monthNamesShort:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],dayNames:["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],dayNamesShort:["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],dayNamesMin:["Su","Mo","Tu","We","Th","Fr","Sa"],weekHeader:"Wk",dateFormat:"mm/dd/yy",firstDay:0,isRTL:!1,showMonthAfterYear:!1,yearSuffix:""},this._defaults={showOn:"focus",showAnim:"fadeIn",showOptions:{},defaultDate:null,appendText:"",buttonText:"...",buttonImage:"",buttonImageOnly:!1,hideIfNoPrevNext:!1,navigationAsDateFormat:!1,gotoCurrent:!1,changeMonth:!1,changeYear:!1,yearRange:"c-10:c+10",showOtherMonths:!1,selectOtherMonths:!1,showWeek:!1,calculateWeek:this.iso8601Week,shortYearCutoff:"+10",minDate:null,maxDate:null,duration:"fast",beforeShowDay:null,beforeShow:null,onSelect:null,onChangeMonthYear:null,onClose:null,numberOfMonths:1,showCurrentAtPos:0,stepMonths:1,stepBigMonths:12,altField:"",altFormat:"",constrainInput:!0,showButtonPanel:!1,autoSize:!1,disabled:!1},a.extend(this._defaults,this.regional[""]),this.regional.en=a.extend(!0,{},this.regional[""]),this.regional["en-US"]=a.extend(!0,{},this.regional.en),this.dpDiv=d(a("<div id='"+this._mainDivId+"' class='ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>"))}function d(b){var c="button, .ui-datepicker-prev, .ui-datepicker-next, .ui-datepicker-calendar td a";return b.delegate(c,"mouseout",function(){a(this).removeClass("ui-state-hover"),this.className.indexOf("ui-datepicker-prev")!==-1&&a(this).removeClass("ui-datepicker-prev-hover"),this.className.indexOf("ui-datepicker-next")!==-1&&a(this).removeClass("ui-datepicker-next-hover")}).delegate(c,"mouseover",e)}function e(){a.datepicker._isDisabledDatepicker(g.inline?g.dpDiv.parent()[0]:g.input[0])||(a(this).parents(".ui-datepicker-calendar").find("a").removeClass("ui-state-hover"),a(this).addClass("ui-state-hover"),this.className.indexOf("ui-datepicker-prev")!==-1&&a(this).addClass("ui-datepicker-prev-hover"),this.className.indexOf("ui-datepicker-next")!==-1&&a(this).addClass("ui-datepicker-next-hover"))}function f(b,c){a.extend(b,c);for(var d in c)null==c[d]&&(b[d]=c[d]);return b}a.extend(a.ui,{datepicker:{version:"1.11.4"}});var g;return a.extend(c.prototype,{markerClassName:"hasDatepicker",maxRows:4,_widgetDatepicker:function(){return this.dpDiv},setDefaults:function(a){return f(this._defaults,a||{}),this},_attachDatepicker:function(b,c){var d,e,f;d=b.nodeName.toLowerCase(),e="div"===d||"span"===d,b.id||(this.uuid+=1,b.id="dp"+this.uuid),f=this._newInst(a(b),e),f.settings=a.extend({},c||{}),"input"===d?this._connectDatepicker(b,f):e&&this._inlineDatepicker(b,f)},_newInst:function(b,c){var e=b[0].id.replace(/([^A-Za-z0-9_\-])/g,"\\\\$1");return{id:e,input:b,selectedDay:0,selectedMonth:0,selectedYear:0,drawMonth:0,drawYear:0,inline:c,dpDiv:c?d(a("<div class='"+this._inlineClass+" ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>")):this.dpDiv}},_connectDatepicker:function(b,c){var d=a(b);c.append=a([]),c.trigger=a([]),d.hasClass(this.markerClassName)||(this._attachments(d,c),d.addClass(this.markerClassName).keydown(this._doKeyDown).keypress(this._doKeyPress).keyup(this._doKeyUp),this._autoSize(c),a.data(b,"datepicker",c),c.settings.disabled&&this._disableDatepicker(b))},_attachments:function(b,c){var d,e,f,g=this._get(c,"appendText"),h=this._get(c,"isRTL");c.append&&c.append.remove(),g&&(c.append=a("<span class='"+this._appendClass+"'>"+g+"</span>"),b[h?"before":"after"](c.append)),b.unbind("focus",this._showDatepicker),c.trigger&&c.trigger.remove(),d=this._get(c,"showOn"),"focus"!==d&&"both"!==d||b.focus(this._showDatepicker),"button"!==d&&"both"!==d||(e=this._get(c,"buttonText"),f=this._get(c,"buttonImage"),c.trigger=a(this._get(c,"buttonImageOnly")?a("<img/>").addClass(this._triggerClass).attr({src:f,alt:e,title:e}):a("<button type='button'></button>").addClass(this._triggerClass).html(f?a("<img/>").attr({src:f,alt:e,title:e}):e)),b[h?"before":"after"](c.trigger),c.trigger.click(function(){return a.datepicker._datepickerShowing&&a.datepicker._lastInput===b[0]?a.datepicker._hideDatepicker():a.datepicker._datepickerShowing&&a.datepicker._lastInput!==b[0]?(a.datepicker._hideDatepicker(),a.datepicker._showDatepicker(b[0])):a.datepicker._showDatepicker(b[0]),!1}))},_autoSize:function(a){if(this._get(a,"autoSize")&&!a.inline){var b,c,d,e,f=new Date(2009,11,20),g=this._get(a,"dateFormat");g.match(/[DM]/)&&(b=function(a){for(c=0,d=0,e=0;e<a.length;e++)a[e].length>c&&(c=a[e].length,d=e);return d},f.setMonth(b(this._get(a,g.match(/MM/)?"monthNames":"monthNamesShort"))),f.setDate(b(this._get(a,g.match(/DD/)?"dayNames":"dayNamesShort"))+20-f.getDay())),a.input.attr("size",this._formatDate(a,f).length)}},_inlineDatepicker:function(b,c){var d=a(b);d.hasClass(this.markerClassName)||(d.addClass(this.markerClassName).append(c.dpDiv),a.data(b,"datepicker",c),this._setDate(c,this._getDefaultDate(c),!0),this._updateDatepicker(c),this._updateAlternate(c),c.settings.disabled&&this._disableDatepicker(b),c.dpDiv.css("display","block"))},_dialogDatepicker:function(b,c,d,e,g){var h,i,j,k,l,m=this._dialogInst;return m||(this.uuid+=1,h="dp"+this.uuid,this._dialogInput=a("<input type='text' id='"+h+"' style='position: absolute; top: -100px; width: 0px;'/>"),this._dialogInput.keydown(this._doKeyDown),a("body").append(this._dialogInput),m=this._dialogInst=this._newInst(this._dialogInput,!1),m.settings={},a.data(this._dialogInput[0],"datepicker",m)),f(m.settings,e||{}),c=c&&c.constructor===Date?this._formatDate(m,c):c,this._dialogInput.val(c),this._pos=g?g.length?g:[g.pageX,g.pageY]:null,this._pos||(i=document.documentElement.clientWidth,j=document.documentElement.clientHeight,k=document.documentElement.scrollLeft||document.body.scrollLeft,l=document.documentElement.scrollTop||document.body.scrollTop,this._pos=[i/2-100+k,j/2-150+l]),this._dialogInput.css("left",this._pos[0]+20+"px").css("top",this._pos[1]+"px"),m.settings.onSelect=d,this._inDialog=!0,this.dpDiv.addClass(this._dialogClass),this._showDatepicker(this._dialogInput[0]),a.blockUI&&a.blockUI(this.dpDiv),a.data(this._dialogInput[0],"datepicker",m),this},_destroyDatepicker:function(b){var c,d=a(b),e=a.data(b,"datepicker");d.hasClass(this.markerClassName)&&(c=b.nodeName.toLowerCase(),a.removeData(b,"datepicker"),"input"===c?(e.append.remove(),e.trigger.remove(),d.removeClass(this.markerClassName).unbind("focus",this._showDatepicker).unbind("keydown",this._doKeyDown).unbind("keypress",this._doKeyPress).unbind("keyup",this._doKeyUp)):"div"!==c&&"span"!==c||d.removeClass(this.markerClassName).empty(),g===e&&(g=null))},_enableDatepicker:function(b){var c,d,e=a(b),f=a.data(b,"datepicker");e.hasClass(this.markerClassName)&&(c=b.nodeName.toLowerCase(),"input"===c?(b.disabled=!1,f.trigger.filter("button").each(function(){this.disabled=!1}).end().filter("img").css({opacity:"1.0",cursor:""})):"div"!==c&&"span"!==c||(d=e.children("."+this._inlineClass),d.children().removeClass("ui-state-disabled"),d.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled",!1)),this._disabledInputs=a.map(this._disabledInputs,function(a){return a===b?null:a}))},_disableDatepicker:function(b){var c,d,e=a(b),f=a.data(b,"datepicker");e.hasClass(this.markerClassName)&&(c=b.nodeName.toLowerCase(),"input"===c?(b.disabled=!0,f.trigger.filter("button").each(function(){this.disabled=!0}).end().filter("img").css({opacity:"0.5",cursor:"default"})):"div"!==c&&"span"!==c||(d=e.children("."+this._inlineClass),d.children().addClass("ui-state-disabled"),d.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled",!0)),this._disabledInputs=a.map(this._disabledInputs,function(a){return a===b?null:a}),this._disabledInputs[this._disabledInputs.length]=b)},_isDisabledDatepicker:function(a){if(!a)return!1;for(var b=0;b<this._disabledInputs.length;b++)if(this._disabledInputs[b]===a)return!0;return!1},_getInst:function(b){try{return a.data(b,"datepicker")}catch(c){throw"Missing instance data for this datepicker"}},_optionDatepicker:function(b,c,d){var e,g,h,i,j=this._getInst(b);return 2===arguments.length&&"string"==typeof c?"defaults"===c?a.extend({},a.datepicker._defaults):j?"all"===c?a.extend({},j.settings):this._get(j,c):null:(e=c||{},"string"==typeof c&&(e={},e[c]=d),void(j&&(this._curInst===j&&this._hideDatepicker(),g=this._getDateDatepicker(b,!0),h=this._getMinMaxDate(j,"min"),i=this._getMinMaxDate(j,"max"),f(j.settings,e),null!==h&&void 0!==e.dateFormat&&void 0===e.minDate&&(j.settings.minDate=this._formatDate(j,h)),null!==i&&void 0!==e.dateFormat&&void 0===e.maxDate&&(j.settings.maxDate=this._formatDate(j,i)),"disabled"in e&&(e.disabled?this._disableDatepicker(b):this._enableDatepicker(b)),this._attachments(a(b),j),this._autoSize(j),this._setDate(j,g),this._updateAlternate(j),this._updateDatepicker(j))))},_changeDatepicker:function(a,b,c){this._optionDatepicker(a,b,c)},_refreshDatepicker:function(a){var b=this._getInst(a);b&&this._updateDatepicker(b)},_setDateDatepicker:function(a,b){var c=this._getInst(a);c&&(this._setDate(c,b),this._updateDatepicker(c),this._updateAlternate(c))},_getDateDatepicker:function(a,b){var c=this._getInst(a);return c&&!c.inline&&this._setDateFromField(c,b),c?this._getDate(c):null},_doKeyDown:function(b){var c,d,e,f=a.datepicker._getInst(b.target),g=!0,h=f.dpDiv.is(".ui-datepicker-rtl");if(f._keyEvent=!0,a.datepicker._datepickerShowing)switch(b.keyCode){case 9:a.datepicker._hideDatepicker(),g=!1;break;case 13:return e=a("td."+a.datepicker._dayOverClass+":not(."+a.datepicker._currentClass+")",f.dpDiv),e[0]&&a.datepicker._selectDay(b.target,f.selectedMonth,f.selectedYear,e[0]),c=a.datepicker._get(f,"onSelect"),c?(d=a.datepicker._formatDate(f),c.apply(f.input?f.input[0]:null,[d,f])):a.datepicker._hideDatepicker(),!1;case 27:a.datepicker._hideDatepicker();break;case 33:a.datepicker._adjustDate(b.target,b.ctrlKey?-a.datepicker._get(f,"stepBigMonths"):-a.datepicker._get(f,"stepMonths"),"M");break;case 34:a.datepicker._adjustDate(b.target,b.ctrlKey?+a.datepicker._get(f,"stepBigMonths"):+a.datepicker._get(f,"stepMonths"),"M");break;case 35:(b.ctrlKey||b.metaKey)&&a.datepicker._clearDate(b.target),g=b.ctrlKey||b.metaKey;break;case 36:(b.ctrlKey||b.metaKey)&&a.datepicker._gotoToday(b.target),g=b.ctrlKey||b.metaKey;break;case 37:(b.ctrlKey||b.metaKey)&&a.datepicker._adjustDate(b.target,h?1:-1,"D"),g=b.ctrlKey||b.metaKey,b.originalEvent.altKey&&a.datepicker._adjustDate(b.target,b.ctrlKey?-a.datepicker._get(f,"stepBigMonths"):-a.datepicker._get(f,"stepMonths"),"M");break;case 38:(b.ctrlKey||b.metaKey)&&a.datepicker._adjustDate(b.target,-7,"D"),g=b.ctrlKey||b.metaKey;break;case 39:(b.ctrlKey||b.metaKey)&&a.datepicker._adjustDate(b.target,h?-1:1,"D"),g=b.ctrlKey||b.metaKey,b.originalEvent.altKey&&a.datepicker._adjustDate(b.target,b.ctrlKey?+a.datepicker._get(f,"stepBigMonths"):+a.datepicker._get(f,"stepMonths"),"M");break;case 40:(b.ctrlKey||b.metaKey)&&a.datepicker._adjustDate(b.target,7,"D"),g=b.ctrlKey||b.metaKey;break;default:g=!1}else 36===b.keyCode&&b.ctrlKey?a.datepicker._showDatepicker(this):g=!1;g&&(b.preventDefault(),b.stopPropagation())},_doKeyPress:function(b){var c,d,e=a.datepicker._getInst(b.target);if(a.datepicker._get(e,"constrainInput"))return c=a.datepicker._possibleChars(a.datepicker._get(e,"dateFormat")),d=String.fromCharCode(null==b.charCode?b.keyCode:b.charCode),b.ctrlKey||b.metaKey||d<" "||!c||c.indexOf(d)>-1},_doKeyUp:function(b){var c,d=a.datepicker._getInst(b.target);if(d.input.val()!==d.lastVal)try{c=a.datepicker.parseDate(a.datepicker._get(d,"dateFormat"),d.input?d.input.val():null,a.datepicker._getFormatConfig(d)),c&&(a.datepicker._setDateFromField(d),a.datepicker._updateAlternate(d),a.datepicker._updateDatepicker(d))}catch(e){}return!0},_showDatepicker:function(c){if(c=c.target||c,"input"!==c.nodeName.toLowerCase()&&(c=a("input",c.parentNode)[0]),!a.datepicker._isDisabledDatepicker(c)&&a.datepicker._lastInput!==c){var d,e,g,h,i,j,k;d=a.datepicker._getInst(c),a.datepicker._curInst&&a.datepicker._curInst!==d&&(a.datepicker._curInst.dpDiv.stop(!0,!0),d&&a.datepicker._datepickerShowing&&a.datepicker._hideDatepicker(a.datepicker._curInst.input[0])),e=a.datepicker._get(d,"beforeShow"),g=e?e.apply(c,[c,d]):{},g!==!1&&(f(d.settings,g),d.lastVal=null,a.datepicker._lastInput=c,a.datepicker._setDateFromField(d),a.datepicker._inDialog&&(c.value=""),a.datepicker._pos||(a.datepicker._pos=a.datepicker._findPos(c),a.datepicker._pos[1]+=c.offsetHeight),h=!1,a(c).parents().each(function(){return h|="fixed"===a(this).css("position"),!h}),i={left:a.datepicker._pos[0],top:a.datepicker._pos[1]},a.datepicker._pos=null,d.dpDiv.empty(),d.dpDiv.css({position:"absolute",display:"block",top:"-1000px"}),a.datepicker._updateDatepicker(d),i=a.datepicker._checkOffset(d,i,h),d.dpDiv.css({position:a.datepicker._inDialog&&a.blockUI?"static":h?"fixed":"absolute",display:"none",left:i.left+"px",top:i.top+"px"}),d.inline||(j=a.datepicker._get(d,"showAnim"),k=a.datepicker._get(d,"duration"),d.dpDiv.css("z-index",b(a(c))+1),a.datepicker._datepickerShowing=!0,a.effects&&a.effects.effect[j]?d.dpDiv.show(j,a.datepicker._get(d,"showOptions"),k):d.dpDiv[j||"show"](j?k:null),a.datepicker._shouldFocusInput(d)&&d.input.focus(),a.datepicker._curInst=d))}},_updateDatepicker:function(b){this.maxRows=4,g=b,b.dpDiv.empty().append(this._generateHTML(b)),this._attachHandlers(b);var c,d=this._getNumberOfMonths(b),f=d[1],h=17,i=b.dpDiv.find("."+this._dayOverClass+" a");i.length>0&&e.apply(i.get(0)),b.dpDiv.removeClass("ui-datepicker-multi-2 ui-datepicker-multi-3 ui-datepicker-multi-4").width(""),f>1&&b.dpDiv.addClass("ui-datepicker-multi-"+f).css("width",h*f+"em"),b.dpDiv[(1!==d[0]||1!==d[1]?"add":"remove")+"Class"]("ui-datepicker-multi"),b.dpDiv[(this._get(b,"isRTL")?"add":"remove")+"Class"]("ui-datepicker-rtl"),b===a.datepicker._curInst&&a.datepicker._datepickerShowing&&a.datepicker._shouldFocusInput(b)&&b.input.focus(),b.yearshtml&&(c=b.yearshtml,setTimeout(function(){c===b.yearshtml&&b.yearshtml&&b.dpDiv.find("select.ui-datepicker-year:first").replaceWith(b.yearshtml),c=b.yearshtml=null},0))},_shouldFocusInput:function(a){return a.input&&a.input.is(":visible")&&!a.input.is(":disabled")&&!a.input.is(":focus")},_checkOffset:function(b,c,d){var e=b.dpDiv.outerWidth(),f=b.dpDiv.outerHeight(),g=b.input?b.input.outerWidth():0,h=b.input?b.input.outerHeight():0,i=document.documentElement.clientWidth+(d?0:a(document).scrollLeft()),j=document.documentElement.clientHeight+(d?0:a(document).scrollTop());return c.left-=this._get(b,"isRTL")?e-g:0,c.left-=d&&c.left===b.input.offset().left?a(document).scrollLeft():0,c.top-=d&&c.top===b.input.offset().top+h?a(document).scrollTop():0,c.left-=Math.min(c.left,c.left+e>i&&i>e?Math.abs(c.left+e-i):0),c.top-=Math.min(c.top,c.top+f>j&&j>f?Math.abs(f+h):0),c},_findPos:function(b){for(var c,d=this._getInst(b),e=this._get(d,"isRTL");b&&("hidden"===b.type||1!==b.nodeType||a.expr.filters.hidden(b));)b=b[e?"previousSibling":"nextSibling"];return c=a(b).offset(),[c.left,c.top]},_hideDatepicker:function(b){var c,d,e,f,g=this._curInst;!g||b&&g!==a.data(b,"datepicker")||this._datepickerShowing&&(c=this._get(g,"showAnim"),d=this._get(g,"duration"),e=function(){a.datepicker._tidyDialog(g)},a.effects&&(a.effects.effect[c]||a.effects[c])?g.dpDiv.hide(c,a.datepicker._get(g,"showOptions"),d,e):g.dpDiv["slideDown"===c?"slideUp":"fadeIn"===c?"fadeOut":"hide"](c?d:null,e),c||e(),this._datepickerShowing=!1,f=this._get(g,"onClose"),f&&f.apply(g.input?g.input[0]:null,[g.input?g.input.val():"",g]),this._lastInput=null,this._inDialog&&(this._dialogInput.css({position:"absolute",left:"0",top:"-100px"}),a.blockUI&&(a.unblockUI(),a("body").append(this.dpDiv))),this._inDialog=!1)},_tidyDialog:function(a){a.dpDiv.removeClass(this._dialogClass).unbind(".ui-datepicker-calendar")},_checkExternalClick:function(b){if(a.datepicker._curInst){var c=a(b.target),d=a.datepicker._getInst(c[0]);(c[0].id===a.datepicker._mainDivId||0!==c.parents("#"+a.datepicker._mainDivId).length||c.hasClass(a.datepicker.markerClassName)||c.closest("."+a.datepicker._triggerClass).length||!a.datepicker._datepickerShowing||a.datepicker._inDialog&&a.blockUI)&&(!c.hasClass(a.datepicker.markerClassName)||a.datepicker._curInst===d)||a.datepicker._hideDatepicker()}},_adjustDate:function(b,c,d){var e=a(b),f=this._getInst(e[0]);this._isDisabledDatepicker(e[0])||(this._adjustInstDate(f,c+("M"===d?this._get(f,"showCurrentAtPos"):0),d),this._updateDatepicker(f))},_gotoToday:function(b){var c,d=a(b),e=this._getInst(d[0]);this._get(e,"gotoCurrent")&&e.currentDay?(e.selectedDay=e.currentDay,e.drawMonth=e.selectedMonth=e.currentMonth,e.drawYear=e.selectedYear=e.currentYear):(c=new Date,e.selectedDay=c.getDate(),e.drawMonth=e.selectedMonth=c.getMonth(),e.drawYear=e.selectedYear=c.getFullYear()),this._notifyChange(e),this._adjustDate(d)},_selectMonthYear:function(b,c,d){var e=a(b),f=this._getInst(e[0]);f["selected"+("M"===d?"Month":"Year")]=f["draw"+("M"===d?"Month":"Year")]=parseInt(c.options[c.selectedIndex].value,10),this._notifyChange(f),this._adjustDate(e)},_selectDay:function(b,c,d,e){var f,g=a(b);a(e).hasClass(this._unselectableClass)||this._isDisabledDatepicker(g[0])||(f=this._getInst(g[0]),f.selectedDay=f.currentDay=a("a",e).html(),f.selectedMonth=f.currentMonth=c,f.selectedYear=f.currentYear=d,this._selectDate(b,this._formatDate(f,f.currentDay,f.currentMonth,f.currentYear)))},_clearDate:function(b){var c=a(b);this._selectDate(c,"")},_selectDate:function(b,c){var d,e=a(b),f=this._getInst(e[0]);c=null!=c?c:this._formatDate(f),f.input&&f.input.val(c),this._updateAlternate(f),d=this._get(f,"onSelect"),d?d.apply(f.input?f.input[0]:null,[c,f]):f.input&&f.input.trigger("change"),f.inline?this._updateDatepicker(f):(this._hideDatepicker(),this._lastInput=f.input[0],"object"!=typeof f.input[0]&&f.input.focus(),this._lastInput=null)},_updateAlternate:function(b){var c,d,e,f=this._get(b,"altField");f&&(c=this._get(b,"altFormat")||this._get(b,"dateFormat"),d=this._getDate(b),e=this.formatDate(c,d,this._getFormatConfig(b)),a(f).each(function(){a(this).val(e)}))},noWeekends:function(a){var b=a.getDay();return[b>0&&b<6,""]},iso8601Week:function(a){var b,c=new Date(a.getTime());return c.setDate(c.getDate()+4-(c.getDay()||7)),b=c.getTime(),c.setMonth(0),c.setDate(1),Math.floor(Math.round((b-c)/864e5)/7)+1},parseDate:function(b,c,d){if(null==b||null==c)throw"Invalid arguments";if(c="object"==typeof c?c.toString():c+"",""===c)return null;var e,f,g,h,i=0,j=(d?d.shortYearCutoff:null)||this._defaults.shortYearCutoff,k="string"!=typeof j?j:(new Date).getFullYear()%100+parseInt(j,10),l=(d?d.dayNamesShort:null)||this._defaults.dayNamesShort,m=(d?d.dayNames:null)||this._defaults.dayNames,n=(d?d.monthNamesShort:null)||this._defaults.monthNamesShort,o=(d?d.monthNames:null)||this._defaults.monthNames,p=-1,q=-1,r=-1,s=-1,t=!1,u=function(a){var c=e+1<b.length&&b.charAt(e+1)===a;return c&&e++,c},v=function(a){var b=u(a),d="@"===a?14:"!"===a?20:"y"===a&&b?4:"o"===a?3:2,e="y"===a?d:1,f=new RegExp("^\\d{"+e+","+d+"}"),g=c.substring(i).match(f);if(!g)throw"Missing number at position "+i;return i+=g[0].length,parseInt(g[0],10)},w=function(b,d,e){var f=-1,g=a.map(u(b)?e:d,function(a,b){return[[b,a]]}).sort(function(a,b){return-(a[1].length-b[1].length)});if(a.each(g,function(a,b){var d=b[1];if(c.substr(i,d.length).toLowerCase()===d.toLowerCase())return f=b[0],i+=d.length,!1}),f!==-1)return f+1;throw"Unknown name at position "+i},x=function(){if(c.charAt(i)!==b.charAt(e))throw"Unexpected literal at position "+i;i++};for(e=0;e<b.length;e++)if(t)"'"!==b.charAt(e)||u("'")?x():t=!1;else switch(b.charAt(e)){case"d":r=v("d");break;case"D":w("D",l,m);break;case"o":s=v("o");break;case"m":q=v("m");break;case"M":q=w("M",n,o);break;case"y":p=v("y");break;case"@":h=new Date(v("@")),p=h.getFullYear(),q=h.getMonth()+1,r=h.getDate();break;case"!":h=new Date((v("!")-this._ticksTo1970)/1e4),p=h.getFullYear(),q=h.getMonth()+1,r=h.getDate();break;case"'":u("'")?x():t=!0;break;default:x()}if(i<c.length&&(g=c.substr(i),!/^\s+/.test(g)))throw"Extra/unparsed characters found in date: "+g;if(p===-1?p=(new Date).getFullYear():p<100&&(p+=(new Date).getFullYear()-(new Date).getFullYear()%100+(p<=k?0:-100)),s>-1)for(q=1,r=s;;){if(f=this._getDaysInMonth(p,q-1),r<=f)break;q++,r-=f}if(h=this._daylightSavingAdjust(new Date(p,q-1,r)),h.getFullYear()!==p||h.getMonth()+1!==q||h.getDate()!==r)throw"Invalid date";return h},ATOM:"yy-mm-dd",COOKIE:"D, dd M yy",ISO_8601:"yy-mm-dd",RFC_822:"D, d M y",RFC_850:"DD, dd-M-y",RFC_1036:"D, d M y",RFC_1123:"D, d M yy",RFC_2822:"D, d M yy",RSS:"D, d M y",TICKS:"!",TIMESTAMP:"@",W3C:"yy-mm-dd",_ticksTo1970:24*(718685+Math.floor(492.5)-Math.floor(19.7)+Math.floor(4.925))*60*60*1e7,formatDate:function(a,b,c){if(!b)return"";var d,e=(c?c.dayNamesShort:null)||this._defaults.dayNamesShort,f=(c?c.dayNames:null)||this._defaults.dayNames,g=(c?c.monthNamesShort:null)||this._defaults.monthNamesShort,h=(c?c.monthNames:null)||this._defaults.monthNames,i=function(b){var c=d+1<a.length&&a.charAt(d+1)===b;return c&&d++,c},j=function(a,b,c){var d=""+b;if(i(a))for(;d.length<c;)d="0"+d;return d},k=function(a,b,c,d){return i(a)?d[b]:c[b]},l="",m=!1;if(b)for(d=0;d<a.length;d++)if(m)"'"!==a.charAt(d)||i("'")?l+=a.charAt(d):m=!1;else switch(a.charAt(d)){case"d":l+=j("d",b.getDate(),2);break;case"D":l+=k("D",b.getDay(),e,f);break;case"o":l+=j("o",Math.round((new Date(b.getFullYear(),b.getMonth(),b.getDate()).getTime()-new Date(b.getFullYear(),0,0).getTime())/864e5),3);break;case"m":l+=j("m",b.getMonth()+1,2);break;case"M":l+=k("M",b.getMonth(),g,h);break;case"y":l+=i("y")?b.getFullYear():(b.getYear()%100<10?"0":"")+b.getYear()%100;break;case"@":l+=b.getTime();break;case"!":l+=1e4*b.getTime()+this._ticksTo1970;break;case"'":i("'")?l+="'":m=!0;break;default:l+=a.charAt(d)}return l},_possibleChars:function(a){var b,c="",d=!1,e=function(c){var d=b+1<a.length&&a.charAt(b+1)===c;return d&&b++,d};for(b=0;b<a.length;b++)if(d)"'"!==a.charAt(b)||e("'")?c+=a.charAt(b):d=!1;else switch(a.charAt(b)){case"d":case"m":case"y":case"@":c+="0123456789";break;case"D":case"M":return null;case"'":e("'")?c+="'":d=!0;break;default:c+=a.charAt(b)}return c},_get:function(a,b){return void 0!==a.settings[b]?a.settings[b]:this._defaults[b]},_setDateFromField:function(a,b){if(a.input.val()!==a.lastVal){var c=this._get(a,"dateFormat"),d=a.lastVal=a.input?a.input.val():null,e=this._getDefaultDate(a),f=e,g=this._getFormatConfig(a);try{f=this.parseDate(c,d,g)||e}catch(h){d=b?"":d}a.selectedDay=f.getDate(),a.drawMonth=a.selectedMonth=f.getMonth(),a.drawYear=a.selectedYear=f.getFullYear(),a.currentDay=d?f.getDate():0,a.currentMonth=d?f.getMonth():0,a.currentYear=d?f.getFullYear():0,this._adjustInstDate(a)}},_getDefaultDate:function(a){return this._restrictMinMax(a,this._determineDate(a,this._get(a,"defaultDate"),new Date))},_determineDate:function(b,c,d){var e=function(a){var b=new Date;return b.setDate(b.getDate()+a),b},f=function(c){try{return a.datepicker.parseDate(a.datepicker._get(b,"dateFormat"),c,a.datepicker._getFormatConfig(b))}catch(d){}for(var e=(c.toLowerCase().match(/^c/)?a.datepicker._getDate(b):null)||new Date,f=e.getFullYear(),g=e.getMonth(),h=e.getDate(),i=/([+\-]?[0-9]+)\s*(d|D|w|W|m|M|y|Y)?/g,j=i.exec(c);j;){switch(j[2]||"d"){case"d":case"D":h+=parseInt(j[1],10);break;case"w":case"W":h+=7*parseInt(j[1],10);break;case"m":case"M":g+=parseInt(j[1],10),h=Math.min(h,a.datepicker._getDaysInMonth(f,g));break;case"y":case"Y":f+=parseInt(j[1],10),h=Math.min(h,a.datepicker._getDaysInMonth(f,g))}j=i.exec(c)}return new Date(f,g,h)},g=null==c||""===c?d:"string"==typeof c?f(c):"number"==typeof c?isNaN(c)?d:e(c):new Date(c.getTime());return g=g&&"Invalid Date"===g.toString()?d:g,g&&(g.setHours(0),g.setMinutes(0),g.setSeconds(0),g.setMilliseconds(0)),this._daylightSavingAdjust(g)},_daylightSavingAdjust:function(a){return a?(a.setHours(a.getHours()>12?a.getHours()+2:0),a):null},_setDate:function(a,b,c){var d=!b,e=a.selectedMonth,f=a.selectedYear,g=this._restrictMinMax(a,this._determineDate(a,b,new Date));a.selectedDay=a.currentDay=g.getDate(),a.drawMonth=a.selectedMonth=a.currentMonth=g.getMonth(),a.drawYear=a.selectedYear=a.currentYear=g.getFullYear(),e===a.selectedMonth&&f===a.selectedYear||c||this._notifyChange(a),this._adjustInstDate(a),a.input&&a.input.val(d?"":this._formatDate(a))},_getDate:function(a){var b=!a.currentYear||a.input&&""===a.input.val()?null:this._daylightSavingAdjust(new Date(a.currentYear,a.currentMonth,a.currentDay));return b},_attachHandlers:function(b){var c=this._get(b,"stepMonths"),d="#"+b.id.replace(/\\\\/g,"\\");b.dpDiv.find("[data-handler]").map(function(){var b={prev:function(){a.datepicker._adjustDate(d,-c,"M")},next:function(){a.datepicker._adjustDate(d,+c,"M")},hide:function(){a.datepicker._hideDatepicker()},today:function(){a.datepicker._gotoToday(d)},selectDay:function(){return a.datepicker._selectDay(d,+this.getAttribute("data-month"),+this.getAttribute("data-year"),this),!1},selectMonth:function(){return a.datepicker._selectMonthYear(d,this,"M"),!1},selectYear:function(){return a.datepicker._selectMonthYear(d,this,"Y"),!1}};a(this).bind(this.getAttribute("data-event"),b[this.getAttribute("data-handler")])})},_generateHTML:function(a){var b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O=new Date,P=this._daylightSavingAdjust(new Date(O.getFullYear(),O.getMonth(),O.getDate())),Q=this._get(a,"isRTL"),R=this._get(a,"showButtonPanel"),S=this._get(a,"hideIfNoPrevNext"),T=this._get(a,"navigationAsDateFormat"),U=this._getNumberOfMonths(a),V=this._get(a,"showCurrentAtPos"),W=this._get(a,"stepMonths"),X=1!==U[0]||1!==U[1],Y=this._daylightSavingAdjust(a.currentDay?new Date(a.currentYear,a.currentMonth,a.currentDay):new Date(9999,9,9)),Z=this._getMinMaxDate(a,"min"),$=this._getMinMaxDate(a,"max"),_=a.drawMonth-V,aa=a.drawYear;if(_<0&&(_+=12,aa--),$)for(b=this._daylightSavingAdjust(new Date($.getFullYear(),$.getMonth()-U[0]*U[1]+1,$.getDate())),b=Z&&b<Z?Z:b;this._daylightSavingAdjust(new Date(aa,_,1))>b;)_--,_<0&&(_=11,aa--);for(a.drawMonth=_,a.drawYear=aa,c=this._get(a,"prevText"),c=T?this.formatDate(c,this._daylightSavingAdjust(new Date(aa,_-W,1)),this._getFormatConfig(a)):c,d=this._canAdjustMonth(a,-1,aa,_)?"<a class='ui-datepicker-prev ui-corner-all' data-handler='prev' data-event='click' title='"+c+"'><span class='ui-icon ui-icon-circle-triangle-"+(Q?"e":"w")+"'>"+c+"</span></a>":S?"":"<a class='ui-datepicker-prev ui-corner-all ui-state-disabled' title='"+c+"'><span class='ui-icon ui-icon-circle-triangle-"+(Q?"e":"w")+"'>"+c+"</span></a>",e=this._get(a,"nextText"),e=T?this.formatDate(e,this._daylightSavingAdjust(new Date(aa,_+W,1)),this._getFormatConfig(a)):e,f=this._canAdjustMonth(a,1,aa,_)?"<a class='ui-datepicker-next ui-corner-all' data-handler='next' data-event='click' title='"+e+"'><span class='ui-icon ui-icon-circle-triangle-"+(Q?"w":"e")+"'>"+e+"</span></a>":S?"":"<a class='ui-datepicker-next ui-corner-all ui-state-disabled' title='"+e+"'><span class='ui-icon ui-icon-circle-triangle-"+(Q?"w":"e")+"'>"+e+"</span></a>",g=this._get(a,"currentText"),h=this._get(a,"gotoCurrent")&&a.currentDay?Y:P,g=T?this.formatDate(g,h,this._getFormatConfig(a)):g,i=a.inline?"":"<button type='button' class='ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all' data-handler='hide' data-event='click'>"+this._get(a,"closeText")+"</button>",j=R?"<div class='ui-datepicker-buttonpane ui-widget-content'>"+(Q?i:"")+(this._isInRange(a,h)?"<button type='button' class='ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all' data-handler='today' data-event='click'>"+g+"</button>":"")+(Q?"":i)+"</div>":"",k=parseInt(this._get(a,"firstDay"),10),k=isNaN(k)?0:k,l=this._get(a,"showWeek"),m=this._get(a,"dayNames"),n=this._get(a,"dayNamesMin"),o=this._get(a,"monthNames"),p=this._get(a,"monthNamesShort"),q=this._get(a,"beforeShowDay"),r=this._get(a,"showOtherMonths"),s=this._get(a,"selectOtherMonths"),t=this._getDefaultDate(a),u="",w=0;w<U[0];w++){for(x="",this.maxRows=4,y=0;y<U[1];y++){if(z=this._daylightSavingAdjust(new Date(aa,_,a.selectedDay)),A=" ui-corner-all",B="",X){if(B+="<div class='ui-datepicker-group",U[1]>1)switch(y){case 0:B+=" ui-datepicker-group-first",A=" ui-corner-"+(Q?"right":"left");break;case U[1]-1:B+=" ui-datepicker-group-last",A=" ui-corner-"+(Q?"left":"right");break;default:B+=" ui-datepicker-group-middle",A=""}B+="'>"}for(B+="<div class='ui-datepicker-header ui-widget-header ui-helper-clearfix"+A+"'>"+(/all|left/.test(A)&&0===w?Q?f:d:"")+(/all|right/.test(A)&&0===w?Q?d:f:"")+this._generateMonthYearHeader(a,_,aa,Z,$,w>0||y>0,o,p)+"</div><table class='ui-datepicker-calendar'><thead><tr>",C=l?"<th class='ui-datepicker-week-col'>"+this._get(a,"weekHeader")+"</th>":"",v=0;v<7;v++)D=(v+k)%7,C+="<th scope='col'"+((v+k+6)%7>=5?" class='ui-datepicker-week-end'":"")+"><span title='"+m[D]+"'>"+n[D]+"</span></th>";for(B+=C+"</tr></thead><tbody>",E=this._getDaysInMonth(aa,_),aa===a.selectedYear&&_===a.selectedMonth&&(a.selectedDay=Math.min(a.selectedDay,E)),F=(this._getFirstDayOfMonth(aa,_)-k+7)%7,G=Math.ceil((F+E)/7),H=X&&this.maxRows>G?this.maxRows:G,this.maxRows=H,I=this._daylightSavingAdjust(new Date(aa,_,1-F)),J=0;J<H;J++){for(B+="<tr>",K=l?"<td class='ui-datepicker-week-col'>"+this._get(a,"calculateWeek")(I)+"</td>":"",v=0;v<7;v++)L=q?q.apply(a.input?a.input[0]:null,[I]):[!0,""],M=I.getMonth()!==_,N=M&&!s||!L[0]||Z&&I<Z||$&&I>$,K+="<td class='"+((v+k+6)%7>=5?" ui-datepicker-week-end":"")+(M?" ui-datepicker-other-month":"")+(I.getTime()===z.getTime()&&_===a.selectedMonth&&a._keyEvent||t.getTime()===I.getTime()&&t.getTime()===z.getTime()?" "+this._dayOverClass:"")+(N?" "+this._unselectableClass+" ui-state-disabled":"")+(M&&!r?"":" "+L[1]+(I.getTime()===Y.getTime()?" "+this._currentClass:"")+(I.getTime()===P.getTime()?" ui-datepicker-today":""))+"'"+(M&&!r||!L[2]?"":" title='"+L[2].replace(/'/g,"&#39;")+"'")+(N?"":" data-handler='selectDay' data-event='click' data-month='"+I.getMonth()+"' data-year='"+I.getFullYear()+"'")+">"+(M&&!r?"&#xa0;":N?"<span class='ui-state-default'>"+I.getDate()+"</span>":"<a class='ui-state-default"+(I.getTime()===P.getTime()?" ui-state-highlight":"")+(I.getTime()===Y.getTime()?" ui-state-active":"")+(M?" ui-priority-secondary":"")+"' href='#'>"+I.getDate()+"</a>")+"</td>",I.setDate(I.getDate()+1),I=this._daylightSavingAdjust(I);B+=K+"</tr>"}_++,_>11&&(_=0,aa++),B+="</tbody></table>"+(X?"</div>"+(U[0]>0&&y===U[1]-1?"<div class='ui-datepicker-row-break'></div>":""):""),x+=B}u+=x}return u+=j,a._keyEvent=!1,u},_generateMonthYearHeader:function(a,b,c,d,e,f,g,h){var i,j,k,l,m,n,o,p,q=this._get(a,"changeMonth"),r=this._get(a,"changeYear"),s=this._get(a,"showMonthAfterYear"),t="<div class='ui-datepicker-title'>",u="";if(f||!q)u+="<span class='ui-datepicker-month'>"+g[b]+"</span>";else{
for(i=d&&d.getFullYear()===c,j=e&&e.getFullYear()===c,u+="<select class='ui-datepicker-month' data-handler='selectMonth' data-event='change'>",k=0;k<12;k++)(!i||k>=d.getMonth())&&(!j||k<=e.getMonth())&&(u+="<option value='"+k+"'"+(k===b?" selected='selected'":"")+">"+h[k]+"</option>");u+="</select>"}if(s||(t+=u+(!f&&q&&r?"":"&#xa0;")),!a.yearshtml)if(a.yearshtml="",f||!r)t+="<span class='ui-datepicker-year'>"+c+"</span>";else{for(l=this._get(a,"yearRange").split(":"),m=(new Date).getFullYear(),n=function(a){var b=a.match(/c[+\-].*/)?c+parseInt(a.substring(1),10):a.match(/[+\-].*/)?m+parseInt(a,10):parseInt(a,10);return isNaN(b)?m:b},o=n(l[0]),p=Math.max(o,n(l[1]||"")),o=d?Math.max(o,d.getFullYear()):o,p=e?Math.min(p,e.getFullYear()):p,a.yearshtml+="<select class='ui-datepicker-year' data-handler='selectYear' data-event='change'>";o<=p;o++)a.yearshtml+="<option value='"+o+"'"+(o===c?" selected='selected'":"")+">"+o+"</option>";a.yearshtml+="</select>",t+=a.yearshtml,a.yearshtml=null}return t+=this._get(a,"yearSuffix"),s&&(t+=(!f&&q&&r?"":"&#xa0;")+u),t+="</div>"},_adjustInstDate:function(a,b,c){var d=a.drawYear+("Y"===c?b:0),e=a.drawMonth+("M"===c?b:0),f=Math.min(a.selectedDay,this._getDaysInMonth(d,e))+("D"===c?b:0),g=this._restrictMinMax(a,this._daylightSavingAdjust(new Date(d,e,f)));a.selectedDay=g.getDate(),a.drawMonth=a.selectedMonth=g.getMonth(),a.drawYear=a.selectedYear=g.getFullYear(),"M"!==c&&"Y"!==c||this._notifyChange(a)},_restrictMinMax:function(a,b){var c=this._getMinMaxDate(a,"min"),d=this._getMinMaxDate(a,"max"),e=c&&b<c?c:b;return d&&e>d?d:e},_notifyChange:function(a){var b=this._get(a,"onChangeMonthYear");b&&b.apply(a.input?a.input[0]:null,[a.selectedYear,a.selectedMonth+1,a])},_getNumberOfMonths:function(a){var b=this._get(a,"numberOfMonths");return null==b?[1,1]:"number"==typeof b?[1,b]:b},_getMinMaxDate:function(a,b){return this._determineDate(a,this._get(a,b+"Date"),null)},_getDaysInMonth:function(a,b){return 32-this._daylightSavingAdjust(new Date(a,b,32)).getDate()},_getFirstDayOfMonth:function(a,b){return new Date(a,b,1).getDay()},_canAdjustMonth:function(a,b,c,d){var e=this._getNumberOfMonths(a),f=this._daylightSavingAdjust(new Date(c,d+(b<0?b:e[0]*e[1]),1));return b<0&&f.setDate(this._getDaysInMonth(f.getFullYear(),f.getMonth())),this._isInRange(a,f)},_isInRange:function(a,b){var c,d,e=this._getMinMaxDate(a,"min"),f=this._getMinMaxDate(a,"max"),g=null,h=null,i=this._get(a,"yearRange");return i&&(c=i.split(":"),d=(new Date).getFullYear(),g=parseInt(c[0],10),h=parseInt(c[1],10),c[0].match(/[+\-].*/)&&(g+=d),c[1].match(/[+\-].*/)&&(h+=d)),(!e||b.getTime()>=e.getTime())&&(!f||b.getTime()<=f.getTime())&&(!g||b.getFullYear()>=g)&&(!h||b.getFullYear()<=h)},_getFormatConfig:function(a){var b=this._get(a,"shortYearCutoff");return b="string"!=typeof b?b:(new Date).getFullYear()%100+parseInt(b,10),{shortYearCutoff:b,dayNamesShort:this._get(a,"dayNamesShort"),dayNames:this._get(a,"dayNames"),monthNamesShort:this._get(a,"monthNamesShort"),monthNames:this._get(a,"monthNames")}},_formatDate:function(a,b,c,d){b||(a.currentDay=a.selectedDay,a.currentMonth=a.selectedMonth,a.currentYear=a.selectedYear);var e=b?"object"==typeof b?b:this._daylightSavingAdjust(new Date(d,c,b)):this._daylightSavingAdjust(new Date(a.currentYear,a.currentMonth,a.currentDay));return this.formatDate(this._get(a,"dateFormat"),e,this._getFormatConfig(a))}}),a.fn.datepicker=function(b){if(!this.length)return this;a.datepicker.initialized||(a(document).mousedown(a.datepicker._checkExternalClick),a.datepicker.initialized=!0),0===a("#"+a.datepicker._mainDivId).length&&a("body").append(a.datepicker.dpDiv);var c=Array.prototype.slice.call(arguments,1);return"string"!=typeof b||"isDisabled"!==b&&"getDate"!==b&&"widget"!==b?"option"===b&&2===arguments.length&&"string"==typeof arguments[1]?a.datepicker["_"+b+"Datepicker"].apply(a.datepicker,[this[0]].concat(c)):this.each(function(){"string"==typeof b?a.datepicker["_"+b+"Datepicker"].apply(a.datepicker,[this].concat(c)):a.datepicker._attachDatepicker(this,b)}):a.datepicker["_"+b+"Datepicker"].apply(a.datepicker,[this[0]].concat(c))},a.datepicker=new c,a.datepicker.initialized=!1,a.datepicker.uuid=(new Date).getTime(),a.datepicker.version="1.11.4",a.datepicker});

// fin date picker 
// http://spin.js.org/#v2.3.2
!function(a,b){"object"==typeof module&&module.exports?module.exports=b():"function"==typeof define&&define.amd?define(b):a.Spinner=b()}(this,function(){"use strict";function a(a,b){var c,d=document.createElement(a||"div");for(c in b)d[c]=b[c];return d}function b(a){for(var b=1,c=arguments.length;c>b;b++)a.appendChild(arguments[b]);return a}function c(a,b,c,d){var e=["opacity",b,~~(100*a),c,d].join("-"),f=.01+c/d*100,g=Math.max(1-(1-a)/b*(100-f),a),h=j.substring(0,j.indexOf("Animation")).toLowerCase(),i=h&&"-"+h+"-"||"";return m[e]||(k.insertRule("@"+i+"keyframes "+e+"{0%{opacity:"+g+"}"+f+"%{opacity:"+a+"}"+(f+.01)+"%{opacity:1}"+(f+b)%100+"%{opacity:"+a+"}100%{opacity:"+g+"}}",k.cssRules.length),m[e]=1),e}function d(a,b){var c,d,e=a.style;if(b=b.charAt(0).toUpperCase()+b.slice(1),void 0!==e[b])return b;for(d=0;d<l.length;d++)if(c=l[d]+b,void 0!==e[c])return c}function e(a,b){for(var c in b)a.style[d(a,c)||c]=b[c];return a}function f(a){for(var b=1;b<arguments.length;b++){var c=arguments[b];for(var d in c)void 0===a[d]&&(a[d]=c[d])}return a}function g(a,b){return"string"==typeof a?a:a[b%a.length]}function h(a){this.opts=f(a||{},h.defaults,n)}function i(){function c(b,c){return a("<"+b+' xmlns="urn:schemas-microsoft.com:vml" class="spin-vml">',c)}k.addRule(".spin-vml","behavior:url(#default#VML)"),h.prototype.lines=function(a,d){function f(){return e(c("group",{coordsize:k+" "+k,coordorigin:-j+" "+-j}),{width:k,height:k})}function h(a,h,i){b(m,b(e(f(),{rotation:360/d.lines*a+"deg",left:~~h}),b(e(c("roundrect",{arcsize:d.corners}),{width:j,height:d.scale*d.width,left:d.scale*d.radius,top:-d.scale*d.width>>1,filter:i}),c("fill",{color:g(d.color,a),opacity:d.opacity}),c("stroke",{opacity:0}))))}var i,j=d.scale*(d.length+d.width),k=2*d.scale*j,l=-(d.width+d.length)*d.scale*2+"px",m=e(f(),{position:"absolute",top:l,left:l});if(d.shadow)for(i=1;i<=d.lines;i++)h(i,-2,"progid:DXImageTransform.Microsoft.Blur(pixelradius=2,makeshadow=1,shadowopacity=.3)");for(i=1;i<=d.lines;i++)h(i);return b(a,m)},h.prototype.opacity=function(a,b,c,d){var e=a.firstChild;d=d.shadow&&d.lines||0,e&&b+d<e.childNodes.length&&(e=e.childNodes[b+d],e=e&&e.firstChild,e=e&&e.firstChild,e&&(e.opacity=c))}}var j,k,l=["webkit","Moz","ms","O"],m={},n={lines:12,length:7,width:5,radius:10,scale:1,corners:1,color:"#000",opacity:.25,rotate:0,direction:1,speed:1,trail:100,fps:20,zIndex:2e9,className:"spinner",top:"50%",left:"50%",shadow:!1,hwaccel:!1,position:"absolute"};if(h.defaults={},f(h.prototype,{spin:function(b){this.stop();var c=this,d=c.opts,f=c.el=a(null,{className:d.className});if(e(f,{position:d.position,width:0,zIndex:d.zIndex,left:d.left,top:d.top}),b&&b.insertBefore(f,b.firstChild||null),f.setAttribute("role","progressbar"),c.lines(f,c.opts),!j){var g,h=0,i=(d.lines-1)*(1-d.direction)/2,k=d.fps,l=k/d.speed,m=(1-d.opacity)/(l*d.trail/100),n=l/d.lines;!function o(){h++;for(var a=0;a<d.lines;a++)g=Math.max(1-(h+(d.lines-a)*n)%l*m,d.opacity),c.opacity(f,a*d.direction+i,g,d);c.timeout=c.el&&setTimeout(o,~~(1e3/k))}()}return c},stop:function(){var a=this.el;return a&&(clearTimeout(this.timeout),a.parentNode&&a.parentNode.removeChild(a),this.el=void 0),this},lines:function(d,f){function h(b,c){return e(a(),{position:"absolute",width:f.scale*(f.length+f.width)+"px",height:f.scale*f.width+"px",background:b,boxShadow:c,transformOrigin:"left",transform:"rotate("+~~(360/f.lines*k+f.rotate)+"deg) translate("+f.scale*f.radius+"px,0)",borderRadius:(f.corners*f.scale*f.width>>1)+"px"})}for(var i,k=0,l=(f.lines-1)*(1-f.direction)/2;k<f.lines;k++)i=e(a(),{position:"absolute",top:1+~(f.scale*f.width/2)+"px",transform:f.hwaccel?"translate3d(0,0,0)":"",opacity:f.opacity,animation:j&&c(f.opacity,f.trail,l+k*f.direction,f.lines)+" "+1/f.speed+"s linear infinite"}),f.shadow&&b(i,e(h("#000","0 0 4px #000"),{top:"2px"})),b(d,b(i,h(g(f.color,k),"0 0 1px rgba(0,0,0,.1)")));return d},opacity:function(a,b,c){b<a.childNodes.length&&(a.childNodes[b].style.opacity=c)}}),"undefined"!=typeof document){k=function(){var c=a("style",{type:"text/css"});return b(document.getElementsByTagName("head")[0],c),c.sheet||c.styleSheet}();var o=e(a("group"),{behavior:"url(#default#VML)"});!d(o,"transform")&&o.adj?i():j=d(o,"animation")}return h});

// fin spin 

/**
 * Copyright (c) 2011-2014 Felix Gnass
 * Licensed under the MIT license
 */

/*

Basic Usage:
============

$('#el').spin(); // Creates a default Spinner using the text color of #el.
$('#el').spin({ ... }); // Creates a Spinner using the provided options.

$('#el').spin(false); // Stops and removes the spinner.

Using Presets:
==============

$('#el').spin('small'); // Creates a 'small' Spinner using the text color of #el.
$('#el').spin('large', '#fff'); // Creates a 'large' white Spinner.

Adding a custom preset:
=======================

$.fn.spin.presets.flower = {
  lines: 9
  length: 10
  width: 20
  radius: 0
}

$('#el').spin('flower', 'red');

*/

(function(factory) {

  if (typeof exports == 'object') {
    // CommonJS
    factory(require('jquery'), require('spin'))
  }
  else if (typeof define == 'function' && define.amd) {
    // AMD, register as anonymous module
    define(['jquery', 'spin'], factory)
  }
  else {
    // Browser globals
    if (!window.Spinner) throw new Error('Spin.js not present')
    factory(window.jQuery, window.Spinner)
  }

}(function($, Spinner) {

  $.fn.spin = function(opts, color) {

    return this.each(function() {
      var $this = $(this),
        data = $this.data();

      if (data.spinner) {
        data.spinner.stop();
        delete data.spinner;
      }
      if (opts !== false) {
        opts = $.extend(
          { color: color || $this.css('color') },
          $.fn.spin.presets[opts] || opts
        )
        data.spinner = new Spinner(opts).spin(this)
      }
    })
  }

  $.fn.spin.presets = {
    tiny: { lines: 8, length: 2, width: 2, radius: 3 },
    small: { lines: 8, length: 4, width: 3, radius: 5 },
    large: { lines: 10, length: 8, width: 4, radius: 8 }
  }

}));

// fin jq spin 

/* Tooltipster v3.3.0 */;(function(e,t,n){function s(t,n){this.bodyOverflowX;this.callbacks={hide:[],show:[]};this.checkInterval=null;this.Content;this.$el=e(t);this.$elProxy;this.elProxyPosition;this.enabled=true;this.options=e.extend({},i,n);this.mouseIsOverProxy=false;this.namespace="tooltipster-"+Math.round(Math.random()*1e5);this.Status="hidden";this.timerHide=null;this.timerShow=null;this.$tooltip;this.options.iconTheme=this.options.iconTheme.replace(".","");this.options.theme=this.options.theme.replace(".","");this._init()}function o(t,n){var r=true;e.each(t,function(e,i){if(typeof n[e]==="undefined"||t[e]!==n[e]){r=false;return false}});return r}function f(){return!a&&u}function l(){var e=n.body||n.documentElement,t=e.style,r="transition";if(typeof t[r]=="string"){return true}v=["Moz","Webkit","Khtml","O","ms"],r=r.charAt(0).toUpperCase()+r.substr(1);for(var i=0;i<v.length;i++){if(typeof t[v[i]+r]=="string"){return true}}return false}var r="tooltipster",i={animation:"fade",arrow:true,arrowColor:"",autoClose:true,content:null,contentAsHTML:false,contentCloning:true,debug:true,delay:200,minWidth:0,maxWidth:null,functionInit:function(e,t){},functionBefore:function(e,t){t()},functionReady:function(e,t){},functionAfter:function(e){},hideOnClick:false,icon:"(?)",iconCloning:true,iconDesktop:false,iconTouch:false,iconTheme:"tooltipster-icon",interactive:false,interactiveTolerance:350,multiple:false,offsetX:0,offsetY:0,onlyOne:false,position:"top",positionTracker:false,positionTrackerCallback:function(e){if(this.option("trigger")=="hover"&&this.option("autoClose")){this.hide()}},restoration:"current",speed:350,timer:0,theme:"tooltipster-default",touchDevices:true,trigger:"hover",updateAnimation:true};s.prototype={_init:function(){var t=this;if(n.querySelector){var r=null;if(t.$el.data("tooltipster-initialTitle")===undefined){r=t.$el.attr("title");if(r===undefined)r=null;t.$el.data("tooltipster-initialTitle",r)}if(t.options.content!==null){t._content_set(t.options.content)}else{t._content_set(r)}var i=t.options.functionInit.call(t.$el,t.$el,t.Content);if(typeof i!=="undefined")t._content_set(i);t.$el.removeAttr("title").addClass("tooltipstered");if(!u&&t.options.iconDesktop||u&&t.options.iconTouch){if(typeof t.options.icon==="string"){t.$elProxy=e('<span class="'+t.options.iconTheme+'"></span>');t.$elProxy.text(t.options.icon)}else{if(t.options.iconCloning)t.$elProxy=t.options.icon.clone(true);else t.$elProxy=t.options.icon}t.$elProxy.insertAfter(t.$el)}else{t.$elProxy=t.$el}if(t.options.trigger=="hover"){t.$elProxy.on("mouseenter."+t.namespace,function(){if(!f()||t.options.touchDevices){t.mouseIsOverProxy=true;t._show()}}).on("mouseleave."+t.namespace,function(){if(!f()||t.options.touchDevices){t.mouseIsOverProxy=false}});if(u&&t.options.touchDevices){t.$elProxy.on("touchstart."+t.namespace,function(){t._showNow()})}}else if(t.options.trigger=="click"){t.$elProxy.on("click."+t.namespace,function(){if(!f()||t.options.touchDevices){t._show()}})}}},_show:function(){var e=this;if(e.Status!="shown"&&e.Status!="appearing"){if(e.options.delay){e.timerShow=setTimeout(function(){if(e.options.trigger=="click"||e.options.trigger=="hover"&&e.mouseIsOverProxy){e._showNow()}},e.options.delay)}else e._showNow()}},_showNow:function(n){var r=this;r.options.functionBefore.call(r.$el,r.$el,function(){if(r.enabled&&r.Content!==null){if(n)r.callbacks.show.push(n);r.callbacks.hide=[];clearTimeout(r.timerShow);r.timerShow=null;clearTimeout(r.timerHide);r.timerHide=null;if(r.options.onlyOne){e(".tooltipstered").not(r.$el).each(function(t,n){var r=e(n),i=r.data("tooltipster-ns");e.each(i,function(e,t){var n=r.data(t),i=n.status(),s=n.option("autoClose");if(i!=="hidden"&&i!=="disappearing"&&s){n.hide()}})})}var i=function(){r.Status="shown";e.each(r.callbacks.show,function(e,t){t.call(r.$el)});r.callbacks.show=[]};if(r.Status!=="hidden"){var s=0;if(r.Status==="disappearing"){r.Status="appearing";if(l()){r.$tooltip.clearQueue().removeClass("tooltipster-dying").addClass("tooltipster-"+r.options.animation+"-show");if(r.options.speed>0)r.$tooltip.delay(r.options.speed);r.$tooltip.queue(i)}else{r.$tooltip.stop().fadeIn(i)}}else if(r.Status==="shown"){i()}}else{r.Status="appearing";var s=r.options.speed;r.bodyOverflowX=e("body").css("overflow-x");e("body").css("overflow-x","hidden");var o="tooltipster-"+r.options.animation,a="-webkit-transition-duration: "+r.options.speed+"ms; -webkit-animation-duration: "+r.options.speed+"ms; -moz-transition-duration: "+r.options.speed+"ms; -moz-animation-duration: "+r.options.speed+"ms; -o-transition-duration: "+r.options.speed+"ms; -o-animation-duration: "+r.options.speed+"ms; -ms-transition-duration: "+r.options.speed+"ms; -ms-animation-duration: "+r.options.speed+"ms; transition-duration: "+r.options.speed+"ms; animation-duration: "+r.options.speed+"ms;",f=r.options.minWidth?"min-width:"+Math.round(r.options.minWidth)+"px;":"",c=r.options.maxWidth?"max-width:"+Math.round(r.options.maxWidth)+"px;":"",h=r.options.interactive?"pointer-events: auto;":"";r.$tooltip=e('<div class="tooltipster-base '+r.options.theme+'" style="'+f+" "+c+" "+h+" "+a+'"><div class="tooltipster-content"></div></div>');if(l())r.$tooltip.addClass(o);r._content_insert();r.$tooltip.appendTo("body");r.reposition();r.options.functionReady.call(r.$el,r.$el,r.$tooltip);if(l()){r.$tooltip.addClass(o+"-show");if(r.options.speed>0)r.$tooltip.delay(r.options.speed);r.$tooltip.queue(i)}else{r.$tooltip.css("display","none").fadeIn(r.options.speed,i)}r._interval_set();e(t).on("scroll."+r.namespace+" resize."+r.namespace,function(){r.reposition()});if(r.options.autoClose){e("body").off("."+r.namespace);if(r.options.trigger=="hover"){if(u){setTimeout(function(){e("body").on("touchstart."+r.namespace,function(){r.hide()})},0)}if(r.options.interactive){if(u){r.$tooltip.on("touchstart."+r.namespace,function(e){e.stopPropagation()})}var p=null;r.$elProxy.add(r.$tooltip).on("mouseleave."+r.namespace+"-autoClose",function(){clearTimeout(p);p=setTimeout(function(){r.hide()},r.options.interactiveTolerance)}).on("mouseenter."+r.namespace+"-autoClose",function(){clearTimeout(p)})}else{r.$elProxy.on("mouseleave."+r.namespace+"-autoClose",function(){r.hide()})}if(r.options.hideOnClick){r.$elProxy.on("click."+r.namespace+"-autoClose",function(){r.hide()})}}else if(r.options.trigger=="click"){setTimeout(function(){e("body").on("click."+r.namespace+" touchstart."+r.namespace,function(){r.hide()})},0);if(r.options.interactive){r.$tooltip.on("click."+r.namespace+" touchstart."+r.namespace,function(e){e.stopPropagation()})}}}}if(r.options.timer>0){r.timerHide=setTimeout(function(){r.timerHide=null;r.hide()},r.options.timer+s)}}})},_interval_set:function(){var t=this;t.checkInterval=setInterval(function(){if(e("body").find(t.$el).length===0||e("body").find(t.$elProxy).length===0||t.Status=="hidden"||e("body").find(t.$tooltip).length===0){if(t.Status=="shown"||t.Status=="appearing")t.hide();t._interval_cancel()}else{if(t.options.positionTracker){var n=t._repositionInfo(t.$elProxy),r=false;if(o(n.dimension,t.elProxyPosition.dimension)){if(t.$elProxy.css("position")==="fixed"){if(o(n.position,t.elProxyPosition.position))r=true}else{if(o(n.offset,t.elProxyPosition.offset))r=true}}if(!r){t.reposition();t.options.positionTrackerCallback.call(t,t.$el)}}}},200)},_interval_cancel:function(){clearInterval(this.checkInterval);this.checkInterval=null},_content_set:function(e){if(typeof e==="object"&&e!==null&&this.options.contentCloning){e=e.clone(true)}this.Content=e},_content_insert:function(){var e=this,t=this.$tooltip.find(".tooltipster-content");if(typeof e.Content==="string"&&!e.options.contentAsHTML){t.text(e.Content)}else{t.empty().append(e.Content)}},_update:function(e){var t=this;t._content_set(e);if(t.Content!==null){if(t.Status!=="hidden"){t._content_insert();t.reposition();if(t.options.updateAnimation){if(l()){t.$tooltip.css({width:"","-webkit-transition":"all "+t.options.speed+"ms, width 0ms, height 0ms, left 0ms, top 0ms","-moz-transition":"all "+t.options.speed+"ms, width 0ms, height 0ms, left 0ms, top 0ms","-o-transition":"all "+t.options.speed+"ms, width 0ms, height 0ms, left 0ms, top 0ms","-ms-transition":"all "+t.options.speed+"ms, width 0ms, height 0ms, left 0ms, top 0ms",transition:"all "+t.options.speed+"ms, width 0ms, height 0ms, left 0ms, top 0ms"}).addClass("tooltipster-content-changing");setTimeout(function(){if(t.Status!="hidden"){t.$tooltip.removeClass("tooltipster-content-changing");setTimeout(function(){if(t.Status!=="hidden"){t.$tooltip.css({"-webkit-transition":t.options.speed+"ms","-moz-transition":t.options.speed+"ms","-o-transition":t.options.speed+"ms","-ms-transition":t.options.speed+"ms",transition:t.options.speed+"ms"})}},t.options.speed)}},t.options.speed)}else{t.$tooltip.fadeTo(t.options.speed,.5,function(){if(t.Status!="hidden"){t.$tooltip.fadeTo(t.options.speed,1)}})}}}}else{t.hide()}},_repositionInfo:function(e){return{dimension:{height:e.outerHeight(false),width:e.outerWidth(false)},offset:e.offset(),position:{left:parseInt(e.css("left")),top:parseInt(e.css("top"))}}},hide:function(n){var r=this;if(n)r.callbacks.hide.push(n);r.callbacks.show=[];clearTimeout(r.timerShow);r.timerShow=null;clearTimeout(r.timerHide);r.timerHide=null;var i=function(){e.each(r.callbacks.hide,function(e,t){t.call(r.$el)});r.callbacks.hide=[]};if(r.Status=="shown"||r.Status=="appearing"){r.Status="disappearing";var s=function(){r.Status="hidden";if(typeof r.Content=="object"&&r.Content!==null){r.Content.detach()}r.$tooltip.remove();r.$tooltip=null;e(t).off("."+r.namespace);e("body").off("."+r.namespace).css("overflow-x",r.bodyOverflowX);e("body").off("."+r.namespace);r.$elProxy.off("."+r.namespace+"-autoClose");r.options.functionAfter.call(r.$el,r.$el);i()};if(l()){r.$tooltip.clearQueue().removeClass("tooltipster-"+r.options.animation+"-show").addClass("tooltipster-dying");if(r.options.speed>0)r.$tooltip.delay(r.options.speed);r.$tooltip.queue(s)}else{r.$tooltip.stop().fadeOut(r.options.speed,s)}}else if(r.Status=="hidden"){i()}return r},show:function(e){this._showNow(e);return this},update:function(e){return this.content(e)},content:function(e){if(typeof e==="undefined"){return this.Content}else{this._update(e);return this}},reposition:function(){var n=this;if(e("body").find(n.$tooltip).length!==0){n.$tooltip.css("width","");n.elProxyPosition=n._repositionInfo(n.$elProxy);var r=null,i=e(t).width(),s=n.elProxyPosition,o=n.$tooltip.outerWidth(false),u=n.$tooltip.innerWidth()+1,a=n.$tooltip.outerHeight(false);if(n.$elProxy.is("area")){var f=n.$elProxy.attr("shape"),l=n.$elProxy.parent().attr("name"),c=e('img[usemap="#'+l+'"]'),h=c.offset().left,p=c.offset().top,d=n.$elProxy.attr("coords")!==undefined?n.$elProxy.attr("coords").split(","):undefined;if(f=="circle"){var v=parseInt(d[0]),m=parseInt(d[1]),g=parseInt(d[2]);s.dimension.height=g*2;s.dimension.width=g*2;s.offset.top=p+m-g;s.offset.left=h+v-g}else if(f=="rect"){var v=parseInt(d[0]),m=parseInt(d[1]),y=parseInt(d[2]),b=parseInt(d[3]);s.dimension.height=b-m;s.dimension.width=y-v;s.offset.top=p+m;s.offset.left=h+v}else if(f=="poly"){var w=[],E=[],S=0,x=0,T=0,N=0,C="even";for(var k=0;k<d.length;k++){var L=parseInt(d[k]);if(C=="even"){if(L>T){T=L;if(k===0){S=T}}if(L<S){S=L}C="odd"}else{if(L>N){N=L;if(k==1){x=N}}if(L<x){x=L}C="even"}}s.dimension.height=N-x;s.dimension.width=T-S;s.offset.top=p+x;s.offset.left=h+S}else{s.dimension.height=c.outerHeight(false);s.dimension.width=c.outerWidth(false);s.offset.top=p;s.offset.left=h}}var A=0,O=0,M=0,_=parseInt(n.options.offsetY),D=parseInt(n.options.offsetX),P=n.options.position;function H(){var n=e(t).scrollLeft();if(A-n<0){r=A-n;A=n}if(A+o-n>i){r=A-(i+n-o);A=i+n-o}}function B(n,r){if(s.offset.top-e(t).scrollTop()-a-_-12<0&&r.indexOf("top")>-1){P=n}if(s.offset.top+s.dimension.height+a+12+_>e(t).scrollTop()+e(t).height()&&r.indexOf("bottom")>-1){P=n;M=s.offset.top-a-_-12}}if(P=="top"){var j=s.offset.left+o-(s.offset.left+s.dimension.width);A=s.offset.left+D-j/2;M=s.offset.top-a-_-12;H();B("bottom","top")}if(P=="top-left"){A=s.offset.left+D;M=s.offset.top-a-_-12;H();B("bottom-left","top-left")}if(P=="top-right"){A=s.offset.left+s.dimension.width+D-o;M=s.offset.top-a-_-12;H();B("bottom-right","top-right")}if(P=="bottom"){var j=s.offset.left+o-(s.offset.left+s.dimension.width);A=s.offset.left-j/2+D;M=s.offset.top+s.dimension.height+_+12;H();B("top","bottom")}if(P=="bottom-left"){A=s.offset.left+D;M=s.offset.top+s.dimension.height+_+12;H();B("top-left","bottom-left")}if(P=="bottom-right"){A=s.offset.left+s.dimension.width+D-o;M=s.offset.top+s.dimension.height+_+12;H();B("top-right","bottom-right")}if(P=="left"){A=s.offset.left-D-o-12;O=s.offset.left+D+s.dimension.width+12;var F=s.offset.top+a-(s.offset.top+s.dimension.height);M=s.offset.top-F/2-_;if(A<0&&O+o>i){var I=parseFloat(n.$tooltip.css("border-width"))*2,q=o+A-I;n.$tooltip.css("width",q+"px");a=n.$tooltip.outerHeight(false);A=s.offset.left-D-q-12-I;F=s.offset.top+a-(s.offset.top+s.dimension.height);M=s.offset.top-F/2-_}else if(A<0){A=s.offset.left+D+s.dimension.width+12;r="left"}}if(P=="right"){A=s.offset.left+D+s.dimension.width+12;O=s.offset.left-D-o-12;var F=s.offset.top+a-(s.offset.top+s.dimension.height);M=s.offset.top-F/2-_;if(A+o>i&&O<0){var I=parseFloat(n.$tooltip.css("border-width"))*2,q=i-A-I;n.$tooltip.css("width",q+"px");a=n.$tooltip.outerHeight(false);F=s.offset.top+a-(s.offset.top+s.dimension.height);M=s.offset.top-F/2-_}else if(A+o>i){A=s.offset.left-D-o-12;r="right"}}if(n.options.arrow){var R="tooltipster-arrow-"+P;if(n.options.arrowColor.length<1){var U=n.$tooltip.css("background-color")}else{var U=n.options.arrowColor}if(!r){r=""}else if(r=="left"){R="tooltipster-arrow-right";r=""}else if(r=="right"){R="tooltipster-arrow-left";r=""}else{r="left:"+Math.round(r)+"px;"}if(P=="top"||P=="top-left"||P=="top-right"){var z=parseFloat(n.$tooltip.css("border-bottom-width")),W=n.$tooltip.css("border-bottom-color")}else if(P=="bottom"||P=="bottom-left"||P=="bottom-right"){var z=parseFloat(n.$tooltip.css("border-top-width")),W=n.$tooltip.css("border-top-color")}else if(P=="left"){var z=parseFloat(n.$tooltip.css("border-right-width")),W=n.$tooltip.css("border-right-color")}else if(P=="right"){var z=parseFloat(n.$tooltip.css("border-left-width")),W=n.$tooltip.css("border-left-color")}else{var z=parseFloat(n.$tooltip.css("border-bottom-width")),W=n.$tooltip.css("border-bottom-color")}if(z>1){z++}var X="";if(z!==0){var V="",J="border-color: "+W+";";if(R.indexOf("bottom")!==-1){V="margin-top: -"+Math.round(z)+"px;"}else if(R.indexOf("top")!==-1){V="margin-bottom: -"+Math.round(z)+"px;"}else if(R.indexOf("left")!==-1){V="margin-right: -"+Math.round(z)+"px;"}else if(R.indexOf("right")!==-1){V="margin-left: -"+Math.round(z)+"px;"}X='<span class="tooltipster-arrow-border" style="'+V+" "+J+';"></span>'}n.$tooltip.find(".tooltipster-arrow").remove();var K='<div class="'+R+' tooltipster-arrow" style="'+r+'">'+X+'<span style="border-color:'+U+';"></span></div>';n.$tooltip.append(K)}n.$tooltip.css({top:Math.round(M)+"px",left:Math.round(A)+"px"})}return n},enable:function(){this.enabled=true;return this},disable:function(){this.hide();this.enabled=false;return this},destroy:function(){var t=this;t.hide();if(t.$el[0]!==t.$elProxy[0]){t.$elProxy.remove()}t.$el.removeData(t.namespace).off("."+t.namespace);var n=t.$el.data("tooltipster-ns");if(n.length===1){var r=null;if(t.options.restoration==="previous"){r=t.$el.data("tooltipster-initialTitle")}else if(t.options.restoration==="current"){r=typeof t.Content==="string"?t.Content:e("<div></div>").append(t.Content).html()}if(r){t.$el.attr("title",r)}t.$el.removeClass("tooltipstered").removeData("tooltipster-ns").removeData("tooltipster-initialTitle")}else{n=e.grep(n,function(e,n){return e!==t.namespace});t.$el.data("tooltipster-ns",n)}return t},elementIcon:function(){return this.$el[0]!==this.$elProxy[0]?this.$elProxy[0]:undefined},elementTooltip:function(){return this.$tooltip?this.$tooltip[0]:undefined},option:function(e,t){if(typeof t=="undefined")return this.options[e];else{this.options[e]=t;return this}},status:function(){return this.Status}};e.fn[r]=function(){var t=arguments;if(this.length===0){if(typeof t[0]==="string"){var n=true;switch(t[0]){case"setDefaults":e.extend(i,t[1]);break;default:n=false;break}if(n)return true;else return this}else{return this}}else{if(typeof t[0]==="string"){var r="#*$~&";this.each(function(){var n=e(this).data("tooltipster-ns"),i=n?e(this).data(n[0]):null;if(i){if(typeof i[t[0]]==="function"){var s=i[t[0]](t[1],t[2])}else{throw new Error('Unknown method .tooltipster("'+t[0]+'")')}if(s!==i){r=s;return false}}else{throw new Error("You called Tooltipster's \""+t[0]+'" method on an uninitialized element')}});return r!=="#*$~&"?r:this}else{var o=[],u=t[0]&&typeof t[0].multiple!=="undefined",a=u&&t[0].multiple||!u&&i.multiple,f=t[0]&&typeof t[0].debug!=="undefined",l=f&&t[0].debug||!f&&i.debug;this.each(function(){var n=false,r=e(this).data("tooltipster-ns"),i=null;if(!r){n=true}else if(a){n=true}else if(l){console.log('Tooltipster: one or more tooltips are already attached to this element: ignoring. Use the "multiple" option to attach more tooltips.')}if(n){i=new s(this,t[0]);if(!r)r=[];r.push(i.namespace);e(this).data("tooltipster-ns",r);e(this).data(i.namespace,i)}o.push(i)});if(a)return o;else return this}}};var u=!!("ontouchstart"in t);var a=false;e("body").one("mousemove",function(){a=true})})(jQuery,window,document);

// fin jq tooltipster 


// make it a global variable so other scripts can access it
var booked_load_calendar_date_booking_options,
	booked_appt_form_options,
	bookedNewAppointment;

;(function($, window, document, undefined) {
	
	var $win = $(window);

	$.fn.spin.presets.booked = {
	 	lines: 10, // The number of lines to draw
		length: 7, // The length of each line
		width: 5, // The line thickness
		radius: 11, // The radius of the inner circle
		corners: 1, // Corner roundness (0..1)
		rotate: 0, // The rotation offset
		direction: 1, // 1: clockwise, -1: counterclockwise
		color: '#555', // #rgb or #rrggbb or array of colors
		speed: 1, // Rounds per second
		trail: 60, // Afterglow percentage
		shadow: false, // Whether to render a shadow
		hwaccel: false, // Whether to use hardware acceleration
		className: 'booked-spinner', // The CSS class to assign to the spinner
		zIndex: 2e9, // The z-index (defaults to 2000000000)
		top: '50%', // Top position relative to parent
		left: '50%' // Left position relative to parent
	}
	
	$.fn.spin.presets.booked_top = {
	 	lines: 11, // The number of lines to draw
		length: 10, // The length of each line
		width: 6, // The line thickness
		radius: 15, // The radius of the inner circle
		corners: 1, // Corner roundness (0..1)
		rotate: 0, // The rotation offset
		scale: 0.5,
		direction: 1, // 1: clockwise, -1: counterclockwise
		color: '#aaaaaa', // #rgb or #rrggbb or array of colors
		speed: 1, // Rounds per second
		trail: 60, // Afterglow percentage
		shadow: false, // Whether to render a shadow
		hwaccel: false, // Whether to use hardware acceleration
		className: 'booked-spinner booked-spinner-top', // The CSS class to assign to the spinner
		zIndex: 2e9, // The z-index (defaults to 2000000000)
		top: '15px', // Top position relative to parent
		left: '50%' // Left position relative to parent
	}
	
	$.fn.spin.presets.booked_white = {
	 	lines: 13, // The number of lines to draw
		length: 11, // The length of each line
		width: 5, // The line thickness
		radius: 18, // The radius of the inner circle
		scale: 1,
		corners: 1, // Corner roundness (0..1)
		rotate: 0, // The rotation offset
		direction: 1, // 1: clockwise, -1: counterclockwise
		color: '#fff', // #rgb or #rrggbb or array of colors
		speed: 1, // Rounds per second
		trail: 60, // Afterglow percentage
		shadow: false, // Whether to render a shadow
		hwaccel: false, // Whether to use hardware acceleration
		className: 'booked-spinner booked-white', // The CSS class to assign to the spinner
		zIndex: 2e9, // The z-index (defaults to 2000000000)
		top: '50%', // Top position relative to parent
		left: '50%' // Left position relative to parent
	}
	
	// Adjust the calendar sizing when resizing the window
	$win.on('resize', function(){
		
		adjust_calendar_boxes();
		resize_booked_modal();

	});

	$win.on('load', function() {

		BookedTabs.Init();

		var ajaxRequests = [];
		
		// Adjust the calendar sizing on load
		adjust_calendar_boxes();
		
		$('.booked-calendar-wrap').each(function(){
			var thisCalendar = $(this);
			var calendar_month = thisCalendar.find('table.booked-calendar').attr('data-calendar-date');
			thisCalendar.attr('data-default',calendar_month);
			init_tooltips(thisCalendar);
		});
		
		$('.booked-list-view').each(function(){
			var thisList = $(this);
			var list_date = thisList.find('.booked-appt-list').attr('data-list-date');
			thisList.attr('data-default',list_date);
		});
		
		bookedRemoveEmptyTRs();
		init_appt_list_date_picker();

		$('.booked_calendar_chooser').change(function(e){
	
			e.preventDefault();
			
			var $selector 			= $(this),
				thisIsCalendar		= $selector.parents('.booked-calendarSwitcher').hasClass('calendar');	
	
			if (!thisIsCalendar){
				
				var thisCalendarWrap	= $selector.parents('.booked-calendar-shortcode-wrap').find('.booked-list-view'),
				thisDefaultDate			= thisCalendarWrap.attr('data-default'),
				thisIsCalendar			= $selector.parents('.booked-calendarSwitcher').hasClass('calendar');
				
				if (typeof thisDefaultDate == 'undefined'){ thisDefaultDate = false; }
				
				thisCalendarWrap.addClass('booked-loading');
	
				var args = {
					'action'		: 'booked_appointment_list_date',
					'date'		: thisDefaultDate,
					'calendar_id'	: $selector.val()
				};
				
				$(document).trigger("booked-before-loading-appointment-list-booking-options");
				thisCalendarWrap.spin('booked_top');
			
				$.ajax({
					url: booked_js_vars.ajax_url,
					type: 'post',
					data: args,
					success: function( html ) {
						
						thisCalendarWrap.html( html );
						
						init_appt_list_date_picker();
						setTimeout(function(){
							thisCalendarWrap.removeClass('booked-loading');
						},1);
						
					}
				});
				
			} else {
				
				var thisCalendarWrap 	= $selector.parents('.booked-calendar-shortcode-wrap').find('.booked-calendar-wrap'),
				thisDefaultDate			= thisCalendarWrap.attr('data-default');
				if (typeof thisDefaultDate == 'undefined'){ thisDefaultDate = false; }
				
				var args = {
					'action'		: 'booked_calendar_month',
					'gotoMonth'		: thisDefaultDate,
					'calendar_id'	: $selector.val()
				};
			
				savingState(true,thisCalendarWrap);
			
				$.ajax({
					url: booked_js_vars.ajax_url,
					type: 'post',
					data: args,
					success: function( html ) {
						
						thisCalendarWrap.html( html );
						
						adjust_calendar_boxes();
						bookedRemoveEmptyTRs();
						init_tooltips(thisCalendarWrap);
					 	$(window).trigger('booked-load-calendar', args, $selector );
						
					}
				});
				
			}
			
			return false;
			
		});
		
		// Calendar Next/Prev Click
		$('.booked-calendar-wrap').on('click', '.page-right, .page-left, .monthName a', function(e) {

			e.preventDefault();

			var $button 			= $(this),
				gotoMonth			= $button.attr('data-goto'),
				thisCalendarWrap 	= $button.parents('.booked-calendar-wrap'),
				thisCalendarDefault = thisCalendarWrap.attr('data-default'),
				calendar_id			= $button.parents('table.booked-calendar').attr('data-calendar-id');
				
			if (typeof thisCalendarDefault == 'undefined'){ thisCalendarDefault = false; }
			
			var args = {
				'action'		: 'booked_calendar_month',
				'gotoMonth'		: gotoMonth,
				'calendar_id'	: calendar_id,
				'force_default'	: thisCalendarDefault
			};
		
			savingState(true,thisCalendarWrap);
		
			$.ajax({
				url: booked_js_vars.ajax_url,
				type: 'post',
				data: args,
				success: function( html ) {
					
					thisCalendarWrap.html( html );
					
					adjust_calendar_boxes();
					bookedRemoveEmptyTRs();
					init_tooltips(thisCalendarWrap);
					$(window).trigger('booked-load-calendar', args, $button );
					
				}
			});

			return false;

		});

		// Calendar Date Click
		$('.booked-calendar-wrap').on('click', 'tr.week td', function(e) {

			e.preventDefault();

			var $thisDate 				= $(this),
				booked_calendar_table 	= $thisDate.parents('table.booked-calendar'),
				$thisRow				= $thisDate.parent(),
				date					= $thisDate.attr('data-date'),
				calendar_id				= booked_calendar_table.attr('data-calendar-id'),
				colspanSetting			= $thisRow.find('td').length;
				
			if (!calendar_id){ calendar_id = 0; }

			if ($thisDate.hasClass('blur') || $thisDate.hasClass('booked') && !booked_js_vars.publicAppointments || $thisDate.hasClass('prev-date')){

				// Do nothing.

			} else if ($thisDate.hasClass('active')){

				$thisDate.removeClass('active');
				$('tr.entryBlock').remove();
				
				var calendarHeight = booked_calendar_table.height();
				booked_calendar_table.parent().height(calendarHeight);

			} else {

				$('tr.week td').removeClass('active');
				$thisDate.addClass('active');

				$('tr.entryBlock').remove();
				$thisRow.after('<tr class="entryBlock loading"><td colspan="'+colspanSetting+'"></td></tr>');
				$('tr.entryBlock').find('td').spin('booked');

				booked_load_calendar_date_booking_options = {'action':'booked_calendar_date','date':date,'calendar_id':calendar_id};
				$(document).trigger("booked-before-loading-calendar-booking-options");
				
				var calendarHeight = booked_calendar_table.height();
				booked_calendar_table.parent().height(calendarHeight);
				
				$.ajax({
					url: booked_js_vars.ajax_url,
					type: 'post',
					data: booked_load_calendar_date_booking_options,
					success: function( html ) {
						
						$('tr.entryBlock').find('td').html( html );
						
						$('tr.entryBlock').removeClass('loading');
						$('tr.entryBlock').find('.booked-appt-list').fadeIn(300);
						$('tr.entryBlock').find('.booked-appt-list').addClass('shown');
						adjust_calendar_boxes();
						
					}
				});

			}
			
			return;

		});
		
		// Appointment List Next/Prev Date Click
		$('.booked-list-view').on('click', '.booked-list-view-date-prev,.booked-list-view-date-next', function(e) {

			e.preventDefault();

			var $thisLink 			= $(this),
				date				= $thisLink.attr('data-date'),
				thisList			= $thisLink.parents('.booked-list-view'),
				defaultDate			= thisList.attr('data-default'),
				calendar_id			= $thisLink.parents('.booked-list-view-nav').attr('data-calendar-id');
				
			if (typeof defaultDate == 'undefined'){ defaultDate = false; }
				
			if (!calendar_id){ calendar_id = 0; }
			
			thisList.addClass('booked-loading');
	
			var booked_load_list_view_date_booking_options = {
				'action'		: 'booked_appointment_list_date',
				'date'			: date,
				'calendar_id'	: calendar_id,
				'force_default'	: defaultDate
			};
			
			$(document).trigger("booked-before-loading-appointment-list-booking-options");
			thisList.spin('booked_top');
		
			$.ajax({
				url: booked_js_vars.ajax_url,
				type: 'post',
				data: booked_load_list_view_date_booking_options,
				success: function( html ) {
					
					thisList.html( html );
					
					init_appt_list_date_picker();
					setTimeout(function(){
						thisList.removeClass('booked-loading');
					},1);
					
				}
			});
			
			return false;

		});

		// New Appointment Click
		bookedNewAppointment = function(e) {
			e.preventDefault();

			var $button 		= $(this),
				title           = $button.attr('data-title'),
				timeslot		= $button.attr('data-timeslot'),
				date			= $button.attr('data-date'),
				calendar_id		= $button.attr('data-calendar-id'),
				$thisTimeslot	= $button.parents('.timeslot'),
				is_list_view	= $button.parents('.booked-calendar-wrap').hasClass('booked-list-view');
				
			if (typeof is_list_view != 'undefined' && is_list_view){
				var new_calendar_id	= $button.parents('.booked-list-view').find('.booked-list-view-nav').attr('data-calendar-id');
			} else {
				var new_calendar_id	= $button.parents('table.booked-calendar').attr('data-calendar-id');
			}
			calendar_id = new_calendar_id ? new_calendar_id : calendar_id;

			booked_appt_form_options = {'action':'booked_new_appointment_form','date':date,'timeslot':timeslot,'calendar_id':calendar_id,'title':title};
			$(document).trigger("booked-before-loading-booking-form");

			create_booked_modal();
			setTimeout(function(){
				
				$.ajax({
					url: booked_js_vars.ajax_url,
					type: 'post',
					data: booked_appt_form_options,
					success: function( html ) {
						
						$('.bm-window').html( html );
						
						var bookedModal = $('.booked-modal');
						var bmWindow = bookedModal.find('.bm-window');
						bmWindow.css({'visibility':'hidden'});
						bookedModal.removeClass('bm-loading');
						$(document).trigger("booked-on-new-app");
						resize_booked_modal();
						bmWindow.hide();
						$('.booked-modal .bm-overlay').find('.booked-spinner').remove();
						
						setTimeout(function(){
							bmWindow.css({'visibility':'visible'});
							bmWindow.show();
						},50);
						
					}
				});
			
			},100);

			return false;
		}
		$('.booked-calendar-wrap').on('click', 'button.new-appt', bookedNewAppointment);

		// Profile Tabs
		var profileTabs = $('.booked-tabs');

		if (!profileTabs.find('li.active').length){
			profileTabs.find('li:first-child').addClass("active");
		}

		if (profileTabs.length){
			$('.booked-tab-content').hide();
			var activeTab = profileTabs.find('.active > a').attr('href');
			activeTab = activeTab.split('#');
			activeTab = activeTab[1];
			$('#profile-'+activeTab).show();

			profileTabs.find('li > a').on('click', function(e) {

				e.preventDefault();
				$('.booked-tab-content').hide();
				profileTabs.find('li').removeClass('active');

				$(this).parent().addClass('active');
				var activeTab = $(this).attr('href');
				activeTab = activeTab.split('#');
				activeTab = activeTab[1];

				$('#profile-'+activeTab).show();
				return false;

			});
		}

		// Show Additional Information
		$('.booked-profile-appt-list').on('click', '.booked-show-cf', function(e) {
		
			e.preventDefault();
			var hiddenBlock = $(this).parent().find('.cf-meta-values-hidden');
		
			if(hiddenBlock.is(':visible')){
				hiddenBlock.hide();
				$(this).removeClass('booked-cf-active');
			} else {
				hiddenBlock.show();
				$(this).addClass('booked-cf-active');
			}
		
			return false;
		
		});

		// Check Login/Registration/Forgot Password forms before Submitting
		if ($('#loginform').length){
			$('#loginform input[type="submit"]').on('click',function(e) {
				if ($('#loginform input[name="log"]').val() && $('#loginform input[name="pwd"]').val()){
					$('#loginform .booked-custom-error').hide();
				} else {
					e.preventDefault();
					$('#loginform').parents('.booked-form-wrap').find('.booked-custom-error').fadeOut(200).fadeIn(200);
				}
			});
		}

		if ($('#profile-forgot').length){
			$('#profile-forgot input[type="submit"]').on('click',function(e) {
				if ($('#profile-forgot input[name="user_login"]').val()){
					$('#profile-forgot .booked-custom-error').hide();
				} else {
					e.preventDefault();
					$('#profile-forgot').find('.booked-custom-error').fadeOut(200).fadeIn(200);
				}
			});
		}

		// Custom Upload Field
		if ($('.booked-upload-wrap').length){

			$('.booked-upload-wrap input[type=file]').on('change',function(){

				var fileName = $(this).val();
				$(this).parent().find('span').html(fileName);
				$(this).parent().addClass('hasFile');

			});

		}

		// Delete Appointment
		$('.booked-profile-appt-list').on('click', '.appt-block .cancel', function(e) {

			e.preventDefault();

			var $button 		= $(this),
				$thisParent		= $button.parents('.appt-block'),
				appt_id			= $thisParent.attr('data-appt-id');

			confirm_delete = confirm(booked_js_vars.i18n_confirm_appt_delete);
			if (confirm_delete == true){

				var currentApptCount = parseInt($('.booked-profile-appt-list').find('h4').find('span.count').html());
				currentApptCount = parseInt(currentApptCount - 1);
				if (currentApptCount < 1){
					$('.booked-profile-appt-list').find('h4').find('span.count').html('0');
					$('.no-appts-message').slideDown('fast');
				} else {
					$('.booked-profile-appt-list').find('h4').find('span.count').html(currentApptCount);
				}
				
				$('.appt-block').animate({'opacity':0.4},0);

	  			$thisParent.slideUp('fast',function(){
					$(this).remove();
				});

				$.ajax({
					'url' 		: booked_js_vars.ajax_url,
					'method' 	: 'post',
					'data'		: {
						'action'     	: 'booked_cancel_appt',
						'appt_id'     	: appt_id
					},
					success: function(data) {
						$('.appt-block').animate({'opacity':1},150);
					}
				});

			}

			return false;

		});

		$('body').on('touchstart click','.bm-overlay, .bm-window .close, .booked-form .cancel',function(e){
			e.preventDefault();
			close_booked_modal();
			return false;
		});

		$('body')
		.on('focusin', '.booked-form input', function() {
			if(this.title==this.value) {
				$(this).addClass('hasContent');
				this.value = '';
			}
		}).on('focusout', '.booked-form input', function(){
			if(this.value==='') {
				$(this).removeClass('hasContent');
				this.value = this.title;
			}
		});

		$('body').on('change','.booked-form input',function(){

			var condition = $(this).attr('data-condition'),
				thisVal = $(this).val();

			if (condition && $('.condition-block').length) {
				$('.condition-block.'+condition).hide();
				$('#condition-'+thisVal).fadeIn(200);
				resize_booked_modal();
			}

		});

		// Perform AJAX login on form submit
	    $('body').on('submit','form#ajaxlogin', function(e){
		    e.preventDefault();

	        $('form#ajaxlogin p.status').show().html('<i class="fa fa-refresh fa-spin"></i>&nbsp;&nbsp;&nbsp;' + booked_js_vars.i18n_please_wait);
	        resize_booked_modal();

	        var $this = $(this),
	        	date = $this.data('date'),
	        	title = $this.data('title'),
	        	timeslot = $this.data('timeslot'),
	        	calendar_id = $this.data('calendar-id');

	        $.ajax({
		        type	: 'post',
				url 	: booked_js_vars.ajax_url,
				data	: $('form#ajaxlogin').serialize(),
				success	: function(data) {
					if (data == 'success'){

						// close the modal box
						close_booked_modal();

						// reopen the modal box
						var $button = $( '<button data-title="' + title + '" data-timeslot="' + timeslot + '" data-date="' + date + '" data-calendar-id="' + calendar_id + '"></button>' );
						$button.on( 'click', window.bookedNewAppointment );
						$button.triggerHandler( 'click' );
						$button.unbind( 'click', window.bookedNewAppointment );
						$button.detach();
						
					} else {
						$('form#ajaxlogin p.status').show().html('<i class="fa fa-warning" style="color:#E35656"></i>&nbsp;&nbsp;&nbsp;' + booked_js_vars.i18n_wrong_username_pass);
						resize_booked_modal();
					}
	            }
	        });
	        e.preventDefault();
	    });
	    
	    $('body').on('click','.booked-forgot-password',function(e){
			
			e.preventDefault();
			$('#ajaxlogin').hide();
			$('#ajaxforgot').show();
			
			resize_booked_modal();
			 
	    });
	    
	     $('body').on('click','.booked-forgot-goback',function(e){
			
			e.preventDefault();
			$('#ajaxlogin').show();
			$('#ajaxforgot').hide();
			
			resize_booked_modal();
			 
	    });
	    
	    // Perform AJAX login on form submit

	    $('body').on('submit','form#ajaxforgot', function(e){
		    e.preventDefault();

	        $('form#ajaxforgot p.status').show().html('<i class="fa fa-refresh fa-spin"></i>&nbsp;&nbsp;&nbsp;' + booked_js_vars.i18n_please_wait);
	        resize_booked_modal();

	        var $this = $(this);

	        $.ajax({
		        type	: 'post',
				url 	: booked_js_vars.ajax_url,
				data	: $('form#ajaxforgot').serialize(),
				success	: function(data) {
					if (data == 'success'){

						e.preventDefault();
						$('#ajaxlogin').show();
						$('#ajaxforgot').hide();
						
						$('form#ajaxlogin p.status').show().html('<i class="fa fa-check" style="color:#56c477"></i>&nbsp;&nbsp;&nbsp;' + booked_js_vars.i18n_password_reset);
						resize_booked_modal();
						
					} else {
						
						console.log(data);
						$('form#ajaxforgot p.status').show().html('<i class="fa fa-warning" style="color:#E35656"></i>&nbsp;&nbsp;&nbsp;' + booked_js_vars.i18n_password_reset_error);
						resize_booked_modal();
						
					}
	            }
	        });
	        e.preventDefault();
	    });


		// Submit the "Request Appointment" Form
		$('body').on('click','.booked-form input#submit-request-appointment',function(e){
			
			$('form#newAppointmentForm p.status').show().html('<i class="fa fa-refresh fa-spin"></i>&nbsp;&nbsp;&nbsp;' + booked_js_vars.i18n_please_wait);
	        resize_booked_modal();
			
			e.preventDefault();
			
			var customerType        = $('#newAppointmentForm input[name=customer_type]').val(),
				customerID          = $('#newAppointmentForm input[name=user_id]').val(),
				name                = $('#newAppointmentForm input[name=booked_appt_name]').val(),
				surname             = $('#newAppointmentForm input[name=booked_appt_surname]').val(),
				surnameActive		= $('#newAppointmentForm input[name=booked_appt_surname]').length,
				guest_name          = $('#newAppointmentForm input[name=guest_name]').val(),
				guest_surname      	= $('#newAppointmentForm input[name=guest_surname]').val(),
				guest_surnameActive = $('#newAppointmentForm input[name=guest_surname]').length,
				guest_email			= $('#newAppointmentForm input[name=guest_email]').val(),
				guest_emailActive 	= $('#newAppointmentForm input[name=guest_email]').length,
				email               = $('#newAppointmentForm input[name=booked_appt_email]').val(),
				password            = $('#newAppointmentForm input[name=booked_appt_password]').val(),
				showRequiredError   = false,
				ajaxRequests        = [];

			$(this).parents('.booked-form').find('input,textarea,select').each(function(i,field){

				var required = $(this).attr('required');

				if (required && $(field).attr('type') == 'hidden'){
					var fieldParts = $(field).attr('name');
					fieldParts = fieldParts.split('---');
					fieldName = fieldParts[0];
					fieldNumber = fieldParts[1].split('___');
					fieldNumber = fieldNumber[0];

					if (fieldName == 'radio-buttons-label'){
						var radioValue = false;
						$('input:radio[name="single-radio-button---'+fieldNumber+'[]"]:checked').each(function(){
							if ($(this).val()){
								radioValue = $(this).val();
							}
						});
						if (!radioValue){
							showRequiredError = true;
						}
					} else if (fieldName == 'checkboxes-label'){
						var checkboxValue = false;
						$('input:checkbox[name="single-checkbox---'+fieldNumber+'[]"]:checked').each(function(){
							if ($(this).val()){
								checkboxValue = $(this).val();
							}
						});
						if (!checkboxValue){
							showRequiredError = true;
						}
					}

				} else if (required && $(field).attr('type') != 'hidden' && $(field).val() == ''){
		            showRequiredError = true;
		        }

		    });

		    if (showRequiredError) {
			    $('form#newAppointmentForm p.status').show().html('<i class="fa fa-warning" style="color:#E35656"></i>&nbsp;&nbsp;&nbsp;' + booked_js_vars.i18n_fill_out_required_fields);
				resize_booked_modal();
			    return false;
		    }

			if ( customerType == 'new' && !name || customerType == 'new' && surnameActive && !surname || customerType == 'new' && !email || customerType == 'new' && !password ) {
				$('form#newAppointmentForm p.status').show().html('<i class="fa fa-warning" style="color:#E35656"></i>&nbsp;&nbsp;&nbsp;' + booked_js_vars.i18n_appt_required_fields);
				resize_booked_modal();
				return false;
			}
			
			if ( customerType == 'guest' && !guest_name || customerType == 'guest' && guest_emailActive && !guest_email || customerType == 'guest' && guest_surnameActive && !guest_surname ){
				$('form#newAppointmentForm p.status').show().html('<i class="fa fa-warning" style="color:#E35656"></i>&nbsp;&nbsp;&nbsp;' + booked_js_vars.i18n_appt_required_fields_guest);
				resize_booked_modal();
				return false;
			}
			
			if (customerType == 'current' && customerID ||
				customerType == 'guest' && guest_name && !guest_surnameActive && !guest_emailActive ||
				customerType == 'guest' && guest_name && guest_surnameActive && guest_surname && !guest_emailActive ||
				customerType == 'guest' && guest_name && guest_emailActive && guest_email && !guest_surnameActive ||
				customerType == 'guest' && guest_name && guest_emailActive && guest_email && guest_surnameActive && guest_surname ) {
			   
			    SubmitRequestAppointment.currentUserOrGuest();
			
			}

			if (customerType == 'new' && name && email && password) {
				if ( !surnameActive || surnameActive && surname ){
					SubmitRequestAppointment.newUser();
				}
			}

		});

		var SubmitRequestAppointment = {

			formSelector: '#newAppointmentForm',
			formBtnRequestSelector: '.booked-form input#submit-request-appointment',
			formStatusSelector: 'p.status',
			formSubmitBtnSelector: '#submit-request-appointment',

			apptContainerSelector: '.booked-appointment-details',

			baseFields: 	[ 'guest_name','guest_surname','guest_email','action', 'customer_type', 'user_id' ],
			apptFields: 	[ 'appoinment', 'calendar_id', 'title', 'date', 'timestamp', 'timeslot' ],
			userFields: 	[ 'booked_appt_name','booked_appt_surname','booked_appt_email', 'booked_appt_password' ],
			captchaFields: 	[ 'captcha_word', 'captcha_code' ],

			currentApptIndex: false,
			currentApptCounter: false,
			hasAnyErrors: false,

			currentUserOrGuest: function() {
				var total_appts = SubmitRequestAppointment._totalAppts();

				if ( ! total_appts ) {
					return;
				}

				SubmitRequestAppointment._showLoadingMessage();
				SubmitRequestAppointment._resetDefaultValues();

				var data = SubmitRequestAppointment._getBaseData();

				SubmitRequestAppointment.currentApptIndex = 0;
				SubmitRequestAppointment.currentApptCounter = 1;
				SubmitRequestAppointment._doRequestAppointment( data, total_appts );
				
			},

			// pretty much the same as SubmitRequestAppointment.currentUserOrGuest(), however, it include the user name, email and password
			newUser: function() {
				var total_appts = SubmitRequestAppointment._totalAppts();

				if ( ! total_appts ) {
					return;
				}

				SubmitRequestAppointment._showLoadingMessage();
				SubmitRequestAppointment._resetDefaultValues();

				var data = SubmitRequestAppointment._getBaseData();

				// when there are more than one appointment, we need to make the registration request first and then loop the appointments
				if ( total_appts > 1 ) {
					var data_obj_with_no_reference = null;
					data_obj_with_no_reference = $.extend( true, {}, data );
					data_obj_with_no_reference = SubmitRequestAppointment._addUserRegistrationData( data_obj_with_no_reference );
					SubmitRequestAppointment._requestUserRegistration( data_obj_with_no_reference );

					data.customer_type = 'current';
				} else {
					// add user registration fields values
					data = SubmitRequestAppointment._addUserRegistrationData( data );
				}

				SubmitRequestAppointment.currentApptIndex = 0;
				SubmitRequestAppointment._doRequestAppointment( data, total_appts );
			},

			_doRequestAppointment: function( data, total_appts ) {
				
				var appt_fields = SubmitRequestAppointment.apptFields;

				// for the first item only
				if ( SubmitRequestAppointment.currentApptIndex === 0 ) {
					SubmitRequestAppointment._hideCancelBtn();
					SubmitRequestAppointment._disableSubmitBtn();
					SubmitRequestAppointment.hasAnyErrors = false;
				}
				// <------end

				var data_obj_with_no_reference = $.extend( true, {}, data );

				// add the appointment fields to the data
				for (var i = 0; i < appt_fields.length; i++) {
					data_obj_with_no_reference[ appt_fields[i] ] = SubmitRequestAppointment._getFieldVal( appt_fields[i], SubmitRequestAppointment.currentApptIndex );
				}

				var calendar_id = SubmitRequestAppointment._getFieldVal( 'calendar_id', SubmitRequestAppointment.currentApptIndex );
				data_obj_with_no_reference = SubmitRequestAppointment._addCustomFieldsData( data_obj_with_no_reference, calendar_id );

				var $appt = SubmitRequestAppointment._getApptElement( SubmitRequestAppointment.currentApptIndex );

				if ( ! $appt.hasClass('skip') ) {
					$.ajax({
						type    : 'post',
						url     : booked_js_vars.ajax_url,
						data    : data_obj_with_no_reference,
						async   : true,
						success	: function( response ) {
							
							SubmitRequestAppointment._requestAppointmentResponseHandler( response );
							SubmitRequestAppointment.currentApptIndex++;

							setTimeout( function() {
								if ( SubmitRequestAppointment.currentApptCounter === total_appts ) {
									// for the last item only
									if ( ! SubmitRequestAppointment.hasAnyErrors ) {
										SubmitRequestAppointment._onAfterRequestAppointment();
									} else {
										SubmitRequestAppointment._enableSubmitBtn();
										SubmitRequestAppointment._showCancelBtn();
									}
									// <------end
								} else {
									SubmitRequestAppointment.currentApptCounter++;
									SubmitRequestAppointment._doRequestAppointment( data, total_appts );
								}
							}, 100 );
						}
					});
				} else {
					SubmitRequestAppointment.currentApptIndex++;
					SubmitRequestAppointment.currentApptCounter++;
					SubmitRequestAppointment._doRequestAppointment( data, total_appts, SubmitRequestAppointment.currentApptIndex );
				}
			},

			_totalAppts: function() {
				return $(SubmitRequestAppointment.formSelector + ' input[name="appoinment[]"]').length;
			},

			_getBaseData: function() {
				var data = {},
					fields = SubmitRequestAppointment.baseFields;

				// set up the base form data
				for ( var i = 0; i < fields.length; i++ ) {
					data[ fields[i] ] = SubmitRequestAppointment._getFieldVal( fields[i] );
				}

				data['is_fe_form'] = true;
				data['total_appts'] = SubmitRequestAppointment._totalAppts();

				return data;
			},

			_getFieldVal: function( field_name, field_index ) {
				var field_name = typeof field_name === 'undefined' ? '' : field_name,
					field_index = typeof field_index === 'undefined' ? false : field_index,
					selector = SubmitRequestAppointment.formSelector + ' ';
					
				if ( field_index === false ) {
					selector += ' [name=' + field_name + ']';
					return $( selector ).val();
				}

				selector += ' [name="' + field_name + '[]"]';
				return $( selector ).eq( field_index ).val();
			},

			_resetDefaultValues: function() {
				 $('.booked-form input').each(function(){
					var thisVal = $(this).val(),
						thisDefault = $(this).attr('title');

					if ( thisDefault == thisVal ){ 
						$(this).val(''); 
					}
				});
			},

			_resetToDefaultValues: function() {
				$('.booked-form input').each(function(){
					var thisVal = $(this).val(),
						thisDefault = $(this).attr('title');

					if ( ! thisVal ){ 
						$(this).val( thisDefault ); 
					}
				});
			},

			_addUserRegistrationData: function( data ) {
				// populate the user data
				$.each( SubmitRequestAppointment.userFields, function( index, field_name ) {
					data[ field_name ] = SubmitRequestAppointment._getFieldVal( field_name );
				} );

				// populate captcha data if available
				$.each( SubmitRequestAppointment.captchaFields, function( index, field_name ) {
					var field_value = SubmitRequestAppointment._getFieldVal( field_name );

					if ( ! field_value ) {
						return;
					}

					data[ field_name ] = field_value;
				} );

				return data;
			},

			_addCustomFieldsData: function( data, calendar_id ) {
				var custom_fields_data = $('.cf-block [name]')
					.filter( function( index ) {
						var $this = $(this);
						return parseInt($this.data('calendar-id')) === parseInt(calendar_id) && $this.attr('name').match(/---\d+/g);
					} )
					.each( function( index ) {
						var $this = $(this),
							name = $this.attr('name'),
							value = $this.val(),
							type = $this.attr('type');

						if ( ! value ) {
							return;
						}

						if ( ! name.match(/checkbox|radio+/g) ) {
							data[ name ] = value;
							return;
						}

						if ( name.match(/radio+/g) && $this.is(':checked') ) {
							data[ name ] = value;
							return;
						}

						if ( typeof data[ name ] === 'undefined' || data[ name ].constructor !== Array ) {
							data[ name ] = [];
						}

						if ( ! $this.is(':checked') ) {
							return;
						}

						data[ name ].push( value );
					} );

				return data;
			},

			_requestUserRegistration: function( base_data, appt_index ) {
				$.ajax({
					type    : 'post',
					url     : booked_js_vars.ajax_url,
					data    : base_data,
					async   : false,
					success	: function( response ) {
						SubmitRequestAppointment._requestUserRegistrationResponseHandler( response );
					}
				});
			},

			_requestUserRegistrationResponseHandler: function( response ) {
				var response_parts = response.split('###'),
					data_result = response_parts[0].substr( response_parts[0].length - 5 );

				if ( data_result === 'error' ) {
					// do something on registration failure
					return;
				}

				// do something on successful registration
			},

			_requestAppointment: function( response ) {
				SubmitRequestAppointment._requestAppointmentResponseHandler( response );
			},

			_requestAppointmentResponseHandler: function( response ) {
				var response_parts = response.split('###'),
					data_result = response_parts[0].substr( response_parts[0].length - 5 );

				if ( data_result === 'error' ) {
					SubmitRequestAppointment._requestAppointmentOnError( response_parts );
					return;
				}

				SubmitRequestAppointment._requestAppointmentOnSuccess( response_parts );
			},

			_requestAppointmentOnError: function( response_parts ) {
				var $apptEl = SubmitRequestAppointment._getApptElement();

				$(document).trigger("booked-on-requested-appt-error",[$apptEl]);

				SubmitRequestAppointment._highlightAppt();

				SubmitRequestAppointment._setStatusMsg( response_parts[1] );

				SubmitRequestAppointment.hasAnyErrors = true;

				resize_booked_modal();
			},

			_requestAppointmentOnSuccess: function( response_parts ) {
				var $apptEl = SubmitRequestAppointment._getApptElement();
				
				$(document).trigger("booked-on-requested-appt-success",[$apptEl]);

				SubmitRequestAppointment._unhighlightAppt();
			},

			_onAfterRequestAppointment: function() {
				var redirectObj = { redirect : false };
				var redirect = $(document).trigger("booked-on-requested-appointment",[redirectObj]);

				if ( redirectObj.redirect ) {
					return;
				}

				if ( booked_js_vars.profilePage ) {
					window.location = booked_js_vars.profilePage;
					return;
				}

				SubmitRequestAppointment._reloadApptsList();
				SubmitRequestAppointment._reloadCalendarTable();
			},

			_setStatusMsg: function( msg ) {
				var form_status_selector = SubmitRequestAppointment.formSelector + ' ' + SubmitRequestAppointment.formStatusSelector;
				$( form_status_selector ).show().html( '<i class="fa fa-warning" style="color:#E35656"></i>&nbsp;&nbsp;&nbsp;' + msg );
			},

			_getApptElement: function( appt_index ) {
				var appt_index = typeof appt_index === 'undefined' ? SubmitRequestAppointment.currentApptIndex : appt_index,
					appt_cnt_selector = SubmitRequestAppointment.formSelector + ' ' + SubmitRequestAppointment.apptContainerSelector;

				return $( appt_cnt_selector ).eq( appt_index );
			},

			_highlightAppt: function( msg ) {
				var $apptEl = SubmitRequestAppointment._getApptElement();

				if ( ! $apptEl.length ) {
					return;
				}

				$apptEl.addClass('has-error');
			},

			_unhighlightAppt: function( msg ) {
				var $apptEl = SubmitRequestAppointment._getApptElement();

				if ( ! $apptEl.length ) {
					return;
				}

				$apptEl.removeClass('has-error').addClass('skip');
			},

			_enableSubmitBtn: function() {
				var btn_selector = SubmitRequestAppointment.formSelector + ' ' + SubmitRequestAppointment.formSubmitBtnSelector;
				$( btn_selector ).attr( 'disabled', false );
			},

			_disableSubmitBtn: function() {
				var btn_selector = SubmitRequestAppointment.formSelector + ' ' + SubmitRequestAppointment.formSubmitBtnSelector;
				$( btn_selector ).attr( 'disabled', true );
			},

			_showCancelBtn: function() {
				$( SubmitRequestAppointment.formSelector ).find('button.cancel').show();
			},

			_hideCancelBtn: function() {
				$( SubmitRequestAppointment.formSelector ).find('button.cancel').hide();
			},

			_showLoadingMessage: function() {
				$('form#newAppointmentForm p.status').show().html('<i class="fa fa-refresh fa-spin"></i>&nbsp;&nbsp;&nbsp;' + booked_js_vars.i18n_please_wait);
			},

			_reloadApptsList: function() {
				if ( ! $('.booked-appt-list').length ){
					return;
				}

				$('.booked-appt-list').each( function() {
					var $thisApptList  = $(this),
						date          = $thisApptList.attr('data-list-date'),
						thisList      = $thisApptList.parents('.booked-list-view'),
						defaultDate   = thisList.attr('data-default'),
						calendar_id   = parseInt($thisApptList.find('.booked-list-view-nav').attr('data-calendar-id')) || 0;
					
					defaultDate = typeof defaultDate === 'undefined' ? false : defaultDate;
					calendar_id = calendar_id ? calendar_id : 0;

					thisList.addClass('booked-loading');

					var booked_load_list_view_date_booking_options = {
						'action'		: 'booked_appointment_list_date',
						'date'			: date,
						'calendar_id'	: calendar_id,
						'force_default'	: defaultDate
					};
					
					$(document).trigger("booked-before-loading-appointment-list-booking-options");
					thisList.spin('booked_top');
				
					$.ajax({
						url: booked_js_vars.ajax_url,
						type: 'post',
						data: booked_load_list_view_date_booking_options,
						success: function( html ) {
							thisList.html( html );
							
							close_booked_modal();
							init_appt_list_date_picker();
							setTimeout(function(){
								thisList.removeClass('booked-loading');
							},1);
						}
					});
				});
			},

			_reloadCalendarTable: function() {
				if ( ! $('td.active').length ) {
					return;
				}

				var $activeTD = $('td.active'),
					activeDate = $activeTD.attr('data-date'),
					calendar_id = parseInt( $activeTD.parents('table').data('calendar-id') ) || 0;

				booked_load_calendar_date_booking_options = { 'action':'booked_calendar_date', 'date':activeDate, 'calendar_id':calendar_id };
				$(document).trigger("booked-before-loading-calendar-booking-options");
				
				$.ajax({
					url: booked_js_vars.ajax_url,
					type: 'post',
					data: booked_load_calendar_date_booking_options,
					success: function( html ) {
						
						$('tr.entryBlock').find('td').html( html );
						
						close_booked_modal();
						$('tr.entryBlock').removeClass('loading');
						$('tr.entryBlock').find('.booked-appt-list').hide().fadeIn(300);
						$('tr.entryBlock').find('.booked-appt-list').addClass('shown');
						adjust_calendar_boxes();
					}
				});
			}
		}
	});
	
	function bookedRemoveEmptyTRs(){
		$('table.booked-calendar').find('tr.week').each(function(){
			if ($(this).children().length == 0){
				$(this).remove();
			}
		});
	}

	// Saving state updater
	function savingState(show,limit_to){

		show = typeof show !== 'undefined' ? show : true;
		limit_to = typeof limit_to !== 'undefined' ? limit_to : false;

		if (limit_to){

			var $savingStateDIV = limit_to.find('li.active .savingState, .topSavingState.savingState, .calendarSavingState');
			var $stuffToHide = limit_to.find('.monthName');
			var $stuffToTransparent = limit_to.find('table.booked-calendar tbody');

		} else {

			var $savingStateDIV = $('li.active .savingState, .topSavingState.savingState, .calendarSavingState');
			var $stuffToHide = $('.monthName');
			var $stuffToTransparent = $('table.booked-calendar tbody');

		}

		if (show){
			$savingStateDIV.fadeIn(200);
			$stuffToHide.hide();
			$stuffToTransparent.animate({'opacity':0.2},100);
		} else {
			$savingStateDIV.hide();
			$stuffToHide.show();
			$stuffToTransparent.animate({'opacity':1},0);
		}

	}

	$(document).ajaxStop(function() {
		savingState(false);
	});
	
	function init_appt_list_date_picker(){
		
		$('.booked_list_date_picker').each(function(){
			var thisDatePicker = $(this);
			var minDateVal = thisDatePicker.parents('.booked-appt-list').attr('data-min-date');
			var maxDateVal = thisDatePicker.parents('.booked-appt-list').attr('data-max-date');
			if (typeof minDateVal == 'undefined'){ var minDateVal = thisDatePicker.attr('data-min-date'); }
		
			thisDatePicker.datepicker({
		        dateFormat: 'yy-mm-dd',
		        minDate: minDateVal,
		        maxDate: maxDateVal,
		        showAnim: false,
		        beforeShow: function(input, inst) {
					$('#ui-datepicker-div').removeClass();
					$('#ui-datepicker-div').addClass('booked_custom_date_picker');
			    },
			    onClose: function(dateText){
					$('.booked_list_date_picker_trigger').removeClass('booked-dp-active'); 
			    },
			    onSelect: function(dateText){
				   
				   	var thisInput 			= $(this),
						date				= dateText,
						thisList			= thisInput.parents('.booked-list-view'),
						defaultDate			= thisList.attr('data-default'),
						calendar_id			= thisInput.parents('.booked-list-view-nav').attr('data-calendar-id');
						
					if (typeof defaultDate == 'undefined'){ defaultDate = false; }
						
					if (!calendar_id){ calendar_id = 0; }
					thisList.addClass('booked-loading');
					
					var booked_load_list_view_date_booking_options = {
						'action'		: 'booked_appointment_list_date',
						'date'			: date,
						'calendar_id'	: calendar_id,
						'force_default'	: defaultDate
					};
					
					$(document).trigger("booked-before-loading-appointment-list-booking-options");
					thisList.spin('booked_top');
				
					$.ajax({
						url: booked_js_vars.ajax_url,
						type: 'post',
						data: booked_load_list_view_date_booking_options,
						success: function( html ) {
							
							thisList.html( html );
							
							init_appt_list_date_picker();
							setTimeout(function(){
								thisList.removeClass('booked-loading');
							},1);
							
						}
					});
					
					return false;
			    }
		    });
		    
		});
		
		$('body').on('click','.booked_list_date_picker_trigger',function(e){
			e.preventDefault();
			if (!$(this).hasClass('booked-dp-active')){
				$(this).addClass('booked-dp-active');
				$(this).parents('.booked-appt-list').find('.booked_list_date_picker').datepicker('show');
			}
			
	    }); 
	    
	}

	var BookedTabs = {
		bookingModalSelector: '.booked-modal',
		tabSelector: '.booked-tabs',
		tabNavSelector: '.booked-tabs-nav span',
		tabCntSelector: '.booked-tabs-cnt',

		Init: function() {
			$(document).on( 'click', this.tabNavSelector, this.tabsNav );
		},

		tabsNav: function( event ) {
			event.preventDefault();

			BookedTabs.switchToTab( $(this) );
			BookedTabs.maybeResizeBookingModal();
		},

		switchToTab: function( tab_nav_item ) {
			var $nav_item = tab_nav_item,
				tab_cnt_class = '.' + $nav_item.data('tab-cnt'),
				$tabs_container = $nav_item.parents( BookedTabs.tabSelector );

			$nav_item
				.addClass( 'active' )
				.siblings()
				.removeClass( 'active' )

			$tabs_container
				.find( BookedTabs.tabCntSelector + ' ' + tab_cnt_class )
				.addClass( 'active' )
				.siblings()
				.removeClass( 'active' );
		},

		maybeResizeBookingModal: function() {
			if ( ! $(BookedTabs.bookingModalSelector).length ) {
				return;
			}
			
			resize_booked_modal();
		}
	}

})(jQuery, window, document);

// Create Booked Modal
function create_booked_modal(){
	var windowHeight = jQuery(window).height();
	var windowWidth = jQuery(window).width();
	if (windowWidth > 720){
		var maxModalHeight = windowHeight - 295;
	} else {
		var maxModalHeight = windowHeight;
	}
	
	jQuery('body input, body textarea, body select').blur();
	jQuery('body').addClass('booked-noScroll');
	jQuery('<div class="booked-modal bm-loading"><div class="bm-overlay"></div><div class="bm-window"><div style="height:100px"></div></div></div>').appendTo('body');
	jQuery('.booked-modal .bm-overlay').spin('booked_white');
	jQuery('.booked-modal .bm-window').css({'max-height':maxModalHeight+'px'});
}

var previousRealModalHeight = 100;

function resize_booked_modal(){
	
	var windowHeight = jQuery(window).height();
	var windowWidth = jQuery(window).width();
	
	var common43 = 43;
	
	if (jQuery('.booked-modal .bm-window .booked-scrollable').length){
		var realModalHeight = jQuery('.booked-modal .bm-window .booked-scrollable')[0].scrollHeight;
		
		if (realModalHeight < 100){
			realModalHeight = previousRealModalHeight;
		} else {
			previousRealModalHeight = realModalHeight;
		}
		
	} else {
		var realModalHeight = 0;
	}
	var minimumWindowHeight = realModalHeight + common43 + common43;
	var modalScrollableHeight = realModalHeight - common43;
	var maxModalHeight;
	var maxFormHeight;
	
	if (windowHeight < minimumWindowHeight){
		modalScrollableHeight = windowHeight - common43 - common43;
	} else {
		modalScrollableHeight = realModalHeight;
	}
	
	if (windowWidth > 720){
		maxModalHeight = modalScrollableHeight - 25;
		maxFormHeight = maxModalHeight - 15;
		var modalNegMargin = (maxModalHeight + 78) / 2;
	} else {
		maxModalHeight = windowHeight - common43;
		maxFormHeight = maxModalHeight - 60 - common43;
		var modalNegMargin = (maxModalHeight) / 2;
	}
	
	jQuery('.booked-modal').css({'margin-top':'-'+modalNegMargin+'px'});
	jQuery('.booked-modal .bm-window').css({'max-height':maxModalHeight+'px'});
	jQuery('.booked-modal .bm-window .booked-scrollable').css({'max-height':maxFormHeight+'px'});
	
}

function close_booked_modal(){
	var modal = jQuery('.booked-modal');
	modal.fadeOut(200);
	modal.addClass('bm-closing');
	jQuery('body').removeClass('booked-noScroll');
	setTimeout(function(){
		modal.remove();
	},300);
}

function init_tooltips(container){
	jQuery('.tooltipster').tooltipster({
		theme: 		'tooltipster-light',
		animation:	'grow',
		speed:		200,
		delay: 		100,
		offsetY:	-13
	});
}

// Function to adjust calendar sizing
function adjust_calendar_boxes(){
	jQuery('.booked-calendar').each(function(){
		
		var windowWidth = jQuery(window).width();
		var smallCalendar = jQuery(this).parents('.booked-calendar-wrap').hasClass('small');
		var boxesWidth = jQuery(this).find('tbody tr.week td').width();
		var calendarHeight = jQuery(this).height();
		boxesHeight = boxesWidth * 1;
		jQuery(this).find('tbody tr.week td').height(boxesHeight);
		jQuery(this).find('tbody tr.week td .date').css('line-height',boxesHeight+'px');
		jQuery(this).find('tbody tr.week td .date .number').css('line-height',boxesHeight+'px');
		if (smallCalendar || windowWidth < 720){
			jQuery(this).find('tbody tr.week td .date .number').css('line-height',boxesHeight+'px');
		} else {
			jQuery(this).find('tbody tr.week td .date .number').css('line-height','');
		}

		var calendarHeight = jQuery(this).height();
		jQuery(this).parent().height(calendarHeight);

	});
}

// fin functions 

(function($){
	window.tm_pb_smooth_scroll = function( $target, $top_section, speed, easing ) {
		var $window_width = $( window ).width();

		if ( $( 'body' ).hasClass( 'tm_fixed_nav' ) && $window_width > 980 ) {
			$menu_offset = $( '#top-header' ).outerHeight() + $( '#main-header' ).outerHeight() - 1;
		} else {
			$menu_offset = -1;
		}

		if ( $ ('#wpadminbar').length && $window_width > 600 ) {
			$menu_offset += $( '#wpadminbar' ).outerHeight();
		}

		//fix sidenav scroll to top
		if ( $top_section ) {
			$scroll_position = 0;
		} else {
			$scroll_position = $target.offset().top - $menu_offset;
		}

		// set swing (animate's scrollTop default) as default value
		if( typeof easing === 'undefined' ){
			easing = 'swing';
		}

		$( 'html, body' ).animate( { scrollTop :  $scroll_position }, speed, easing );
	}

	window.tm_fix_video_wmode = function( video_wrapper ) {
		$( video_wrapper ).each( function() {
			if ( $(this).find( 'iframe' ).length ) {
				var $this_el = $(this).find( 'iframe' ),
					src_attr = $this_el.attr('src'),
					wmode_character = src_attr.indexOf( '?' ) == -1 ? '?' : '&amp;',
					this_src = src_attr + wmode_character + 'wmode=opaque';

				$this_el.attr('src', this_src);
			}
		} );
	}

	window.tm_pb_form_placeholders_init = function( $form ) {
		$form.find('input:text, textarea').each(function(index,domEle){
			var $tm_current_input = jQuery(domEle),
				$tm_comment_label = $tm_current_input.siblings('label'),
				tm_comment_label_value = $tm_current_input.siblings('label').text();
			if ( $tm_comment_label.length ) {
				$tm_comment_label.hide();
				if ( $tm_current_input.siblings('span.required') ) {
					tm_comment_label_value += $tm_current_input.siblings('span.required').text();
					$tm_current_input.siblings('span.required').hide();
				}
				$tm_current_input.val(tm_comment_label_value);
			}
		}).bind('focus',function(){
			var tm_label_text = jQuery(this).siblings('label').text();
			if ( jQuery(this).siblings('span.required').length ) tm_label_text += jQuery(this).siblings('span.required').text();
			if (jQuery(this).val() === tm_label_text) jQuery(this).val("");
		}).bind('blur',function(){
			var tm_label_text = jQuery(this).siblings('label').text();
			if ( jQuery(this).siblings('span.required').length ) tm_label_text += jQuery(this).siblings('span.required').text();
			if (jQuery(this).val() === "") jQuery(this).val( tm_label_text );
		});
	}

	window.tm_duplicate_menu = function( menu, append_to, menu_id, menu_class, menu_click_event ){
		append_to.each( function() {
			var $this_menu = $(this),
				$cloned_nav;

			// make this function work with existing menus, without cloning
			if ( '' !== menu ) {
				menu.clone().attr('id',menu_id).removeClass().attr('class',menu_class).appendTo( $this_menu );
			}

			$cloned_nav = $this_menu.find('> ul');
			$cloned_nav.find('.menu_slide').remove();
			$cloned_nav.find('li:first').addClass('tm_first_mobile_item');

			$cloned_nav.find( 'a' ).on( 'click', function(){
				$this_menu.trigger( 'click' );
			} );

			if ( 'no_click_event' !== menu_click_event ) {
				$this_menu.on( 'click', '.mobile_menu_bar', function(){
					if ( $this_menu.hasClass('closed') ){
						$this_menu.removeClass( 'closed' ).addClass( 'opened' );
						$cloned_nav.stop().slideDown( 500 );
					} else {
						$this_menu.removeClass( 'opened' ).addClass( 'closed' );
						$cloned_nav.stop().slideUp( 500 );
					}
					return false;
				} );
			}
		} );

		$('#mobile_menu .centered-inline-logo-wrap').remove();
	}

	// remove placeholder text before form submission
	window.tm_pb_remove_placeholder_text = function( $form ) {
		$form.find('input:text, textarea').each(function(index,domEle){
			var $tm_current_input = jQuery(domEle),
				$tm_label = $tm_current_input.siblings('label'),
				tm_label_value = $tm_current_input.siblings('label').text();

			if ( $tm_label.length && $tm_label.is(':hidden') ) {
				if ( $tm_label.text() == $tm_current_input.val() )
					$tm_current_input.val( '' );
			}
		});
	}

	window.tm_fix_fullscreen_section = function() {
		var $tm_window = $(window);

		$( 'section.tm_pb_fullscreen' ).each( function(){
			var $this_section = $( this );

			$.proxy( tm_calc_fullscreen_section, $this_section )();

			$tm_window.on( 'resize', $.proxy( tm_calc_fullscreen_section, $this_section ) );
		});
	}
})(jQuery)


// fin front end builder global 

/**
 * Swiper 3.3.1
 * Most modern mobile touch slider and framework with hardware accelerated transitions
 * 
 * http://www.idangero.us/swiper/
 * 
 * Copyright 2016, Vladimir Kharlampidi
 * The iDangero.us
 * http://www.idangero.us/
 * 
 * Licensed under MIT
 * 
 * Released on: February 7, 2016
 */
!function(){"use strict";function e(e){e.fn.swiper=function(a){var s;return e(this).each(function(){var e=new t(this,a);s||(s=e)}),s}}var a,t=function(e,s){function r(e){return Math.floor(e)}function i(){y.autoplayTimeoutId=setTimeout(function(){y.params.loop?(y.fixLoop(),y._slideNext(),y.emit("onAutoplay",y)):y.isEnd?s.autoplayStopOnLast?y.stopAutoplay():(y._slideTo(0),y.emit("onAutoplay",y)):(y._slideNext(),y.emit("onAutoplay",y))},y.params.autoplay)}function n(e,t){var s=a(e.target);if(!s.is(t))if("string"==typeof t)s=s.parents(t);else if(t.nodeType){var r;return s.parents().each(function(e,a){a===t&&(r=t)}),r?t:void 0}if(0!==s.length)return s[0]}function o(e,a){a=a||{};var t=window.MutationObserver||window.WebkitMutationObserver,s=new t(function(e){e.forEach(function(e){y.onResize(!0),y.emit("onObserverUpdate",y,e)})});s.observe(e,{attributes:"undefined"==typeof a.attributes?!0:a.attributes,childList:"undefined"==typeof a.childList?!0:a.childList,characterData:"undefined"==typeof a.characterData?!0:a.characterData}),y.observers.push(s)}function l(e){e.originalEvent&&(e=e.originalEvent);var a=e.keyCode||e.charCode;if(!y.params.allowSwipeToNext&&(y.isHorizontal()&&39===a||!y.isHorizontal()&&40===a))return!1;if(!y.params.allowSwipeToPrev&&(y.isHorizontal()&&37===a||!y.isHorizontal()&&38===a))return!1;if(!(e.shiftKey||e.altKey||e.ctrlKey||e.metaKey||document.activeElement&&document.activeElement.nodeName&&("input"===document.activeElement.nodeName.toLowerCase()||"textarea"===document.activeElement.nodeName.toLowerCase()))){if(37===a||39===a||38===a||40===a){var t=!1;if(y.container.parents(".swiper-slide").length>0&&0===y.container.parents(".swiper-slide-active").length)return;var s={left:window.pageXOffset,top:window.pageYOffset},r=window.innerWidth,i=window.innerHeight,n=y.container.offset();y.rtl&&(n.left=n.left-y.container[0].scrollLeft);for(var o=[[n.left,n.top],[n.left+y.width,n.top],[n.left,n.top+y.height],[n.left+y.width,n.top+y.height]],l=0;l<o.length;l++){var p=o[l];p[0]>=s.left&&p[0]<=s.left+r&&p[1]>=s.top&&p[1]<=s.top+i&&(t=!0)}if(!t)return}y.isHorizontal()?((37===a||39===a)&&(e.preventDefault?e.preventDefault():e.returnValue=!1),(39===a&&!y.rtl||37===a&&y.rtl)&&y.slideNext(),(37===a&&!y.rtl||39===a&&y.rtl)&&y.slidePrev()):((38===a||40===a)&&(e.preventDefault?e.preventDefault():e.returnValue=!1),40===a&&y.slideNext(),38===a&&y.slidePrev())}}function p(e){e.originalEvent&&(e=e.originalEvent);var a=y.mousewheel.event,t=0,s=y.rtl?-1:1;if("mousewheel"===a)if(y.params.mousewheelForceToAxis)if(y.isHorizontal()){if(!(Math.abs(e.wheelDeltaX)>Math.abs(e.wheelDeltaY)))return;t=e.wheelDeltaX*s}else{if(!(Math.abs(e.wheelDeltaY)>Math.abs(e.wheelDeltaX)))return;t=e.wheelDeltaY}else t=Math.abs(e.wheelDeltaX)>Math.abs(e.wheelDeltaY)?-e.wheelDeltaX*s:-e.wheelDeltaY;else if("DOMMouseScroll"===a)t=-e.detail;else if("wheel"===a)if(y.params.mousewheelForceToAxis)if(y.isHorizontal()){if(!(Math.abs(e.deltaX)>Math.abs(e.deltaY)))return;t=-e.deltaX*s}else{if(!(Math.abs(e.deltaY)>Math.abs(e.deltaX)))return;t=-e.deltaY}else t=Math.abs(e.deltaX)>Math.abs(e.deltaY)?-e.deltaX*s:-e.deltaY;if(0!==t){if(y.params.mousewheelInvert&&(t=-t),y.params.freeMode){var r=y.getWrapperTranslate()+t*y.params.mousewheelSensitivity,i=y.isBeginning,n=y.isEnd;if(r>=y.minTranslate()&&(r=y.minTranslate()),r<=y.maxTranslate()&&(r=y.maxTranslate()),y.setWrapperTransition(0),y.setWrapperTranslate(r),y.updateProgress(),y.updateActiveIndex(),(!i&&y.isBeginning||!n&&y.isEnd)&&y.updateClasses(),y.params.freeModeSticky?(clearTimeout(y.mousewheel.timeout),y.mousewheel.timeout=setTimeout(function(){y.slideReset()},300)):y.params.lazyLoading&&y.lazy&&y.lazy.load(),0===r||r===y.maxTranslate())return}else{if((new window.Date).getTime()-y.mousewheel.lastScrollTime>60)if(0>t)if(y.isEnd&&!y.params.loop||y.animating){if(y.params.mousewheelReleaseOnEdges)return!0}else y.slideNext();else if(y.isBeginning&&!y.params.loop||y.animating){if(y.params.mousewheelReleaseOnEdges)return!0}else y.slidePrev();y.mousewheel.lastScrollTime=(new window.Date).getTime()}return y.params.autoplay&&y.stopAutoplay(),e.preventDefault?e.preventDefault():e.returnValue=!1,!1}}function d(e,t){e=a(e);var s,r,i,n=y.rtl?-1:1;s=e.attr("data-swiper-parallax")||"0",r=e.attr("data-swiper-parallax-x"),i=e.attr("data-swiper-parallax-y"),r||i?(r=r||"0",i=i||"0"):y.isHorizontal()?(r=s,i="0"):(i=s,r="0"),r=r.indexOf("%")>=0?parseInt(r,10)*t*n+"%":r*t*n+"px",i=i.indexOf("%")>=0?parseInt(i,10)*t+"%":i*t+"px",e.transform("translate3d("+r+", "+i+",0px)")}function u(e){return 0!==e.indexOf("on")&&(e=e[0]!==e[0].toUpperCase()?"on"+e[0].toUpperCase()+e.substring(1):"on"+e),e}if(!(this instanceof t))return new t(e,s);var c={direction:"horizontal",touchEventsTarget:"container",initialSlide:0,speed:300,autoplay:!1,autoplayDisableOnInteraction:!0,autoplayStopOnLast:!1,iOSEdgeSwipeDetection:!1,iOSEdgeSwipeThreshold:20,freeMode:!1,freeModeMomentum:!0,freeModeMomentumRatio:1,freeModeMomentumBounce:!0,freeModeMomentumBounceRatio:1,freeModeSticky:!1,freeModeMinimumVelocity:.02,autoHeight:!1,setWrapperSize:!1,virtualTranslate:!1,effect:"slide",coverflow:{rotate:50,stretch:0,depth:100,modifier:1,slideShadows:!0},flip:{slideShadows:!0,limitRotation:!0},cube:{slideShadows:!0,shadow:!0,shadowOffset:20,shadowScale:.94},fade:{crossFade:!1},parallax:!1,scrollbar:null,scrollbarHide:!0,scrollbarDraggable:!1,scrollbarSnapOnRelease:!1,keyboardControl:!1,mousewheelControl:!1,mousewheelReleaseOnEdges:!1,mousewheelInvert:!1,mousewheelForceToAxis:!1,mousewheelSensitivity:1,hashnav:!1,breakpoints:void 0,spaceBetween:0,slidesPerView:1,slidesPerColumn:1,slidesPerColumnFill:"column",slidesPerGroup:1,centeredSlides:!1,slidesOffsetBefore:0,slidesOffsetAfter:0,roundLengths:!1,touchRatio:1,touchAngle:45,simulateTouch:!0,shortSwipes:!0,longSwipes:!0,longSwipesRatio:.5,longSwipesMs:300,followFinger:!0,onlyExternal:!1,threshold:0,touchMoveStopPropagation:!0,uniqueNavElements:!0,pagination:null,paginationElement:"span",paginationClickable:!1,paginationHide:!1,paginationBulletRender:null,paginationProgressRender:null,paginationFractionRender:null,paginationCustomRender:null,paginationType:"bullets",resistance:!0,resistanceRatio:.85,nextButton:null,prevButton:null,watchSlidesProgress:!1,watchSlidesVisibility:!1,grabCursor:!1,preventClicks:!0,preventClicksPropagation:!0,slideToClickedSlide:!1,lazyLoading:!1,lazyLoadingInPrevNext:!1,lazyLoadingInPrevNextAmount:1,lazyLoadingOnTransitionStart:!1,preloadImages:!0,updateOnImagesReady:!0,loop:!1,loopAdditionalSlides:0,loopedSlides:null,control:void 0,controlInverse:!1,controlBy:"slide",allowSwipeToPrev:!0,allowSwipeToNext:!0,swipeHandler:null,noSwiping:!0,noSwipingClass:"swiper-no-swiping",slideClass:"swiper-slide",slideActiveClass:"swiper-slide-active",slideVisibleClass:"swiper-slide-visible",slideDuplicateClass:"swiper-slide-duplicate",slideNextClass:"swiper-slide-next",slidePrevClass:"swiper-slide-prev",wrapperClass:"swiper-wrapper",bulletClass:"swiper-pagination-bullet",bulletActiveClass:"swiper-pagination-bullet-active",buttonDisabledClass:"swiper-button-disabled",paginationCurrentClass:"swiper-pagination-current",paginationTotalClass:"swiper-pagination-total",paginationHiddenClass:"swiper-pagination-hidden",paginationProgressbarClass:"swiper-pagination-progressbar",observer:!1,observeParents:!1,a11y:!1,prevSlideMessage:"Previous slide",nextSlideMessage:"Next slide",firstSlideMessage:"This is the first slide",lastSlideMessage:"This is the last slide",paginationBulletMessage:"Go to slide {{index}}",runCallbacksOnInit:!0},m=s&&s.virtualTranslate;s=s||{};var f={};for(var g in s)if("object"!=typeof s[g]||null===s[g]||(s[g].nodeType||s[g]===window||s[g]===document||"undefined"!=typeof Dom7&&s[g]instanceof Dom7||"undefined"!=typeof jQuery&&s[g]instanceof jQuery))f[g]=s[g];else{f[g]={};for(var h in s[g])f[g][h]=s[g][h]}for(var v in c)if("undefined"==typeof s[v])s[v]=c[v];else if("object"==typeof s[v])for(var w in c[v])"undefined"==typeof s[v][w]&&(s[v][w]=c[v][w]);var y=this;if(y.params=s,y.originalParams=f,y.classNames=[],"undefined"!=typeof a&&"undefined"!=typeof Dom7&&(a=Dom7),("undefined"!=typeof a||(a="undefined"==typeof Dom7?window.Dom7||window.Zepto||window.jQuery:Dom7))&&(y.$=a,y.currentBreakpoint=void 0,y.getActiveBreakpoint=function(){if(!y.params.breakpoints)return!1;var e,a=!1,t=[];for(e in y.params.breakpoints)y.params.breakpoints.hasOwnProperty(e)&&t.push(e);t.sort(function(e,a){return parseInt(e,10)>parseInt(a,10)});for(var s=0;s<t.length;s++)e=t[s],e>=window.innerWidth&&!a&&(a=e);return a||"max"},y.setBreakpoint=function(){var e=y.getActiveBreakpoint();if(e&&y.currentBreakpoint!==e){var a=e in y.params.breakpoints?y.params.breakpoints[e]:y.originalParams,t=y.params.loop&&a.slidesPerView!==y.params.slidesPerView;for(var s in a)y.params[s]=a[s];y.currentBreakpoint=e,t&&y.destroyLoop&&y.reLoop(!0)}},y.params.breakpoints&&y.setBreakpoint(),y.container=a(e),0!==y.container.length)){if(y.container.length>1){var b=[];return y.container.each(function(){b.push(new t(this,s))}),b}y.container[0].swiper=y,y.container.data("swiper",y),y.classNames.push("swiper-container-"+y.params.direction),y.params.freeMode&&y.classNames.push("swiper-container-free-mode"),y.support.flexbox||(y.classNames.push("swiper-container-no-flexbox"),y.params.slidesPerColumn=1),y.params.autoHeight&&y.classNames.push("swiper-container-autoheight"),(y.params.parallax||y.params.watchSlidesVisibility)&&(y.params.watchSlidesProgress=!0),["cube","coverflow","flip"].indexOf(y.params.effect)>=0&&(y.support.transforms3d?(y.params.watchSlidesProgress=!0,y.classNames.push("swiper-container-3d")):y.params.effect="slide"),"slide"!==y.params.effect&&y.classNames.push("swiper-container-"+y.params.effect),"cube"===y.params.effect&&(y.params.resistanceRatio=0,y.params.slidesPerView=1,y.params.slidesPerColumn=1,y.params.slidesPerGroup=1,y.params.centeredSlides=!1,y.params.spaceBetween=0,y.params.virtualTranslate=!0,y.params.setWrapperSize=!1),("fade"===y.params.effect||"flip"===y.params.effect)&&(y.params.slidesPerView=1,y.params.slidesPerColumn=1,y.params.slidesPerGroup=1,y.params.watchSlidesProgress=!0,y.params.spaceBetween=0,y.params.setWrapperSize=!1,"undefined"==typeof m&&(y.params.virtualTranslate=!0)),y.params.grabCursor&&y.support.touch&&(y.params.grabCursor=!1),y.wrapper=y.container.children("."+y.params.wrapperClass),y.params.pagination&&(y.paginationContainer=a(y.params.pagination),y.params.uniqueNavElements&&"string"==typeof y.params.pagination&&y.paginationContainer.length>1&&1===y.container.find(y.params.pagination).length&&(y.paginationContainer=y.container.find(y.params.pagination)),"bullets"===y.params.paginationType&&y.params.paginationClickable?y.paginationContainer.addClass("swiper-pagination-clickable"):y.params.paginationClickable=!1,y.paginationContainer.addClass("swiper-pagination-"+y.params.paginationType)),(y.params.nextButton||y.params.prevButton)&&(y.params.nextButton&&(y.nextButton=a(y.params.nextButton),y.params.uniqueNavElements&&"string"==typeof y.params.nextButton&&y.nextButton.length>1&&1===y.container.find(y.params.nextButton).length&&(y.nextButton=y.container.find(y.params.nextButton))),y.params.prevButton&&(y.prevButton=a(y.params.prevButton),y.params.uniqueNavElements&&"string"==typeof y.params.prevButton&&y.prevButton.length>1&&1===y.container.find(y.params.prevButton).length&&(y.prevButton=y.container.find(y.params.prevButton)))),y.isHorizontal=function(){return"horizontal"===y.params.direction},y.rtl=y.isHorizontal()&&("rtl"===y.container[0].dir.toLowerCase()||"rtl"===y.container.css("direction")),y.rtl&&y.classNames.push("swiper-container-rtl"),y.rtl&&(y.wrongRTL="-webkit-box"===y.wrapper.css("display")),y.params.slidesPerColumn>1&&y.classNames.push("swiper-container-multirow"),y.device.android&&y.classNames.push("swiper-container-android"),y.container.addClass(y.classNames.join(" ")),y.translate=0,y.progress=0,y.velocity=0,y.lockSwipeToNext=function(){y.params.allowSwipeToNext=!1},y.lockSwipeToPrev=function(){y.params.allowSwipeToPrev=!1},y.lockSwipes=function(){y.params.allowSwipeToNext=y.params.allowSwipeToPrev=!1},y.unlockSwipeToNext=function(){y.params.allowSwipeToNext=!0},y.unlockSwipeToPrev=function(){y.params.allowSwipeToPrev=!0},y.unlockSwipes=function(){y.params.allowSwipeToNext=y.params.allowSwipeToPrev=!0},y.params.grabCursor&&(y.container[0].style.cursor="move",y.container[0].style.cursor="-webkit-grab",y.container[0].style.cursor="-moz-grab",y.container[0].style.cursor="grab"),y.imagesToLoad=[],y.imagesLoaded=0,y.loadImage=function(e,a,t,s,r){function i(){r&&r()}var n;e.complete&&s?i():a?(n=new window.Image,n.onload=i,n.onerror=i,t&&(n.srcset=t),a&&(n.src=a)):i()},y.preloadImages=function(){function e(){"undefined"!=typeof y&&null!==y&&(void 0!==y.imagesLoaded&&y.imagesLoaded++,y.imagesLoaded===y.imagesToLoad.length&&(y.params.updateOnImagesReady&&y.update(),y.emit("onImagesReady",y)))}y.imagesToLoad=y.container.find("img");for(var a=0;a<y.imagesToLoad.length;a++)y.loadImage(y.imagesToLoad[a],y.imagesToLoad[a].currentSrc||y.imagesToLoad[a].getAttribute("src"),y.imagesToLoad[a].srcset||y.imagesToLoad[a].getAttribute("srcset"),!0,e)},y.autoplayTimeoutId=void 0,y.autoplaying=!1,y.autoplayPaused=!1,y.startAutoplay=function(){return"undefined"!=typeof y.autoplayTimeoutId?!1:y.params.autoplay?y.autoplaying?!1:(y.autoplaying=!0,y.emit("onAutoplayStart",y),void i()):!1},y.stopAutoplay=function(e){y.autoplayTimeoutId&&(y.autoplayTimeoutId&&clearTimeout(y.autoplayTimeoutId),y.autoplaying=!1,y.autoplayTimeoutId=void 0,y.emit("onAutoplayStop",y))},y.pauseAutoplay=function(e){y.autoplayPaused||(y.autoplayTimeoutId&&clearTimeout(y.autoplayTimeoutId),y.autoplayPaused=!0,0===e?(y.autoplayPaused=!1,i()):y.wrapper.transitionEnd(function(){y&&(y.autoplayPaused=!1,y.autoplaying?i():y.stopAutoplay())}))},y.minTranslate=function(){return-y.snapGrid[0]},y.maxTranslate=function(){return-y.snapGrid[y.snapGrid.length-1]},y.updateAutoHeight=function(){var e=y.slides.eq(y.activeIndex)[0];if("undefined"!=typeof e){var a=e.offsetHeight;a&&y.wrapper.css("height",a+"px")}},y.updateContainerSize=function(){var e,a;e="undefined"!=typeof y.params.width?y.params.width:y.container[0].clientWidth,a="undefined"!=typeof y.params.height?y.params.height:y.container[0].clientHeight,0===e&&y.isHorizontal()||0===a&&!y.isHorizontal()||(e=e-parseInt(y.container.css("padding-left"),10)-parseInt(y.container.css("padding-right"),10),a=a-parseInt(y.container.css("padding-top"),10)-parseInt(y.container.css("padding-bottom"),10),y.width=e,y.height=a,y.size=y.isHorizontal()?y.width:y.height)},y.updateSlidesSize=function(){y.slides=y.wrapper.children("."+y.params.slideClass),y.snapGrid=[],y.slidesGrid=[],y.slidesSizesGrid=[];var e,a=y.params.spaceBetween,t=-y.params.slidesOffsetBefore,s=0,i=0;if("undefined"!=typeof y.size){"string"==typeof a&&a.indexOf("%")>=0&&(a=parseFloat(a.replace("%",""))/100*y.size),y.virtualSize=-a,y.rtl?y.slides.css({marginLeft:"",marginTop:""}):y.slides.css({marginRight:"",marginBottom:""});var n;y.params.slidesPerColumn>1&&(n=Math.floor(y.slides.length/y.params.slidesPerColumn)===y.slides.length/y.params.slidesPerColumn?y.slides.length:Math.ceil(y.slides.length/y.params.slidesPerColumn)*y.params.slidesPerColumn,"auto"!==y.params.slidesPerView&&"row"===y.params.slidesPerColumnFill&&(n=Math.max(n,y.params.slidesPerView*y.params.slidesPerColumn)));var o,l=y.params.slidesPerColumn,p=n/l,d=p-(y.params.slidesPerColumn*p-y.slides.length);for(e=0;e<y.slides.length;e++){o=0;var u=y.slides.eq(e);if(y.params.slidesPerColumn>1){var c,m,f;"column"===y.params.slidesPerColumnFill?(m=Math.floor(e/l),f=e-m*l,(m>d||m===d&&f===l-1)&&++f>=l&&(f=0,m++),c=m+f*n/l,u.css({"-webkit-box-ordinal-group":c,"-moz-box-ordinal-group":c,"-ms-flex-order":c,"-webkit-order":c,order:c})):(f=Math.floor(e/p),m=e-f*p),u.css({"margin-top":0!==f&&y.params.spaceBetween&&y.params.spaceBetween+"px"}).attr("data-swiper-column",m).attr("data-swiper-row",f)}"none"!==u.css("display")&&("auto"===y.params.slidesPerView?(o=y.isHorizontal()?u.outerWidth(!0):u.outerHeight(!0),y.params.roundLengths&&(o=r(o))):(o=(y.size-(y.params.slidesPerView-1)*a)/y.params.slidesPerView,y.params.roundLengths&&(o=r(o)),y.isHorizontal()?y.slides[e].style.width=o+"px":y.slides[e].style.height=o+"px"),y.slides[e].swiperSlideSize=o,y.slidesSizesGrid.push(o),y.params.centeredSlides?(t=t+o/2+s/2+a,0===e&&(t=t-y.size/2-a),Math.abs(t)<.001&&(t=0),i%y.params.slidesPerGroup===0&&y.snapGrid.push(t),y.slidesGrid.push(t)):(i%y.params.slidesPerGroup===0&&y.snapGrid.push(t),y.slidesGrid.push(t),t=t+o+a),y.virtualSize+=o+a,s=o,i++)}y.virtualSize=Math.max(y.virtualSize,y.size)+y.params.slidesOffsetAfter;var g;if(y.rtl&&y.wrongRTL&&("slide"===y.params.effect||"coverflow"===y.params.effect)&&y.wrapper.css({width:y.virtualSize+y.params.spaceBetween+"px"}),(!y.support.flexbox||y.params.setWrapperSize)&&(y.isHorizontal()?y.wrapper.css({width:y.virtualSize+y.params.spaceBetween+"px"}):y.wrapper.css({height:y.virtualSize+y.params.spaceBetween+"px"})),y.params.slidesPerColumn>1&&(y.virtualSize=(o+y.params.spaceBetween)*n,y.virtualSize=Math.ceil(y.virtualSize/y.params.slidesPerColumn)-y.params.spaceBetween,y.wrapper.css({width:y.virtualSize+y.params.spaceBetween+"px"}),y.params.centeredSlides)){for(g=[],e=0;e<y.snapGrid.length;e++)y.snapGrid[e]<y.virtualSize+y.snapGrid[0]&&g.push(y.snapGrid[e]);y.snapGrid=g}if(!y.params.centeredSlides){for(g=[],e=0;e<y.snapGrid.length;e++)y.snapGrid[e]<=y.virtualSize-y.size&&g.push(y.snapGrid[e]);y.snapGrid=g,Math.floor(y.virtualSize-y.size)-Math.floor(y.snapGrid[y.snapGrid.length-1])>1&&y.snapGrid.push(y.virtualSize-y.size)}0===y.snapGrid.length&&(y.snapGrid=[0]),0!==y.params.spaceBetween&&(y.isHorizontal()?y.rtl?y.slides.css({marginLeft:a+"px"}):y.slides.css({marginRight:a+"px"}):y.slides.css({marginBottom:a+"px"})),y.params.watchSlidesProgress&&y.updateSlidesOffset()}},y.updateSlidesOffset=function(){for(var e=0;e<y.slides.length;e++)y.slides[e].swiperSlideOffset=y.isHorizontal()?y.slides[e].offsetLeft:y.slides[e].offsetTop},y.updateSlidesProgress=function(e){if("undefined"==typeof e&&(e=y.translate||0),0!==y.slides.length){"undefined"==typeof y.slides[0].swiperSlideOffset&&y.updateSlidesOffset();var a=-e;y.rtl&&(a=e),y.slides.removeClass(y.params.slideVisibleClass);for(var t=0;t<y.slides.length;t++){var s=y.slides[t],r=(a-s.swiperSlideOffset)/(s.swiperSlideSize+y.params.spaceBetween);if(y.params.watchSlidesVisibility){var i=-(a-s.swiperSlideOffset),n=i+y.slidesSizesGrid[t],o=i>=0&&i<y.size||n>0&&n<=y.size||0>=i&&n>=y.size;o&&y.slides.eq(t).addClass(y.params.slideVisibleClass)}s.progress=y.rtl?-r:r}}},y.updateProgress=function(e){"undefined"==typeof e&&(e=y.translate||0);var a=y.maxTranslate()-y.minTranslate(),t=y.isBeginning,s=y.isEnd;0===a?(y.progress=0,y.isBeginning=y.isEnd=!0):(y.progress=(e-y.minTranslate())/a,y.isBeginning=y.progress<=0,y.isEnd=y.progress>=1),y.isBeginning&&!t&&y.emit("onReachBeginning",y),y.isEnd&&!s&&y.emit("onReachEnd",y),y.params.watchSlidesProgress&&y.updateSlidesProgress(e),y.emit("onProgress",y,y.progress)},y.updateActiveIndex=function(){var e,a,t,s=y.rtl?y.translate:-y.translate;for(a=0;a<y.slidesGrid.length;a++)"undefined"!=typeof y.slidesGrid[a+1]?s>=y.slidesGrid[a]&&s<y.slidesGrid[a+1]-(y.slidesGrid[a+1]-y.slidesGrid[a])/2?e=a:s>=y.slidesGrid[a]&&s<y.slidesGrid[a+1]&&(e=a+1):s>=y.slidesGrid[a]&&(e=a);(0>e||"undefined"==typeof e)&&(e=0),t=Math.floor(e/y.params.slidesPerGroup),t>=y.snapGrid.length&&(t=y.snapGrid.length-1),e!==y.activeIndex&&(y.snapIndex=t,y.previousIndex=y.activeIndex,y.activeIndex=e,y.updateClasses())},y.updateClasses=function(){y.slides.removeClass(y.params.slideActiveClass+" "+y.params.slideNextClass+" "+y.params.slidePrevClass);var e=y.slides.eq(y.activeIndex);e.addClass(y.params.slideActiveClass);var t=e.next("."+y.params.slideClass).addClass(y.params.slideNextClass);y.params.loop&&0===t.length&&y.slides.eq(0).addClass(y.params.slideNextClass);var s=e.prev("."+y.params.slideClass).addClass(y.params.slidePrevClass);if(y.params.loop&&0===s.length&&y.slides.eq(-1).addClass(y.params.slidePrevClass),y.paginationContainer&&y.paginationContainer.length>0){var r,i=y.params.loop?Math.ceil((y.slides.length-2*y.loopedSlides)/y.params.slidesPerGroup):y.snapGrid.length;if(y.params.loop?(r=Math.ceil((y.activeIndex-y.loopedSlides)/y.params.slidesPerGroup),r>y.slides.length-1-2*y.loopedSlides&&(r-=y.slides.length-2*y.loopedSlides),r>i-1&&(r-=i),0>r&&"bullets"!==y.params.paginationType&&(r=i+r)):r="undefined"!=typeof y.snapIndex?y.snapIndex:y.activeIndex||0,"bullets"===y.params.paginationType&&y.bullets&&y.bullets.length>0&&(y.bullets.removeClass(y.params.bulletActiveClass),y.paginationContainer.length>1?y.bullets.each(function(){a(this).index()===r&&a(this).addClass(y.params.bulletActiveClass)}):y.bullets.eq(r).addClass(y.params.bulletActiveClass)),"fraction"===y.params.paginationType&&(y.paginationContainer.find("."+y.params.paginationCurrentClass).text(r+1),y.paginationContainer.find("."+y.params.paginationTotalClass).text(i)),"progress"===y.params.paginationType){var n=(r+1)/i,o=n,l=1;y.isHorizontal()||(l=n,o=1),y.paginationContainer.find("."+y.params.paginationProgressbarClass).transform("translate3d(0,0,0) scaleX("+o+") scaleY("+l+")").transition(y.params.speed)}"custom"===y.params.paginationType&&y.params.paginationCustomRender&&(y.paginationContainer.html(y.params.paginationCustomRender(y,r+1,i)),y.emit("onPaginationRendered",y,y.paginationContainer[0]))}y.params.loop||(y.params.prevButton&&y.prevButton&&y.prevButton.length>0&&(y.isBeginning?(y.prevButton.addClass(y.params.buttonDisabledClass),y.params.a11y&&y.a11y&&y.a11y.disable(y.prevButton)):(y.prevButton.removeClass(y.params.buttonDisabledClass),y.params.a11y&&y.a11y&&y.a11y.enable(y.prevButton))),y.params.nextButton&&y.nextButton&&y.nextButton.length>0&&(y.isEnd?(y.nextButton.addClass(y.params.buttonDisabledClass),y.params.a11y&&y.a11y&&y.a11y.disable(y.nextButton)):(y.nextButton.removeClass(y.params.buttonDisabledClass),y.params.a11y&&y.a11y&&y.a11y.enable(y.nextButton))))},y.updatePagination=function(){if(y.params.pagination&&y.paginationContainer&&y.paginationContainer.length>0){var e="";if("bullets"===y.params.paginationType){for(var a=y.params.loop?Math.ceil((y.slides.length-2*y.loopedSlides)/y.params.slidesPerGroup):y.snapGrid.length,t=0;a>t;t++)e+=y.params.paginationBulletRender?y.params.paginationBulletRender(t,y.params.bulletClass):"<"+y.params.paginationElement+' class="'+y.params.bulletClass+'"></'+y.params.paginationElement+">";y.paginationContainer.html(e),y.bullets=y.paginationContainer.find("."+y.params.bulletClass),y.params.paginationClickable&&y.params.a11y&&y.a11y&&y.a11y.initPagination()}"fraction"===y.params.paginationType&&(e=y.params.paginationFractionRender?y.params.paginationFractionRender(y,y.params.paginationCurrentClass,y.params.paginationTotalClass):'<span class="'+y.params.paginationCurrentClass+'"></span> / <span class="'+y.params.paginationTotalClass+'"></span>',y.paginationContainer.html(e)),"progress"===y.params.paginationType&&(e=y.params.paginationProgressRender?y.params.paginationProgressRender(y,y.params.paginationProgressbarClass):'<span class="'+y.params.paginationProgressbarClass+'"></span>',y.paginationContainer.html(e)),"custom"!==y.params.paginationType&&y.emit("onPaginationRendered",y,y.paginationContainer[0])}},y.update=function(e){function a(){s=Math.min(Math.max(y.translate,y.maxTranslate()),y.minTranslate()),y.setWrapperTranslate(s),y.updateActiveIndex(),y.updateClasses()}if(y.updateContainerSize(),y.updateSlidesSize(),y.updateProgress(),y.updatePagination(),y.updateClasses(),y.params.scrollbar&&y.scrollbar&&y.scrollbar.set(),e){var t,s;y.controller&&y.controller.spline&&(y.controller.spline=void 0),y.params.freeMode?(a(),y.params.autoHeight&&y.updateAutoHeight()):(t=("auto"===y.params.slidesPerView||y.params.slidesPerView>1)&&y.isEnd&&!y.params.centeredSlides?y.slideTo(y.slides.length-1,0,!1,!0):y.slideTo(y.activeIndex,0,!1,!0),t||a())}else y.params.autoHeight&&y.updateAutoHeight()},y.onResize=function(e){y.params.breakpoints&&y.setBreakpoint();var a=y.params.allowSwipeToPrev,t=y.params.allowSwipeToNext;y.params.allowSwipeToPrev=y.params.allowSwipeToNext=!0,y.updateContainerSize(),y.updateSlidesSize(),("auto"===y.params.slidesPerView||y.params.freeMode||e)&&y.updatePagination(),y.params.scrollbar&&y.scrollbar&&y.scrollbar.set(),y.controller&&y.controller.spline&&(y.controller.spline=void 0);var s=!1;if(y.params.freeMode){var r=Math.min(Math.max(y.translate,y.maxTranslate()),y.minTranslate());y.setWrapperTranslate(r),y.updateActiveIndex(),y.updateClasses(),y.params.autoHeight&&y.updateAutoHeight()}else y.updateClasses(),s=("auto"===y.params.slidesPerView||y.params.slidesPerView>1)&&y.isEnd&&!y.params.centeredSlides?y.slideTo(y.slides.length-1,0,!1,!0):y.slideTo(y.activeIndex,0,!1,!0);y.params.lazyLoading&&!s&&y.lazy&&y.lazy.load(),y.params.allowSwipeToPrev=a,y.params.allowSwipeToNext=t};var x=["mousedown","mousemove","mouseup"];window.navigator.pointerEnabled?x=["pointerdown","pointermove","pointerup"]:window.navigator.msPointerEnabled&&(x=["MSPointerDown","MSPointerMove","MSPointerUp"]),y.touchEvents={start:y.support.touch||!y.params.simulateTouch?"touchstart":x[0],move:y.support.touch||!y.params.simulateTouch?"touchmove":x[1],end:y.support.touch||!y.params.simulateTouch?"touchend":x[2]},(window.navigator.pointerEnabled||window.navigator.msPointerEnabled)&&("container"===y.params.touchEventsTarget?y.container:y.wrapper).addClass("swiper-wp8-"+y.params.direction),y.initEvents=function(e){var a=e?"off":"on",t=e?"removeEventListener":"addEventListener",r="container"===y.params.touchEventsTarget?y.container[0]:y.wrapper[0],i=y.support.touch?r:document,n=y.params.nested?!0:!1;y.browser.ie?(r[t](y.touchEvents.start,y.onTouchStart,!1),i[t](y.touchEvents.move,y.onTouchMove,n),i[t](y.touchEvents.end,y.onTouchEnd,!1)):(y.support.touch&&(r[t](y.touchEvents.start,y.onTouchStart,!1),r[t](y.touchEvents.move,y.onTouchMove,n),r[t](y.touchEvents.end,y.onTouchEnd,!1)),!s.simulateTouch||y.device.ios||y.device.android||(r[t]("mousedown",y.onTouchStart,!1),document[t]("mousemove",y.onTouchMove,n),document[t]("mouseup",y.onTouchEnd,!1))),window[t]("resize",y.onResize),y.params.nextButton&&y.nextButton&&y.nextButton.length>0&&(y.nextButton[a]("click",y.onClickNext),y.params.a11y&&y.a11y&&y.nextButton[a]("keydown",y.a11y.onEnterKey)),y.params.prevButton&&y.prevButton&&y.prevButton.length>0&&(y.prevButton[a]("click",y.onClickPrev),y.params.a11y&&y.a11y&&y.prevButton[a]("keydown",y.a11y.onEnterKey)),y.params.pagination&&y.params.paginationClickable&&(y.paginationContainer[a]("click","."+y.params.bulletClass,y.onClickIndex),y.params.a11y&&y.a11y&&y.paginationContainer[a]("keydown","."+y.params.bulletClass,y.a11y.onEnterKey)),(y.params.preventClicks||y.params.preventClicksPropagation)&&r[t]("click",y.preventClicks,!0)},y.attachEvents=function(){y.initEvents()},y.detachEvents=function(){y.initEvents(!0)},y.allowClick=!0,y.preventClicks=function(e){y.allowClick||(y.params.preventClicks&&e.preventDefault(),y.params.preventClicksPropagation&&y.animating&&(e.stopPropagation(),e.stopImmediatePropagation()))},y.onClickNext=function(e){e.preventDefault(),(!y.isEnd||y.params.loop)&&y.slideNext()},y.onClickPrev=function(e){e.preventDefault(),(!y.isBeginning||y.params.loop)&&y.slidePrev()},y.onClickIndex=function(e){e.preventDefault();var t=a(this).index()*y.params.slidesPerGroup;y.params.loop&&(t+=y.loopedSlides),y.slideTo(t)},y.updateClickedSlide=function(e){var t=n(e,"."+y.params.slideClass),s=!1;if(t)for(var r=0;r<y.slides.length;r++)y.slides[r]===t&&(s=!0);if(!t||!s)return y.clickedSlide=void 0,void(y.clickedIndex=void 0);if(y.clickedSlide=t,y.clickedIndex=a(t).index(),y.params.slideToClickedSlide&&void 0!==y.clickedIndex&&y.clickedIndex!==y.activeIndex){var i,o=y.clickedIndex;if(y.params.loop){if(y.animating)return;i=a(y.clickedSlide).attr("data-swiper-slide-index"),y.params.centeredSlides?o<y.loopedSlides-y.params.slidesPerView/2||o>y.slides.length-y.loopedSlides+y.params.slidesPerView/2?(y.fixLoop(),o=y.wrapper.children("."+y.params.slideClass+'[data-swiper-slide-index="'+i+'"]:not(.swiper-slide-duplicate)').eq(0).index(),setTimeout(function(){y.slideTo(o)},0)):y.slideTo(o):o>y.slides.length-y.params.slidesPerView?(y.fixLoop(),o=y.wrapper.children("."+y.params.slideClass+'[data-swiper-slide-index="'+i+'"]:not(.swiper-slide-duplicate)').eq(0).index(),setTimeout(function(){y.slideTo(o)},0)):y.slideTo(o)}else y.slideTo(o)}};var T,S,C,z,M,P,I,k,E,B,D="input, select, textarea, button",L=Date.now(),H=[];y.animating=!1,y.touches={startX:0,startY:0,currentX:0,currentY:0,diff:0};var G,A;if(y.onTouchStart=function(e){if(e.originalEvent&&(e=e.originalEvent),G="touchstart"===e.type,G||!("which"in e)||3!==e.which){if(y.params.noSwiping&&n(e,"."+y.params.noSwipingClass))return void(y.allowClick=!0);if(!y.params.swipeHandler||n(e,y.params.swipeHandler)){var t=y.touches.currentX="touchstart"===e.type?e.targetTouches[0].pageX:e.pageX,s=y.touches.currentY="touchstart"===e.type?e.targetTouches[0].pageY:e.pageY;if(!(y.device.ios&&y.params.iOSEdgeSwipeDetection&&t<=y.params.iOSEdgeSwipeThreshold)){if(T=!0,S=!1,C=!0,M=void 0,A=void 0,y.touches.startX=t,y.touches.startY=s,z=Date.now(),y.allowClick=!0,y.updateContainerSize(),y.swipeDirection=void 0,y.params.threshold>0&&(k=!1),"touchstart"!==e.type){var r=!0;a(e.target).is(D)&&(r=!1),document.activeElement&&a(document.activeElement).is(D)&&document.activeElement.blur(),r&&e.preventDefault()}y.emit("onTouchStart",y,e)}}}},y.onTouchMove=function(e){if(e.originalEvent&&(e=e.originalEvent),!G||"mousemove"!==e.type){if(e.preventedByNestedSwiper)return y.touches.startX="touchmove"===e.type?e.targetTouches[0].pageX:e.pageX,void(y.touches.startY="touchmove"===e.type?e.targetTouches[0].pageY:e.pageY);if(y.params.onlyExternal)return y.allowClick=!1,void(T&&(y.touches.startX=y.touches.currentX="touchmove"===e.type?e.targetTouches[0].pageX:e.pageX,y.touches.startY=y.touches.currentY="touchmove"===e.type?e.targetTouches[0].pageY:e.pageY,z=Date.now()));if(G&&document.activeElement&&e.target===document.activeElement&&a(e.target).is(D))return S=!0,void(y.allowClick=!1);if(C&&y.emit("onTouchMove",y,e),!(e.targetTouches&&e.targetTouches.length>1)){if(y.touches.currentX="touchmove"===e.type?e.targetTouches[0].pageX:e.pageX,y.touches.currentY="touchmove"===e.type?e.targetTouches[0].pageY:e.pageY,"undefined"==typeof M){var t=180*Math.atan2(Math.abs(y.touches.currentY-y.touches.startY),Math.abs(y.touches.currentX-y.touches.startX))/Math.PI;M=y.isHorizontal()?t>y.params.touchAngle:90-t>y.params.touchAngle}if(M&&y.emit("onTouchMoveOpposite",y,e),"undefined"==typeof A&&y.browser.ieTouch&&(y.touches.currentX!==y.touches.startX||y.touches.currentY!==y.touches.startY)&&(A=!0),T){if(M)return void(T=!1);if(A||!y.browser.ieTouch){y.allowClick=!1,y.emit("onSliderMove",y,e),e.preventDefault(),y.params.touchMoveStopPropagation&&!y.params.nested&&e.stopPropagation(),S||(s.loop&&y.fixLoop(),I=y.getWrapperTranslate(),y.setWrapperTransition(0),y.animating&&y.wrapper.trigger("webkitTransitionEnd transitionend oTransitionEnd MSTransitionEnd msTransitionEnd"),y.params.autoplay&&y.autoplaying&&(y.params.autoplayDisableOnInteraction?y.stopAutoplay():y.pauseAutoplay()),B=!1,y.params.grabCursor&&(y.container[0].style.cursor="move",y.container[0].style.cursor="-webkit-grabbing",y.container[0].style.cursor="-moz-grabbin",y.container[0].style.cursor="grabbing")),S=!0;var r=y.touches.diff=y.isHorizontal()?y.touches.currentX-y.touches.startX:y.touches.currentY-y.touches.startY;r*=y.params.touchRatio,y.rtl&&(r=-r),y.swipeDirection=r>0?"prev":"next",P=r+I;var i=!0;if(r>0&&P>y.minTranslate()?(i=!1,y.params.resistance&&(P=y.minTranslate()-1+Math.pow(-y.minTranslate()+I+r,y.params.resistanceRatio))):0>r&&P<y.maxTranslate()&&(i=!1,y.params.resistance&&(P=y.maxTranslate()+1-Math.pow(y.maxTranslate()-I-r,y.params.resistanceRatio))),
i&&(e.preventedByNestedSwiper=!0),!y.params.allowSwipeToNext&&"next"===y.swipeDirection&&I>P&&(P=I),!y.params.allowSwipeToPrev&&"prev"===y.swipeDirection&&P>I&&(P=I),y.params.followFinger){if(y.params.threshold>0){if(!(Math.abs(r)>y.params.threshold||k))return void(P=I);if(!k)return k=!0,y.touches.startX=y.touches.currentX,y.touches.startY=y.touches.currentY,P=I,void(y.touches.diff=y.isHorizontal()?y.touches.currentX-y.touches.startX:y.touches.currentY-y.touches.startY)}(y.params.freeMode||y.params.watchSlidesProgress)&&y.updateActiveIndex(),y.params.freeMode&&(0===H.length&&H.push({position:y.touches[y.isHorizontal()?"startX":"startY"],time:z}),H.push({position:y.touches[y.isHorizontal()?"currentX":"currentY"],time:(new window.Date).getTime()})),y.updateProgress(P),y.setWrapperTranslate(P)}}}}}},y.onTouchEnd=function(e){if(e.originalEvent&&(e=e.originalEvent),C&&y.emit("onTouchEnd",y,e),C=!1,T){y.params.grabCursor&&S&&T&&(y.container[0].style.cursor="move",y.container[0].style.cursor="-webkit-grab",y.container[0].style.cursor="-moz-grab",y.container[0].style.cursor="grab");var t=Date.now(),s=t-z;if(y.allowClick&&(y.updateClickedSlide(e),y.emit("onTap",y,e),300>s&&t-L>300&&(E&&clearTimeout(E),E=setTimeout(function(){y&&(y.params.paginationHide&&y.paginationContainer.length>0&&!a(e.target).hasClass(y.params.bulletClass)&&y.paginationContainer.toggleClass(y.params.paginationHiddenClass),y.emit("onClick",y,e))},300)),300>s&&300>t-L&&(E&&clearTimeout(E),y.emit("onDoubleTap",y,e))),L=Date.now(),setTimeout(function(){y&&(y.allowClick=!0)},0),!T||!S||!y.swipeDirection||0===y.touches.diff||P===I)return void(T=S=!1);T=S=!1;var r;if(r=y.params.followFinger?y.rtl?y.translate:-y.translate:-P,y.params.freeMode){if(r<-y.minTranslate())return void y.slideTo(y.activeIndex);if(r>-y.maxTranslate())return void(y.slides.length<y.snapGrid.length?y.slideTo(y.snapGrid.length-1):y.slideTo(y.slides.length-1));if(y.params.freeModeMomentum){if(H.length>1){var i=H.pop(),n=H.pop(),o=i.position-n.position,l=i.time-n.time;y.velocity=o/l,y.velocity=y.velocity/2,Math.abs(y.velocity)<y.params.freeModeMinimumVelocity&&(y.velocity=0),(l>150||(new window.Date).getTime()-i.time>300)&&(y.velocity=0)}else y.velocity=0;H.length=0;var p=1e3*y.params.freeModeMomentumRatio,d=y.velocity*p,u=y.translate+d;y.rtl&&(u=-u);var c,m=!1,f=20*Math.abs(y.velocity)*y.params.freeModeMomentumBounceRatio;if(u<y.maxTranslate())y.params.freeModeMomentumBounce?(u+y.maxTranslate()<-f&&(u=y.maxTranslate()-f),c=y.maxTranslate(),m=!0,B=!0):u=y.maxTranslate();else if(u>y.minTranslate())y.params.freeModeMomentumBounce?(u-y.minTranslate()>f&&(u=y.minTranslate()+f),c=y.minTranslate(),m=!0,B=!0):u=y.minTranslate();else if(y.params.freeModeSticky){var g,h=0;for(h=0;h<y.snapGrid.length;h+=1)if(y.snapGrid[h]>-u){g=h;break}u=Math.abs(y.snapGrid[g]-u)<Math.abs(y.snapGrid[g-1]-u)||"next"===y.swipeDirection?y.snapGrid[g]:y.snapGrid[g-1],y.rtl||(u=-u)}if(0!==y.velocity)p=y.rtl?Math.abs((-u-y.translate)/y.velocity):Math.abs((u-y.translate)/y.velocity);else if(y.params.freeModeSticky)return void y.slideReset();y.params.freeModeMomentumBounce&&m?(y.updateProgress(c),y.setWrapperTransition(p),y.setWrapperTranslate(u),y.onTransitionStart(),y.animating=!0,y.wrapper.transitionEnd(function(){y&&B&&(y.emit("onMomentumBounce",y),y.setWrapperTransition(y.params.speed),y.setWrapperTranslate(c),y.wrapper.transitionEnd(function(){y&&y.onTransitionEnd()}))})):y.velocity?(y.updateProgress(u),y.setWrapperTransition(p),y.setWrapperTranslate(u),y.onTransitionStart(),y.animating||(y.animating=!0,y.wrapper.transitionEnd(function(){y&&y.onTransitionEnd()}))):y.updateProgress(u),y.updateActiveIndex()}return void((!y.params.freeModeMomentum||s>=y.params.longSwipesMs)&&(y.updateProgress(),y.updateActiveIndex()))}var v,w=0,b=y.slidesSizesGrid[0];for(v=0;v<y.slidesGrid.length;v+=y.params.slidesPerGroup)"undefined"!=typeof y.slidesGrid[v+y.params.slidesPerGroup]?r>=y.slidesGrid[v]&&r<y.slidesGrid[v+y.params.slidesPerGroup]&&(w=v,b=y.slidesGrid[v+y.params.slidesPerGroup]-y.slidesGrid[v]):r>=y.slidesGrid[v]&&(w=v,b=y.slidesGrid[y.slidesGrid.length-1]-y.slidesGrid[y.slidesGrid.length-2]);var x=(r-y.slidesGrid[w])/b;if(s>y.params.longSwipesMs){if(!y.params.longSwipes)return void y.slideTo(y.activeIndex);"next"===y.swipeDirection&&(x>=y.params.longSwipesRatio?y.slideTo(w+y.params.slidesPerGroup):y.slideTo(w)),"prev"===y.swipeDirection&&(x>1-y.params.longSwipesRatio?y.slideTo(w+y.params.slidesPerGroup):y.slideTo(w))}else{if(!y.params.shortSwipes)return void y.slideTo(y.activeIndex);"next"===y.swipeDirection&&y.slideTo(w+y.params.slidesPerGroup),"prev"===y.swipeDirection&&y.slideTo(w)}}},y._slideTo=function(e,a){return y.slideTo(e,a,!0,!0)},y.slideTo=function(e,a,t,s){"undefined"==typeof t&&(t=!0),"undefined"==typeof e&&(e=0),0>e&&(e=0),y.snapIndex=Math.floor(e/y.params.slidesPerGroup),y.snapIndex>=y.snapGrid.length&&(y.snapIndex=y.snapGrid.length-1);var r=-y.snapGrid[y.snapIndex];y.params.autoplay&&y.autoplaying&&(s||!y.params.autoplayDisableOnInteraction?y.pauseAutoplay(a):y.stopAutoplay()),y.updateProgress(r);for(var i=0;i<y.slidesGrid.length;i++)-Math.floor(100*r)>=Math.floor(100*y.slidesGrid[i])&&(e=i);return!y.params.allowSwipeToNext&&r<y.translate&&r<y.minTranslate()?!1:!y.params.allowSwipeToPrev&&r>y.translate&&r>y.maxTranslate()&&(y.activeIndex||0)!==e?!1:("undefined"==typeof a&&(a=y.params.speed),y.previousIndex=y.activeIndex||0,y.activeIndex=e,y.rtl&&-r===y.translate||!y.rtl&&r===y.translate?(y.params.autoHeight&&y.updateAutoHeight(),y.updateClasses(),"slide"!==y.params.effect&&y.setWrapperTranslate(r),!1):(y.updateClasses(),y.onTransitionStart(t),0===a?(y.setWrapperTranslate(r),y.setWrapperTransition(0),y.onTransitionEnd(t)):(y.setWrapperTranslate(r),y.setWrapperTransition(a),y.animating||(y.animating=!0,y.wrapper.transitionEnd(function(){y&&y.onTransitionEnd(t)}))),!0))},y.onTransitionStart=function(e){"undefined"==typeof e&&(e=!0),y.params.autoHeight&&y.updateAutoHeight(),y.lazy&&y.lazy.onTransitionStart(),e&&(y.emit("onTransitionStart",y),y.activeIndex!==y.previousIndex&&(y.emit("onSlideChangeStart",y),y.activeIndex>y.previousIndex?y.emit("onSlideNextStart",y):y.emit("onSlidePrevStart",y)))},y.onTransitionEnd=function(e){y.animating=!1,y.setWrapperTransition(0),"undefined"==typeof e&&(e=!0),y.lazy&&y.lazy.onTransitionEnd(),e&&(y.emit("onTransitionEnd",y),y.activeIndex!==y.previousIndex&&(y.emit("onSlideChangeEnd",y),y.activeIndex>y.previousIndex?y.emit("onSlideNextEnd",y):y.emit("onSlidePrevEnd",y))),y.params.hashnav&&y.hashnav&&y.hashnav.setHash()},y.slideNext=function(e,a,t){if(y.params.loop){if(y.animating)return!1;y.fixLoop();y.container[0].clientLeft;return y.slideTo(y.activeIndex+y.params.slidesPerGroup,a,e,t)}return y.slideTo(y.activeIndex+y.params.slidesPerGroup,a,e,t)},y._slideNext=function(e){return y.slideNext(!0,e,!0)},y.slidePrev=function(e,a,t){if(y.params.loop){if(y.animating)return!1;y.fixLoop();y.container[0].clientLeft;return y.slideTo(y.activeIndex-1,a,e,t)}return y.slideTo(y.activeIndex-1,a,e,t)},y._slidePrev=function(e){return y.slidePrev(!0,e,!0)},y.slideReset=function(e,a,t){return y.slideTo(y.activeIndex,a,e)},y.setWrapperTransition=function(e,a){y.wrapper.transition(e),"slide"!==y.params.effect&&y.effects[y.params.effect]&&y.effects[y.params.effect].setTransition(e),y.params.parallax&&y.parallax&&y.parallax.setTransition(e),y.params.scrollbar&&y.scrollbar&&y.scrollbar.setTransition(e),y.params.control&&y.controller&&y.controller.setTransition(e,a),y.emit("onSetTransition",y,e)},y.setWrapperTranslate=function(e,a,t){var s=0,i=0,n=0;y.isHorizontal()?s=y.rtl?-e:e:i=e,y.params.roundLengths&&(s=r(s),i=r(i)),y.params.virtualTranslate||(y.support.transforms3d?y.wrapper.transform("translate3d("+s+"px, "+i+"px, "+n+"px)"):y.wrapper.transform("translate("+s+"px, "+i+"px)")),y.translate=y.isHorizontal()?s:i;var o,l=y.maxTranslate()-y.minTranslate();o=0===l?0:(e-y.minTranslate())/l,o!==y.progress&&y.updateProgress(e),a&&y.updateActiveIndex(),"slide"!==y.params.effect&&y.effects[y.params.effect]&&y.effects[y.params.effect].setTranslate(y.translate),y.params.parallax&&y.parallax&&y.parallax.setTranslate(y.translate),y.params.scrollbar&&y.scrollbar&&y.scrollbar.setTranslate(y.translate),y.params.control&&y.controller&&y.controller.setTranslate(y.translate,t),y.emit("onSetTranslate",y,y.translate)},y.getTranslate=function(e,a){var t,s,r,i;return"undefined"==typeof a&&(a="x"),y.params.virtualTranslate?y.rtl?-y.translate:y.translate:(r=window.getComputedStyle(e,null),window.WebKitCSSMatrix?(s=r.transform||r.webkitTransform,s.split(",").length>6&&(s=s.split(", ").map(function(e){return e.replace(",",".")}).join(", ")),i=new window.WebKitCSSMatrix("none"===s?"":s)):(i=r.MozTransform||r.OTransform||r.MsTransform||r.msTransform||r.transform||r.getPropertyValue("transform").replace("translate(","matrix(1, 0, 0, 1,"),t=i.toString().split(",")),"x"===a&&(s=window.WebKitCSSMatrix?i.m41:16===t.length?parseFloat(t[12]):parseFloat(t[4])),"y"===a&&(s=window.WebKitCSSMatrix?i.m42:16===t.length?parseFloat(t[13]):parseFloat(t[5])),y.rtl&&s&&(s=-s),s||0)},y.getWrapperTranslate=function(e){return"undefined"==typeof e&&(e=y.isHorizontal()?"x":"y"),y.getTranslate(y.wrapper[0],e)},y.observers=[],y.initObservers=function(){if(y.params.observeParents)for(var e=y.container.parents(),a=0;a<e.length;a++)o(e[a]);o(y.container[0],{childList:!1}),o(y.wrapper[0],{attributes:!1})},y.disconnectObservers=function(){for(var e=0;e<y.observers.length;e++)y.observers[e].disconnect();y.observers=[]},y.createLoop=function(){y.wrapper.children("."+y.params.slideClass+"."+y.params.slideDuplicateClass).remove();var e=y.wrapper.children("."+y.params.slideClass);"auto"!==y.params.slidesPerView||y.params.loopedSlides||(y.params.loopedSlides=e.length),y.loopedSlides=parseInt(y.params.loopedSlides||y.params.slidesPerView,10),y.loopedSlides=y.loopedSlides+y.params.loopAdditionalSlides,y.loopedSlides>e.length&&(y.loopedSlides=e.length);var t,s=[],r=[];for(e.each(function(t,i){var n=a(this);t<y.loopedSlides&&r.push(i),t<e.length&&t>=e.length-y.loopedSlides&&s.push(i),n.attr("data-swiper-slide-index",t)}),t=0;t<r.length;t++)y.wrapper.append(a(r[t].cloneNode(!0)).addClass(y.params.slideDuplicateClass));for(t=s.length-1;t>=0;t--)y.wrapper.prepend(a(s[t].cloneNode(!0)).addClass(y.params.slideDuplicateClass))},y.destroyLoop=function(){y.wrapper.children("."+y.params.slideClass+"."+y.params.slideDuplicateClass).remove(),y.slides.removeAttr("data-swiper-slide-index")},y.reLoop=function(e){var a=y.activeIndex-y.loopedSlides;y.destroyLoop(),y.createLoop(),y.updateSlidesSize(),e&&y.slideTo(a+y.loopedSlides,0,!1)},y.fixLoop=function(){var e;y.activeIndex<y.loopedSlides?(e=y.slides.length-3*y.loopedSlides+y.activeIndex,e+=y.loopedSlides,y.slideTo(e,0,!1,!0)):("auto"===y.params.slidesPerView&&y.activeIndex>=2*y.loopedSlides||y.activeIndex>y.slides.length-2*y.params.slidesPerView)&&(e=-y.slides.length+y.activeIndex+y.loopedSlides,e+=y.loopedSlides,y.slideTo(e,0,!1,!0))},y.appendSlide=function(e){if(y.params.loop&&y.destroyLoop(),"object"==typeof e&&e.length)for(var a=0;a<e.length;a++)e[a]&&y.wrapper.append(e[a]);else y.wrapper.append(e);y.params.loop&&y.createLoop(),y.params.observer&&y.support.observer||y.update(!0)},y.prependSlide=function(e){y.params.loop&&y.destroyLoop();var a=y.activeIndex+1;if("object"==typeof e&&e.length){for(var t=0;t<e.length;t++)e[t]&&y.wrapper.prepend(e[t]);a=y.activeIndex+e.length}else y.wrapper.prepend(e);y.params.loop&&y.createLoop(),y.params.observer&&y.support.observer||y.update(!0),y.slideTo(a,0,!1)},y.removeSlide=function(e){y.params.loop&&(y.destroyLoop(),y.slides=y.wrapper.children("."+y.params.slideClass));var a,t=y.activeIndex;if("object"==typeof e&&e.length){for(var s=0;s<e.length;s++)a=e[s],y.slides[a]&&y.slides.eq(a).remove(),t>a&&t--;t=Math.max(t,0)}else a=e,y.slides[a]&&y.slides.eq(a).remove(),t>a&&t--,t=Math.max(t,0);y.params.loop&&y.createLoop(),y.params.observer&&y.support.observer||y.update(!0),y.params.loop?y.slideTo(t+y.loopedSlides,0,!1):y.slideTo(t,0,!1)},y.removeAllSlides=function(){for(var e=[],a=0;a<y.slides.length;a++)e.push(a);y.removeSlide(e)},y.effects={fade:{setTranslate:function(){for(var e=0;e<y.slides.length;e++){var a=y.slides.eq(e),t=a[0].swiperSlideOffset,s=-t;y.params.virtualTranslate||(s-=y.translate);var r=0;y.isHorizontal()||(r=s,s=0);var i=y.params.fade.crossFade?Math.max(1-Math.abs(a[0].progress),0):1+Math.min(Math.max(a[0].progress,-1),0);a.css({opacity:i}).transform("translate3d("+s+"px, "+r+"px, 0px)")}},setTransition:function(e){if(y.slides.transition(e),y.params.virtualTranslate&&0!==e){var a=!1;y.slides.transitionEnd(function(){if(!a&&y){a=!0,y.animating=!1;for(var e=["webkitTransitionEnd","transitionend","oTransitionEnd","MSTransitionEnd","msTransitionEnd"],t=0;t<e.length;t++)y.wrapper.trigger(e[t])}})}}},flip:{setTranslate:function(){for(var e=0;e<y.slides.length;e++){var t=y.slides.eq(e),s=t[0].progress;y.params.flip.limitRotation&&(s=Math.max(Math.min(t[0].progress,1),-1));var r=t[0].swiperSlideOffset,i=-180*s,n=i,o=0,l=-r,p=0;if(y.isHorizontal()?y.rtl&&(n=-n):(p=l,l=0,o=-n,n=0),t[0].style.zIndex=-Math.abs(Math.round(s))+y.slides.length,y.params.flip.slideShadows){var d=y.isHorizontal()?t.find(".swiper-slide-shadow-left"):t.find(".swiper-slide-shadow-top"),u=y.isHorizontal()?t.find(".swiper-slide-shadow-right"):t.find(".swiper-slide-shadow-bottom");0===d.length&&(d=a('<div class="swiper-slide-shadow-'+(y.isHorizontal()?"left":"top")+'"></div>'),t.append(d)),0===u.length&&(u=a('<div class="swiper-slide-shadow-'+(y.isHorizontal()?"right":"bottom")+'"></div>'),t.append(u)),d.length&&(d[0].style.opacity=Math.max(-s,0)),u.length&&(u[0].style.opacity=Math.max(s,0))}t.transform("translate3d("+l+"px, "+p+"px, 0px) rotateX("+o+"deg) rotateY("+n+"deg)")}},setTransition:function(e){if(y.slides.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e),y.params.virtualTranslate&&0!==e){var t=!1;y.slides.eq(y.activeIndex).transitionEnd(function(){if(!t&&y&&a(this).hasClass(y.params.slideActiveClass)){t=!0,y.animating=!1;for(var e=["webkitTransitionEnd","transitionend","oTransitionEnd","MSTransitionEnd","msTransitionEnd"],s=0;s<e.length;s++)y.wrapper.trigger(e[s])}})}}},cube:{setTranslate:function(){var e,t=0;y.params.cube.shadow&&(y.isHorizontal()?(e=y.wrapper.find(".swiper-cube-shadow"),0===e.length&&(e=a('<div class="swiper-cube-shadow"></div>'),y.wrapper.append(e)),e.css({height:y.width+"px"})):(e=y.container.find(".swiper-cube-shadow"),0===e.length&&(e=a('<div class="swiper-cube-shadow"></div>'),y.container.append(e))));for(var s=0;s<y.slides.length;s++){var r=y.slides.eq(s),i=90*s,n=Math.floor(i/360);y.rtl&&(i=-i,n=Math.floor(-i/360));var o=Math.max(Math.min(r[0].progress,1),-1),l=0,p=0,d=0;s%4===0?(l=4*-n*y.size,d=0):(s-1)%4===0?(l=0,d=4*-n*y.size):(s-2)%4===0?(l=y.size+4*n*y.size,d=y.size):(s-3)%4===0&&(l=-y.size,d=3*y.size+4*y.size*n),y.rtl&&(l=-l),y.isHorizontal()||(p=l,l=0);var u="rotateX("+(y.isHorizontal()?0:-i)+"deg) rotateY("+(y.isHorizontal()?i:0)+"deg) translate3d("+l+"px, "+p+"px, "+d+"px)";if(1>=o&&o>-1&&(t=90*s+90*o,y.rtl&&(t=90*-s-90*o)),r.transform(u),y.params.cube.slideShadows){var c=y.isHorizontal()?r.find(".swiper-slide-shadow-left"):r.find(".swiper-slide-shadow-top"),m=y.isHorizontal()?r.find(".swiper-slide-shadow-right"):r.find(".swiper-slide-shadow-bottom");0===c.length&&(c=a('<div class="swiper-slide-shadow-'+(y.isHorizontal()?"left":"top")+'"></div>'),r.append(c)),0===m.length&&(m=a('<div class="swiper-slide-shadow-'+(y.isHorizontal()?"right":"bottom")+'"></div>'),r.append(m)),c.length&&(c[0].style.opacity=Math.max(-o,0)),m.length&&(m[0].style.opacity=Math.max(o,0))}}if(y.wrapper.css({"-webkit-transform-origin":"50% 50% -"+y.size/2+"px","-moz-transform-origin":"50% 50% -"+y.size/2+"px","-ms-transform-origin":"50% 50% -"+y.size/2+"px","transform-origin":"50% 50% -"+y.size/2+"px"}),y.params.cube.shadow)if(y.isHorizontal())e.transform("translate3d(0px, "+(y.width/2+y.params.cube.shadowOffset)+"px, "+-y.width/2+"px) rotateX(90deg) rotateZ(0deg) scale("+y.params.cube.shadowScale+")");else{var f=Math.abs(t)-90*Math.floor(Math.abs(t)/90),g=1.5-(Math.sin(2*f*Math.PI/360)/2+Math.cos(2*f*Math.PI/360)/2),h=y.params.cube.shadowScale,v=y.params.cube.shadowScale/g,w=y.params.cube.shadowOffset;e.transform("scale3d("+h+", 1, "+v+") translate3d(0px, "+(y.height/2+w)+"px, "+-y.height/2/v+"px) rotateX(-90deg)")}var b=y.isSafari||y.isUiWebView?-y.size/2:0;y.wrapper.transform("translate3d(0px,0,"+b+"px) rotateX("+(y.isHorizontal()?0:t)+"deg) rotateY("+(y.isHorizontal()?-t:0)+"deg)")},setTransition:function(e){y.slides.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e),y.params.cube.shadow&&!y.isHorizontal()&&y.container.find(".swiper-cube-shadow").transition(e)}},coverflow:{setTranslate:function(){for(var e=y.translate,t=y.isHorizontal()?-e+y.width/2:-e+y.height/2,s=y.isHorizontal()?y.params.coverflow.rotate:-y.params.coverflow.rotate,r=y.params.coverflow.depth,i=0,n=y.slides.length;n>i;i++){var o=y.slides.eq(i),l=y.slidesSizesGrid[i],p=o[0].swiperSlideOffset,d=(t-p-l/2)/l*y.params.coverflow.modifier,u=y.isHorizontal()?s*d:0,c=y.isHorizontal()?0:s*d,m=-r*Math.abs(d),f=y.isHorizontal()?0:y.params.coverflow.stretch*d,g=y.isHorizontal()?y.params.coverflow.stretch*d:0;Math.abs(g)<.001&&(g=0),Math.abs(f)<.001&&(f=0),Math.abs(m)<.001&&(m=0),Math.abs(u)<.001&&(u=0),Math.abs(c)<.001&&(c=0);var h="translate3d("+g+"px,"+f+"px,"+m+"px)  rotateX("+c+"deg) rotateY("+u+"deg)";if(o.transform(h),o[0].style.zIndex=-Math.abs(Math.round(d))+1,y.params.coverflow.slideShadows){var v=y.isHorizontal()?o.find(".swiper-slide-shadow-left"):o.find(".swiper-slide-shadow-top"),w=y.isHorizontal()?o.find(".swiper-slide-shadow-right"):o.find(".swiper-slide-shadow-bottom");0===v.length&&(v=a('<div class="swiper-slide-shadow-'+(y.isHorizontal()?"left":"top")+'"></div>'),o.append(v)),0===w.length&&(w=a('<div class="swiper-slide-shadow-'+(y.isHorizontal()?"right":"bottom")+'"></div>'),o.append(w)),v.length&&(v[0].style.opacity=d>0?d:0),w.length&&(w[0].style.opacity=-d>0?-d:0)}}if(y.browser.ie){var b=y.wrapper[0].style;b.perspectiveOrigin=t+"px 50%"}},setTransition:function(e){y.slides.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e)}}},y.lazy={initialImageLoaded:!1,loadImageInSlide:function(e,t){if("undefined"!=typeof e&&("undefined"==typeof t&&(t=!0),0!==y.slides.length)){var s=y.slides.eq(e),r=s.find(".swiper-lazy:not(.swiper-lazy-loaded):not(.swiper-lazy-loading)");!s.hasClass("swiper-lazy")||s.hasClass("swiper-lazy-loaded")||s.hasClass("swiper-lazy-loading")||(r=r.add(s[0])),0!==r.length&&r.each(function(){var e=a(this);e.addClass("swiper-lazy-loading");var r=e.attr("data-background"),i=e.attr("data-src"),n=e.attr("data-srcset");y.loadImage(e[0],i||r,n,!1,function(){if(r?(e.css("background-image",'url("'+r+'")'),e.removeAttr("data-background")):(n&&(e.attr("srcset",n),e.removeAttr("data-srcset")),i&&(e.attr("src",i),e.removeAttr("data-src"))),e.addClass("swiper-lazy-loaded").removeClass("swiper-lazy-loading"),s.find(".swiper-lazy-preloader, .preloader").remove(),y.params.loop&&t){var a=s.attr("data-swiper-slide-index");if(s.hasClass(y.params.slideDuplicateClass)){var o=y.wrapper.children('[data-swiper-slide-index="'+a+'"]:not(.'+y.params.slideDuplicateClass+")");y.lazy.loadImageInSlide(o.index(),!1)}else{var l=y.wrapper.children("."+y.params.slideDuplicateClass+'[data-swiper-slide-index="'+a+'"]');y.lazy.loadImageInSlide(l.index(),!1)}}y.emit("onLazyImageReady",y,s[0],e[0])}),y.emit("onLazyImageLoad",y,s[0],e[0])})}},load:function(){var e;if(y.params.watchSlidesVisibility)y.wrapper.children("."+y.params.slideVisibleClass).each(function(){y.lazy.loadImageInSlide(a(this).index())});else if(y.params.slidesPerView>1)for(e=y.activeIndex;e<y.activeIndex+y.params.slidesPerView;e++)y.slides[e]&&y.lazy.loadImageInSlide(e);else y.lazy.loadImageInSlide(y.activeIndex);if(y.params.lazyLoadingInPrevNext)if(y.params.slidesPerView>1||y.params.lazyLoadingInPrevNextAmount&&y.params.lazyLoadingInPrevNextAmount>1){var t=y.params.lazyLoadingInPrevNextAmount,s=y.params.slidesPerView,r=Math.min(y.activeIndex+s+Math.max(t,s),y.slides.length),i=Math.max(y.activeIndex-Math.max(s,t),0);for(e=y.activeIndex+y.params.slidesPerView;r>e;e++)y.slides[e]&&y.lazy.loadImageInSlide(e);for(e=i;e<y.activeIndex;e++)y.slides[e]&&y.lazy.loadImageInSlide(e)}else{var n=y.wrapper.children("."+y.params.slideNextClass);n.length>0&&y.lazy.loadImageInSlide(n.index());var o=y.wrapper.children("."+y.params.slidePrevClass);o.length>0&&y.lazy.loadImageInSlide(o.index())}},onTransitionStart:function(){y.params.lazyLoading&&(y.params.lazyLoadingOnTransitionStart||!y.params.lazyLoadingOnTransitionStart&&!y.lazy.initialImageLoaded)&&y.lazy.load()},onTransitionEnd:function(){y.params.lazyLoading&&!y.params.lazyLoadingOnTransitionStart&&y.lazy.load()}},y.scrollbar={isTouched:!1,setDragPosition:function(e){var a=y.scrollbar,t=y.isHorizontal()?"touchstart"===e.type||"touchmove"===e.type?e.targetTouches[0].pageX:e.pageX||e.clientX:"touchstart"===e.type||"touchmove"===e.type?e.targetTouches[0].pageY:e.pageY||e.clientY,s=t-a.track.offset()[y.isHorizontal()?"left":"top"]-a.dragSize/2,r=-y.minTranslate()*a.moveDivider,i=-y.maxTranslate()*a.moveDivider;r>s?s=r:s>i&&(s=i),s=-s/a.moveDivider,y.updateProgress(s),y.setWrapperTranslate(s,!0)},dragStart:function(e){var a=y.scrollbar;a.isTouched=!0,e.preventDefault(),e.stopPropagation(),a.setDragPosition(e),clearTimeout(a.dragTimeout),a.track.transition(0),y.params.scrollbarHide&&a.track.css("opacity",1),y.wrapper.transition(100),a.drag.transition(100),y.emit("onScrollbarDragStart",y)},dragMove:function(e){var a=y.scrollbar;a.isTouched&&(e.preventDefault?e.preventDefault():e.returnValue=!1,a.setDragPosition(e),y.wrapper.transition(0),a.track.transition(0),a.drag.transition(0),y.emit("onScrollbarDragMove",y))},dragEnd:function(e){var a=y.scrollbar;a.isTouched&&(a.isTouched=!1,y.params.scrollbarHide&&(clearTimeout(a.dragTimeout),a.dragTimeout=setTimeout(function(){a.track.css("opacity",0),a.track.transition(400)},1e3)),y.emit("onScrollbarDragEnd",y),y.params.scrollbarSnapOnRelease&&y.slideReset())},enableDraggable:function(){var e=y.scrollbar,t=y.support.touch?e.track:document;a(e.track).on(y.touchEvents.start,e.dragStart),a(t).on(y.touchEvents.move,e.dragMove),a(t).on(y.touchEvents.end,e.dragEnd)},disableDraggable:function(){var e=y.scrollbar,t=y.support.touch?e.track:document;a(e.track).off(y.touchEvents.start,e.dragStart),a(t).off(y.touchEvents.move,e.dragMove),a(t).off(y.touchEvents.end,e.dragEnd)},set:function(){if(y.params.scrollbar){var e=y.scrollbar;e.track=a(y.params.scrollbar),y.params.uniqueNavElements&&"string"==typeof y.params.scrollbar&&e.track.length>1&&1===y.container.find(y.params.scrollbar).length&&(e.track=y.container.find(y.params.scrollbar)),e.drag=e.track.find(".swiper-scrollbar-drag"),0===e.drag.length&&(e.drag=a('<div class="swiper-scrollbar-drag"></div>'),e.track.append(e.drag)),e.drag[0].style.width="",e.drag[0].style.height="",e.trackSize=y.isHorizontal()?e.track[0].offsetWidth:e.track[0].offsetHeight,e.divider=y.size/y.virtualSize,e.moveDivider=e.divider*(e.trackSize/y.size),e.dragSize=e.trackSize*e.divider,y.isHorizontal()?e.drag[0].style.width=e.dragSize+"px":e.drag[0].style.height=e.dragSize+"px",e.divider>=1?e.track[0].style.display="none":e.track[0].style.display="",y.params.scrollbarHide&&(e.track[0].style.opacity=0)}},setTranslate:function(){if(y.params.scrollbar){var e,a=y.scrollbar,t=(y.translate||0,a.dragSize);e=(a.trackSize-a.dragSize)*y.progress,y.rtl&&y.isHorizontal()?(e=-e,e>0?(t=a.dragSize-e,e=0):-e+a.dragSize>a.trackSize&&(t=a.trackSize+e)):0>e?(t=a.dragSize+e,e=0):e+a.dragSize>a.trackSize&&(t=a.trackSize-e),y.isHorizontal()?(y.support.transforms3d?a.drag.transform("translate3d("+e+"px, 0, 0)"):a.drag.transform("translateX("+e+"px)"),a.drag[0].style.width=t+"px"):(y.support.transforms3d?a.drag.transform("translate3d(0px, "+e+"px, 0)"):a.drag.transform("translateY("+e+"px)"),a.drag[0].style.height=t+"px"),y.params.scrollbarHide&&(clearTimeout(a.timeout),a.track[0].style.opacity=1,a.timeout=setTimeout(function(){a.track[0].style.opacity=0,a.track.transition(400)},1e3))}},setTransition:function(e){y.params.scrollbar&&y.scrollbar.drag.transition(e)}},y.controller={LinearSpline:function(e,a){this.x=e,this.y=a,this.lastIndex=e.length-1;var t,s;this.x.length;this.interpolate=function(e){return e?(s=r(this.x,e),t=s-1,(e-this.x[t])*(this.y[s]-this.y[t])/(this.x[s]-this.x[t])+this.y[t]):0};var r=function(){var e,a,t;return function(s,r){for(a=-1,e=s.length;e-a>1;)s[t=e+a>>1]<=r?a=t:e=t;return e}}()},getInterpolateFunction:function(e){y.controller.spline||(y.controller.spline=y.params.loop?new y.controller.LinearSpline(y.slidesGrid,e.slidesGrid):new y.controller.LinearSpline(y.snapGrid,e.snapGrid))},setTranslate:function(e,a){function s(a){e=a.rtl&&"horizontal"===a.params.direction?-y.translate:y.translate,"slide"===y.params.controlBy&&(y.controller.getInterpolateFunction(a),i=-y.controller.spline.interpolate(-e)),i&&"container"!==y.params.controlBy||(r=(a.maxTranslate()-a.minTranslate())/(y.maxTranslate()-y.minTranslate()),i=(e-y.minTranslate())*r+a.minTranslate()),y.params.controlInverse&&(i=a.maxTranslate()-i),a.updateProgress(i),a.setWrapperTranslate(i,!1,y),a.updateActiveIndex()}var r,i,n=y.params.control;if(y.isArray(n))for(var o=0;o<n.length;o++)n[o]!==a&&n[o]instanceof t&&s(n[o]);else n instanceof t&&a!==n&&s(n)},setTransition:function(e,a){function s(a){a.setWrapperTransition(e,y),0!==e&&(a.onTransitionStart(),a.wrapper.transitionEnd(function(){i&&(a.params.loop&&"slide"===y.params.controlBy&&a.fixLoop(),a.onTransitionEnd())}))}var r,i=y.params.control;if(y.isArray(i))for(r=0;r<i.length;r++)i[r]!==a&&i[r]instanceof t&&s(i[r]);else i instanceof t&&a!==i&&s(i)}},y.hashnav={init:function(){if(y.params.hashnav){y.hashnav.initialized=!0;var e=document.location.hash.replace("#","");if(e)for(var a=0,t=0,s=y.slides.length;s>t;t++){var r=y.slides.eq(t),i=r.attr("data-hash");if(i===e&&!r.hasClass(y.params.slideDuplicateClass)){var n=r.index();y.slideTo(n,a,y.params.runCallbacksOnInit,!0)}}}},setHash:function(){y.hashnav.initialized&&y.params.hashnav&&(document.location.hash=y.slides.eq(y.activeIndex).attr("data-hash")||"")}},y.disableKeyboardControl=function(){y.params.keyboardControl=!1,a(document).off("keydown",l)},y.enableKeyboardControl=function(){y.params.keyboardControl=!0,a(document).on("keydown",l)},y.mousewheel={event:!1,lastScrollTime:(new window.Date).getTime()},y.params.mousewheelControl){try{new window.WheelEvent("wheel"),y.mousewheel.event="wheel"}catch(O){(window.WheelEvent||y.container[0]&&"wheel"in y.container[0])&&(y.mousewheel.event="wheel")}!y.mousewheel.event&&window.WheelEvent,y.mousewheel.event||void 0===document.onmousewheel||(y.mousewheel.event="mousewheel"),y.mousewheel.event||(y.mousewheel.event="DOMMouseScroll")}y.disableMousewheelControl=function(){return y.mousewheel.event?(y.container.off(y.mousewheel.event,p),!0):!1},y.enableMousewheelControl=function(){return y.mousewheel.event?(y.container.on(y.mousewheel.event,p),!0):!1},y.parallax={setTranslate:function(){y.container.children("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function(){d(this,y.progress)}),y.slides.each(function(){var e=a(this);e.find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function(){var a=Math.min(Math.max(e[0].progress,-1),1);d(this,a)})})},setTransition:function(e){"undefined"==typeof e&&(e=y.params.speed),y.container.find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function(){var t=a(this),s=parseInt(t.attr("data-swiper-parallax-duration"),10)||e;0===e&&(s=0),t.transition(s)})}},y._plugins=[];for(var N in y.plugins){var R=y.plugins[N](y,y.params[N]);R&&y._plugins.push(R)}return y.callPlugins=function(e){for(var a=0;a<y._plugins.length;a++)e in y._plugins[a]&&y._plugins[a][e](arguments[1],arguments[2],arguments[3],arguments[4],arguments[5])},y.emitterEventListeners={},y.emit=function(e){y.params[e]&&y.params[e](arguments[1],arguments[2],arguments[3],arguments[4],arguments[5]);var a;if(y.emitterEventListeners[e])for(a=0;a<y.emitterEventListeners[e].length;a++)y.emitterEventListeners[e][a](arguments[1],arguments[2],arguments[3],arguments[4],arguments[5]);y.callPlugins&&y.callPlugins(e,arguments[1],arguments[2],arguments[3],arguments[4],arguments[5])},y.on=function(e,a){return e=u(e),y.emitterEventListeners[e]||(y.emitterEventListeners[e]=[]),y.emitterEventListeners[e].push(a),y},y.off=function(e,a){var t;if(e=u(e),"undefined"==typeof a)return y.emitterEventListeners[e]=[],y;if(y.emitterEventListeners[e]&&0!==y.emitterEventListeners[e].length){for(t=0;t<y.emitterEventListeners[e].length;t++)y.emitterEventListeners[e][t]===a&&y.emitterEventListeners[e].splice(t,1);return y}},y.once=function(e,a){e=u(e);var t=function(){a(arguments[0],arguments[1],arguments[2],arguments[3],arguments[4]),y.off(e,t)};return y.on(e,t),y},y.a11y={makeFocusable:function(e){return e.attr("tabIndex","0"),e},addRole:function(e,a){return e.attr("role",a),e},addLabel:function(e,a){return e.attr("aria-label",a),e},disable:function(e){return e.attr("aria-disabled",!0),e},enable:function(e){return e.attr("aria-disabled",!1),e},onEnterKey:function(e){13===e.keyCode&&(a(e.target).is(y.params.nextButton)?(y.onClickNext(e),y.isEnd?y.a11y.notify(y.params.lastSlideMessage):y.a11y.notify(y.params.nextSlideMessage)):a(e.target).is(y.params.prevButton)&&(y.onClickPrev(e),y.isBeginning?y.a11y.notify(y.params.firstSlideMessage):y.a11y.notify(y.params.prevSlideMessage)),a(e.target).is("."+y.params.bulletClass)&&a(e.target)[0].click())},liveRegion:a('<span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>'),notify:function(e){var a=y.a11y.liveRegion;0!==a.length&&(a.html(""),a.html(e))},init:function(){y.params.nextButton&&y.nextButton&&y.nextButton.length>0&&(y.a11y.makeFocusable(y.nextButton),y.a11y.addRole(y.nextButton,"button"),y.a11y.addLabel(y.nextButton,y.params.nextSlideMessage)),y.params.prevButton&&y.prevButton&&y.prevButton.length>0&&(y.a11y.makeFocusable(y.prevButton),y.a11y.addRole(y.prevButton,"button"),y.a11y.addLabel(y.prevButton,y.params.prevSlideMessage)),a(y.container).append(y.a11y.liveRegion)},initPagination:function(){y.params.pagination&&y.params.paginationClickable&&y.bullets&&y.bullets.length&&y.bullets.each(function(){var e=a(this);y.a11y.makeFocusable(e),y.a11y.addRole(e,"button"),y.a11y.addLabel(e,y.params.paginationBulletMessage.replace(/{{index}}/,e.index()+1))})},destroy:function(){y.a11y.liveRegion&&y.a11y.liveRegion.length>0&&y.a11y.liveRegion.remove()}},y.init=function(){y.params.loop&&y.createLoop(),y.updateContainerSize(),y.updateSlidesSize(),y.updatePagination(),y.params.scrollbar&&y.scrollbar&&(y.scrollbar.set(),y.params.scrollbarDraggable&&y.scrollbar.enableDraggable()),"slide"!==y.params.effect&&y.effects[y.params.effect]&&(y.params.loop||y.updateProgress(),y.effects[y.params.effect].setTranslate()),y.params.loop?y.slideTo(y.params.initialSlide+y.loopedSlides,0,y.params.runCallbacksOnInit):(y.slideTo(y.params.initialSlide,0,y.params.runCallbacksOnInit),0===y.params.initialSlide&&(y.parallax&&y.params.parallax&&y.parallax.setTranslate(),y.lazy&&y.params.lazyLoading&&(y.lazy.load(),y.lazy.initialImageLoaded=!0))),y.attachEvents(),y.params.observer&&y.support.observer&&y.initObservers(),y.params.preloadImages&&!y.params.lazyLoading&&y.preloadImages(),y.params.autoplay&&y.startAutoplay(),y.params.keyboardControl&&y.enableKeyboardControl&&y.enableKeyboardControl(),y.params.mousewheelControl&&y.enableMousewheelControl&&y.enableMousewheelControl(),
y.params.hashnav&&y.hashnav&&y.hashnav.init(),y.params.a11y&&y.a11y&&y.a11y.init(),y.emit("onInit",y)},y.cleanupStyles=function(){y.container.removeClass(y.classNames.join(" ")).removeAttr("style"),y.wrapper.removeAttr("style"),y.slides&&y.slides.length&&y.slides.removeClass([y.params.slideVisibleClass,y.params.slideActiveClass,y.params.slideNextClass,y.params.slidePrevClass].join(" ")).removeAttr("style").removeAttr("data-swiper-column").removeAttr("data-swiper-row"),y.paginationContainer&&y.paginationContainer.length&&y.paginationContainer.removeClass(y.params.paginationHiddenClass),y.bullets&&y.bullets.length&&y.bullets.removeClass(y.params.bulletActiveClass),y.params.prevButton&&a(y.params.prevButton).removeClass(y.params.buttonDisabledClass),y.params.nextButton&&a(y.params.nextButton).removeClass(y.params.buttonDisabledClass),y.params.scrollbar&&y.scrollbar&&(y.scrollbar.track&&y.scrollbar.track.length&&y.scrollbar.track.removeAttr("style"),y.scrollbar.drag&&y.scrollbar.drag.length&&y.scrollbar.drag.removeAttr("style"))},y.destroy=function(e,a){y.detachEvents(),y.stopAutoplay(),y.params.scrollbar&&y.scrollbar&&y.params.scrollbarDraggable&&y.scrollbar.disableDraggable(),y.params.loop&&y.destroyLoop(),a&&y.cleanupStyles(),y.disconnectObservers(),y.params.keyboardControl&&y.disableKeyboardControl&&y.disableKeyboardControl(),y.params.mousewheelControl&&y.disableMousewheelControl&&y.disableMousewheelControl(),y.params.a11y&&y.a11y&&y.a11y.destroy(),y.emit("onDestroy"),e!==!1&&(y=null)},y.init(),y}};t.prototype={isSafari:function(){var e=navigator.userAgent.toLowerCase();return e.indexOf("safari")>=0&&e.indexOf("chrome")<0&&e.indexOf("android")<0}(),isUiWebView:/(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(navigator.userAgent),isArray:function(e){return"[object Array]"===Object.prototype.toString.apply(e)},browser:{ie:window.navigator.pointerEnabled||window.navigator.msPointerEnabled,ieTouch:window.navigator.msPointerEnabled&&window.navigator.msMaxTouchPoints>1||window.navigator.pointerEnabled&&window.navigator.maxTouchPoints>1},device:function(){var e=navigator.userAgent,a=e.match(/(Android);?[\s\/]+([\d.]+)?/),t=e.match(/(iPad).*OS\s([\d_]+)/),s=e.match(/(iPod)(.*OS\s([\d_]+))?/),r=!t&&e.match(/(iPhone\sOS)\s([\d_]+)/);return{ios:t||r||s,android:a}}(),support:{touch:window.Modernizr&&Modernizr.touch===!0||function(){return!!("ontouchstart"in window||window.DocumentTouch&&document instanceof DocumentTouch)}(),transforms3d:window.Modernizr&&Modernizr.csstransforms3d===!0||function(){var e=document.createElement("div").style;return"webkitPerspective"in e||"MozPerspective"in e||"OPerspective"in e||"MsPerspective"in e||"perspective"in e}(),flexbox:function(){for(var e=document.createElement("div").style,a="alignItems webkitAlignItems webkitBoxAlign msFlexAlign mozBoxAlign webkitFlexDirection msFlexDirection mozBoxDirection mozBoxOrient webkitBoxDirection webkitBoxOrient".split(" "),t=0;t<a.length;t++)if(a[t]in e)return!0}(),observer:function(){return"MutationObserver"in window||"WebkitMutationObserver"in window}()},plugins:{}};for(var s=["jQuery","Zepto","Dom7"],r=0;r<s.length;r++)window[s[r]]&&e(window[s[r]]);var i;i="undefined"==typeof Dom7?window.Dom7||window.Zepto||window.jQuery:Dom7,i&&("transitionEnd"in i.fn||(i.fn.transitionEnd=function(e){function a(i){if(i.target===this)for(e.call(this,i),t=0;t<s.length;t++)r.off(s[t],a)}var t,s=["webkitTransitionEnd","transitionend","oTransitionEnd","MSTransitionEnd","msTransitionEnd"],r=this;if(e)for(t=0;t<s.length;t++)r.on(s[t],a);return this}),"transform"in i.fn||(i.fn.transform=function(e){for(var a=0;a<this.length;a++){var t=this[a].style;t.webkitTransform=t.MsTransform=t.msTransform=t.MozTransform=t.OTransform=t.transform=e}return this}),"transition"in i.fn||(i.fn.transition=function(e){"string"!=typeof e&&(e+="ms");for(var a=0;a<this.length;a++){var t=this[a].style;t.webkitTransitionDuration=t.MsTransitionDuration=t.msTransitionDuration=t.MozTransitionDuration=t.OTransitionDuration=t.transitionDuration=e}return this})),window.Swiper=t}(),"undefined"!=typeof module?module.exports=window.Swiper:"function"==typeof define&&define.amd&&define([],function(){"use strict";return window.Swiper});
//# sourceMappingURL=maps/swiper.jquery.min.js.map


// fin swiper .jq

//     Underscore.js 1.8.3
//     http://underscorejs.org
//     (c) 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
//     Underscore may be freely distributed under the MIT license.
(function(){function n(n){function t(t,r,e,u,i,o){for(;i>=0&&o>i;i+=n){var a=u?u[i]:i;e=r(e,t[a],a,t)}return e}return function(r,e,u,i){e=b(e,i,4);var o=!k(r)&&m.keys(r),a=(o||r).length,c=n>0?0:a-1;return arguments.length<3&&(u=r[o?o[c]:c],c+=n),t(r,e,u,o,c,a)}}function t(n){return function(t,r,e){r=x(r,e);for(var u=O(t),i=n>0?0:u-1;i>=0&&u>i;i+=n)if(r(t[i],i,t))return i;return-1}}function r(n,t,r){return function(e,u,i){var o=0,a=O(e);if("number"==typeof i)n>0?o=i>=0?i:Math.max(i+a,o):a=i>=0?Math.min(i+1,a):i+a+1;else if(r&&i&&a)return i=r(e,u),e[i]===u?i:-1;if(u!==u)return i=t(l.call(e,o,a),m.isNaN),i>=0?i+o:-1;for(i=n>0?o:a-1;i>=0&&a>i;i+=n)if(e[i]===u)return i;return-1}}function e(n,t){var r=I.length,e=n.constructor,u=m.isFunction(e)&&e.prototype||a,i="constructor";for(m.has(n,i)&&!m.contains(t,i)&&t.push(i);r--;)i=I[r],i in n&&n[i]!==u[i]&&!m.contains(t,i)&&t.push(i)}var u=this,i=u._,o=Array.prototype,a=Object.prototype,c=Function.prototype,f=o.push,l=o.slice,s=a.toString,p=a.hasOwnProperty,h=Array.isArray,v=Object.keys,g=c.bind,y=Object.create,d=function(){},m=function(n){return n instanceof m?n:this instanceof m?void(this._wrapped=n):new m(n)};"undefined"!=typeof exports?("undefined"!=typeof module&&module.exports&&(exports=module.exports=m),exports._=m):u._=m,m.VERSION="1.8.3";var b=function(n,t,r){if(t===void 0)return n;switch(null==r?3:r){case 1:return function(r){return n.call(t,r)};case 2:return function(r,e){return n.call(t,r,e)};case 3:return function(r,e,u){return n.call(t,r,e,u)};case 4:return function(r,e,u,i){return n.call(t,r,e,u,i)}}return function(){return n.apply(t,arguments)}},x=function(n,t,r){return null==n?m.identity:m.isFunction(n)?b(n,t,r):m.isObject(n)?m.matcher(n):m.property(n)};m.iteratee=function(n,t){return x(n,t,1/0)};var _=function(n,t){return function(r){var e=arguments.length;if(2>e||null==r)return r;for(var u=1;e>u;u++)for(var i=arguments[u],o=n(i),a=o.length,c=0;a>c;c++){var f=o[c];t&&r[f]!==void 0||(r[f]=i[f])}return r}},j=function(n){if(!m.isObject(n))return{};if(y)return y(n);d.prototype=n;var t=new d;return d.prototype=null,t},w=function(n){return function(t){return null==t?void 0:t[n]}},A=Math.pow(2,53)-1,O=w("length"),k=function(n){var t=O(n);return"number"==typeof t&&t>=0&&A>=t};m.each=m.forEach=function(n,t,r){t=b(t,r);var e,u;if(k(n))for(e=0,u=n.length;u>e;e++)t(n[e],e,n);else{var i=m.keys(n);for(e=0,u=i.length;u>e;e++)t(n[i[e]],i[e],n)}return n},m.map=m.collect=function(n,t,r){t=x(t,r);for(var e=!k(n)&&m.keys(n),u=(e||n).length,i=Array(u),o=0;u>o;o++){var a=e?e[o]:o;i[o]=t(n[a],a,n)}return i},m.reduce=m.foldl=m.inject=n(1),m.reduceRight=m.foldr=n(-1),m.find=m.detect=function(n,t,r){var e;return e=k(n)?m.findIndex(n,t,r):m.findKey(n,t,r),e!==void 0&&e!==-1?n[e]:void 0},m.filter=m.select=function(n,t,r){var e=[];return t=x(t,r),m.each(n,function(n,r,u){t(n,r,u)&&e.push(n)}),e},m.reject=function(n,t,r){return m.filter(n,m.negate(x(t)),r)},m.every=m.all=function(n,t,r){t=x(t,r);for(var e=!k(n)&&m.keys(n),u=(e||n).length,i=0;u>i;i++){var o=e?e[i]:i;if(!t(n[o],o,n))return!1}return!0},m.some=m.any=function(n,t,r){t=x(t,r);for(var e=!k(n)&&m.keys(n),u=(e||n).length,i=0;u>i;i++){var o=e?e[i]:i;if(t(n[o],o,n))return!0}return!1},m.contains=m.includes=m.include=function(n,t,r,e){return k(n)||(n=m.values(n)),("number"!=typeof r||e)&&(r=0),m.indexOf(n,t,r)>=0},m.invoke=function(n,t){var r=l.call(arguments,2),e=m.isFunction(t);return m.map(n,function(n){var u=e?t:n[t];return null==u?u:u.apply(n,r)})},m.pluck=function(n,t){return m.map(n,m.property(t))},m.where=function(n,t){return m.filter(n,m.matcher(t))},m.findWhere=function(n,t){return m.find(n,m.matcher(t))},m.max=function(n,t,r){var e,u,i=-1/0,o=-1/0;if(null==t&&null!=n){n=k(n)?n:m.values(n);for(var a=0,c=n.length;c>a;a++)e=n[a],e>i&&(i=e)}else t=x(t,r),m.each(n,function(n,r,e){u=t(n,r,e),(u>o||u===-1/0&&i===-1/0)&&(i=n,o=u)});return i},m.min=function(n,t,r){var e,u,i=1/0,o=1/0;if(null==t&&null!=n){n=k(n)?n:m.values(n);for(var a=0,c=n.length;c>a;a++)e=n[a],i>e&&(i=e)}else t=x(t,r),m.each(n,function(n,r,e){u=t(n,r,e),(o>u||1/0===u&&1/0===i)&&(i=n,o=u)});return i},m.shuffle=function(n){for(var t,r=k(n)?n:m.values(n),e=r.length,u=Array(e),i=0;e>i;i++)t=m.random(0,i),t!==i&&(u[i]=u[t]),u[t]=r[i];return u},m.sample=function(n,t,r){return null==t||r?(k(n)||(n=m.values(n)),n[m.random(n.length-1)]):m.shuffle(n).slice(0,Math.max(0,t))},m.sortBy=function(n,t,r){return t=x(t,r),m.pluck(m.map(n,function(n,r,e){return{value:n,index:r,criteria:t(n,r,e)}}).sort(function(n,t){var r=n.criteria,e=t.criteria;if(r!==e){if(r>e||r===void 0)return 1;if(e>r||e===void 0)return-1}return n.index-t.index}),"value")};var F=function(n){return function(t,r,e){var u={};return r=x(r,e),m.each(t,function(e,i){var o=r(e,i,t);n(u,e,o)}),u}};m.groupBy=F(function(n,t,r){m.has(n,r)?n[r].push(t):n[r]=[t]}),m.indexBy=F(function(n,t,r){n[r]=t}),m.countBy=F(function(n,t,r){m.has(n,r)?n[r]++:n[r]=1}),m.toArray=function(n){return n?m.isArray(n)?l.call(n):k(n)?m.map(n,m.identity):m.values(n):[]},m.size=function(n){return null==n?0:k(n)?n.length:m.keys(n).length},m.partition=function(n,t,r){t=x(t,r);var e=[],u=[];return m.each(n,function(n,r,i){(t(n,r,i)?e:u).push(n)}),[e,u]},m.first=m.head=m.take=function(n,t,r){return null==n?void 0:null==t||r?n[0]:m.initial(n,n.length-t)},m.initial=function(n,t,r){return l.call(n,0,Math.max(0,n.length-(null==t||r?1:t)))},m.last=function(n,t,r){return null==n?void 0:null==t||r?n[n.length-1]:m.rest(n,Math.max(0,n.length-t))},m.rest=m.tail=m.drop=function(n,t,r){return l.call(n,null==t||r?1:t)},m.compact=function(n){return m.filter(n,m.identity)};var S=function(n,t,r,e){for(var u=[],i=0,o=e||0,a=O(n);a>o;o++){var c=n[o];if(k(c)&&(m.isArray(c)||m.isArguments(c))){t||(c=S(c,t,r));var f=0,l=c.length;for(u.length+=l;l>f;)u[i++]=c[f++]}else r||(u[i++]=c)}return u};m.flatten=function(n,t){return S(n,t,!1)},m.without=function(n){return m.difference(n,l.call(arguments,1))},m.uniq=m.unique=function(n,t,r,e){m.isBoolean(t)||(e=r,r=t,t=!1),null!=r&&(r=x(r,e));for(var u=[],i=[],o=0,a=O(n);a>o;o++){var c=n[o],f=r?r(c,o,n):c;t?(o&&i===f||u.push(c),i=f):r?m.contains(i,f)||(i.push(f),u.push(c)):m.contains(u,c)||u.push(c)}return u},m.union=function(){return m.uniq(S(arguments,!0,!0))},m.intersection=function(n){for(var t=[],r=arguments.length,e=0,u=O(n);u>e;e++){var i=n[e];if(!m.contains(t,i)){for(var o=1;r>o&&m.contains(arguments[o],i);o++);o===r&&t.push(i)}}return t},m.difference=function(n){var t=S(arguments,!0,!0,1);return m.filter(n,function(n){return!m.contains(t,n)})},m.zip=function(){return m.unzip(arguments)},m.unzip=function(n){for(var t=n&&m.max(n,O).length||0,r=Array(t),e=0;t>e;e++)r[e]=m.pluck(n,e);return r},m.object=function(n,t){for(var r={},e=0,u=O(n);u>e;e++)t?r[n[e]]=t[e]:r[n[e][0]]=n[e][1];return r},m.findIndex=t(1),m.findLastIndex=t(-1),m.sortedIndex=function(n,t,r,e){r=x(r,e,1);for(var u=r(t),i=0,o=O(n);o>i;){var a=Math.floor((i+o)/2);r(n[a])<u?i=a+1:o=a}return i},m.indexOf=r(1,m.findIndex,m.sortedIndex),m.lastIndexOf=r(-1,m.findLastIndex),m.range=function(n,t,r){null==t&&(t=n||0,n=0),r=r||1;for(var e=Math.max(Math.ceil((t-n)/r),0),u=Array(e),i=0;e>i;i++,n+=r)u[i]=n;return u};var E=function(n,t,r,e,u){if(!(e instanceof t))return n.apply(r,u);var i=j(n.prototype),o=n.apply(i,u);return m.isObject(o)?o:i};m.bind=function(n,t){if(g&&n.bind===g)return g.apply(n,l.call(arguments,1));if(!m.isFunction(n))throw new TypeError("Bind must be called on a function");var r=l.call(arguments,2),e=function(){return E(n,e,t,this,r.concat(l.call(arguments)))};return e},m.partial=function(n){var t=l.call(arguments,1),r=function(){for(var e=0,u=t.length,i=Array(u),o=0;u>o;o++)i[o]=t[o]===m?arguments[e++]:t[o];for(;e<arguments.length;)i.push(arguments[e++]);return E(n,r,this,this,i)};return r},m.bindAll=function(n){var t,r,e=arguments.length;if(1>=e)throw new Error("bindAll must be passed function names");for(t=1;e>t;t++)r=arguments[t],n[r]=m.bind(n[r],n);return n},m.memoize=function(n,t){var r=function(e){var u=r.cache,i=""+(t?t.apply(this,arguments):e);return m.has(u,i)||(u[i]=n.apply(this,arguments)),u[i]};return r.cache={},r},m.delay=function(n,t){var r=l.call(arguments,2);return setTimeout(function(){return n.apply(null,r)},t)},m.defer=m.partial(m.delay,m,1),m.throttle=function(n,t,r){var e,u,i,o=null,a=0;r||(r={});var c=function(){a=r.leading===!1?0:m.now(),o=null,i=n.apply(e,u),o||(e=u=null)};return function(){var f=m.now();a||r.leading!==!1||(a=f);var l=t-(f-a);return e=this,u=arguments,0>=l||l>t?(o&&(clearTimeout(o),o=null),a=f,i=n.apply(e,u),o||(e=u=null)):o||r.trailing===!1||(o=setTimeout(c,l)),i}},m.debounce=function(n,t,r){var e,u,i,o,a,c=function(){var f=m.now()-o;t>f&&f>=0?e=setTimeout(c,t-f):(e=null,r||(a=n.apply(i,u),e||(i=u=null)))};return function(){i=this,u=arguments,o=m.now();var f=r&&!e;return e||(e=setTimeout(c,t)),f&&(a=n.apply(i,u),i=u=null),a}},m.wrap=function(n,t){return m.partial(t,n)},m.negate=function(n){return function(){return!n.apply(this,arguments)}},m.compose=function(){var n=arguments,t=n.length-1;return function(){for(var r=t,e=n[t].apply(this,arguments);r--;)e=n[r].call(this,e);return e}},m.after=function(n,t){return function(){return--n<1?t.apply(this,arguments):void 0}},m.before=function(n,t){var r;return function(){return--n>0&&(r=t.apply(this,arguments)),1>=n&&(t=null),r}},m.once=m.partial(m.before,2);var M=!{toString:null}.propertyIsEnumerable("toString"),I=["valueOf","isPrototypeOf","toString","propertyIsEnumerable","hasOwnProperty","toLocaleString"];m.keys=function(n){if(!m.isObject(n))return[];if(v)return v(n);var t=[];for(var r in n)m.has(n,r)&&t.push(r);return M&&e(n,t),t},m.allKeys=function(n){if(!m.isObject(n))return[];var t=[];for(var r in n)t.push(r);return M&&e(n,t),t},m.values=function(n){for(var t=m.keys(n),r=t.length,e=Array(r),u=0;r>u;u++)e[u]=n[t[u]];return e},m.mapObject=function(n,t,r){t=x(t,r);for(var e,u=m.keys(n),i=u.length,o={},a=0;i>a;a++)e=u[a],o[e]=t(n[e],e,n);return o},m.pairs=function(n){for(var t=m.keys(n),r=t.length,e=Array(r),u=0;r>u;u++)e[u]=[t[u],n[t[u]]];return e},m.invert=function(n){for(var t={},r=m.keys(n),e=0,u=r.length;u>e;e++)t[n[r[e]]]=r[e];return t},m.functions=m.methods=function(n){var t=[];for(var r in n)m.isFunction(n[r])&&t.push(r);return t.sort()},m.extend=_(m.allKeys),m.extendOwn=m.assign=_(m.keys),m.findKey=function(n,t,r){t=x(t,r);for(var e,u=m.keys(n),i=0,o=u.length;o>i;i++)if(e=u[i],t(n[e],e,n))return e},m.pick=function(n,t,r){var e,u,i={},o=n;if(null==o)return i;m.isFunction(t)?(u=m.allKeys(o),e=b(t,r)):(u=S(arguments,!1,!1,1),e=function(n,t,r){return t in r},o=Object(o));for(var a=0,c=u.length;c>a;a++){var f=u[a],l=o[f];e(l,f,o)&&(i[f]=l)}return i},m.omit=function(n,t,r){if(m.isFunction(t))t=m.negate(t);else{var e=m.map(S(arguments,!1,!1,1),String);t=function(n,t){return!m.contains(e,t)}}return m.pick(n,t,r)},m.defaults=_(m.allKeys,!0),m.create=function(n,t){var r=j(n);return t&&m.extendOwn(r,t),r},m.clone=function(n){return m.isObject(n)?m.isArray(n)?n.slice():m.extend({},n):n},m.tap=function(n,t){return t(n),n},m.isMatch=function(n,t){var r=m.keys(t),e=r.length;if(null==n)return!e;for(var u=Object(n),i=0;e>i;i++){var o=r[i];if(t[o]!==u[o]||!(o in u))return!1}return!0};var N=function(n,t,r,e){if(n===t)return 0!==n||1/n===1/t;if(null==n||null==t)return n===t;n instanceof m&&(n=n._wrapped),t instanceof m&&(t=t._wrapped);var u=s.call(n);if(u!==s.call(t))return!1;switch(u){case"[object RegExp]":case"[object String]":return""+n==""+t;case"[object Number]":return+n!==+n?+t!==+t:0===+n?1/+n===1/t:+n===+t;case"[object Date]":case"[object Boolean]":return+n===+t}var i="[object Array]"===u;if(!i){if("object"!=typeof n||"object"!=typeof t)return!1;var o=n.constructor,a=t.constructor;if(o!==a&&!(m.isFunction(o)&&o instanceof o&&m.isFunction(a)&&a instanceof a)&&"constructor"in n&&"constructor"in t)return!1}r=r||[],e=e||[];for(var c=r.length;c--;)if(r[c]===n)return e[c]===t;if(r.push(n),e.push(t),i){if(c=n.length,c!==t.length)return!1;for(;c--;)if(!N(n[c],t[c],r,e))return!1}else{var f,l=m.keys(n);if(c=l.length,m.keys(t).length!==c)return!1;for(;c--;)if(f=l[c],!m.has(t,f)||!N(n[f],t[f],r,e))return!1}return r.pop(),e.pop(),!0};m.isEqual=function(n,t){return N(n,t)},m.isEmpty=function(n){return null==n?!0:k(n)&&(m.isArray(n)||m.isString(n)||m.isArguments(n))?0===n.length:0===m.keys(n).length},m.isElement=function(n){return!(!n||1!==n.nodeType)},m.isArray=h||function(n){return"[object Array]"===s.call(n)},m.isObject=function(n){var t=typeof n;return"function"===t||"object"===t&&!!n},m.each(["Arguments","Function","String","Number","Date","RegExp","Error"],function(n){m["is"+n]=function(t){return s.call(t)==="[object "+n+"]"}}),m.isArguments(arguments)||(m.isArguments=function(n){return m.has(n,"callee")}),"function"!=typeof/./&&"object"!=typeof Int8Array&&(m.isFunction=function(n){return"function"==typeof n||!1}),m.isFinite=function(n){return isFinite(n)&&!isNaN(parseFloat(n))},m.isNaN=function(n){return m.isNumber(n)&&n!==+n},m.isBoolean=function(n){return n===!0||n===!1||"[object Boolean]"===s.call(n)},m.isNull=function(n){return null===n},m.isUndefined=function(n){return n===void 0},m.has=function(n,t){return null!=n&&p.call(n,t)},m.noConflict=function(){return u._=i,this},m.identity=function(n){return n},m.constant=function(n){return function(){return n}},m.noop=function(){},m.property=w,m.propertyOf=function(n){return null==n?function(){}:function(t){return n[t]}},m.matcher=m.matches=function(n){return n=m.extendOwn({},n),function(t){return m.isMatch(t,n)}},m.times=function(n,t,r){var e=Array(Math.max(0,n));t=b(t,r,1);for(var u=0;n>u;u++)e[u]=t(u);return e},m.random=function(n,t){return null==t&&(t=n,n=0),n+Math.floor(Math.random()*(t-n+1))},m.now=Date.now||function(){return(new Date).getTime()};var B={"&":"&amp;","<":"&lt;",">":"&gt;",'"':"&quot;","'":"&#x27;","`":"&#x60;"},T=m.invert(B),R=function(n){var t=function(t){return n[t]},r="(?:"+m.keys(n).join("|")+")",e=RegExp(r),u=RegExp(r,"g");return function(n){return n=null==n?"":""+n,e.test(n)?n.replace(u,t):n}};m.escape=R(B),m.unescape=R(T),m.result=function(n,t,r){var e=null==n?void 0:n[t];return e===void 0&&(e=r),m.isFunction(e)?e.call(n):e};var q=0;m.uniqueId=function(n){var t=++q+"";return n?n+t:t},m.templateSettings={evaluate:/<%([\s\S]+?)%>/g,interpolate:/<%=([\s\S]+?)%>/g,escape:/<%-([\s\S]+?)%>/g};var K=/(.)^/,z={"'":"'","\\":"\\","\r":"r","\n":"n","\u2028":"u2028","\u2029":"u2029"},D=/\\|'|\r|\n|\u2028|\u2029/g,L=function(n){return"\\"+z[n]};m.template=function(n,t,r){!t&&r&&(t=r),t=m.defaults({},t,m.templateSettings);var e=RegExp([(t.escape||K).source,(t.interpolate||K).source,(t.evaluate||K).source].join("|")+"|$","g"),u=0,i="__p+='";n.replace(e,function(t,r,e,o,a){return i+=n.slice(u,a).replace(D,L),u=a+t.length,r?i+="'+\n((__t=("+r+"))==null?'':_.escape(__t))+\n'":e?i+="'+\n((__t=("+e+"))==null?'':__t)+\n'":o&&(i+="';\n"+o+"\n__p+='"),t}),i+="';\n",t.variable||(i="with(obj||{}){\n"+i+"}\n"),i="var __t,__p='',__j=Array.prototype.join,"+"print=function(){__p+=__j.call(arguments,'');};\n"+i+"return __p;\n";try{var o=new Function(t.variable||"obj","_",i)}catch(a){throw a.source=i,a}var c=function(n){return o.call(this,n,m)},f=t.variable||"obj";return c.source="function("+f+"){\n"+i+"}",c},m.chain=function(n){var t=m(n);return t._chain=!0,t};var P=function(n,t){return n._chain?m(t).chain():t};m.mixin=function(n){m.each(m.functions(n),function(t){var r=m[t]=n[t];m.prototype[t]=function(){var n=[this._wrapped];return f.apply(n,arguments),P(this,r.apply(m,n))}})},m.mixin(m),m.each(["pop","push","reverse","shift","sort","splice","unshift"],function(n){var t=o[n];m.prototype[n]=function(){var r=this._wrapped;return t.apply(r,arguments),"shift"!==n&&"splice"!==n||0!==r.length||delete r[0],P(this,r)}}),m.each(["concat","join","slice"],function(n){var t=o[n];m.prototype[n]=function(){return P(this,t.apply(this._wrapped,arguments))}}),m.prototype.value=function(){return this._wrapped},m.prototype.valueOf=m.prototype.toJSON=m.prototype.value,m.prototype.toString=function(){return""+this._wrapped},"function"==typeof define&&define.amd&&define("underscore",[],function(){return m})}).call(this);


// fin underscore 

window.wp=window.wp||{},function(a){var b="undefined"==typeof _wpUtilSettings?{}:_wpUtilSettings;wp.template=_.memoize(function(b){var c,d={evaluate:/<#([\s\S]+?)#>/g,interpolate:/\{\{\{([\s\S]+?)\}\}\}/g,escape:/\{\{([^\}]+?)\}\}(?!\})/g,variable:"data"};return function(e){return(c=c||_.template(a("#tmpl-"+b).html(),d))(e)}}),wp.ajax={settings:b.ajax||{},post:function(a,b){return wp.ajax.send({data:_.isObject(a)?a:_.extend(b||{},{action:a})})},send:function(b,c){var d,e;return _.isObject(b)?c=b:(c=c||{},c.data=_.extend(c.data||{},{action:b})),c=_.defaults(c||{},{type:"POST",url:wp.ajax.settings.url,context:this}),e=a.Deferred(function(b){c.success&&b.done(c.success),c.error&&b.fail(c.error),delete c.success,delete c.error,b.jqXHR=a.ajax(c).done(function(a){"1"!==a&&1!==a||(a={success:!0}),_.isObject(a)&&!_.isUndefined(a.success)?b[a.success?"resolveWith":"rejectWith"](this,[a.data]):b.rejectWith(this,[a])}).fail(function(){b.rejectWith(this,arguments)})}),d=e.promise(),d.abort=function(){return e.jqXHR.abort(),this},d}}}(jQuery);

// fin wp-until.min.js 

/*! Magnific Popup - v1.1.0 - 2016-02-20
* http://dimsemenov.com/plugins/magnific-popup/
* Copyright (c) 2016 Dmitry Semenov; */
!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):a("object"==typeof exports?require("jquery"):window.jQuery||window.Zepto)}(function(a){var b,c,d,e,f,g,h="Close",i="BeforeClose",j="AfterClose",k="BeforeAppend",l="MarkupParse",m="Open",n="Change",o="mfp",p="."+o,q="mfp-ready",r="mfp-removing",s="mfp-prevent-close",t=function(){},u=!!window.jQuery,v=a(window),w=function(a,c){b.ev.on(o+a+p,c)},x=function(b,c,d,e){var f=document.createElement("div");return f.className="mfp-"+b,d&&(f.innerHTML=d),e?c&&c.appendChild(f):(f=a(f),c&&f.appendTo(c)),f},y=function(c,d){b.ev.triggerHandler(o+c,d),b.st.callbacks&&(c=c.charAt(0).toLowerCase()+c.slice(1),b.st.callbacks[c]&&b.st.callbacks[c].apply(b,a.isArray(d)?d:[d]))},z=function(c){return c===g&&b.currTemplate.closeBtn||(b.currTemplate.closeBtn=a(b.st.closeMarkup.replace("%title%",b.st.tClose)),g=c),b.currTemplate.closeBtn},A=function(){a.magnificPopup.instance||(b=new t,b.init(),a.magnificPopup.instance=b)},B=function(){var a=document.createElement("p").style,b=["ms","O","Moz","Webkit"];if(void 0!==a.transition)return!0;for(;b.length;)if(b.pop()+"Transition"in a)return!0;return!1};t.prototype={constructor:t,init:function(){var c=navigator.appVersion;b.isLowIE=b.isIE8=document.all&&!document.addEventListener,b.isAndroid=/android/gi.test(c),b.isIOS=/iphone|ipad|ipod/gi.test(c),b.supportsTransition=B(),b.probablyMobile=b.isAndroid||b.isIOS||/(Opera Mini)|Kindle|webOS|BlackBerry|(Opera Mobi)|(Windows Phone)|IEMobile/i.test(navigator.userAgent),d=a(document),b.popupsCache={}},open:function(c){var e;if(c.isObj===!1){b.items=c.items.toArray(),b.index=0;var g,h=c.items;for(e=0;e<h.length;e++)if(g=h[e],g.parsed&&(g=g.el[0]),g===c.el[0]){b.index=e;break}}else b.items=a.isArray(c.items)?c.items:[c.items],b.index=c.index||0;if(b.isOpen)return void b.updateItemHTML();b.types=[],f="",c.mainEl&&c.mainEl.length?b.ev=c.mainEl.eq(0):b.ev=d,c.key?(b.popupsCache[c.key]||(b.popupsCache[c.key]={}),b.currTemplate=b.popupsCache[c.key]):b.currTemplate={},b.st=a.extend(!0,{},a.magnificPopup.defaults,c),b.fixedContentPos="auto"===b.st.fixedContentPos?!b.probablyMobile:b.st.fixedContentPos,b.st.modal&&(b.st.closeOnContentClick=!1,b.st.closeOnBgClick=!1,b.st.showCloseBtn=!1,b.st.enableEscapeKey=!1),b.bgOverlay||(b.bgOverlay=x("bg").on("click"+p,function(){b.close()}),b.wrap=x("wrap").attr("tabindex",-1).on("click"+p,function(a){b._checkIfClose(a.target)&&b.close()}),b.container=x("container",b.wrap)),b.contentContainer=x("content"),b.st.preloader&&(b.preloader=x("preloader",b.container,b.st.tLoading));var i=a.magnificPopup.modules;for(e=0;e<i.length;e++){var j=i[e];j=j.charAt(0).toUpperCase()+j.slice(1),b["init"+j].call(b)}y("BeforeOpen"),b.st.showCloseBtn&&(b.st.closeBtnInside?(w(l,function(a,b,c,d){c.close_replaceWith=z(d.type)}),f+=" mfp-close-btn-in"):b.wrap.append(z())),b.st.alignTop&&(f+=" mfp-align-top"),b.fixedContentPos?b.wrap.css({overflow:b.st.overflowY,overflowX:"hidden",overflowY:b.st.overflowY}):b.wrap.css({top:v.scrollTop(),position:"absolute"}),(b.st.fixedBgPos===!1||"auto"===b.st.fixedBgPos&&!b.fixedContentPos)&&b.bgOverlay.css({height:d.height(),position:"absolute"}),b.st.enableEscapeKey&&d.on("keyup"+p,function(a){27===a.keyCode&&b.close()}),v.on("resize"+p,function(){b.updateSize()}),b.st.closeOnContentClick||(f+=" mfp-auto-cursor"),f&&b.wrap.addClass(f);var k=b.wH=v.height(),n={};if(b.fixedContentPos&&b._hasScrollBar(k)){var o=b._getScrollbarSize();o&&(n.marginRight=o)}b.fixedContentPos&&(b.isIE7?a("body, html").css("overflow","hidden"):n.overflow="hidden");var r=b.st.mainClass;return b.isIE7&&(r+=" mfp-ie7"),r&&b._addClassToMFP(r),b.updateItemHTML(),y("BuildControls"),a("html").css(n),b.bgOverlay.add(b.wrap).prependTo(b.st.prependTo||a(document.body)),b._lastFocusedEl=document.activeElement,setTimeout(function(){b.content?(b._addClassToMFP(q),b._setFocus()):b.bgOverlay.addClass(q),d.on("focusin"+p,b._onFocusIn)},16),b.isOpen=!0,b.updateSize(k),y(m),c},close:function(){b.isOpen&&(y(i),b.isOpen=!1,b.st.removalDelay&&!b.isLowIE&&b.supportsTransition?(b._addClassToMFP(r),setTimeout(function(){b._close()},b.st.removalDelay)):b._close())},_close:function(){y(h);var c=r+" "+q+" ";if(b.bgOverlay.detach(),b.wrap.detach(),b.container.empty(),b.st.mainClass&&(c+=b.st.mainClass+" "),b._removeClassFromMFP(c),b.fixedContentPos){var e={marginRight:""};b.isIE7?a("body, html").css("overflow",""):e.overflow="",a("html").css(e)}d.off("keyup"+p+" focusin"+p),b.ev.off(p),b.wrap.attr("class","mfp-wrap").removeAttr("style"),b.bgOverlay.attr("class","mfp-bg"),b.container.attr("class","mfp-container"),!b.st.showCloseBtn||b.st.closeBtnInside&&b.currTemplate[b.currItem.type]!==!0||b.currTemplate.closeBtn&&b.currTemplate.closeBtn.detach(),b.st.autoFocusLast&&b._lastFocusedEl&&a(b._lastFocusedEl).focus(),b.currItem=null,b.content=null,b.currTemplate=null,b.prevHeight=0,y(j)},updateSize:function(a){if(b.isIOS){var c=document.documentElement.clientWidth/window.innerWidth,d=window.innerHeight*c;b.wrap.css("height",d),b.wH=d}else b.wH=a||v.height();b.fixedContentPos||b.wrap.css("height",b.wH),y("Resize")},updateItemHTML:function(){var c=b.items[b.index];b.contentContainer.detach(),b.content&&b.content.detach(),c.parsed||(c=b.parseEl(b.index));var d=c.type;if(y("BeforeChange",[b.currItem?b.currItem.type:"",d]),b.currItem=c,!b.currTemplate[d]){var f=b.st[d]?b.st[d].markup:!1;y("FirstMarkupParse",f),f?b.currTemplate[d]=a(f):b.currTemplate[d]=!0}e&&e!==c.type&&b.container.removeClass("mfp-"+e+"-holder");var g=b["get"+d.charAt(0).toUpperCase()+d.slice(1)](c,b.currTemplate[d]);b.appendContent(g,d),c.preloaded=!0,y(n,c),e=c.type,b.container.prepend(b.contentContainer),y("AfterChange")},appendContent:function(a,c){b.content=a,a?b.st.showCloseBtn&&b.st.closeBtnInside&&b.currTemplate[c]===!0?b.content.find(".mfp-close").length||b.content.append(z()):b.content=a:b.content="",y(k),b.container.addClass("mfp-"+c+"-holder"),b.contentContainer.append(b.content)},parseEl:function(c){var d,e=b.items[c];if(e.tagName?e={el:a(e)}:(d=e.type,e={data:e,src:e.src}),e.el){for(var f=b.types,g=0;g<f.length;g++)if(e.el.hasClass("mfp-"+f[g])){d=f[g];break}e.src=e.el.attr("data-mfp-src"),e.src||(e.src=e.el.attr("href"))}return e.type=d||b.st.type||"inline",e.index=c,e.parsed=!0,b.items[c]=e,y("ElementParse",e),b.items[c]},addGroup:function(a,c){var d=function(d){d.mfpEl=this,b._openClick(d,a,c)};c||(c={});var e="click.magnificPopup";c.mainEl=a,c.items?(c.isObj=!0,a.off(e).on(e,d)):(c.isObj=!1,c.delegate?a.off(e).on(e,c.delegate,d):(c.items=a,a.off(e).on(e,d)))},_openClick:function(c,d,e){var f=void 0!==e.midClick?e.midClick:a.magnificPopup.defaults.midClick;if(f||!(2===c.which||c.ctrlKey||c.metaKey||c.altKey||c.shiftKey)){var g=void 0!==e.disableOn?e.disableOn:a.magnificPopup.defaults.disableOn;if(g)if(a.isFunction(g)){if(!g.call(b))return!0}else if(v.width()<g)return!0;c.type&&(c.preventDefault(),b.isOpen&&c.stopPropagation()),e.el=a(c.mfpEl),e.delegate&&(e.items=d.find(e.delegate)),b.open(e)}},updateStatus:function(a,d){if(b.preloader){c!==a&&b.container.removeClass("mfp-s-"+c),d||"loading"!==a||(d=b.st.tLoading);var e={status:a,text:d};y("UpdateStatus",e),a=e.status,d=e.text,b.preloader.html(d),b.preloader.find("a").on("click",function(a){a.stopImmediatePropagation()}),b.container.addClass("mfp-s-"+a),c=a}},_checkIfClose:function(c){if(!a(c).hasClass(s)){var d=b.st.closeOnContentClick,e=b.st.closeOnBgClick;if(d&&e)return!0;if(!b.content||a(c).hasClass("mfp-close")||b.preloader&&c===b.preloader[0])return!0;if(c===b.content[0]||a.contains(b.content[0],c)){if(d)return!0}else if(e&&a.contains(document,c))return!0;return!1}},_addClassToMFP:function(a){b.bgOverlay.addClass(a),b.wrap.addClass(a)},_removeClassFromMFP:function(a){this.bgOverlay.removeClass(a),b.wrap.removeClass(a)},_hasScrollBar:function(a){return(b.isIE7?d.height():document.body.scrollHeight)>(a||v.height())},_setFocus:function(){(b.st.focus?b.content.find(b.st.focus).eq(0):b.wrap).focus()},_onFocusIn:function(c){return c.target===b.wrap[0]||a.contains(b.wrap[0],c.target)?void 0:(b._setFocus(),!1)},_parseMarkup:function(b,c,d){var e;d.data&&(c=a.extend(d.data,c)),y(l,[b,c,d]),a.each(c,function(c,d){if(void 0===d||d===!1)return!0;if(e=c.split("_"),e.length>1){var f=b.find(p+"-"+e[0]);if(f.length>0){var g=e[1];"replaceWith"===g?f[0]!==d[0]&&f.replaceWith(d):"img"===g?f.is("img")?f.attr("src",d):f.replaceWith(a("<img>").attr("src",d).attr("class",f.attr("class"))):f.attr(e[1],d)}}else b.find(p+"-"+c).html(d)})},_getScrollbarSize:function(){if(void 0===b.scrollbarSize){var a=document.createElement("div");a.style.cssText="width: 99px; height: 99px; overflow: scroll; position: absolute; top: -9999px;",document.body.appendChild(a),b.scrollbarSize=a.offsetWidth-a.clientWidth,document.body.removeChild(a)}return b.scrollbarSize}},a.magnificPopup={instance:null,proto:t.prototype,modules:[],open:function(b,c){return A(),b=b?a.extend(!0,{},b):{},b.isObj=!0,b.index=c||0,this.instance.open(b)},close:function(){return a.magnificPopup.instance&&a.magnificPopup.instance.close()},registerModule:function(b,c){c.options&&(a.magnificPopup.defaults[b]=c.options),a.extend(this.proto,c.proto),this.modules.push(b)},defaults:{disableOn:0,key:null,midClick:!1,mainClass:"",preloader:!0,focus:"",closeOnContentClick:!1,closeOnBgClick:!0,closeBtnInside:!0,showCloseBtn:!0,enableEscapeKey:!0,modal:!1,alignTop:!1,removalDelay:0,prependTo:null,fixedContentPos:"auto",fixedBgPos:"auto",overflowY:"auto",closeMarkup:'<button title="%title%" type="button" class="mfp-close">&#215;</button>',tClose:"Close (Esc)",tLoading:"Loading...",autoFocusLast:!0}},a.fn.magnificPopup=function(c){A();var d=a(this);if("string"==typeof c)if("open"===c){var e,f=u?d.data("magnificPopup"):d[0].magnificPopup,g=parseInt(arguments[1],10)||0;f.items?e=f.items[g]:(e=d,f.delegate&&(e=e.find(f.delegate)),e=e.eq(g)),b._openClick({mfpEl:e},d,f)}else b.isOpen&&b[c].apply(b,Array.prototype.slice.call(arguments,1));else c=a.extend(!0,{},c),u?d.data("magnificPopup",c):d[0].magnificPopup=c,b.addGroup(d,c);return d};var C,D,E,F="inline",G=function(){E&&(D.after(E.addClass(C)).detach(),E=null)};a.magnificPopup.registerModule(F,{options:{hiddenClass:"hide",markup:"",tNotFound:"Content not found"},proto:{initInline:function(){b.types.push(F),w(h+"."+F,function(){G()})},getInline:function(c,d){if(G(),c.src){var e=b.st.inline,f=a(c.src);if(f.length){var g=f[0].parentNode;g&&g.tagName&&(D||(C=e.hiddenClass,D=x(C),C="mfp-"+C),E=f.after(D).detach().removeClass(C)),b.updateStatus("ready")}else b.updateStatus("error",e.tNotFound),f=a("<div>");return c.inlineElement=f,f}return b.updateStatus("ready"),b._parseMarkup(d,{},c),d}}});var H,I="ajax",J=function(){H&&a(document.body).removeClass(H)},K=function(){J(),b.req&&b.req.abort()};a.magnificPopup.registerModule(I,{options:{settings:null,cursor:"mfp-ajax-cur",tError:'<a href="%url%">The content</a> could not be loaded.'},proto:{initAjax:function(){b.types.push(I),H=b.st.ajax.cursor,w(h+"."+I,K),w("BeforeChange."+I,K)},getAjax:function(c){H&&a(document.body).addClass(H),b.updateStatus("loading");var d=a.extend({url:c.src,success:function(d,e,f){var g={data:d,xhr:f};y("ParseAjax",g),b.appendContent(a(g.data),I),c.finished=!0,J(),b._setFocus(),setTimeout(function(){b.wrap.addClass(q)},16),b.updateStatus("ready"),y("AjaxContentAdded")},error:function(){J(),c.finished=c.loadError=!0,b.updateStatus("error",b.st.ajax.tError.replace("%url%",c.src))}},b.st.ajax.settings);return b.req=a.ajax(d),""}}});var L,M=function(c){if(c.data&&void 0!==c.data.title)return c.data.title;var d=b.st.image.titleSrc;if(d){if(a.isFunction(d))return d.call(b,c);if(c.el)return c.el.attr(d)||""}return""};a.magnificPopup.registerModule("image",{options:{markup:'<div class="mfp-figure"><div class="mfp-close"></div><figure><div class="mfp-img"></div><figcaption><div class="mfp-bottom-bar"><div class="mfp-title"></div><div class="mfp-counter"></div></div></figcaption></figure></div>',cursor:"mfp-zoom-out-cur",titleSrc:"title",verticalFit:!0,tError:'<a href="%url%">The image</a> could not be loaded.'},proto:{initImage:function(){var c=b.st.image,d=".image";b.types.push("image"),w(m+d,function(){"image"===b.currItem.type&&c.cursor&&a(document.body).addClass(c.cursor)}),w(h+d,function(){c.cursor&&a(document.body).removeClass(c.cursor),v.off("resize"+p)}),w("Resize"+d,b.resizeImage),b.isLowIE&&w("AfterChange",b.resizeImage)},resizeImage:function(){var a=b.currItem;if(a&&a.img&&b.st.image.verticalFit){var c=0;b.isLowIE&&(c=parseInt(a.img.css("padding-top"),10)+parseInt(a.img.css("padding-bottom"),10)),a.img.css("max-height",b.wH-c)}},_onImageHasSize:function(a){a.img&&(a.hasSize=!0,L&&clearInterval(L),a.isCheckingImgSize=!1,y("ImageHasSize",a),a.imgHidden&&(b.content&&b.content.removeClass("mfp-loading"),a.imgHidden=!1))},findImageSize:function(a){var c=0,d=a.img[0],e=function(f){L&&clearInterval(L),L=setInterval(function(){return d.naturalWidth>0?void b._onImageHasSize(a):(c>200&&clearInterval(L),c++,void(3===c?e(10):40===c?e(50):100===c&&e(500)))},f)};e(1)},getImage:function(c,d){var e=0,f=function(){c&&(c.img[0].complete?(c.img.off(".mfploader"),c===b.currItem&&(b._onImageHasSize(c),b.updateStatus("ready")),c.hasSize=!0,c.loaded=!0,y("ImageLoadComplete")):(e++,200>e?setTimeout(f,100):g()))},g=function(){c&&(c.img.off(".mfploader"),c===b.currItem&&(b._onImageHasSize(c),b.updateStatus("error",h.tError.replace("%url%",c.src))),c.hasSize=!0,c.loaded=!0,c.loadError=!0)},h=b.st.image,i=d.find(".mfp-img");if(i.length){var j=document.createElement("img");j.className="mfp-img",c.el&&c.el.find("img").length&&(j.alt=c.el.find("img").attr("alt")),c.img=a(j).on("load.mfploader",f).on("error.mfploader",g),j.src=c.src,i.is("img")&&(c.img=c.img.clone()),j=c.img[0],j.naturalWidth>0?c.hasSize=!0:j.width||(c.hasSize=!1)}return b._parseMarkup(d,{title:M(c),img_replaceWith:c.img},c),b.resizeImage(),c.hasSize?(L&&clearInterval(L),c.loadError?(d.addClass("mfp-loading"),b.updateStatus("error",h.tError.replace("%url%",c.src))):(d.removeClass("mfp-loading"),b.updateStatus("ready")),d):(b.updateStatus("loading"),c.loading=!0,c.hasSize||(c.imgHidden=!0,d.addClass("mfp-loading"),b.findImageSize(c)),d)}}});var N,O=function(){return void 0===N&&(N=void 0!==document.createElement("p").style.MozTransform),N};a.magnificPopup.registerModule("zoom",{options:{enabled:!1,easing:"ease-in-out",duration:300,opener:function(a){return a.is("img")?a:a.find("img")}},proto:{initZoom:function(){var a,c=b.st.zoom,d=".zoom";if(c.enabled&&b.supportsTransition){var e,f,g=c.duration,j=function(a){var b=a.clone().removeAttr("style").removeAttr("class").addClass("mfp-animated-image"),d="all "+c.duration/1e3+"s "+c.easing,e={position:"fixed",zIndex:9999,left:0,top:0,"-webkit-backface-visibility":"hidden"},f="transition";return e["-webkit-"+f]=e["-moz-"+f]=e["-o-"+f]=e[f]=d,b.css(e),b},k=function(){b.content.css("visibility","visible")};w("BuildControls"+d,function(){if(b._allowZoom()){if(clearTimeout(e),b.content.css("visibility","hidden"),a=b._getItemToZoom(),!a)return void k();f=j(a),f.css(b._getOffset()),b.wrap.append(f),e=setTimeout(function(){f.css(b._getOffset(!0)),e=setTimeout(function(){k(),setTimeout(function(){f.remove(),a=f=null,y("ZoomAnimationEnded")},16)},g)},16)}}),w(i+d,function(){if(b._allowZoom()){if(clearTimeout(e),b.st.removalDelay=g,!a){if(a=b._getItemToZoom(),!a)return;f=j(a)}f.css(b._getOffset(!0)),b.wrap.append(f),b.content.css("visibility","hidden"),setTimeout(function(){f.css(b._getOffset())},16)}}),w(h+d,function(){b._allowZoom()&&(k(),f&&f.remove(),a=null)})}},_allowZoom:function(){return"image"===b.currItem.type},_getItemToZoom:function(){return b.currItem.hasSize?b.currItem.img:!1},_getOffset:function(c){var d;d=c?b.currItem.img:b.st.zoom.opener(b.currItem.el||b.currItem);var e=d.offset(),f=parseInt(d.css("padding-top"),10),g=parseInt(d.css("padding-bottom"),10);e.top-=a(window).scrollTop()-f;var h={width:d.width(),height:(u?d.innerHeight():d[0].offsetHeight)-g-f};return O()?h["-moz-transform"]=h.transform="translate("+e.left+"px,"+e.top+"px)":(h.left=e.left,h.top=e.top),h}}});var P="iframe",Q="//about:blank",R=function(a){if(b.currTemplate[P]){var c=b.currTemplate[P].find("iframe");c.length&&(a||(c[0].src=Q),b.isIE8&&c.css("display",a?"block":"none"))}};a.magnificPopup.registerModule(P,{options:{markup:'<div class="mfp-iframe-scaler"><div class="mfp-close"></div><iframe class="mfp-iframe" src="//about:blank" frameborder="0" allowfullscreen></iframe></div>',srcAction:"iframe_src",patterns:{youtube:{index:"youtube.com",id:"v=",src:"//www.youtube.com/embed/%id%?autoplay=1"},vimeo:{index:"vimeo.com/",id:"/",src:"//player.vimeo.com/video/%id%?autoplay=1"},gmaps:{index:"//maps.google.",src:"%id%&output=embed"}}},proto:{initIframe:function(){b.types.push(P),w("BeforeChange",function(a,b,c){b!==c&&(b===P?R():c===P&&R(!0))}),w(h+"."+P,function(){R()})},getIframe:function(c,d){var e=c.src,f=b.st.iframe;a.each(f.patterns,function(){return e.indexOf(this.index)>-1?(this.id&&(e="string"==typeof this.id?e.substr(e.lastIndexOf(this.id)+this.id.length,e.length):this.id.call(this,e)),e=this.src.replace("%id%",e),!1):void 0});var g={};return f.srcAction&&(g[f.srcAction]=e),b._parseMarkup(d,g,c),b.updateStatus("ready"),d}}});var S=function(a){var c=b.items.length;return a>c-1?a-c:0>a?c+a:a},T=function(a,b,c){return a.replace(/%curr%/gi,b+1).replace(/%total%/gi,c)};a.magnificPopup.registerModule("gallery",{options:{enabled:!1,arrowMarkup:'<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',preload:[0,2],navigateByImgClick:!0,arrows:!0,tPrev:"Previous (Left arrow key)",tNext:"Next (Right arrow key)",tCounter:"%curr% of %total%"},proto:{initGallery:function(){var c=b.st.gallery,e=".mfp-gallery";return b.direction=!0,c&&c.enabled?(f+=" mfp-gallery",w(m+e,function(){c.navigateByImgClick&&b.wrap.on("click"+e,".mfp-img",function(){return b.items.length>1?(b.next(),!1):void 0}),d.on("keydown"+e,function(a){37===a.keyCode?b.prev():39===a.keyCode&&b.next()})}),w("UpdateStatus"+e,function(a,c){c.text&&(c.text=T(c.text,b.currItem.index,b.items.length))}),w(l+e,function(a,d,e,f){var g=b.items.length;e.counter=g>1?T(c.tCounter,f.index,g):""}),w("BuildControls"+e,function(){if(b.items.length>1&&c.arrows&&!b.arrowLeft){var d=c.arrowMarkup,e=b.arrowLeft=a(d.replace(/%title%/gi,c.tPrev).replace(/%dir%/gi,"left")).addClass(s),f=b.arrowRight=a(d.replace(/%title%/gi,c.tNext).replace(/%dir%/gi,"right")).addClass(s);e.click(function(){b.prev()}),f.click(function(){b.next()}),b.container.append(e.add(f))}}),w(n+e,function(){b._preloadTimeout&&clearTimeout(b._preloadTimeout),b._preloadTimeout=setTimeout(function(){b.preloadNearbyImages(),b._preloadTimeout=null},16)}),void w(h+e,function(){d.off(e),b.wrap.off("click"+e),b.arrowRight=b.arrowLeft=null})):!1},next:function(){b.direction=!0,b.index=S(b.index+1),b.updateItemHTML()},prev:function(){b.direction=!1,b.index=S(b.index-1),b.updateItemHTML()},goTo:function(a){b.direction=a>=b.index,b.index=a,b.updateItemHTML()},preloadNearbyImages:function(){var a,c=b.st.gallery.preload,d=Math.min(c[0],b.items.length),e=Math.min(c[1],b.items.length);for(a=1;a<=(b.direction?e:d);a++)b._preloadItem(b.index+a);for(a=1;a<=(b.direction?d:e);a++)b._preloadItem(b.index-a)},_preloadItem:function(c){if(c=S(c),!b.items[c].preloaded){var d=b.items[c];d.parsed||(d=b.parseEl(c)),y("LazyLoad",d),"image"===d.type&&(d.img=a('<img class="mfp-img" />').on("load.mfploader",function(){d.hasSize=!0}).on("error.mfploader",function(){d.hasSize=!0,d.loadError=!0,y("LazyLoadError",d)}).attr("src",d.src)),d.preloaded=!0}}}});var U="retina";a.magnificPopup.registerModule(U,{options:{replaceSrc:function(a){return a.src.replace(/\.\w+$/,function(a){return"@2x"+a})},ratio:1},proto:{initRetina:function(){if(window.devicePixelRatio>1){var a=b.st.retina,c=a.ratio;c=isNaN(c)?c():c,c>1&&(w("ImageHasSize."+U,function(a,b){b.img.css({"max-width":b.img[0].naturalWidth/c,width:"100%"})}),w("ElementParse."+U,function(b,d){d.src=a.replaceSrc(d,c)}))}}}}),A()});


// fin magnific popup min 

(function($){
	"use strict";

	CherryJsCore.utilites.namespace('cherryProjectsFrontSingleScripts');
	CherryJsCore.cherryProjectsFrontSingleScripts = {
		init: function () {
			var self = this;

			if( CherryJsCore.status.is_ready ){
				self.readyRender( self );
			}else{
				CherryJsCore.variable.$document.on( 'ready', self.readyRender( self ) );
			}

		},
		readyRender: function( self ) {

			self.skillsListInit( self );
			self.imagesListingInit( self );
			self.sliderInit( self );
		},
		skillsListInit: function( self ) {
			$( '.cherry-projects-single-skills-list li ' ).each( function() {
				var $this = $( this ),
					skillValue = $( '.skill-bar', $this).data( 'skill-value' );
					$( '.skill-bar span', $this).css( {
						'width': skillValue + '%'
					} );
			} );
		},
		imagesListingInit: function( self ) {
			$('.cherry-projects-additional-image-list').magnificPopup({
				delegate: 'a',
				type: 'image',
				gallery: {
					enabled: true
				}
			});

			$('.featured-image').magnificPopup({
				delegate: 'a',
				type: 'image',
				mainClass: 'mfp-with-zoom',
				zoom: {
					enabled: true,
					duration: 300,
					easing: 'ease-in-out',
					opener: function(openerElement) {
						return openerElement.is('img') ? openerElement : openerElement.find('img');
					}
				}
			});

			$('.cherry-projects-additional-image-list').magnificPopup({
				delegate: 'a',
				type: 'image',
				gallery: {
					enabled: true
				},
				mainClass: 'mfp-with-zoom',
				zoom: {
					enabled: true,
					duration: 300,
					easing: 'ease-in-out',
					opener: function(openerElement) {
						return openerElement.is('img') ? openerElement : openerElement.find('img');
					}
				}
			});

			$( '.cherry-projects-additional-image-list' ).each( function() {
				var $this         = $( this ),
					$thisList     = $( '.additional-image-list', $this ),
					$listItems    = $( '.image-item', $thisList ),
					listingLayout = $thisList.data( 'listing-layout' ),
					columnNumber  = $thisList.data( 'column-number' ),
					imageMargin  = $thisList.data( 'image-margin' );

					switch ( listingLayout ) {
						case 'grid-layout':
							var itemWidth = ( 100 / +columnNumber ).toFixed(3);
							$listItems.css( {
								'-webkit-flex-basis': itemWidth + '%',
								'flex-basis': itemWidth + '%',
							} );

							$('.inner-wrapper', $listItems ).css( {
								'margin': Math.floor( imageMargin / 2 ) + 'px',
							} );

							$thisList.css( {
								'margin': - Math.floor( imageMargin / 2 ) + 'px',
							} );

						break;
						case 'masonry-layout':
							$thisList.css( {
								'-webkit-column-count': +columnNumber,
								'column-count': +columnNumber,
								'-webkit-column-gap': +imageMargin,
								'column-gap': +imageMargin,
							} );

							$('.inner-wrapper', $listItems ).css( {
								'margin-bottom': imageMargin + 'px',
							} );

						break;
					}
			} );
		},
		sliderInit: function( self ) {
			$( '.cherry-projects-slider__instance' ).each( function() {
				var slider = $(this),
					settings = slider.data('settings'),
					sliderId = settings['id'];

				if ( $( '.projects-slider__item', '#' + sliderId ).length > 0 ) {
					$( '#' + sliderId ).sliderPro( {
						width: settings['width'],
						height: settings['height'],
						orientation: 'horizontal',
						imageScaleMode: settings['scale-mode'],
						forceSize: settings['force-size'],
						aspectRatio: -1,
						visibleSize: settings['visible-size'],
						slideDistance: +settings['distance'],
						slideAnimationDuration: +settings['duration'],
						fade: false,
						arrows: settings['navigation'],
						fadeArrows: true,
						buttons: false,
						autoplay: settings['autoplay'],
						fullScreen: true,
						shuffle: false,
						loop: settings['loop'],
						waitForLayers: false,
						thumbnailArrows: false,
						thumbnailsPosition: settings['thumbnails-position'],
						thumbnailWidth: settings['thumbnails-width'],
						thumbnailHeight: settings['thumbnails-height'],
						init: function() {
							$( this ).resize();
						},
						breakpoints: {
							992: {
								height: +settings['height'] * 0.75,
							},
							768: {
								height: +settings['height'] * 0.5
							}
						}
					} );
				}
			});//each end
		}
	}
	CherryJsCore.cherryProjectsFrontSingleScripts.init();
}(jQuery));


// fin cherry projects single scripts 

!function(t,i){"use strict";i.utilites.namespace("post_formats"),i.post_formats={init:function(){var t=this;i.status.document_ready?t.render(t):i.variable.$document.on("ready",t.render(t))},render:function(t){t.initalize("slider"),t.initalize("popup")},initalize:function(i){t(window).load(function(){t("*[data-cherry"+i+'="1"]').each(function(){var n=t(this).data(i),e=t(this).data("init");return t(this).data("initalized",!1),t(this).trigger({type:"cherry-post-formats-custom-init",item:t(this),object:i}),!0===t(this).data("initalized")?1:n?t.isFunction(jQuery.fn[n])?(t(this)[n](e),void 0):!1:!1})})}},i.post_formats.init()}(jQuery,window.CherryJsCore);




// fin cherry post formats min js 

/*jshint browser:true */
/*!
* FitVids 1.1
*
* Copyright 2013, Chris Coyier - http://css-tricks.com + Dave Rupert - http://daverupert.com
* Credit to Thierry Koblentz - http://www.alistapart.com/articles/creating-intrinsic-ratios-for-video/
* Released under the WTFPL license - http://sam.zoy.org/wtfpl/
*
*/

;(function( $ ){

  'use strict';

  $.fn.fitVids = function( options ) {
    var settings = {
      customSelector: null,
      ignore: null
    };

    if(!document.getElementById('fit-vids-style')) {
      // appendStyles: https://github.com/toddmotto/fluidvids/blob/master/dist/fluidvids.js
      var head = document.head || document.getElementsByTagName('head')[0];
      var css = '.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}';
      var div = document.createElement("div");
      div.innerHTML = '<p>x</p><style id="fit-vids-style">' + css + '</style>';
      head.appendChild(div.childNodes[1]);
    }

    if ( options ) {
      $.extend( settings, options );
    }

    return this.each(function(){
      var selectors = [
        'iframe[src*="player.vimeo.com"]',
        'iframe[src*="youtube.com"]',
        'iframe[src*="youtube-nocookie.com"]',
        'iframe[src*="kickstarter.com"][src*="video.html"]',
        'object',
        'embed'
      ];

      if (settings.customSelector) {
        selectors.push(settings.customSelector);
      }

      var ignoreList = '.fitvidsignore';

      if(settings.ignore) {
        ignoreList = ignoreList + ', ' + settings.ignore;
      }

      var $allVideos = $(this).find(selectors.join(','));
      $allVideos = $allVideos.not('object object'); // SwfObj conflict patch
      $allVideos = $allVideos.not(ignoreList); // Disable FitVids on this video.

      $allVideos.each(function(count){
        var $this = $(this);
        if($this.parents(ignoreList).length > 0) {
          return; // Disable FitVids on this video.
        }
        if (this.tagName.toLowerCase() === 'embed' && $this.parent('object').length || $this.parent('.fluid-width-video-wrapper').length) { return; }
        if ((!$this.css('height') && !$this.css('width')) && (isNaN($this.attr('height')) || isNaN($this.attr('width'))))
        {
          $this.attr('height', 9);
          $this.attr('width', 16);
        }
        var height = ( this.tagName.toLowerCase() === 'object' || ($this.attr('height') && !isNaN(parseInt($this.attr('height'), 10))) ) ? parseInt($this.attr('height'), 10) : $this.height(),
            width = !isNaN(parseInt($this.attr('width'), 10)) ? parseInt($this.attr('width'), 10) : $this.width(),
            aspectRatio = height / width;
        if(!$this.attr('id')){
          var videoID = 'fitvid' + count;
          $this.attr('id', videoID);
        }
        $this.wrap('<div class="fluid-width-video-wrapper"></div>').parent('.fluid-width-video-wrapper').css('padding-top', (aspectRatio * 100)+'%');
        $this.removeAttr('height').removeAttr('width');
      });
    });
  };
// Works with either jQuery or Zepto
})( window.jQuery || window.Zepto );


// fin fitvids 
/*!
Waypoints - 4.0.0
Copyright © 2011-2015 Caleb Troughton
Licensed under the MIT license.
https://github.com/imakewebthings/waypoints/blog/master/licenses.txt
*/
!function(){"use strict";function t(o){if(!o)throw new Error("No options passed to Waypoint constructor");if(!o.element)throw new Error("No element option passed to Waypoint constructor");if(!o.handler)throw new Error("No handler option passed to Waypoint constructor");this.key="waypoint-"+e,this.options=t.Adapter.extend({},t.defaults,o),this.element=this.options.element,this.adapter=new t.Adapter(this.element),this.callback=o.handler,this.axis=this.options.horizontal?"horizontal":"vertical",this.enabled=this.options.enabled,this.triggerPoint=null,this.group=t.Group.findOrCreate({name:this.options.group,axis:this.axis}),this.context=t.Context.findOrCreateByElement(this.options.context),t.offsetAliases[this.options.offset]&&(this.options.offset=t.offsetAliases[this.options.offset]),this.group.add(this),this.context.add(this),i[this.key]=this,e+=1}var e=0,i={};t.prototype.queueTrigger=function(t){this.group.queueTrigger(this,t)},t.prototype.trigger=function(t){this.enabled&&this.callback&&this.callback.apply(this,t)},t.prototype.destroy=function(){this.context.remove(this),this.group.remove(this),delete i[this.key]},t.prototype.disable=function(){return this.enabled=!1,this},t.prototype.enable=function(){return this.context.refresh(),this.enabled=!0,this},t.prototype.next=function(){return this.group.next(this)},t.prototype.previous=function(){return this.group.previous(this)},t.invokeAll=function(t){var e=[];for(var o in i)e.push(i[o]);for(var n=0,r=e.length;r>n;n++)e[n][t]()},t.destroyAll=function(){t.invokeAll("destroy")},t.disableAll=function(){t.invokeAll("disable")},t.enableAll=function(){t.invokeAll("enable")},t.refreshAll=function(){t.Context.refreshAll()},t.viewportHeight=function(){return window.innerHeight||document.documentElement.clientHeight},t.viewportWidth=function(){return document.documentElement.clientWidth},t.adapters=[],t.defaults={context:window,continuous:!0,enabled:!0,group:"default",horizontal:!1,offset:0},t.offsetAliases={"bottom-in-view":function(){return this.context.innerHeight()-this.adapter.outerHeight()},"right-in-view":function(){return this.context.innerWidth()-this.adapter.outerWidth()}},window.Waypoint=t}(),function(){"use strict";function t(t){window.setTimeout(t,1e3/60)}function e(t){this.element=t,this.Adapter=n.Adapter,this.adapter=new this.Adapter(t),this.key="waypoint-context-"+i,this.didScroll=!1,this.didResize=!1,this.oldScroll={x:this.adapter.scrollLeft(),y:this.adapter.scrollTop()},this.waypoints={vertical:{},horizontal:{}},t.waypointContextKey=this.key,o[t.waypointContextKey]=this,i+=1,this.createThrottledScrollHandler(),this.createThrottledResizeHandler()}var i=0,o={},n=window.Waypoint,r=window.onload;e.prototype.add=function(t){var e=t.options.horizontal?"horizontal":"vertical";this.waypoints[e][t.key]=t,this.refresh()},e.prototype.checkEmpty=function(){var t=this.Adapter.isEmptyObject(this.waypoints.horizontal),e=this.Adapter.isEmptyObject(this.waypoints.vertical);t&&e&&(this.adapter.off(".waypoints"),delete o[this.key])},e.prototype.createThrottledResizeHandler=function(){function t(){e.handleResize(),e.didResize=!1}var e=this;this.adapter.on("resize.waypoints",function(){e.didResize||(e.didResize=!0,n.requestAnimationFrame(t))})},e.prototype.createThrottledScrollHandler=function(){function t(){e.handleScroll(),e.didScroll=!1}var e=this;this.adapter.on("scroll.waypoints",function(){(!e.didScroll||n.isTouch)&&(e.didScroll=!0,n.requestAnimationFrame(t))})},e.prototype.handleResize=function(){n.Context.refreshAll()},e.prototype.handleScroll=function(){var t={},e={horizontal:{newScroll:this.adapter.scrollLeft(),oldScroll:this.oldScroll.x,forward:"right",backward:"left"},vertical:{newScroll:this.adapter.scrollTop(),oldScroll:this.oldScroll.y,forward:"down",backward:"up"}};for(var i in e){var o=e[i],n=o.newScroll>o.oldScroll,r=n?o.forward:o.backward;for(var s in this.waypoints[i]){var a=this.waypoints[i][s],l=o.oldScroll<a.triggerPoint,h=o.newScroll>=a.triggerPoint,p=l&&h,u=!l&&!h;(p||u)&&(a.queueTrigger(r),t[a.group.id]=a.group)}}for(var c in t)t[c].flushTriggers();this.oldScroll={x:e.horizontal.newScroll,y:e.vertical.newScroll}},e.prototype.innerHeight=function(){return this.element==this.element.window?n.viewportHeight():this.adapter.innerHeight()},e.prototype.remove=function(t){delete this.waypoints[t.axis][t.key],this.checkEmpty()},e.prototype.innerWidth=function(){return this.element==this.element.window?n.viewportWidth():this.adapter.innerWidth()},e.prototype.destroy=function(){var t=[];for(var e in this.waypoints)for(var i in this.waypoints[e])t.push(this.waypoints[e][i]);for(var o=0,n=t.length;n>o;o++)t[o].destroy()},e.prototype.refresh=function(){var t,e=this.element==this.element.window,i=e?void 0:this.adapter.offset(),o={};this.handleScroll(),t={horizontal:{contextOffset:e?0:i.left,contextScroll:e?0:this.oldScroll.x,contextDimension:this.innerWidth(),oldScroll:this.oldScroll.x,forward:"right",backward:"left",offsetProp:"left"},vertical:{contextOffset:e?0:i.top,contextScroll:e?0:this.oldScroll.y,contextDimension:this.innerHeight(),oldScroll:this.oldScroll.y,forward:"down",backward:"up",offsetProp:"top"}};for(var r in t){var s=t[r];for(var a in this.waypoints[r]){var l,h,p,u,c,d=this.waypoints[r][a],f=d.options.offset,w=d.triggerPoint,y=0,g=null==w;d.element!==d.element.window&&(y=d.adapter.offset()[s.offsetProp]),"function"==typeof f?f=f.apply(d):"string"==typeof f&&(f=parseFloat(f),d.options.offset.indexOf("%")>-1&&(f=Math.ceil(s.contextDimension*f/100))),l=s.contextScroll-s.contextOffset,d.triggerPoint=y+l-f,h=w<s.oldScroll,p=d.triggerPoint>=s.oldScroll,u=h&&p,c=!h&&!p,!g&&u?(d.queueTrigger(s.backward),o[d.group.id]=d.group):!g&&c?(d.queueTrigger(s.forward),o[d.group.id]=d.group):g&&s.oldScroll>=d.triggerPoint&&(d.queueTrigger(s.forward),o[d.group.id]=d.group)}}return n.requestAnimationFrame(function(){for(var t in o)o[t].flushTriggers()}),this},e.findOrCreateByElement=function(t){return e.findByElement(t)||new e(t)},e.refreshAll=function(){for(var t in o)o[t].refresh()},e.findByElement=function(t){return o[t.waypointContextKey]},window.onload=function(){r&&r(),e.refreshAll()},n.requestAnimationFrame=function(e){var i=window.requestAnimationFrame||window.mozRequestAnimationFrame||window.webkitRequestAnimationFrame||t;i.call(window,e)},n.Context=e}(),function(){"use strict";function t(t,e){return t.triggerPoint-e.triggerPoint}function e(t,e){return e.triggerPoint-t.triggerPoint}function i(t){this.name=t.name,this.axis=t.axis,this.id=this.name+"-"+this.axis,this.waypoints=[],this.clearTriggerQueues(),o[this.axis][this.name]=this}var o={vertical:{},horizontal:{}},n=window.Waypoint;i.prototype.add=function(t){this.waypoints.push(t)},i.prototype.clearTriggerQueues=function(){this.triggerQueues={up:[],down:[],left:[],right:[]}},i.prototype.flushTriggers=function(){for(var i in this.triggerQueues){var o=this.triggerQueues[i],n="up"===i||"left"===i;o.sort(n?e:t);for(var r=0,s=o.length;s>r;r+=1){var a=o[r];(a.options.continuous||r===o.length-1)&&a.trigger([i])}}this.clearTriggerQueues()},i.prototype.next=function(e){this.waypoints.sort(t);var i=n.Adapter.inArray(e,this.waypoints),o=i===this.waypoints.length-1;return o?null:this.waypoints[i+1]},i.prototype.previous=function(e){this.waypoints.sort(t);var i=n.Adapter.inArray(e,this.waypoints);return i?this.waypoints[i-1]:null},i.prototype.queueTrigger=function(t,e){this.triggerQueues[e].push(t)},i.prototype.remove=function(t){var e=n.Adapter.inArray(t,this.waypoints);e>-1&&this.waypoints.splice(e,1)},i.prototype.first=function(){return this.waypoints[0]},i.prototype.last=function(){return this.waypoints[this.waypoints.length-1]},i.findOrCreate=function(t){return o[t.axis][t.name]||new i(t)},n.Group=i}(),function(){"use strict";function t(t){this.$element=e(t)}var e=window.jQuery,i=window.Waypoint;e.each(["innerHeight","innerWidth","off","offset","on","outerHeight","outerWidth","scrollLeft","scrollTop"],function(e,i){t.prototype[i]=function(){var t=Array.prototype.slice.call(arguments);return this.$element[i].apply(this.$element,t)}}),e.each(["extend","inArray","isEmptyObject"],function(i,o){t[o]=e[o]}),i.adapters.push({name:"jquery",Adapter:t}),i.Adapter=t}(),function(){"use strict";function t(t){return function(){var i=[],o=arguments[0];return t.isFunction(arguments[0])&&(o=t.extend({},arguments[1]),o.handler=arguments[0]),this.each(function(){var n=t.extend({},o,{element:this});"string"==typeof n.context&&(n.context=t(this).closest(n.context)[0]),i.push(new e(n))}),i}}var e=window.Waypoint;window.jQuery&&(window.jQuery.fn.waypoint=t(window.jQuery)),window.Zepto&&(window.Zepto.fn.waypoint=t(window.Zepto))}();

// fin Waypoints

/*! jQuery Mobile v1.4.5 | Copyright 2010, 2014 jQuery Foundation, Inc. | jquery.org/license */

(function(e,t,n){typeof define=="function"&&define.amd?define(["jquery"],function(r){return n(r,e,t),r.mobile}):n(e.jQuery,e,t)})(this,document,function(e,t,n,r){(function(e,t,n,r){function T(e){while(e&&typeof e.originalEvent!="undefined")e=e.originalEvent;return e}function N(t,n){var i=t.type,s,o,a,l,c,h,p,d,v;t=e.Event(t),t.type=n,s=t.originalEvent,o=e.event.props,i.search(/^(mouse|click)/)>-1&&(o=f);if(s)for(p=o.length,l;p;)l=o[--p],t[l]=s[l];i.search(/mouse(down|up)|click/)>-1&&!t.which&&(t.which=1);if(i.search(/^touch/)!==-1){a=T(s),i=a.touches,c=a.changedTouches,h=i&&i.length?i[0]:c&&c.length?c[0]:r;if(h)for(d=0,v=u.length;d<v;d++)l=u[d],t[l]=h[l]}return t}function C(t){var n={},r,s;while(t){r=e.data(t,i);for(s in r)r[s]&&(n[s]=n.hasVirtualBinding=!0);t=t.parentNode}return n}function k(t,n){var r;while(t){r=e.data(t,i);if(r&&(!n||r[n]))return t;t=t.parentNode}return null}function L(){g=!1}function A(){g=!0}function O(){E=0,v.length=0,m=!1,A()}function M(){L()}function _(){D(),c=setTimeout(function(){c=0,O()},e.vmouse.resetTimerDuration)}function D(){c&&(clearTimeout(c),c=0)}function P(t,n,r){var i;if(r&&r[t]||!r&&k(n.target,t))i=N(n,t),e(n.target).trigger(i);return i}function H(t){var n=e.data(t.target,s),r;!m&&(!E||E!==n)&&(r=P("v"+t.type,t),r&&(r.isDefaultPrevented()&&t.preventDefault(),r.isPropagationStopped()&&t.stopPropagation(),r.isImmediatePropagationStopped()&&t.stopImmediatePropagation()))}function B(t){var n=T(t).touches,r,i,o;n&&n.length===1&&(r=t.target,i=C(r),i.hasVirtualBinding&&(E=w++,e.data(r,s,E),D(),M(),d=!1,o=T(t).touches[0],h=o.pageX,p=o.pageY,P("vmouseover",t,i),P("vmousedown",t,i)))}function j(e){if(g)return;d||P("vmousecancel",e,C(e.target)),d=!0,_()}function F(t){if(g)return;var n=T(t).touches[0],r=d,i=e.vmouse.moveDistanceThreshold,s=C(t.target);d=d||Math.abs(n.pageX-h)>i||Math.abs(n.pageY-p)>i,d&&!r&&P("vmousecancel",t,s),P("vmousemove",t,s),_()}function I(e){if(g)return;A();var t=C(e.target),n,r;P("vmouseup",e,t),d||(n=P("vclick",e,t),n&&n.isDefaultPrevented()&&(r=T(e).changedTouches[0],v.push({touchID:E,x:r.clientX,y:r.clientY}),m=!0)),P("vmouseout",e,t),d=!1,_()}function q(t){var n=e.data(t,i),r;if(n)for(r in n)if(n[r])return!0;return!1}function R(){}function U(t){var n=t.substr(1);return{setup:function(){q(this)||e.data(this,i,{});var r=e.data(this,i);r[t]=!0,l[t]=(l[t]||0)+1,l[t]===1&&b.bind(n,H),e(this).bind(n,R),y&&(l.touchstart=(l.touchstart||0)+1,l.touchstart===1&&b.bind("touchstart",B).bind("touchend",I).bind("touchmove",F).bind("scroll",j))},teardown:function(){--l[t],l[t]||b.unbind(n,H),y&&(--l.touchstart,l.touchstart||b.unbind("touchstart",B).unbind("touchmove",F).unbind("touchend",I).unbind("scroll",j));var r=e(this),s=e.data(this,i);s&&(s[t]=!1),r.unbind(n,R),q(this)||r.removeData(i)}}}var i="virtualMouseBindings",s="virtualTouchID",o="vmouseover vmousedown vmousemove vmouseup vclick vmouseout vmousecancel".split(" "),u="clientX clientY pageX pageY screenX screenY".split(" "),a=e.event.mouseHooks?e.event.mouseHooks.props:[],f=e.event.props.concat(a),l={},c=0,h=0,p=0,d=!1,v=[],m=!1,g=!1,y="addEventListener"in n,b=e(n),w=1,E=0,S,x;e.vmouse={moveDistanceThreshold:10,clickDistanceThreshold:10,resetTimerDuration:1500};for(x=0;x<o.length;x++)e.event.special[o[x]]=U(o[x]);y&&n.addEventListener("click",function(t){var n=v.length,r=t.target,i,o,u,a,f,l;if(n){i=t.clientX,o=t.clientY,S=e.vmouse.clickDistanceThreshold,u=r;while(u){for(a=0;a<n;a++){f=v[a],l=0;if(u===r&&Math.abs(f.x-i)<S&&Math.abs(f.y-o)<S||e.data(u,s)===f.touchID){t.preventDefault(),t.stopPropagation();return}}u=u.parentNode}}},!0)})(e,t,n),function(e){e.mobile={}}(e),function(e,t){var r={touch:"ontouchend"in n};e.mobile.support=e.mobile.support||{},e.extend(e.support,r),e.extend(e.mobile.support,r)}(e),function(e,t,r){function l(t,n,i,s){var o=i.type;i.type=n,s?e.event.trigger(i,r,t):e.event.dispatch.call(t,i),i.type=o}var i=e(n),s=e.mobile.support.touch,o="touchmove scroll",u=s?"touchstart":"mousedown",a=s?"touchend":"mouseup",f=s?"touchmove":"mousemove";e.each("touchstart touchmove touchend tap taphold swipe swipeleft swiperight scrollstart scrollstop".split(" "),function(t,n){e.fn[n]=function(e){return e?this.bind(n,e):this.trigger(n)},e.attrFn&&(e.attrFn[n]=!0)}),e.event.special.scrollstart={enabled:!0,setup:function(){function s(e,n){r=n,l(t,r?"scrollstart":"scrollstop",e)}var t=this,n=e(t),r,i;n.bind(o,function(t){if(!e.event.special.scrollstart.enabled)return;r||s(t,!0),clearTimeout(i),i=setTimeout(function(){s(t,!1)},50)})},teardown:function(){e(this).unbind(o)}},e.event.special.tap={tapholdThreshold:750,emitTapOnTaphold:!0,setup:function(){var t=this,n=e(t),r=!1;n.bind("vmousedown",function(s){function a(){clearTimeout(u)}function f(){a(),n.unbind("vclick",c).unbind("vmouseup",a),i.unbind("vmousecancel",f)}function c(e){f(),!r&&o===e.target?l(t,"tap",e):r&&e.preventDefault()}r=!1;if(s.which&&s.which!==1)return!1;var o=s.target,u;n.bind("vmouseup",a).bind("vclick",c),i.bind("vmousecancel",f),u=setTimeout(function(){e.event.special.tap.emitTapOnTaphold||(r=!0),l(t,"taphold",e.Event("taphold",{target:o}))},e.event.special.tap.tapholdThreshold)})},teardown:function(){e(this).unbind("vmousedown").unbind("vclick").unbind("vmouseup"),i.unbind("vmousecancel")}},e.event.special.swipe={scrollSupressionThreshold:30,durationThreshold:1e3,horizontalDistanceThreshold:30,verticalDistanceThreshold:30,getLocation:function(e){var n=t.pageXOffset,r=t.pageYOffset,i=e.clientX,s=e.clientY;if(e.pageY===0&&Math.floor(s)>Math.floor(e.pageY)||e.pageX===0&&Math.floor(i)>Math.floor(e.pageX))i-=n,s-=r;else if(s<e.pageY-r||i<e.pageX-n)i=e.pageX-n,s=e.pageY-r;return{x:i,y:s}},start:function(t){var n=t.originalEvent.touches?t.originalEvent.touches[0]:t,r=e.event.special.swipe.getLocation(n);return{time:(new Date).getTime(),coords:[r.x,r.y],origin:e(t.target)}},stop:function(t){var n=t.originalEvent.touches?t.originalEvent.touches[0]:t,r=e.event.special.swipe.getLocation(n);return{time:(new Date).getTime(),coords:[r.x,r.y]}},handleSwipe:function(t,n,r,i){if(n.time-t.time<e.event.special.swipe.durationThreshold&&Math.abs(t.coords[0]-n.coords[0])>e.event.special.swipe.horizontalDistanceThreshold&&Math.abs(t.coords[1]-n.coords[1])<e.event.special.swipe.verticalDistanceThreshold){var s=t.coords[0]>n.coords[0]?"swipeleft":"swiperight";return l(r,"swipe",e.Event("swipe",{target:i,swipestart:t,swipestop:n}),!0),l(r,s,e.Event(s,{target:i,swipestart:t,swipestop:n}),!0),!0}return!1},eventInProgress:!1,setup:function(){var t,n=this,r=e(n),s={};t=e.data(this,"mobile-events"),t||(t={length:0},e.data(this,"mobile-events",t)),t.length++,t.swipe=s,s.start=function(t){if(e.event.special.swipe.eventInProgress)return;e.event.special.swipe.eventInProgress=!0;var r,o=e.event.special.swipe.start(t),u=t.target,l=!1;s.move=function(t){if(!o||t.isDefaultPrevented())return;r=e.event.special.swipe.stop(t),l||(l=e.event.special.swipe.handleSwipe(o,r,n,u),l&&(e.event.special.swipe.eventInProgress=!1)),Math.abs(o.coords[0]-r.coords[0])>e.event.special.swipe.scrollSupressionThreshold&&t.preventDefault()},s.stop=function(){l=!0,e.event.special.swipe.eventInProgress=!1,i.off(f,s.move),s.move=null},i.on(f,s.move).one(a,s.stop)},r.on(u,s.start)},teardown:function(){var t,n;t=e.data(this,"mobile-events"),t&&(n=t.swipe,delete t.swipe,t.length--,t.length===0&&e.removeData(this,"mobile-events")),n&&(n.start&&e(this).off(u,n.start),n.move&&i.off(f,n.move),n.stop&&i.off(a,n.stop))}},e.each({scrollstop:"scrollstart",taphold:"tap",swipeleft:"swipe.left",swiperight:"swipe.right"},function(t,n){e.event.special[t]={setup:function(){e(this).bind(n,e.noop)},teardown:function(){e(this).unbind(n)}}})}(e,this)});

// fin jq mobile custom .mn .js 

(function($){
  jQuery.fn.closest_descendent = function( selector ) {
    var $found,
      $current_children = this.children();

    while ( $current_children.length ) {
      $found = $current_children.filter( selector );
      if ( $found.length ) {
        break;
      }
      $current_children = $current_children.children();
    }

    return $found;
  };
}(jQuery));


// fin jq closet decendent 

(function($){
	jQuery.fn.reverse = [].reverse;
}(jQuery));


// fin reverse 

(function($) {
  $.tm_pb_simple_carousel = function(el, options) {
    var settings = $.extend( {
      slide_duration	: 500,
    }, options );

    var $tm_carousel 			= $(el),
      $carousel_items 		= $tm_carousel.find('.tm_pb_carousel_items'),
      $the_carousel_items 	= $carousel_items.find('.tm_pb_carousel_item');

    $tm_carousel.tm_animation_running = false;

    $tm_carousel.addClass('container-width-change-notify').on('containerWidthChanged', function( event ){
      set_carousel_columns( $tm_carousel );
      set_carousel_height( $tm_carousel );
    });

    $carousel_items.data('items', $the_carousel_items.toArray() );
    $tm_carousel.data('columns_setting_up', false );

    $carousel_items.prepend('<div class="tm-pb-slider-arrows"><a class="tm-pb-slider-arrow et-pb-arrow-prev" href="#">' + '<span>' + tm_pb_custom.previous + '</span>' + '</a><a class="tm-pb-slider-arrow et-pb-arrow-next" href="#">' + '<span>' + tm_pb_custom.next + '</span>' + '</a></div>');

    set_carousel_columns( $tm_carousel );
    set_carousel_height( $tm_carousel );

    $tm_carousel_next 	= $tm_carousel.find( '.tm-pb-arrow-next' );
    $tm_carousel_prev 	= $tm_carousel.find( '.tm-pb-arrow-prev'  );

    $tm_carousel_next.click( function(){
      if ( $tm_carousel.tm_animation_running ) return false;

      $tm_carousel.tm_carousel_move_to( 'next' );

      return false;
    } );

    $tm_carousel_prev.click( function(){
      if ( $tm_carousel.tm_animation_running ) return false;

      $tm_carousel.tm_carousel_move_to( 'previous' );

      return false;
    } );

    // swipe support requires et-jquery-touch-mobile
    $tm_carousel.on( 'swipeleft', function() {
      $tm_carousel.tm_carousel_move_to( 'next' );
    });
    $tm_carousel.on( 'swiperight', function() {
      $tm_carousel.tm_carousel_move_to( 'previous' );
    });

    function set_carousel_height( $the_carousel ) {
      var carousel_items_width = $the_carousel_items.width(),
        carousel_items_height = $the_carousel_items.height();

      $carousel_items.css('height', carousel_items_height + 'px' );
    }

    function set_carousel_columns( $the_carousel ) {
      var columns,
        $carousel_parent = $the_carousel.parents('.tm_pb_column'),
        carousel_items_width = $carousel_items.width(),
        carousel_item_count = $the_carousel_items.length;

      if ( $carousel_parent.hasClass('tm_pb_column_4_4') || $carousel_parent.hasClass('tm_pb_column_3_4') || $carousel_parent.hasClass('tm_pb_column_2_3') ) {
        if ( $tm_window.width() < 768 ) {
          columns = 3;
        } else {
          columns = 4;
        }
      } else if ( $carousel_parent.hasClass('tm_pb_column_1_2') || $carousel_parent.hasClass('tm_pb_column_3_8') || $carousel_parent.hasClass('tm_pb_column_1_3') ) {
        columns = 3;
      } else if ( $carousel_parent.hasClass('tm_pb_column_1_4') ) {
        if ( $tm_window.width() > 480 && $tm_window.width() < 980 ) {
          columns = 3;
        } else {
          columns = 2;
        }
      }

      if ( columns === $carousel_items.data('portfolio-columns') ) {
        return;
      }

      if ( $the_carousel.data('columns_setting_up') ) {
        return;
      }

      $the_carousel.data('columns_setting_up', true );

      // store last setup column
      $carousel_items.removeClass('columns-' + $carousel_items.data('portfolio-columns') );
      $carousel_items.addClass('columns-' + columns );
      $carousel_items.data('portfolio-columns', columns );

      // kill all previous groups to get ready to re-group
      if ( $carousel_items.find('.tm-carousel-group').length ) {
        $the_carousel_items.appendTo( $carousel_items );
        $carousel_items.find('.tm-carousel-group').remove();
      }

      // setup the grouping
      var the_carousel_items = $carousel_items.data('items'),
        $carousel_group = $('<div class="tm-carousel-group active">').appendTo( $carousel_items );

      $the_carousel_items.data('position', '');
      if ( the_carousel_items.length <= columns ) {
        $carousel_items.find('.tm-pb-slider-arrows').hide();
      } else {
        $carousel_items.find('.tm-pb-slider-arrows').show();
      }

      for ( position = 1, x=0 ;x < the_carousel_items.length; x++, position++ ) {
        if ( x < columns ) {
          $( the_carousel_items[x] ).show();
          $( the_carousel_items[x] ).appendTo( $carousel_group );
          $( the_carousel_items[x] ).data('position', position );
          $( the_carousel_items[x] ).addClass('position_' + position );
        } else {
          position = $( the_carousel_items[x] ).data('position');
          $( the_carousel_items[x] ).removeClass('position_' + position );
          $( the_carousel_items[x] ).data('position', '' );
          $( the_carousel_items[x] ).hide();
        }
      }

      $the_carousel.data('columns_setting_up', false );

    } /* end set_carousel_columns() */

    $tm_carousel.tm_carousel_move_to = function ( direction ) {
      var $active_carousel_group 	= $carousel_items.find('.tm-carousel-group.active'),
        items 					= $carousel_items.data('items'),
        columns 				= $carousel_items.data('portfolio-columns');

      $tm_carousel.tm_animation_running = true;

      var left = 0;
      $active_carousel_group.children().each(function(){
        $(this).css({'position':'absolute', 'left': left });
        left = left + $(this).outerWidth(true);
      });

      if ( direction == 'next' ) {
        var $next_carousel_group,
          current_position = 1,
          next_position = 1,
          active_items_start = items.indexOf( $active_carousel_group.children().first()[0] ),
          active_items_end = active_items_start + columns,
          next_items_start = active_items_end,
          next_items_end = next_items_start + columns;

        $next_carousel_group = $('<div class="tm-carousel-group next" style="display: none;left: 100%;position: absolute;top: 0;">').insertAfter( $active_carousel_group );
        $next_carousel_group.css({ 'width': $active_carousel_group.innerWidth() }).show();

        // this is an endless loop, so it can decide internally when to break out, so that next_position
        // can get filled up, even to the extent of an element having both and current_ and next_ position
        for( x = 0, total = 0 ; ; x++, total++ ) {
          if ( total >= active_items_start && total < active_items_end ) {
            $( items[x] ).addClass( 'changing_position current_position current_position_' + current_position );
            $( items[x] ).data('current_position', current_position );
            current_position++;
          }

          if ( total >= next_items_start && total < next_items_end ) {
            $( items[x] ).data('next_position', next_position );
            $( items[x] ).addClass('changing_position next_position next_position_' + next_position );

            if ( !$( items[x] ).hasClass( 'current_position' ) ) {
              $( items[x] ).addClass('container_append');
            } else {
              $( items[x] ).clone(true).appendTo( $active_carousel_group ).hide().addClass('delayed_container_append_dup').attr('id', $( items[x] ).attr('id') + '-dup' );
              $( items[x] ).addClass('delayed_container_append');
            }

            next_position++;
          }

          if ( next_position > columns ) {
            break;
          }

          if ( x >= ( items.length -1 )) {
            x = -1;
          }
        }

        var sorted = $carousel_items.find('.container_append, .delayed_container_append_dup').sort(function (a, b) {
          var el_a_position = parseInt( $(a).data('next_position') );
          var el_b_position = parseInt( $(b).data('next_position') );
          return ( el_a_position < el_b_position ) ? -1 : ( el_a_position > el_b_position ) ? 1 : 0;
        });

        $( sorted ).show().appendTo( $next_carousel_group );

        var left = 0;
        $next_carousel_group.children().each(function(){
          $(this).css({'position':'absolute', 'left': left });
          left = left + $(this).outerWidth(true);
        });

        $active_carousel_group.animate({
          left: '-100%'
        }, {
          duration: settings.slide_duration,
          complete: function() {
            $carousel_items.find('.delayed_container_append').each(function(){
              left = $( '#' + $(this).attr('id') + '-dup' ).css('left');
              $(this).css({'position':'absolute', 'left': left });
              $(this).appendTo( $next_carousel_group );
            });

            $active_carousel_group.removeClass('active');
            $active_carousel_group.children().each(function(){
              position = $(this).data('position');
              current_position = $(this).data('current_position');
              $(this).removeClass('position_' + position + ' ' + 'changing_position current_position current_position_' + current_position );
              $(this).data('position', '');
              $(this).data('current_position', '');
              $(this).hide();
              $(this).css({'position': '', 'left': ''});
              $(this).appendTo( $carousel_items );
            });

            $active_carousel_group.remove();

          }
        } );

        next_left = $active_carousel_group.width() + parseInt( $the_carousel_items.first().css('marginRight').slice(0, -2) );
        $next_carousel_group.addClass('active').css({'position':'absolute', 'top':0, left: next_left });
        $next_carousel_group.animate({
          left: '0%'
        }, {
          duration: settings.slide_duration,
          complete: function(){
            $next_carousel_group.removeClass('next').addClass('active').css({'position':'', 'width':'', 'top':'', 'left': ''});

            $next_carousel_group.find('.changing_position').each(function( index ){
              position = $(this).data('position');
              current_position = $(this).data('current_position');
              next_position = $(this).data('next_position');
              $(this).removeClass('container_append delayed_container_append position_' + position + ' ' + 'changing_position current_position current_position_' + current_position + ' next_position next_position_' + next_position );
              $(this).data('current_position', '');
              $(this).data('next_position', '');
              $(this).data('position', ( index + 1 ) );
            });

            $next_carousel_group.children().css({'position': '', 'left': ''});
            $next_carousel_group.find('.delayed_container_append_dup').remove();

            $tm_carousel.tm_animation_running = false;
          }
        } );

      } else if ( direction == 'previous' ) {
        var $prev_carousel_group,
          current_position = columns,
          prev_position = columns,
          columns_span = columns - 1,
          active_items_start = items.indexOf( $active_carousel_group.children().last()[0] ),
          active_items_end = active_items_start - columns_span,
          prev_items_start = active_items_end - 1,
          prev_items_end = prev_items_start - columns_span;

        $prev_carousel_group = $('<div class="tm-carousel-group prev" style="display: none;left: 100%;position: absolute;top: 0;">').insertBefore( $active_carousel_group );
        $prev_carousel_group.css({ 'left': '-' + $active_carousel_group.innerWidth(), 'width': $active_carousel_group.innerWidth() }).show();

        // this is an endless loop, so it can decide internally when to break out, so that next_position
        // can get filled up, even to the extent of an element having both and current_ and next_ position
        for( x = ( items.length - 1 ), total = ( items.length - 1 ) ; ; x--, total-- ) {

          if ( total <= active_items_start && total >= active_items_end ) {
            $( items[x] ).addClass( 'changing_position current_position current_position_' + current_position );
            $( items[x] ).data('current_position', current_position );
            current_position--;
          }

          if ( total <= prev_items_start && total >= prev_items_end ) {
            $( items[x] ).data('prev_position', prev_position );
            $( items[x] ).addClass('changing_position prev_position prev_position_' + prev_position );

            if ( !$( items[x] ).hasClass( 'current_position' ) ) {
              $( items[x] ).addClass('container_append');
            } else {
              $( items[x] ).clone(true).appendTo( $active_carousel_group ).addClass('delayed_container_append_dup').attr('id', $( items[x] ).attr('id') + '-dup' );
              $( items[x] ).addClass('delayed_container_append');
            }

            prev_position--;
          }

          if ( prev_position <= 0 ) {
            break;
          }

          if ( x == 0 ) {
            x = items.length;
          }
        }

        var sorted = $carousel_items.find('.container_append, .delayed_container_append_dup').sort(function (a, b) {
          var el_a_position = parseInt( $(a).data('prev_position') );
          var el_b_position = parseInt( $(b).data('prev_position') );
          return ( el_a_position < el_b_position ) ? -1 : ( el_a_position > el_b_position ) ? 1 : 0;
        });

        $( sorted ).show().appendTo( $prev_carousel_group );

        var left = 0;
        $prev_carousel_group.children().each(function(){
          $(this).css({'position':'absolute', 'left': left });
          left = left + $(this).outerWidth(true);
        });

        $active_carousel_group.animate({
          left: '100%'
        }, {
          duration: settings.slide_duration,
          complete: function() {
            $carousel_items.find('.delayed_container_append').reverse().each(function(){
              left = $( '#' + $(this).attr('id') + '-dup' ).css('left');
              $(this).css({'position':'absolute', 'left': left });
              $(this).prependTo( $prev_carousel_group );
            });

            $active_carousel_group.removeClass('active');
            $active_carousel_group.children().each(function(){
              position = $(this).data('position');
              current_position = $(this).data('current_position');
              $(this).removeClass('position_' + position + ' ' + 'changing_position current_position current_position_' + current_position );
              $(this).data('position', '');
              $(this).data('current_position', '');
              $(this).hide();
              $(this).css({'position': '', 'left': ''});
              $(this).appendTo( $carousel_items );
            });

            $active_carousel_group.remove();
          }
        } );

        prev_left = (-1) * $active_carousel_group.width() - parseInt( $the_carousel_items.first().css('marginRight').slice(0, -2) );
        $prev_carousel_group.addClass('active').css({'position':'absolute', 'top':0, left: prev_left });
        $prev_carousel_group.animate({
          left: '0%'
        }, {
          duration: settings.slide_duration,
          complete: function(){
            $prev_carousel_group.removeClass('prev').addClass('active').css({'position':'', 'width':'', 'top':'', 'left': ''});

            $prev_carousel_group.find('.delayed_container_append_dup').remove();

            $prev_carousel_group.find('.changing_position').each(function( index ){
              position = $(this).data('position');
              current_position = $(this).data('current_position');
              prev_position = $(this).data('prev_position');
              $(this).removeClass('container_append delayed_container_append position_' + position + ' ' + 'changing_position current_position current_position_' + current_position + ' prev_position prev_position_' + prev_position );
              $(this).data('current_position', '');
              $(this).data('prev_position', '');
              position = index + 1;
              $(this).data('position', position );
              $(this).addClass('position_' + position );
            });

            $prev_carousel_group.children().css({'position': '', 'left': ''});
            $tm_carousel.tm_animation_running = false;
          }
        } );
      }
    }
  }

  $.fn.tm_pb_simple_carousel = function( options ) {
    return this.each(function() {
      new $.tm_pb_simple_carousel(this, options);
    });
  }

}(jQuery));


// fin jq tm pb sim carousel 

(function($) {
  $.tm_pb_simple_slider = function(el, options) {
    var settings = $.extend( {
      slide         			: '.tm-slide',				 	// slide class
      arrows					: '.tm-pb-slider-arrows',		// arrows container class
      prev_arrow				: '.tm-pb-arrow-prev',			// left arrow class
      next_arrow				: '.tm-pb-arrow-next',			// right arrow class
      controls 				: '.tm-pb-controllers a',		// control selector
      carousel_controls 		: '.tm_pb_carousel_item',		// carousel control selector
      control_active_class	: 'tm-pb-active-control',		// active control class name
      previous_text			: tm_pb_custom.previous,			// previous arrow text
      next_text				: tm_pb_custom.next,				// next arrow text
      fade_speed				: 500,							// fade effect speed
      use_arrows				: true,							// use arrows?
      use_controls			: true,							// use controls?
      manual_arrows			: '',							// html code for custom arrows
      append_controls_to		: '',							// controls are appended to the slider element by default, here you can specify the element it should append to
      controls_below			: false,
      controls_class			: 'tm-pb-controllers',				// controls container class name
      slideshow				: false,						// automattic animation?
      slideshow_speed			: 7000,							// automattic animation speed
      show_progress_bar		: false,							// show progress bar if automattic animation is active
      tabs_animation			: false,
      use_carousel			: false
    }, options );

    var $tm_slider 			= $(el),
      $tm_slide			= $tm_slider.closest_descendent( settings.slide ),
      tm_slides_number	= $tm_slide.length,
      tm_fade_speed		= settings.fade_speed,
      tm_active_slide		= 0,
      $tm_slider_arrows,
      $tm_slider_prev,
      $tm_slider_next,
      $tm_slider_controls,
      $tm_slider_carousel_controls,
      tm_slider_timer,
      controls_html = '',
      carousel_html = '',
      $progress_bar = null,
      progress_timer_count = 0,
      $tm_pb_container = $tm_slider.find( '.tm_pb_container' ),
      tm_pb_container_width = $tm_pb_container.width(),
      is_post_slider = $tm_slider.hasClass( 'tm_pb_post_slider' );

      $tm_slider.tm_animation_running = false;

      $.data(el, "tm_pb_simple_slider", $tm_slider);

      $tm_slide.eq(0).addClass( 'tm-pb-active-slide' );

      if ( ! settings.tabs_animation ) {
        if ( !$tm_slider.hasClass('tm_pb_bg_layout_dark') && !$tm_slider.hasClass('tm_pb_bg_layout_light') ) {
          $tm_slider.addClass( tm_get_bg_layout_color( $tm_slide.eq(0) ) );
        }
      }

      if ( settings.use_arrows && tm_slides_number > 1 ) {
        if ( settings.manual_arrows == '' )
          $tm_slider.append( '<div class="tm-pb-slider-arrows"><a class="tm-pb-arrow-prev" href="#">' + '<span>' +settings.previous_text + '</span>' + '</a><a class="tm-pb-arrow-next" href="#">' + '<span>' + settings.next_text + '</span>' + '</a></div>' );
        else
          $tm_slider.append( settings.manual_arrows );

        $tm_slider_arrows 	= $tm_slider.find( settings.arrows );
        $tm_slider_prev 	= $tm_slider.find( settings.prev_arrow );
        $tm_slider_next 	= $tm_slider.find( settings.next_arrow );

        $tm_slider_next.click( function(){
          if ( $tm_slider.tm_animation_running )	return false;

          $tm_slider.tm_slider_move_to( 'next' );

          return false;
        } );

        $tm_slider_prev.click( function(){
          if ( $tm_slider.tm_animation_running )	return false;

          $tm_slider.tm_slider_move_to( 'previous' );

          return false;
        } );

        // swipe support requires et-jquery-touch-mobile
        $tm_slider.find( settings.slide ).on( 'swipeleft', function() {
          $tm_slider.tm_slider_move_to( 'next' );
        });
        $tm_slider.find( settings.slide ).on( 'swiperight', function() {
          $tm_slider.tm_slider_move_to( 'previous' );
        });
      }

      if ( settings.use_controls && tm_slides_number > 1 ) {
        for ( var i = 1; i <= tm_slides_number; i++ ) {
          controls_html += '<a href="#"' + ( i == 1 ? ' class="' + settings.control_active_class + '"' : '' ) + '>' + i + '</a>';
        }

        controls_html =
          '<div class="' + settings.controls_class + '">' +
            controls_html +
          '</div>';

        if ( settings.append_controls_to == '' )
          $tm_slider.append( controls_html );
        else
          $( settings.append_controls_to ).append( controls_html );

        if ( settings.controls_below )
          $tm_slider_controls	= $tm_slider.parent().find( settings.controls );
        else
          $tm_slider_controls	= $tm_slider.find( settings.controls );

        tm_maybe_set_controls_color( $tm_slide.eq(0) );

        $tm_slider_controls.click( function(){
          if ( $tm_slider.tm_animation_running )	return false;

          $tm_slider.tm_slider_move_to( $(this).index() );

          return false;
        } );
      }

      if ( settings.use_carousel && tm_slides_number > 1 ) {
        for ( var i = 1; i <= tm_slides_number; i++ ) {
          slide_id = i - 1;
          image_src = ( $tm_slide.eq(slide_id).data('image') !== undefined ) ? 'url(' + $tm_slide.eq(slide_id).data('image') + ')' : 'none';
          carousel_html += '<div class="tm_pb_carousel_item ' + ( i == 1 ? settings.control_active_class : '' ) + '" data-slide-id="'+ slide_id +'">' +
            '<div class="tm_pb_video_overlay" href="#" style="background-image: ' + image_src + ';">' +
              '<div class="tm_pb_video_overlay_hover"><a href="#" class="tm_pb_video_play"></a></div>' +
            '</div>' +
          '</div>';
        }

        carousel_html =
          '<div class="tm_pb_carousel">' +
          '<div class="tm_pb_carousel_items">' +
            carousel_html +
          '</div>' +
          '</div>';
        $tm_slider.after( carousel_html );

        $tm_slider_carousel_controls = $tm_slider.siblings('.tm_pb_carousel').find( settings.carousel_controls );
        $tm_slider_carousel_controls.click( function(){
          if ( $tm_slider.tm_animation_running )	return false;

          var $this = $(this);
          $tm_slider.tm_slider_move_to( $this.data('slide-id') );

          return false;
        } );
      }

      if ( settings.slideshow && tm_slides_number > 1 ) {
        $tm_slider.hover( function() {
          if ( $tm_slider.hasClass( 'tm_slider_auto_ignore_hover' ) ) {
            return;
          }

          $tm_slider.addClass( 'tm_slider_hovered' );

          if ( typeof tm_slider_timer != 'undefined' ) {
            clearInterval( tm_slider_timer );
          }
        }, function() {
          if ( $tm_slider.hasClass( 'tm_slider_auto_ignore_hover' ) ) {
            return;
          }

          $tm_slider.removeClass( 'tm_slider_hovered' );

          tm_slider_auto_rotate();
        } );
      }

      tm_slider_auto_rotate();

      function tm_slider_auto_rotate(){
        if ( settings.slideshow && tm_slides_number > 1 && ! $tm_slider.hasClass( 'tm_slider_hovered' ) ) {
          tm_slider_timer = setTimeout( function() {
            $tm_slider.tm_slider_move_to( 'next' );
          }, settings.slideshow_speed );
        }
      }

      function tm_stop_video( active_slide ) {
        var $tm_video, tm_video_src;

        // if there is a video in the slide, stop it when switching to another slide
        if ( active_slide.has( 'iframe' ).length ) {
          $tm_video = active_slide.find( 'iframe' );
          tm_video_src = $tm_video.attr( 'src' );

          $tm_video.attr( 'src', '' );
          $tm_video.attr( 'src', tm_video_src );

        } else if ( active_slide.has( 'video' ).length ) {
          if ( !active_slide.find('.tm_pb_section_video_bg').length ) {
            $tm_video = active_slide.find( 'video' );
            $tm_video[0].pause();
          }
        }
      }

      function tm_fix_slider_content_images() {
        var $this_slider           = $tm_slider,
          $slide_image_container = $this_slider.find( '.tm-pb-active-slide .tm_pb_slide_image' );
          $slide_video_container = $this_slider.find( '.tm-pb-active-slide .tm_pb_slide_video' );
          $slide                 = $slide_image_container.closest( '.tm_pb_slide' ),
          $slider                = $slide.closest( '.tm_pb_slider' ),
          slide_height           = $slider.innerHeight(),
          image_height           = parseInt( slide_height * 0.8 ),
          $top_header 		   = $('#top-header'),
          $main_header		   = $('#main-header'),
          $tm_transparent_nav    = $( '.tm_transparent_nav' ),
          $tm_vertical_nav 	   = $('.tm_vertical_nav');

        $slide_image_container.find( 'img' ).css( 'maxHeight', image_height + 'px' );

        if ( $slide.hasClass( 'tm_pb_media_alignment_center' ) ) {
          $slide_image_container.css( 'marginTop', '-' + parseInt( $slide_image_container.height() / 2 ) + 'px' );
        }

        $slide_video_container.css( 'marginTop', '-' + parseInt( $slide_video_container.height() / 2 ) + 'px' );

        $slide_image_container.find( 'img' ).addClass( 'active' );
      }

      function tm_get_bg_layout_color( $slide ) {
        if ( $slide.hasClass( 'tm_pb_bg_layout_dark' ) ) {
          return 'tm_pb_bg_layout_dark';
        }

        return 'tm_pb_bg_layout_light';
      }

      function tm_maybe_set_controls_color( $slide ) {
        var next_slide_dot_color,
          $arrows,
          arrows_color;

        if ( typeof $tm_slider_controls !== 'undefined' && $tm_slider_controls.length ) {
          next_slide_dot_color = $slide.data( 'dots_color' ) || '';

          if ( next_slide_dot_color !== '' ) {
            $tm_slider_controls.attr( 'style', 'background-color: ' + hex_to_rgba( next_slide_dot_color, '0.3' ) + ';' )
            $tm_slider_controls.filter( '.tm-pb-active-control' ).attr( 'style', 'background-color: ' + hex_to_rgba( next_slide_dot_color ) + '!important;' );
          } else {
            $tm_slider_controls.removeAttr( 'style' );
          }
        }

        if ( typeof $tm_slider_arrows !== 'undefined' && $tm_slider_arrows.length ) {
          $arrows      = $tm_slider_arrows.find( 'a' );
          arrows_color = $slide.data( 'arrows_color' ) || '';

          if ( arrows_color !== '' ) {
            $arrows.css( 'color', arrows_color );
          } else {
            $arrows.css( 'color', 'inherit' );
          }
        }
      }

      // fix the appearance of some modules inside the post slider
      function tm_fix_builder_content() {
        if ( is_post_slider ) {
          setTimeout( function() {
            var $tm_pb_circle_counter = $( '.tm_pb_circle_counter' ),
              $tm_pb_number_counter = $( '.tm_pb_number_counter' );

            window.tm_fix_testimonial_inner_width();

            if ( $tm_pb_circle_counter.length ) {
              window.tm_pb_reinit_circle_counters( $tm_pb_circle_counter );
            }

            if ( $tm_pb_number_counter.length ) {
              window.tm_pb_reinit_number_counters( $tm_pb_number_counter );
            }
            window.tm_reinint_waypoint_modules();
          }, 1000 );
        }
      }

      function hex_to_rgba( color, alpha ) {
        var color_16 = parseInt( color.replace( '#', '' ), 16 ),
          red      = ( color_16 >> 16 ) & 255,
          green    = ( color_16 >> 8 ) & 255,
          blue     = color_16 & 255,
          alpha    = alpha || 1,
          rgba;

        rgba = red + ',' + green + ',' + blue + ',' + alpha;
        rgba = 'rgba(' + rgba + ')';

        return rgba;
      }

      $tm_window.load( function() {
        tm_fix_slider_content_images();
      } );

      $tm_window.resize( function() {
        tm_fix_slider_content_images();
      } );

      $tm_slider.tm_slider_move_to = function ( direction ) {
        var $active_slide = $tm_slide.eq( tm_active_slide ),
          $next_slide;

        $tm_slider.tm_animation_running = true;

        $tm_slider.removeClass('tm_slide_transition_to_next tm_slide_transition_to_previous').addClass('tm_slide_transition_to_' + direction );

        $tm_slider.find('.tm-pb-moved-slide').removeClass('tm-pb-moved-slide');

        if ( direction == 'next' || direction == 'previous' ){

          if ( direction == 'next' )
            tm_active_slide = ( tm_active_slide + 1 ) < tm_slides_number ? tm_active_slide + 1 : 0;
          else
            tm_active_slide = ( tm_active_slide - 1 ) >= 0 ? tm_active_slide - 1 : tm_slides_number - 1;

        } else {

          if ( tm_active_slide == direction ) {
            $tm_slider.tm_animation_running = false;
            return;
          }

          tm_active_slide = direction;

        }

        if ( typeof tm_slider_timer != 'undefined' )
          clearInterval( tm_slider_timer );

        $next_slide	= $tm_slide.eq( tm_active_slide );

        $tm_slider.trigger( 'simple_slider_before_move_to', { direction : direction, next_slide : $next_slide });

        $tm_slide.each( function(){
          $(this).css( 'zIndex', 1 );
        } );
        $active_slide.css( 'zIndex', 2 ).removeClass( 'tm-pb-active-slide' ).addClass('tm-pb-moved-slide');
        $next_slide.css( { 'display' : 'block', opacity : 0 } ).addClass( 'tm-pb-active-slide' );

        tm_fix_slider_content_images();

        tm_fix_builder_content();

        if ( settings.use_controls )
          $tm_slider_controls.removeClass( settings.control_active_class ).eq( tm_active_slide ).addClass( settings.control_active_class );

        if ( settings.use_carousel )
          $tm_slider_carousel_controls.removeClass( settings.control_active_class ).eq( tm_active_slide ).addClass( settings.control_active_class );

        if ( ! settings.tabs_animation ) {
          tm_maybe_set_controls_color( $next_slide );

          $next_slide.animate( { opacity : 1 }, tm_fade_speed );
          $active_slide.addClass( 'tm_slide_transition' ).css( { 'display' : 'list-item', 'opacity' : 1 } ).animate( { opacity : 0 }, tm_fade_speed, function(){
            var active_slide_layout_bg_color = tm_get_bg_layout_color( $active_slide ),
              next_slide_layout_bg_color = tm_get_bg_layout_color( $next_slide );

            $(this).css('display', 'none').removeClass( 'tm_slide_transition' );

            tm_stop_video( $active_slide );

            $tm_slider
              .removeClass( active_slide_layout_bg_color )
              .addClass( next_slide_layout_bg_color );

            $tm_slider.tm_animation_running = false;

            $tm_slider.trigger( 'simple_slider_after_move_to', { next_slide : $next_slide } );
          } );
        } else {
          $next_slide.css( { 'display' : 'none', opacity : 0 } );

          $active_slide.addClass( 'tm_slide_transition' ).css( { 'display' : 'block', 'opacity' : 1 } ).animate( { opacity : 0 }, tm_fade_speed, function(){
            $(this).css('display', 'none').removeClass( 'tm_slide_transition' );

            $next_slide.css( { 'display' : 'block', 'opacity' : 0 } ).animate( { opacity : 1 }, tm_fade_speed, function() {
              $tm_slider.tm_animation_running = false;

              $tm_slider.trigger( 'simple_slider_after_move_to', { next_slide : $next_slide } );
            } );
          } );
        }

        tm_slider_auto_rotate();
      }
  }

  $.fn.tm_pb_simple_slider = function( options ) {
    return this.each(function() {
      new $.tm_pb_simple_slider(this, options);
    });
  }

}(jQuery));

// fin jq tm pb simple slider 

/**!
 * easyPieChart
 * Lightweight plugin to render simple, animated and retina optimized pie charts
 *
 * @license 
 * @author Robert Fleischmann <rendro87@gmail.com> (http://robert-fleischmann.de)
 * @version 2.1.5
 **/

(function(root, factory) {
    if(typeof exports === 'object') {
        module.exports = factory(require('jquery'));
    }
    else if(typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    }
    else {
        factory(root.jQuery);
    }
}(this, function($) {

/**
 * Renderer to render the chart on a canvas object
 * @param {DOMElement} el      DOM element to host the canvas (root of the plugin)
 * @param {object}     options options object of the plugin
 */
var CanvasRenderer = function(el, options) {
	var cachedBackground;
	var canvas = document.createElement('canvas');

	el.appendChild(canvas);

	if (typeof(G_vmlCanvasManager) !== 'undefined') {
		G_vmlCanvasManager.initElement(canvas);
	}

	var ctx = canvas.getContext('2d');

	canvas.width = canvas.height = options.size;

	// canvas on retina devices
	var scaleBy = 1;
	if (window.devicePixelRatio > 1) {
		scaleBy = window.devicePixelRatio;
		canvas.style.width = canvas.style.height = [options.size, 'px'].join('');
		canvas.width = canvas.height = options.size * scaleBy;
		ctx.scale(scaleBy, scaleBy);
	}

	// move 0,0 coordinates to the center
	ctx.translate(options.size / 2, options.size / 2);

	// rotate canvas -90deg
	ctx.rotate((-1 / 2 + options.rotate / 180) * Math.PI);

	var radius = (options.size - options.lineWidth) / 2;
	if (options.scaleColor && options.scaleLength) {
		radius -= options.scaleLength + 2; // 2 is the distance between scale and bar
	}

	// IE polyfill for Date
	Date.now = Date.now || function() {
		return +(new Date());
	};

	/**
	 * Draw a circle around the center of the canvas
	 * @param {strong} color     Valid CSS color string
	 * @param {number} lineWidth Width of the line in px
	 * @param {number} percent   Percentage to draw (float between -1 and 1)
	 */
	var drawCircle = function(color, lineWidth, percent, alpha ) {
		percent = Math.min(Math.max(-1, percent || 0), 1);
		var isNegative = percent <= 0 ? true : false;

		ctx.beginPath();
		ctx.arc(0, 0, radius, 0, Math.PI * 2 * percent, isNegative);

		ctx.strokeStyle = color;
		ctx.globalAlpha = alpha;
		ctx.lineWidth = lineWidth;

		ctx.stroke();
	};

	/**
	 * Draw the scale of the chart
	 */
	var drawScale = function() {
		var offset;
		var length;

		ctx.lineWidth = 1;
		ctx.fillStyle = options.scaleColor;

		ctx.save();
		for (var i = 24; i > 0; --i) {
			if (i % 6 === 0) {
				length = options.scaleLength;
				offset = 0;
			} else {
				length = options.scaleLength * 0.6;
				offset = options.scaleLength - length;
			}
			ctx.fillRect(-options.size/2 + offset, 0, length, 1);
			ctx.rotate(Math.PI / 12);
		}
		ctx.restore();
	};

	/**
	 * Request animation frame wrapper with polyfill
	 * @return {function} Request animation frame method or timeout fallback
	 */
	var reqAnimationFrame = (function() {
		return  window.requestAnimationFrame ||
				window.webkitRequestAnimationFrame ||
				window.mozRequestAnimationFrame ||
				function(callback) {
					window.setTimeout(callback, 1000 / 60);
				};
	}());

	/**
	 * Draw the background of the plugin including the scale and the track
	 */
	var drawBackground = function() {
		if(options.scaleColor) drawScale();
		if(options.trackColor) drawCircle(options.trackColor, options.lineWidth, 1, options.trackAlpha );
	};

  /**
    * Canvas accessor
   */
  this.getCanvas = function() {
    return canvas;
  };
  
  /**
    * Canvas 2D context 'ctx' accessor
   */
  this.getCtx = function() {
    return ctx;
  };

	/**
	 * Clear the complete canvas
	 */
	this.clear = function() {
		ctx.clearRect(options.size / -2, options.size / -2, options.size, options.size);
	};

	/**
	 * Draw the complete chart
	 * @param {number} percent Percent shown by the chart between -100 and 100
	 */
	this.draw = function(percent) {
		// do we need to render a background
		if (!!options.scaleColor || !!options.trackColor) {
			// getImageData and putImageData are supported
			if (ctx.getImageData && ctx.putImageData) {
				if (!cachedBackground) {
					drawBackground();
					cachedBackground = ctx.getImageData(0, 0, options.size * scaleBy, options.size * scaleBy);
				} else {
					ctx.putImageData(cachedBackground, 0, 0);
				}
			} else {
				this.clear();
				drawBackground();
			}
		} else {
			this.clear();
		}

		ctx.lineCap = options.lineCap;

		// if barcolor is a function execute it and pass the percent as a value
		var color;
		if (typeof(options.barColor) === 'function') {
			color = options.barColor(percent);
		} else {
			color = options.barColor;
		}

		// draw bar
		drawCircle(color, options.lineWidth, percent / 100, options.barAlpha );
	}.bind(this);

	/**
	 * Animate from some percent to some other percentage
	 * @param {number} from Starting percentage
	 * @param {number} to   Final percentage
	 */
	this.animate = function(from, to) {
		var startTime = Date.now();
		options.onStart(from, to);
		var animation = function() {
			var process = Math.min(Date.now() - startTime, options.animate.duration);
			var currentValue = options.easing(this, process, from, to - from, options.animate.duration);
			this.draw(currentValue);
			options.onStep(from, to, currentValue);
			if (process >= options.animate.duration) {
				options.onStop(from, to);
			} else {
				reqAnimationFrame(animation);
			}
		}.bind(this);

		reqAnimationFrame(animation);
	}.bind(this);
};

var EasyPieChart = function(el, opts) {
	var defaultOptions = {
		barColor: '#ef1e25',
		barAlpha: 1.0,
		trackColor: '#f9f9f9',
		trackAlpha: 1.0,
		scaleColor: '#dfe0e0',
		scaleLength: 5,
		lineCap: 'round',
		lineWidth: 3,
		size: 110,
		rotate: 0,
		render: true,
		animate: {
			duration: 1000,
			enabled: true
		},
		easing: function (x, t, b, c, d) { // more can be found here: http://gsgd.co.uk/sandbox/jquery/easing/
			t = t / (d/2);
			if (t < 1) {
				return c / 2 * t * t + b;
			}
			return -c/2 * ((--t)*(t-2) - 1) + b;
		},
		onStart: function(from, to) {
			return;
		},
		onStep: function(from, to, currentValue) {
			return;
		},
		onStop: function(from, to) {
			return;
		}
	};

	// detect present renderer
	if (typeof(CanvasRenderer) !== 'undefined') {
		defaultOptions.renderer = CanvasRenderer;
	} else if (typeof(SVGRenderer) !== 'undefined') {
		defaultOptions.renderer = SVGRenderer;
	} else {
		throw new Error('Please load either the SVG- or the CanvasRenderer');
	}

	var options = {};
	var currentValue = 0;

	/**
	 * Initialize the plugin by creating the options object and initialize rendering
	 */
	var init = function() {
		this.el = el;
		this.options = options;

		// merge user options into default options
		for (var i in defaultOptions) {
			if (defaultOptions.hasOwnProperty(i)) {
				options[i] = opts && typeof(opts[i]) !== 'undefined' ? opts[i] : defaultOptions[i];
				if (typeof(options[i]) === 'function') {
					options[i] = options[i].bind(this);
				}
			}
		}

		// check for jQuery easing
		if (typeof(options.easing) === 'string' && typeof(jQuery) !== 'undefined' && jQuery.isFunction(jQuery.easing[options.easing])) {
			options.easing = jQuery.easing[options.easing];
		} else {
			options.easing = defaultOptions.easing;
		}

		// process earlier animate option to avoid bc breaks
		if (typeof(options.animate) === 'number') {
			options.animate = {
				duration: options.animate,
				enabled: true
			};
		}

		if (typeof(options.animate) === 'boolean' && !options.animate) {
			options.animate = {
				duration: 1000,
				enabled: options.animate
			};
		}

		// create renderer
		this.renderer = new options.renderer(el, options);

		// initial draw
		this.renderer.draw(currentValue);

		// initial update
		if (el.dataset && el.dataset.percent) {
			this.update(parseFloat(el.dataset.percent));
		} else if (el.getAttribute && el.getAttribute('data-percent')) {
			this.update(parseFloat(el.getAttribute('data-percent')));
		}
	}.bind(this);

	/**
	 * Update the value of the chart
	 * @param  {number} newValue Number between 0 and 100
	 * @return {object}          Instance of the plugin for method chaining
	 */
	this.update = function(newValue) {
		newValue = parseFloat(newValue);
		if (options.animate.enabled) {
			this.renderer.animate(currentValue, newValue);
		} else {
			this.renderer.draw(newValue);
		}
		currentValue = newValue;
		return this;
	}.bind(this);

	/**
	 * Disable animation
	 * @return {object} Instance of the plugin for method chaining
	 */
	this.disableAnimation = function() {
		options.animate.enabled = false;
		return this;
	};

	/**
	 * Enable animation
	 * @return {object} Instance of the plugin for method chaining
	 */
	this.enableAnimation = function() {
		options.animate.enabled = true;
		return this;
	};

	init();
};

$.fn.easyPieChart = function(options) {
	return this.each(function() {
		var instanceOptions;

		if (!$.data(this, 'easyPieChart')) {
			instanceOptions = $.extend({}, options, $(this).data());
			$.data(this, 'easyPieChart', new EasyPieChart(this, instanceOptions));
		}
	});
};


}));

// fin jq easy pie chart 

var tm_hash_module_seperator = '||',
  tm_hash_module_param_seperator = '|';

function process_tm_hashchange( hash ) {
  if ( ( hash.indexOf( tm_hash_module_seperator, 0 ) ) !== -1 ) {
    modules = hash.split( tm_hash_module_seperator );
    for ( var i = 0; i < modules.length; i++ ) {
      var module_params = modules[i].split( tm_hash_module_param_seperator );
      var element = module_params[0];
      module_params.shift();
      if ( $('#' + element ).length ) {
        $('#' + element ).trigger({
          type: "tm_hashchange",
          params: module_params
        });
      }
    }
  } else {
    module_params = hash.split( tm_hash_module_param_seperator );
    var element = module_params[0];
    module_params.shift();
    if ( $('#' + element ).length ) {
      $('#' + element ).trigger({
        type: "tm_hashchange",
        params: module_params
      });
    }
  }
}

function tm_set_hash( module_state_hash ) {
  module_id = module_state_hash.split( tm_hash_module_param_seperator )[0];
  if ( !$('#' + module_id ).length ) {
    return;
  }

  if ( window.location.hash ) {
    var hash = window.location.hash.substring(1), //Puts hash in variable, and removes the # character
      new_hash = [];

    if ( ( hash.indexOf( tm_hash_module_seperator, 0 ) ) !== -1 ) {
      modules = hash.split( tm_hash_module_seperator );
      var in_hash = false;
      for ( var i = 0; i < modules.length; i++ ) {
        var element = modules[i].split( tm_hash_module_param_seperator )[0];
        if ( element === module_id ) {
          new_hash.push( module_state_hash );
          in_hash = true;
        } else {
          new_hash.push( modules[i] );
        }
      }
      if ( !in_hash ) {
        new_hash.push( module_state_hash );
      }
    } else {
      module_params = hash.split( tm_hash_module_param_seperator );
      var element = module_params[0];
      if ( element !== module_id ) {
        new_hash.push( hash );
      }
      new_hash.push( module_state_hash );
    }

    hash = new_hash.join( tm_hash_module_seperator );
  } else {
    hash = module_state_hash;
  }

  var yScroll = document.body.scrollTop;
  window.location.hash = hash;
  document.body.scrollTop = yScroll;
}


// fin tm-hash






var $tm_pb_slider  = jQuery( '.tm_pb_slider' ),
	$tm_pb_tabs    = jQuery( '.tm_pb_tabs' ),
	$tm_pb_tabs_li = $tm_pb_tabs.find( '.tm_pb_tabs_controls li' ),
	$tm_pb_video_section = jQuery('.tm_pb_section_video_bg'),
	$tm_pb_newsletter_button = jQuery( '.tm_pb_newsletter_button' ),
	$tm_pb_filterable_portfolio = jQuery( '.tm_pb_filterable_portfolio' ),
	$tm_pb_fullwidth_portfolio = jQuery( '.tm_pb_fullwidth_portfolio' ),
	$tm_pb_gallery = jQuery( '.tm_pb_gallery' ),
	$tm_pb_countdown_timer = jQuery( '.tm_pb_countdown_timer' ),
	$tm_post_gallery = jQuery( '.tm_post_gallery' ),
	$tm_lightbox_image = jQuery( '.tm_pb_lightbox_image'),
	$tm_pb_map    = jQuery( '.tm_pb_map_container' ),
	$tm_pb_circle_counter = jQuery( '.tm_pb_circle_counter_bar' ),
	$tm_pb_number_counter = jQuery( '.tm_pb_number_counter' ),
	$tm_pb_parallax = jQuery( '.tm_parallax_bg' ),
	$tm_pb_shop = jQuery( '.tm_pb_shop' ),
	$tm_pb_post_fullwidth = jQuery( '.single.tm_pb_pagebuilder_layout.tm_full_width_page' ),
	tm_is_mobile_device = navigator.userAgent.match( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/ ),
	tm_is_ipad = navigator.userAgent.match( /iPad/ ),
	$tm_container = ! tm_pb_custom.is_builder_plugin_used ? jQuery( '.container' ) : jQuery( '.tm_pb_row' ),
	tm_container_width = $tm_container.width(),
	tm_is_fixed_nav = jQuery( 'body' ).hasClass( 'tm_fixed_nav' ),
	tm_is_vertical_fixed_nav = jQuery( 'body' ).hasClass( 'tm_vertical_fixed' ),
	tm_is_rtl = jQuery( 'body' ).hasClass( 'rtl' ),
	tm_hide_nav = jQuery( 'body' ).hasClass( 'tm_hide_nav' ),
	tm_header_style_left = jQuery( 'body' ).hasClass( 'tm_header_style_left' ),
	tm_vertical_navigation = jQuery( 'body' ).hasClass( 'tm_vertical_nav' ),
	$top_header = jQuery('#top-header'),
	$main_header = jQuery('#main-header'),
	$main_container_wrapper = jQuery( '#page-container' ),
	$tm_transparent_nav = jQuery( '.tm_transparent_nav' ),
	$tm_pb_first_row = jQuery( 'body.tm_pb_pagebuilder_layout .tm_pb_section:first-child' ),
	$tm_main_content_first_row = jQuery( '#main-content .container:first-child' ),
	$tm_main_content_first_row_meta_wrapper = $tm_main_content_first_row.find('.tm_post_meta_wrapper:first'),
	$tm_main_content_first_row_meta_wrapper_title = $tm_main_content_first_row_meta_wrapper.find( 'h1' ),
	$tm_main_content_first_row_content = $tm_main_content_first_row.find('.entry-content:first'),
	$tm_single_post = jQuery( 'body.single-post' ),
	$tm_window = jQuery(window),
	etRecalculateOffset = false,
	tm_header_height,
	tm_header_modifier,
	tm_header_offset,
	tm_primary_header_top,
	$tm_vertical_nav = jQuery('.tm_vertical_nav'),
	$tm_header_style_split = jQuery('.tm_header_style_split'),
	$tm_top_navigation = jQuery('#tm-top-navigation'),
	$logo = jQuery('#logo'),
	$tm_sticky_image = jQuery('.tm_pb_image_sticky'),
	$tm_pb_counter_amount = jQuery('.tm_pb_counter_amount'),
	$tm_pb_carousel = jQuery( '.tm_pb_carousel' ),
	$tm_menu_selector = tm_pb_custom.is_divi_theme_used ? jQuery( 'ul.nav' ) : jQuery( '.tm_pb_fullwidth_menu ul.nav' );

jQuery(document).ready( function($){
	var $tm_top_menu = $tm_menu_selector,
		tm_parent_menu_longpress_limit = 300,
		tm_parent_menu_longpress_start,
		tm_parent_menu_click = true;

	$( '.tm_pb_posts' ).each( function() {

		var $item  = $( this ),
			loader = '<div class="tm-pb-spinner tm-pb-spinner-double-bounce"><div class="tm-pb-double-bounce1"></div><div class="tm-pb-double-bounce2"></div></div>';

		$item.on( 'click', '.tm_pb_ajax_more', function( event ) {

			var $this   = $( this ),
				$result = $item.find( '.tm-posts_listing .row' ),
				pages   = $item.data( 'pages' ),
				data    = new Object();

			event.preventDefault();

			if ( $this.hasClass( 'in-progress' ) ) {
				return;
			}

			data.page   = $item.data( 'page' );
			data.atts   = $item.data( 'atts' );
			data.action = 'tm_pb_load_more';

			$this.addClass( 'in-progress' ).after( loader );

			$.ajax({
				url: window.tm_pb_custom.ajaxurl,
				type: 'post',
				dataType: 'json',
				data: data,
				error: function() {
					$this.removeClass( 'in-progress' ).next( '.tm-pb-spinner' ).remove();
				}
			}).done( function( response ) {
				$result.append( response.data.result );
				$item.data( 'page', response.data.page );
				$this.removeClass( 'in-progress' ).next( '.tm-pb-spinner' ).remove();
				if ( response.data.page == pages ) {
					$this.addClass( 'btn-hidden' );
				}

			});

		});

	});

	if ( $( '.tm_pb_row' ).length ) {
		$( '.tm_pb_row' ).each( function() {
			var $this_row = $( this ),
				row_class = '';

			row_class = tm_get_column_types( $this_row.find( '>.tm_pb_column' ) );

			if ( '' !== row_class && ( -1 !== row_class.indexOf( '1-4' ) || '_4col' === row_class ) ) {
				$this_row.addClass( 'tm_pb_row' + row_class );
			}

			if ( $this_row.find( '.tm_pb_row_inner' ).length ) {
				$this_row.find( '.tm_pb_row_inner' ).each( function() {
					var $this_row_inner = $( this );
					row_class = tm_get_column_types( $this_row_inner.find( '.tm_pb_column' ) );

					if ( '' !== row_class && -1 !== row_class.indexOf( '1-4' ) ) {
						$this_row_inner.addClass( 'tm_pb_row' + row_class );
					}
				});
			}
		});
	}

	function tm_get_column_types( $columns ) {
		var row_class = '';

		if ( $columns.length ) {
			$columns.each( function() {
				var $this_column = $( this ),
					column_type = $this_column.attr( 'class' ).split( 'tm_pb_column_' )[1],
					column_type_clean = column_type.split( ' ', 1 )[0],
					column_type_updated = column_type_clean.replace( '_', '-' ).trim();

				row_class += '_' + column_type_updated;
			});

			row_class = '_1-4_1-4_1-4_1-4' === row_class ? '_4col' : row_class;
		}

		return row_class;
	}

	$tm_top_menu.find( 'li' ).hover( function() {
		if ( ! $(this).closest( 'li.mega-menu' ).length || $(this).hasClass( 'mega-menu' ) ) {
			$(this).addClass( 'tm-show-dropdown' );
			$(this).removeClass( 'tm-hover' ).addClass( 'tm-hover' );
		}
	}, function() {
		var $this_el = $(this);

		$this_el.removeClass( 'tm-show-dropdown' );

		setTimeout( function() {
			if ( ! $this_el.hasClass( 'tm-show-dropdown' ) ) {
				$this_el.removeClass( 'tm-hover' );
			}
		}, 200 );
	} );

	// Dropdown menu adjustment for touch screen
	$tm_top_menu.find('.menu-item-has-children > a').on( 'touchstart', function(){
		tm_parent_menu_longpress_start = new Date().getTime();
	} ).on( 'touchend', function(){
		var tm_parent_menu_longpress_end = new Date().getTime()
		if ( tm_parent_menu_longpress_end  >= tm_parent_menu_longpress_start + tm_parent_menu_longpress_limit ) {
			tm_parent_menu_click = true;
		} else {
			tm_parent_menu_click = false;

			// Close sub-menu if toggled
			var $tm_parent_menu = $(this).parent('li');
			if ( $tm_parent_menu.hasClass( 'tm-hover') ) {
				$tm_parent_menu.trigger( 'mouseleave' );
			} else {
				$tm_parent_menu.trigger( 'mouseenter' );
			}
		}
		tm_parent_menu_longpress_start = 0;
	} ).click(function() {
		if ( tm_parent_menu_click ) {
			return true;
		}

		return false;
	} );

	$tm_top_menu.find( 'li.mega-menu' ).each(function(){
		var $li_mega_menu           = $(this),
			$li_mega_menu_item      = $li_mega_menu.children( 'ul' ).children( 'li' ),
			li_mega_menu_item_count = $li_mega_menu_item.length;

		if ( li_mega_menu_item_count < 4 ) {
			$li_mega_menu.addClass( 'mega-menu-parent mega-menu-parent-' + li_mega_menu_item_count );
		}
	});

	$tm_sticky_image.each( function() {
		var $this_el            = $(this),
			$row                = $this_el.closest('.tm_pb_row'),
			$section            = $row.closest('.tm_pb_section'),
			$column             = $this_el.closest( '.tm_pb_column' ),
			sticky_class        = 'tm_pb_section_sticky',
			sticky_mobile_class = 'tm_pb_section_sticky_mobile';

		// If it is not in the last row, continue
		if ( ! $row.is( ':last-child' ) ) {
			return true;
		}

		// Make sure sticky image is the last element in the column
		if ( ! $this_el.is( ':last-child' ) ) {
			return true;
		}

		// If it is in the last row, find the parent section and attach new class to it
		if ( ! $section.hasClass( sticky_class ) ) {
			$section.addClass( sticky_class );
		}

		$column.addClass( 'tm_pb_row_sticky' );

		if ( ! $section.hasClass( sticky_mobile_class ) && $column.is( ':last-child' ) ) {
			$section.addClass( sticky_mobile_class );
		}
	} );

	if ( tm_is_mobile_device ) {
		$( '.tm_pb_section_video_bg' ).each( function() {
			var $this_el = $(this);

			$this_el.css( 'visibility', 'hidden' ).closest( '.tm_pb_preload' ).removeClass( 'tm_pb_preload' )
		} );

		$( 'body' ).addClass( 'tm_mobile_device' );

		if ( ! tm_is_ipad ) {
			$( 'body' ).addClass( 'tm_mobile_device_not_ipad' );
		}
	}

	if ( $tm_pb_video_section.length ) {
		$tm_pb_video_section.find( 'video' ).mediaelementplayer( {
			pauseOtherPlayers: false,
			success : function( mediaElement, domObject ) {
				mediaElement.addEventListener( 'loadeddata', function() {
					tm_pb_resize_section_video_bg( $(domObject) );
					tm_pb_center_video( $(domObject) );
				}, false );

				mediaElement.addEventListener( 'canplay', function() {
					$(domObject).closest( '.tm_pb_preload' ).removeClass( 'tm_pb_preload' );
				}, false );
			}
		} );
	}

	if ( $tm_post_gallery.length ) {
		// swipe support in magnific popup only if gallery exists
		var magnificPopup = $.magnificPopup.instance;

		$( 'body' ).on( 'swiperight', '.mfp-container', function() {
			magnificPopup.prev();
		} );
		$( 'body' ).on( 'swipeleft', '.mfp-container', function() {
			magnificPopup.next();
		} );

		$tm_post_gallery.each(function() {
			$(this).magnificPopup( {
				delegate: 'a',
				type: 'image',
				removalDelay: 500,
				gallery: {
					enabled: true,
					navigateByImgClick: true
				},
				mainClass: 'mfp-fade',
				zoom: {
					enabled: true,
					duration: 500,
					opener: function(element) {
						return element.find('img');
					}
				}
			} );
		} );
		// prevent attaching of any further actions on click
		$tm_post_gallery.find( 'a' ).unbind( 'click' );
	}

	if ( $tm_lightbox_image.length ) {
		// prevent attaching of any further actions on click
		$tm_lightbox_image.unbind( 'click' );
		$tm_lightbox_image.bind( 'click' );

		$tm_lightbox_image.magnificPopup( {
			type: 'image',
			removalDelay: 500,
			mainClass: 'mfp-fade',
			zoom: {
				enabled: true,
				duration: 500,
				opener: function(element) {
					return element.find('img');
				}
			}
		} );
	}

	if ( $tm_pb_slider.length ) {
		$tm_pb_slider.each( function() {
			var $this_slider = $(this),
				tm_slider_settings = {
					fade_speed 		: 700,
					slide			: ! $this_slider.hasClass( 'tm_pb_gallery' ) ? '.tm_pb_slide' : '.tm_pb_gallery_item'
				}

			if ( $this_slider.hasClass('tm_pb_slider_no_arrows') )
				tm_slider_settings.use_arrows = false;

			if ( $this_slider.hasClass('tm_pb_slider_no_pagination') )
				tm_slider_settings.use_controls = false;

			if ( $this_slider.hasClass('tm_slider_auto') ) {
				var tm_slider_autospeed_class_value = /tm_slider_speed_(\d+)/g;

				tm_slider_settings.slideshow = true;

				tm_slider_autospeed = tm_slider_autospeed_class_value.exec( $this_slider.attr('class') );

				tm_slider_settings.slideshow_speed = tm_slider_autospeed[1];
			}

			if ( $this_slider.parent().hasClass('tm_pb_video_slider') ) {
				tm_slider_settings.controls_below = true;
				tm_slider_settings.append_controls_to = $this_slider.parent();

				setTimeout( function() {
					$( '.tm_pb_preload' ).removeClass( 'tm_pb_preload' );
				}, 500 );
			}

			if ( $this_slider.hasClass('tm_pb_slider_carousel') )
				tm_slider_settings.use_carousel = true;

			$this_slider.tm_pb_simple_slider( tm_slider_settings );

		} );
	}

	$tm_pb_carousel  = $( '.tm_pb_carousel' );
	if ( $tm_pb_carousel.length ) {
		$tm_pb_carousel.each( function() {
			var $this_carousel = $(this),
				tm_carousel_settings = {
					fade_speed 		: 1000
				};

			$this_carousel.tm_pb_simple_carousel( tm_carousel_settings );
		} );
	}

	if ( $tm_pb_fullwidth_portfolio.length ) {

		function set_fullwidth_portfolio_columns( $the_portfolio, carousel_mode ) {
			var columns,
				$portfolio_items = $the_portfolio.find('.tm_pb_portfolio_items'),
				portfolio_items_width = $portfolio_items.width(),
				$the_portfolio_items = $portfolio_items.find('.tm_pb_portfolio_item'),
				portfolio_item_count = $the_portfolio_items.length;

			// calculate column breakpoints
			if ( portfolio_items_width >= 1600 ) {
				columns = 5;
			} else if ( portfolio_items_width >= 1024 ) {
				columns = 4;
			} else if ( portfolio_items_width >= 768 ) {
				columns = 3;
			} else if ( portfolio_items_width >= 480 ) {
				columns = 2;
			} else {
				columns = 1;
			}

			// set height of items
			portfolio_item_width = portfolio_items_width / columns;
			portfolio_item_height = portfolio_item_width * .75;

			if ( carousel_mode ) {
				$portfolio_items.css({ 'height' : portfolio_item_height });
			}

			$the_portfolio_items.css({ 'height' : portfolio_item_height });

			if ( columns === $portfolio_items.data('portfolio-columns') ) {
				return;
			}

			if ( $the_portfolio.data('columns_setting_up') ) {
				return;
			}

			$the_portfolio.data('columns_setting_up', true );

			var portfolio_item_width_percentage = ( 100 / columns ) + '%';
			$the_portfolio_items.css({ 'width' : portfolio_item_width_percentage });

			// store last setup column
			$portfolio_items.removeClass('columns-' + $portfolio_items.data('portfolio-columns') );
			$portfolio_items.addClass('columns-' + columns );
			$portfolio_items.data('portfolio-columns', columns );

			if ( !carousel_mode ) {
				return $the_portfolio.data('columns_setting_up', false );
			}

			// kill all previous groups to get ready to re-group
			if ( $portfolio_items.find('.tm_pb_carousel_group').length ) {
				$the_portfolio_items.appendTo( $portfolio_items );
				$portfolio_items.find('.tm_pb_carousel_group').remove();
			}

			// setup the grouping
			var the_portfolio_items = $portfolio_items.data('items' ),
				$carousel_group = $('<div class="tm_pb_carousel_group active">').appendTo( $portfolio_items );

			$the_portfolio_items.data('position', '');
			if ( the_portfolio_items.length <= columns ) {
				$portfolio_items.find('.tm-pb-slider-arrows').hide();
			} else {
				$portfolio_items.find('.tm-pb-slider-arrows').show();
			}

			for ( position = 1, x=0 ;x < the_portfolio_items.length; x++, position++ ) {
				if ( x < columns ) {
					$( the_portfolio_items[x] ).show();
					$( the_portfolio_items[x] ).appendTo( $carousel_group );
					$( the_portfolio_items[x] ).data('position', position );
					$( the_portfolio_items[x] ).addClass('position_' + position );
				} else {
					position = $( the_portfolio_items[x] ).data('position');
					$( the_portfolio_items[x] ).removeClass('position_' + position );
					$( the_portfolio_items[x] ).data('position', '' );
					$( the_portfolio_items[x] ).hide();
				}
			}

			$the_portfolio.data('columns_setting_up', false );

		}

		function tm_carousel_auto_rotate( $carousel ) {
			if ( 'on' === $carousel.data('auto-rotate') && $carousel.find('.tm_pb_portfolio_item').length > $carousel.find('.tm_pb_carousel_group .tm_pb_portfolio_item').length && ! $carousel.hasClass( 'tm_carousel_hovered' ) ) {

				tm_carousel_timer = setTimeout( function() {
					$carousel.find('.tm-pb-arrow-next').click();
				}, $carousel.data('auto-rotate-speed') );

				$carousel.data('tm_carousel_timer', tm_carousel_timer);
			}
		}

		$tm_pb_fullwidth_portfolio.each(function(){
			var $the_portfolio = $(this),
				$portfolio_items = $the_portfolio.find('.tm_pb_portfolio_items');

				$portfolio_items.data('items', $portfolio_items.find('.tm_pb_portfolio_item').toArray() );
				$the_portfolio.data('columns_setting_up', false );

			if ( $the_portfolio.hasClass('tm_pb_fullwidth_portfolio_carousel') ) {
				// add left and right arrows
				$portfolio_items.prepend('<div class="tm-pb-slider-arrows"><a class="tm-pb-arrow-prev" href="#">' + '<span>' + tm_pb_custom.previous + '</span>' + '</a><a class="tm-pb-arrow-next" href="#">' + '<span>' + tm_pb_custom.next + '</span>' + '</a></div>');

				set_fullwidth_portfolio_columns( $the_portfolio, true );

				tm_carousel_auto_rotate( $the_portfolio );

				// swipe support
				$the_portfolio.on( 'swiperight', function() {
					$( this ).find( '.tm-pb-arrow-prev' ).click();
				});
				$the_portfolio.on( 'swipeleft', function() {
					$( this ).find( '.tm-pb-arrow-next' ).click();
				});

				$the_portfolio.hover(
					function(){
						$(this).addClass('tm_carousel_hovered');
						if ( typeof $(this).data('tm_carousel_timer') != 'undefined' ) {
							clearInterval( $(this).data('tm_carousel_timer') );
						}
					},
					function(){
						$(this).removeClass('tm_carousel_hovered');
						tm_carousel_auto_rotate( $(this) );
					}
				);

				$the_portfolio.data('carouseling', false );

				$the_portfolio.on('click', '.tm-pb-slider-arrows a', function(e){
					var $the_portfolio = $(this).parents('.tm_pb_fullwidth_portfolio'),
						$portfolio_items = $the_portfolio.find('.tm_pb_portfolio_items'),
						$the_portfolio_items = $portfolio_items.find('.tm_pb_portfolio_item'),
						$active_carousel_group = $portfolio_items.find('.tm_pb_carousel_group.active'),
						slide_duration = 700,
						items = $portfolio_items.data('items'),
						columns = $portfolio_items.data('portfolio-columns'),
						item_width = $active_carousel_group.innerWidth() / columns, //$active_carousel_group.children().first().innerWidth(),
						original_item_width = ( 100 / columns ) + '%';

					e.preventDefault();

					if ( $the_portfolio.data('carouseling') ) {
						return;
					}

					$the_portfolio.data('carouseling', true);

					$active_carousel_group.children().each(function(){
						$(this).css({'width': $(this).innerWidth() + 1, 'position':'absolute', 'left': ( $(this).innerWidth() * ( $(this).data('position') - 1 ) ) });
					});

					if ( $(this).hasClass('tm-pb-arrow-next') ) {
						var $next_carousel_group,
							current_position = 1,
							next_position = 1,
							active_items_start = items.indexOf( $active_carousel_group.children().first()[0] ),
							active_items_end = active_items_start + columns,
							next_items_start = active_items_end,
							next_items_end = next_items_start + columns;

						$next_carousel_group = $('<div class="tm_pb_carousel_group next" style="display: none;left: 100%;position: absolute;top: 0;">').insertAfter( $active_carousel_group );
						$next_carousel_group.css({ 'width': $active_carousel_group.innerWidth() }).show();

						// this is an endless loop, so it can decide internally when to break out, so that next_position
						// can get filled up, even to the extent of an element having both and current_ and next_ position
						for( x = 0, total = 0 ; ; x++, total++ ) {
							if ( total >= active_items_start && total < active_items_end ) {
								$( items[x] ).addClass( 'changing_position current_position current_position_' + current_position );
								$( items[x] ).data('current_position', current_position );
								current_position++;
							}

							if ( total >= next_items_start && total < next_items_end ) {
								$( items[x] ).data('next_position', next_position );
								$( items[x] ).addClass('changing_position next_position next_position_' + next_position );

								if ( !$( items[x] ).hasClass( 'current_position' ) ) {
									$( items[x] ).addClass('container_append');
								} else {
									$( items[x] ).clone(true).appendTo( $active_carousel_group ).hide().addClass('delayed_container_append_dup').attr('id', $( items[x] ).attr('id') + '-dup' );
									$( items[x] ).addClass('delayed_container_append');
								}

								next_position++;
							}

							if ( next_position > columns ) {
								break;
							}

							if ( x >= ( items.length -1 )) {
								x = -1;
							}
						}

						sorted = $portfolio_items.find('.container_append, .delayed_container_append_dup').sort(function (a, b) {
							var el_a_position = parseInt( $(a).data('next_position') );
							var el_b_position = parseInt( $(b).data('next_position') );
							return ( el_a_position < el_b_position ) ? -1 : ( el_a_position > el_b_position ) ? 1 : 0;
						});

						$( sorted ).show().appendTo( $next_carousel_group );

						$next_carousel_group.children().each(function(){
							$(this).css({'width': item_width, 'position':'absolute', 'left': ( item_width * ( $(this).data('next_position') - 1 ) ) });
						});

						$active_carousel_group.animate({
							left: '-100%'
						}, {
							duration: slide_duration,
							complete: function() {
								$portfolio_items.find('.delayed_container_append').each(function(){
									$(this).css({'width': item_width, 'position':'absolute', 'left': ( item_width * ( $(this).data('next_position') - 1 ) ) });
									$(this).appendTo( $next_carousel_group );
								});

								$active_carousel_group.removeClass('active');
								$active_carousel_group.children().each(function(){
									position = $(this).data('position');
									current_position = $(this).data('current_position');
									$(this).removeClass('position_' + position + ' ' + 'changing_position current_position current_position_' + current_position );
									$(this).data('position', '');
									$(this).data('current_position', '');
									$(this).hide();
									$(this).css({'position': '', 'width': '', 'left': ''});
									$(this).appendTo( $portfolio_items );
								});

								$active_carousel_group.remove();

								tm_carousel_auto_rotate( $the_portfolio );

							}
						} );

						$next_carousel_group.addClass('active').css({'position':'absolute', 'top':0, left: '100%'});
						$next_carousel_group.animate({
							left: '0%'
						}, {
							duration: slide_duration,
							complete: function(){
								setTimeout(function(){
									$next_carousel_group.removeClass('next').addClass('active').css({'position':'', 'width':'', 'top':'', 'left': ''});

									$next_carousel_group.find('.delayed_container_append_dup').remove();

									$next_carousel_group.find('.changing_position').each(function( index ){
										position = $(this).data('position');
										current_position = $(this).data('current_position');
										next_position = $(this).data('next_position');
										$(this).removeClass('container_append delayed_container_append position_' + position + ' ' + 'changing_position current_position current_position_' + current_position + ' next_position next_position_' + next_position );
										$(this).data('current_position', '');
										$(this).data('next_position', '');
										$(this).data('position', ( index + 1 ) );
									});

									$next_carousel_group.children().css({'position': '', 'width': original_item_width, 'left': ''});

									$the_portfolio.data('carouseling', false);
								}, 100 );
							}
						} );

					} else {
						var $prev_carousel_group,
							current_position = columns,
							prev_position = columns,
							columns_span = columns - 1,
							active_items_start = items.indexOf( $active_carousel_group.children().last()[0] ),
							active_items_end = active_items_start - columns_span,
							prev_items_start = active_items_end - 1,
							prev_items_end = prev_items_start - columns_span;

						$prev_carousel_group = $('<div class="tm_pb_carousel_group prev" style="display: none;left: 100%;position: absolute;top: 0;">').insertBefore( $active_carousel_group );
						$prev_carousel_group.css({ 'left': '-' + $active_carousel_group.innerWidth(), 'width': $active_carousel_group.innerWidth() }).show();

						// this is an endless loop, so it can decide internally when to break out, so that next_position
						// can get filled up, even to the extent of an element having both and current_ and next_ position
						for( x = ( items.length - 1 ), total = ( items.length - 1 ) ; ; x--, total-- ) {

							if ( total <= active_items_start && total >= active_items_end ) {
								$( items[x] ).addClass( 'changing_position current_position current_position_' + current_position );
								$( items[x] ).data('current_position', current_position );
								current_position--;
							}

							if ( total <= prev_items_start && total >= prev_items_end ) {
								$( items[x] ).data('prev_position', prev_position );
								$( items[x] ).addClass('changing_position prev_position prev_position_' + prev_position );

								if ( !$( items[x] ).hasClass( 'current_position' ) ) {
									$( items[x] ).addClass('container_append');
								} else {
									$( items[x] ).clone(true).appendTo( $active_carousel_group ).addClass('delayed_container_append_dup').attr('id', $( items[x] ).attr('id') + '-dup' );
									$( items[x] ).addClass('delayed_container_append');
								}

								prev_position--;
							}

							if ( prev_position <= 0 ) {
								break;
							}

							if ( x == 0 ) {
								x = items.length;
							}
						}

						sorted = $portfolio_items.find('.container_append, .delayed_container_append_dup').sort(function (a, b) {
							var el_a_position = parseInt( $(a).data('prev_position') );
							var el_b_position = parseInt( $(b).data('prev_position') );
							return ( el_a_position < el_b_position ) ? -1 : ( el_a_position > el_b_position ) ? 1 : 0;
						});

						$( sorted ).show().appendTo( $prev_carousel_group );

						$prev_carousel_group.children().each(function(){
							$(this).css({'width': item_width, 'position':'absolute', 'left': ( item_width * ( $(this).data('prev_position') - 1 ) ) });
						});

						$active_carousel_group.animate({
							left: '100%'
						}, {
							duration: slide_duration,
							complete: function() {
								$portfolio_items.find('.delayed_container_append').reverse().each(function(){
									$(this).css({'width': item_width, 'position':'absolute', 'left': ( item_width * ( $(this).data('prev_position') - 1 ) ) });
									$(this).prependTo( $prev_carousel_group );
								});

								$active_carousel_group.removeClass('active');
								$active_carousel_group.children().each(function(){
									position = $(this).data('position');
									current_position = $(this).data('current_position');
									$(this).removeClass('position_' + position + ' ' + 'changing_position current_position current_position_' + current_position );
									$(this).data('position', '');
									$(this).data('current_position', '');
									$(this).hide();
									$(this).css({'position': '', 'width': '', 'left': ''});
									$(this).appendTo( $portfolio_items );
								});

								$active_carousel_group.remove();
							}
						} );

						$prev_carousel_group.addClass('active').css({'position':'absolute', 'top':0, left: '-100%'});
						$prev_carousel_group.animate({
							left: '0%'
						}, {
							duration: slide_duration,
							complete: function(){
								setTimeout(function(){
									$prev_carousel_group.removeClass('prev').addClass('active').css({'position':'', 'width':'', 'top':'', 'left': ''});

									$prev_carousel_group.find('.delayed_container_append_dup').remove();

									$prev_carousel_group.find('.changing_position').each(function( index ){
										position = $(this).data('position');
										current_position = $(this).data('current_position');
										prev_position = $(this).data('prev_position');
										$(this).removeClass('container_append delayed_container_append position_' + position + ' ' + 'changing_position current_position current_position_' + current_position + ' prev_position prev_position_' + prev_position );
										$(this).data('current_position', '');
										$(this).data('prev_position', '');
										position = index + 1;
										$(this).data('position', position );
										$(this).addClass('position_' + position );
									});

									$prev_carousel_group.children().css({'position': '', 'width': original_item_width, 'left': ''});
									$the_portfolio.data('carouseling', false);
								}, 100 );
							}
						} );
					}

					return false;
				});

			} else {
				// setup fullwidth portfolio grid
				set_fullwidth_portfolio_columns( $the_portfolio, false );
			}

		});
	}

	function tm_audio_module_set() {
		if ( $( '.tm_pb_audio_module .mejs-audio' ).length || $( '.tm_audio_content .mejs-audio' ).length ) {
			$( '.tm_audio_container' ).each( function(){
				var $this_player = $( this ),
					$time_rail = $this_player.find( '.mejs-time-rail' ),
					$time_slider = $this_player.find( '.mejs-time-slider' );
				// remove previously added width and min-width attributes to calculate the new sizes accurately
				$time_rail.removeAttr( 'style' );
				$time_slider.removeAttr( 'style' );

				var $count_timer = $this_player.find( 'div.mejs-currenttime-container' ),
					player_width = $this_player.width(),
					controls_play_width = $this_player.find( '.mejs-play' ).outerWidth(),
					time_width = $this_player.find( '.mejs-currenttime-container' ).outerWidth(),
					volume_icon_width = $this_player.find( '.mejs-volume-button' ).outerWidth(),
					volume_bar_width = $this_player.find( '.mejs-horizontal-volume-slider' ).outerWidth(),
					new_time_rail_width;

				$count_timer.addClass( 'custom' );
				$this_player.find( '.mejs-controls div.mejs-duration-container' ).replaceWith( $count_timer );
				new_time_rail_width = player_width - ( controls_play_width + time_width + volume_icon_width + volume_bar_width + 100 );

				if ( 0 < new_time_rail_width ) {
					$time_rail.attr( 'style', 'min-width: ' + new_time_rail_width + 'px;' );
					$time_slider.attr( 'style', 'min-width: ' + new_time_rail_width + 'px;' );
				}
			});
		}
	}

	if ( $('.tm_pb_section_video').length ) {
		window._wpmejsSettings.pauseOtherPlayers = false;
	}

	if ( $tm_pb_filterable_portfolio.length ) {

		$(window).load(function(){

			$tm_pb_filterable_portfolio.each(function(){
				var $the_portfolio = $(this),
					$the_portfolio_items = $the_portfolio.find('.tm_pb_portfolio_items'),
					$left_orientatation = true == $the_portfolio.data( 'rtl' ) ? false : true;

				$the_portfolio.show();

				set_filterable_grid_items( $the_portfolio );

				$the_portfolio.on('click', '.tm_pb_portfolio_filter a', function(e){
					e.preventDefault();
					var category_slug = $(this).data('category-slug');
					$the_portfolio_items = $(this).parents('.tm_pb_filterable_portfolio').find('.tm_pb_portfolio_items');

					if ( 'all' == category_slug ) {
						$the_portfolio.find('.tm_pb_portfolio_filter a').removeClass('active');
						$the_portfolio.find('.tm_pb_portfolio_filter_all a').addClass('active');
						$the_portfolio.find('.tm_pb_portfolio_item').removeClass('active inactive');
						$the_portfolio.find('.tm_pb_portfolio_item').show();
						$the_portfolio.find('.tm_pb_portfolio_item').addClass('active');
					} else {
						$the_portfolio.find('.tm_pb_portfolio_filter_all').removeClass('active');
						$the_portfolio.find('.tm_pb_portfolio_filter a').removeClass('active');
						$the_portfolio.find('.tm_pb_portfolio_filter_all a').removeClass('active');
						$(this).addClass('active');

						$the_portfolio_items.find('.tm_pb_portfolio_item').hide();
						$the_portfolio_items.find('.tm_pb_portfolio_item').addClass( 'inactive' );
						$the_portfolio_items.find('.tm_pb_portfolio_item').removeClass('active');
						$the_portfolio_items.find('.tm_pb_portfolio_item.project_category_' + $(this).data('category-slug') ).show();
						$the_portfolio_items.find('.tm_pb_portfolio_item.project_category_' + $(this).data('category-slug') ).addClass('active').removeClass( 'inactive' );
					}

					set_filterable_grid_items( $the_portfolio );
					setTimeout(function(){
						set_filterable_portfolio_hash( $the_portfolio );
					}, 500 );
				});

				$(this).on('tm_hashchange', function( event ){
					var params = event.params;
					$the_portfolio = $( '#' + event.target.id );

					if ( !$the_portfolio.find('.tm_pb_portfolio_filter a[data-category-slug="' + params[0] + '"]').hasClass('active') ) {
						$the_portfolio.find('.tm_pb_portfolio_filter a[data-category-slug="' + params[0] + '"]').click();
					}

					if ( params[1] ) {
						setTimeout(function(){
							if ( !$the_portfolio.find('.tm_pb_portofolio_pagination a.page-' + params[1]).hasClass('active') ) {
								$the_portfolio.find('.tm_pb_portofolio_pagination a.page-' + params[1]).addClass('active').click();
							}
						}, 300 );
					}
				});
			});

		}); // End $(window).load()

		function set_filterable_grid_items( $the_portfolio ) {
			var active_category = $the_portfolio.find('.tm_pb_portfolio_filter > a.active').data('category-slug'),
				container_width = $the_portfolio.find( '.tm_pb_portfolio_items' ).innerWidth(),
				item_width = $the_portfolio.find( '.tm_pb_portfolio_item' ).outerWidth( true ),
				last_item_margin = item_width - $the_portfolio.find( '.tm_pb_portfolio_item' ).outerWidth(),
				columns_count = Math.round( ( container_width + last_item_margin ) / item_width ),
				counter = 1,
				first_in_row = 1;

				$the_portfolio.find( '.tm_pb_portfolio_item' ).removeClass( 'last_in_row first_in_row' );
				$the_portfolio.find( '.tm_pb_portfolio_item' ).each( function() {
					var $this_el = $( this );

					if ( ! $this_el.hasClass( 'inactive' ) ) {
						if ( first_in_row === counter ) {
							$this_el.addClass( 'first_in_row' );
						}

						if ( 0 === counter % columns_count ) {
							$this_el.addClass( 'last_in_row' );
							first_in_row = counter + 1;
						}
						counter++;
					}
				});

			if ( 'all' === active_category ) {
				$the_portfolio_visible_items = $the_portfolio.find('.tm_pb_portfolio_item');
			} else {
				$the_portfolio_visible_items = $the_portfolio.find('.tm_pb_portfolio_item.project_category_' + active_category);
			}

			var visible_grid_items = $the_portfolio_visible_items.length,
				posts_number = $the_portfolio.data('posts-number'),
				pages = Math.ceil( visible_grid_items / posts_number );

			set_filterable_grid_pages( $the_portfolio, pages );

			var visible_grid_items = 0;
			var _page = 1;
			$the_portfolio.find('.tm_pb_portfolio_item').data('page', '');
			$the_portfolio_visible_items.each(function(i){
				visible_grid_items++;
				if ( 0 === parseInt( visible_grid_items % posts_number ) ) {
					$(this).data('page', _page);
					_page++;
				} else {
					$(this).data('page', _page);
				}
			});

			$the_portfolio_visible_items.filter(function() {
				return $(this).data('page') == 1;
			}).show();

			$the_portfolio_visible_items.filter(function() {
				return $(this).data('page') != 1;
			}).hide();
		}

		function set_filterable_grid_pages( $the_portfolio, pages ) {
			$pagination = $the_portfolio.find('.tm_pb_portofolio_pagination');

			if ( !$pagination.length ) {
				return;
			}

			$pagination.html('<ul></ul>');
			if ( pages <= 1 ) {
				return;
			}

			$pagination_list = $pagination.children('ul');
			$pagination_list.append('<li class="prev" style="display:none;"><a href="#" data-page="prev" class="page-prev">' + tm_pb_custom.prev + '</a></li>');
			for( var page = 1; page <= pages; page++ ) {
				var first_page_class = page === 1 ? ' active' : '',
					last_page_class = page === pages ? ' last-page' : '',
					hidden_page_class = page >= 5 ? ' style="display:none;"' : '';
				$pagination_list.append('<li' + hidden_page_class + ' class="page page-' + page + '"><a href="#" data-page="' + page + '" class="page-' + page + first_page_class + last_page_class + '">' + page + '</a></li>');
			}
			$pagination_list.append('<li class="next"><a href="#" data-page="next" class="page-next">' + tm_pb_custom.next + '</a></li>');
		}

		$tm_pb_filterable_portfolio.on('click', '.tm_pb_portofolio_pagination a', function(e){
			e.preventDefault();

			var to_page = $(this).data('page'),
				$the_portfolio = $(this).parents('.tm_pb_filterable_portfolio'),
				$the_portfolio_items = $the_portfolio.find('.tm_pb_portfolio_items');

			tm_pb_smooth_scroll( $the_portfolio, false, 800 );

			if ( $(this).hasClass('page-prev') ) {
				to_page = parseInt( $(this).parents('ul').find('a.active').data('page') ) - 1;
			} else if ( $(this).hasClass('page-next') ) {
				to_page = parseInt( $(this).parents('ul').find('a.active').data('page') ) + 1;
			}

			$(this).parents('ul').find('a').removeClass('active');
			$(this).parents('ul').find('a.page-' + to_page ).addClass('active');

			var current_index = $(this).parents('ul').find('a.page-' + to_page ).parent().index(),
				total_pages = $(this).parents('ul').find('li.page').length;

			$(this).parent().nextUntil('.page-' + ( current_index + 3 ) ).show();
			$(this).parent().prevUntil('.page-' + ( current_index - 3 ) ).show();

			$(this).parents('ul').find('li.page').each(function(i){
				if ( !$(this).hasClass('prev') && !$(this).hasClass('next') ) {
					if ( i < ( current_index - 3 ) ) {
						$(this).hide();
					} else if ( i > ( current_index + 1 ) ) {
						$(this).hide();
					} else {
						$(this).show();
					}

					if ( total_pages - current_index <= 2 && total_pages - i <= 5 ) {
						$(this).show();
					} else if ( current_index <= 3 && i <= 4 ) {
						$(this).show();
					}

				}
			});

			if ( to_page > 1 ) {
				$(this).parents('ul').find('li.prev').show();
			} else {
				$(this).parents('ul').find('li.prev').hide();
			}

			if ( $(this).parents('ul').find('a.active').hasClass('last-page') ) {
				$(this).parents('ul').find('li.next').hide();
			} else {
				$(this).parents('ul').find('li.next').show();
			}

			$the_portfolio.find('.tm_pb_portfolio_item').hide();
			$the_portfolio.find('.tm_pb_portfolio_item').filter(function( index ) {
				return $(this).data('page') === to_page;
			}).show();

			setTimeout(function(){
				set_filterable_portfolio_hash( $the_portfolio );
			}, 500 );
		});

		function set_filterable_portfolio_hash( $the_portfolio ) {

			if ( !$the_portfolio.attr('id') ) {
				return;
			}

			var this_portfolio_state = [];
			this_portfolio_state.push( $the_portfolio.attr('id') );
			this_portfolio_state.push( $the_portfolio.find('.tm_pb_portfolio_filter > a.active').data('category-slug') );

			if ( $the_portfolio.find('.tm_pb_portofolio_pagination a.active').length ) {
				this_portfolio_state.push( $the_portfolio.find('.tm_pb_portofolio_pagination a.active').data('page') );
			} else {
				this_portfolio_state.push( 1 );
			}

			this_portfolio_state = this_portfolio_state.join( tm_hash_module_param_seperator );

			tm_set_hash( this_portfolio_state );
		}
	} /*  end if ( $tm_pb_filterable_portfolio.length ) */

	if ( $tm_pb_gallery.length ) {

		function set_gallery_grid_items( $the_gallery ) {
			var $the_gallery_items_container = $the_gallery.find('.tm_pb_gallery_items'),
				$the_gallery_items = $the_gallery_items_container.find('.tm_pb_gallery_item');

			var total_grid_items = $the_gallery_items.length,
				posts_number = $the_gallery_items_container.data('per_page'),
				pages = Math.ceil( total_grid_items / posts_number );

			set_gallery_grid_pages( $the_gallery, pages );

			var total_grid_items = 0;
			var _page = 1;
			$the_gallery_items.data('page', '');
			$the_gallery_items.each(function(i){
				total_grid_items++;
				if ( 0 === parseInt( total_grid_items % posts_number ) ) {
					$(this).data('page', _page);
					_page++;
				} else {
					$(this).data('page', _page);
				}

			});

			var visible_items = $the_gallery_items.filter(function() {
				return $(this).data('page') == 1;
			}).show();

			$the_gallery_items.filter(function() {
				return $(this).data('page') != 1;
			}).hide();
		}

		function set_gallery_grid_pages( $the_gallery, pages ) {
			$pagination = $the_gallery.find('.tm_pb_gallery_pagination');

			if ( !$pagination.length ) {
				return;
			}

			$pagination.html('<ul></ul>');
			if ( pages <= 1 ) {
				$pagination.hide();
				return;
			}

			$pagination_list = $pagination.children('ul');
			$pagination_list.append('<li class="prev" style="display:none;"><a href="#" data-page="prev" class="page-prev">' + tm_pb_custom.prev + '</a></li>');
			for( var page = 1; page <= pages; page++ ) {
				var first_page_class = page === 1 ? ' active' : '',
					last_page_class = page === pages ? ' last-page' : '',
					hidden_page_class = page >= 5 ? ' style="display:none;"' : '';
				$pagination_list.append('<li' + hidden_page_class + ' class="page page-' + page + '"><a href="#" data-page="' + page + '" class="page-' + page + first_page_class + last_page_class + '">' + page + '</a></li>');
			}
			$pagination_list.append('<li class="next"><a href="#" data-page="next" class="page-next">' + tm_pb_custom.next + '</a></li>');
		}

		function set_gallery_hash( $the_gallery ) {

			if ( !$the_gallery.attr('id') ) {
				return;
			}

			var this_gallery_state = [];
			this_gallery_state.push( $the_gallery.attr('id') );

			if ( $the_gallery.find('.tm_pb_gallery_pagination a.active').length ) {
				this_gallery_state.push( $the_gallery.find('.tm_pb_gallery_pagination a.active').data('page') );
			} else {
				this_gallery_state.push( 1 );
			}

			this_gallery_state = this_gallery_state.join( tm_hash_module_param_seperator );

			tm_set_hash( this_gallery_state );
		}

		$tm_pb_gallery.each(function(){
			var $the_gallery = $(this);

			if ( $the_gallery.hasClass( 'tm_pb_gallery_grid' ) ) {

				$the_gallery.show();
				set_gallery_grid_items( $the_gallery );

				$the_gallery.on('tm_hashchange', function( event ){
					var params = event.params;
					$the_gallery = $( '#' + event.target.id );

					if ( page_to = params[0] ) {
						if ( !$the_gallery.find('.tm_pb_gallery_pagination a.page-' + page_to ).hasClass('active') ) {
							$the_gallery.find('.tm_pb_gallery_pagination a.page-' + page_to ).addClass('active').click();
						}
					}
				});
			}

		});

		$tm_pb_gallery.data('paginating', false );
		$tm_pb_gallery.on('click', '.tm_pb_gallery_pagination a', function(e){
			e.preventDefault();

			var to_page = $(this).data('page'),
				$the_gallery = $(this).parents('.tm_pb_gallery'),
				$the_gallery_items_container = $the_gallery.find('.tm_pb_gallery_items'),
				$the_gallery_items = $the_gallery_items_container.find('.tm_pb_gallery_item');

			if ( $the_gallery.data('paginating') ) {
				return;
			}

			$the_gallery.data('paginating', true );

			if ( $(this).hasClass('page-prev') ) {
				to_page = parseInt( $(this).parents('ul').find('a.active').data('page') ) - 1;
			} else if ( $(this).hasClass('page-next') ) {
				to_page = parseInt( $(this).parents('ul').find('a.active').data('page') ) + 1;
			}

			$(this).parents('ul').find('a').removeClass('active');
			$(this).parents('ul').find('a.page-' + to_page ).addClass('active');

			var current_index = $(this).parents('ul').find('a.page-' + to_page ).parent().index(),
				total_pages = $(this).parents('ul').find('li.page').length;

			$(this).parent().nextUntil('.page-' + ( current_index + 3 ) ).show();
			$(this).parent().prevUntil('.page-' + ( current_index - 3 ) ).show();

			$(this).parents('ul').find('li.page').each(function(i){
				if ( !$(this).hasClass('prev') && !$(this).hasClass('next') ) {
					if ( i < ( current_index - 3 ) ) {
						$(this).hide();
					} else if ( i > ( current_index + 1 ) ) {
						$(this).hide();
					} else {
						$(this).show();
					}

					if ( total_pages - current_index <= 2 && total_pages - i <= 5 ) {
						$(this).show();
					} else if ( current_index <= 3 && i <= 4 ) {
						$(this).show();
					}

				}
			});

			if ( to_page > 1 ) {
				$(this).parents('ul').find('li.prev').show();
			} else {
				$(this).parents('ul').find('li.prev').hide();
			}

			if ( $(this).parents('ul').find('a.active').hasClass('last-page') ) {
				$(this).parents('ul').find('li.next').hide();
			} else {
				$(this).parents('ul').find('li.next').show();
			}

			$the_gallery_items.hide();
			var visible_items = $the_gallery_items.filter(function( index ) {
				return $(this).data('page') === to_page;
			}).show();

			$the_gallery.data('paginating', false );

			setTimeout(function(){
				set_gallery_hash( $the_gallery );
			}, 100 );

			$( 'html, body' ).animate( { scrollTop : $the_gallery.offset().top - 200 }, 200 );
		});

	} /*  end if ( $tm_pb_gallery.length ) */

	if ( $tm_pb_counter_amount.length ) {
		$tm_pb_counter_amount.each(function(){
			var $bar_item           = $(this),
				bar_item_width      = $bar_item.attr( 'data-width' ),
				bar_item_padding    = Math.ceil( parseFloat( $bar_item.css('paddingLeft') ) ) + Math.ceil( parseFloat( $bar_item.css('paddingRight') ) ),
				$bar_item_text      = $bar_item.children( '.tm_pb_counter_amount_number' ),
				bar_item_text_width = $bar_item_text.width() + bar_item_padding;

			$bar_item.css({
				'width' : bar_item_width,
				'min-width' : bar_item_text_width
			});
		});
	} /* $tm_pb_counter_amount.length */

	function tm_countdown_timer( timer ) {
		var end_date = parseInt( timer.data( 'end-timestamp') ),
			current_date = new Date().getTime() / 1000,
			seconds_left = ( end_date - current_date );

		days = parseInt(seconds_left / 86400);
		days = days > 0 ? days : 0;
		seconds_left = seconds_left % 86400;

		hours = parseInt(seconds_left / 3600);
		hours = hours > 0 ? hours : 0;

		seconds_left = seconds_left % 3600;

		minutes = parseInt(seconds_left / 60);
		minutes = minutes > 0 ? minutes : 0;

		seconds = parseInt(seconds_left % 60);
		seconds = seconds > 0 ? seconds : 0;

		if ( days == 0 ) {
			if ( !timer.find('.days > .value').parent('.section').hasClass('zero') ) {
				timer.find('.days > .value').html( '00' ).parent('.section').addClass('zero').next().addClass('zero');
			}
		} else {
			days_slice = days.toString().length >= 2 ? days.toString().length : 2;
			timer.find('.days > .value').html( ('00' + days).slice(-days_slice) );
		}

		if ( days == 0 && hours == 0 ) {
			if ( !timer.find('.hours > .value').parent('.section').hasClass('zero') ) {
				timer.find('.hours > .value').html('00').parent('.section').addClass('zero').next().addClass('zero');
			}
		} else {
			timer.find('.hours > .value').html( ( '0' + hours ).slice(-2) );
		}

		if ( days == 0 && hours == 0 && minutes == 0 ) {
			if ( !timer.find('.minutes > .value').parent('.section').hasClass('zero') ) {
				timer.find('.minutes > .value').html('00').parent('.section').addClass('zero').next().addClass('zero');
			}
		} else {
			timer.find('.minutes > .value').html( ( '0' + minutes ).slice(-2) );
		}

		if ( days == 0 && hours == 0 && minutes == 0 && seconds == 0 ) {
			if ( !timer.find('.seconds > .value').parent('.section').hasClass('zero') ) {
				timer.find('.seconds > .value').html('00').parent('.section').addClass('zero');
			}
		} else {
			timer.find('.seconds > .value').html( ( '0' + seconds ).slice(-2) );
		}
	}

	function tm_countdown_timer_labels( timer ) {
		if ( timer.closest( '.tm_pb_column_3_8' ).length || timer.closest( '.tm_pb_column_1_4' ).length || timer.children('.tm_pb_countdown_timer_container').width() <= 400 ) {
			timer.find('.days .label').html( timer.find('.days').data('short') );
			timer.find('.hours .label').html( timer.find('.hours').data('short') );
			timer.find('.minutes .label').html( timer.find('.minutes').data('short') );
			timer.find('.seconds .label').html( timer.find('.seconds').data('short') );
		} else {
			timer.find('.days .label').html( timer.find('.days').data('full') );
			timer.find('.hours .label').html( timer.find('.hours').data('full') );
			timer.find('.minutes .label').html( timer.find('.minutes').data('full') );
			timer.find('.seconds .label').html( timer.find('.seconds').data('full') );
		}
	}

	if ( $tm_pb_countdown_timer.length ) {
		$tm_pb_countdown_timer.each(function(){
			var timer = $(this);
			tm_countdown_timer_labels( timer );
			tm_countdown_timer( timer );
			setInterval(function(){
				tm_countdown_timer( timer );
			}, 1000);
		});
	}

	if ( $tm_pb_tabs.length ) {
		$tm_pb_tabs.tm_pb_simple_slider( {
			use_controls   : false,
			use_arrows     : false,
			slide          : '.tm_pb_all_tabs > div',
			tabs_animation : true
		} ).on('tm_hashchange', function( event ){
			var params = event.params;
			var $the_tabs = $( '#' + event.target.id );
			var active_tab = params[0];
			if ( !$the_tabs.find( '.tm_pb_tabs_controls li' ).eq( active_tab ).hasClass('tm_pb_tab_active') ) {
				$the_tabs.find( '.tm_pb_tabs_controls li' ).eq( active_tab ).click();
			}
		});

		$tm_pb_tabs_li.click( function() {
			var $this_el        = $(this),
				$tabs_container = $this_el.closest( '.tm_pb_tabs' ).data('tm_pb_simple_slider');

			if ( $tabs_container.tm_animation_running ) return false;

			$this_el.addClass( 'tm_pb_tab_active' ).siblings().removeClass( 'tm_pb_tab_active' );

			$tabs_container.data('tm_pb_simple_slider').tm_slider_move_to( $this_el.index() );

			if ( $this_el.closest( '.tm_pb_tabs' ).attr('id') ) {
				var tab_state = [];
				tab_state.push( $this_el.closest( '.tm_pb_tabs' ).attr('id') );
				tab_state.push( $this_el.index() );
				tab_state = tab_state.join( tm_hash_module_param_seperator );
				tm_set_hash( tab_state );
			}

			return false;
		} );
	}

	if ( $tm_pb_map.length ) {
		google.maps.event.addDomListener(window, 'load', function() {
			$tm_pb_map.each(function(){
				var $this_map_container = $(this),
					$this_map = $this_map_container.children('.tm_pb_map'),
					this_map_grayscale = $this_map_container.data( 'grayscale' ) || 0,
					infowindow_active,
					marker_icon = $this_map.data( 'marker-icon' ) || [ tm_pb_custom.builder_images_uri + '/marker.png', 34, 53, false ],
					map_style = $this_map.data( 'map-style' );

					if ( this_map_grayscale !== 0 ) {
						this_map_grayscale = '-' + this_map_grayscale.toString();
					}

					$this_map_container.data('map', new google.maps.Map( $this_map[0], {
						zoom: parseInt( $this_map.data('zoom') ),
						center: new google.maps.LatLng( parseFloat( $this_map.data('center-lat') ) , parseFloat( $this_map.data('center-lng') )),
						mapTypeId: google.maps.MapTypeId.ROADMAP,
						scrollwheel: $this_map.data('mouse-wheel') == 'on' ? true : false,
						panControlOptions: {
							position: $this_map_container.is( '.tm_beneath_transparent_nav' ) ? google.maps.ControlPosition.LEFT_BOTTOM : google.maps.ControlPosition.LEFT_TOP
						},
						zoomControlOptions: {
							position: $this_map_container.is( '.tm_beneath_transparent_nav' ) ? google.maps.ControlPosition.LEFT_BOTTOM : google.maps.ControlPosition.LEFT_TOP
						},
						/*styles: [ {
							stylers: [
								{ saturation: parseInt( this_map_grayscale ) }
							]
						} ],*/
						styles: map_style
					}));

					$this_map_container.data('bounds', new google.maps.LatLngBounds() );
					$this_map_container.find('.tm_pb_map_pin').each(function(){

						var $this_marker = $(this),
							position = new google.maps.LatLng( parseFloat( $this_marker.data('lat') ) , parseFloat( $this_marker.data('lng') ) );

						$this_map_container.data('bounds').extend( position );

						var marker = new google.maps.Marker({
							position: position,
							map: $this_map_container.data('map'),
							title: $this_marker.data('title'),
							icon: {
								url: marker_icon[0],
								size: new google.maps.Size( marker_icon[1], marker_icon[2] ),
								anchor: new google.maps.Point( marker_icon[1] / 2, marker_icon[2] )
							},
							//shape: { coord: [1, 1, marker_icon[1], marker_icon[2] ], type: 'rect' },
						 //anchorPoint: new google.maps.Point(0, -45),
							animation: google.maps.Animation.DROP
						});

						if ( $this_marker.find('.infowindow').length ) {
							var infowindow = new google.maps.InfoWindow({
								content: $this_marker.html()
							});

							google.maps.event.addListener( $this_map_container.data('map'), 'click', function() {
								infowindow.close();
							});

							google.maps.event.addListener(marker, 'click', function() {
								if( infowindow_active ) {
									infowindow_active.close();
								}
								infowindow_active = infowindow;

								infowindow.open( $this_map_container.data('map'), marker );
							});
						}
					});

					google.maps.event.addListener( $this_map_container.data('map'), 'bounds_changed', function() {

						if ( !$this_map_container.data('map').getBounds().contains( $this_map_container.data('bounds').getNorthEast() ) || !$this_map_container.data('map').getBounds().contains( $this_map_container.data('bounds').getSouthWest() ) ) {
							$this_map_container.data('map').fitBounds( $this_map_container.data('bounds') );
						}

					});
			});
		} );
	}

	if ( $tm_pb_shop.length ) {
		$tm_pb_shop.each( function() {
			var $this_el = $(this),
				icon     = $this_el.data('icon') || '';

			if ( icon === '' ) {
				return true;
			}

			$this_el.find( '.tm_overlay' )
				.attr( 'data-icon', icon )
				.addClass( 'tm_pb_inline_icon' );
		} );
	}

	if ( $tm_pb_circle_counter.length ) {

		window.tm_pb_circle_counter_init = function($the_counter, animate) {
			if ( 0 === $the_counter.width() ) {
				return;
			}

			$the_counter.easyPieChart({
				animate: {
					duration: 1800,
					enabled: true
				},
				size: 0 !== $the_counter.width() ? $the_counter.width() : 10, // set the width to 10 if actual width is 0 to avoid js errors
				barColor: $the_counter.data( 'bar-bg-color' ),
				trackColor: $the_counter.data( 'color' ) || '#000000',
				trackAlpha: $the_counter.data( 'alpha' ) || '0.1',
				lineWidth: $the_counter.data( 'circle-width' ) || '5',
				size: $the_counter.data( 'circle-size' ) || '110',
				scaleColor: false,
				lineCap: $the_counter.data( 'bar-type' ) || 'round',
				onStart: function() {
					$(this.el).find('.percent p').css({ 'visibility' : 'visible' });
				},
				onStep: function(from, to, percent) {
					$(this.el).find('.percent-value').text( Math.round( parseInt( percent ) ) );
				},
				onStop: function(from, to) {
					$(this.el).find('.percent-value').text( $(this.el).data('number-value') );
				}
			});
		}

		window.tm_pb_reinit_circle_counters = function( $tm_pb_circle_counter ) {
			$tm_pb_circle_counter.each(function(){
				var $the_counter = $(this);
				window.tm_pb_circle_counter_init($the_counter, false);

				$the_counter.on('containerWidthChanged', function( event ){
					$the_counter = $( event.target );
					$the_counter.find('canvas').remove();
					$the_counter.removeData('easyPieChart' );
					window.tm_pb_circle_counter_init($the_counter, true);
				});

			});
		}
		window.tm_pb_reinit_circle_counters( $tm_pb_circle_counter );
	}

	if ( $tm_pb_number_counter.length ) {
		window.tm_pb_reinit_number_counters = function( $tm_pb_number_counter ) {

			if ( $.fn.fitText ) {
				$tm_pb_number_counter.find( '.percent' ).fitText( 0.3 );
			}

			$tm_pb_number_counter.each(function(){
				var $this_counter = $(this);
				$this_counter.easyPieChart({
					animate: {
						duration: 1800,
						enabled: true
					},
					size: 0,
					trackColor: false,
					scaleColor: false,
					lineWidth: 0,
					onStart: function() {
						$(this.el).find('.percent').css({ 'visibility' : 'visible' });
					},
					onStep: function(from, to, percent) {
						if ( percent != to )
							$(this.el).find('.percent-value').text( Math.round( parseInt( percent ) ) );
					},
					onStop: function(from, to) {
						$(this.el).find('.percent-value').text( $(this.el).data('number-value') );
					}
				});
			});
		}
		window.tm_pb_reinit_number_counters( $tm_pb_number_counter );
	}

	function tm_apply_parallax() {
		var $this = $(this),
			element_top = $this.offset().top,
			window_top = $tm_window.scrollTop(),
			y_pos = ( ( ( window_top + $tm_window.height() ) - element_top ) * 0.3 ),
			main_position;

		main_position = 'translate(0, ' + y_pos + 'px)';

		$this.find('.tm_parallax_bg').css( {
			'-webkit-transform' : main_position,
			'-moz-transform'    : main_position,
			'-ms-transform'     : main_position,
			'transform'         : main_position
		} );
	}

	function tm_parallax_set_height() {
		var $this = $(this),
			bg_height;

		bg_height = ( $tm_window.height() * 0.3 + $this.innerHeight() );

		$this.find('.tm_parallax_bg').css( { 'height' : bg_height } );
	}

	$('.tm_pb_toggle_title').click( function(){
		var $this_heading         = $(this),
			$module               = $this_heading.closest('.tm_pb_toggle'),
			$section              = $module.parents( '.tm_pb_section' ),
			$content              = $module.find('.tm_pb_toggle_content'),
			$accordion            = $module.closest( '.tm_pb_accordion' ),
			is_accordion          = $accordion.length,
			is_accordion_toggling = $accordion.hasClass( 'tm_pb_accordion_toggling' ),
			$accordion_active_toggle;

		if ( is_accordion ) {
			if ( $module.hasClass('tm_pb_toggle_open') || is_accordion_toggling ) {
				return false;
			}

			$accordion.addClass( 'tm_pb_accordion_toggling' );
			$accordion_active_toggle = $module.siblings('.tm_pb_toggle_open');
		}

		if ( $content.is( ':animated' ) ) {
			return;
		}

		$content.slideToggle( 700, function() {
			if ( $module.hasClass('tm_pb_toggle_close') ) {
				$module.removeClass('tm_pb_toggle_close').addClass('tm_pb_toggle_open');
			} else {
				$module.removeClass('tm_pb_toggle_open').addClass('tm_pb_toggle_close');
			}

			if ( $section.hasClass( 'tm_pb_section_parallax' ) && !$section.children().hasClass( 'tm_pb_parallax_css') ) {
				$.proxy( tm_parallax_set_height, $section )();
			}
		} );

		if ( is_accordion ) {
			$accordion_active_toggle.find('.tm_pb_toggle_content').slideToggle( 700, function() {
				$accordion_active_toggle.removeClass( 'tm_pb_toggle_open' ).addClass('tm_pb_toggle_close');
				$accordion.removeClass( 'tm_pb_accordion_toggling' );
			} );
		}
	} );

	var $tm_contact_container = $( '.tm_pb_contact_form_container' );

	if ( $tm_contact_container.length ) {
		$tm_contact_container.each( function() {
			var $this_contact_container = $( this ),
				$tm_contact_form = $this_contact_container.find( 'form' ),
				$tm_contact_submit = $this_contact_container.find( 'input.tm_pb_contact_submit' ),
				$tm_inputs = $tm_contact_form.find( 'input[type=text],textarea' ),
				tm_email_reg = /^[\w-]+(\.[\w-]+)*@([a-z0-9-]+(\.[a-z0-9-]+)*?\.[a-z]{2,6}|(\d{1,3}\.){3}\d{1,3})(:\d{4})?$/,
				redirect_url = typeof $this_contact_container.data( 'redirect_url' ) !== 'undefined' ? $this_contact_container.data( 'redirect_url' ) : '';

			$tm_contact_form.on( 'submit', function( event ) {
				var $this_contact_form = $( this ),
					$this_inputs = $this_contact_form.find( '.tm_pb_contact_form_input, .tm_pb_contact_captcha' ),
					this_tm_contact_error = false,
					$tm_contact_message = $this_contact_form.closest( '.tm_pb_contact_form_container' ).find( '.tm-pb-contact-message' ),
					tm_message = '',
					tm_fields_message = '',
					$this_contact_container = $this_contact_form.closest( '.tm_pb_contact_form_container' ),
					$captcha_field = $this_contact_form.find( '.tm_pb_contact_captcha' ),
					form_unique_id = typeof $this_contact_container.data( 'form_unique_num' ) !== 'undefined' ? $this_contact_container.data( 'form_unique_num' ) : 0,
					inputs_list = [];
				tm_message = '<ul>';

				$this_inputs.removeClass( 'tm_contact_error' );

				$this_inputs.each( function(){
					var $this_el = $( this ),
						this_val = $this_el.val(),
						this_label = $this_el.siblings( 'label' ).text(),
						field_type = typeof $this_el.data( 'field_type' ) !== 'undefined' ? $this_el.data( 'field_type' ) : 'text',
						required_mark = typeof $this_el.data( 'required_mark' ) !== 'undefined' ? $this_el.data( 'required_mark' ) : 'not_required',
						original_id = typeof $this_el.data( 'original_id' ) !== 'undefined' ? $this_el.data( 'original_id' ) : '',
						field_name;

					// add current field data into array of inputs
					if ( typeof $this_el.attr( 'id' ) !== 'undefined' ) {
						inputs_list.push( { 'field_id' : $this_el.attr( 'id' ), 'original_id' : original_id, 'required_mark' : required_mark, 'field_type' : field_type, 'field_label' : this_label } );
					}

					// add error message for the field if it is required and empty
					if ( 'required' === required_mark && ( '' === this_val || this_label === this_val || null === this_val ) ) {
						$this_el.addClass( 'tm_contact_error' );
						this_tm_contact_error = true;

						field_name = $this_el.data( 'original_title' );

						if ( ! field_name ) {
							field_name = $this_el.attr( 'name' );
						}

						tm_fields_message += '<li>' + field_name + '</li>';
					}

					// add error message if email field is not empty and fails the email validation
					if ( 'email' === field_type && '' !== this_val && this_label !== this_val && ! tm_email_reg.test( this_val ) ) {
						$this_el.addClass( 'tm_contact_error' );
						this_tm_contact_error = true;

						if ( ! tm_email_reg.test( this_val ) ) {
							tm_message += '<li>' + tm_pb_custom.invalid + '</li>';
						}
					}
				});

				// check the captcha value if required for current form
				if ( $captcha_field.length && '' !== $captcha_field.val() ) {
					var first_digit = parseInt( $captcha_field.data( 'first_digit' ) ),
						second_digit = parseInt( $captcha_field.data( 'second_digit' ) );

					if ( parseInt( $captcha_field.val() ) !== first_digit + second_digit ) {

						tm_message += '<li>' + tm_pb_custom.wrong_captcha + '</li>';
						this_tm_contact_error = true;

						// generate new digits for captcha
						first_digit = Math.floor( ( Math.random() * 15 ) + 1 );
						second_digit = Math.floor( ( Math.random() * 15 ) + 1 );

						// set new digits for captcha
						$captcha_field.data( 'first_digit', first_digit );
						$captcha_field.data( 'second_digit', second_digit );

						// regenerate captcha on page
						$this_contact_form.find( '.tm_pb_contact_captcha_question' ).empty().append( first_digit  + ' + ' + second_digit );
					}

				}

				if ( ! this_tm_contact_error ) {
					var $href = $( this ).attr( 'action' ),
						form_data = $( this ).serializeArray();

					form_data.push( { 'name': 'tm_pb_contact_email_fields_' + form_unique_id, 'value' : JSON.stringify( inputs_list ) } );

					$this_contact_container.fadeTo( 'fast', 0.2 ).load( $href + ' #' + $this_contact_form.closest( '.tm_pb_contact_form_container' ).attr( 'id' ), form_data, function( responseText ) {
						// redirect if redirect URL is not empty and no errors in contact form
						if ( '' !== redirect_url && ! $( responseText ).find( '.tm_pb_contact_error_text').length ) {
							window.location.href = redirect_url;
						}

						$this_contact_container.fadeTo( 'fast', 1 );
					} );
				}

				tm_message += '</ul>';

				if ( '' !== tm_fields_message ) {
					if ( tm_message != '<ul></ul>' ) {
						tm_message = '<p class="tm_normal_padding">' + tm_pb_custom.contact_error_message + '</p>' + tm_message;
					}

					tm_fields_message = '<ul>' + tm_fields_message + '</ul>';

					tm_fields_message = '<p>' + tm_pb_custom.fill_message + '</p>' + tm_fields_message;

					tm_message = tm_fields_message + tm_message;
				}

				if ( tm_message != '<ul></ul>' ) {
					$tm_contact_message.html( tm_message );
				}

				event.preventDefault();
			});
		});
	}

	$( '.tm_pb_video .tm_pb_video_overlay, .tm_pb_video_wrap .tm_pb_video_overlay' ).click( function() {
		var $this        = $(this),
			$video_image = $this.closest( '.tm_pb_video_overlay' );

		$video_image.fadeTo( 500, 0, function() {
			var $image = $(this);

			$image.css( 'display', 'none' );
		} );

		return false;
	} );

	function tm_pb_resize_section_video_bg( $video ) {
		$element = typeof $video !== 'undefined' ? $video.closest( '.tm_pb_section_video_bg' ) : $( '.tm_pb_section_video_bg' );

		$element.each( function() {
			var $this_el = $(this),
				ratio = ( typeof $this_el.attr( 'data-ratio' ) !== 'undefined' )
					? $this_el.attr( 'data-ratio' )
					: $this_el.find('video').attr( 'width' ) / $this_el.find('video').attr( 'height' ),
				$video_elements = $this_el.find( '.mejs-video, video, object' ).css( 'margin', 0 ),
				$container = $this_el.closest( '.tm_pb_section_video' ).length
					? $this_el.closest( '.tm_pb_section_video' )
					: $this_el.closest( '.tm_pb_slides' ),
				body_width = $container.width(),
				container_height = $container.innerHeight(),
				width, height;

			if ( typeof $this_el.attr( 'data-ratio' ) == 'undefined' )
				$this_el.attr( 'data-ratio', ratio );

			if ( body_width / container_height < ratio ) {
				width = container_height * ratio;
				height = container_height;
			} else {
				width = body_width;
				height = body_width / ratio;
			}

			$video_elements.width( width ).height( height );
		} );
	}

	function tm_pb_center_video( $video ) {
		$element = typeof $video !== 'undefined' ? $video : $( '.tm_pb_section_video_bg .mejs-video' );

		$element.each( function() {
			var $video_width = $(this).width() / 2;
			var $video_width_negative = 0 - $video_width;
			$(this).css("margin-left",$video_width_negative );

			if ( typeof $video !== 'undefined' ) {
				if ( $video.closest( '.tm_pb_slider' ).length && ! $video.closest( '.tm_pb_first_video' ).length )
					return false;
			}
		} );
	}

	function tm_fix_slider_height() {
		if ( ! $tm_pb_slider.length ) return;

		$tm_pb_slider.each( function() {
			var $slide_section = $(this).parent( '.tm_pb_section' ),
				$slide = $(this).find( '.tm_pb_slide' ),
				$slide_container = $slide.find( '.tm_pb_container' ),
				max_height = 0;

			// If this is appears at the first section benath transparent nav, skip it
			// leave it to tm_fix_page_container_position()
			if ( $slide_section.is( '.tm_pb_section_first' ) ){
				return true;
			}

			$slide_container.css( 'min-height', 0 );

			$slide.each( function() {
				var $this_el = $(this),
					height = $this_el.innerHeight();

				if ( max_height < height )
					max_height = height;
			} );

			$slide_container.css( 'min-height', max_height );
		} );
	}

	/**
	 * Add conditional class to prevent unwanted dropdown nav
	 */
	function tm_fix_nav_direction() {
		window_width = $(window).width();
		$('.nav li.tm-reverse-direction-nav').removeClass( 'tm-reverse-direction-nav' );
		$('.nav li li ul').each(function(){
			var $dropdown       = $(this),
				dropdown_width  = $dropdown.width(),
				dropdown_offset = $dropdown.offset(),
				$parents        = $dropdown.parents('.nav > li');

			if ( dropdown_offset.left > ( window_width - dropdown_width ) ) {
				$parents.addClass( 'tm-reverse-direction-nav' );
			}
		});
	}
	tm_fix_nav_direction();

	tm_pb_form_placeholders_init( $( '.tm_pb_newsletter_form' ) );

	$('.tm_pb_fullwidth_menu ul.nav').each(function(i) {
		i++;
		tm_duplicate_menu( $(this), $(this).parents('.tm_pb_row').find('div .mobile_nav'), 'mobile_menu' + i, 'tm_mobile_menu' );
	});

	window.tm_fix_testimonial_inner_width = function() {
		var window_width = $( window ).width();

		if( window_width > 767 ){
			$( '.tm_pb_testimonial' ).each( function() {
				if ( ! $(this).is(':visible') ) {
					return;
				}

				var $testimonial      = $(this),
					testimonial_width = $testimonial.width(),
					$portrait         = $testimonial.find( '.tm_pb_testimonial_portrait' ),
					portrait_width    = $portrait.width(),
					$testimonial_inner= $testimonial.find( '.tm_pb_testimonial_description_inner' ),
					$outer_column     = $testimonial.closest( '.tm_pb_column' ),
					testimonial_inner_width = testimonial_width,
					subtract = ! ( $outer_column.hasClass( 'tm_pb_column_1_3' ) || $outer_column.hasClass( 'tm_pb_column_1_4' ) || $outer_column.hasClass( 'tm_pb_column_3_8' ) ) ? portrait_width + 31 : 0;

					$testimonial_inner.width( testimonial_inner_width - subtract );
			} );
		} else {
			$( '.tm_pb_testimonial_description_inner' ).removeAttr( 'style' );
		}
	}
	window.tm_fix_testimonial_inner_width();

	window.tm_reinint_waypoint_modules = function() {
		if ( $.fn.waypoint ) {
			var $tm_pb_circle_counter = $( '.tm_pb_circle_counter_bar' ),
				$tm_pb_number_counter = $( '.tm_pb_number_counter' );

			$( '.tm_pb_counter_container, .tm-waypoint' ).waypoint( {
				offset: '75%',
				handler: function() {
					$(this.element).addClass( 'tm-animated' );
				}
			} );

			if ( $tm_pb_circle_counter.length ) {
				$tm_pb_circle_counter.each(function(){
					var $this_counter = $(this);
					if ( ! $this_counter.is( ':visible' ) ) {
						return;
					}
					$this_counter.waypoint({
						offset: '65%',
						handler: function() {
							$this_counter.data('easyPieChart').update( $this_counter.data('number-value') );
						}
					});
				});
			}

			if ( $tm_pb_number_counter.length ) {
				$tm_pb_number_counter.each(function(){
					var $this_counter = $(this);
					$this_counter.waypoint({
						offset: '75%',
						handler: function() {
							$this_counter.data('easyPieChart').update( $this_counter.data('number-value') );
						}
					});
				});
			}
		}
	}

	window.tm_calc_fullscreen_section = function() {
		var $tm_window = $(window),
			$body = $( 'body' ),
			$wpadminbar = $( '#wpadminbar' ),
			tm_is_vertical_nav = $body.hasClass( 'tm_vertical_nav' ),
			$this_section = $(this),
			this_section_index = $this_section.index('.tm_pb_fullwidth_header'),
			$header = $this_section.children('.tm_pb_fullwidth_header_container'),
			$header_content = $header.children('.header-content-container'),
			$header_image = $header.children('.header-image-container'),
			sectionHeight = $tm_window.height(),
			$wpadminbar = $('#wpadminbar'),
			$top_header = $('#top-header'),
			$main_header = $('#main-header'),
			tm_header_height,
			secondary_nav_height;

			secondary_nav_height = $top_header.length && $top_header.is( ':visible' ) ? $top_header.innerHeight() : 0;
			tm_header_height = $main_header.length ? $main_header.innerHeight() + secondary_nav_height : 0;

		var calc_header_offset = ( $wpadminbar.length ) ? tm_header_height + $wpadminbar.innerHeight() - 1 : tm_header_height - 1;

		// Section height adjustment differs in vertical and horizontal nav
		if ( $body.hasClass('tm_vertical_nav') ) {
			if ( $tm_window.width() >= 980 && $top_header.length ) {
				sectionHeight -= $top_header.height();
			}

			if ( $wpadminbar.length ) {
				sectionHeight -= $wpadminbar.height();
			}
		} else {
			if ( $body.hasClass('tm_hide_nav' ) ) {
				// If user is logged in and hide navigation is in use, adjust the section height
				if ( $wpadminbar.length ) {
					sectionHeight -= $wpadminbar.height();
				}

				// In mobile, header always appears. Adjust the section height
				if ( $tm_window.width() < 981 && ! $body.hasClass('tm_transparent_nav') ) {
					sectionHeight -= $('#main-header').height();
				}
			} else {
				if ( $this_section.offset().top <= calc_header_offset + 3 ) {
					if ( tm_is_vertical_nav ) {
						var $top_header = $('#top-header'),
							top_header_height = ( $top_header.length && 0 === $this_section.index( '.tm_pb_fullscreen' ) ) ? $top_header.height() : 0,
							wpadminbar_height = ( $wpadminbar.length && 0 === $this_section.index( '.tm_pb_fullscreen' ) ) ? $wpadminbar.height() : 0,
							calc_header_offset_vertical = wpadminbar_height + top_header_height;

						sectionHeight -= calc_header_offset_vertical;
					} else {
						sectionHeight -= calc_header_offset;
					}
				}
			}
		}

		// If the transparent primary nav + hide nav until scroll is being used,
		// cancel automatic padding-top added by transparent nav mechanism
		if ( $body.hasClass('tm_transparent_nav') && $body.hasClass( 'tm_hide_nav' ) &&  0 === this_section_index ) {
			$this_section.css( 'padding-top', '' );
		}

		$this_section.css('min-height', sectionHeight + 'px' );
		$header.css('min-height', sectionHeight + 'px' );

		if ( $header.hasClass('center') && $header_content.hasClass('bottom') && $header_image.hasClass('bottom') ) {
			$header.addClass('bottom-bottom');
		}

		if ( $header.hasClass('center') && $header_content.hasClass('center') && $header_image.hasClass('center') ) {
			$header.addClass('center-center');
		}

		if ( $header.hasClass('center') && $header_content.hasClass('center') && $header_image.hasClass('bottom') ) {
			$header.addClass('center-bottom');

			var contentHeight = sectionHeight - $header_image.outerHeight( true );

			if ( contentHeight > 0 ) {
				$header_content.css('min-height', contentHeight + 'px' );
			}
		}

		if ( $header.hasClass('center') && $header_content.hasClass('bottom') && $header_image.hasClass('center') ) {
			$header.addClass('bottom-center');
		}

		if ( ( $header.hasClass('left') || $header.hasClass('right') ) && !$header_content.length && $header_image.length ) {
			$header.css('justify-content', 'flex-end');
		}

		if ( $header.hasClass('center') && $header_content.hasClass('bottom') && !$header_image.length ) {
			$header_content.find('.header-content').css( 'margin-bottom', 80 + 'px' );
		}

		if ( $header_content.hasClass('bottom') && $header_image.hasClass('center') ) {
			$header_image.find('.header-image').css( 'margin-bottom', 80 + 'px' );
			$header_image.css('align-self', 'flex-end');
		}
	}

	$( window ).resize( function(){
		var window_width                = $tm_window.width(),
			tm_container_css_width      = $tm_container.css( 'width' ),
			tm_container_width_in_pixel = ( typeof tm_container_css_width !== 'undefined' ) ? tm_container_css_width.substr( -1, 1 ) !== '%' : '',
			tm_container_actual_width   = ( tm_container_width_in_pixel ) ? $tm_container.width() : ( ( $tm_container.width() / 100 ) * window_width ), // $tm_container.width() doesn't recognize pixel or percentage unit. It's our duty to understand what it returns and convert it properly
			containerWidthChanged       = tm_container_width !== tm_container_actual_width;

		tm_pb_resize_section_video_bg();
		tm_pb_center_video();
		tm_fix_slider_height();
		tm_fix_nav_direction();

		$tm_pb_fullwidth_portfolio.each(function(){
			set_container_height = $(this).hasClass('tm_pb_fullwidth_portfolio_carousel') ? true : false;
			set_fullwidth_portfolio_columns( $(this), set_container_height );
		});

		if ( containerWidthChanged ) {
			$('.container-width-change-notify').trigger('containerWidthChanged');

			setTimeout( function() {
				$tm_pb_filterable_portfolio.each(function(){
					set_filterable_grid_items( $(this) );
				});
				$tm_pb_gallery.each(function(){
					if ( $(this).hasClass( 'tm_pb_gallery_grid' ) ) {
						set_gallery_grid_items( $(this) );
					}
				});
			}, 100 );

			tm_container_width = tm_container_actual_width;

			etRecalculateOffset = true;

			if ( $tm_pb_circle_counter.length ) {
				$tm_pb_circle_counter.each(function(){
					var $this_counter = $(this);
					if ( ! $this_counter.is( ':visible' ) ) {
						return;
					}

					$this_counter.data('easyPieChart').update( $this_counter.data('number-value') );
				});
			}
			if ( $tm_pb_countdown_timer.length ) {
				$tm_pb_countdown_timer.each(function(){
					var timer = $(this);
					tm_countdown_timer_labels( timer );
				} );
			}
		}

		window.tm_fix_testimonial_inner_width();

		tm_audio_module_set();
	} );

	$( window ).ready( function(){
		if ( $.fn.fitVids ) {
			$( '.tm_pb_slide_video' ).fitVids();
			$( '.tm_pb_module' ).fitVids( { customSelector: "iframe[src^='http://www.hulu.com'], iframe[src^='http://www.dailymotion.com'], iframe[src^='http://www.funnyordie.com'], iframe[src^='https://embed-ssl.ted.com'], iframe[src^='http://embed.revision3.com'], iframe[src^='https://flickr.com'], iframe[src^='http://blip.tv'], iframe[src^='http://www.collegehumor.com']"} );
		}

		tm_fix_video_wmode('.fluid-width-video-wrapper');

		tm_fix_slider_height();
	} );

	$( window ).load( function(){
		tm_fix_fullscreen_section();

		$( 'section.tm_pb_fullscreen' ).each( function(){
			var $this_section = $( this );

			$.proxy( tm_calc_fullscreen_section, $this_section )();

			$tm_window.on( 'resize', $.proxy( tm_calc_fullscreen_section, $this_section ) );
		});

		$( '.tm_pb_fullwidth_header_scroll a' ).click( function( event ) {
			event.preventDefault();

			var $this_section      = $(this).parents( 'section' ),
				is_next_fullscreen = $this_section.next().hasClass( 'tm_pb_fullscreen' ),
				$wpadminbar        = $('#wpadminbar'),
				wpadminbar_height  = ( $wpadminbar.length && ! is_next_fullscreen ) ? $wpadminbar.height() : 0,
				main_header_height = is_next_fullscreen || ! tm_is_fixed_nav ? 0 : $main_header.height(),
				top_header_height  = is_next_fullscreen || ! tm_is_fixed_nav ? 0 : $top_header.height(),
				section_bottom     = $this_section.offset().top + $this_section.outerHeight( true ) - ( wpadminbar_height + top_header_height + main_header_height );

			if ( $this_section.length ) {
				$( 'html, body' ).animate( { scrollTop : section_bottom }, 800 );

				if ( ! $( '#main-header' ).hasClass( 'tm-fixed-header' ) && $( 'body' ).hasClass( 'tm_fixed_nav' ) && $( window ).width() > 980 ) {
					setTimeout(function(){
						var section_offset_top = $this_section.offset().top,
							section_height     = $this_section.outerHeight( true ),
							main_header_height = is_next_fullscreen ? 0 : $main_header.height(),
							section_bottom     = section_offset_top + section_height - ( main_header_height + top_header_height + wpadminbar_height);

						$( 'html, body' ).animate( { scrollTop : section_bottom }, 280, 'linear' );
					}, 780 );
				}
			}
		});

		setTimeout( function() {
			$( '.tm_pb_preload' ).removeClass( 'tm_pb_preload' );
		}, 500 );

		if ( $.fn.hashchange ) {
			$(window).hashchange( function(){
				var hash = window.location.hash.substring(1);
				process_tm_hashchange( hash );
			});
			$(window).hashchange();
		}

		if ( $tm_pb_parallax.length && !tm_is_mobile_device ) {
			$tm_pb_parallax.each(function(){
				if ( $(this).hasClass('tm_pb_parallax_css') ) {
					return;
				}

				var $this_parent = $(this).parent();

				$.proxy( tm_parallax_set_height, $this_parent )();

				$.proxy( tm_apply_parallax, $this_parent )();

				$tm_window.on( 'scroll', $.proxy( tm_apply_parallax, $this_parent ) );

				$tm_window.on( 'resize', $.proxy( tm_parallax_set_height, $this_parent ) );
				$tm_window.on( 'resize', $.proxy( tm_apply_parallax, $this_parent ) );

				$this_parent.find('.tm-learn-more .heading-more').click( function() {
					setTimeout(function(){
						$.proxy( tm_parallax_set_height, $this_parent )();
					}, 300 );
				});
			});
		}

		tm_audio_module_set();

		window.tm_reinint_waypoint_modules();
	} );

	if ( $( '.tm_section_specialty' ).length ) {
		$( '.tm_section_specialty' ).each( function() {
			var this_row = $( this ).find( '.tm_pb_row' );

			this_row.find( '>.tm_pb_column:not(.tm_pb_specialty_column)' ).addClass( 'tm_pb_column_single' );
		});
	}

	/**
	* In particular browser, map + parallax doesn't play well due the use of CSS 3D transform
	*/
	if ( $('.tm_pb_section_parallax').length && $('.tm_pb_map').length ) {
		$('body').addClass( 'parallax-map-support' );
	}

	/**
	 * Add conditional class for search widget in sidebar module
	 */
	$('.tm_pb_widget_area ' + tm_pb_custom.widget_search_selector ).each( function() {
		var $search_wrap              = $(this),
			$search_input_submit      = $search_wrap.find('input[type="submit"]'),
			search_input_submit_text = $search_input_submit.attr( 'value' ),
			$search_button            = $search_wrap.find('button'),
			search_button_text       = $search_button.text(),
			has_submit_button         = $search_input_submit.length || $search_button.length ? true : false,
			min_column_width          = 150;

		if ( ! $search_wrap.find( 'input[type="text"]' ).length && ! $search_wrap.find( 'input[type="search"]' ).length ) {
			return;
		}

		// Mark no button state
		if ( ! has_submit_button ) {
			$search_wrap.addClass( 'tm-no-submit-button' );
		}

		// Mark narrow state
		if ( $search_wrap.width() < 150 ) {
			$search_wrap.addClass( 'tm-narrow-wrapper' );
		}

		// Fixes issue where theme's search button has no text: treat it as non-existent
		if ( $search_input_submit.length && ( typeof search_input_submit_text == 'undefined' || search_input_submit_text === '' ) ) {
			$search_input_submit.remove();
			$search_wrap.addClass( 'tm-no-submit-button' );
		}

		if ( $search_button.length && ( typeof search_button_text == 'undefined' || search_button_text === '' ) ) {
			$search_button.remove();
			$search_wrap.addClass( 'tm-no-submit-button' );
		}

	} );

	if ( $( '.tm_pb_search' ).length ) {
		$( '.tm_pb_search' ).each( function() {
			var $this_module = $( this ),
				$input_field = $this_module.find( '.tm_pb_s' ),
				$button = $this_module.find( '.tm_pb_searchsubmit' ),
				input_padding = $this_module.hasClass( 'tm_pb_text_align_right' ) ? 'paddingLeft' : 'paddingRight',
				disabled_button = $this_module.hasClass( 'tm_pb_hide_search_button' );

			if ( $button.innerHeight() > $input_field.innerHeight() ) {
				$input_field.height( $button.innerHeight() );
			}

			if ( ! disabled_button ) {
				$input_field.css( input_padding, $button.innerWidth() + 10 );
			}
		});
	}

	// apply required classes for the Reply buttons in Comments Module
	if ( $( '.tm_pb_comments_module' ).length ) {
		$( '.tm_pb_comments_module' ).each( function() {
			var $comments_module = $( this ),
				$comments_module_button = $comments_module.find( '.comment-reply-link' );

			if ( $comments_module_button.length ) {
				$comments_module_button.addClass( 'tm_pb_button' );

				if ( typeof $comments_module.data( 'icon' ) !== 'undefined' ) {
					$comments_module_button.attr( 'data-icon', $comments_module.data( 'icon' ) );
					$comments_module_button.addClass( 'tm_pb_custom_button_icon' );
				}
			}
		});
	}

	//  Carousel Module
	if ( $( '.tm_pb_swiper' )[0] ) {
		$( '.tm_pb_swiper' ).each( function() {
			var $this = $( this ),
				settings = $this.data('settings'),
				pagination = ( 'on' === settings['pagination'] ) ? true : false ,
				navigateButton = ( 'on' === settings['navigateButton'] ) ? true : false ,
				autoplay = ( 'on' === settings['autoplay'] ) ? 3500 : false ,
				centeredSlides = ( 'on' === settings['centeredSlides'] ) ? true : false ,
				spaceBetweenSlides = settings['spaceBetweenSlides'] || 0 ,
				slidesPerView = settings['slidesPerView'],
				swiperContainer = $( '.swiper-container', $this ),
				swiper = new Swiper( swiperContainer , {
						slidesPerView: +slidesPerView,
						autoplay: autoplay,
						centeredSlides: centeredSlides,
						mousewheelControl: false,
						paginationClickable: true,
						spaceBetween: +spaceBetweenSlides,
						speed: 500,
						nextButton: ( navigateButton ) ? $('.swiper-button-next', $this) : null,
						prevButton: ( navigateButton ) ? $('.swiper-button-prev', $this) : null,
						pagination: ( pagination ) ? $('.swiper-pagination', $this) : null,
						onInit: function(){
							if ( ! navigateButton ) {
								$('.swiper-button-next, .swiper-button-prev', $this).remove();
							}

							if ( ! pagination ) {
								$('.swiper-pagination', $this).remove();
							}
						},
						breakpoints: {
							1200: {
								slidesPerView: Math.floor( slidesPerView * 0.75 ),
								spaceBetween: Math.floor( spaceBetweenSlides * 0.75 )
							},
							992: {
								slidesPerView: Math.floor( slidesPerView * 0.5 ),
								spaceBetween: Math.floor( spaceBetweenSlides * 0.5 )
							},
							769: {
								slidesPerView: ( 0 !== Math.floor( slidesPerView * 0.25 ) ) ? Math.floor( slidesPerView * 0.25 ) : 1
							},
						}
				});
		});

		$( window ).on( 'load', loadHandler );
		function loadHandler() {
			$( '.tm_pb_swiper' ).css({ 'opacity': '1' });
		}
	}

});

// fin scripts.js 


/**
 * Swiper 3.3.1
 * Most modern mobile touch slider and framework with hardware accelerated transitions
 * 
 * http://www.idangero.us/swiper/
 * 
 * Copyright 2016, Vladimir Kharlampidi
 * The iDangero.us
 * http://www.idangero.us/
 * 
 * Licensed under MIT
 * 
 * Released on: February 7, 2016
 */
!function(){"use strict";function e(e){e.fn.swiper=function(a){var s;return e(this).each(function(){var e=new t(this,a);s||(s=e)}),s}}var a,t=function(e,s){function r(e){return Math.floor(e)}function i(){y.autoplayTimeoutId=setTimeout(function(){y.params.loop?(y.fixLoop(),y._slideNext(),y.emit("onAutoplay",y)):y.isEnd?s.autoplayStopOnLast?y.stopAutoplay():(y._slideTo(0),y.emit("onAutoplay",y)):(y._slideNext(),y.emit("onAutoplay",y))},y.params.autoplay)}function n(e,t){var s=a(e.target);if(!s.is(t))if("string"==typeof t)s=s.parents(t);else if(t.nodeType){var r;return s.parents().each(function(e,a){a===t&&(r=t)}),r?t:void 0}if(0!==s.length)return s[0]}function o(e,a){a=a||{};var t=window.MutationObserver||window.WebkitMutationObserver,s=new t(function(e){e.forEach(function(e){y.onResize(!0),y.emit("onObserverUpdate",y,e)})});s.observe(e,{attributes:"undefined"==typeof a.attributes?!0:a.attributes,childList:"undefined"==typeof a.childList?!0:a.childList,characterData:"undefined"==typeof a.characterData?!0:a.characterData}),y.observers.push(s)}function l(e){e.originalEvent&&(e=e.originalEvent);var a=e.keyCode||e.charCode;if(!y.params.allowSwipeToNext&&(y.isHorizontal()&&39===a||!y.isHorizontal()&&40===a))return!1;if(!y.params.allowSwipeToPrev&&(y.isHorizontal()&&37===a||!y.isHorizontal()&&38===a))return!1;if(!(e.shiftKey||e.altKey||e.ctrlKey||e.metaKey||document.activeElement&&document.activeElement.nodeName&&("input"===document.activeElement.nodeName.toLowerCase()||"textarea"===document.activeElement.nodeName.toLowerCase()))){if(37===a||39===a||38===a||40===a){var t=!1;if(y.container.parents(".swiper-slide").length>0&&0===y.container.parents(".swiper-slide-active").length)return;var s={left:window.pageXOffset,top:window.pageYOffset},r=window.innerWidth,i=window.innerHeight,n=y.container.offset();y.rtl&&(n.left=n.left-y.container[0].scrollLeft);for(var o=[[n.left,n.top],[n.left+y.width,n.top],[n.left,n.top+y.height],[n.left+y.width,n.top+y.height]],l=0;l<o.length;l++){var p=o[l];p[0]>=s.left&&p[0]<=s.left+r&&p[1]>=s.top&&p[1]<=s.top+i&&(t=!0)}if(!t)return}y.isHorizontal()?((37===a||39===a)&&(e.preventDefault?e.preventDefault():e.returnValue=!1),(39===a&&!y.rtl||37===a&&y.rtl)&&y.slideNext(),(37===a&&!y.rtl||39===a&&y.rtl)&&y.slidePrev()):((38===a||40===a)&&(e.preventDefault?e.preventDefault():e.returnValue=!1),40===a&&y.slideNext(),38===a&&y.slidePrev())}}function p(e){e.originalEvent&&(e=e.originalEvent);var a=y.mousewheel.event,t=0,s=y.rtl?-1:1;if("mousewheel"===a)if(y.params.mousewheelForceToAxis)if(y.isHorizontal()){if(!(Math.abs(e.wheelDeltaX)>Math.abs(e.wheelDeltaY)))return;t=e.wheelDeltaX*s}else{if(!(Math.abs(e.wheelDeltaY)>Math.abs(e.wheelDeltaX)))return;t=e.wheelDeltaY}else t=Math.abs(e.wheelDeltaX)>Math.abs(e.wheelDeltaY)?-e.wheelDeltaX*s:-e.wheelDeltaY;else if("DOMMouseScroll"===a)t=-e.detail;else if("wheel"===a)if(y.params.mousewheelForceToAxis)if(y.isHorizontal()){if(!(Math.abs(e.deltaX)>Math.abs(e.deltaY)))return;t=-e.deltaX*s}else{if(!(Math.abs(e.deltaY)>Math.abs(e.deltaX)))return;t=-e.deltaY}else t=Math.abs(e.deltaX)>Math.abs(e.deltaY)?-e.deltaX*s:-e.deltaY;if(0!==t){if(y.params.mousewheelInvert&&(t=-t),y.params.freeMode){var r=y.getWrapperTranslate()+t*y.params.mousewheelSensitivity,i=y.isBeginning,n=y.isEnd;if(r>=y.minTranslate()&&(r=y.minTranslate()),r<=y.maxTranslate()&&(r=y.maxTranslate()),y.setWrapperTransition(0),y.setWrapperTranslate(r),y.updateProgress(),y.updateActiveIndex(),(!i&&y.isBeginning||!n&&y.isEnd)&&y.updateClasses(),y.params.freeModeSticky?(clearTimeout(y.mousewheel.timeout),y.mousewheel.timeout=setTimeout(function(){y.slideReset()},300)):y.params.lazyLoading&&y.lazy&&y.lazy.load(),0===r||r===y.maxTranslate())return}else{if((new window.Date).getTime()-y.mousewheel.lastScrollTime>60)if(0>t)if(y.isEnd&&!y.params.loop||y.animating){if(y.params.mousewheelReleaseOnEdges)return!0}else y.slideNext();else if(y.isBeginning&&!y.params.loop||y.animating){if(y.params.mousewheelReleaseOnEdges)return!0}else y.slidePrev();y.mousewheel.lastScrollTime=(new window.Date).getTime()}return y.params.autoplay&&y.stopAutoplay(),e.preventDefault?e.preventDefault():e.returnValue=!1,!1}}function d(e,t){e=a(e);var s,r,i,n=y.rtl?-1:1;s=e.attr("data-swiper-parallax")||"0",r=e.attr("data-swiper-parallax-x"),i=e.attr("data-swiper-parallax-y"),r||i?(r=r||"0",i=i||"0"):y.isHorizontal()?(r=s,i="0"):(i=s,r="0"),r=r.indexOf("%")>=0?parseInt(r,10)*t*n+"%":r*t*n+"px",i=i.indexOf("%")>=0?parseInt(i,10)*t+"%":i*t+"px",e.transform("translate3d("+r+", "+i+",0px)")}function u(e){return 0!==e.indexOf("on")&&(e=e[0]!==e[0].toUpperCase()?"on"+e[0].toUpperCase()+e.substring(1):"on"+e),e}if(!(this instanceof t))return new t(e,s);var c={direction:"horizontal",touchEventsTarget:"container",initialSlide:0,speed:300,autoplay:!1,autoplayDisableOnInteraction:!0,autoplayStopOnLast:!1,iOSEdgeSwipeDetection:!1,iOSEdgeSwipeThreshold:20,freeMode:!1,freeModeMomentum:!0,freeModeMomentumRatio:1,freeModeMomentumBounce:!0,freeModeMomentumBounceRatio:1,freeModeSticky:!1,freeModeMinimumVelocity:.02,autoHeight:!1,setWrapperSize:!1,virtualTranslate:!1,effect:"slide",coverflow:{rotate:50,stretch:0,depth:100,modifier:1,slideShadows:!0},flip:{slideShadows:!0,limitRotation:!0},cube:{slideShadows:!0,shadow:!0,shadowOffset:20,shadowScale:.94},fade:{crossFade:!1},parallax:!1,scrollbar:null,scrollbarHide:!0,scrollbarDraggable:!1,scrollbarSnapOnRelease:!1,keyboardControl:!1,mousewheelControl:!1,mousewheelReleaseOnEdges:!1,mousewheelInvert:!1,mousewheelForceToAxis:!1,mousewheelSensitivity:1,hashnav:!1,breakpoints:void 0,spaceBetween:0,slidesPerView:1,slidesPerColumn:1,slidesPerColumnFill:"column",slidesPerGroup:1,centeredSlides:!1,slidesOffsetBefore:0,slidesOffsetAfter:0,roundLengths:!1,touchRatio:1,touchAngle:45,simulateTouch:!0,shortSwipes:!0,longSwipes:!0,longSwipesRatio:.5,longSwipesMs:300,followFinger:!0,onlyExternal:!1,threshold:0,touchMoveStopPropagation:!0,uniqueNavElements:!0,pagination:null,paginationElement:"span",paginationClickable:!1,paginationHide:!1,paginationBulletRender:null,paginationProgressRender:null,paginationFractionRender:null,paginationCustomRender:null,paginationType:"bullets",resistance:!0,resistanceRatio:.85,nextButton:null,prevButton:null,watchSlidesProgress:!1,watchSlidesVisibility:!1,grabCursor:!1,preventClicks:!0,preventClicksPropagation:!0,slideToClickedSlide:!1,lazyLoading:!1,lazyLoadingInPrevNext:!1,lazyLoadingInPrevNextAmount:1,lazyLoadingOnTransitionStart:!1,preloadImages:!0,updateOnImagesReady:!0,loop:!1,loopAdditionalSlides:0,loopedSlides:null,control:void 0,controlInverse:!1,controlBy:"slide",allowSwipeToPrev:!0,allowSwipeToNext:!0,swipeHandler:null,noSwiping:!0,noSwipingClass:"swiper-no-swiping",slideClass:"swiper-slide",slideActiveClass:"swiper-slide-active",slideVisibleClass:"swiper-slide-visible",slideDuplicateClass:"swiper-slide-duplicate",slideNextClass:"swiper-slide-next",slidePrevClass:"swiper-slide-prev",wrapperClass:"swiper-wrapper",bulletClass:"swiper-pagination-bullet",bulletActiveClass:"swiper-pagination-bullet-active",buttonDisabledClass:"swiper-button-disabled",paginationCurrentClass:"swiper-pagination-current",paginationTotalClass:"swiper-pagination-total",paginationHiddenClass:"swiper-pagination-hidden",paginationProgressbarClass:"swiper-pagination-progressbar",observer:!1,observeParents:!1,a11y:!1,prevSlideMessage:"Previous slide",nextSlideMessage:"Next slide",firstSlideMessage:"This is the first slide",lastSlideMessage:"This is the last slide",paginationBulletMessage:"Go to slide {{index}}",runCallbacksOnInit:!0},m=s&&s.virtualTranslate;s=s||{};var f={};for(var g in s)if("object"!=typeof s[g]||null===s[g]||(s[g].nodeType||s[g]===window||s[g]===document||"undefined"!=typeof Dom7&&s[g]instanceof Dom7||"undefined"!=typeof jQuery&&s[g]instanceof jQuery))f[g]=s[g];else{f[g]={};for(var h in s[g])f[g][h]=s[g][h]}for(var v in c)if("undefined"==typeof s[v])s[v]=c[v];else if("object"==typeof s[v])for(var w in c[v])"undefined"==typeof s[v][w]&&(s[v][w]=c[v][w]);var y=this;if(y.params=s,y.originalParams=f,y.classNames=[],"undefined"!=typeof a&&"undefined"!=typeof Dom7&&(a=Dom7),("undefined"!=typeof a||(a="undefined"==typeof Dom7?window.Dom7||window.Zepto||window.jQuery:Dom7))&&(y.$=a,y.currentBreakpoint=void 0,y.getActiveBreakpoint=function(){if(!y.params.breakpoints)return!1;var e,a=!1,t=[];for(e in y.params.breakpoints)y.params.breakpoints.hasOwnProperty(e)&&t.push(e);t.sort(function(e,a){return parseInt(e,10)>parseInt(a,10)});for(var s=0;s<t.length;s++)e=t[s],e>=window.innerWidth&&!a&&(a=e);return a||"max"},y.setBreakpoint=function(){var e=y.getActiveBreakpoint();if(e&&y.currentBreakpoint!==e){var a=e in y.params.breakpoints?y.params.breakpoints[e]:y.originalParams,t=y.params.loop&&a.slidesPerView!==y.params.slidesPerView;for(var s in a)y.params[s]=a[s];y.currentBreakpoint=e,t&&y.destroyLoop&&y.reLoop(!0)}},y.params.breakpoints&&y.setBreakpoint(),y.container=a(e),0!==y.container.length)){if(y.container.length>1){var b=[];return y.container.each(function(){b.push(new t(this,s))}),b}y.container[0].swiper=y,y.container.data("swiper",y),y.classNames.push("swiper-container-"+y.params.direction),y.params.freeMode&&y.classNames.push("swiper-container-free-mode"),y.support.flexbox||(y.classNames.push("swiper-container-no-flexbox"),y.params.slidesPerColumn=1),y.params.autoHeight&&y.classNames.push("swiper-container-autoheight"),(y.params.parallax||y.params.watchSlidesVisibility)&&(y.params.watchSlidesProgress=!0),["cube","coverflow","flip"].indexOf(y.params.effect)>=0&&(y.support.transforms3d?(y.params.watchSlidesProgress=!0,y.classNames.push("swiper-container-3d")):y.params.effect="slide"),"slide"!==y.params.effect&&y.classNames.push("swiper-container-"+y.params.effect),"cube"===y.params.effect&&(y.params.resistanceRatio=0,y.params.slidesPerView=1,y.params.slidesPerColumn=1,y.params.slidesPerGroup=1,y.params.centeredSlides=!1,y.params.spaceBetween=0,y.params.virtualTranslate=!0,y.params.setWrapperSize=!1),("fade"===y.params.effect||"flip"===y.params.effect)&&(y.params.slidesPerView=1,y.params.slidesPerColumn=1,y.params.slidesPerGroup=1,y.params.watchSlidesProgress=!0,y.params.spaceBetween=0,y.params.setWrapperSize=!1,"undefined"==typeof m&&(y.params.virtualTranslate=!0)),y.params.grabCursor&&y.support.touch&&(y.params.grabCursor=!1),y.wrapper=y.container.children("."+y.params.wrapperClass),y.params.pagination&&(y.paginationContainer=a(y.params.pagination),y.params.uniqueNavElements&&"string"==typeof y.params.pagination&&y.paginationContainer.length>1&&1===y.container.find(y.params.pagination).length&&(y.paginationContainer=y.container.find(y.params.pagination)),"bullets"===y.params.paginationType&&y.params.paginationClickable?y.paginationContainer.addClass("swiper-pagination-clickable"):y.params.paginationClickable=!1,y.paginationContainer.addClass("swiper-pagination-"+y.params.paginationType)),(y.params.nextButton||y.params.prevButton)&&(y.params.nextButton&&(y.nextButton=a(y.params.nextButton),y.params.uniqueNavElements&&"string"==typeof y.params.nextButton&&y.nextButton.length>1&&1===y.container.find(y.params.nextButton).length&&(y.nextButton=y.container.find(y.params.nextButton))),y.params.prevButton&&(y.prevButton=a(y.params.prevButton),y.params.uniqueNavElements&&"string"==typeof y.params.prevButton&&y.prevButton.length>1&&1===y.container.find(y.params.prevButton).length&&(y.prevButton=y.container.find(y.params.prevButton)))),y.isHorizontal=function(){return"horizontal"===y.params.direction},y.rtl=y.isHorizontal()&&("rtl"===y.container[0].dir.toLowerCase()||"rtl"===y.container.css("direction")),y.rtl&&y.classNames.push("swiper-container-rtl"),y.rtl&&(y.wrongRTL="-webkit-box"===y.wrapper.css("display")),y.params.slidesPerColumn>1&&y.classNames.push("swiper-container-multirow"),y.device.android&&y.classNames.push("swiper-container-android"),y.container.addClass(y.classNames.join(" ")),y.translate=0,y.progress=0,y.velocity=0,y.lockSwipeToNext=function(){y.params.allowSwipeToNext=!1},y.lockSwipeToPrev=function(){y.params.allowSwipeToPrev=!1},y.lockSwipes=function(){y.params.allowSwipeToNext=y.params.allowSwipeToPrev=!1},y.unlockSwipeToNext=function(){y.params.allowSwipeToNext=!0},y.unlockSwipeToPrev=function(){y.params.allowSwipeToPrev=!0},y.unlockSwipes=function(){y.params.allowSwipeToNext=y.params.allowSwipeToPrev=!0},y.params.grabCursor&&(y.container[0].style.cursor="move",y.container[0].style.cursor="-webkit-grab",y.container[0].style.cursor="-moz-grab",y.container[0].style.cursor="grab"),y.imagesToLoad=[],y.imagesLoaded=0,y.loadImage=function(e,a,t,s,r){function i(){r&&r()}var n;e.complete&&s?i():a?(n=new window.Image,n.onload=i,n.onerror=i,t&&(n.srcset=t),a&&(n.src=a)):i()},y.preloadImages=function(){function e(){"undefined"!=typeof y&&null!==y&&(void 0!==y.imagesLoaded&&y.imagesLoaded++,y.imagesLoaded===y.imagesToLoad.length&&(y.params.updateOnImagesReady&&y.update(),y.emit("onImagesReady",y)))}y.imagesToLoad=y.container.find("img");for(var a=0;a<y.imagesToLoad.length;a++)y.loadImage(y.imagesToLoad[a],y.imagesToLoad[a].currentSrc||y.imagesToLoad[a].getAttribute("src"),y.imagesToLoad[a].srcset||y.imagesToLoad[a].getAttribute("srcset"),!0,e)},y.autoplayTimeoutId=void 0,y.autoplaying=!1,y.autoplayPaused=!1,y.startAutoplay=function(){return"undefined"!=typeof y.autoplayTimeoutId?!1:y.params.autoplay?y.autoplaying?!1:(y.autoplaying=!0,y.emit("onAutoplayStart",y),void i()):!1},y.stopAutoplay=function(e){y.autoplayTimeoutId&&(y.autoplayTimeoutId&&clearTimeout(y.autoplayTimeoutId),y.autoplaying=!1,y.autoplayTimeoutId=void 0,y.emit("onAutoplayStop",y))},y.pauseAutoplay=function(e){y.autoplayPaused||(y.autoplayTimeoutId&&clearTimeout(y.autoplayTimeoutId),y.autoplayPaused=!0,0===e?(y.autoplayPaused=!1,i()):y.wrapper.transitionEnd(function(){y&&(y.autoplayPaused=!1,y.autoplaying?i():y.stopAutoplay())}))},y.minTranslate=function(){return-y.snapGrid[0]},y.maxTranslate=function(){return-y.snapGrid[y.snapGrid.length-1]},y.updateAutoHeight=function(){var e=y.slides.eq(y.activeIndex)[0];if("undefined"!=typeof e){var a=e.offsetHeight;a&&y.wrapper.css("height",a+"px")}},y.updateContainerSize=function(){var e,a;e="undefined"!=typeof y.params.width?y.params.width:y.container[0].clientWidth,a="undefined"!=typeof y.params.height?y.params.height:y.container[0].clientHeight,0===e&&y.isHorizontal()||0===a&&!y.isHorizontal()||(e=e-parseInt(y.container.css("padding-left"),10)-parseInt(y.container.css("padding-right"),10),a=a-parseInt(y.container.css("padding-top"),10)-parseInt(y.container.css("padding-bottom"),10),y.width=e,y.height=a,y.size=y.isHorizontal()?y.width:y.height)},y.updateSlidesSize=function(){y.slides=y.wrapper.children("."+y.params.slideClass),y.snapGrid=[],y.slidesGrid=[],y.slidesSizesGrid=[];var e,a=y.params.spaceBetween,t=-y.params.slidesOffsetBefore,s=0,i=0;if("undefined"!=typeof y.size){"string"==typeof a&&a.indexOf("%")>=0&&(a=parseFloat(a.replace("%",""))/100*y.size),y.virtualSize=-a,y.rtl?y.slides.css({marginLeft:"",marginTop:""}):y.slides.css({marginRight:"",marginBottom:""});var n;y.params.slidesPerColumn>1&&(n=Math.floor(y.slides.length/y.params.slidesPerColumn)===y.slides.length/y.params.slidesPerColumn?y.slides.length:Math.ceil(y.slides.length/y.params.slidesPerColumn)*y.params.slidesPerColumn,"auto"!==y.params.slidesPerView&&"row"===y.params.slidesPerColumnFill&&(n=Math.max(n,y.params.slidesPerView*y.params.slidesPerColumn)));var o,l=y.params.slidesPerColumn,p=n/l,d=p-(y.params.slidesPerColumn*p-y.slides.length);for(e=0;e<y.slides.length;e++){o=0;var u=y.slides.eq(e);if(y.params.slidesPerColumn>1){var c,m,f;"column"===y.params.slidesPerColumnFill?(m=Math.floor(e/l),f=e-m*l,(m>d||m===d&&f===l-1)&&++f>=l&&(f=0,m++),c=m+f*n/l,u.css({"-webkit-box-ordinal-group":c,"-moz-box-ordinal-group":c,"-ms-flex-order":c,"-webkit-order":c,order:c})):(f=Math.floor(e/p),m=e-f*p),u.css({"margin-top":0!==f&&y.params.spaceBetween&&y.params.spaceBetween+"px"}).attr("data-swiper-column",m).attr("data-swiper-row",f)}"none"!==u.css("display")&&("auto"===y.params.slidesPerView?(o=y.isHorizontal()?u.outerWidth(!0):u.outerHeight(!0),y.params.roundLengths&&(o=r(o))):(o=(y.size-(y.params.slidesPerView-1)*a)/y.params.slidesPerView,y.params.roundLengths&&(o=r(o)),y.isHorizontal()?y.slides[e].style.width=o+"px":y.slides[e].style.height=o+"px"),y.slides[e].swiperSlideSize=o,y.slidesSizesGrid.push(o),y.params.centeredSlides?(t=t+o/2+s/2+a,0===e&&(t=t-y.size/2-a),Math.abs(t)<.001&&(t=0),i%y.params.slidesPerGroup===0&&y.snapGrid.push(t),y.slidesGrid.push(t)):(i%y.params.slidesPerGroup===0&&y.snapGrid.push(t),y.slidesGrid.push(t),t=t+o+a),y.virtualSize+=o+a,s=o,i++)}y.virtualSize=Math.max(y.virtualSize,y.size)+y.params.slidesOffsetAfter;var g;if(y.rtl&&y.wrongRTL&&("slide"===y.params.effect||"coverflow"===y.params.effect)&&y.wrapper.css({width:y.virtualSize+y.params.spaceBetween+"px"}),(!y.support.flexbox||y.params.setWrapperSize)&&(y.isHorizontal()?y.wrapper.css({width:y.virtualSize+y.params.spaceBetween+"px"}):y.wrapper.css({height:y.virtualSize+y.params.spaceBetween+"px"})),y.params.slidesPerColumn>1&&(y.virtualSize=(o+y.params.spaceBetween)*n,y.virtualSize=Math.ceil(y.virtualSize/y.params.slidesPerColumn)-y.params.spaceBetween,y.wrapper.css({width:y.virtualSize+y.params.spaceBetween+"px"}),y.params.centeredSlides)){for(g=[],e=0;e<y.snapGrid.length;e++)y.snapGrid[e]<y.virtualSize+y.snapGrid[0]&&g.push(y.snapGrid[e]);y.snapGrid=g}if(!y.params.centeredSlides){for(g=[],e=0;e<y.snapGrid.length;e++)y.snapGrid[e]<=y.virtualSize-y.size&&g.push(y.snapGrid[e]);y.snapGrid=g,Math.floor(y.virtualSize-y.size)-Math.floor(y.snapGrid[y.snapGrid.length-1])>1&&y.snapGrid.push(y.virtualSize-y.size)}0===y.snapGrid.length&&(y.snapGrid=[0]),0!==y.params.spaceBetween&&(y.isHorizontal()?y.rtl?y.slides.css({marginLeft:a+"px"}):y.slides.css({marginRight:a+"px"}):y.slides.css({marginBottom:a+"px"})),y.params.watchSlidesProgress&&y.updateSlidesOffset()}},y.updateSlidesOffset=function(){for(var e=0;e<y.slides.length;e++)y.slides[e].swiperSlideOffset=y.isHorizontal()?y.slides[e].offsetLeft:y.slides[e].offsetTop},y.updateSlidesProgress=function(e){if("undefined"==typeof e&&(e=y.translate||0),0!==y.slides.length){"undefined"==typeof y.slides[0].swiperSlideOffset&&y.updateSlidesOffset();var a=-e;y.rtl&&(a=e),y.slides.removeClass(y.params.slideVisibleClass);for(var t=0;t<y.slides.length;t++){var s=y.slides[t],r=(a-s.swiperSlideOffset)/(s.swiperSlideSize+y.params.spaceBetween);if(y.params.watchSlidesVisibility){var i=-(a-s.swiperSlideOffset),n=i+y.slidesSizesGrid[t],o=i>=0&&i<y.size||n>0&&n<=y.size||0>=i&&n>=y.size;o&&y.slides.eq(t).addClass(y.params.slideVisibleClass)}s.progress=y.rtl?-r:r}}},y.updateProgress=function(e){"undefined"==typeof e&&(e=y.translate||0);var a=y.maxTranslate()-y.minTranslate(),t=y.isBeginning,s=y.isEnd;0===a?(y.progress=0,y.isBeginning=y.isEnd=!0):(y.progress=(e-y.minTranslate())/a,y.isBeginning=y.progress<=0,y.isEnd=y.progress>=1),y.isBeginning&&!t&&y.emit("onReachBeginning",y),y.isEnd&&!s&&y.emit("onReachEnd",y),y.params.watchSlidesProgress&&y.updateSlidesProgress(e),y.emit("onProgress",y,y.progress)},y.updateActiveIndex=function(){var e,a,t,s=y.rtl?y.translate:-y.translate;for(a=0;a<y.slidesGrid.length;a++)"undefined"!=typeof y.slidesGrid[a+1]?s>=y.slidesGrid[a]&&s<y.slidesGrid[a+1]-(y.slidesGrid[a+1]-y.slidesGrid[a])/2?e=a:s>=y.slidesGrid[a]&&s<y.slidesGrid[a+1]&&(e=a+1):s>=y.slidesGrid[a]&&(e=a);(0>e||"undefined"==typeof e)&&(e=0),t=Math.floor(e/y.params.slidesPerGroup),t>=y.snapGrid.length&&(t=y.snapGrid.length-1),e!==y.activeIndex&&(y.snapIndex=t,y.previousIndex=y.activeIndex,y.activeIndex=e,y.updateClasses())},y.updateClasses=function(){y.slides.removeClass(y.params.slideActiveClass+" "+y.params.slideNextClass+" "+y.params.slidePrevClass);var e=y.slides.eq(y.activeIndex);e.addClass(y.params.slideActiveClass);var t=e.next("."+y.params.slideClass).addClass(y.params.slideNextClass);y.params.loop&&0===t.length&&y.slides.eq(0).addClass(y.params.slideNextClass);var s=e.prev("."+y.params.slideClass).addClass(y.params.slidePrevClass);if(y.params.loop&&0===s.length&&y.slides.eq(-1).addClass(y.params.slidePrevClass),y.paginationContainer&&y.paginationContainer.length>0){var r,i=y.params.loop?Math.ceil((y.slides.length-2*y.loopedSlides)/y.params.slidesPerGroup):y.snapGrid.length;if(y.params.loop?(r=Math.ceil((y.activeIndex-y.loopedSlides)/y.params.slidesPerGroup),r>y.slides.length-1-2*y.loopedSlides&&(r-=y.slides.length-2*y.loopedSlides),r>i-1&&(r-=i),0>r&&"bullets"!==y.params.paginationType&&(r=i+r)):r="undefined"!=typeof y.snapIndex?y.snapIndex:y.activeIndex||0,"bullets"===y.params.paginationType&&y.bullets&&y.bullets.length>0&&(y.bullets.removeClass(y.params.bulletActiveClass),y.paginationContainer.length>1?y.bullets.each(function(){a(this).index()===r&&a(this).addClass(y.params.bulletActiveClass)}):y.bullets.eq(r).addClass(y.params.bulletActiveClass)),"fraction"===y.params.paginationType&&(y.paginationContainer.find("."+y.params.paginationCurrentClass).text(r+1),y.paginationContainer.find("."+y.params.paginationTotalClass).text(i)),"progress"===y.params.paginationType){var n=(r+1)/i,o=n,l=1;y.isHorizontal()||(l=n,o=1),y.paginationContainer.find("."+y.params.paginationProgressbarClass).transform("translate3d(0,0,0) scaleX("+o+") scaleY("+l+")").transition(y.params.speed)}"custom"===y.params.paginationType&&y.params.paginationCustomRender&&(y.paginationContainer.html(y.params.paginationCustomRender(y,r+1,i)),y.emit("onPaginationRendered",y,y.paginationContainer[0]))}y.params.loop||(y.params.prevButton&&y.prevButton&&y.prevButton.length>0&&(y.isBeginning?(y.prevButton.addClass(y.params.buttonDisabledClass),y.params.a11y&&y.a11y&&y.a11y.disable(y.prevButton)):(y.prevButton.removeClass(y.params.buttonDisabledClass),y.params.a11y&&y.a11y&&y.a11y.enable(y.prevButton))),y.params.nextButton&&y.nextButton&&y.nextButton.length>0&&(y.isEnd?(y.nextButton.addClass(y.params.buttonDisabledClass),y.params.a11y&&y.a11y&&y.a11y.disable(y.nextButton)):(y.nextButton.removeClass(y.params.buttonDisabledClass),y.params.a11y&&y.a11y&&y.a11y.enable(y.nextButton))))},y.updatePagination=function(){if(y.params.pagination&&y.paginationContainer&&y.paginationContainer.length>0){var e="";if("bullets"===y.params.paginationType){for(var a=y.params.loop?Math.ceil((y.slides.length-2*y.loopedSlides)/y.params.slidesPerGroup):y.snapGrid.length,t=0;a>t;t++)e+=y.params.paginationBulletRender?y.params.paginationBulletRender(t,y.params.bulletClass):"<"+y.params.paginationElement+' class="'+y.params.bulletClass+'"></'+y.params.paginationElement+">";y.paginationContainer.html(e),y.bullets=y.paginationContainer.find("."+y.params.bulletClass),y.params.paginationClickable&&y.params.a11y&&y.a11y&&y.a11y.initPagination()}"fraction"===y.params.paginationType&&(e=y.params.paginationFractionRender?y.params.paginationFractionRender(y,y.params.paginationCurrentClass,y.params.paginationTotalClass):'<span class="'+y.params.paginationCurrentClass+'"></span> / <span class="'+y.params.paginationTotalClass+'"></span>',y.paginationContainer.html(e)),"progress"===y.params.paginationType&&(e=y.params.paginationProgressRender?y.params.paginationProgressRender(y,y.params.paginationProgressbarClass):'<span class="'+y.params.paginationProgressbarClass+'"></span>',y.paginationContainer.html(e)),"custom"!==y.params.paginationType&&y.emit("onPaginationRendered",y,y.paginationContainer[0])}},y.update=function(e){function a(){s=Math.min(Math.max(y.translate,y.maxTranslate()),y.minTranslate()),y.setWrapperTranslate(s),y.updateActiveIndex(),y.updateClasses()}if(y.updateContainerSize(),y.updateSlidesSize(),y.updateProgress(),y.updatePagination(),y.updateClasses(),y.params.scrollbar&&y.scrollbar&&y.scrollbar.set(),e){var t,s;y.controller&&y.controller.spline&&(y.controller.spline=void 0),y.params.freeMode?(a(),y.params.autoHeight&&y.updateAutoHeight()):(t=("auto"===y.params.slidesPerView||y.params.slidesPerView>1)&&y.isEnd&&!y.params.centeredSlides?y.slideTo(y.slides.length-1,0,!1,!0):y.slideTo(y.activeIndex,0,!1,!0),t||a())}else y.params.autoHeight&&y.updateAutoHeight()},y.onResize=function(e){y.params.breakpoints&&y.setBreakpoint();var a=y.params.allowSwipeToPrev,t=y.params.allowSwipeToNext;y.params.allowSwipeToPrev=y.params.allowSwipeToNext=!0,y.updateContainerSize(),y.updateSlidesSize(),("auto"===y.params.slidesPerView||y.params.freeMode||e)&&y.updatePagination(),y.params.scrollbar&&y.scrollbar&&y.scrollbar.set(),y.controller&&y.controller.spline&&(y.controller.spline=void 0);var s=!1;if(y.params.freeMode){var r=Math.min(Math.max(y.translate,y.maxTranslate()),y.minTranslate());y.setWrapperTranslate(r),y.updateActiveIndex(),y.updateClasses(),y.params.autoHeight&&y.updateAutoHeight()}else y.updateClasses(),s=("auto"===y.params.slidesPerView||y.params.slidesPerView>1)&&y.isEnd&&!y.params.centeredSlides?y.slideTo(y.slides.length-1,0,!1,!0):y.slideTo(y.activeIndex,0,!1,!0);y.params.lazyLoading&&!s&&y.lazy&&y.lazy.load(),y.params.allowSwipeToPrev=a,y.params.allowSwipeToNext=t};var x=["mousedown","mousemove","mouseup"];window.navigator.pointerEnabled?x=["pointerdown","pointermove","pointerup"]:window.navigator.msPointerEnabled&&(x=["MSPointerDown","MSPointerMove","MSPointerUp"]),y.touchEvents={start:y.support.touch||!y.params.simulateTouch?"touchstart":x[0],move:y.support.touch||!y.params.simulateTouch?"touchmove":x[1],end:y.support.touch||!y.params.simulateTouch?"touchend":x[2]},(window.navigator.pointerEnabled||window.navigator.msPointerEnabled)&&("container"===y.params.touchEventsTarget?y.container:y.wrapper).addClass("swiper-wp8-"+y.params.direction),y.initEvents=function(e){var a=e?"off":"on",t=e?"removeEventListener":"addEventListener",r="container"===y.params.touchEventsTarget?y.container[0]:y.wrapper[0],i=y.support.touch?r:document,n=y.params.nested?!0:!1;y.browser.ie?(r[t](y.touchEvents.start,y.onTouchStart,!1),i[t](y.touchEvents.move,y.onTouchMove,n),i[t](y.touchEvents.end,y.onTouchEnd,!1)):(y.support.touch&&(r[t](y.touchEvents.start,y.onTouchStart,!1),r[t](y.touchEvents.move,y.onTouchMove,n),r[t](y.touchEvents.end,y.onTouchEnd,!1)),!s.simulateTouch||y.device.ios||y.device.android||(r[t]("mousedown",y.onTouchStart,!1),document[t]("mousemove",y.onTouchMove,n),document[t]("mouseup",y.onTouchEnd,!1))),window[t]("resize",y.onResize),y.params.nextButton&&y.nextButton&&y.nextButton.length>0&&(y.nextButton[a]("click",y.onClickNext),y.params.a11y&&y.a11y&&y.nextButton[a]("keydown",y.a11y.onEnterKey)),y.params.prevButton&&y.prevButton&&y.prevButton.length>0&&(y.prevButton[a]("click",y.onClickPrev),y.params.a11y&&y.a11y&&y.prevButton[a]("keydown",y.a11y.onEnterKey)),y.params.pagination&&y.params.paginationClickable&&(y.paginationContainer[a]("click","."+y.params.bulletClass,y.onClickIndex),y.params.a11y&&y.a11y&&y.paginationContainer[a]("keydown","."+y.params.bulletClass,y.a11y.onEnterKey)),(y.params.preventClicks||y.params.preventClicksPropagation)&&r[t]("click",y.preventClicks,!0)},y.attachEvents=function(){y.initEvents()},y.detachEvents=function(){y.initEvents(!0)},y.allowClick=!0,y.preventClicks=function(e){y.allowClick||(y.params.preventClicks&&e.preventDefault(),y.params.preventClicksPropagation&&y.animating&&(e.stopPropagation(),e.stopImmediatePropagation()))},y.onClickNext=function(e){e.preventDefault(),(!y.isEnd||y.params.loop)&&y.slideNext()},y.onClickPrev=function(e){e.preventDefault(),(!y.isBeginning||y.params.loop)&&y.slidePrev()},y.onClickIndex=function(e){e.preventDefault();var t=a(this).index()*y.params.slidesPerGroup;y.params.loop&&(t+=y.loopedSlides),y.slideTo(t)},y.updateClickedSlide=function(e){var t=n(e,"."+y.params.slideClass),s=!1;if(t)for(var r=0;r<y.slides.length;r++)y.slides[r]===t&&(s=!0);if(!t||!s)return y.clickedSlide=void 0,void(y.clickedIndex=void 0);if(y.clickedSlide=t,y.clickedIndex=a(t).index(),y.params.slideToClickedSlide&&void 0!==y.clickedIndex&&y.clickedIndex!==y.activeIndex){var i,o=y.clickedIndex;if(y.params.loop){if(y.animating)return;i=a(y.clickedSlide).attr("data-swiper-slide-index"),y.params.centeredSlides?o<y.loopedSlides-y.params.slidesPerView/2||o>y.slides.length-y.loopedSlides+y.params.slidesPerView/2?(y.fixLoop(),o=y.wrapper.children("."+y.params.slideClass+'[data-swiper-slide-index="'+i+'"]:not(.swiper-slide-duplicate)').eq(0).index(),setTimeout(function(){y.slideTo(o)},0)):y.slideTo(o):o>y.slides.length-y.params.slidesPerView?(y.fixLoop(),o=y.wrapper.children("."+y.params.slideClass+'[data-swiper-slide-index="'+i+'"]:not(.swiper-slide-duplicate)').eq(0).index(),setTimeout(function(){y.slideTo(o)},0)):y.slideTo(o)}else y.slideTo(o)}};var T,S,C,z,M,P,I,k,E,B,D="input, select, textarea, button",L=Date.now(),H=[];y.animating=!1,y.touches={startX:0,startY:0,currentX:0,currentY:0,diff:0};var G,A;if(y.onTouchStart=function(e){if(e.originalEvent&&(e=e.originalEvent),G="touchstart"===e.type,G||!("which"in e)||3!==e.which){if(y.params.noSwiping&&n(e,"."+y.params.noSwipingClass))return void(y.allowClick=!0);if(!y.params.swipeHandler||n(e,y.params.swipeHandler)){var t=y.touches.currentX="touchstart"===e.type?e.targetTouches[0].pageX:e.pageX,s=y.touches.currentY="touchstart"===e.type?e.targetTouches[0].pageY:e.pageY;if(!(y.device.ios&&y.params.iOSEdgeSwipeDetection&&t<=y.params.iOSEdgeSwipeThreshold)){if(T=!0,S=!1,C=!0,M=void 0,A=void 0,y.touches.startX=t,y.touches.startY=s,z=Date.now(),y.allowClick=!0,y.updateContainerSize(),y.swipeDirection=void 0,y.params.threshold>0&&(k=!1),"touchstart"!==e.type){var r=!0;a(e.target).is(D)&&(r=!1),document.activeElement&&a(document.activeElement).is(D)&&document.activeElement.blur(),r&&e.preventDefault()}y.emit("onTouchStart",y,e)}}}},y.onTouchMove=function(e){if(e.originalEvent&&(e=e.originalEvent),!G||"mousemove"!==e.type){if(e.preventedByNestedSwiper)return y.touches.startX="touchmove"===e.type?e.targetTouches[0].pageX:e.pageX,void(y.touches.startY="touchmove"===e.type?e.targetTouches[0].pageY:e.pageY);if(y.params.onlyExternal)return y.allowClick=!1,void(T&&(y.touches.startX=y.touches.currentX="touchmove"===e.type?e.targetTouches[0].pageX:e.pageX,y.touches.startY=y.touches.currentY="touchmove"===e.type?e.targetTouches[0].pageY:e.pageY,z=Date.now()));if(G&&document.activeElement&&e.target===document.activeElement&&a(e.target).is(D))return S=!0,void(y.allowClick=!1);if(C&&y.emit("onTouchMove",y,e),!(e.targetTouches&&e.targetTouches.length>1)){if(y.touches.currentX="touchmove"===e.type?e.targetTouches[0].pageX:e.pageX,y.touches.currentY="touchmove"===e.type?e.targetTouches[0].pageY:e.pageY,"undefined"==typeof M){var t=180*Math.atan2(Math.abs(y.touches.currentY-y.touches.startY),Math.abs(y.touches.currentX-y.touches.startX))/Math.PI;M=y.isHorizontal()?t>y.params.touchAngle:90-t>y.params.touchAngle}if(M&&y.emit("onTouchMoveOpposite",y,e),"undefined"==typeof A&&y.browser.ieTouch&&(y.touches.currentX!==y.touches.startX||y.touches.currentY!==y.touches.startY)&&(A=!0),T){if(M)return void(T=!1);if(A||!y.browser.ieTouch){y.allowClick=!1,y.emit("onSliderMove",y,e),e.preventDefault(),y.params.touchMoveStopPropagation&&!y.params.nested&&e.stopPropagation(),S||(s.loop&&y.fixLoop(),I=y.getWrapperTranslate(),y.setWrapperTransition(0),y.animating&&y.wrapper.trigger("webkitTransitionEnd transitionend oTransitionEnd MSTransitionEnd msTransitionEnd"),y.params.autoplay&&y.autoplaying&&(y.params.autoplayDisableOnInteraction?y.stopAutoplay():y.pauseAutoplay()),B=!1,y.params.grabCursor&&(y.container[0].style.cursor="move",y.container[0].style.cursor="-webkit-grabbing",y.container[0].style.cursor="-moz-grabbin",y.container[0].style.cursor="grabbing")),S=!0;var r=y.touches.diff=y.isHorizontal()?y.touches.currentX-y.touches.startX:y.touches.currentY-y.touches.startY;r*=y.params.touchRatio,y.rtl&&(r=-r),y.swipeDirection=r>0?"prev":"next",P=r+I;var i=!0;if(r>0&&P>y.minTranslate()?(i=!1,y.params.resistance&&(P=y.minTranslate()-1+Math.pow(-y.minTranslate()+I+r,y.params.resistanceRatio))):0>r&&P<y.maxTranslate()&&(i=!1,y.params.resistance&&(P=y.maxTranslate()+1-Math.pow(y.maxTranslate()-I-r,y.params.resistanceRatio))),
i&&(e.preventedByNestedSwiper=!0),!y.params.allowSwipeToNext&&"next"===y.swipeDirection&&I>P&&(P=I),!y.params.allowSwipeToPrev&&"prev"===y.swipeDirection&&P>I&&(P=I),y.params.followFinger){if(y.params.threshold>0){if(!(Math.abs(r)>y.params.threshold||k))return void(P=I);if(!k)return k=!0,y.touches.startX=y.touches.currentX,y.touches.startY=y.touches.currentY,P=I,void(y.touches.diff=y.isHorizontal()?y.touches.currentX-y.touches.startX:y.touches.currentY-y.touches.startY)}(y.params.freeMode||y.params.watchSlidesProgress)&&y.updateActiveIndex(),y.params.freeMode&&(0===H.length&&H.push({position:y.touches[y.isHorizontal()?"startX":"startY"],time:z}),H.push({position:y.touches[y.isHorizontal()?"currentX":"currentY"],time:(new window.Date).getTime()})),y.updateProgress(P),y.setWrapperTranslate(P)}}}}}},y.onTouchEnd=function(e){if(e.originalEvent&&(e=e.originalEvent),C&&y.emit("onTouchEnd",y,e),C=!1,T){y.params.grabCursor&&S&&T&&(y.container[0].style.cursor="move",y.container[0].style.cursor="-webkit-grab",y.container[0].style.cursor="-moz-grab",y.container[0].style.cursor="grab");var t=Date.now(),s=t-z;if(y.allowClick&&(y.updateClickedSlide(e),y.emit("onTap",y,e),300>s&&t-L>300&&(E&&clearTimeout(E),E=setTimeout(function(){y&&(y.params.paginationHide&&y.paginationContainer.length>0&&!a(e.target).hasClass(y.params.bulletClass)&&y.paginationContainer.toggleClass(y.params.paginationHiddenClass),y.emit("onClick",y,e))},300)),300>s&&300>t-L&&(E&&clearTimeout(E),y.emit("onDoubleTap",y,e))),L=Date.now(),setTimeout(function(){y&&(y.allowClick=!0)},0),!T||!S||!y.swipeDirection||0===y.touches.diff||P===I)return void(T=S=!1);T=S=!1;var r;if(r=y.params.followFinger?y.rtl?y.translate:-y.translate:-P,y.params.freeMode){if(r<-y.minTranslate())return void y.slideTo(y.activeIndex);if(r>-y.maxTranslate())return void(y.slides.length<y.snapGrid.length?y.slideTo(y.snapGrid.length-1):y.slideTo(y.slides.length-1));if(y.params.freeModeMomentum){if(H.length>1){var i=H.pop(),n=H.pop(),o=i.position-n.position,l=i.time-n.time;y.velocity=o/l,y.velocity=y.velocity/2,Math.abs(y.velocity)<y.params.freeModeMinimumVelocity&&(y.velocity=0),(l>150||(new window.Date).getTime()-i.time>300)&&(y.velocity=0)}else y.velocity=0;H.length=0;var p=1e3*y.params.freeModeMomentumRatio,d=y.velocity*p,u=y.translate+d;y.rtl&&(u=-u);var c,m=!1,f=20*Math.abs(y.velocity)*y.params.freeModeMomentumBounceRatio;if(u<y.maxTranslate())y.params.freeModeMomentumBounce?(u+y.maxTranslate()<-f&&(u=y.maxTranslate()-f),c=y.maxTranslate(),m=!0,B=!0):u=y.maxTranslate();else if(u>y.minTranslate())y.params.freeModeMomentumBounce?(u-y.minTranslate()>f&&(u=y.minTranslate()+f),c=y.minTranslate(),m=!0,B=!0):u=y.minTranslate();else if(y.params.freeModeSticky){var g,h=0;for(h=0;h<y.snapGrid.length;h+=1)if(y.snapGrid[h]>-u){g=h;break}u=Math.abs(y.snapGrid[g]-u)<Math.abs(y.snapGrid[g-1]-u)||"next"===y.swipeDirection?y.snapGrid[g]:y.snapGrid[g-1],y.rtl||(u=-u)}if(0!==y.velocity)p=y.rtl?Math.abs((-u-y.translate)/y.velocity):Math.abs((u-y.translate)/y.velocity);else if(y.params.freeModeSticky)return void y.slideReset();y.params.freeModeMomentumBounce&&m?(y.updateProgress(c),y.setWrapperTransition(p),y.setWrapperTranslate(u),y.onTransitionStart(),y.animating=!0,y.wrapper.transitionEnd(function(){y&&B&&(y.emit("onMomentumBounce",y),y.setWrapperTransition(y.params.speed),y.setWrapperTranslate(c),y.wrapper.transitionEnd(function(){y&&y.onTransitionEnd()}))})):y.velocity?(y.updateProgress(u),y.setWrapperTransition(p),y.setWrapperTranslate(u),y.onTransitionStart(),y.animating||(y.animating=!0,y.wrapper.transitionEnd(function(){y&&y.onTransitionEnd()}))):y.updateProgress(u),y.updateActiveIndex()}return void((!y.params.freeModeMomentum||s>=y.params.longSwipesMs)&&(y.updateProgress(),y.updateActiveIndex()))}var v,w=0,b=y.slidesSizesGrid[0];for(v=0;v<y.slidesGrid.length;v+=y.params.slidesPerGroup)"undefined"!=typeof y.slidesGrid[v+y.params.slidesPerGroup]?r>=y.slidesGrid[v]&&r<y.slidesGrid[v+y.params.slidesPerGroup]&&(w=v,b=y.slidesGrid[v+y.params.slidesPerGroup]-y.slidesGrid[v]):r>=y.slidesGrid[v]&&(w=v,b=y.slidesGrid[y.slidesGrid.length-1]-y.slidesGrid[y.slidesGrid.length-2]);var x=(r-y.slidesGrid[w])/b;if(s>y.params.longSwipesMs){if(!y.params.longSwipes)return void y.slideTo(y.activeIndex);"next"===y.swipeDirection&&(x>=y.params.longSwipesRatio?y.slideTo(w+y.params.slidesPerGroup):y.slideTo(w)),"prev"===y.swipeDirection&&(x>1-y.params.longSwipesRatio?y.slideTo(w+y.params.slidesPerGroup):y.slideTo(w))}else{if(!y.params.shortSwipes)return void y.slideTo(y.activeIndex);"next"===y.swipeDirection&&y.slideTo(w+y.params.slidesPerGroup),"prev"===y.swipeDirection&&y.slideTo(w)}}},y._slideTo=function(e,a){return y.slideTo(e,a,!0,!0)},y.slideTo=function(e,a,t,s){"undefined"==typeof t&&(t=!0),"undefined"==typeof e&&(e=0),0>e&&(e=0),y.snapIndex=Math.floor(e/y.params.slidesPerGroup),y.snapIndex>=y.snapGrid.length&&(y.snapIndex=y.snapGrid.length-1);var r=-y.snapGrid[y.snapIndex];y.params.autoplay&&y.autoplaying&&(s||!y.params.autoplayDisableOnInteraction?y.pauseAutoplay(a):y.stopAutoplay()),y.updateProgress(r);for(var i=0;i<y.slidesGrid.length;i++)-Math.floor(100*r)>=Math.floor(100*y.slidesGrid[i])&&(e=i);return!y.params.allowSwipeToNext&&r<y.translate&&r<y.minTranslate()?!1:!y.params.allowSwipeToPrev&&r>y.translate&&r>y.maxTranslate()&&(y.activeIndex||0)!==e?!1:("undefined"==typeof a&&(a=y.params.speed),y.previousIndex=y.activeIndex||0,y.activeIndex=e,y.rtl&&-r===y.translate||!y.rtl&&r===y.translate?(y.params.autoHeight&&y.updateAutoHeight(),y.updateClasses(),"slide"!==y.params.effect&&y.setWrapperTranslate(r),!1):(y.updateClasses(),y.onTransitionStart(t),0===a?(y.setWrapperTranslate(r),y.setWrapperTransition(0),y.onTransitionEnd(t)):(y.setWrapperTranslate(r),y.setWrapperTransition(a),y.animating||(y.animating=!0,y.wrapper.transitionEnd(function(){y&&y.onTransitionEnd(t)}))),!0))},y.onTransitionStart=function(e){"undefined"==typeof e&&(e=!0),y.params.autoHeight&&y.updateAutoHeight(),y.lazy&&y.lazy.onTransitionStart(),e&&(y.emit("onTransitionStart",y),y.activeIndex!==y.previousIndex&&(y.emit("onSlideChangeStart",y),y.activeIndex>y.previousIndex?y.emit("onSlideNextStart",y):y.emit("onSlidePrevStart",y)))},y.onTransitionEnd=function(e){y.animating=!1,y.setWrapperTransition(0),"undefined"==typeof e&&(e=!0),y.lazy&&y.lazy.onTransitionEnd(),e&&(y.emit("onTransitionEnd",y),y.activeIndex!==y.previousIndex&&(y.emit("onSlideChangeEnd",y),y.activeIndex>y.previousIndex?y.emit("onSlideNextEnd",y):y.emit("onSlidePrevEnd",y))),y.params.hashnav&&y.hashnav&&y.hashnav.setHash()},y.slideNext=function(e,a,t){if(y.params.loop){if(y.animating)return!1;y.fixLoop();y.container[0].clientLeft;return y.slideTo(y.activeIndex+y.params.slidesPerGroup,a,e,t)}return y.slideTo(y.activeIndex+y.params.slidesPerGroup,a,e,t)},y._slideNext=function(e){return y.slideNext(!0,e,!0)},y.slidePrev=function(e,a,t){if(y.params.loop){if(y.animating)return!1;y.fixLoop();y.container[0].clientLeft;return y.slideTo(y.activeIndex-1,a,e,t)}return y.slideTo(y.activeIndex-1,a,e,t)},y._slidePrev=function(e){return y.slidePrev(!0,e,!0)},y.slideReset=function(e,a,t){return y.slideTo(y.activeIndex,a,e)},y.setWrapperTransition=function(e,a){y.wrapper.transition(e),"slide"!==y.params.effect&&y.effects[y.params.effect]&&y.effects[y.params.effect].setTransition(e),y.params.parallax&&y.parallax&&y.parallax.setTransition(e),y.params.scrollbar&&y.scrollbar&&y.scrollbar.setTransition(e),y.params.control&&y.controller&&y.controller.setTransition(e,a),y.emit("onSetTransition",y,e)},y.setWrapperTranslate=function(e,a,t){var s=0,i=0,n=0;y.isHorizontal()?s=y.rtl?-e:e:i=e,y.params.roundLengths&&(s=r(s),i=r(i)),y.params.virtualTranslate||(y.support.transforms3d?y.wrapper.transform("translate3d("+s+"px, "+i+"px, "+n+"px)"):y.wrapper.transform("translate("+s+"px, "+i+"px)")),y.translate=y.isHorizontal()?s:i;var o,l=y.maxTranslate()-y.minTranslate();o=0===l?0:(e-y.minTranslate())/l,o!==y.progress&&y.updateProgress(e),a&&y.updateActiveIndex(),"slide"!==y.params.effect&&y.effects[y.params.effect]&&y.effects[y.params.effect].setTranslate(y.translate),y.params.parallax&&y.parallax&&y.parallax.setTranslate(y.translate),y.params.scrollbar&&y.scrollbar&&y.scrollbar.setTranslate(y.translate),y.params.control&&y.controller&&y.controller.setTranslate(y.translate,t),y.emit("onSetTranslate",y,y.translate)},y.getTranslate=function(e,a){var t,s,r,i;return"undefined"==typeof a&&(a="x"),y.params.virtualTranslate?y.rtl?-y.translate:y.translate:(r=window.getComputedStyle(e,null),window.WebKitCSSMatrix?(s=r.transform||r.webkitTransform,s.split(",").length>6&&(s=s.split(", ").map(function(e){return e.replace(",",".")}).join(", ")),i=new window.WebKitCSSMatrix("none"===s?"":s)):(i=r.MozTransform||r.OTransform||r.MsTransform||r.msTransform||r.transform||r.getPropertyValue("transform").replace("translate(","matrix(1, 0, 0, 1,"),t=i.toString().split(",")),"x"===a&&(s=window.WebKitCSSMatrix?i.m41:16===t.length?parseFloat(t[12]):parseFloat(t[4])),"y"===a&&(s=window.WebKitCSSMatrix?i.m42:16===t.length?parseFloat(t[13]):parseFloat(t[5])),y.rtl&&s&&(s=-s),s||0)},y.getWrapperTranslate=function(e){return"undefined"==typeof e&&(e=y.isHorizontal()?"x":"y"),y.getTranslate(y.wrapper[0],e)},y.observers=[],y.initObservers=function(){if(y.params.observeParents)for(var e=y.container.parents(),a=0;a<e.length;a++)o(e[a]);o(y.container[0],{childList:!1}),o(y.wrapper[0],{attributes:!1})},y.disconnectObservers=function(){for(var e=0;e<y.observers.length;e++)y.observers[e].disconnect();y.observers=[]},y.createLoop=function(){y.wrapper.children("."+y.params.slideClass+"."+y.params.slideDuplicateClass).remove();var e=y.wrapper.children("."+y.params.slideClass);"auto"!==y.params.slidesPerView||y.params.loopedSlides||(y.params.loopedSlides=e.length),y.loopedSlides=parseInt(y.params.loopedSlides||y.params.slidesPerView,10),y.loopedSlides=y.loopedSlides+y.params.loopAdditionalSlides,y.loopedSlides>e.length&&(y.loopedSlides=e.length);var t,s=[],r=[];for(e.each(function(t,i){var n=a(this);t<y.loopedSlides&&r.push(i),t<e.length&&t>=e.length-y.loopedSlides&&s.push(i),n.attr("data-swiper-slide-index",t)}),t=0;t<r.length;t++)y.wrapper.append(a(r[t].cloneNode(!0)).addClass(y.params.slideDuplicateClass));for(t=s.length-1;t>=0;t--)y.wrapper.prepend(a(s[t].cloneNode(!0)).addClass(y.params.slideDuplicateClass))},y.destroyLoop=function(){y.wrapper.children("."+y.params.slideClass+"."+y.params.slideDuplicateClass).remove(),y.slides.removeAttr("data-swiper-slide-index")},y.reLoop=function(e){var a=y.activeIndex-y.loopedSlides;y.destroyLoop(),y.createLoop(),y.updateSlidesSize(),e&&y.slideTo(a+y.loopedSlides,0,!1)},y.fixLoop=function(){var e;y.activeIndex<y.loopedSlides?(e=y.slides.length-3*y.loopedSlides+y.activeIndex,e+=y.loopedSlides,y.slideTo(e,0,!1,!0)):("auto"===y.params.slidesPerView&&y.activeIndex>=2*y.loopedSlides||y.activeIndex>y.slides.length-2*y.params.slidesPerView)&&(e=-y.slides.length+y.activeIndex+y.loopedSlides,e+=y.loopedSlides,y.slideTo(e,0,!1,!0))},y.appendSlide=function(e){if(y.params.loop&&y.destroyLoop(),"object"==typeof e&&e.length)for(var a=0;a<e.length;a++)e[a]&&y.wrapper.append(e[a]);else y.wrapper.append(e);y.params.loop&&y.createLoop(),y.params.observer&&y.support.observer||y.update(!0)},y.prependSlide=function(e){y.params.loop&&y.destroyLoop();var a=y.activeIndex+1;if("object"==typeof e&&e.length){for(var t=0;t<e.length;t++)e[t]&&y.wrapper.prepend(e[t]);a=y.activeIndex+e.length}else y.wrapper.prepend(e);y.params.loop&&y.createLoop(),y.params.observer&&y.support.observer||y.update(!0),y.slideTo(a,0,!1)},y.removeSlide=function(e){y.params.loop&&(y.destroyLoop(),y.slides=y.wrapper.children("."+y.params.slideClass));var a,t=y.activeIndex;if("object"==typeof e&&e.length){for(var s=0;s<e.length;s++)a=e[s],y.slides[a]&&y.slides.eq(a).remove(),t>a&&t--;t=Math.max(t,0)}else a=e,y.slides[a]&&y.slides.eq(a).remove(),t>a&&t--,t=Math.max(t,0);y.params.loop&&y.createLoop(),y.params.observer&&y.support.observer||y.update(!0),y.params.loop?y.slideTo(t+y.loopedSlides,0,!1):y.slideTo(t,0,!1)},y.removeAllSlides=function(){for(var e=[],a=0;a<y.slides.length;a++)e.push(a);y.removeSlide(e)},y.effects={fade:{setTranslate:function(){for(var e=0;e<y.slides.length;e++){var a=y.slides.eq(e),t=a[0].swiperSlideOffset,s=-t;y.params.virtualTranslate||(s-=y.translate);var r=0;y.isHorizontal()||(r=s,s=0);var i=y.params.fade.crossFade?Math.max(1-Math.abs(a[0].progress),0):1+Math.min(Math.max(a[0].progress,-1),0);a.css({opacity:i}).transform("translate3d("+s+"px, "+r+"px, 0px)")}},setTransition:function(e){if(y.slides.transition(e),y.params.virtualTranslate&&0!==e){var a=!1;y.slides.transitionEnd(function(){if(!a&&y){a=!0,y.animating=!1;for(var e=["webkitTransitionEnd","transitionend","oTransitionEnd","MSTransitionEnd","msTransitionEnd"],t=0;t<e.length;t++)y.wrapper.trigger(e[t])}})}}},flip:{setTranslate:function(){for(var e=0;e<y.slides.length;e++){var t=y.slides.eq(e),s=t[0].progress;y.params.flip.limitRotation&&(s=Math.max(Math.min(t[0].progress,1),-1));var r=t[0].swiperSlideOffset,i=-180*s,n=i,o=0,l=-r,p=0;if(y.isHorizontal()?y.rtl&&(n=-n):(p=l,l=0,o=-n,n=0),t[0].style.zIndex=-Math.abs(Math.round(s))+y.slides.length,y.params.flip.slideShadows){var d=y.isHorizontal()?t.find(".swiper-slide-shadow-left"):t.find(".swiper-slide-shadow-top"),u=y.isHorizontal()?t.find(".swiper-slide-shadow-right"):t.find(".swiper-slide-shadow-bottom");0===d.length&&(d=a('<div class="swiper-slide-shadow-'+(y.isHorizontal()?"left":"top")+'"></div>'),t.append(d)),0===u.length&&(u=a('<div class="swiper-slide-shadow-'+(y.isHorizontal()?"right":"bottom")+'"></div>'),t.append(u)),d.length&&(d[0].style.opacity=Math.max(-s,0)),u.length&&(u[0].style.opacity=Math.max(s,0))}t.transform("translate3d("+l+"px, "+p+"px, 0px) rotateX("+o+"deg) rotateY("+n+"deg)")}},setTransition:function(e){if(y.slides.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e),y.params.virtualTranslate&&0!==e){var t=!1;y.slides.eq(y.activeIndex).transitionEnd(function(){if(!t&&y&&a(this).hasClass(y.params.slideActiveClass)){t=!0,y.animating=!1;for(var e=["webkitTransitionEnd","transitionend","oTransitionEnd","MSTransitionEnd","msTransitionEnd"],s=0;s<e.length;s++)y.wrapper.trigger(e[s])}})}}},cube:{setTranslate:function(){var e,t=0;y.params.cube.shadow&&(y.isHorizontal()?(e=y.wrapper.find(".swiper-cube-shadow"),0===e.length&&(e=a('<div class="swiper-cube-shadow"></div>'),y.wrapper.append(e)),e.css({height:y.width+"px"})):(e=y.container.find(".swiper-cube-shadow"),0===e.length&&(e=a('<div class="swiper-cube-shadow"></div>'),y.container.append(e))));for(var s=0;s<y.slides.length;s++){var r=y.slides.eq(s),i=90*s,n=Math.floor(i/360);y.rtl&&(i=-i,n=Math.floor(-i/360));var o=Math.max(Math.min(r[0].progress,1),-1),l=0,p=0,d=0;s%4===0?(l=4*-n*y.size,d=0):(s-1)%4===0?(l=0,d=4*-n*y.size):(s-2)%4===0?(l=y.size+4*n*y.size,d=y.size):(s-3)%4===0&&(l=-y.size,d=3*y.size+4*y.size*n),y.rtl&&(l=-l),y.isHorizontal()||(p=l,l=0);var u="rotateX("+(y.isHorizontal()?0:-i)+"deg) rotateY("+(y.isHorizontal()?i:0)+"deg) translate3d("+l+"px, "+p+"px, "+d+"px)";if(1>=o&&o>-1&&(t=90*s+90*o,y.rtl&&(t=90*-s-90*o)),r.transform(u),y.params.cube.slideShadows){var c=y.isHorizontal()?r.find(".swiper-slide-shadow-left"):r.find(".swiper-slide-shadow-top"),m=y.isHorizontal()?r.find(".swiper-slide-shadow-right"):r.find(".swiper-slide-shadow-bottom");0===c.length&&(c=a('<div class="swiper-slide-shadow-'+(y.isHorizontal()?"left":"top")+'"></div>'),r.append(c)),0===m.length&&(m=a('<div class="swiper-slide-shadow-'+(y.isHorizontal()?"right":"bottom")+'"></div>'),r.append(m)),c.length&&(c[0].style.opacity=Math.max(-o,0)),m.length&&(m[0].style.opacity=Math.max(o,0))}}if(y.wrapper.css({"-webkit-transform-origin":"50% 50% -"+y.size/2+"px","-moz-transform-origin":"50% 50% -"+y.size/2+"px","-ms-transform-origin":"50% 50% -"+y.size/2+"px","transform-origin":"50% 50% -"+y.size/2+"px"}),y.params.cube.shadow)if(y.isHorizontal())e.transform("translate3d(0px, "+(y.width/2+y.params.cube.shadowOffset)+"px, "+-y.width/2+"px) rotateX(90deg) rotateZ(0deg) scale("+y.params.cube.shadowScale+")");else{var f=Math.abs(t)-90*Math.floor(Math.abs(t)/90),g=1.5-(Math.sin(2*f*Math.PI/360)/2+Math.cos(2*f*Math.PI/360)/2),h=y.params.cube.shadowScale,v=y.params.cube.shadowScale/g,w=y.params.cube.shadowOffset;e.transform("scale3d("+h+", 1, "+v+") translate3d(0px, "+(y.height/2+w)+"px, "+-y.height/2/v+"px) rotateX(-90deg)")}var b=y.isSafari||y.isUiWebView?-y.size/2:0;y.wrapper.transform("translate3d(0px,0,"+b+"px) rotateX("+(y.isHorizontal()?0:t)+"deg) rotateY("+(y.isHorizontal()?-t:0)+"deg)")},setTransition:function(e){y.slides.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e),y.params.cube.shadow&&!y.isHorizontal()&&y.container.find(".swiper-cube-shadow").transition(e)}},coverflow:{setTranslate:function(){for(var e=y.translate,t=y.isHorizontal()?-e+y.width/2:-e+y.height/2,s=y.isHorizontal()?y.params.coverflow.rotate:-y.params.coverflow.rotate,r=y.params.coverflow.depth,i=0,n=y.slides.length;n>i;i++){var o=y.slides.eq(i),l=y.slidesSizesGrid[i],p=o[0].swiperSlideOffset,d=(t-p-l/2)/l*y.params.coverflow.modifier,u=y.isHorizontal()?s*d:0,c=y.isHorizontal()?0:s*d,m=-r*Math.abs(d),f=y.isHorizontal()?0:y.params.coverflow.stretch*d,g=y.isHorizontal()?y.params.coverflow.stretch*d:0;Math.abs(g)<.001&&(g=0),Math.abs(f)<.001&&(f=0),Math.abs(m)<.001&&(m=0),Math.abs(u)<.001&&(u=0),Math.abs(c)<.001&&(c=0);var h="translate3d("+g+"px,"+f+"px,"+m+"px)  rotateX("+c+"deg) rotateY("+u+"deg)";if(o.transform(h),o[0].style.zIndex=-Math.abs(Math.round(d))+1,y.params.coverflow.slideShadows){var v=y.isHorizontal()?o.find(".swiper-slide-shadow-left"):o.find(".swiper-slide-shadow-top"),w=y.isHorizontal()?o.find(".swiper-slide-shadow-right"):o.find(".swiper-slide-shadow-bottom");0===v.length&&(v=a('<div class="swiper-slide-shadow-'+(y.isHorizontal()?"left":"top")+'"></div>'),o.append(v)),0===w.length&&(w=a('<div class="swiper-slide-shadow-'+(y.isHorizontal()?"right":"bottom")+'"></div>'),o.append(w)),v.length&&(v[0].style.opacity=d>0?d:0),w.length&&(w[0].style.opacity=-d>0?-d:0)}}if(y.browser.ie){var b=y.wrapper[0].style;b.perspectiveOrigin=t+"px 50%"}},setTransition:function(e){y.slides.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e)}}},y.lazy={initialImageLoaded:!1,loadImageInSlide:function(e,t){if("undefined"!=typeof e&&("undefined"==typeof t&&(t=!0),0!==y.slides.length)){var s=y.slides.eq(e),r=s.find(".swiper-lazy:not(.swiper-lazy-loaded):not(.swiper-lazy-loading)");!s.hasClass("swiper-lazy")||s.hasClass("swiper-lazy-loaded")||s.hasClass("swiper-lazy-loading")||(r=r.add(s[0])),0!==r.length&&r.each(function(){var e=a(this);e.addClass("swiper-lazy-loading");var r=e.attr("data-background"),i=e.attr("data-src"),n=e.attr("data-srcset");y.loadImage(e[0],i||r,n,!1,function(){if(r?(e.css("background-image",'url("'+r+'")'),e.removeAttr("data-background")):(n&&(e.attr("srcset",n),e.removeAttr("data-srcset")),i&&(e.attr("src",i),e.removeAttr("data-src"))),e.addClass("swiper-lazy-loaded").removeClass("swiper-lazy-loading"),s.find(".swiper-lazy-preloader, .preloader").remove(),y.params.loop&&t){var a=s.attr("data-swiper-slide-index");if(s.hasClass(y.params.slideDuplicateClass)){var o=y.wrapper.children('[data-swiper-slide-index="'+a+'"]:not(.'+y.params.slideDuplicateClass+")");y.lazy.loadImageInSlide(o.index(),!1)}else{var l=y.wrapper.children("."+y.params.slideDuplicateClass+'[data-swiper-slide-index="'+a+'"]');y.lazy.loadImageInSlide(l.index(),!1)}}y.emit("onLazyImageReady",y,s[0],e[0])}),y.emit("onLazyImageLoad",y,s[0],e[0])})}},load:function(){var e;if(y.params.watchSlidesVisibility)y.wrapper.children("."+y.params.slideVisibleClass).each(function(){y.lazy.loadImageInSlide(a(this).index())});else if(y.params.slidesPerView>1)for(e=y.activeIndex;e<y.activeIndex+y.params.slidesPerView;e++)y.slides[e]&&y.lazy.loadImageInSlide(e);else y.lazy.loadImageInSlide(y.activeIndex);if(y.params.lazyLoadingInPrevNext)if(y.params.slidesPerView>1||y.params.lazyLoadingInPrevNextAmount&&y.params.lazyLoadingInPrevNextAmount>1){var t=y.params.lazyLoadingInPrevNextAmount,s=y.params.slidesPerView,r=Math.min(y.activeIndex+s+Math.max(t,s),y.slides.length),i=Math.max(y.activeIndex-Math.max(s,t),0);for(e=y.activeIndex+y.params.slidesPerView;r>e;e++)y.slides[e]&&y.lazy.loadImageInSlide(e);for(e=i;e<y.activeIndex;e++)y.slides[e]&&y.lazy.loadImageInSlide(e)}else{var n=y.wrapper.children("."+y.params.slideNextClass);n.length>0&&y.lazy.loadImageInSlide(n.index());var o=y.wrapper.children("."+y.params.slidePrevClass);o.length>0&&y.lazy.loadImageInSlide(o.index())}},onTransitionStart:function(){y.params.lazyLoading&&(y.params.lazyLoadingOnTransitionStart||!y.params.lazyLoadingOnTransitionStart&&!y.lazy.initialImageLoaded)&&y.lazy.load()},onTransitionEnd:function(){y.params.lazyLoading&&!y.params.lazyLoadingOnTransitionStart&&y.lazy.load()}},y.scrollbar={isTouched:!1,setDragPosition:function(e){var a=y.scrollbar,t=y.isHorizontal()?"touchstart"===e.type||"touchmove"===e.type?e.targetTouches[0].pageX:e.pageX||e.clientX:"touchstart"===e.type||"touchmove"===e.type?e.targetTouches[0].pageY:e.pageY||e.clientY,s=t-a.track.offset()[y.isHorizontal()?"left":"top"]-a.dragSize/2,r=-y.minTranslate()*a.moveDivider,i=-y.maxTranslate()*a.moveDivider;r>s?s=r:s>i&&(s=i),s=-s/a.moveDivider,y.updateProgress(s),y.setWrapperTranslate(s,!0)},dragStart:function(e){var a=y.scrollbar;a.isTouched=!0,e.preventDefault(),e.stopPropagation(),a.setDragPosition(e),clearTimeout(a.dragTimeout),a.track.transition(0),y.params.scrollbarHide&&a.track.css("opacity",1),y.wrapper.transition(100),a.drag.transition(100),y.emit("onScrollbarDragStart",y)},dragMove:function(e){var a=y.scrollbar;a.isTouched&&(e.preventDefault?e.preventDefault():e.returnValue=!1,a.setDragPosition(e),y.wrapper.transition(0),a.track.transition(0),a.drag.transition(0),y.emit("onScrollbarDragMove",y))},dragEnd:function(e){var a=y.scrollbar;a.isTouched&&(a.isTouched=!1,y.params.scrollbarHide&&(clearTimeout(a.dragTimeout),a.dragTimeout=setTimeout(function(){a.track.css("opacity",0),a.track.transition(400)},1e3)),y.emit("onScrollbarDragEnd",y),y.params.scrollbarSnapOnRelease&&y.slideReset())},enableDraggable:function(){var e=y.scrollbar,t=y.support.touch?e.track:document;a(e.track).on(y.touchEvents.start,e.dragStart),a(t).on(y.touchEvents.move,e.dragMove),a(t).on(y.touchEvents.end,e.dragEnd)},disableDraggable:function(){var e=y.scrollbar,t=y.support.touch?e.track:document;a(e.track).off(y.touchEvents.start,e.dragStart),a(t).off(y.touchEvents.move,e.dragMove),a(t).off(y.touchEvents.end,e.dragEnd)},set:function(){if(y.params.scrollbar){var e=y.scrollbar;e.track=a(y.params.scrollbar),y.params.uniqueNavElements&&"string"==typeof y.params.scrollbar&&e.track.length>1&&1===y.container.find(y.params.scrollbar).length&&(e.track=y.container.find(y.params.scrollbar)),e.drag=e.track.find(".swiper-scrollbar-drag"),0===e.drag.length&&(e.drag=a('<div class="swiper-scrollbar-drag"></div>'),e.track.append(e.drag)),e.drag[0].style.width="",e.drag[0].style.height="",e.trackSize=y.isHorizontal()?e.track[0].offsetWidth:e.track[0].offsetHeight,e.divider=y.size/y.virtualSize,e.moveDivider=e.divider*(e.trackSize/y.size),e.dragSize=e.trackSize*e.divider,y.isHorizontal()?e.drag[0].style.width=e.dragSize+"px":e.drag[0].style.height=e.dragSize+"px",e.divider>=1?e.track[0].style.display="none":e.track[0].style.display="",y.params.scrollbarHide&&(e.track[0].style.opacity=0)}},setTranslate:function(){if(y.params.scrollbar){var e,a=y.scrollbar,t=(y.translate||0,a.dragSize);e=(a.trackSize-a.dragSize)*y.progress,y.rtl&&y.isHorizontal()?(e=-e,e>0?(t=a.dragSize-e,e=0):-e+a.dragSize>a.trackSize&&(t=a.trackSize+e)):0>e?(t=a.dragSize+e,e=0):e+a.dragSize>a.trackSize&&(t=a.trackSize-e),y.isHorizontal()?(y.support.transforms3d?a.drag.transform("translate3d("+e+"px, 0, 0)"):a.drag.transform("translateX("+e+"px)"),a.drag[0].style.width=t+"px"):(y.support.transforms3d?a.drag.transform("translate3d(0px, "+e+"px, 0)"):a.drag.transform("translateY("+e+"px)"),a.drag[0].style.height=t+"px"),y.params.scrollbarHide&&(clearTimeout(a.timeout),a.track[0].style.opacity=1,a.timeout=setTimeout(function(){a.track[0].style.opacity=0,a.track.transition(400)},1e3))}},setTransition:function(e){y.params.scrollbar&&y.scrollbar.drag.transition(e)}},y.controller={LinearSpline:function(e,a){this.x=e,this.y=a,this.lastIndex=e.length-1;var t,s;this.x.length;this.interpolate=function(e){return e?(s=r(this.x,e),t=s-1,(e-this.x[t])*(this.y[s]-this.y[t])/(this.x[s]-this.x[t])+this.y[t]):0};var r=function(){var e,a,t;return function(s,r){for(a=-1,e=s.length;e-a>1;)s[t=e+a>>1]<=r?a=t:e=t;return e}}()},getInterpolateFunction:function(e){y.controller.spline||(y.controller.spline=y.params.loop?new y.controller.LinearSpline(y.slidesGrid,e.slidesGrid):new y.controller.LinearSpline(y.snapGrid,e.snapGrid))},setTranslate:function(e,a){function s(a){e=a.rtl&&"horizontal"===a.params.direction?-y.translate:y.translate,"slide"===y.params.controlBy&&(y.controller.getInterpolateFunction(a),i=-y.controller.spline.interpolate(-e)),i&&"container"!==y.params.controlBy||(r=(a.maxTranslate()-a.minTranslate())/(y.maxTranslate()-y.minTranslate()),i=(e-y.minTranslate())*r+a.minTranslate()),y.params.controlInverse&&(i=a.maxTranslate()-i),a.updateProgress(i),a.setWrapperTranslate(i,!1,y),a.updateActiveIndex()}var r,i,n=y.params.control;if(y.isArray(n))for(var o=0;o<n.length;o++)n[o]!==a&&n[o]instanceof t&&s(n[o]);else n instanceof t&&a!==n&&s(n)},setTransition:function(e,a){function s(a){a.setWrapperTransition(e,y),0!==e&&(a.onTransitionStart(),a.wrapper.transitionEnd(function(){i&&(a.params.loop&&"slide"===y.params.controlBy&&a.fixLoop(),a.onTransitionEnd())}))}var r,i=y.params.control;if(y.isArray(i))for(r=0;r<i.length;r++)i[r]!==a&&i[r]instanceof t&&s(i[r]);else i instanceof t&&a!==i&&s(i)}},y.hashnav={init:function(){if(y.params.hashnav){y.hashnav.initialized=!0;var e=document.location.hash.replace("#","");if(e)for(var a=0,t=0,s=y.slides.length;s>t;t++){var r=y.slides.eq(t),i=r.attr("data-hash");if(i===e&&!r.hasClass(y.params.slideDuplicateClass)){var n=r.index();y.slideTo(n,a,y.params.runCallbacksOnInit,!0)}}}},setHash:function(){y.hashnav.initialized&&y.params.hashnav&&(document.location.hash=y.slides.eq(y.activeIndex).attr("data-hash")||"")}},y.disableKeyboardControl=function(){y.params.keyboardControl=!1,a(document).off("keydown",l)},y.enableKeyboardControl=function(){y.params.keyboardControl=!0,a(document).on("keydown",l)},y.mousewheel={event:!1,lastScrollTime:(new window.Date).getTime()},y.params.mousewheelControl){try{new window.WheelEvent("wheel"),y.mousewheel.event="wheel"}catch(O){(window.WheelEvent||y.container[0]&&"wheel"in y.container[0])&&(y.mousewheel.event="wheel")}!y.mousewheel.event&&window.WheelEvent,y.mousewheel.event||void 0===document.onmousewheel||(y.mousewheel.event="mousewheel"),y.mousewheel.event||(y.mousewheel.event="DOMMouseScroll")}y.disableMousewheelControl=function(){return y.mousewheel.event?(y.container.off(y.mousewheel.event,p),!0):!1},y.enableMousewheelControl=function(){return y.mousewheel.event?(y.container.on(y.mousewheel.event,p),!0):!1},y.parallax={setTranslate:function(){y.container.children("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function(){d(this,y.progress)}),y.slides.each(function(){var e=a(this);e.find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function(){var a=Math.min(Math.max(e[0].progress,-1),1);d(this,a)})})},setTransition:function(e){"undefined"==typeof e&&(e=y.params.speed),y.container.find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function(){var t=a(this),s=parseInt(t.attr("data-swiper-parallax-duration"),10)||e;0===e&&(s=0),t.transition(s)})}},y._plugins=[];for(var N in y.plugins){var R=y.plugins[N](y,y.params[N]);R&&y._plugins.push(R)}return y.callPlugins=function(e){for(var a=0;a<y._plugins.length;a++)e in y._plugins[a]&&y._plugins[a][e](arguments[1],arguments[2],arguments[3],arguments[4],arguments[5])},y.emitterEventListeners={},y.emit=function(e){y.params[e]&&y.params[e](arguments[1],arguments[2],arguments[3],arguments[4],arguments[5]);var a;if(y.emitterEventListeners[e])for(a=0;a<y.emitterEventListeners[e].length;a++)y.emitterEventListeners[e][a](arguments[1],arguments[2],arguments[3],arguments[4],arguments[5]);y.callPlugins&&y.callPlugins(e,arguments[1],arguments[2],arguments[3],arguments[4],arguments[5])},y.on=function(e,a){return e=u(e),y.emitterEventListeners[e]||(y.emitterEventListeners[e]=[]),y.emitterEventListeners[e].push(a),y},y.off=function(e,a){var t;if(e=u(e),"undefined"==typeof a)return y.emitterEventListeners[e]=[],y;if(y.emitterEventListeners[e]&&0!==y.emitterEventListeners[e].length){for(t=0;t<y.emitterEventListeners[e].length;t++)y.emitterEventListeners[e][t]===a&&y.emitterEventListeners[e].splice(t,1);return y}},y.once=function(e,a){e=u(e);var t=function(){a(arguments[0],arguments[1],arguments[2],arguments[3],arguments[4]),y.off(e,t)};return y.on(e,t),y},y.a11y={makeFocusable:function(e){return e.attr("tabIndex","0"),e},addRole:function(e,a){return e.attr("role",a),e},addLabel:function(e,a){return e.attr("aria-label",a),e},disable:function(e){return e.attr("aria-disabled",!0),e},enable:function(e){return e.attr("aria-disabled",!1),e},onEnterKey:function(e){13===e.keyCode&&(a(e.target).is(y.params.nextButton)?(y.onClickNext(e),y.isEnd?y.a11y.notify(y.params.lastSlideMessage):y.a11y.notify(y.params.nextSlideMessage)):a(e.target).is(y.params.prevButton)&&(y.onClickPrev(e),y.isBeginning?y.a11y.notify(y.params.firstSlideMessage):y.a11y.notify(y.params.prevSlideMessage)),a(e.target).is("."+y.params.bulletClass)&&a(e.target)[0].click())},liveRegion:a('<span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>'),notify:function(e){var a=y.a11y.liveRegion;0!==a.length&&(a.html(""),a.html(e))},init:function(){y.params.nextButton&&y.nextButton&&y.nextButton.length>0&&(y.a11y.makeFocusable(y.nextButton),y.a11y.addRole(y.nextButton,"button"),y.a11y.addLabel(y.nextButton,y.params.nextSlideMessage)),y.params.prevButton&&y.prevButton&&y.prevButton.length>0&&(y.a11y.makeFocusable(y.prevButton),y.a11y.addRole(y.prevButton,"button"),y.a11y.addLabel(y.prevButton,y.params.prevSlideMessage)),a(y.container).append(y.a11y.liveRegion)},initPagination:function(){y.params.pagination&&y.params.paginationClickable&&y.bullets&&y.bullets.length&&y.bullets.each(function(){var e=a(this);y.a11y.makeFocusable(e),y.a11y.addRole(e,"button"),y.a11y.addLabel(e,y.params.paginationBulletMessage.replace(/{{index}}/,e.index()+1))})},destroy:function(){y.a11y.liveRegion&&y.a11y.liveRegion.length>0&&y.a11y.liveRegion.remove()}},y.init=function(){y.params.loop&&y.createLoop(),y.updateContainerSize(),y.updateSlidesSize(),y.updatePagination(),y.params.scrollbar&&y.scrollbar&&(y.scrollbar.set(),y.params.scrollbarDraggable&&y.scrollbar.enableDraggable()),"slide"!==y.params.effect&&y.effects[y.params.effect]&&(y.params.loop||y.updateProgress(),y.effects[y.params.effect].setTranslate()),y.params.loop?y.slideTo(y.params.initialSlide+y.loopedSlides,0,y.params.runCallbacksOnInit):(y.slideTo(y.params.initialSlide,0,y.params.runCallbacksOnInit),0===y.params.initialSlide&&(y.parallax&&y.params.parallax&&y.parallax.setTranslate(),y.lazy&&y.params.lazyLoading&&(y.lazy.load(),y.lazy.initialImageLoaded=!0))),y.attachEvents(),y.params.observer&&y.support.observer&&y.initObservers(),y.params.preloadImages&&!y.params.lazyLoading&&y.preloadImages(),y.params.autoplay&&y.startAutoplay(),y.params.keyboardControl&&y.enableKeyboardControl&&y.enableKeyboardControl(),y.params.mousewheelControl&&y.enableMousewheelControl&&y.enableMousewheelControl(),
y.params.hashnav&&y.hashnav&&y.hashnav.init(),y.params.a11y&&y.a11y&&y.a11y.init(),y.emit("onInit",y)},y.cleanupStyles=function(){y.container.removeClass(y.classNames.join(" ")).removeAttr("style"),y.wrapper.removeAttr("style"),y.slides&&y.slides.length&&y.slides.removeClass([y.params.slideVisibleClass,y.params.slideActiveClass,y.params.slideNextClass,y.params.slidePrevClass].join(" ")).removeAttr("style").removeAttr("data-swiper-column").removeAttr("data-swiper-row"),y.paginationContainer&&y.paginationContainer.length&&y.paginationContainer.removeClass(y.params.paginationHiddenClass),y.bullets&&y.bullets.length&&y.bullets.removeClass(y.params.bulletActiveClass),y.params.prevButton&&a(y.params.prevButton).removeClass(y.params.buttonDisabledClass),y.params.nextButton&&a(y.params.nextButton).removeClass(y.params.buttonDisabledClass),y.params.scrollbar&&y.scrollbar&&(y.scrollbar.track&&y.scrollbar.track.length&&y.scrollbar.track.removeAttr("style"),y.scrollbar.drag&&y.scrollbar.drag.length&&y.scrollbar.drag.removeAttr("style"))},y.destroy=function(e,a){y.detachEvents(),y.stopAutoplay(),y.params.scrollbar&&y.scrollbar&&y.params.scrollbarDraggable&&y.scrollbar.disableDraggable(),y.params.loop&&y.destroyLoop(),a&&y.cleanupStyles(),y.disconnectObservers(),y.params.keyboardControl&&y.disableKeyboardControl&&y.disableKeyboardControl(),y.params.mousewheelControl&&y.disableMousewheelControl&&y.disableMousewheelControl(),y.params.a11y&&y.a11y&&y.a11y.destroy(),y.emit("onDestroy"),e!==!1&&(y=null)},y.init(),y}};t.prototype={isSafari:function(){var e=navigator.userAgent.toLowerCase();return e.indexOf("safari")>=0&&e.indexOf("chrome")<0&&e.indexOf("android")<0}(),isUiWebView:/(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(navigator.userAgent),isArray:function(e){return"[object Array]"===Object.prototype.toString.apply(e)},browser:{ie:window.navigator.pointerEnabled||window.navigator.msPointerEnabled,ieTouch:window.navigator.msPointerEnabled&&window.navigator.msMaxTouchPoints>1||window.navigator.pointerEnabled&&window.navigator.maxTouchPoints>1},device:function(){var e=navigator.userAgent,a=e.match(/(Android);?[\s\/]+([\d.]+)?/),t=e.match(/(iPad).*OS\s([\d_]+)/),s=e.match(/(iPod)(.*OS\s([\d_]+))?/),r=!t&&e.match(/(iPhone\sOS)\s([\d_]+)/);return{ios:t||r||s,android:a}}(),support:{touch:window.Modernizr&&Modernizr.touch===!0||function(){return!!("ontouchstart"in window||window.DocumentTouch&&document instanceof DocumentTouch)}(),transforms3d:window.Modernizr&&Modernizr.csstransforms3d===!0||function(){var e=document.createElement("div").style;return"webkitPerspective"in e||"MozPerspective"in e||"OPerspective"in e||"MsPerspective"in e||"perspective"in e}(),flexbox:function(){for(var e=document.createElement("div").style,a="alignItems webkitAlignItems webkitBoxAlign msFlexAlign mozBoxAlign webkitFlexDirection msFlexDirection mozBoxDirection mozBoxOrient webkitBoxDirection webkitBoxOrient".split(" "),t=0;t<a.length;t++)if(a[t]in e)return!0}(),observer:function(){return"MutationObserver"in window||"WebkitMutationObserver"in window}()},plugins:{}};for(var s=["jQuery","Zepto","Dom7"],r=0;r<s.length;r++)window[s[r]]&&e(window[s[r]]);var i;i="undefined"==typeof Dom7?window.Dom7||window.Zepto||window.jQuery:Dom7,i&&("transitionEnd"in i.fn||(i.fn.transitionEnd=function(e){function a(i){if(i.target===this)for(e.call(this,i),t=0;t<s.length;t++)r.off(s[t],a)}var t,s=["webkitTransitionEnd","transitionend","oTransitionEnd","MSTransitionEnd","msTransitionEnd"],r=this;if(e)for(t=0;t<s.length;t++)r.on(s[t],a);return this}),"transform"in i.fn||(i.fn.transform=function(e){for(var a=0;a<this.length;a++){var t=this[a].style;t.webkitTransform=t.MsTransform=t.msTransform=t.MozTransform=t.OTransform=t.transform=e}return this}),"transition"in i.fn||(i.fn.transition=function(e){"string"!=typeof e&&(e+="ms");for(var a=0;a<this.length;a++){var t=this[a].style;t.webkitTransitionDuration=t.MsTransitionDuration=t.msTransitionDuration=t.MozTransitionDuration=t.OTransitionDuration=t.transitionDuration=e}return this})),window.Swiper=t}(),"undefined"!=typeof module?module.exports=window.Swiper:"function"==typeof define&&define.amd&&define([],function(){"use strict";return window.Swiper});
//# sourceMappingURL=maps/swiper.jquery.min.js.map

// fin swiper 

/*global jQuery */
/*!
* FitText.js 1.2
*
* Copyright 2011, Dave Rupert http://daverupert.com
* Released under the WTFPL license
* http://sam.zoy.org/wtfpl/
*
* Date: Thu May 05 14:23:00 2011 -0600
*/

(function( $ ){

  $.fn.fitText = function( kompressor, options ) {

    // Setup options
    var compressor = kompressor || 1,
        settings = $.extend({
          'minFontSize' : Number.NEGATIVE_INFINITY,
          'maxFontSize' : $(this).css('font-size')
        }, options);

    return this.each(function(){

      // Store the object
      var $this = $(this);

      // Resizer() resizes items based on the object width divided by the compressor * 10
      var resizer = function () {
        $this.css('font-size', Math.max(Math.min($this.width() / (compressor*10), parseFloat(settings.maxFontSize)), parseFloat(settings.minFontSize)));
      };

      // Call once to set.
      resizer();

      // Call on resize. Opera debounces their resize by default.
      $(window).on('resize.fittext orientationchange.fittext', resizer);

    });

  };

})( jQuery );

// fin fittext

!function(a){a.fn.hoverIntent=function(b,c,d){var e={interval:100,sensitivity:6,timeout:0};e="object"==typeof b?a.extend(e,b):a.isFunction(c)?a.extend(e,{over:b,out:c,selector:d}):a.extend(e,{over:b,out:b,selector:c});var f,g,h,i,j=function(a){f=a.pageX,g=a.pageY},k=function(b,c){return c.hoverIntent_t=clearTimeout(c.hoverIntent_t),Math.sqrt((h-f)*(h-f)+(i-g)*(i-g))<e.sensitivity?(a(c).off("mousemove.hoverIntent",j),c.hoverIntent_s=!0,e.over.apply(c,[b])):(h=f,i=g,c.hoverIntent_t=setTimeout(function(){k(b,c)},e.interval),void 0)},l=function(a,b){return b.hoverIntent_t=clearTimeout(b.hoverIntent_t),b.hoverIntent_s=!1,e.out.apply(b,[a])},m=function(b){var c=a.extend({},b),d=this;d.hoverIntent_t&&(d.hoverIntent_t=clearTimeout(d.hoverIntent_t)),"mouseenter"===b.type?(h=c.pageX,i=c.pageY,a(d).on("mousemove.hoverIntent",j),d.hoverIntent_s||(d.hoverIntent_t=setTimeout(function(){k(c,d)},e.interval))):(a(d).off("mousemove.hoverIntent",j),d.hoverIntent_s&&(d.hoverIntent_t=setTimeout(function(){l(c,d)},e.timeout)))};return this.on({"mouseenter.hoverIntent":m,"mouseleave.hoverIntent":m},e.selector)}}(jQuery);



// fin hover intent 

/* UItoTop jQuery Plugin | Matt Varone | http://www.mattvarone.com/web-design/uitotop-jquery-plugin */
!function(n){n.fn.UItoTop=function(o){var e={text:"To Top",min:200,inDelay:600,outDelay:400,containerID:"toTop",containerHoverID:"toTopHover",scrollSpeed:1200,easingType:"linear"},t=n.extend(e,o),i="#"+t.containerID,a="#"+t.containerHoverID;n("body").append('<a href="#" id="'+t.containerID+'"><span>'+t.text+"</span></a>"),n(i).hide().on("click.UItoTop",function(){return n("html, body").animate({scrollTop:0},t.scrollSpeed,t.easingType),n("#"+t.containerHoverID,this).stop().animate({opacity:0},t.inDelay,t.easingType),!1}).hover(function(){n(a,this).stop().animate({opacity:1},600,"linear")},function(){n(a,this).stop().animate({opacity:0},700,"linear")}),n(window).scroll(function(){var o=n(window).scrollTop();"undefined"==typeof document.body.style.maxHeight&&n(i).css({position:"absolute",top:o+n(window).height()-50}),o>t.min?n(i).fadeIn(t.inDelay):n(i).fadeOut(t.Outdelay)})}}(jQuery);
/* jquery.stickup.min.js */
!function(t){t.fn.stickUp=function(e){function o(){a=parseInt(n.offset().top),d=parseInt(n.css("margin-top")),u=parseInt(n.outerHeight(!0)),c.pseudo&&(t('<div class="pseudoStickyBlock"></div>').insertAfter(n),i=t(".pseudoStickyBlock"),i.css({position:"relative",display:"block"})),c.active&&s(),n.addClass("stuckMenu")}function s(){p.on("scroll.stickUp",function(){r=t(this).scrollTop(),S=r>k?"down":"up",k=r,0!=h.length?correctionValue=h.outerHeight(!0):correctionValue=0,f=parseInt(l.scrollTop()),a-correctionValue<f?(n.addClass("isStuck"),v.addClass("isStuck"),c.pseudo?(n.css({position:"fixed",top:correctionValue}),i.css({height:u})):n.css({position:"fixed",top:correctionValue})):(n.removeClass("isStuck"),v.removeClass("isStuck"),c.pseudo?(n.css({position:"relative",top:0}),i.css({height:0})):n.css({position:"absolute",top:0}))}).trigger("scroll.stickUp"),p.on("resize",function(){n.hasClass("isStuck")?a!=parseInt(i.offset().top)&&(a=parseInt(i.offset().top)):a!=parseInt(n.offset().top)&&(a=parseInt(n.offset().top))})}var c={correctionSelector:".correctionSelector",listenSelector:".listenSelector",active:!1,pseudo:!0};t.extend(c,e);var i,r,n=t(this),l=t(window),p=t(document),a=0,u=0,d=0,f=0,k=0,S="",h=t(c.correctionSelector),v=t(c.listenSelector);0!=n.length&&o()}}(jQuery);
/* super-guacamole.min.js */
!function(e,t){function n(t){var n,i,r=this;n={id:"",href:"",title:"&middot;&middot;&middot;",children:{},templates:{},container:null},i=e.extend(n,t),r.id=i.id,r.href=i.href,r.title=i.title,r.children=i.children,r.templates=i.templates,r.$container=i.container,r.node=null,r.attachedNode=null,r.options={},r.visible=!0}var i={menu:'<li class="%1$s"><a href="%2$s">%3$s</a>%4$s</li>',child_wrap:"<ul>%s</ul>",child:'<li class="%1$s" id="%5$s"><a href="%2$s">%3$s</a><ul class="sub-menu">%4$s</ul></li>'};n.prototype.set=function(e){if(!1==e instanceof n)throw new Error("Invalid argument type");return this.children[e.id]=e,this},n.prototype.push=function(e){return this.set(e)},n.prototype.get=function(e){var t=null;return this.map(e,function(e,n){return t=n,n}),t},n.prototype.map=function(e,t,n){var i={id:e},r=this;return"string"!=typeof e&&(i=e),n=n||this.children,0>=n.length?i:(Object.keys(n).forEach(function(e){child=n[e],i.id===child.id?n[i.id]=t(child):child.children&&0<Object.keys(child.children).length&&(i=r.map(i,t,child.children))}),i)},n.prototype.has=function(e){return t!==this.children[e]},n.prototype.isVisible=function(){return this.visible},n.prototype.forEach=function(e){return this.children.forEach(e)},n.prototype.countVisibleAttachedNodes=function(){var t=this,n=-1;return Object.keys(t.children).forEach(function(i){e(t.children[i].getAttachedNode()).attr("hidden")||n++}),n},n.prototype.countVisibleNodes=function(){var t,n=this,i=0;return Object.keys(n.children).forEach(function(r){t=n.children[r],e(t.getNode()).attr("hidden")||i++}),i},n.prototype.countVisible=function(){var e=this,t=0;return Object.keys(e.children).forEach(function(n){e.children[n].isVisible()&&t++}),t},n.prototype.getNode=function(){return this.node},n.prototype.getAttachedNode=function(){return this.attachedNode},n.prototype.setNode=function(e){this.node=e},n.prototype.attachNode=function(e){this.attachedNode=e},n.prototype.setOptions=function(e){return this.options=e,this},n.prototype.getOptions=function(){return this.options},n.prototype.render=function(){function t(e,n,i){var r=e.replace(new RegExp("\\%"+n+"\\$s","g"),i);return pipes={replace:function(e,n){return t(r,e,n),pipes},get:function(){return r}},pipes}function n(e,i,r){var o="",a=Object.keys(i.children);return r=r||!1,a.forEach(function(e){o+=n("super-guacamole__menu__child",i.children[e])}),t(r?d:c,1,e+" menu-item"+(0<a.length?" menu-item-has-children":"")).replace(2,i.href).replace(3,i.title).replace(4,0<a.length?o:"").replace(5,i.id).get().replace('<ul class="sub-menu"></ul>',"")}function i(e){var t="";return Object.keys(e).forEach(function(i){t+=n("super-guacamole__menu",e[i])}),t}var r,o=this,c=o.templates.menu,d=o.templates.child,a=o.$container;o.options.$menu;return 0<a.length&&(a.append(i([o])),a.find(".super-guacamole__menu__child").each(function(){$current_el=e(this),id=e(this).attr("id"),r=a.find("#"+id.replace("sg-","")),0===r.length&&(r=a.find("."+id.replace("sg-",""))),0<r.length&&o.map(id,function(e){return e.attachNode(r),e.setNode($current_el),e})})),this},n.extract=function(t){function i(e){var t="",n=null,i=/menu\-item\-[0-9]+/i;return e.attr("class").split(" ").forEach(function(e){n=i.exec(e),null!==n&&(t=n[0])}),t}var r,o,c,d,a={};return t.each(function(t,u){r=e(u),o=r.find("a:first"),menuId=r.attr("id"),"undefined"==typeof menuId&&(menuId=i(r)),c=new n({id:"sg-"+menuId,href:o.attr("href"),title:o.get(0).childNodes[0].data}),c.attachNode(r),-1<r.children(".sub-menu").length&&(subMenu=n.extract(r.children(".sub-menu").children(".menu-item")),Object.keys(subMenu).forEach(function(e){d=subMenu[e],c.set(d)})),a[c.id]=c}),a},n.prototype.attachedNodesFit=function(){var t,n,i=this,r=0,o=0,c=e(e(".header-container > .container").length>0?".header-container > .container":".header-container > div"),d=i.$container.outerWidth(!0)-i.$container.find(".super-guacamole__menu").outerWidth(!0)-(parseInt(c.css("padding-left"),10)+parseInt(c.css("padding-right"),10))/2;return Object.keys(i.children).forEach(function(r){n=i.children[r],$attachedNode=e(n.getAttachedNode()),t=e(n.getNode()),$attachedNode.removeAttr("hidden"),t.attr("hidden",!0)}),Object.keys(i.children).forEach(function(c){n=n=i.children[c],$attachedNode=e(n.getAttachedNode()),t=e(n.getNode()),o=$attachedNode.outerWidth(!0),o>0&&$attachedNode.data("width",o),r+=$attachedNode.data("width"),r>d&&($attachedNode.attr("hidden",!0),t.removeAttr("hidden"))}),!0},n.prototype.menuFit=function(t){var n,i=this,r={removeAttr:function(e,t){return e.removeAttr(t)},attr:function(e,t){return e.attr(t,!0)}},o="removeAttr",c=i.options.threshold||768;return t=t||!1,0===i.countVisibleNodes()&&(o="attr"),e(window).width()<=c-1&&(o="attr",Object.keys(i.children).forEach(function(t){n=i.children[t],$attachedNode=e(n.getAttachedNode()),$node=e(n.getNode()),$attachedNode.removeAttr("hidden"),$node.attr("hidden",!0)})),t||r[o](i.$container.find(".super-guacamole__menu"),"hidden"),"removeAttr"===o},n.prototype.watch=function(t){function n(){r.attachedNodesFit(),r.menuFit()}function i(e){var t;return function(i){function r(){n(),timeout=null}t&&clearTimeout(t),t=setTimeout(r,e)}}var r=this;return(t=t||!1)?(n(),r):(e(window).on("resize",i(10)),e(window).on("orientationchange",i(10)),r)},e.fn.superGuacamole=function(t){var r,o,c,d,a=e(this),u=a.find("#main-menu");r={threshold:544,minChildren:3,childrenFilter:"li",menuTitle:"&middot;&middot;&middot;",menuUrl:"#",templates:i},o=e.extend(r,t),c=u.children(o.childrenFilter+":not(.super-guacamole__menu):not(.super-guacamole__menu__child)"),d=new n({title:o.menuTitle,href:o.menuUrl,templates:o.templates,children:n.extract(c),container:u}),o.$menu=u,d.setOptions(o).render().watch(!0).watch()}}(jQuery);

(function( $ ) {
	"use strict";

	CherryJsCore.utilites.namespace( 'theme_script' );
	CherryJsCore.theme_script = {
		init: function() {
			// Document ready event check
			if ( CherryJsCore.status.is_ready ) {
				this.document_ready_render();
			} else {
				CherryJsCore.variable.$document.on( 'ready', this.document_ready_render.bind( this ) );
			}

			// Windows load event check
			if ( CherryJsCore.status.on_load ) {
				this.window_load_render();
			} else {
				CherryJsCore.variable.$window.on( 'load', this.window_load_render.bind( this ) );
			}
		},

		document_ready_render: function() {
			this.smart_slider_init( this );
			this.swiper_carousel_init( this );
			this.post_formats_custom_init( this );
			this.navbar_init( this );
			this.subscribe_init( this );
			this.main_menu( this, $( '.main-navigation' ) );
			this.mega_menu( this );
			this.to_top_init( this );
			this.header_search( this );
			this.mobile_menu( this );
			this.vertical_menu_init( this );
			this.add_project_inline_style( this );
			this.anchor_navigation( this );
			this.anchor_scrolling_navigation( this );
			this.ofi_init( this );
		},

		window_load_render: function() {
			this.page_preloader_init( this );
		},

		anchor_navigation: function ( self ) {
			$( '.page-template-landing div:not(.woocommerce-tabs) a[href*=#]:not([href=#])' ).on( 'click', function() {

				if ( location.pathname.replace( /^\//, '' ) === this.pathname.replace( /^\//, '' ) || location.hostname === this.hostname ) {
					var target     = $( this.hash ),
						targetLink = this.hash,
						menuHeight = $( '#main-menu' ).outerHeight(true);

					target = target.length ? target : $( '[name=' + this.hash.slice( 1 ) + ']' );

					if ( target.length ) {
						$( 'html, body' ).animate( {
							scrollTop: target.offset().top - menuHeight
						}, 1000 );

						return false;
					}
				}
			});
		},

		anchor_scrolling_navigation: function ( self ) {

			var $document   = $( document ),
				top         = null,
				changed     = false,
				currentHash = null,
				sections    = null,
				timeout     = null,
				interval    = null,
				menuHeight  = $( '#main-menu' ).outerHeight(true),
				$sections   = $( '.tm_pb_anchor' );

			sections = getSections();


			$( window ).on( 'scroll.cherry_anchor_navigation', function () {
				var newTop  = $document.scrollTop();

				changed = newTop != top;

				if ( changed ) {
					top = newTop;
				}
			});

			$( window ).on( 'resize.cherry_anchor_navigation', function () {
				sections = getSections();
			});

			function getSections() {
				sections = $.map( $sections, function ( event ) {
					var $event = $( event ),
						position = $event.position();

					return {
						top: position.top - menuHeight,
						bottom: position.top + $event.outerHeight( true ) - menuHeight,
						hash: $event.attr( 'id' )
					};
				});

				return sections;

			}

			function iteration() {

				var sectionsLength = sections.length,
					section,
					scrollTop;

				while ( section = sections[--sectionsLength] ) {

					if ( section.top >= top || section.bottom <= top ) {
						continue;
					}

					if ( currentHash == section.hash ) {
						break;
					}

					currentHash = section.hash;
					history.pushState( null, null, '#' + section.hash );
				}
			}

			timeout = setTimeout( function (){
				interval = setInterval( iteration, 250 );
			}, 250 );
		},

		smart_slider_init: function( self ) {
			$( '.latteccino-smartslider' ).each( function() {
				var slider = $( this ),
					sliderId = slider.data( 'id' ),
					sliderWidth = slider.data( 'width' ),
					sliderHeight = slider.data( 'height' ),
					sliderOrientation = slider.data( 'orientation' ),
					slideDistance = slider.data( 'slide-distance' ),
					slideDuration = slider.data( 'slide-duration' ),
					sliderFade = slider.data( 'slide-fade' ),
					sliderNavigation = slider.data( 'navigation' ),
					sliderFadeNavigation = slider.data( 'fade-navigation' ),
					sliderPagination = slider.data( 'pagination' ),
					sliderAutoplay = slider.data( 'autoplay' ),
					sliderFullScreen = slider.data( 'fullscreen' ),
					sliderShuffle = slider.data( 'shuffle' ),
					sliderLoop = slider.data( 'loop' ),
					sliderThumbnailsArrows = slider.data( 'thumbnails-arrows' ),
					sliderThumbnailsPosition = slider.data( 'thumbnails-position' ),
					sliderThumbnailsWidth = slider.data( 'thumbnails-width' ),
					sliderThumbnailsHeight = slider.data( 'thumbnails-height' );

				if ( $( '.smart-slider__items', '#' + sliderId ).length > 0 ) {
					$( '#' + sliderId ).sliderPro( {
						width: sliderWidth,
						height: sliderHeight,
						orientation: sliderOrientation,
						slideDistance: slideDistance,
						slideAnimationDuration: slideDuration,
						fade: sliderFade,
						arrows: sliderNavigation,
						fadeArrows: sliderFadeNavigation,
						buttons: sliderPagination,
						autoplay: sliderAutoplay,
						fullScreen: sliderFullScreen,
						shuffle: sliderShuffle,
						loop: sliderLoop,
						waitForLayers: false,
						thumbnailArrows: sliderThumbnailsArrows,
						thumbnailsPosition: sliderThumbnailsPosition,
						thumbnailWidth: sliderThumbnailsWidth,
						thumbnailHeight: sliderThumbnailsHeight,
						init: function() {
							$( this ).resize();
						},
						sliderResize: function( event ) {
							var thisSlider = $( '#' + sliderId ),
								slides = $( '.sp-slide', thisSlider );

							slides.each( function() {

								if ( $( '.sp-title a', this ).width() > $( this ).width() ) {
									$( this ).addClass( 'text-wrapped' );
								} else {
									$( this ).removeClass( 'text-wrapped' );
								}

							} );
						},
						breakpoints: {
							991: {
								height: parseFloat( sliderHeight ) * 0.75
							},
							767: {
								height: parseFloat( sliderHeight ) * 0.5,
								thumbnailsPosition: ( 'top' === this.thumbnailsPosition ) ? 'top' : 'bottom',
								thumbnailHeight: parseFloat( sliderThumbnailsHeight ) * 0.75,
								thumbnailWidth: parseFloat( sliderThumbnailsWidth ) * 0.75
							}
						}
					} );
				}
			} );//each end
		},

		swiper_carousel_init: function( self ) {

			// Enable swiper carousels
			jQuery( '.latteccino-carousel' ).each( function() {
				var swiper = null,
					uniqId = jQuery( this ).data( 'uniq-id' ),
					slidesPerView = parseFloat( jQuery( this ).data( 'slides-per-view' ) ),
					slidesPerGroup = parseFloat( jQuery( this ).data( 'slides-per-group' ) ),
					slidesPerColumn = parseFloat( jQuery( this ).data( 'slides-per-column' ) ),
					spaceBetweenSlides = parseFloat( jQuery( this ).data( 'space-between-slides' ) ),
					durationSpeed = parseFloat( jQuery( this ).data( 'duration-speed' ) ),
					swiperLoop = jQuery( this ).data( 'swiper-loop' ),
					freeMode = jQuery( this ).data( 'free-mode' ),
					grabCursor = jQuery( this ).data( 'grab-cursor' ),
					mouseWheel = jQuery( this ).data( 'mouse-wheel' ),
					breakpointsSettings = {
						1199: {
							slidesPerView: Math.floor( slidesPerView * 0.75 ),
							spaceBetween: Math.floor( spaceBetweenSlides * 0.75 )
						},
						991: {
							slidesPerView: Math.floor( slidesPerView * 0.5 ),
							spaceBetween: Math.floor( spaceBetweenSlides * 0.5 )
						},
						767: {
							slidesPerView: ( 0 !== Math.floor( slidesPerView * 0.25 ) ) ? Math.floor( slidesPerView * 0.25 ) : 1
						}
					};

				if ( 1 == slidesPerView ) {
					breakpointsSettings = {}
				}

				var swiper = new Swiper( '#' + uniqId, {
						slidesPerView: slidesPerView,
						slidesPerGroup: slidesPerGroup,
						slidesPerColumn: slidesPerColumn,
						spaceBetween: spaceBetweenSlides,
						speed: durationSpeed,
						loop: swiperLoop,
						freeMode: freeMode,
						grabCursor: grabCursor,
						mousewheelControl: mouseWheel,
						paginationClickable: true,
						nextButton: '#' + uniqId + '-next',
						prevButton: '#' + uniqId + '-prev',
						pagination: '#' + uniqId + '-pagination',
						onInit: function() {
							$( '#' + uniqId + '-next' ).css( { 'display': 'block' } );
							$( '#' + uniqId + '-prev' ).css( { 'display': 'block' } );
						},
						breakpoints: breakpointsSettings
					}
				);
			} );
		},

		post_formats_custom_init: function( self ) {
			CherryJsCore.variable.$document.on( 'cherry-post-formats-custom-init', function( event ) {

				if ( 'slider' !== event.object ) {
					return;
				}

				var uniqId = '#' + event.item.attr( 'id' ),
					swiper = new Swiper( uniqId, {
						pagination: uniqId + ' .swiper-pagination',
						paginationClickable: true,
						nextButton: uniqId + ' .swiper-button-next',
						prevButton: uniqId + ' .swiper-button-prev',
						spaceBetween: 0,
						onInit: function() {
							$( uniqId + ' .swiper-button-next' ).css( { 'display': 'block' } );
							$( uniqId + ' .swiper-button-prev' ).css( { 'display': 'block' } );
						}
					} );

				event.item.data( 'initalized', true );
			} );

			var items = [];

			$( '.mini-gallery .post-thumbnail__link' ).on( 'click', function( event ) {
				event.preventDefault();

				$( this ).parents( '.mini-gallery' ).find( '.post-gallery__slides > a[href]' ).each( function() {
					items.push( {
						src: $( this ).attr( 'href' ),
						type: 'image'
					} );
				} );

				$.magnificPopup.open( {
					items: items,
					gallery: {
						enabled: true
					}
				} );
			} );
		},

		navbar_init: function( self ) {

			$( window ).load( function() {

				var $layout = window.latteccino.labels.header_layout,
					$navbar = ( 'style-3' === $layout || 'style-7' === $layout  ) ? $( '.vertical-menu-toggle' ) : $( '#main-menu' );

				if ( !$.isFunction( jQuery.fn.stickUp ) || undefined === window.latteccino.stickUp || !$navbar.length ) {
					return !1;
				}

				$navbar.stickUp( {
					correctionSelector: '#wpadminbar',
					listenSelector: '.listenSelector',
					pseudo: true,
					active: true
				} );
				CherryJsCore.variable.$document.trigger( 'scroll.stickUp' );

			} );
		},

		subscribe_init: function( self ) {
			CherryJsCore.variable.$document.on( 'click', '.subscribe-block__submit', function( event ) {

				event.preventDefault();

				var $this = $( this ),
					form = $this.parents( 'form' ),
					nonce = form.find( 'input[name="nonce"]' ).val(),
					mail_input = form.find( 'input[name="subscribe-mail"]' ),
					mail = mail_input.val(),
					error = form.find( '.subscribe-block__error' ),
					success = form.find( '.subscribe-block__success' ),
					hidden = 'hidden';

				if ( '' === mail ) {
					mail_input.addClass( 'error' );
					return !1;
				}

				if ( $this.hasClass( 'processing' ) ) {
					return !1;
				}

				$this.addClass( 'processing' );
				error.empty();
				mail_input.removeClass( 'error' );

				if ( !error.hasClass( hidden ) ) {
					error.addClass( hidden );
				}

				if ( !success.hasClass( hidden ) ) {
					success.addClass( hidden );
				}

				$.ajax( {
					url: latteccino.ajaxurl,
					type: 'post',
					dataType: 'json',
					data: {
						action: 'latteccino_subscribe',
						mail: mail,
						nonce: nonce
					},
					error: function() {
						$this.removeClass( 'processing' );
					}
				} ).done( function( response ) {

					$this.removeClass( 'processing' );

					if ( true === response.success ) {
						success.removeClass( hidden );
						mail_input.val( '' );
						return 1;
					}

					error.removeClass( hidden ).html( response.data.message );
					mail_input.addClass( 'error' );
					return !1;

				} );

			} );
		},


		isMegaMenuEnabled: function() {

			if ( undefined === window.latteccino.megaMenu ) {
				return false;
			}

			if ( true === window.latteccino.megaMenu.isActive && 'main' == window.latteccino.megaMenu.location ) {
				return true;
			}

			return false;
		},

		main_menu: function( self, $mainNavigation ) {

			if ( self.isMegaMenuEnabled() ) {
				//return;
			}

			var transitionend = 'transitionend oTransitionEnd webkitTransitionEnd',
				moreMenuContent = '&middot;&middot;&middot;',
				imgurl = '',
				srcset = '',
				hasimg = false,
				hasicon = false,
				hasprop = Object.prototype.hasOwnProperty,
				$menuToggle = $( '.main-menu-toggle[aria-controls="main-menu"]', $mainNavigation ),
				liWithChildren = 'li.menu-item-has-children, li.page_item_has_children',
				$body = $( 'body' ),
				$parentNode,
				menuItem,
				subMenu,
				index = -1,
				$layout = window.latteccino.labels.header_layout;

			if ( hasprop.call( window, 'latteccino' ) &&
				hasprop.call( window.latteccino, 'more_button_options' ) &&
				hasprop.call( window.latteccino.more_button_options, 'more_button_type' ) ) {
				switch ( window.latteccino.more_button_options.more_button_type ) {
					case 'image':
						imgurl = window.latteccino.more_button_options.more_button_image_url;
						if ( window.latteccino.more_button_options.retina_more_button_image_url ) {
							srcset = ' srcset="' + window.latteccino.more_button_options.retina_more_button_image_url + ' 2x"';
						}
						moreMenuContent = '<img src="' + imgurl + '"' + srcset + ' alt="' + moreMenuContent + '">';
						hasimg = true;
						break;
					case 'icon':
						moreMenuContent = '<i class="fa ' + window.latteccino.more_button_options.more_button_icon + '"></i>';
						hasicon = true;
						break;
					case 'text':
					default:
						moreMenuContent = window.latteccino.more_button_options.more_button_text || moreMenuContent;
						hasimg = false;
						hasicon = false;
						break;
				}
			}

			if ( 'style-3' !== $layout && 'style-7' !== $layout  ) {
				$mainNavigation.superGuacamole( {
					threshold: 768, // Minimal menu width, when this plugin activates
					minChildren: 3, // Minimal visible children count
					childrenFilter: '.menu-item', // Child elements selector
					menuTitle: moreMenuContent, // Menu title
					menuUrl: '#',
					templates: {
						menu: '<li id="%5$s" class="%1$s' + ( hasimg ? ' super-guacamole__menu-with-image' : '' ) +
						( hasicon ? ' super-guacamole__menu-with-icon' : '' ) + '"><a href="%2$s">%3$s</a><ul class="sub-menu">%4$s</ul></li>',
						child_wrap: '<ul class="%1$s">%2$s</ul>',
						child: '<li id="%5$s" class="%1$s"><a href="%2$s">%3$s</a><ul class="sub-menu">%4$s</ul></li>'
					}
				} );
			}

			function hideSubMenu( menuItem, $event ) {
				var subMenus = menuItem.find( '.sub-menu' ),
					subMenu = menuItem.children( '.sub-menu' ).first();

				menuItem
					.removeData( 'index' )
					.removeClass( 'menu-hover' );

				subMenus.addClass( 'in-transition' );

				subMenus
					.one( transitionend, function() {
						subMenus.removeClass( 'in-transition' );
					} );
			}

			function handleMenuItemHover( $event ) {
				if ( $( 'html' ).hasClass( 'mobile-menu-active' ) ) {
					return;
				}
				menuItem = $( $event.target ).parents( '.menu-item' );
				subMenu = menuItem.children( '.sub-menu' ).first();

				var subMenus = menuItem.find( '.sub-menu' );

				if ( !menuItem.hasClass( 'menu-item-has-children' ) ) {
					menuItem = $event.target.tagName === 'LI' ?
						$( $event.target ) :
						$( $event.target ).parents().filter( '.menu-item' );
				}

				switch ( $event.type ) {
					case 'mouseenter':
					case 'mouseover':
						if ( 0 < subMenu.length ) {
							var maxWidth = $body.outerWidth( true ),
								subMenuOffset = subMenu.offset().left + subMenu.outerWidth( true );
							menuItem.addClass( 'menu-hover' );
							subMenus.addClass( 'in-transition' );
							if ( maxWidth <= subMenuOffset ) {
								subMenu.addClass( 'left-side' );
								subMenu.find( '.sub-menu' ).addClass( 'left-side' );
							} else if ( 0 > subMenu.offset().left ) {
								subMenu.removeClass( 'left-side' );
								subMenu.find( '.sub-menu' ).removeClass( 'left-side' );
							}
							subMenus
								.one( transitionend, function() {
									subMenus.removeClass( 'in-transition' );
								} );
						}
						break;
					case 'mouseleave':
					case 'mouseout':
						hideSubMenu( menuItem, $event );
						break;
				}
			}

			CherryJsCore.variable.$window.on( 'orientationchange resize', function() {
				if ( $( 'html' ).hasClass( 'mobile-menu-active' ) ) {
					return;
				}
				$mainNavigation.find( '.menu-item' ).removeClass( 'menu-hover' );
				$mainNavigation.find( '.sub-menu.left-side' ).removeClass( 'left-side' );
			} );

			$( liWithChildren ).hoverIntent( {
				over: function() {
				},
				out: function() {
				},
				timeout: 300,
				selector: '.menu-item'
			} );

			$mainNavigation.on( 'mouseenter mouseover mouseleave mouseout', '.menu-item', handleMenuItemHover );

			function doubleClickMenu( $jqEvent ) {
				$parentNode = $( this );

				if ( $( 'html' ).hasClass( 'mobile-menu-active' ) ) {
					return true;
				}

				var menuIndex = $parentNode.index();

				if ( menuIndex !== parseInt( $parentNode.data( 'index' ), 10 ) ) {
					$jqEvent.preventDefault();
				}

				$parentNode.data( 'index', menuIndex );
			}

			// Check if touch events supported
			if ( 'ontouchend' in window ) {

				// Attach event listener for double click
				$( liWithChildren, $mainNavigation )
					.on( 'click', doubleClickMenu );

				// Reset index on touchend event
				CherryJsCore.variable.$document.on( 'touchend', function( $jqEvent ) {
					if ( !$( 'html' ).hasClass( 'mobile-menu-active' ) ) {
						$parentNode = $( $jqEvent.target ).parents().filter( '.menu-item:first' );

						if ( $parentNode.hasClass( 'menu-hover' ) === false ) {
							hideSubMenu( $parentNode, $jqEvent );

							index = $parentNode.data( 'index' );

							if ( index ) {
								$parentNode.data( 'index', parseInt( index, 10 ) - 1 );
							}
						}
					}
				} );
			}

			$menuToggle.on( 'click', function( $event ) {
				$event.preventDefault();
				$mainNavigation.toggleClass( 'toggled' );
			} );
		},

		mega_menu: function( self ) {

			/**
			 * Mega-menu mobile SubMenu Toggled.
			 */
			function megaMenuSubMenuToggled() {
				$( this ).toggleClass( 'active' );
			}

			// ADD to Mega-menu sub-menu toggle active class.
			$( '.mega-menu-mobile-arrow' ).on( 'click', megaMenuSubMenuToggled );
		},

		mobile_menu: function( self ) {

			var $mainNavigation = $( '.main-navigation' ),
				$menuToggle = $( '.main-menu-toggle[aria-controls="main-menu"]' );

			$mainNavigation
				.find( 'li.menu-item-has-children > a' )
				.append( '<span class="sub-menu-toggle"></span>' );

			/**
			 * Debounce the function call
			 *
			 * @param  {number}   threshold The delay.
			 * @param  {Function} callback  The function.
			 */
			function debounce( threshold, callback ) {
				var timeout;

				return function debounced( $event ) {
					function delayed() {
						callback.call( this, $event );
						timeout = null;
					}

					if ( timeout ) {
						clearTimeout( timeout );
					}

					timeout = setTimeout( delayed, threshold );
				};
			}

			/**
			 * Resize event handler.
			 *
			 * @param {jqEvent} jQuery event.
			 */
			function resizeHandler( $event ) {
				var $window = CherryJsCore.variable.$window,
					width = $window.outerWidth( true );

				if ( 768 <= width ) {
					$mainNavigation.removeClass( 'mobile-menu' );
				} else {
					$mainNavigation.addClass( 'mobile-menu' );
				}
			}

			/**
			 * Toggle sub-menus.
			 *
			 * @param  {jqEvent} $event jQuery event.
			 */
			function toggleSubMenuHandler( $event ) {

				$event.preventDefault();

				$( this ).toggleClass( 'active' );
				$( this ).parents().filter( 'li:first' ).toggleClass( 'sub-menu-open' );
			}

			/**
			 * Toggle menu.
			 *
			 * @param  {jqEvent} $event jQuery event.
			 */
			function toggleMenuHandler( $event ) {
				var $toggle = $( '.sub-menu-toggle' );

				if ( !$event.isDefaultPrevented() ) {
					$event.preventDefault();
				}

				setTimeout( function() {
					if ( !$mainNavigation.hasClass( 'animate' ) ) {
						$mainNavigation.addClass( 'animate' );
					}
					$mainNavigation.toggleClass( 'show' );
					$( 'html' ).toggleClass( 'mobile-menu-active' );
				}, 10 );

				$menuToggle.toggleClass( 'toggled' );
				$menuToggle.attr( 'aria-expanded', !$menuToggle.hasClass( 'toggled' ) );

				if ( $toggle.hasClass( 'active' ) ) {
					$toggle.removeClass( 'active' );
					$mainNavigation.find( '.sub-menu-open' ).removeClass( 'sub-menu-open' );
				}
			}

			resizeHandler();
			CherryJsCore.variable.$window.on( 'resize orientationchange', debounce( 500, resizeHandler ) );
			$( '.sub-menu-toggle', $mainNavigation ).on( 'click', toggleSubMenuHandler );
			$menuToggle.on( 'click', toggleMenuHandler );
		},

		page_preloader_init: function( self ) {

			if ( $( '.page-preloader-cover' )[0] ) {
				$( '.page-preloader-cover' ).delay( 500 ).fadeTo( 500, 0, function() {
					$( this ).remove();
				} );
			}
		},

		to_top_init: function( self ) {
			if ( $.isFunction( jQuery.fn.UItoTop ) && undefined !== window.latteccino.toTop ) {
				$().UItoTop( {
					text: latteccino.labels.totop_button,
					scrollSpeed: 600
				} );
			}
		},

		header_search: function( self ) {
			var $header = $( '.site-header' ),
				$searchToggle = $( '.search-form__toggle, .search-form__close', $header ),
				$headerSearch = $( '.header-search', $header ),
				$searchInput = $( '.search-form__field', $headerSearch ),
				searchHandler = function( event ) {
					$header.toggleClass( 'search-active' );
					if ( $header.hasClass( 'search-active' ) ) {
						$searchInput.focus();
					}
				},
				removeActiveClass = function( event ) {
					if ( $( event.target ).closest( $searchToggle ).length || $( event.target ).closest( $headerSearch ).length ) {
						return;
					}

					if ( $header.hasClass( 'search-active' ) ) {
						$header.removeClass( 'search-active' );
					}

					event.stopPropagation();
				};

			$searchToggle.on( 'click', searchHandler );
			CherryJsCore.variable.$document.on( 'click', removeActiveClass );

		},
		vertical_menu_init: function( self ) {
			var $mainNavigation = $( '.main-navigation.vertical-menu' ),
				$mainMenu = $( '.menu', $mainNavigation ),
				$back = $( '.back', $mainNavigation ),
				$close = $( '.close', $mainNavigation ),
				currentTranslate = parseInt( $mainMenu.css( 'transform' ) ),
				isAnimate = false,
				offset = 300;

			resizeHandler();
			CherryJsCore.variable.$window.on( 'resize', resizeHandler );

			$( '.vertical-menu-toggle' ).on( 'click.open', openHandler );
			$( '.menu-item-has-children > a', $mainNavigation ).on( 'click.active', clickHandler );
			$back.on( 'click.back', backHandler );
			$close.on( 'click.close', closeHandler );
			$mainMenu.addClass( 'scroll' );

			$( '.menu-item-has-children', $mainNavigation ).each( function() {
				var $li = $( this ),
					$link = $( '>a', $li ),
					linkText = $link.html(),
					linkHref = $link[0].cloneNode( true ),
					$subMenu = $( '> .sub-menu', $li );
				$subMenu.prepend( '<li class="parent-title"><a href="' + linkHref + '">' + linkText + '</a></li>' );
			} );

			function resizeHandler( event ) {
				var $window = CherryJsCore.variable.$window,
					width = $window.outerWidth( true );

				if ( 768 > width ) {
					$mainNavigation.removeClass( 'vertical-menu' );
				} else {
					$mainNavigation.addClass( 'vertical-menu' );
				}
			}

			function openHandler( event ) {
				$( this ).toggleClass( 'toggled' );
				if ( $mainNavigation.hasClass( 'menu-open' ) ) {
					closeHandler();
					return false;
				}
				$mainNavigation.toggleClass( 'menu-open' );
			}

			function backHandler( event ) {
				var currentTranslate = parseInt( ($mainMenu.css( 'transform' ).replace( /,/g, "" )).split( " " )[4] ),
					translate = currentTranslate + offset,
					$active = $( '.active', $mainMenu ),
					$lastActive = $( $active[$active.length - 1] );

				if ( isAnimate ) {
					return false;
				}

				if ( currentTranslate < 0 ) {
					isAnimate = true;
					$mainMenu.css( 'transform', 'translateX(' + translate + 'px)' );

					if ( translate >= 0 ) {
						translate = 0;
						$( this ).addClass( 'hide' );
						$close.removeClass( 'hide' );
					}

					setTimeout( function() {
						$lastActive.removeClass( 'active' );
						$lastActive.siblings().toggleClass( 'hide' );
						$( '>a', $lastActive ).removeClass( 'hide' );
						$lastActive.parent().addClass( 'scroll' );
						isAnimate = false;
					}, 250 );

				}
				return false;
			}

			function closeHandler( event ) {
				if ( !isAnimate ) {
					$( '.active', $mainMenu ).removeClass( 'active' );
					$( '.hide', $mainMenu ).removeClass( 'hide' );
					$( '.close.hide', $mainNavigation ).removeClass( 'hide' );
					$back.addClass( 'hide' );
					$mainNavigation.removeClass( 'menu-open' );
					$mainMenu.css( 'transform', 'translateX(0)' );
				}

				$( '.vertical-menu-toggle' ).removeClass( 'toggled' );
				return false;
			}

			function clickHandler( event ) {
				var $_target = $( event.currentTarget ),
					$mainMenu = $_target.closest( '.menu' ),
					deep = $_target.parents( 'ul' ).length,
					translate = deep * offset;

				$mainMenu.css( 'transform', 'translateX(' + -translate + 'px)' );

				setTimeout( function() {
					$_target.parent().addClass( 'active' );
					$_target.parent().siblings().toggleClass( 'hide' );
					$_target.parents( '.active' ).find( '> a' ).addClass( 'hide' );
					$_target.siblings( 'ul' ).addClass( 'scroll' );
					$_target.parents( 'ul' ).removeClass( 'scroll' );
				}, 250 );

				$back.removeClass( 'hide' );
				$close.addClass( 'hide' );

				return false;
			}
		},

		add_project_inline_style: function( self ) {
			var $projectGridContainer = $( '.projects-container.grid-layout' ),
				$projectJustifiedContainer = $( '.projects-container.justified-layout' ),
				$projectGridTermsContainer = $( '.projects-terms-container.grid-layout' ),
				$projectContainer = $( '.projects-container' ),

				addInlineStyle = function() {
					var $this = $( this ),
						$projectsSettings = $this.data( 'settings' ),
						$projectItemIndent = Math.ceil( +$projectsSettings['item-margin'] );

					$this.css( {
						'margin-left': -$projectItemIndent / 2 + 'px',
						'margin-right': -$projectItemIndent / 2 + 'px'
					} );
				},

				addTemplateClass = function() {
					var $this = $( this ),
						$projectsSettings = $this.data( 'settings' ),
						$projectsTemplate = $projectsSettings['template'];

					if ( $projectsTemplate ) {
						$this.addClass( $projectsTemplate.replace( '.', '-' ) );
					}
				};

			$projectGridContainer.each( addInlineStyle );
			$projectJustifiedContainer.each( addInlineStyle );
			$projectGridTermsContainer.each( addInlineStyle );

			$projectContainer.each( addTemplateClass );
		},

		ofi_init: function( self ) {
			if ( $( 'body' ).hasClass( 'ie' ) && 'undefined' !== typeof objectFitImages ) {
				objectFitImages();
			}
		}
	};
	CherryJsCore.theme_script.init();

}( jQuery ));


// fin theme scipt

!function(a,b){"use strict";function c(){if(!e){e=!0;var a,c,d,f,g=-1!==navigator.appVersion.indexOf("MSIE 10"),h=!!navigator.userAgent.match(/Trident.*rv:11\./),i=b.querySelectorAll("iframe.wp-embedded-content");for(c=0;c<i.length;c++){if(d=i[c],!d.getAttribute("data-secret"))f=Math.random().toString(36).substr(2,10),d.src+="#?secret="+f,d.setAttribute("data-secret",f);if(g||h)a=d.cloneNode(!0),a.removeAttribute("security"),d.parentNode.replaceChild(a,d)}}}var d=!1,e=!1;if(b.querySelector)if(a.addEventListener)d=!0;if(a.wp=a.wp||{},!a.wp.receiveEmbedMessage)if(a.wp.receiveEmbedMessage=function(c){var d=c.data;if(d.secret||d.message||d.value)if(!/[^a-zA-Z0-9]/.test(d.secret)){var e,f,g,h,i,j=b.querySelectorAll('iframe[data-secret="'+d.secret+'"]'),k=b.querySelectorAll('blockquote[data-secret="'+d.secret+'"]');for(e=0;e<k.length;e++)k[e].style.display="none";for(e=0;e<j.length;e++)if(f=j[e],c.source===f.contentWindow){if(f.removeAttribute("style"),"height"===d.message){if(g=parseInt(d.value,10),g>1e3)g=1e3;else if(~~g<200)g=200;f.height=g}if("link"===d.message)if(h=b.createElement("a"),i=b.createElement("a"),h.href=f.getAttribute("src"),i.href=d.value,i.host===h.host)if(b.activeElement===f)a.top.location.href=d.value}else;}},d)a.addEventListener("message",a.wp.receiveEmbedMessage,!1),b.addEventListener("DOMContentLoaded",c,!1),a.addEventListener("load",c,!1)}(window,document);

// fin wp embedbed 

window.MP_RM_Registry=function(){"use strict";function e(e){return"function"==typeof e.getInstance}function t(t,n){if(!e(n))throw new Error('Invalide module "'+t+'". The function "getInstance" is not defined.');a[t]=n}function n(t){for(var n in t)if(t.hasOwnProperty(n)){if(!e(t[n]))throw new Error('Invalide module "'+n+'" inside the collection. The function "getInstance" is not defined.');a[n]=t[n]}}function r(e){delete a[e]}function i(e){var t=a[e];if(!t)throw new Error('The module "'+e+'" has not been registered or it was unregistered.');if("function"!=typeof t.getInstance)throw new Error('The module "'+e+'" can not be instantiated. The function "getInstance" is not defined.');return a[e].getInstance()}var a={};return{register:t,unregister:r,_get:i,MP_RM_RegistryMap:n}}(),MP_RM_Registry.register("MP_RM_Functions",function(e){"use strict";function t(){return{init:function(){},wpAjax:function(e,t,n){e.mprm_action=e.action,delete e.action,wp.ajax.send("route_url",{success:function(e){!_.isUndefined(n)&&_.isFunction(n)&&t(e)},error:function(e){!_.isUndefined(n)&&_.isFunction(n)?n(e):console.log(e)},data:e})},getParameterByName:function(e,t){var n,r=[];if(e){for(var i=e.slice(e.indexOf("?")+1).split("&"),a=0;a<i.length;a++)n=i[a].split("="),r.push(n[0]),r[n[0]]=n[1];return"undefined"!=typeof t?r[t]:r}return!1},stringToBoolean:function(e){switch(e.toLowerCase().trim()){case"true":case"yes":case"1":return!0;case"false":case"no":case"0":case null:return!1;default:return Boolean(e)}},callModal:function(t,n,r){t=_.isEmpty(t)?o:t;var i=e(window).outerHeight()-60,a=e(window).outerWidth()-60,o=wp.html.string({tag:"span",attrs:{"class":"spinner is-active"},content:""}),m={content:t,closeOnEsc:!0,animation:{open:"zoomIn",close:"zoomOut"},width:a,height:i,closeButton:"box",addClass:"mprm-modal mprm-restaurant",onOpen:function(){var t=e("#"+this.id);n.call(this,t)},onClose:function(){e("#"+this.id).remove()}};_.isUndefined(r)||e.extend(m,r);var s=new jBox("Modal",m);s.open()},inArray:function(t,n){var r=[],i=e.inArray(n,t);return i>=0&&r.push(t[i]),r},validateForm:function(t){if(t){var n=e("#"+t),r=document.getElementById(t);return"function"==typeof r.checkValidity&&!1===r.checkValidity()?(n.find(":invalid").addClass("mprm-form-error"),n.find("input").not(":submit, :reset, :image, [disabled], :hidden").each(function(){this.validity.valid||e(this).focus()}),e("input",n).off("keypress").on("keypress",function(t){var n=e(this).attr("type");/date|email|month|number|search|tel|text|time|url|week/.test(n)&&13===t.keyCode||e(this).removeClass("mprm-form-error")}),e("select",n).off("change").on("change",function(){e(this).removeClass("mprm-form-error")}),!1):!0}return!1}}}var n;return{getInstance:function(){return n||(n=t()),n}}}(jQuery)),MP_RM_Registry.register("HtmlBuilder",function(e){"use strict";function t(){return{_singleHtmlTag:{tag:"div",attrs:{id:356,"class":"item","data-selector":"some-button"},content:"<%= data-key %>"},_htmlStructure:{tag:"div",attrs:{id:356,"class":"item","data-selector":"some-button"},content:{tag:"div",attrs:{"class":"title"},content:"<%= data-key %>"}},_htmlStructureWithArray:{tag:"div",attrs:{id:356,"class":"item","data-selector":"some-button"},content:[{tag:"div",attrs:{"class":"title"},content:"<%= data-key %>"},{tag:"span",attrs:{"class":"date"},content:"<%= data-key %>"}]},generateHTML:function(t){var r,i="";if(_.isObject(t)){var a=document.createElement(t.tag);_.isUndefined(t.attrs)||e.each(t.attrs,function(e,t){a.setAttribute(e,t)}),_.isArray(t.content)?(e.each(t.content,function(e,t){i+=n.generateHTML(t)}),e(a).html(i)):_.isObject(t.content)?(i=n.generateHTML(t.content),e(a).html(i)):_.isUndefined(t.content)?e(a).html(""):e(a).html(t.content),r=e(a).get(0).outerHTML}else r=_.isString(t)?t:!1;return r},getHtml:function(t,r){if(_.isUndefined(t))return!1;var i=!1;if(_.isUndefined(r)&&(_.isArray(t)?(i="",e.each(t,function(e,t){i+=n.generateHTML(t)})):i=n.generateHTML(t)),_.isObject(r)){var a=_.template(i);i=a(r)}return i}}}var n;return{getInstance:function(){return n||(n=t()),n}}}(jQuery)),MP_RM_Registry.register("Menu-Shop",function(e){"use strict";function t(){return{initPreloader:function(e,t){"show"===e?(t.find(".mprm-add-menu-item .mprm-add-to-cart").addClass("mprm-preloader-color"),t.find(".mprm-add-menu-item .mprm-container-preloader .mprm-floating-circle-wrapper").removeClass("mprm-hidden")):(t.find(".mprm-add-menu-item .mprm-container-preloader .mprm-floating-circle-wrapper").addClass("mprm-hidden"),t.find(".mprm-add-menu-item .mprm-add-to-cart").removeClass("mprm-preloader-color"))},addToCart:function(){e(".mprm-add-to-cart.mprm-has-js").on("click",function(t){t.preventDefault();var r=e(this),i=r.closest("form"),a=i.serializeArray(),o=i.parents(".mprm_menu_item_buy_button").find("> .mprm-notice"),m=i.parents(".mprm_menu_item_buy_button");e(".mprm_menu_item_buy_button").find("> .mprm-notice").addClass("mprm-hidden"),a.push({name:"is_ajax",value:!0}),e(".widget_mprm_cart_widget").length&&a.push({name:"cart",value:!0}),n.initPreloader("show",m),MP_RM_Registry._get("MP_RM_Functions").wpAjax(a,function(t){o.addClass("mprm-notice-success"),o.removeClass("mprm-hidden mprm-notice-error"),e(".widget_mprm_cart_widget .mprm-cart-content").html(t.cart),n.initPreloader("hide",m),t.redirect?window.location=t.redirect:(e(".mprm-cart-added-alert",i).fadeIn(),setTimeout(function(){e(".mprm-cart-added-alert",i).fadeOut()},3e3))},function(e){var t=i.parents(".mprm_menu_item_buy_button").find(".mprm-notice");t.addClass("mprm-notice-error").removeClass("mprm-hidden"),n.initPreloader("hide",m),console.warn("Some error!!!"),console.warn(e)})})},changeGateway:function(){e("input[name=payment-mode]","#mprm_purchase_form").on("change",function(){e("#mprm_purchase_form_wrap").html(""),n.loadGateway()})},loadGateway:function(){var t=e("input[name=payment-mode]:checked","#mprm_purchase_form").val();if(t){var r=[{name:"controller",value:"cart"},{name:"mprm_action",value:"load_gateway"},{name:"payment-mode",value:t}];e(".mprm-cart-ajax").show(),MP_RM_Registry._get("MP_RM_Functions").wpAjax(r,function(t){e(".mprm-no-js:not(.mprm-add-to-cart)").hide(),e("#mprm_purchase_form_wrap").html(t.html),n.showTerms()},function(e){console.warn("Some error!!!"),console.warn(e)})}},update_item_quantities:function(){e(".mprm-item-quantity").on("change",function(){var t=this;clearTimeout(r),r=setTimeout(function(){var n=e(t),r=n.val(),i=n.data("key"),a=n.closest(".mprm_cart_item").data("menu-item-id"),o=n.parent().find('input[name="mprm-cart-menu-item-'+i+'-options"]').val(),m={action:"update_cart_item_quantity",controller:"cart",quantity:r,menu_item_id:a,options:o,position:i};MP_RM_Registry._get("MP_RM_Functions").wpAjax(m,function(t){e(".mprm_cart_subtotal_amount").each(function(){var n=e(this);n.text(t.subtotal),n.attr("data-subtotal",t.subtotal),n.attr("data-total",t.subtotal)}),e(".mprm_cart_tax_amount").each(function(){e(this).text(t.taxes)}),e(".mprm_cart_amount").each(function(){var n=e(this);n.text(t.total),n.attr("data-subtotal",t.total),n.attr("data-total",t.total)})},function(e){console.warn("Some error!!!"),console.warn(e)})},1e3)})},purchaseForm:function(){e(document).on("click","#mprm_purchase_form #mprm_purchase_submit input[type=submit]",function(t){var n=e(this).parents("form");if(n.length&&!n.hasClass("mprm-no-js")){var r=document.getElementById("mprm_purchase_form");if(!MP_RM_Registry._get("MP_RM_Functions").validateForm("mprm_purchase_form"))return;t.preventDefault(),e(this).after('<span class="mprm-cart-ajax"><i class="mprm-icon-spinner mprm-icon-spin"></i></span>');var i=e(r).serializeArray();e.each(i,function(e,t){t&&"mprm_action"===t.name&&"gateway_select"===t.value&&i.splice(e,1)}),e(".mprm-cart-ajax").show(),e(".mprm-errors").remove(),MP_RM_Registry._get("MP_RM_Functions").wpAjax(i,function(t){t.errors?e("#mprm_final_total_wrap").before(t.errors):e("#mprm_purchase_form").submit()},function(t){t.error&&e("#mprm_final_total_wrap").before(t.errors),e(".mprm-cart-ajax").remove(),console.warn("Some error!!!"),console.warn(t)})}})},showTerms:function(){e("#mprm_show_terms").find(".mprm_terms_links").on("click",function(t){t.preventDefault(),e(this).parents("#mprm_show_terms").find(".mprm_terms_links").toggle(),e("#mprm_terms").toggle()})},get_login:function(){e("#mprm_checkout_form_wrap").on("click",".mprm_checkout_register_login",function(t){t.preventDefault();var n=e(this),r={action:"get_login",controller:"customer"};MP_RM_Registry._get("MP_RM_Functions").wpAjax(r,function(e){n.parent().html(e.html)},function(e){console.warn("Some error!!!"),console.warn(e)})})},loginAjax:function(){e("#mprm_checkout_form_wrap").on("click",'#mprm_login_submit,[name="mprm_login_submit"]',function(t){t.preventDefault(),e(".mprm-errors").remove();var n={action:"login_ajax",controller:"customer",nonce:e('[name="mprm_login_nonce"]').val(),redirect:e('[name="redirect"]').val(),pass:e('[name="mprm_user_pass"]').val(),login:e('[name="mprm_user_login"]').val()};MP_RM_Registry._get("MP_RM_Functions").wpAjax(n,function(e){e.redirect?window.location=e.redirect_url:window.location.reload()},function(t){t.data.html?e("#mprm_checkout_form_wrap").find(".mprm-login-fields").after(t.data.html):(console.warn("Some error!!!"),console.warn(t))})})}}}var n,r;return{getInstance:function(){return n||(n=t()),n}}}(jQuery)),MP_RM_Registry.register("Order",function(e){"use strict";function t(){return{init:function(){n.hideElementOrder(),n.addComment(),n.removeComment(),n.addCustomer(),n.removeMenuItem(),n.addMenuItem(),n.recalculate_total(),n.changeOrderBaseCountry(),n.changeCustomer()},changeOrderBaseCountry:function(){e("[name='mprm_settings[base_state]'] option").length<1&&e("[name='mprm_settings[base_state]']").parents("tr").hide(),e("select.mprm-country-list").on("change",function(){var t={action:"get_state_list",controller:"settings",country:e(this).val()},n=e(this).parents(".mprm-columns.mprm-four"),r=n.find("select.mprm-country-state");MP_RM_Registry._get("MP_RM_Functions").wpAjax(t,function(t){r.parents("#mprm-order-address-state-wrap").hide(),e.isEmptyObject(t)?r.parents("#mprm-order-address-state-wrap").hide():(r.find("option").remove(),e.each(t,function(t,n){r.append(e("<option>").text(n).attr("value",t))}),r.trigger("chosen:updated"),r.parents("#mprm-order-address-state-wrap").show())},function(e){console.warn("Some error!!!"),console.warn(e)})})},addComment:function(){e("#mprm-add-order-note").on("click",function(t){t.preventDefault();var r={action:"add_comment",controller:"order",order_id:e(this).attr("data-order-id"),noteText:e("#mprm-order-note").val()};MP_RM_Registry._get("MP_RM_Functions").wpAjax(r,function(t){e(".mprm-no-order-notes").hide(),e("#mprm-order-notes-inner").append(t.html),e("#mprm-order-note").val(""),n.removeComment()},function(e){console.warn("Some error!!!"),console.warn(e)})})},addMenuItem:function(){e('[name="mprm-order-menu-item-select"]').on("change",function(){var t={action:"get_price",controller:"menu_item",menu_item:e(this).val()};MP_RM_Registry._get("MP_RM_Functions").wpAjax(t,function(t){e('[name="mprm-order-menu-item-amount"]').val(t.price)},function(e){console.warn("Some error!!!"),console.warn(e)})}),e("#mprm-order-add-menu-item").on("click",function(t){t.preventDefault();var n=e('[name="mprm-order-menu-item-select"]'),r=e("#mprm-order-menu-item-quantity"),i=e('[name="mprm-order-menu-item-amount"]'),a=n.val(),o=e('[name="mprm-order-menu-item-select"] option:selected').text(),m=r.val(),s=i.val(),c=0,d=!1,u=0;if(1>a)return!1;if(s||(s=0),s=parseFloat(s),isNaN(s))return alert(mprm_admin_vars.numeric_item_price),!1;var l=s;if("1"===mprm_admin_vars.quantities_enabled){if(isNaN(parseInt(m)))return alert(mprm_admin_vars.numeric_quantity),!1;s*=m}var p=(s/m).toFixed(mprm_admin_vars.currency_decimals)+mprm_admin_vars.currency_sign;if("before"===mprm_admin_vars.currency_pos&&(p=mprm_admin_vars.currency_sign+(s/m).toFixed(mprm_admin_vars.currency_decimals)),MP_RM_Registry._get("MP_RM_Functions").stringToBoolean(mprm_admin_vars.enable_taxes)&&mprm_admin_vars.rate>1){var _=mprm_admin_vars.rate/100;if(isNaN(_))return alert(mprm_admin_vars.numeric_tax),!1;u=s*_,s=parseFloat(s)+parseFloat(u)}s=s.toFixed(mprm_admin_vars.currency_decimals);var f=s+mprm_admin_vars.currency_sign;"before"===mprm_admin_vars.currency_pos&&(f=mprm_admin_vars.currency_sign+s),d&&(o=o+" - "+d);var g=e(".mprm-row.item").length,h=e("#mprm-purchased-wrapper").find(".mprm-row.item:last").clone();h.find(".menu_item span").html('<a href="post.php?post='+a+'&action=edit"></a>'),h.find(".item span a").text(o),h.find(".price-text").text(f),h.find(".item-quantity").text(m),h.find(".item-price").text(p),h.find("input.mprm-order-detail-id").val(a),h.find("input.mprm-order-detail-price-id").val(c),h.find("input.mprm-order-detail-item-price").val(l),h.find("input.mprm-order-detail-amount").val(s),h.find("input.mprm-order-detail-tax").val(u),h.find("input.mprm-order-detail-quantity").val(m),h.find("input.mprm-order-detail-has-log").val(0),h.find("input").each(function(){var t=e(this).attr("name");t=t.replace(/\[(\d+)\]/,"["+parseInt(g)+"]"),e(this).attr("name",t).attr("id",t)}),e("#mprm-payment-menu-items-changed").val(1),e(h).insertAfter("#mprm-purchased-wrapper .mprm-row.item:last"),e(".mprm-order-recalc-totals").show()})},recalculate_total:function(){e("#mprm-order-recalc-total").on("click",function(t){t.preventDefault();var n=0,r=e("#mprm-purchased-wrapper").find(".mprm-row.item .mprm-order-detail-amount"),i=0;r.length&&r.each(function(){n+=parseFloat(e(this).val())}),e(".mprm-order-fees").length&&e(".mprm-order-fees span.fee-amount").each(function(){n+=parseFloat(e(this).data("fee"))}),e(".mprm-order-detail-tax").length&&e(".mprm-order-detail-tax","#mprm-purchased-wrapper").each(function(){i+=parseFloat(e(this).val())}),e('input[name="mprm-order-total"]').val(n.toFixed(mprm_admin_vars.currency_decimals)),e('input[name="mprm-order-tax"]').val(i.toFixed(mprm_admin_vars.currency_decimals)),e(".mprm-order-recalc-totals").hide()})},removeMenuItem:function(){e(".mprm-order-remove-menu-item.mprm-delete").on("click",function(t){t.preventDefault();var n=e(document.body).find("#mprm-purchased-wrapper > .mprm-row").length;if(1===n)return alert(mprm_admin_vars.one_menu_item_min),!1;if(confirm(mprm_admin_vars.delete_payment_menu_item)){var r=e(this).data("key"),i=e('input[name="mprm-order-removed"]'),a=e('input[name="mprm-order-details['+r+'][id]"]').val(),o=e('input[name="mprm-order-details['+r+'][price_id]"]').val(),m=e('input[name="mprm-order-details['+r+'][quantity]"]').val(),s=e('input[name="mprm-order-details['+r+'][amount]"]').val(),c=i.val();c=e.parseJSON(c),c.length<1&&(c={}),c[r]=[{id:a,price_id:o,quantity:m,amount:s,cart_index:r}],i.val(JSON.stringify(c)),e(this).parents(".mprm-row").remove(),e("#mprm-order-menu-items-changed").val(1),e(".mprm-order-recalc-totals").show()}return!1})},changeCustomer:function(){e('[name="customer-id"]').on("change",function(){var t={action:"get_customer_information",controller:"customer",customer_id:e(this).val()};MP_RM_Registry._get("MP_RM_Functions").wpAjax(t,function(t){e(".mprm-customer-information").html(t.customer_information)},function(e){console.warn("Some error!!!"),console.warn(e)})})},addCustomer:function(){var t=e(".customer-info.mprm-row"),n=e(".new-customer.mprm-row"),r=e('[name="mprm-new-customer"]');e(".mprm-new-customer").on("click",function(){t.hide(),n.show(),r.val(1)}),e(".mprm-new-customer-cancel").on("click",function(){t.show(),n.hide(),r.val(0)}),e(".mprm-new-customer-save").on("click",function(r){r.preventDefault();var i={action:"add_customer",controller:"customer",name:e('[name="mprm-new-customer-name"]').val(),email:e('[name="mprm-new-customer-email"]').val(),phone:e('[name="mprm-new-phone-number"]').val()};MP_RM_Registry._get("MP_RM_Functions").wpAjax(i,function(r){t.show(),n.hide(),e('[name="customer-id"]').replaceWith(r.html),e(".mprm-customer-information").html(r.customer_information)},function(e){t.show(),n.hide(),console.warn("Some error!!!"),console.warn(e)})})},initChosen:function(){var t=e(".mprm-select-chosen");t.length&&e.each(t,function(){var t=e(this),n="undefined"!=typeof t.attr("data-text_single")?t.attr("data-text_single"):mprm_admin_vars.one_option,r="undefined"!=typeof t.attr("data-text_multiple")?t.attr("data-text_multiple"):mprm_admin_vars.one_or_more_option;t.chosen({inherit_select_classes:!0,placeholder_text_single:n,placeholder_text_multiple:r})})},removeComment:function(){e(document).on("click.remove_order_note",".mprm-delete-order-note",function(t){t.preventDefault();var n=e(this).attr("data-note-id"),r={action:"remove_comment",controller:"order",order_id:e(this).attr("data-order-id"),note_id:n};MP_RM_Registry._get("MP_RM_Functions").wpAjax(r,function(){e("#mprm-payment-note-"+n).remove(),e(".mprm-payment-note").length<1&&e(".mprm-no-order-notes").show()},function(e){console.warn("Some error!!!"),console.warn(e)})})},hideElementOrder:function(){var t=e("#post_status");e("#submitdiv").hide(),e("#order-log").hide(),e("#titlewrap").parents("#post-body-content").hide(),e("#commentstatusdiv").parent().hide(),t.find("option").remove(),e('select[name="mprm-order-status"]').find("option").clone().appendTo(t)}}}var n;return{getInstance:function(){return n||(n=t()),n}}}(jQuery)),MP_RM_Registry.register("Menu-Settings",function(e){"use strict";function t(){return{init:function(){e("#rm_settings").on("submit",function(){var t=e(this).serializeArray();return e("#setting-error-settings_updated:visible").remove(),n.saveSettings(t,function(){var t=e("#setting-error-settings_updated").clone();t.removeClass("hidden"),e(".wrap #settings-title").after(t),e(".notice-dismiss").on("click",function(){e(this).parent().remove()})}),!1}),n.changeBaseCountry(),n.settingsUpload()},saveSettings:function(e,t){MP_RM_Registry._get("MP_RM_Functions").wpAjax(e,function(e){!_.isUndefined(t)&&_.isFunction(t)&&t(e)},function(e){console.warn("Some error!!!"),console.warn(e)})},delete_checked:function(){e("#mprm-customer-delete-confirm").change(function(){var t=e("#mprm-customer-delete-records"),n=e("#mprm-delete-customer");e(this).prop("checked")?(t.attr("disabled",!1),n.attr("disabled",!1)):(t.attr("disabled",!0),t.prop("checked",!1),n.attr("disabled",!0))})},settingsUpload:function(){if("undefined"==typeof wp||"1"!==mprm_admin_vars.new_media_ui){var t=e(".mprm_settings_upload_button");t.length>0&&(window.formfield="",e(document.body).on("click",t,function(t){t.preventDefault(),window.formfield=e(this).parent().prev(),window.tbframe_interval=setInterval(function(){jQuery("#TB_iframeContent").contents().find(".savesend .button").val(mprm_admin_vars.use_this_file).end().find("#insert-gallery, .wp-post-thumbnail").hide()},2e3),tb_show(mprm_admin_vars.add_new_menu_item,"media-upload.php?TB_iframe=true")}),window.mprm_send_to_editor=window.send_to_editor,window.send_to_editor=function(t){if(window.formfield){var n=e("a","<div>"+t+"</div>").attr("href");window.formfield.val(n),window.clearInterval(window.tbframe_interval),tb_remove()}else window.mprm_send_to_editor(t);window.send_to_editor=window.mprm_send_to_editor,window.formfield="",window.imagefield=!1})}else{var n;window.formfield="",e(document.body).on("click",".mprm_settings_upload_button",function(t){t.preventDefault();var r=e(this);return window.formfield=e(this).parent().prev(),n?void n.open():(n=wp.media.frames.file_frame=wp.media({frame:"post",state:"insert",title:r.data("uploader_title"),button:{text:r.data("uploader_button_text")},multiple:!1}),n.on("menu:render:default",function(e){var t={};e.unset("library-separator"),e.unset("gallery"),e.unset("featured-image"),e.unset("embed"),e.set(t)}),n.on("insert",function(){var e=n.state().get("selection");e.each(function(e){e=e.toJSON(),window.formfield.val(e.url)})}),void n.open())}),window.formfield=""}},changeBaseCountry:function(){e("[name='mprm_settings[base_state]'] option").length<1&&e("[name='mprm_settings[base_state]']").parents("tr").hide(),e("[name='mprm_settings[base_country]']").on("change",function(){var t={action:"get_state_list",controller:"settings",country:e(this).val()},n=e(this).closest("tr"),r=n.next().find("select");MP_RM_Registry._get("MP_RM_Functions").wpAjax(t,function(t){e.isEmptyObject(t)?n.next().hide():(n.next().show(),r.remove("option"),e.each(t,function(t,n){r.append(e("<option>").text(n).attr("value",t))}))},function(e){console.warn("Some error!!!"),console.warn(e)})})}}}var n;return{getInstance:function(){return n||(n=t()),n}}}(jQuery)),MP_RM_Registry.register("Menu-Item",function(e){"use strict";function t(){return{imgIds:[],init:function(){n.imagesInit(),e("a.mp_menu_gallery").on("click",function(){n.openMediaWindow()})},imagesInit:function(){e(document).on("click.mprm_delete_img",".mp_menu_images a.mprm-delete",function(){return e(this).parents("li.mprm-image").remove(),n.refreshImages(),!1}),n.refreshImages(),e("#mprm-menu-item-gallery").find(".mp_menu_images").sortable({items:"li.mprm-image",cursor:"move",scrollSensitivity:40,forcePlaceholderSize:!0,forceHelperSize:!1,helper:"clone",opacity:.65,placeholder:"mp-metabox-sortable-placeholder",start:function(e,t){t.item.css("background-color","#f6f6f6")},stop:function(e,t){t.item.removeAttr("style")},update:function(){n.refreshImages()}})},openMediaWindow:function(){if(void 0===this.window){this.window=wp.media({title:mprm_admin_vars.insert_media,library:{type:"image"},multiple:!0,button:{text:mprm_admin_vars.insert}});var t=this;this.window.on("select",function(){var r=t.window.state().get("selection").toJSON();e("#mprm-menu-item-gallery").find(".mp_menu_images").append(n._buildImages(r)),n.imagesInit()})}return this.window.open(),!1},refreshImages:function(){var t=[],n=e("#mprm-menu-item-gallery");n.find(".mp_menu_images li.mprm-image").each(function(n,r){t[n]=e(r).attr("data-attachment_id"),e(r).attr("data-key",n)}),n.find(' input[name="mp_menu_gallery"]').val(t.join(","))},_buildImages:function(t){var n=[];e.each(t,function(e,t){n.push({tag:"li",attrs:{"class":"mprm-image","data-attachment_id":t.id,new_image:1},content:[{tag:"img",attrs:{src:t.sizes.thumbnail.url}},{tag:"ul",attrs:{"class":"mprm-actions"},content:[{tag:"li",content:[{tag:"a",attrs:{"class":"mprm-delete",title:mprm_admin_vars.delete_img,href:"#"},content:mprm_admin_vars["delete"]}]}]}]})});try{return MP_RM_Registry._get("HtmlBuilder").getHtml(n)}catch(r){console.log(r)}},quickEdit:function(){e(document).on("click","#the-list .editinline",function(){var t=e(this).closest("tr"),n=t.attr("id").replace(/\D/gi,""),r=t.find(".mprm-data-price").text(),i=t.find(".mprm-data-sku").text(),a=t.find(".mprm-data-calories").text(),o=t.find(".mprm-data-cholesterol").text(),m=t.find(".mprm-data-fiber").text(),s=t.find(".mprm-data-sodium").text(),c=t.find(".mprm-data-carbohydrates").text(),d=t.find(".mprm-data-fat").text(),u=t.find(".mprm-data-protein").text(),l=t.find(".mprm-data-bulk").text(),p=t.find(".mprm-data-size").text(),_=t.find(".mprm-data-weight").text(),f=e("#edit-"+n);f.find('.inline-price [name="price"]').val(r),f.find('.inline-sku [name="sku"]').val(i),f.find('[name="nutritional[calories][val]"]').val(a),f.find('[name="nutritional[cholesterol][val]"]').val(o),f.find('[name="nutritional[fiber][val]"]').val(m),f.find('[name="nutritional[sodium][val]"]').val(s),f.find('[name="nutritional[carbohydrates][val]"]').val(c),f.find('[name="nutritional[fat][val]"]').val(d),f.find('[name="nutritional[protein][val]"]').val(u),f.find('[name="attributes[bulk][val]"]').val(l),f.find('[name="attributes[size][val]"]').val(p),f.find('[name="attributes[weight][val]"]').val(_)})}}}var n;return{getInstance:function(){return n||(n=t()),n}}}(jQuery)),MP_RM_Registry.register("Menu-Category",function(e){"use strict";function t(){return{init:function(){e("#IconPicker").fontIconPicker({source:e.fnt_icons_categorized,emptyIcon:!0,hasSearch:!0}).on("change",function(){}),e(".remove_icon_button").on("click",function(){e(this).siblings(".mprm_icon_p").find("input").attr({value:""})}),e(".upload_image_button").on("click",function(){return n.openUploadWindow(),!1}),e(".remove_image_button").on("click",function(){var t=e("#menu_category_thumbnail");return t.find("img").attr("src",t.find("img").attr("data-placeholder")),e("#menu_category_thumbnail_id").val(""),e(".remove_image_button").hide(),!1})},openUploadWindow:function(){if(void 0===this.window){this.window=wp.media.frames.menu_itemable_file=wp.media({title:mprm_admin_vars.choose_image,button:{text:mprm_admin_vars.use_image},multiple:!1});var t=this;this.window.on("select",function(){var n=t.window.state().get("selection").first().toJSON();e("#menu_category_thumbnail_id").val(n.id),e("#menu_category_thumbnail").find("img").attr("src",n.sizes.thumbnail.url),e(".remove_image_button").show()})}this.window.open()}}}var n;return{getInstance:function(){return n||(n=t()),n}}}(jQuery)),MP_RM_Registry.register("Theme",function(e){"use strict";function t(){return{init:function(){"undefined"!=typeof e.magnificPopup&&e(".type-mp_menu_item .gallery-item,.type-mp_menu_item .mprm-item-gallery").magnificPopup({delegate:"a",type:"image",tLoading:"Loading image #%curr%...",mainClass:"mfp-img-mobile",gallery:{enabled:!0,navigateByImgClick:!0,preload:[0,1]},image:{tError:'<a href="%url%">The image #%curr%</a> could not be loaded.',titleSrc:function(e){return e.el.attr("title")}},zoom:{enabled:!0,duration:300}})},viewParams:function(e,t){switch(t){case"simple-list":e.find(".mprm-widget-feat_img").addClass("hidden"),e.find(".mprm-widget-buy").addClass("hidden"),e.find('select.mprm-widget-categ_name option[value="with_img"]').change("only_text").addClass("hidden"),e.find(".mprm-widget-price_pos").removeClass("hidden");break;default:e.find(".mprm-widget-feat_img").removeClass("hidden"),e.find(".mprm-widget-buy").removeClass("hidden"),e.find('select.mprm-widget-categ_name option[value="with_img"]').change("only_text").removeClass("hidden"),e.find(".mprm-widget-price_pos").addClass("hidden")}},customizeWidget:function(){e.each(e(".mprm-widget-view"),function(){var t=e(this).parents(".widget-content"),r=e(this).val();n.viewParams(t,r)}),e(document.body).on("change",".mprm-widget-view",function(){var t=e(this).parents(".widget-content"),r=e(this).val();n.viewParams(t,r)})}}}var n;return{getInstance:function(){return n||(n=t()),n}}}(jQuery)),function(e){"use strict";e(document).ready(function(){"mp_menu_item"===e(window.post_type).val()&&MP_RM_Registry._get("Menu-Item").init(),"edit-mp_menu_item"===window.pagenow&&MP_RM_Registry._get("Menu-Item").quickEdit(),"restaurant-menu_page_mprm-settings"===window.pagenow&&MP_RM_Registry._get("Menu-Settings").init(),"restaurant-menu_page_mprm-customers"===window.pagenow&&MP_RM_Registry._get("Menu-Settings").delete_checked(),"edit-mp_menu_category"===window.pagenow&&MP_RM_Registry._get("Menu-Category").init(),"mprm_order"===e(window.post_type).val()&&MP_RM_Registry._get("Order").init(),e(".mprm-select-chosen").length&&MP_RM_Registry._get("Order").initChosen(),e(".mprm-add-to-cart").length&&MP_RM_Registry._get("Menu-Shop").addToCart(),e("#mprm_checkout_wrap").length&&MP_RM_Registry._get("Menu-Shop").purchaseForm(),e("#mprm_purchase_form").length&&(MP_RM_Registry._get("Menu-Shop").loadGateway(),MP_RM_Registry._get("Menu-Shop").changeGateway(),MP_RM_Registry._get("Menu-Shop").update_item_quantities(),MP_RM_Registry._get("Menu-Shop").showTerms(),MP_RM_Registry._get("Menu-Shop").get_login(),MP_RM_Registry._get("Menu-Shop").loginAjax()),e(".mprm-widget-view").length&&MP_RM_Registry._get("Theme").customizeWidget(),(e(".gallery-item").length||e(".mprm-item-gallery").length)&&MP_RM_Registry._get("Theme").init()})}(jQuery);

// fin mp restaurant 

!function(t){"use strict";t(function(){t(".tm-testi-slider").each(function(){var i,e=t(this),n=e.data("atts"),a={pagination:"#tm-testi-slider-pagination-"+n.id,nextButton:"#tm-testi-slider-next-"+n.id,prevButton:"#tm-testi-slider-prev-"+n.id,paginationClickable:!0,autoHeight:!1},s=null;if("undefined"!==n)for(i in n)a[i]=n[i];s=new Swiper(e,a)})})}(jQuery);

// fin public 

(function(){"use strict";function a(){}function b(a,b){for(var c=a.length;c--;)if(a[c].listener===b)return c;return-1}function c(a){return function(){return this[a].apply(this,arguments)}}var d=a.prototype,e=this,f=e.EventEmitter;d.getListeners=function(a){var b,c,d=this._getEvents();if("object"==typeof a){b={};for(c in d)d.hasOwnProperty(c)&&a.test(c)&&(b[c]=d[c])}else b=d[a]||(d[a]=[]);return b},d.flattenListeners=function(a){var b,c=[];for(b=0;b<a.length;b+=1)c.push(a[b].listener);return c},d.getListenersAsObject=function(a){var b,c=this.getListeners(a);return c instanceof Array&&(b={},b[a]=c),b||c},d.addListener=function(a,c){var d,e=this.getListenersAsObject(a),f="object"==typeof c;for(d in e)e.hasOwnProperty(d)&&-1===b(e[d],c)&&e[d].push(f?c:{listener:c,once:!1});return this},d.on=c("addListener"),d.addOnceListener=function(a,b){return this.addListener(a,{listener:b,once:!0})},d.once=c("addOnceListener"),d.defineEvent=function(a){return this.getListeners(a),this},d.defineEvents=function(a){for(var b=0;b<a.length;b+=1)this.defineEvent(a[b]);return this},d.removeListener=function(a,c){var d,e,f=this.getListenersAsObject(a);for(e in f)f.hasOwnProperty(e)&&(d=b(f[e],c),-1!==d&&f[e].splice(d,1));return this},d.off=c("removeListener"),d.addListeners=function(a,b){return this.manipulateListeners(!1,a,b)},d.removeListeners=function(a,b){return this.manipulateListeners(!0,a,b)},d.manipulateListeners=function(a,b,c){var d,e,f=a?this.removeListener:this.addListener,g=a?this.removeListeners:this.addListeners;if("object"!=typeof b||b instanceof RegExp)for(d=c.length;d--;)f.call(this,b,c[d]);else for(d in b)b.hasOwnProperty(d)&&(e=b[d])&&("function"==typeof e?f.call(this,d,e):g.call(this,d,e));return this},d.removeEvent=function(a){var b,c=typeof a,d=this._getEvents();if("string"===c)delete d[a];else if("object"===c)for(b in d)d.hasOwnProperty(b)&&a.test(b)&&delete d[b];else delete this._events;return this},d.removeAllListeners=c("removeEvent"),d.emitEvent=function(a,b){var c,d,e,f,g=this.getListenersAsObject(a);for(e in g)if(g.hasOwnProperty(e))for(d=g[e].length;d--;)c=g[e][d],c.once===!0&&this.removeListener(a,c.listener),f=c.listener.apply(this,b||[]),f===this._getOnceReturnValue()&&this.removeListener(a,c.listener);return this},d.trigger=c("emitEvent"),d.emit=function(a){var b=Array.prototype.slice.call(arguments,1);return this.emitEvent(a,b)},d.setOnceReturnValue=function(a){return this._onceReturnValue=a,this},d._getOnceReturnValue=function(){return!this.hasOwnProperty("_onceReturnValue")||this._onceReturnValue},d._getEvents=function(){return this._events||(this._events={})},a.noConflict=function(){return e.EventEmitter=f,a},"function"==typeof define&&define.amd?define("eventEmitter/EventEmitter",[],function(){return a}):"object"==typeof module&&module.exports?module.exports=a:this.EventEmitter=a}).call(this),function(a){function b(b){var c=a.event;return c.target=c.target||c.srcElement||b,c}var c=document.documentElement,d=function(){};c.addEventListener?d=function(a,b,c){a.addEventListener(b,c,!1)}:c.attachEvent&&(d=function(a,c,d){a[c+d]=d.handleEvent?function(){var c=b(a);d.handleEvent.call(d,c)}:function(){var c=b(a);d.call(a,c)},a.attachEvent("on"+c,a[c+d])});var e=function(){};c.removeEventListener?e=function(a,b,c){a.removeEventListener(b,c,!1)}:c.detachEvent&&(e=function(a,b,c){a.detachEvent("on"+b,a[b+c]);try{delete a[b+c]}catch(d){a[b+c]=void 0}});var f={bind:d,unbind:e};"function"==typeof define&&define.amd?define("eventie/eventie",f):a.eventie=f}(this),function(a,b){"use strict";"function"==typeof define&&define.amd?define(["eventEmitter/EventEmitter","eventie/eventie"],function(c,d){return b(a,c,d)}):"object"==typeof module&&module.exports?module.exports=b(a,require("wolfy87-eventemitter"),require("eventie")):a.imagesLoaded=b(a,a.EventEmitter,a.eventie)}(window,function(a,b,c){function d(a,b){for(var c in b)a[c]=b[c];return a}function e(a){return"[object Array]"==l.call(a)}function f(a){var b=[];if(e(a))b=a;else if("number"==typeof a.length)for(var c=0;c<a.length;c++)b.push(a[c]);else b.push(a);return b}function g(a,b,c){if(!(this instanceof g))return new g(a,b,c);"string"==typeof a&&(a=document.querySelectorAll(a)),this.elements=f(a),this.options=d({},this.options),"function"==typeof b?c=b:d(this.options,b),c&&this.on("always",c),this.getImages(),j&&(this.jqDeferred=new j.Deferred);var e=this;setTimeout(function(){e.check()})}function h(a){this.img=a}function i(a,b){this.url=a,this.element=b,this.img=new Image}var j=a.jQuery,k=a.console,l=Object.prototype.toString;g.prototype=new b,g.prototype.options={},g.prototype.getImages=function(){this.images=[];for(var a=0;a<this.elements.length;a++){var b=this.elements[a];this.addElementImages(b)}},g.prototype.addElementImages=function(a){"IMG"==a.nodeName&&this.addImage(a),this.options.background===!0&&this.addElementBackgroundImages(a);var b=a.nodeType;if(b&&m[b]){for(var c=a.querySelectorAll("img"),d=0;d<c.length;d++){var e=c[d];this.addImage(e)}if("string"==typeof this.options.background){var f=a.querySelectorAll(this.options.background);for(d=0;d<f.length;d++){var g=f[d];this.addElementBackgroundImages(g)}}}};var m={1:!0,9:!0,11:!0};g.prototype.addElementBackgroundImages=function(a){for(var b=n(a),c=/url\(['"]*([^'"\)]+)['"]*\)/gi,d=c.exec(b.backgroundImage);null!==d;){var e=d&&d[1];e&&this.addBackground(e,a),d=c.exec(b.backgroundImage)}};var n=a.getComputedStyle||function(a){return a.currentStyle};return g.prototype.addImage=function(a){var b=new h(a);this.images.push(b)},g.prototype.addBackground=function(a,b){var c=new i(a,b);this.images.push(c)},g.prototype.check=function(){function a(a,c,d){setTimeout(function(){b.progress(a,c,d)})}var b=this;if(this.progressedCount=0,this.hasAnyBroken=!1,!this.images.length)return void this.complete();for(var c=0;c<this.images.length;c++){var d=this.images[c];d.once("progress",a),d.check()}},g.prototype.progress=function(a,b,c){this.progressedCount++,this.hasAnyBroken=this.hasAnyBroken||!a.isLoaded,this.emit("progress",this,a,b),this.jqDeferred&&this.jqDeferred.notify&&this.jqDeferred.notify(this,a),this.progressedCount==this.images.length&&this.complete(),this.options.debug&&k&&k.log("progress: "+c,a,b)},g.prototype.complete=function(){var a=this.hasAnyBroken?"fail":"done";if(this.isComplete=!0,this.emit(a,this),this.emit("always",this),this.jqDeferred){var b=this.hasAnyBroken?"reject":"resolve";this.jqDeferred[b](this)}},h.prototype=new b,h.prototype.check=function(){var a=this.getIsImageComplete();return a?void this.confirm(0!==this.img.naturalWidth,"naturalWidth"):(this.proxyImage=new Image,c.bind(this.proxyImage,"load",this),c.bind(this.proxyImage,"error",this),c.bind(this.img,"load",this),c.bind(this.img,"error",this),void(this.proxyImage.src=this.img.src))},h.prototype.getIsImageComplete=function(){return this.img.complete&&void 0!==this.img.naturalWidth},h.prototype.confirm=function(a,b){this.isLoaded=a,this.emit("progress",this,this.img,b)},h.prototype.handleEvent=function(a){var b="on"+a.type;this[b]&&this[b](a)},h.prototype.onload=function(){this.confirm(!0,"onload"),this.unbindEvents()},h.prototype.onerror=function(){this.confirm(!1,"onerror"),this.unbindEvents()},h.prototype.unbindEvents=function(){c.unbind(this.proxyImage,"load",this),c.unbind(this.proxyImage,"error",this),c.unbind(this.img,"load",this),c.unbind(this.img,"error",this)},i.prototype=new h,i.prototype.check=function(){c.bind(this.img,"load",this),c.bind(this.img,"error",this),this.img.src=this.url;var a=this.getIsImageComplete();a&&(this.confirm(0!==this.img.naturalWidth,"naturalWidth"),this.unbindEvents())},i.prototype.unbindEvents=function(){c.unbind(this.img,"load",this),c.unbind(this.img,"error",this)},i.prototype.confirm=function(a,b){this.isLoaded=a,this.emit("progress",this,this.element,b)},g.makeJQueryPlugin=function(b){b=b||a.jQuery,b&&(j=b,j.fn.imagesLoaded=function(a,b){var c=new g(this,a,b);return c.jqDeferred.promise(j(this))})},g.makeJQueryPlugin(),g});

// fin images

// cherryPortfolioPlugin plugin
(function($){
	var methods = {
		init : function( options ) {

			var settings = {
				call: function() {}
			}

			return this.each( function() {
				if ( options ) {
					$.extend( settings, options );
				}

				var $this                  = $( this ),
					$projectsContainer     = $( '.projects-container', $this ),
					$projectsList          = $( '.projects-list', $projectsContainer ),
					$projectsFilters       = $( '.projects-filters', $this ),
					$projectsTermsFilters  = $( 'ul.projects-filters-list', $projectsFilters ),
					$projectsOrderFilters  = $( 'ul.order-filters', $projectsFilters ),
					projectsSettings       = $projectsContainer.data( 'settings' ),
					$ajaxLoader            = null,
					$ajaxMoreLoader        = null,
					$projectsMoreButton    = null,
					orderSetting           = {
						order:   $projectsFilters.data( 'order-default' ) || 'DESC',
						orderby: $projectsFilters.data( 'orderby-default' ) || 'date'
					},
					pagesCount             = Math.ceil( parseInt( $projectsList.data( 'all-posts-count' ) ) / parseInt( projectsSettings['post-per-page'] ) ),
					currentTermSlug        = projectsSettings['single-term'],
					currentPage            = 1
					ajaxRequestSuccess     = true,
					ajaxRequestObject      = null;

				( function () {
					$ajaxLoader = $( '.cherry-projects-ajax-loader' , $this );

					$ajaxLoader.css( { 'display': 'block'} ).fadeTo( 500, 1 );

					$ajaxMoreLoader = $('.projects-end-line-spinner .cherry-spinner', $this );

					getNewProjectsList( currentTermSlug, currentPage, orderSetting );

					if ( $projectsFilters[0] ) {
						addTermsFiltersEventsFunction();
					}

					if ( $projectsFilters[0] && $projectsOrderFilters[0] ) {
						addOrderFiltersEventsFunction();
					}

					addEventsFunction();
				} )();

				/*
				 * Add events for terms filters
				 */
				function addTermsFiltersEventsFunction() {

					$( 'li span', $projectsTermsFilters ).on( 'click', function() {
						var $parent = $(this).parent(),
							slug = '';

						if ( ! $( this ).parent().hasClass( 'active' ) ) {
							$( 'li' , $projectsTermsFilters ).removeClass( 'active' );
							$( this ).parent().addClass( 'active' );

							currentTermSlug = $(this).data( 'slug' );
							currentPage = 1;

							getNewProjectsList( currentTermSlug, currentPage, orderSetting );
						}
					});
				}

				/*
				 * Add events for order filters
				 */
				function addOrderFiltersEventsFunction() {
					$projectsOrderFilters.on( 'click', '[data-filter-type="orderby"]', function() {
						var $this = $(this);

						$this.toggleClass( 'dropdown-state' );
					})

					$projectsOrderFilters.on( 'click', '[data-filter-type="order"]', function() {
						var $this         = $( this ),
							$descLabel    = $this.data('desc-label'),
							$ascLabel     = $this.data('asc-label'),
							order         = '';

						if ( $descLabel === $( '.current', $this ).text() ) {
							$( '.current', $this ).html( $ascLabel );
							orderSetting.order = 'asc';
						} else {
							$( '.current', $this ).html( $descLabel );
							orderSetting.order = 'desc';
						}

						getNewProjectsList( currentTermSlug, currentPage, orderSetting );
					})

					$projectsOrderFilters.on( 'click', '.orderby-list li', function() {
						var $this      = $( this ),
							$parent    = $this.parents('[data-filter-type="orderby"]'),
							orderby    = $this.data('orderby');

						if ( $parent.hasClass( 'dropdown-state' ) ) {
							$parent.removeClass( 'dropdown-state' );
						}

						$( '.current', $parent ).html( $this.html() );
						$( 'li', $parent ).removeClass( 'active' );
						$this.addClass( 'active' );

						orderSetting.orderby = orderby;

						getNewProjectsList( currentTermSlug, currentPage, orderSetting );
					});


					$( '.orderby-list', $projectsOrderFilters ).on( 'mouseleave', function() {
						$( this ).closest( '.dropdown-state' ).removeClass( 'dropdown-state' );
					} );

					$( $projectsOrderFilters ).on( 'mouseleave', '.dropdown-state', function() {
						$( this ).removeClass( 'dropdown-state' );
					} );
				}

				/*
				 * Add events for pagination
				 */
				function addPaginationEventsFunction() {
					var $projectsPagination     = $( '.projects-pagination', $projectsContainer ),
						$pageNavigation         = $( '.page-navigation', $projectsPagination );

					if ( $projectsPagination[0] ) {
						$( '.page-link > li span', $projectsPagination ).on( 'click', function() {
							var $this = $(this);

							if ( ! $this.parent().hasClass( 'active' ) ) {
								$( '.page-link > li', $projectsPagination ).removeClass( 'active' );
								$this.parent().addClass( 'active' );
								currentPage = $this.parent().index() + 1;

								getNewProjectsList( currentTermSlug, currentPage, orderSetting );
							}

						});

						if ( $pageNavigation[0] ) {
							$( '.next-page', $pageNavigation ).on( 'click', function() {
								currentPage++;
								getNewProjectsList( currentTermSlug, currentPage, orderSetting );
							});
							$( '.prev-page', $pageNavigation ).on( 'click', function() {
								currentPage--;
								getNewProjectsList( currentTermSlug, currentPage, orderSetting );
							});
						}
					}
				}

				/*
				 * Waypoint event
				 */
				function addWaypointEvent() {

					$projectsContainer.waypoint( function( direction ) {

						if ( 'down' === direction ) {

							if ( currentPage < pagesCount ) {
								currentPage++;

								getMoreProjects( currentTermSlug, currentPage, orderSetting );
							}
						}
					}, {
						offset: 'bottom-in-view'
					} );
				}

				/*
				 * Add events
				 */
				function addEventsFunction() {
					$( $projectsContainer ).on( 'click', '.projects-ajax-button', function() {

						if ( currentPage < pagesCount ) {
							currentPage++;

							if ( currentPage == pagesCount) {
								$( '.projects-ajax-button', $projectsContainer ).addClass( 'disabled' ).remove();
							}

							getMoreProjects( currentTermSlug, currentPage, orderSetting );
						}
					});
					jQuery( window ).on( 'resize.projects_layout_resize', function() {
						switch ( projectsSettings['list-layout'] ) {
							case 'grid-layout':
								gridLayoutRender( getResponsiveColumn() );
							break;
							case 'masonry-layout':
								masonryLayoutRender( getResponsiveColumn() );
							break;
							case 'cascading-grid-layout':
								cascadingGridLayoutRender();
							break;
						}
					} );
				}

				/*
				 * Get new projects list
				 */
				function getNewProjectsList( slug, page, order ) {
					var data = {
						action: 'get_new_projects',
						settings: {
							slug: slug,
							page: page,
							list_layout: projectsSettings['list-layout'],
							loading_mode: projectsSettings['loading-mode'],
							order_settings: order,
							template: projectsSettings['template'],
							posts_format: projectsSettings['posts-format'],
							filter_type: projectsSettings['filter-type'],
							post_per_page: projectsSettings['post-per-page']
						}
					}

					if ( ! ajaxRequestSuccess ) {
						ajaxRequestObject.abort();
					}

					ajaxRequestObject = $.ajax( {
						type: 'POST',
						url: cherryProjectsObjects.ajax_url,
						data: data,
						cache: false,
						beforeSend: function() {
							ajaxRequestSuccess = false;
							$ajaxLoader.css( { 'display': 'block' } ).fadeTo( 500, 1 );

							hideAnimation( 50 );
						},
						success: function( response ){
							ajaxRequestSuccess = true;

							$projectsContainer.html( response );

							pagesCount = Math.ceil( parseInt( $( '.projects-list', $projectsContainer ).data( 'all-posts-count' ) ) / parseInt( projectsSettings['post-per-page'] ) ),

							addPaginationEventsFunction();

							if ( 'lazy-loading-mode' === projectsSettings['loading-mode'] ) {
								addWaypointEvent();
							}

							switch ( projectsSettings['list-layout'] ) {
								case 'grid-layout':
									gridLayoutRender( getResponsiveColumn() );
								break;
								case 'masonry-layout':
									masonryLayoutRender( getResponsiveColumn() );
								break;
								case 'justified-layout':
									justifiedLayoutRender();
								break;
								case 'cascading-grid-layout':
									cascadingGridLayoutRender();
								break;
								case 'list-layout':
									listLayoutRender();
								break;
							}

							$projectsContainer.imagesLoaded( function() {
								$ajaxLoader.fadeTo( 500, 0, function() { $( this ).css( { 'display': 'none' } ); } );

								showAnimation( 0, 100 );

								Waypoint.refreshAll();

								CherryJsCore.cherryProjectsFrontScripts.magnificIconInit();
								CherryJsCore.variable.$document.trigger( 'getNewProjectsListAjaxSuccess' );
							} );

						}
					});
				}

				/*
				 * Get new projects list
				 */
				function getMoreProjects( slug, page, order ) {
					var data = {
						action: 'get_more_projects',
						settings: {
							slug: slug,
							page: page,
							list_layout: projectsSettings['list-layout'],
							loading_mode: projectsSettings['loading-mode'],
							order_settings: order,
							template: projectsSettings['template'],
							posts_format: projectsSettings['posts-format'],
							filter_type: projectsSettings['filter-type'],
							post_per_page: projectsSettings['post-per-page']
						}
					}

					if ( ! ajaxRequestSuccess ) {
						return;
					}

					ajaxRequestObject = $.ajax( {
						type: 'POST',
						url: cherryProjectsObjects.ajax_url,
						data: data,
						cache: false,
						beforeSend: function() {
							ajaxRequestSuccess = false;
							$ajaxMoreLoader.css( { 'display': 'block' } ).fadeTo( 500, 1 );
						},
						success: function( response ){
							ajaxRequestSuccess = true;

							var $projectsItemLength = $('.projects-item', $projectsContainer).length;

							$('.projects-list', $projectsContainer).append( response );

							switch ( projectsSettings['list-layout'] ) {
								case 'grid-layout':
									gridLayoutRender( getResponsiveColumn() );
								break;
								case 'masonry-layout':
									masonryLayoutRender( getResponsiveColumn() );
								break;
								case 'justified-layout':
									justifiedLayoutRender();
								break;
								case 'cascading-grid-layout':
									cascadingGridLayoutRender();
								break;
								case 'list-layout':
									listLayoutRender();
								break;
							}

							$projectsContainer.imagesLoaded( function() {
								$ajaxMoreLoader.fadeTo( 500, 0, function() { $( this ).css( { 'display': 'none' } ); } );

								showAnimation( $projectsItemLength, 100 );

								Waypoint.refreshAll();

								CherryJsCore.cherryProjectsFrontScripts.magnificIconInit();

								CherryJsCore.variable.$document.trigger( 'getMoreProjectsAjaxSuccess' );
							} );

						}
					});
				}

				/*
				 * Render grid layout
				 */
				function gridLayoutRender( columnNumber ) {
					var projectsList = $('.projects-item', $projectsContainer );

					projectsList.each( function( index ) {

						var $this     = $( this ),
							itemWidth = ( 100 / columnNumber ).toFixed(5);

						$this.css( {
							'-webkit-flex-basis': itemWidth + '%',
							'flex-basis': itemWidth + '%',
							'width': itemWidth + '%',
							'margin-bottom': projectsSettings['item-margin'] + 'px'
						} );

						$('.inner-wrapper', $this ).css( {
							'margin': ( +projectsSettings['item-margin'] / 2 ).toFixed(2) + 'px'
						} );

					});
				}

				/*
				 * Masonry grid layout
				 */
				function masonryLayoutRender( columnNumber ) {
					var projectsListWrap = $('.projects-list', $projectsContainer ),
						projectsList = $('.projects-item', $projectsContainer );

					projectsListWrap.css( {
						'-webkit-column-count': columnNumber,
						'column-count': columnNumber,
						'-webkit-column-gap': +projectsSettings['item-margin'],
						'column-gap': +projectsSettings['item-margin'],
					} );

					$( '.inner-wrapper', projectsList ).css( {
						'margin-bottom': +projectsSettings['item-margin']
					} );
				}

				/*
				 * Justified grid layout
				 */
				function justifiedLayoutRender() {
					var projectsListWrap = $('.projects-list', $projectsContainer ),
						projectsList = $('.projects-item', $projectsContainer );

						projectsList.each( function() {
							var $this = $(this),
								imageWidth = $this.data( 'image-width' ),
								imageHeight = $this.data( 'image-height' ),
								imageRatio = +imageWidth / +imageHeight,
								flexValue = Math.round( imageRatio * 100 ),
								newWidth = Math.round( +projectsSettings['fixed-height'] * imageRatio ),
								newHeight = 'auto';

							$this.css( {
								'flex-grow': flexValue,
								'flex-basis': newWidth
							} );

							$('.inner-wrapper', $this ).css( {
								'margin': Math.ceil( +projectsSettings['item-margin'] / 2 ) + 'px'
							} );

						} );
				}

				/*
				 * Cascading grid layout
				 */
				function cascadingGridLayoutRender( columnNumber ) {
					var projectsListWrap = $('.projects-list', $projectsContainer ),
						projectsList = $('.projects-item', $projectsContainer );

						projectsList.each( function( index ) {
							var $this = $(this),
								imageWidth = $this.data( 'image-width' ),
								imageHeight = $this.data( 'image-height' ),
								newWidth = ( 100 / getCascadingIndex( index ) ).toFixed(2),
								margin = Math.ceil( +projectsSettings['item-margin'] / 2 );

							$this.css( {
								'width': +newWidth + '%',
								'max-width': +newWidth + '%'
							} );

							$('.inner-wrapper', $this ).css( {
								'margin': margin + 'px'
							} );

							projectsListWrap.css( {
								'margin-left': -margin + 'px',
								'margin-right': -margin + 'px',
							} );
						} );
				}

				/*
				 * Render list layout
				 */
				function listLayoutRender() {
					var projectsListWrap = $('.projects-list', $projectsContainer ),
						projectsList = $('.projects-item', $projectsContainer );

						projectsList.css( {
							'margin-bottom': +projectsSettings['item-margin']
						} );
				}

				/**
				 * GetCascadingIndex
				 */
				function getCascadingIndex( index ) {
					var index = index || 0,
						map = [],
						counter = 0,
						mapIndex = 0;

						switch ( getResponsiveLayout() ) {
							case 'xl':
								map = cherryProjectsObjects.cascadingListMap || [ 1, 2, 2, 3, 3, 3, 4, 4, 4, 4 ];
								break
							case 'lg':
								map = [ 1, 2, 2, 3, 3, 3 ];
								break
							case 'md':
								map = [ 1, 2, 2 ];
								break
							case 'sm':
								map = [ 1, 2, 2 ];
								break
							case 'xs':
								map = [ 1 ];
								break
						}

						for ( var i = 0; i < index; i++ ) {
							counter++;

							if ( counter === map.length ) {
								counter = 0;
							}

							mapIndex = counter;
						};

						return map[ mapIndex ];
				}

				/*
				 * Show listing animation
				 */
				function showAnimation( startIndex, delta ){
					var counter = 1;
					$( '.projects-item', $projectsContainer ).each( function() {
						if ( $( this ).index() >= startIndex ) {
							showProjectsItem( $( this ), delta * parseInt( counter ) );
							counter++;
						}
					} );

				}

				function showProjectsItem( item, delay ) {
					var timeOutInterval = setTimeout( function() {
						item.removeClass( 'animate-cycle-show' );
					}, delay );
				}

				/*
				 * Hide listing animation
				 */
				function hideAnimation( delta ) {
					$( '.projects-item', $projectsContainer ).each( function() {
						hideProjectsItem( $( this ), delta * parseInt( $( this ).index() + 1 ) );
					} )

				}

				function hideProjectsItem( item, delay ) {
					var timeOutInterval = setTimeout( function() {
						item.addClass( 'animate-cycle-hide' );
					}, delay );
				}

				function getResponsiveColumn() {
					var columnPerView = +projectsSettings['column-number'],
						widthLayout   = getResponsiveLayout();

					switch ( widthLayout ) {
						case 'xl':
							columnPerView = +projectsSettings['column-number'];
							break
						case 'lg':
							columnPerView = Math.ceil( +projectsSettings['column-number'] / 2 );
							break
						case 'md':
							columnPerView = Math.ceil( +projectsSettings['column-number'] / 3 );
							break
						case 'sm':
							columnPerView = Math.ceil( +projectsSettings['column-number'] / 4 );
							break
						case 'xs':
							columnPerView = 1;
							break
					}

					return columnPerView;
				}

				function getResponsiveLayout() {
					var windowWidth   = $( window ).width(),
						widthLayout   = 'xs';

					if ( windowWidth >= 600 ) {
						widthLayout = 'sm';
					}

					if ( windowWidth >= 900 ) {
						widthLayout = 'md';
					}

					if ( windowWidth >= 1200 ) {
						widthLayout = 'lg';
					}

					if ( windowWidth >= 1600 ) {
						widthLayout = 'xl';
					}

					return widthLayout;
				}

			});
		},
		destroy: function() {},
		update: function( content ) {}
	};

	$.fn.cherryProjectsPlugin = function( method ) {
		if ( methods[method] ) {
			return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ) );
		} else if ( typeof method === 'object' || ! method ) {
			return methods.init.apply( this, arguments );
		} else {
			$.error( 'Method with name ' + method + ' is not exist for jQuery.cherryProjectsPlugin' );
		}
	}//end plugin

})(jQuery)


 // fin cherry projects plugins 

 (function($){
	"use strict";

	CherryJsCore.utilites.namespace('cherryProjectsFrontScripts');
	CherryJsCore.cherryProjectsFrontScripts = {
		init: function () {
			CherryJsCore.variable.$document.on( 'ready', this.readyRender.bind( this ) );
		},

		readyRender: function () {
			this.projectsPluginInit( this );
			this.magnificInit( this );
			this.projectsTermsInit();
		},

		projectsPluginInit: function() {
			if ( $( '.cherry-projects-wrapper' )[0] ) {
				$( '.cherry-projects-wrapper' ).cherryProjectsPlugin( {} );
			}
		},

		magnificIconInit: function() {
			if ( $( '.zoom-link' )[0] ){
				$( '.zoom-link' ).magnificPopup({type: 'image'});
			}
		},

		magnificInit: function() {
			$( '.cherry-projects-wrapper' ).magnificPopup({
				delegate: '.featured-image a',
				type: 'image',
				gallery: {
					enabled: true
				},
				mainClass: 'mfp-with-zoom',
				zoom: {
					enabled: true,
					duration: 300,
					easing: 'ease-in-out',
					opener: function(openerElement) {
						return openerElement.is('img') ? openerElement : openerElement.find('img');
					}
				}
			});
		},

		projectsTermsInit: function() {
			var self                  = this,
				$projectsTermWrapper  = $( '.cherry-projects-terms-wrapper' ),
				$projectsTermInstance = $( '.projects-terms-container', $projectsTermWrapper ),
				$loader               = $( '.cherry-projects-ajax-loader' , $projectsTermWrapper );

			$loader.css( { 'display': 'block' } ).fadeTo( 500, 1 );

			$projectsTermInstance.each( function( index ) {
				var $instance        = $( this ),
					instanceSettings = $instance.data( 'settings' ),
					columnNumber     = self.getResponsiveColumn( +instanceSettings['column-number'] );

					$instance.imagesLoaded( function() {
						$loader.fadeTo( 500, 0, function() { $( this ).css( { 'display': 'none' } ); } );
						self.showAnimation( $( '.projects-terms-item', $instance ), 0, 100 );
					} );

					switch ( instanceSettings['list-layout'] ) {
						case 'grid-layout':
							self.gridLayoutRender( $instance, columnNumber, instanceSettings['item-margin'] );
						break;
						case 'masonry-layout':
							self.masonryLayoutRender( $instance, columnNumber, instanceSettings['item-margin'] );
						break;
						case 'cascading-grid-layout':
							self.cascadingGridLayoutRender( $instance, instanceSettings['item-margin'] );
						break;
					}

					jQuery( window ).on( 'resize.projects_layout_resize', function() {
						var columnNumber = self.getResponsiveColumn( +instanceSettings['column-number'] );

						switch ( instanceSettings['list-layout'] ) {
							case 'grid-layout':
								self.gridLayoutRender( $instance, columnNumber, instanceSettings['item-margin'] );
							break;
							case 'masonry-layout':
								self.masonryLayoutRender( $instance, columnNumber, instanceSettings['item-margin'] );
							break;
							case 'cascading-grid-layout':
								self.cascadingGridLayoutRender( $instance, instanceSettings['item-margin'] );
							break;
						}
					} );
			} );
		},

		gridLayoutRender: function( instance, columnNumber, margin ) {
			var $itemlist = $( '.projects-terms-item', instance );

			$itemlist.each( function( index ) {
				var $this     = $( this ),
					itemWidth = ( 100 / columnNumber ).toFixed(5);

				$this.css( {
					'-webkit-flex-basis': itemWidth + '%',
					'flex-basis': itemWidth + '%',
					'width': itemWidth + '%',
					'margin-bottom': margin + 'px'
				} );

				$('.inner-wrapper', $this ).css( {
					'margin': ( +margin / 2 ).toFixed(2) + 'px'
				} );

			});
		},

		masonryLayoutRender: function( instance, columnNumber, margin ) {
			var $itemlist = $( '.projects-terms-item', instance );

			$( '.projects-terms-list', instance ).css( {
				'-webkit-column-count': columnNumber,
				'column-count': columnNumber,
				'-webkit-column-gap': +margin,
				'column-gap': +margin,
			} );

			$( '.inner-wrapper', $itemlist ).css( {
				'margin-bottom': +margin
			} );
		},

		cascadingGridLayoutRender: function( instance, marginItem ) {
			var $itemlist = $( '.projects-terms-item', instance ),
				self = this;

			$itemlist.each( function( index ) {
					var $this    = $( this ),
						newWidth = ( 100 / getCascadingIndex( index, self ) ).toFixed( 2 ),
						margin   = Math.ceil( +marginItem / 2 );

					$this.css( {
						'width': +newWidth + '%',
						'max-width': +newWidth + '%'
					} );

					$('.inner-wrapper', $this ).css( {
						'margin': margin + 'px'
					} );
				}
			);

			function getCascadingIndex ( index, self ) {
				var index = index || 0,
					map = [],
					counter = 0,
					mapIndex = 0;

					switch ( self.getResponsiveLayout() ) {
						case 'xl':
							map = cherryProjectsTermObjects.cascadingListMap || [ 1, 2, 2, 3, 3, 3, 4, 4, 4, 4 ];
							break
						case 'lg':
							map = [ 1, 2, 2, 3, 3, 3 ];
							break
						case 'md':
							map = [ 1, 2, 2 ];
							break
						case 'sm':
							map = [ 1, 2, 2 ];
							break
						case 'xs':
							map = [ 1 ];
							break
					}

					for ( var i = 0; i < index; i++ ) {
						counter++;

						if ( counter === map.length ) {
							counter = 0;
						}

						mapIndex = counter;
					};

					return map[ mapIndex ];
			}
		},

		showAnimation: function( itemlist, startIndex, delta ) {
			var counter = 1;

			itemlist.each( function() {
				var $this = $( this ),
				timeOutInterval;

				timeOutInterval = setTimeout( function() {
					$this.removeClass( 'animate-cycle-show' );
				}, delta * parseInt( counter ) );

				counter++;
			} );

		},

		getResponsiveColumn: function( columns ) {
			var columnPerView = +columns,
				widthLayout   = this.getResponsiveLayout();

			switch ( widthLayout ) {
				case 'xl':
					columnPerView = +columns;
					break
				case 'lg':
					columnPerView = Math.ceil( columnPerView / 2 );
					break
				case 'md':
					columnPerView = Math.ceil( columnPerView / 3 );
					break
				case 'sm':
					columnPerView = Math.ceil( columnPerView / 4 );
					break
				case 'xs':
					columnPerView = 1;
					break
			}
			return columnPerView;
		},

		getResponsiveLayout: function() {
			var windowWidth   = $( window ).width(),
				widthLayout   = 'xs';

			if ( windowWidth >= 600 ) {
				widthLayout = 'sm';
			}

			if ( windowWidth >= 900 ) {
				widthLayout = 'md';
			}

			if ( windowWidth >= 1200 ) {
				widthLayout = 'lg';
			}

			if ( windowWidth >= 1600 ) {
				widthLayout = 'xl';
			}

			return widthLayout;
		},


	}
	CherryJsCore.cherryProjectsFrontScripts.init();
}(jQuery));



// fin cherry projects 

/* <![CDATA[ */
var wp_load_style = ["cherry-testi.css","jquery-swiper.css","booked-tooltipster.css","booked-tooltipster-theme.css","booked-animations.css","booked-styles.css","booked-responsive.css","cherry-services.css","cherry-services-theme.css","cherry-services-grid.css","font-awesome.css","mp-restaurant-menu-font.css","mprm-style.css","dashicons.css","magnific-popup.css","cherry-projects-styles.css","cherry-google-fonts.css","linearicons.css","tm-builder-swiper.css","tm-builder-modules-style.css","cherry-team.css","cherry-team-grid.css","latteccino-theme-style.css"];
var wp_load_script = {"0":"cherry-js-core.js","1":"jquery.js","2":"jquery-ui-datepicker.js","4":"booked-spin-js.js","5":"booked-spin-jquery.js","6":"booked-tooltipster.js","7":"booked-functions.js","8":"tm-builder-modules-global-functions-script.js","9":"jquery-swiper.js","10":"wp-util.js","11":"magnific-popup.js","12":"cherry-projects-single-scripts.js","13":"cherry-post-formats.js","14":"google-maps-api.js","15":"divi-fitvids.js","16":"waypoints.js","17":"tm-jquery-touch-mobile.js","18":"tm-builder-frontend-closest-descendent.js","19":"tm-builder-frontend-reverse.js","20":"tm-builder-frontend-simple-carousel.js","21":"tm-builder-frontend-simple-slider.js","22":"tm-builder-frontend-easy-pie-chart.js","23":"tm-builder-frontend-tm-hash.js","24":"tm-builder-modules-script.js","25":"tm-builder-swiper.js","26":"fittext.js","27":"latteccino-theme-script.js"};
var cherry_ajax = "c040722b04";
var ui_init_object = {"auto_init":"false","targets":[]};
/* ]]> */




function CherryCSSCollector(){"use strict";var t,e=window.CherryCollectedCSS;void 0!==e&&(t=document.createElement("style"),t.setAttribute("title",e.title),t.setAttribute("type",e.type),t.textContent=e.css,document.head.appendChild(t))}CherryCSSCollector();





jQuery(document).ready(function(jQuery){jQuery.datepicker.setDefaults({"closeText":"Close","currentText":"Today","monthNames":["January","February","March","April","May","June","July","August","September","October","November","December"],"monthNamesShort":["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"nextText":"Next","prevText":"Previous","dayNames":["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],"dayNamesShort":["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],"dayNamesMin":["S","M","T","W","T","F","S"],"dateFormat":"MM d, yy","firstDay":1,"isRTL":false});});



/* <![CDATA[ */
var booked_js_vars = {"ajax_url":"http:\/\/ld-wp.template-help.com\/wordpress_63569\/wp-admin\/admin-ajax.php","profilePage":"","publicAppointments":"","i18n_confirm_appt_delete":"Are you sure you want to cancel this appointment?","i18n_please_wait":"Please wait ...","i18n_wrong_username_pass":"Wrong username\/password combination.","i18n_fill_out_required_fields":"Please fill out all required fields.","i18n_guest_appt_required_fields":"Please enter your name to book an appointment.","i18n_appt_required_fields":"Please enter your name, your email address and choose a password to book an appointment.","i18n_appt_required_fields_guest":"Please fill in all \"Information\" fields.","i18n_password_reset":"Please check your email for instructions on resetting your password.","i18n_password_reset_error":"That username or email is not recognized."};
/* ]]> */



/* <![CDATA[ */
var _wpUtilSettings = {"ajax":{"url":"\/wordpress_63569\/wp-admin\/admin-ajax.php"}};
/* ]]> */







/* <![CDATA[ */
var tm_pb_custom = {"ajaxurl":"http:\/\/ld-wp.template-help.com\/wordpress_63569\/wp-admin\/admin-ajax.php","images_uri":"http:\/\/ld-wp.template-help.com\/wordpress_63569\/wp-content\/themes\/latteccino\/images","builder_images_uri":"http:\/\/ld-wp.template-help.com\/wordpress_63569\/wp-content\/plugins\/power-builder\/framework\/assets\/images","tm_frontend_nonce":"4ee51ad337","subscription_failed":"Please, check the fields below to make sure you entered the correct information.","fill_message":"Please, fill in the following fields:","contact_error_message":"Please, fix the following errors:","invalid":"Invalid email","captcha":"Captcha","prev":"Prev","previous":"Previous","next":"Next","wrong_captcha":"You entered the wrong number in captcha.","is_builder_plugin_used":"1","is_divi_theme_used":"","widget_search_selector":".widget_search"};
/* ]]> */

  


/* <![CDATA[ */
var latteccino = {"ajaxurl":"http:\/\/ld-wp.template-help.com\/wordpress_63569\/wp-admin\/admin-ajax.php","labels":{"totop_button":"","header_layout":"style-6"},"more_button_options":{"more_button_type":"text","more_button_text":"More","more_button_icon":"fa-arrow-down","more_button_image_url":null,"retina_more_button_image_url":null},"toTop":"1"};
/* ]]> */






/* <![CDATA[ */
var cherryProjectsObjects = {"ajax_url":"http:\/\/ld-wp.template-help.com\/wordpress_63569\/wp-admin\/admin-ajax.php","cascadingListMap":[2,2,3,3,3,4,4,4,4]};
/* ]]> */


