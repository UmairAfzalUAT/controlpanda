<?php 

return array(
	'name' => 'babyblog',
	'color' => 'pink',
	'category' => 'Landing Page',
	'theme'  => 'Yeti',
	'libraries' => array('Classie', 'Animated Header', 'Bootstrap Validation', 'Jquery Easing'),
);