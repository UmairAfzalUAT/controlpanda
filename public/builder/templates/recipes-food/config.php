<?php 

return array(
	'name' => 'Food Blogs',
	'color' => 'Orange',
	'category' => 'Food Blog',
	'theme'  => 'Food Blogs',
	'libraries' => array('Classie', 'Animated Header', 'Bootstrap Validation', 'Jquery Easing'),
);