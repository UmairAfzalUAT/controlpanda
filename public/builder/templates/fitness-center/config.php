<?php 

return array(
	'name' => 'fitness center',
	'color' => 'Green',
	'category' => 'Gym/Fitness Centre',
	'theme'  => 'fitness center',
	'libraries' => array('Classie', 'Animated Header', 'Bootstrap Validation', 'Jquery Easing'),
);