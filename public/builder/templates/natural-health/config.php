<?php 

return array(
	'name' => 'Weightloss ',
	'color' => 'Blue',
	'category' => 'Healthy Living 2 ',
	'theme'  => 'natural health',
	'libraries' => array('Classie', 'Animated Header', 'Bootstrap Validation', 'Jquery Easing'),
);