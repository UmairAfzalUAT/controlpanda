<?php 

return array(
	'name' => 'Medical',
	'color' => 'light Blue',
	'category' => 'Health Services ',
	'theme'  => 'Medical',
	'libraries' => array('Classie', 'Animated Header', 'Bootstrap Validation', 'Jquery Easing'),
);