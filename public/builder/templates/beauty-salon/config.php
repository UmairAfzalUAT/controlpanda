<?php 

return array(
	'name' => 'Beauty Salon',
	'color' => 'Dark Orange',
	'category' => 'Beauty ',
	'theme'  => 'Hair & Beauty Salon',
	'libraries' => array('Classie', 'Animated Header', 'Bootstrap Validation', 'Jquery Easing'),
);