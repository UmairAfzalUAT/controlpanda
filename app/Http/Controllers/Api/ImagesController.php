<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Images;
use App\ImagesCategories;


class ImagesController extends Controller {
    private $request;

    public function __construct(Request $request) {
        $this->request = $request;
    }

    public function img_cats() {
        $img_cats = ImagesCategories::where('status',1)->get();
        echo json_encode($img_cats);
    }
    public function all_images() {
        $data['all_images'] = Images::get();
        $data['cat_images'] = ImagesCategories::with('images')->get();
        echo json_encode($data); exit;
    }
    public function shutterstock_images_categories(){
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:7.0.1) Gecko/20100101 Firefox/7.0.1');
        curl_setopt($ch, CURLOPT_URL, "https://api.shutterstock.com/v2/images/categories");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

        curl_setopt($ch, CURLOPT_USERPWD, "ca023-06b4a-8d0f8-aeadf-5ab92-40d9a:f0d3c-134c8-2ece4-11297-e560a-2cccc");

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close ($ch);
        echo json_encode(json_decode($result)); exit;
    }
    public function shutterstock_images(Request $request){
        $input = $this->request->all();
        $category_id = $input['category_id'];
        $page = $input['page'];
        $per_page = $input['per_page'];
        $query_string = '?page='.$page.'&per_page='.$per_page;
        if($category_id != '' && $category_id != 'all'){
            $query_string = $query_string . '&category='.$category_id;
        }
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:7.0.1) Gecko/20100101 Firefox/7.0.1');
        curl_setopt($ch, CURLOPT_URL, "https://api.shutterstock.com/v2/images/search".$query_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

        curl_setopt($ch, CURLOPT_USERPWD, "ca023-06b4a-8d0f8-aeadf-5ab92-40d9a:f0d3c-134c8-2ece4-11297-e560a-2cccc");

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close ($ch);
        echo json_encode(json_decode($result)); exit;
    }

}
