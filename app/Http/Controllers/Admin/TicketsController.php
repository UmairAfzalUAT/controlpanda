<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\DbModel;
use App\HelpArticles;
use Illuminate\Http\Request;
use Alert;
use Auth;

class TicketsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index() {
        not_permissions_redirect(have_premission(80));
        $data['cats'] = \DB::table('tickets')->paginate(20);
        return view('admin.helpArticles.tickets')->with($data);
    }
    
    

    public function view_ticket($id = 0) {
        $data['ticket_id'] = $id;
        $data['replay'] = \DB::select("SELECT tickets.question AS question, tickets_replaies.*, users.first_name, users.last_name, users.profile_image  from tickets
		inner join tickets_replaies on tickets.id = tickets_replaies.ticket_id
		inner join users on tickets_replaies.user_id = users.id
		WHERE tickets_replaies.ticket_id='" . $id . "' order by  tickets_replaies.id ");
        return view('admin.helpArticles.tickets_view')->with($data);
    }

    public function createticket(Request $request) {
        $input = $request->all();
        $data['question'] = $input['q'];
        $data['preority'] = 1;
        $data['user_id'] = Auth::id();
        $data['status'] = 1;
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['updated_at'] = date('Y-m-d H:i:s');
        \DB::table('tickets')->insert($data);
        //Alert::success('Success Message', 'Ticket Successfully Submited')->autoclose(3000);
        echo 1;
        //return redirect('admin/tickets');
    }
    
    public function replyticket(Request $request) {
        $input = $request->all();

        $data['ticket_id'] = $input['ticket_id'];
        $data['replay'] = $input['reply'];
        $data['user_id'] = Auth::id();
        $data['images'] = 0;
        $data['status'] = 1;
        $data['created_at'] = date('Y-m-d H:i:s');
        \DB::table('tickets_replaies')->insert($data);
        Alert::success('Success Message', 'Reply Successfully Submit..')->autoclose(3000);
        return redirect('admin/tickets');
    }

}
