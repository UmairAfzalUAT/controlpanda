<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\HelpArticles;
use App\DbModel;
use Illuminate\Http\Request;
use Alert;
use Image;
use File;

class HelpArticlesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index() {
        not_permissions_redirect(have_premission(102));
        $data['cats'] = HelpArticles::paginate(20);
        return view('admin.helpArticles.index')->with($data);
    }

    public function create() {
        not_permissions_redirect(have_premission(99));
        $data['action'] = "Add";
        return view('admin.helpArticles.edit')->with($data);
    }

    public function edit($id) {
        not_permissions_redirect(have_premission(100));
        $data['category'] = HelpArticles::findOrFail($id);
        $data['action'] = "Edit";
        return view('admin.helpArticles.edit')->with($data);
    }

    public function store(Request $request) {

        $input = $request->all();
		$input['user_id'] = \Auth::user()->id;
			
        if ($input['action'] == 'Edit') {
            not_permissions_redirect(have_premission(100));
            $HelpArticles = HelpArticles::findOrFail($input['id']);
            $HelpArticles->update($input);
            $old_image = $HelpArticles->images;
            $slug = DbModel::unique_slug('help_articles', $input['slug'], $input['id']);
            if ($slug) {
                $blog_update['slug'] = $slug->slug . '-' . $input['id'];
            }
            $photo = "";
            if (isset($_FILES['images']['name']) && $_FILES['images']['size'] > 0) {
                $ext = strtolower(pathinfo($_FILES['images']['name'], PATHINFO_EXTENSION));
                $photo = $input['id'] . '-' . uniqid() . '.' . $ext;
                $destinationPath = "uploads/helparticles/" . $photo;
                move_uploaded_file($_FILES['images']['tmp_name'], $destinationPath);
                if ($old_image) {
                    File::delete("uploads/helparticles/" . $old_image);
                }
            } else {
                $photo = $old_image;
            }
            $blog_update['images'] = $photo;
            $HelpArticles->update($blog_update);
            Alert::success('Success Message', 'Help Articles updated successfully!')->autoclose(3000);
        } else {
            not_permissions_redirect(have_premission(99));
            
			unset($input['action']);
			unset($input['_token']);
			$input['created_at'] = date('Y-m-d H:i:s');
			$input['updated_at'] = date('Y-m-d H:i:s');
			
			$id = \DB::table('help_articles')->insertGetId($input);

            $slug = DbModel::unique_slug('help_articles', $input['slug'], $id);
            if ($slug) {
                $blog_update['slug'] = $slug->slug . '-' . $id;
            }

            $photo = "";
            if (isset($_FILES['images']['name']) && $_FILES['images']['size'] > 0) {
                $ext = strtolower(pathinfo($_FILES['images']['name'], PATHINFO_EXTENSION));
                $photo = $input['id'] . '-' . uniqid() . '.' . $ext;
                $destinationPath = "uploads/helparticles/" . $photo;
                move_uploaded_file($_FILES['images']['tmp_name'], $destinationPath);
            }
            $blog_update['images'] = $photo;
            $HelpArticles = HelpArticles::findOrFail($id);
            $HelpArticles->update($blog_update);


            Alert::success('Success Message', 'Help Articles added successfully!')->autoclose(3000);
        }

        if ((have_premission(74))) {
            return redirect('admin/help-articles');
        } else {
            return redirect('admin/dashboard');
        }
    }

    public function destroy($id) {
        not_permissions_redirect(have_premission(76));
        HelpArticles::destroy($id);
        Alert::success('Success Message', 'Help Articles deleted successfully!')->autoclose(3000);
        return redirect('admin/help-articles');
    }

}
