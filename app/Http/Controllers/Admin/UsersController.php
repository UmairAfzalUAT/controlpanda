<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\DbModel;
use App\PackageOrders;
use App\Notifications;
use Illuminate\Http\Request;
use Socialite;
use Illuminate\Support\Facades\Validator;
use Auth;
use Alert;
use Image;
use Hash;
use File;
use View;
use Session;

class UsersController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index() {
        not_permissions_redirect(have_premission(32));
        $data['users'] = User::join('roles', 'roles.id', '=', 'users.role')->select('roles.title as role_name', 'users.*')->paginate(20);
        $data['total'] = User::join('roles', 'roles.id', '=', 'users.role')->select('roles.title as role_name', 'users.*')->count();
        return view('admin.users.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        not_permissions_redirect(have_premission(33));
        $user = Auth::user();
        $data['action'] = "Add";
        $data['countries'] = \DB::table('countries')->get();
        $data['roles'] = \DB::table('roles')->where('id', '>=', $user->role)->where('is_active', 1)->get();
        return view('admin.users.edit')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        not_permissions_redirect(have_premission(34));
        $data['user'] = User::findOrFail($id);
        $data['countries'] = \DB::table('countries')->get();
        $data['action'] = "Edit";
        $data['roles'] = \DB::table('roles')->where('is_active', 1)->get();
        return view('admin.users.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {

        $input = $request->all();

        if (isset($input['new_password'])) {
            $input['password'] = Hash::make($input['new_password']);
            $input['original_password'] = $input['new_password'];
        }
        if ($input['action'] == 'Add') {
            $user_data = User::select('id')->where('email', $input['email'])->first();
            if (count($user_data) > 0) {
                Alert::error('Error Message', 'Email already exist for other user!')->autoclose(3000);
                return redirect('admin/users/create');
            }
        } else {
            $user_data = User::select('id')->where('email', $input['email'])->first();
            if ($input['id'] != $user_data['id']) {
                Alert::error('Error Message', 'Email already exist for other user!')->autoclose(3000);
                return redirect('admin/users/create');
            }
        }
        if ($input['action'] == 'Edit') {
            not_permissions_redirect(have_premission(34));
            $User = User::findOrFail($input['id']);
            $User->update($input);

            Alert::success('Success Message', 'User updated successfully!')->autoclose(3000);
        } else {
            not_permissions_redirect(have_premission(33));

            $code_length = rand(20, 25);
            $unique_code = DbModel::unqiue_code($code_length);
            $input['unique_code'] = $unique_code;


            $temp=$input;

            unset($input['action']);
            unset($input['new_password']);
            unset($input['confirm_password']);

           $User = User::create($input);
        
            $input=$temp;
                 
            $template = \DB::table('email_templates')->where('template_type', 1)->where('email_type', 'signup')->first();
    

            if ($template != '') {

                $subject = $template->subject;
                $link = url("login");
                $to_replace = ['[FIRSTNAME]', '[LASTNAME]', '[EMAIL]', '[PASSWORD]', '[LINK]'];
                $with_replace = [$input['first_name'], $input['last_name'], $input['email'], $input['original_password'], $link];
                $html_body = '';
                $html_body .= str_replace($to_replace, $with_replace, $template->content);

                $mailContents = View::make('frontend.email_temp.template', ["data" => $html_body])->render();

            } else {

                // send email
                $subject = 'ControlPanda Credentail Details';
                $link = url("login");
                $html_body = '';
                $html_body .= "Hi " . $input['first_name'] . " " . $input['last_name'] . " ! " . "<br />";
                $html_body .= 'Your account has been created successffuly. Follwing are your credential details:' . "<br />";
                $html_body .= "Email: " . $input['email'] . "<br />";
                $html_body .= "Password: " . $input['original_password'] . "<br />";
                $html_body .= '<br><br>';
                $html_body .= "Please login to continue: " . '<a href="' . $link . '"> ' . $link . ' </a>';


                $mailContents = View::make('frontend.email_temp.template', ["data" => $html_body])->render();
            }
            //echo $mailContents; exit;
            $to = $input['email'];
           
            $returnpath = "";
            $cc = "";
          // hardcoded 1 instead of $User->id 
            $smtp = \DB::table("smtp_settings")->where("user_id", 1)->select('from_name', 'from_email')->first();

         //  echo'<pre>';print_r($smtp);exit;

            $dd = DbModel::SendHTMLMail($to, $subject, $mailContents, $smtp->from_email, $smtp->from_name, $returnpath, $cc);

      //      echo $dd;exit;
            Alert::success('Success Message', 'User added successfully!')->autoclose(3000);
        }

        if ((have_premission(32))) {
            return redirect('admin/users');
        } else {
            return redirect('admin/dashboard');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        not_permissions_redirect(have_premission(35));
        \DB::table('notification_users')->where('user_id', $id)->delete();
      //  \DB::table('templates')->where('user_id', $id)->delete();
        User::destroy($id);
        Alert::success('Success Message', 'User deleted!');
        return redirect('admin/users');
    }

    public function create_user() {
        echo Hash::make('123456');
    }

    public function account_details() {
        $data['user_sub_domain'] = Auth::user()->user_sub_domain;
        $data['user_sub_domain'] = explode('.', $data['user_sub_domain']);
        $data['user_sub_domain'] = $data['user_sub_domain'][0];
        $data['user_time_zone'] = Auth::user()->user_time_zone;
        $data['user_locale'] = Auth::user()->user_locale;
        $data['user_wordpress_api_key'] = Auth::user()->wordpress_api_key;

        $data['countries'] = \DB::table('countries')->get();

        return view('admin.users.account_details')->with($data);
    }

    public function my_profile() {
        $data['user'] = Auth::user();
        $data['countries'] = \DB::table('countries')->get();
        if (Auth::user()->role == 2) {
            $data['payment'] = DbModel::current_package(Auth::user()->id);
            $data['packages'] = DbModel::user_packages(Auth::user()->id);
        }
        return view('admin.users.my_profile')->with($data);
    }

    public function get_auth() {
        $data['user'] = Auth::user();
        echo json_encode($data);
    }

    public function update_wordpressapikey(Request $request) {
        $user = Auth::user();
        $input = $request->all();
        $user->update($input);
        Alert::success('Success Message', 'Your ControlPanda Wordpress API key updated successfully!')->autoclose(3000);
        return redirect('admin/account-details?action=wordpressapikey');
    }

    public function update_clickfunneldomain(Request $request) {
        $user = Auth::user();
        $input = $request->all();
        $user->update($input);
        Alert::success('Success Message', 'Your ControlPanda SubDomain updated successfully!')->autoclose(3000);
        return redirect('admin/account-details?action=clickfunnelssubdomain');
    }

    public function update_timezonelocale(Request $request) {
        $user = Auth::user();
        $input = $request->all();
        $user->update($input);
        Alert::success('Success Message', 'Your ControlPanda Time Zone and Locale updated successfully!')->autoclose(3000);
        return redirect('admin/account-details?action=TimezoneandLanguageSettings');
    }

    public function update_profile(Request $request) {
        $user = Auth::user();

        $old_image = $user->profile_image;
        $input = $request->all();
        $user->update($input);
        if ($request->hasFile('profile_image')) {
            $destinationPath = 'uploads/users'; // upload path
            $image = $request->file('profile_image'); // file
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $fileName = $user->id . '-' . time() . '.' . $extension; // renameing image
            $image->move($destinationPath, $fileName); // uploading file to given path
            $image = Image::make(sprintf($destinationPath . '/%s', $fileName))->resize(300, 300)->save();
            //remove old image
            if ($old_image) {
                File::delete($destinationPath . '/' . $old_image);
            }
            //insert image record            
            $user_image['profile_image'] = $fileName;
            $user->update($user_image);
        }
        Alert::success('Success Message', 'Your profile information updated successfully!')->autoclose(3000);
        return redirect('admin/my-profile');
    }

    public function update_password(Request $request) {
        $input = $request->all();
        $user = Auth::user();
        if ($input['current_password'] != $user['original_password']) {
            Alert::error('Error Message', 'Please enter correct password!')->autoclose(3000);
            return redirect('admin/my-profile?action=password');
        } else {
            $insertion_array = array('password' => Hash::make($input['new_password']), 'original_password' => $input['new_password']);
            $user->update($insertion_array);
            Alert::success('Success Message', 'Your password updated successfully!')->autoclose(3000);
            return redirect('admin/my-profile?action=password');
        }
    }

    public function update_authentication(Request $request) {
        $input = $request->all();
        $user = Auth::user();
        $insertion_array = array('unique_code' => $input['unique_code']);
        $user->update($insertion_array);
        Alert::success('Success Message', 'Your auth key updated successfully!')->autoclose(3000);
        return redirect('admin/my-profile?action=authentication');
    }

    public function set_prefrences(Request $request) {
        $user = Auth::user();
        $input = $request->all();
        $user->update($input);
    }

    public function update_paypal_details(Request $request) {

        $input = $request->all();
        $update = DbModel::update_paypal_details($input);
        Alert::success('Success Message', 'Your Paypal details have been updated Successfully!')->autoclose(3000);
        return redirect('admin/my-profile?action=payment');
    }

    public function update_card_details(Request $request) {

        $input = $request->only('cc_number', 'cc_exp_year', 'cc_exp_month', 'cc_cvv');
        $user = User::find(Auth::user()->id);
        $user->update($input);
        Alert::success('Success Message', 'Your credit card details have been updated Successfully!')->autoclose(3000);
        return redirect()->back();
    }

    public function showRegistrationForm($id, $name) {

        $user = Auth::user();
        if ($user != '') {
            return redirect('admin/dashboard');
        }
        $data['package_id'] = $id;
        $data['package_name'] = $name;
        return view('auth/register')->with($data);
    }

    public function post_register(Request $request) {

        $input = $request->all();


        $exist = User::Where('email', $input['email'])->first();
        if ($exist <> '') {
            $order_email = PackageOrders::Where('user_id', $exist->id)->first();
        }

        if (count($exist) > 0) {
            $msg = "Email you entered already exist for a user.";
            Session::flash("error", $msg);
            return redirect()->back()->withInput();
            ;
        }

        if ($exist <> '') {
            if (count($order_email) > 0) {
                $msg = "You have already requested for a free trial.";
                Session::flash("error", $msg);
                return redirect('signup')->withInput();
            }
        }

        if ($input['formtype'] == 'stripe') {
            $datas = array(
                'first_name' => $input['first_name'],
                'last_name' => $input['last_name'],
                'email' => $input['email'],
                'password' => Hash::make($input['password']),
                'original_password' => $input['password'],
                'role' => 1,
                'is_active' => 1,
                'cc_number' => $input['cc_number'],
                'cc_exp_month' => $input['cc_exp_month'],
                'cc_exp_year' => $input['cc_exp_year'],
                'cc_cvv' => $input['cc_cvv']
            );
        } else {

            $datas = array(
                'first_name' => $input['first_name'],
                'last_name' => $input['last_name'],
                'email' => $input['email'],
                'password' => Hash::make($input['password']),
                'role' => 1,
                'is_active' => 1,
                'original_password' => $input['password'],
                'paypal_email' => $input['paypal_email']
            );
        }
        $user_id = User::create($datas);
        $user_id = User::Where('email', $input['email'])->first();

        $package = \DB::table('packages')->where('id', $input['package_id'])->first();

        if ($package) {
            $input = array(
                'package_id' => $package->id,
                'user_id' => $user_id->id,
                'is_active' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'paid' => 0.00,
                'free_trial' => 1
            );

            $msg = "Thankyou for your interest. Your Free Trial started for " . $package->title . " Package. Login Please";
            Session::flash("success", $msg);

            $order_id = \DB::table('package_orders')->insertGetId($input);
            //$order_id = PackageOrders::create($input)->id;

            if ($order_id != '') {

                $user = User::Where('role', 1)->first();

                // Add Notification
                $notification_details = array(
                    'type' => 'new_signup',
                    'title' => 'New Signup',
                    'user_email' => $_POST['email'],
                    'package_title' => $package->title,
                    'package_id' => $package->id,
                    'order_id' => $order_id,
                );

                // echo 'user created successfully';
                $notification_id = Notifications::create($notification_details)->id;

                $notification_users = Notifications::add_users($notification_id, $user->id);

                return redirect()->back();
            }
        } else {
            $msg = "You can not signup because there in no free trial available.";
            Session::flash("error", $msg);
            return redirect()->back();
        }
    }

    public function redirectToProvider($type) {
        Session::put("signup_type", $type);
        return Socialite::driver('facebook')->redirect();
    }

    public function handleProviderCallback(Request $request) {
        $user = Socialite::driver('facebook')->stateless()->user();
        $type = '';
        if (Session::has('signup_type')) {
            $type = Session::get('signup_type');
        }


        $id = $user->getId();
        $name = $user->getName();
        $email = $user->getEmail();
        $avatar_url = $user->avatar;
        $gender = $user['gender'];

        $name_array = explode(" ", $name, 2);

        $firstName = $name_array[0];
        $lastName = $name_array[1];

        $exist = User::Where('email', $email)->first();

        $order_email = PackageOrders::Where('email', $email)->first();

        if ($type == 'login') {

            if (count($exist) > 0) {

                if ($exist->is_active == 1) {

                    Auth::login($exist, true);
                    return redirect('admin/dashboard-login');
                } else {

                    $msg = "Your account is not active.";
                    Session::flash("error", $msg);
                    return redirect('login');
                }
            } else {

                $msg = "These credentials do not match our records.";
                Session::flash("error", $msg);
                return redirect('login');
            }
        } else {

            if (count($exist) > 0) {
                $msg = "These credentials already exist for a user.";
                Session::flash("error", $msg);
                return redirect('signup');
            } elseif (count($order_email) > 0) {
                $msg = "You have already requested for a free trial.";
                Session::flash("error", $msg);
                return redirect('signup');
            } else {

                $package = \DB::table('packages')->where('price', 0)->first();

                if ($package) {
                    $input = array(
                        'package_id' => $package->id,
                        'first_name' => $firstName,
                        'last_name' => $lastName,
                        'email' => $email,
                        'package_title' => $package->title,
                        'duration_days' => $package->duration_days,
                        'duration_text' => $package->duration_text,
                        'price' => $package->price,
                        'is_active' => 1,
                    );
                    $msg = "Thankyou for your interest. We will soon contact you.";
                    Session::flash("success", $msg);
                    $order_id = PackageOrders::create($input)->id;

                    if ($order_id != '') {

                        $user = User::Where('role', 1)->first();

                        // Add Notification
                        $notification_details = array(
                            'type' => 'new_signup',
                            'title' => 'New Signup',
                            'user_email' => $email,
                            'package_title' => $package->title,
                            'package_id' => $package->id,
                            'order_id' => $order_id,
                        );

                        $notification_id = Notifications::create($notification_details)->id;

                        $notification_users = Notifications::add_users($notification_id, $user->id);


                        return redirect('signup');
                    }
                } else {
                    $msg = "You can not signup because there in no free trial available.";
                    Session::flash("error", $msg);
                    return redirect('signup');
                }

                return redirect('signup');
            }
        }
    }

    public function redirectToProviderGoogle($type) {
        Session::put("signup_type", $type);
        return Socialite::driver('google')->redirect();
    }

    public function handleProviderCallbackGoogle() {
        try {

            $user = Socialite::driver('google')->stateless()->user();
            $type = '';
            if (Session::has('signup_type')) {
                $type = Session::get('signup_type');
            }

            if ($user->getEmail() != null) {
                $email = $user->getEmail();

                $firstName = $user["name"]["givenName"];
                $lastName = $user["name"]["familyName"];
                $gender = $user["gender"];
                $image = $user->avatar;

                $exist = User::Where('email', $email)->first();
                $order_email = PackageOrders::Where('email', $email)->first();

                if ($type == 'login') {

                    if (count($exist) > 0) {

                        if ($exist->is_active == 1) {

                            Auth::login($exist, true);
                            return redirect('admin/dashboard-login');
                        } else {

                            $msg = "Your account is not active.";
                            Session::flash("error", $msg);
                            return redirect('login');
                        }
                    } else {

                        $msg = "These credentials do not match our records.";
                        Session::flash("error", $msg);
                        return redirect('login');
                    }
                } else {

                    if (count($exist) > 0) {
                        $msg = "These credentials already exist for a user.";
                        Session::flash("error", $msg);
                        return redirect('signup');
                    } elseif (count($order_email) > 0) {
                        $msg = "You have already requested for a free trial.";
                        Session::flash("error", $msg);
                        return redirect('signup');
                    } else {

                        $package = \DB::table('packages')->where('price', 0)->first();

                        if ($package) {
                            $input = array(
                                'package_id' => $package->id,
                                'first_name' => $firstName,
                                'last_name' => $lastName,
                                'email' => $email,
                                'package_title' => $package->title,
                                'duration_days' => $package->duration_days,
                                'duration_text' => $package->duration_text,
                                'price' => $package->price,
                                'is_active' => 1,
                            );

                            $msg = "Thankyou for your interest. We will soon contact you.";
                            Session::flash("success", $msg);
                            $order_id = PackageOrders::create($input)->id;

                            if ($order_id != '') {

                                $user = User::Where('role', 1)->first();

                                // Add Notification
                                $notification_details = array(
                                    'type' => 'new_signup',
                                    'title' => 'New Signup',
                                    'user_email' => $email,
                                    'package_title' => $package->title,
                                    'package_id' => $package->id,
                                    'order_id' => $order_id,
                                );

                                $notification_id = Notifications::create($notification_details)->id;

                                $notification_users = Notifications::add_users($notification_id, $user->id);


                                return redirect('signup');
                            }
                        } else {
                            $msg = "You can not signup because there in no free trial available.";
                            Session::flash("error", $msg);
                        }

                        return redirect('signup');
                    }
                }
            }
        } catch (\Exception $ex) {
            dd($ex);
            return redirect("/");
        }
    }

    public function redirectToProviderLinkedin($type) {
        Session::put("signup_type", $type);
        return Socialite::driver('linkedin')->redirect();
    }

    public function handleProviderCallbackLinkedin() {
        try {

            $user = Socialite::driver('linkedin')->stateless()->user();
            $type = '';
            if (Session::has('signup_type')) {
                $type = Session::get('signup_type');
            }

            if ($user["emailAddress"] != null) {
                $email = $user["emailAddress"];

                $firstName = $user["firstName"];
                $lastName = $user["lastName"];

                $exist = User::Where('email', $email)->first();
                $order_email = PackageOrders::Where('email', $email)->first();

                if ($type == 'login') {

                    if (count($exist) > 0) {

                        if ($exist->is_active == 1) {

                            Auth::login($exist, true);
                            return redirect('admin/dashboard-login');
                        } else {

                            $msg = "Your account is not active.";
                            Session::flash("error", $msg);
                            return redirect('login');
                        }
                    } else {

                        $msg = "These credentials do not match our records.";
                        Session::flash("error", $msg);
                        return redirect('login');
                    }
                } else {

                    if (count($exist) > 0) {
                        $msg = "These credentials already exist for a user.";
                        Session::flash("error", $msg);
                        return redirect('signup');
                    } elseif (count($order_email) > 0) {
                        $msg = "You have already requested for a free trial.";
                        Session::flash("error", $msg);
                        return redirect('signup');
                    } else {

                        $package = \DB::table('packages')->where('price', 0)->first();

                        if ($package) {
                            $input = array(
                                'package_id' => $package->id,
                                'first_name' => $firstName,
                                'last_name' => $lastName,
                                'email' => $email,
                                'package_title' => $package->title,
                                'duration_days' => $package->duration_days,
                                'duration_text' => $package->duration_text,
                                'price' => $package->price,
                                'is_active' => 1,
                            );

                            $msg = "Thankyou for your interest. We will soon contact you.";
                            Session::flash("success", $msg);
                            $order_id = PackageOrders::create($input)->id;

                            if ($order_id != '') {

                                $user = User::Where('role', 1)->first();

                                // Add Notification
                                $notification_details = array(
                                    'type' => 'new_signup',
                                    'title' => 'New Signup',
                                    'user_email' => $email,
                                    'package_title' => $package->title,
                                    'package_id' => $package->id,
                                    'order_id' => $order_id,
                                );

                                $notification_id = Notifications::create($notification_details)->id;

                                $notification_users = Notifications::add_users($notification_id, $user->id);


                                return redirect('signup');
                            }
                        } else {
                            $msg = "You can not signup because there in no free trial available.";
                            Session::flash("error", $msg);
                        }

                        return redirect('signup');
                    }
                }
            }
        } catch (\Exception $ex) {
            dd($ex);
            return redirect("/");
        }
    }

}
