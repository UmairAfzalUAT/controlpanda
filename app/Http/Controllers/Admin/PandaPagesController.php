<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Alert;
use App\Leads;
use App\EmailTemplates;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Services\TemplateLoader;
use App\Services\ThemesLoader;
use Auth;
use Analytics;
use Spatie\Analytics\Period;
use App\Services\ProjectRepository;

class PandaPagesController extends Controller {

    private $request;
    private $templateLoader;
    private $repository;

    public function __construct(Request $request, TemplateLoader $templateLoader, ThemesLoader $loader, ProjectRepository $repository) {
        $this->templateLoader = $templateLoader;
        $this->request = $request;
        $this->loader = $loader;
        $this->repository = $repository;
    }

    public function index(Request $request) {
        not_permissions_redirect(have_premission(1));

        $data['Projects'] = \DB::table('projects')
                        ->leftJoin('users_projects', 'projects.id', '=', 'users_projects.project_id')
                        ->select('projects.*', \DB::raw('(SELECT industries.title FROM industries WHERE industries.id = projects.ind_id) AS t_cat_name'))->orderBy('projects.id', 'desc')->where('users_projects.user_id', Auth::user()->id)->paginate(200);
        $data['user_id'] = Auth::user()->id;
        return view('admin.pandapages.index')->with($data);
    }

    public function create() {
        not_permissions_redirect(have_premission(2));

        $user_id = Auth::User()->id;

        $package = \DB::table('users as u')
                ->leftJoin('package_orders as o', 'o.user_id', '=', 'u.id')
                ->leftJoin('packages as p', 'p.id', '=', 'o.package_id')
                ->select('p.no_of_funnels')
                ->where('p.is_active', 1)
                ->where('o.user_id', $user_id)
                ->first();

        $no_of_project = \DB::table('users_projects')
                ->where('user_id', $user_id)
                ->count('project_id');

        if ($package->no_of_funnels != 'unlimited' && $no_of_project >= $package->no_of_funnels) {
            Alert::info('Info Message', 'Sorry your website limit reached. Please upgrade your account to create more websites. ')->persistent('Close');
            return redirect()->back();
        }

        //print_r($no_of_project);die;

        $data['categories'] = \DB::table('categories')->where('status', 1)->get();
        $data['industries'] = \DB::table('industries')->where('status', 1)->get();
        $data['template_categories'] = \DB::table('template_categories')->select('template_categories.*')->where('status', 1)->get();


        return view('admin.pandapages.create')->with($data);
    }

    public function edit($id) {
        not_permissions_redirect(have_premission(3));
       
        $user = Auth::user(); 
        $data['ProjectData'] = \DB::table('projects')->leftJoin('users_projects', 'projects.id', '=', 'users_projects.project_id')->select('projects.*', \DB::raw('(SELECT GROUP_CONCAT(domains.name) FROM domains WHERE domains.project_id = projects.id GROUP BY domains.project_id LIMIT 0,1) AS domain_names'), \DB::raw('(SELECT industries.title FROM industries WHERE industries.id = projects.ind_id) AS t_cat_name'))
                ->where('projects.id', $id)
                ->where('users_projects.user_id', Auth::user()->id)
                ->first();
        $data['domains'] = \App\Domains::where('user_id', $user->id)->get();
        $data['user_id'] = $user->id;
        $data['users'] = \DB::table('users')->where('is_active', 1)->select('id', 'first_name', 'last_name')->get();
        $data['project_leads'] = \DB::table('leads')->where('project_id', $id)->count('id');
        $data['email_lists'] = \DB::table('pages_email_lists')->where('project_name', $data['ProjectData']->name)->count('id');
        $data['seo'] = \DB::table('pages_seo_settings')->where('project_name', $data['ProjectData']->name)->count('id');
        $data['tracking_codes'] = \DB::table('pages_tracking_codes')->where('project_name', $data['ProjectData']->name)->count('id');
        //check contacts limit
        $package = \DB::table('users as u')
                ->leftJoin('package_orders as o', 'o.user_id', '=', 'u.id')
                ->leftJoin('packages as p', 'p.id', '=', 'o.package_id')
                ->select('p.no_of_contacts')
                ->where('p.is_active', 1)
                ->where('o.user_id', $user->id)
                ->first();
        $no_of_leads = \DB::table('leads')
                ->where('user_id', $user->id)
                ->count('id');

        if ($package->no_of_contacts != 'unlimited' && $no_of_leads >= $package->no_of_contacts) {
            Alert::warning('Warning Message', 'Your contacts limit reached. Please upgrade your account to create more contacts. ')->persistent('Close');
        }
        
        
        
        
        
         not_permissions_redirect(have_premission(12));
        $user = Auth::user();
        $data['ProjectData'] = \DB::table('projects')->leftJoin('users_projects', 'projects.id', '=', 'users_projects.project_id')->select('projects.*', \DB::raw('(SELECT GROUP_CONCAT(domains.name) FROM domains WHERE domains.project_id = projects.id GROUP BY domains.project_id LIMIT 0,1) AS domain_names'), \DB::raw('(SELECT industries.title FROM industries WHERE industries.id = projects.ind_id) AS t_cat_name'))->where('projects.id', $id)->where('users_projects.user_id', $user->id)->first();
        $data['user_id'] = $user->id;
        $data['contacts'] = Leads::select('leads.*', \DB::raw('(select name from `pages` WHERE pages.id = leads.page_id AND pageable_type = "Project") AS page_name'), \DB::raw('(select name from `projects` WHERE projects.id = leads.project_id) AS project_name'))->where('project_id', $id)->orderby('id', 'DESC')->paginate(20);
        
        
        
        
        
         //$data = array();
        $pages = array();
        not_permissions_redirect(have_premission(11));
        $user = Auth::user();
        $data['ProjectData'] = \DB::table('projects')->leftJoin('users_projects', 'projects.id', '=', 'users_projects.project_id')->select('projects.*', \DB::raw('(SELECT GROUP_CONCAT(domains.name) FROM domains WHERE domains.project_id = projects.id GROUP BY domains.project_id LIMIT 0,1) AS domain_names'), \DB::raw('(SELECT industries.title FROM industries WHERE industries.id = projects.ind_id) AS t_cat_name'))->where('projects.id', $id)->where('users_projects.user_id', $user->id)->first();
        $data['domain_data'] = \DB::table('domains')->where('project_id', $id)->first();
        //print_r($data['domain_data']);
        //if (count($data['domain_data'])) {
        if ($data['domain_data']) {
            $hosting_check = ',ga:hostname' . urlencode('==' . $data['domain_data']->name);
            $page_path_check = 'ga:pagePath' . urlencode("==/sites/" . $data['ProjectData']->name);
        } else {
            $hosting_check = '';
            $page_path_check = 'ga:pagePath' . urlencode("==/sites/" . $data['ProjectData']->name);
        }
        $data['thankyou'] = 0;
        $data['index'] = 0;

        $directory = '../public/storage/projects/' . $user->id . '/' . $data['ProjectData']->uuid . '/';
        if (glob($directory . "*.html") != false) {
            
            foreach (glob($directory . "*.html") as $pg) {
                $p = explode("/", $pg);
                $p = end($p);
                $p = explode(".", $p);
                $page = $p[0];
                $pages[] = $page;
                
                if ($page == 'index') {
                    $page_path_check = $page_path_check . ',' . $page_path_check . 'index/';
                    if ($hosting_check != '') {
                        $page_path_check .= ',ga:pagePath==/sites/';
                    }
                } else {
                    $page_path_check = $page_path_check . $page . '/';
                    if ($hosting_check != '') {
                        $page_path_check .= ',ga:pagePath==/' . $page . '/';
                    }
                }
                //$analyticsData = Analytics::performQuery(Period::months(1), 'ga:pageviews,ga:uniquePageviews', ['filters' => $page_path_check . $hosting_check]);
                
                if (isset($analyticsData['rows'][0])) {
                    $data['pages_views'][$page] = $analyticsData['rows'][0][0];
                    $data['pages_unique_views'][$page] = $analyticsData['rows'][0][1];
                    if ($page == 'thankyou') {
                        $data['thankyou'] = $analyticsData['rows'][0][0];
                    }
                    if ($page == 'index') {
                        $data['index'] = $analyticsData['rows'][0][0];
                    }
                } else {
                    $data['pages_views'][$page] = 0;
                    $data['pages_unique_views'][$page] = 0;
                }
            }
        }
        $data['pages'] = $pages;
        
        
        
        
        
        ///////////////////////////////// Automation ////////////////////////////////////
        
        
        not_permissions_redirect(have_premission(15));
        $user = Auth::user();
        $data['ProjectData'] = \DB::table('projects')->leftJoin('users_projects', 'projects.id', '=', 'users_projects.project_id')->select('projects.*', \DB::raw('(SELECT GROUP_CONCAT(domains.name) FROM domains WHERE domains.project_id = projects.id GROUP BY domains.project_id LIMIT 0,1) AS domain_names'), \DB::raw('(SELECT industries.title FROM industries WHERE industries.id = projects.ind_id) AS t_cat_name'))->where('projects.id', $id)->where('users_projects.user_id', $user->id)->first();
        $data['user_id'] = $user->id;

        $data['emailsdata'] = \DB::table('automation_action_lists')->where('project_id', $id)->where('action_type', 'email')->first();
        $data['textdata'] = \DB::table('automation_action_lists')->where('project_id', $id)->where('action_type', 'text')->first();

        $actions = \DB::table('automation_actions')->where('project_id', $id)->select('form_name')->get();
        $a = [];
        foreach ($actions as $ac)
            $a[] = $ac->form_name;

        $data['actions'] = $a;
        
        
        
        ///////////////////////////////////////     settings ////////////////////////////////////
        
        $pages1 = array();
         not_permissions_redirect(have_premission(14));
        $user = Auth::user();
        $data['ProjectDatas'] = \DB::table('projects')->leftJoin('users_projects', 'projects.id', '=', 'users_projects.project_id')->select('projects.*', \DB::raw('(SELECT GROUP_CONCAT(domains.name) FROM domains WHERE domains.project_id = projects.id GROUP BY domains.project_id LIMIT 0,1) AS domain_names'), \DB::raw('(SELECT industries.title FROM industries WHERE industries.id = projects.ind_id) AS t_cat_name'))->where('projects.id', $id)->where('users_projects.user_id', $user->id)->first();
        $data['user_id'] = $user->id;
        $project_name = $data['ProjectData']->name;
        $data['domains'] = \App\Domains::where('user_id', \Auth::user()->id)->get();
        $directory = '../public/storage/projects/' . $user->id . '/' . $data['ProjectData']->uuid . '/';
        
        if (glob($directory . "*.html") != false) {
            foreach (glob($directory . "*.html") as $pg) {
                $p = explode("/", $pg);
                $p = end($p);
                $p = explode(".", $p);
                $page = $p[0];
                $pages1[] = array("name" => $page, "tc" => \DB::table('pages_tracking_codes')->select('footer_code', 'header_code')->where('project_name', $project_name)->where("page_name", $page)->first());
            }
        }
       
        $data['paged'] = $pages1;
        
        
        
        
        
        //return view('admin.pandapages.edit')->with($data);
        return view('admin.pandapages.connection')->with($data);
    }

    public function store(Request $request) {
        
    }

    public function destroy($id) {
        
    }

    public function get_industry_templates(Request $request) {
        $input = $request->all();
        $templates = $this->templateLoader->loadAll();
        $total = count($templates);
        $perPage = 10;
        $page = $input['start'];
        $_SESSION['industry_id'] = $input['industry_id'];
        $templates = $templates->filter(function($template) {
            if (isset($template['config']['industries']) && in_array($_SESSION['industry_id'], $template['config']['industries'])) {
                return 1;
            }
        });

        $paginator = new LengthAwarePaginator(
                $templates->slice($page, $perPage)->values(), count($templates), $perPage, $page
        );

        foreach ($paginator as $temp) {

            echo '<div class="col-lg-3 col-md-6 col-sm-12"><a href="' . url('admin/panda-pages/' . $temp['name'] . '/' . $input['industry_id'] . '/get-pandapage') . '"><div class="home-img-as"><img src="' . asset($temp['thumbnail']) . '" alt="img-01" class="img-fluid"><div class="footer-img-home"><div class="img-mane">' . $temp['name'] . '</div><div class="page-number"><span>07</span>pages</div></div><div class="overlay"><a href="' . url('admin/create-project/' . $temp['name'] . '/' . $input['industry_id']) . '"><div class="text"><span><img src="'.url('assets/images/setting.svg').'" class="svg-tool"></span> <i>Edit Site / Preview</i></div></a></div></div></a></div>';
            //echo '<pre>';
            //print_r($temp);
            
//            echo '<div class="col-md-6 tiles-tc"><div class="ui card"><div class="header">' . $temp['config']['display_name'] . '</div><div class="dimmable image"><div class="ui dimmer"><div class="content"><div class="center"><a class="ui inverted button" href="' . url('admin/panda-pages/' . $temp['name'] . '/' . $input['industry_id'] . '/get-pandapage') . '">Preview</a><a class="ui inverted button" href="' . url('admin/create-project/' . $temp['name'] . '/' . $input['industry_id']) . '">Edit</a></div></div></div><img style="min-width: 100%;min-height: 100%;width: 100%;" src="' . asset($temp['thumbnail']) . '" alt="#"></div></div></div>';
        }
        if ($total > count($paginator) + $input['start']) {
            $new_start = $input['start'] + 10;
            echo '<div class="clear30 should_remove"></div><div class="col-md-12 should_remove text-center"><a data-start="' . $new_start . '" class="load_more_temp" href="javascript:void(0)"> Load More </a> <div class="clear30"></div></div>';
        }
    }

    public function get_themes(Request $request) {
        $themes = $this->loader->loadAll();
        foreach ($themes as $theme) {
            echo '<div class="col-md-4 tiles-tc"><div class="ui card"><div class="dimmable image"><div class="ui dimmer"><div class="content"><div class="center"><a data-value="' . $theme['name'] . '" class="ui inverted button select-theme" href="javascript:void(0)">' . ucfirst($theme['name']) . '</a></div></div></div><img src="' . asset($theme['thumbnail']) . '" alt="#"></div></div></div>';
        }
        echo '<div class="col-md-4 tiles-tc should_remove"><div class="ui card"><div class="dimmable image"><div class="ui dimmer"><div class="content"><div class="center"><a data-value="let-us-choose" class="ui inverted button select-theme" href="javascript:void(0)">Let us choose the best for you</a></div></div></div><img src="' . asset('images/default-theme.png') . '" alt="#"></div></div></div>';
    }

    public function get_industry_categories(Request $request) {
        $input = $request->all();
        $templates = \DB::table('template_categories')->whereRaw('id IN (SELECT cat_id FROM industry_categories WHERE ind_id = ' . $input['industry_id'] . ')')->get();
        $output = '<select class="form-control" name="category_id" id="category_id"><option  value="">Select Purpose</option>';
        foreach ($templates as $templ) {
            $output .= '<option  value="' . $templ->id . '">' . $templ->title . '</option>';
        }
        $output .= '</select>';
        echo $output;
    }

    public function ChooseTemplate($id) {
        $data['template_category'] = \DB::table('template_categories')->where('id', $id)->first();
        $data['templates'] = \DB::table('templates')->get();
        $data['templates_free'] = \DB::table('templates')->where('price', '<=', 0)->get();
        $data['templates_paid'] = \DB::table('templates')->where('price', '>', 0)->get();

        return view('admin.pandapages.ChooseTemplate')->with($data);
    }

    public function stats($id) {
        //$data = array();
        $pages = array();
        not_permissions_redirect(have_premission(11));
        $user = Auth::user();
        $data['ProjectData'] = \DB::table('projects')->leftJoin('users_projects', 'projects.id', '=', 'users_projects.project_id')->select('projects.*', \DB::raw('(SELECT GROUP_CONCAT(domains.name) FROM domains WHERE domains.project_id = projects.id GROUP BY domains.project_id LIMIT 0,1) AS domain_names'), \DB::raw('(SELECT industries.title FROM industries WHERE industries.id = projects.ind_id) AS t_cat_name'))->where('projects.id', $id)->where('users_projects.user_id', $user->id)->first();
        $data['domain_data'] = \DB::table('domains')->where('project_id', $id)->first();
        //print_r($data['domain_data']);
        //if (count($data['domain_data'])) {
        if ($data['domain_data']) {
            $hosting_check = ',ga:hostname' . urlencode('==' . $data['domain_data']->name);
            $page_path_check = 'ga:pagePath' . urlencode("==/sites/" . $data['ProjectData']->name);
        } else {
            $hosting_check = '';
            $page_path_check = 'ga:pagePath' . urlencode("==/sites/" . $data['ProjectData']->name);
        }
        $data['thankyou'] = 0;
        $data['index'] = 0;

        $directory = '../public/storage/projects/' . $user->id . '/' . $data['ProjectData']->uuid . '/';
        if (glob($directory . "*.html") != false) {
            foreach (glob($directory . "*.html") as $pg) {
                $p = explode("/", $pg);
                $p = end($p);
                $p = explode(".", $p);
                $page = $p[0];
                $pages[] = $page;
                if ($page == 'index') {
                    $page_path_check = $page_path_check . ',' . $page_path_check . 'index/';
                    if ($hosting_check != '') {
                        $page_path_check .= ',ga:pagePath==/sites/';
                    }
                } else {
                    $page_path_check = $page_path_check . $page . '/';
                    if ($hosting_check != '') {
                        $page_path_check .= ',ga:pagePath==/' . $page . '/';
                    }
                }
                $analyticsData = Analytics::performQuery(Period::months(1), 'ga:pageviews,ga:uniquePageviews', ['filters' => $page_path_check . $hosting_check]);
                if (isset($analyticsData['rows'][0])) {
                    $data['pages_views'][$page] = $analyticsData['rows'][0][0];
                    $data['pages_unique_views'][$page] = $analyticsData['rows'][0][1];
                    if ($page == 'thankyou') {
                        $data['thankyou'] = $analyticsData['rows'][0][0];
                    }
                    if ($page == 'index') {
                        $data['index'] = $analyticsData['rows'][0][0];
                    }
                } else {
                    $data['pages_views'][$page] = 0;
                    $data['pages_unique_views'][$page] = 0;
                }
            }
        }
        $data['pages'] = $pages;
        return view('admin.pandapages.stats')->with($data);
    }

    public function stats_ajax($id, Request $request) {
        $input = $request->all();

        $project = \DB::table('projects')->select('projects.*', \DB::raw('(SELECT templates.name FROM templates WHERE templates.id = projects.template_id) AS temp_name'), \DB::raw('(SELECT templates.thumbnail FROM templates WHERE templates.id = projects.template_id) AS temp_thumbnail'), \DB::raw('(SELECT industries.title FROM industries WHERE industries.id = projects.ind_id) AS t_cat_name'))->where('projects.id', $id)->first();
        $domain_ = \DB::table('domains')->where('project_id', $id)->first();
        if (count($domain_)) {
            $hosting_check = ',ga:hostname' . urlencode('==' . $domain_->name);
            $page_path_check = 'ga:pagePath' . urlencode("==/page/" . $project->name);
        } else {
            $hosting_check = '';
            $page_path_check = 'ga:pagePath' . urlencode("==/page/" . $project->name);
        }
        $pages_ = \DB::table('pages')->where('pageable_id', $project->id)->where('pageable_type', 'Project')->get();
        $thankyou = 0;
        $index = 0;
        foreach ($pages_ as $key => $pro) {
            if ($pro->name == 'index') {
                $page_path_check = $page_path_check . ',' . $page_path_check . 'index/';
                if ($hosting_check != '') {
                    $page_path_check .= ',ga:pagePath==/index/';
                }
            } else {
                $page_path_check = $page_path_check . $pro->name . '/';
                if ($hosting_check != '') {
                    $page_path_check .= ',ga:pagePath==/' . $pro->name . '/';
                }
            }
            if ($input["device_category"]) {
                $device_check = ';ga:deviceCategory==' . $input["device_category"];
            } else {
                $device_check = '';
            }

            $start_date = $input["start_date"];
            $start_date = explode("/", $start_date);
            $start_date = $start_date[2] . '-' . $start_date[1] . '-' . $start_date[0];
            $end_date = $input["end_date"];
            $end_date = explode("/", $end_date);
            $end_date = $end_date[2] . '-' . $end_date[1] . '-' . $end_date[0];
            $period = Period::create(new \DateTime($start_date), new \DateTime($end_date));
            $analyticsData = Analytics::performQuery($period, 'ga:pageviews,ga:uniquePageviews', ['filters' => $page_path_check . $hosting_check . $device_check]);

            if (isset($analyticsData['rows'][0])) {
                $pages_views[$pro->id] = $analyticsData['rows'][0][0];
                $pages_unique_views[$pro->id] = $analyticsData['rows'][0][1];
                if ($pro->name == 'thankyou') {
                    $thankyou = $analyticsData['rows'][0][0];
                }
                if ($pro->name == 'index') {
                    $index = $analyticsData['rows'][0][0];
                }
            } else {
                $pages_views[$pro->id] = 0;
                $pages_unique_views[$pro->id] = 0;
            }
        }
        foreach ($pages_ as $page) {
            if ($index == 0) {
                $p_val = '0.00';
            } else {
                $p_val = number_format(($thankyou / $index) * 100, 2);
            }
            echo '<tr class="funnelstep"><td class="opt_ins"><i class="fa fa-fw fa-envelope"></i>' . $page->name . '</td><td class="pageviews">' . $pages_views[$page->id] . '</td><td class="pageviews">' . $pages_unique_views[$page->id] . '</td><td class="opt_ins">' . $thankyou . '</td><td class="opt_ins">' . $p_val . ' %</td></tr>';
        }
    }

    public function contacts($id) {
        not_permissions_redirect(have_premission(12));
        $user = Auth::user();
        $data['ProjectData'] = \DB::table('projects')->leftJoin('users_projects', 'projects.id', '=', 'users_projects.project_id')->select('projects.*', \DB::raw('(SELECT GROUP_CONCAT(domains.name) FROM domains WHERE domains.project_id = projects.id GROUP BY domains.project_id LIMIT 0,1) AS domain_names'), \DB::raw('(SELECT industries.title FROM industries WHERE industries.id = projects.ind_id) AS t_cat_name'))->where('projects.id', $id)->where('users_projects.user_id', $user->id)->first();
        $data['user_id'] = $user->id;
        $data['contacts'] = Leads::select('leads.*', \DB::raw('(select name from `pages` WHERE pages.id = leads.page_id AND pageable_type = "Project") AS page_name'), \DB::raw('(select name from `projects` WHERE projects.id = leads.project_id) AS project_name'))->where('project_id', $id)->orderby('id', 'DESC')->paginate(20);

        return view('admin.pandapages.contacts')->with($data);
    }

    public function contacts_ajax($id, Request $request) {
        $input = $request->all();
        $where = ' leads.project_id = ' . $id . ' ';
        if (isset($input['date_period']) && $input['date_period'] != '') {
            $date_range = explode(' - ', $input['date_period']);
            $start_date = date("Y-m-d", strtotime($date_range[0])) . ' 00:00:00';
            $end_date = date("Y-m-d", strtotime($date_range[1])) . ' 23:59:59';
            $where .= ' AND leads.created_at >= "' . $start_date . '" AND leads.created_at <= "' . $end_date . '" ';
        }
        if (isset($input['search_keyword']) && $input['search_keyword'] != '') {
            $where .= ' AND ( ';
            $where .= ' LOWER(CONCAT(leads.title, " ",leads.first_name, " ", leads.last_name)) LIKE "%' . strtolower($input['search_keyword']) . '%" ';
            $where .= ' OR LOWER(leads.email) LIKE "%' . strtolower($input['search_keyword']) . '%" ';
            $where .= ' OR LOWER(leads.full_name) LIKE "%' . strtolower($input['search_keyword']) . '%" ';
            $splited_array = explode(" ", strtolower($input['search_keyword']));
            if (count($splited_array) > 1) {
                foreach ($splited_array as $single_val) {
                    if ($single_val != '') {
                        $where .= ' OR LOWER(CONCAT(leads.title, " ",leads.first_name, " ", leads.last_name)) LIKE "%' . $single_val . '%" ';
                        $where .= ' OR LOWER(leads.email) LIKE "%' . $single_val . '%" ';
                        $where .= ' OR LOWER(leads.full_name) LIKE "%' . $single_val . '%" ';
                    }
                }
            }
            $where .= ' ) ';
        }
        if (isset($input['page']) && $input['page'] != '') {
            $where .= ' AND leads.page = "' . $input['page'] . '" ';
        }


        $leads = Leads::select('leads.*', \DB::raw('(select name from `projects` WHERE projects.id = leads.project_id) AS project_name'))->whereRaw($where)->orderby('id', 'desc')->paginate(20);

        $output = '<div class="clear40"></div><table class="table table-hover table-nomargin no-margin table-bordered table-striped"><thead><tr><th>Name</th><th>Email</th><th>Phone</th><th>IP Address</th><th>Page</th><th>Status</th><th>Actions</th></tr></thead><tbody>';
        foreach ($leads as $cont) {
            if ($cont->lead_status == 1) {
                $status = '<label class="label label-success">Active</label>';
            } else {
                $status = '<label class="label label-danger">Inactive</label>';
            }
            $name = '';
            if ($cont->title) {
                $name .= $cont->title . ' ';
            }
            if ($cont->full_name) {
                $name .= $cont->full_name;
            } else {
                $name .= $cont->first_name . ' ' . $cont->last_name;
            }
            $output .= '<tr><td>' . $name . '</td><td>' . $cont->email . '</td><td>' . $cont->phone . '</td><td>' . $cont->ip_address . '</td><td>' . $cont->page . '</td><td>' . $status . '</td><td><a href="' . url('admin/contacts/' . $cont->id) . '"><i class="fa fa-edit fa-fw"></i></a><a target="_blank" href="' . url('page/' . $cont->project_name) . '"><i class="fa fa-eye fa-fw"></i></a></td></tr>';
        }
        $output .= '</tbody></table></div><nav class = "pull-right">' . $leads->render() . '</nav>';
        if (count($leads) == 0) {
            $output = '<div class="contact_cnt text-center"><h3>No Contacts Found</h3><p><em>Looks like you have not collected any contacts for this panda page in the selected date range.</em><br><br>Start by sending traffic to the beginning of this panda page or any opt-in page within this panda page. All your contacts will appear here to manage and view.</p></div>';
        }
        echo $output;
    }

    public function contacts_export($id, Request $request) {
        $input = $request->all();
        $where = ' leads.project_id = ' . $id . ' ';
        if (isset($input['date_period']) && $input['date_period'] != '') {
            $date_range = explode(' - ', $input['date_period']);
            $start_date = date("Y-m-d", strtotime($date_range[0])) . ' 00:00:00';
            $end_date = date("Y-m-d", strtotime($date_range[1])) . ' 23:59:59';
            $where .= ' AND leads.created_at >= "' . $start_date . '" AND leads.created_at <= "' . $end_date . '" ';
        }
        if (isset($input['search_keyword']) && $input['search_keyword'] != '') {
            $where .= ' AND ( ';
            $where .= ' LOWER(CONCAT(leads.title, " ",leads.first_name, " ", leads.last_name)) LIKE "%' . strtolower($input['search_keyword']) . '%" ';
            $where .= ' OR LOWER(leads.email) LIKE "%' . strtolower($input['search_keyword']) . '%" ';
            $where .= ' OR LOWER(leads.full_name) LIKE "%' . strtolower($input['search_keyword']) . '%" ';
            $splited_array = explode(" ", strtolower($input['search_keyword']));
            if (count($splited_array) > 1) {
                foreach ($splited_array as $single_val) {
                    if ($single_val != '') {
                        $where .= ' OR LOWER(CONCAT(leads.title, " ",leads.first_name, " ", leads.last_name)) LIKE "%' . $single_val . '%" ';
                        $where .= ' OR LOWER(leads.email) LIKE "%' . $single_val . '%" ';
                        $where .= ' OR LOWER(leads.full_name) LIKE "%' . $single_val . '%" ';
                    }
                }
            }
            $where .= ' ) ';
        }
        if (isset($input['page']) && $input['page'] != '') {
            $where .= ' AND leads.page = "' . $input['page'] . '" ';
        }


        $leads = Leads::select('leads.*', \DB::raw('(select name from `projects` WHERE projects.id = leads.project_id) AS project_name'))->whereRaw($where)->orderby('id', 'desc')->get();
        $header = array('id', 'ip_address', 'created_at', 'lead_status', 'title', 'first_name', 'last_name', 'full_name', 'company', 'designation', 'dob_day', 'dob_month', 'dob_year', 'phone', 'fax', 'email', 'address', 'city', 'zip');

        $head_row = array();
        foreach ($header as $head) {
            $head_row[] = strtoupper(str_replace('_', ' ', $head));
        }

        $file = fopen('uploads/leads_exports/export-leads.csv', 'w');
        fputcsv($file, $head_row);
        foreach ($leads as $lead) {
            $new_array = array();
            $new_row = array();
            if ($lead->lead_status == 1) {
                $lead_status = 'Active';
            } else {
                $lead_status = 'Inactive';
            }

            $new_row[0] = $lead->id;
            $new_row[1] = $lead->ip_address;
            $new_row[2] = $lead->created_at;
            $new_row[3] = $lead_status;
            $new_row[4] = $lead->title;
            $new_row[5] = $lead->first_name;
            $new_row[6] = $lead->last_name;
            $new_row[7] = $lead->full_name;
            $new_row[8] = $lead->company;
            $new_row[9] = $lead->designation;
            $new_row[10] = $lead->dob_day;
            $new_row[11] = $lead->dob_month;
            $new_row[12] = $lead->dob_year;
            $new_row[13] = $lead->phone;
            $new_row[14] = $lead->fax;
            $new_row[15] = $lead->email;
            $new_row[16] = $lead->address;
            $new_row[17] = $lead->city;
            $new_row[18] = $lead->zip;
            if ($lead->other_fields_values != '') {
                $other_all_fields = json_decode($lead['other_fields_values']);
                $j = 19;
                foreach ($other_all_fields as $field) {
                    $new_row[$j] = $field;
                    $j++;
                }
            }
            fputcsv($file, $new_row);
        }
        fclose($file);
        echo url('/uploads/leads_exports/export-leads.csv');
        exit();
    }

    public function settings($id) {
        not_permissions_redirect(have_premission(14));
        $user = Auth::user();
        $data['ProjectData'] = \DB::table('projects')->leftJoin('users_projects', 'projects.id', '=', 'users_projects.project_id')->select('projects.*', \DB::raw('(SELECT GROUP_CONCAT(domains.name) FROM domains WHERE domains.project_id = projects.id GROUP BY domains.project_id LIMIT 0,1) AS domain_names'), \DB::raw('(SELECT industries.title FROM industries WHERE industries.id = projects.ind_id) AS t_cat_name'))->where('projects.id', $id)->where('users_projects.user_id', $user->id)->first();
        $data['user_id'] = $user->id;
        $project_name = $data['ProjectData']->name;
        $data['domains'] = \App\Domains::where('user_id', \Auth::user()->id)->get();
        $directory = '../public/storage/projects/' . $user->id . '/' . $data['ProjectData']->uuid . '/';
        if (glob($directory . "*.html") != false) {
            foreach (glob($directory . "*.html") as $pg) {
                $p = explode("/", $pg);
                $p = end($p);
                $p = explode(".", $p);
                $page = $p[0];
                $pages[] = array("name" => $page, "tc" => \DB::table('pages_tracking_codes')->select('footer_code', 'header_code')->where('project_name', $project_name)->where("page_name", $page)->first());
            }
        }
        $data['pages'] = $pages;

        return view('admin.pandapages.settings')->with($data);
    }

    public function save_settings(Request $request) {
        $user = Auth::user();
        $project_name = $request['project_name'];

        $project = \DB::table('projects')->where('projects.name', $project_name)->first();
        $directory = '../public/storage/projects/' . $user->id . '/' . $project->uuid . '/';
        if (glob($directory . "*.html") != false) {
            $i = 0;
            foreach (glob($directory . "*.html") as $pg) {
                $pg_content = file_get_contents($pg);
                $p = explode("/", $pg);
                $p = end($p);
                $p = explode(".", $p);
                $page = $p[0];
                $pages = \DB::table('pages_tracking_codes')->select('footer_code', 'header_code')->where('project_name', $project_name)->where("page_name", $page)->first();
                if (count($pages) > 0) {
                    \DB::table('pages_tracking_codes')->where('project_name', $project_name)->where("page_name", $page)->update(['header_code' => $request['header_code'][$i], 'footer_code' => $request['footer_code'][$i]]);
                    $new_pg_content = str_replace($pages->header_code, '', str_replace($pages->footer_code, '', $pg_content));
                } else {
                    \DB::table('pages_tracking_codes')->insert(['page_name' => $request['page_name'][$i], 'project_name' => $project_name, 'header_code' => $request['header_code'][$i], 'footer_code' => $request['footer_code'][$i], 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
                    $new_pg_content = $pg_content;
                }
                $new_pg_content = str_replace('</body>', $request['footer_code'][$i] . '</body>', str_replace('</head>', $request['header_code'][$i] . '</head>', $new_pg_content));
                file_put_contents($pg, $new_pg_content);
                $i++;
            }
        }
        Alert::success('Success Message', 'Your settings updated successfully!')->autoclose(3000);
        return redirect()->back();
    }

    
   public function save_trackingcode(Request $request) {
        $user = Auth::user();
        $project_name = $request['project_name'];

        $project = \DB::table('projects')->where('projects.name', $project_name)->first();
        $directory = '../public/storage/projects/' . $user->id . '/' . $project->uuid . '/';
        if (glob($directory . "*.html") != false) {
            $i = 0;
            foreach (glob($directory . "*.html") as $pg) {
                $pg_content = file_get_contents($pg);
                $p = explode("/", $pg);
                $p = end($p);
                $p = explode(".", $p);
                $page = $p[0];
                $pages = \DB::table('pages_tracking_codes')->select('footer_code', 'header_code')->where('project_name', $project_name)->where("page_name", $page)->first();
                if (count($pages) > 0) {
                    \DB::table('pages_tracking_codes')->where('project_name', $project_name)->where("page_name", $page)->update(['header_code' => $request['header_code'][$i], 'footer_code' => $request['footer_code'][$i]]);
                    $new_pg_content = str_replace($pages->header_code, '', str_replace($pages->footer_code, '', $pg_content));
                } else {
                    \DB::table('pages_tracking_codes')->insert(['page_name' => $request['page_name'][$i], 'project_name' => $project_name, 'header_code' => $request['header_code'][$i], 'footer_code' => $request['footer_code'][$i], 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
                    $new_pg_content = $pg_content;
                }
                $new_pg_content = str_replace('</body>', $request['footer_code'][$i] . '</body>', str_replace('</head>', $request['header_code'][$i] . '</head>', $new_pg_content));
                file_put_contents($pg, $new_pg_content);
                $i++;
            }
        }
        Alert::success('Success Message', 'Your settings updated successfully!')->autoclose(3000);
        return redirect()->back();
    }
    
    
    
    
    public function checklist($id) {
        not_permissions_redirect(have_premission(10));
        $data['ProjectData'] = \DB::table('projects')->leftJoin('users_projects', 'projects.id', '=', 'users_projects.project_id')->select('projects.*', \DB::raw('(SELECT GROUP_CONCAT(domains.name) FROM domains WHERE domains.project_id = projects.id GROUP BY domains.project_id LIMIT 0,1) AS domain_names'), \DB::raw('(SELECT industries.title FROM industries WHERE industries.id = projects.ind_id) AS t_cat_name'))->where('projects.id', $id)->where('users_projects.user_id', Auth::user()->id)->first();
        $data['domains'] = \App\Domains::where('user_id', \Auth::user()->id)->where('status', 1)->get();
        $data['emaillists'] = \App\EmailLists::where('status', 1)->get();
        $data['user_id'] = \Auth::user()->id;
        return view('admin.pandapages.checklist')->with($data);
    }

    public function overview($id) {
        $data['ProjectData'] = \DB::table('projects')->select('projects.*', \DB::raw('(SELECT templates.name FROM templates WHERE templates.id = projects.template_id) AS temp_name'), \DB::raw('(SELECT templates.thumbnail FROM templates WHERE templates.id = projects.template_id) AS temp_thumbnail'), \DB::raw('(SELECT industries.title FROM industries WHERE industries.id = projects.ind_id) AS t_cat_name'))->where('projects.id', $id)->first();
        return view('admin.pandapages.overview')->with($data);
    }

    public function automation($id) {
        not_permissions_redirect(have_premission(15));
        $user = Auth::user();
        $data['ProjectData'] = \DB::table('projects')->leftJoin('users_projects', 'projects.id', '=', 'users_projects.project_id')->select('projects.*', \DB::raw('(SELECT GROUP_CONCAT(domains.name) FROM domains WHERE domains.project_id = projects.id GROUP BY domains.project_id LIMIT 0,1) AS domain_names'), \DB::raw('(SELECT industries.title FROM industries WHERE industries.id = projects.ind_id) AS t_cat_name'))->where('projects.id', $id)->where('users_projects.user_id', $user->id)->first();
        $data['user_id'] = $user->id;
        
        $data['emailsdata'] = \DB::table('automation_action_lists')->where('project_id', $id)->where('action_type', 'email')->first();
        $data['textdata'] = \DB::table('automation_action_lists')->where('project_id', $id)->where('action_type', 'text')->first();

        $actions = \DB::table('automation_actions')->where('project_id', $id)->select('form_name')->get();
        $a = [];
        foreach ($actions as $ac)
            $a[] = $ac->form_name;

        $data['actions'] = $a;
        //echo '<pre>';print_r($a);die;
        return view('admin.pandapages.automation')->with($data);
    }

    public function change_contacting_status($proj_id, $form, $action) {
        if ($action == 'add') {
            $array = array('project_id' => $proj_id, 'form_name' => str_replace('-', ' ', $form), 'is_active' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'));

            \DB::table('automation_actions')->insert($array);
        } else {
            \DB::table('automation_actions')->where('project_id', $proj_id)->where('form_name', str_replace('-', ' ', $form))->delete();
        }

        Alert::success('Success Message', 'Your form status updated successfully!')->autoclose(3000);

        return redirect()->back();
    }


    public function get_project_form(Request $request) {
        $input = $request->all();
        $pages_forms = \DB::table('pages')->select('forms')->where('pages.pageable_id', $input['project_id'])->where('pages.pageable_type', 'Project')->get();
        foreach ($pages_forms as $form) {
            $forms = explode(",", $form->forms);
            foreach ($forms as $fo) {
                if ($fo != "") {
                    echo '<p class="text-left"><i class="fa fa-caret-right"></i> ' . $fo . ' <span class="btn btn-success pull-right"> Confirm <i class="fa fa-check"></i></span></p><div class="clear5"></div>';
                }
            }
        }
        echo '<div class="clear10"></div><h3>Note this form? Please select the form from the dropdown below:</h3><div class="clear10"></div><select name="form_name" id="form_name" class="form-control required" ><option value="">Select Form</option>';
        foreach ($pages_forms as $form) {
            $forms = explode(",", $form->forms);
            foreach ($forms as $fo) {
                if ($fo != "") {
                    echo '<option  value="' . $fo . '">' . $fo . '</option>';
                }
            }
        }
        echo '</select>';
    }

    public function publishing($id) {
        $data['ProjectData'] = \DB::table('projects')->select('projects.*', \DB::raw('(SELECT templates.name FROM templates WHERE templates.id = projects.template_id) AS temp_name'), \DB::raw('(SELECT templates.thumbnail FROM templates WHERE templates.id = projects.template_id) AS temp_thumbnail'), \DB::raw('(SELECT industries.title FROM industries WHERE industries.id = projects.ind_id) AS t_cat_name'))->where('projects.id', $id)->first();
        return view('admin.pandapages.publishing')->with($data);
    }

    public function get_temp_cat(Request $request) {
        $input = $request->all();
        $where = " template_categories.status = 1 ";
        if (isset($input['industries'])) {
            $where .= " AND template_categories.id IN (SELECT industory_template_categories.t_cat_id FROM industory_template_categories WHERE industory_template_categories.ind_id IN (" . implode(', ', $input['industries']) . ")) ";
        }
        if (isset($input['categories'])) {
            $where .= " AND template_categories.id IN (SELECT category_template_categories.t_cat_id FROM category_template_categories WHERE category_template_categories.cat_id IN (" . implode(', ', $input['categories']) . ")) ";
        }
        $data['template_categories'] = \DB::table('template_categories')->select('template_categories.*')->whereRaw($where)->get();
        return view('admin.pandapages.partial_tc')->with($data);
    }

    public function GetPandaPage($id, $id2) {
        $user = \Auth::user();
        $unique_code = $user['unique_code'];
        if ($user['facebook_page_id'] != '') {
            $contect_messenger = '<a href="http://m.me/' . $user['facebook_page_id'] . '" style="font-size: 24px;color: #448AFF;text-decoration: none;background-color: transparent;padding: 4px 12px 8px 12px; border: 2px solid;border-radius: 8px;" class=""><img src="' . asset('public/frontend/images/messanger-icon.png') . '" style="height: 30px;width: 30px;"> Messenger</a>';
        } else {
            $contect_messenger = '';
        }
        $temp = $this->templateLoader->load($id);
        $project_last = \DB::table('projects')->orderby('id', 'desc')->first();
        if (count($project_last)) {
            $project_ref = $project_last->id + 1;
        } else {
            $project_ref = 1;
        }
        $pages = array();
        $p_name = generateRandomString(10);
        $uuid = generateRandomString(36);
        $base = '<head><base id="base" href="' . url('/') . '/storage/projects/' . $user['id'] . '/' . $uuid . '/">';
        $links_css = '<link rel="stylesheet" href="' . url('/') . '/storage/projects/' . $user['id'] . '/' . $uuid . '/css/framework.css?=' . $p_name . '" id="framework-css"><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" id="font-awesome"><link rel="stylesheet" href="' . url('/') . '/storage/projects/' . $user['id'] . '/' . $uuid . '/css/theme.css?=' . $p_name . '" id="theme-css"><link rel="stylesheet" href="' . url('/') . '/storage/projects/' . $user['id'] . '/' . $uuid . '/css/custom_elements.css?=' . $p_name . '" id="custom-elements-css"><link rel="stylesheet" href="' . url('/') . '/storage/projects/' . $user['id'] . '/' . $uuid . '/css/template.css?=' . $p_name . '" id="template-css"><link rel="stylesheet" href="' . url('/') . '/storage/projects/' . $user['id'] . '/' . $uuid . '/css/styles.css?=' . $p_name . '" id="custom-css"></head>';
        $links_js = '<script id="jquery" src="' . url('/') . '/storage/projects/' . $user['id'] . '/' . $uuid . '/js/jquery.min.js?=' . $p_name . '"></script><script id="framework-js" src="' . url('/') . '/storage/projects/' . $user['id'] . '/' . $uuid . '/js/framework.js?=' . $p_name . '"></script><script id="template-js" src="' . url('/') . '/storage/projects/' . $user['id'] . '/' . $uuid . '/js/template.js?=' . $p_name . '"></script><script id="custom-js" src="' . url('/') . '/storage/projects/' . $user['id'] . '/' . $uuid . '/js/scripts.js?=' . $p_name . '"></script><script async src="https://www.googletagmanager.com/gtag/js?id=UA-115314865-1"></script><script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag("js", new Date());gtag("config", "UA-115314865-1");</script></body>';
        foreach ($temp['pages'] as $key => $value) {
            $pages[$key]['name'] = $value['name'];
            $pages[$key]['html'] = str_replace('<head>', $base, str_replace('</head>', $links_css, str_replace('</body>', $links_js, str_replace('[PAGE_REF]', $value['name'], str_replace('[MESSAGEUSFBMESSENGER]', $contect_messenger, str_replace('#TWITTER#', $user['twitter'], str_replace('#GOOGLEPLUS#', $user['googleplus'], str_replace('#INSTAGRAM#', $user['instagram'], str_replace('#LINKEDIN#', $user['linkedin'], str_replace('#YOUTUBE#', $user['youtube'], str_replace('#PINTEREST#', $user['pinterest'], str_replace('#FACEBOOK#', $user['facebook'], str_replace('[PROJECT_REF]', $project_ref, str_replace('[UNIQUE_CODE]', $unique_code, $value['html']))))))))))))));
        }
        $temp['pages'] = $pages;


        $project = array(
            'uuid' => $uuid,
            'name' => $p_name,
            'framework' => $temp['config']['framework'],
            'pages' => $temp['pages'],
            'template' => $temp,
            'ind_id' => $id2,
            'id' => $project_ref,
            'forms' => $temp['config']['forms']
        );
        $this->repository->create($project);
        return redirect('admin/panda-pages/' . $p_name . '/get-pandapage');
    }

    public function GetPandaPage2($p_name) {
        $project = \DB::table('projects')->where('name', $p_name)->first();
        $id = $project->template;
        $id2 = $project->ind_id;
        $data['template'] = $this->templateLoader->load($id);
        $data['industry'] = \DB::table('industries')->where('id', $id2)->first();
        $data['project_edit'] = url('builder/' . $project->id);
        $data['project_link'] = url('sites/' . $p_name);
        return view('admin.pandapages.GetPandaPage')->with($data);
    }

    public function create_new_project($id, $id2) {
        $user = \Auth::user();
        
        $unique_code = $user['unique_code'];
        if ($user['facebook_page_id'] != '') {
            $contect_messenger = '<a href="http://m.me/' . $user['facebook_page_id'] . '" style="font-size: 24px;color: #448AFF;text-decoration: none;background-color: transparent;padding: 4px 12px 8px 12px; border: 2px solid;border-radius: 8px;" class=""><img src="' . asset('public/frontend/images/messanger-icon.png') . '" style="height: 30px;width: 30px;"> Messenger</a>';
        } else {
            $contect_messenger = '';
        }
        $temp = $this->templateLoader->load($id);
        //DB::setFetchMode(\PDO::FETCH_ASSOC);
        $project_last = \DB::table('projects')->orderby('id', 'desc')->first();
        $project_last = json_decode(json_encode($project_last), True);
//        echo '<pre>';
//        print_r($datas);
//        die();
        if (count($project_last) !== NULL) {
            $project_ref = $project_last['id'] + 1;
        } else {
            $project_ref = 1;
        }
        
        $pages = array();
        $p_name = generateRandomString(10);
        $uuid = generateRandomString(36);
        $base = '<head><base id="base" href="' . url('/') . '/storage/projects/' . $user['id'] . '/' . $uuid . '/">';
        $links_css = '<link rel="stylesheet" href="' . url('/') . '/storage/projects/' . $user['id'] . '/' . $uuid . '/css/framework.css?=' . $p_name . '" id="framework-css"><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" id="font-awesome"><link rel="stylesheet" href="' . url('/') . '/storage/projects/' . $user['id'] . '/' . $uuid . '/css/theme.css?=' . $p_name . '" id="theme-css"><link rel="stylesheet" href="' . url('/') . '/storage/projects/' . $user['id'] . '/' . $uuid . '/css/custom_elements.css?=' . $p_name . '" id="custom-elements-css"><link rel="stylesheet" href="' . url('/') . '/storage/projects/' . $user['id'] . '/' . $uuid . '/css/template.css?=' . $p_name . '" id="template-css"><link rel="stylesheet" href="' . url('/') . '/storage/projects/' . $user['id'] . '/' . $uuid . '/css/styles.css?=' . $p_name . '" id="custom-css"></head>';
        $links_js = '<script id="jquery" src="' . url('/') . '/storage/projects/' . $user['id'] . '/' . $uuid . '/js/jquery.min.js?=' . $p_name . '"></script><script id="framework-js" src="' . url('/') . '/storage/projects/' . $user['id'] . '/' . $uuid . '/js/framework.js?=' . $p_name . '"></script><script id="template-js" src="' . url('/') . '/storage/projects/' . $user['id'] . '/' . $uuid . '/js/template.js?=' . $p_name . '"></script><script id="custom-js" src="' . url('/') . '/storage/projects/' . $user['id'] . '/' . $uuid . '/js/scripts.js?=' . $p_name . '"></script><script async src="https://www.googletagmanager.com/gtag/js?id=UA-115314865-1"></script><script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag("js", new Date());gtag("config", "UA-115314865-1");</script></body>';
        foreach ($temp['pages'] as $key => $value) {
            $pages[$key]['name'] = $value['name'];
            $pages[$key]['html'] = str_replace('<head>', $base, str_replace('</head>', $links_css, str_replace('</body>', $links_js, str_replace('[PAGE_REF]', $value['name'], str_replace('[MESSAGEUSFBMESSENGER]', $contect_messenger, str_replace('#TWITTER#', $user['twitter'], str_replace('#GOOGLEPLUS#', $user['googleplus'], str_replace('#INSTAGRAM#', $user['instagram'], str_replace('#LINKEDIN#', $user['linkedin'], str_replace('#YOUTUBE#', $user['youtube'], str_replace('#PINTEREST#', $user['pinterest'], str_replace('#FACEBOOK#', $user['facebook'], str_replace('[PROJECT_REF]', $project_ref, str_replace('[UNIQUE_CODE]', $unique_code, $value['html']))))))))))))));
        }
        $temp['pages'] = $pages;


        $project = array(
            'uuid' => $uuid,
            'name' => $p_name,
            'framework' => $temp['config']['framework'],
            'pages' => $temp['pages'],
            'template' => $temp,
            'ind_id' => $id2,
            'id' => $project_ref,
            'forms' => $temp['config']['forms']
        );
        $this->repository->create($project);

        return redirect('builder/' . $project_ref);
    }

    public function add_seo_meta_data(Request $request) {
        $input = $request->all();
        unset($input['social_image']);
        if (isset($_FILES['social_image']['name'])) {
            $sourcePath = $_FILES['social_image']['tmp_name'];
            $temp_name = $_FILES['social_image']['name'];
            $temp_name_array = explode('.', $temp_name);
            $extention = end($temp_name_array);
            $file_name = 'socialImage-' . uniqid() . '.' . $extention;
            $path = 'uploads/social_images/' . $file_name;
            move_uploaded_file($sourcePath, $path);
            $input['social_image'] = $file_name;
        }
        $data = \DB::table('pages_seo_settings')->where('page_name', $input['page_name'])->where('project_name', $input['project_name'])->first();
        if (count($data)) {
            \DB::table('pages_seo_settings')->where('id', $data->id)->update($input);
        } else {
            $input['created_at'] = date('Y-m-d H:i:s');
            \DB::table('pages_seo_settings')->insert($input);
        }
        exit;
    }

    public function add_tracking_codes(Request $request) {
        $input = $request->all();
        $data = \DB::table('pages_tracking_codes')->where('page_name', $input['page_name'])->where('project_name', $input['project_name'])->first();
        if (count($data)) {
            \DB::table('pages_tracking_codes')->where('id', $data->id)->update($input);
        } else {
            $input['created_at'] = date('Y-m-d H:i:s');
            \DB::table('pages_tracking_codes')->insert($input);
        }
        exit;
    }

    public function add_integrations(Request $request) {
        $input = $request->all();
        $input['created_at'] = date('Y-m-d H:i:s');
        \DB::table('pages_email_lists')->insert($input);
        exit;
    }

    public function get_tracking_codes(Request $request) {
        $input = $request->all();
        $data = \DB::table('pages_tracking_codes')->where('page_name', $input['page_name'])->where('project_name', $input['project_name'])->first();
        echo json_encode($data);
        exit;
    }

    public function get_integrations(Request $request) {
        $input = $request->all();
        $data = \DB::table('pages_email_lists')->select('pages_email_lists.*', \DB::raw('(SELECT title FROM email_lists WHERE id = pages_email_lists.email_list_id) AS list_title'))->where('project_name', $input['project_name'])->get();
        echo json_encode($data);
        exit;
    }

    public function get_seo_meta_data(Request $request) {
        $input = $request->all();
        $data = \DB::table('pages_seo_settings')->where('page_name', $input['page_name'])->where('project_name', $input['project_name'])->first();
        echo json_encode($data);
        exit;
    }

    public function get_email_lists() {
        $email_lists = \DB::table('email_lists')->orderby('title', 'ASC')->get();
        foreach ($email_lists as $el) {
            echo '<option value="' . $el->id . '">' . $el->title . '</option>';
        }
        exit;
    }

    public function remove_integrations(Request $request) {
        $input = $request->all();
        \DB::table('pages_email_lists')->where('id', $input['id'])->delete();
    }

    public function build_site(Request $request) {
        $input = $request->all();

        $templates = $this->templateLoader->loadAll();

        $_SESSION['industry_id'] = $input['industry_id'];
        $_SESSION['category_id'] = $input['category_id'];
        $_SESSION['featured_value'] = $input['featured_value'];
        $templates = $templates->filter(function($template) {
            if (isset($template['config']['industries']) && in_array($_SESSION['industry_id'], $template['config']['industries']) && isset($template['config']['t_cat_id']) && in_array($_SESSION['category_id'], $template['config']['t_cat_id']) && isset($template['config']['features'])) {
                $features_values = explode(",", $_SESSION['featured_value']);
                foreach ($features_values as $fv) {
                    if (in_array($fv, $template['config']['features'])) {
                        return 1;
                    }
                }
            }
        });

        if (count($templates)) {
            $user = \Auth::user();
            $unique_code = $user['unique_code'];
            if ($user['facebook_page_id'] != '') {
                $contect_messenger = '<a href="http://m.me/' . $user['facebook_page_id'] . '" style="font-size: 24px;color: #448AFF;text-decoration: none;background-color: transparent;padding: 4px 12px 8px 12px; border: 2px solid;border-radius: 8px;" class=""><img src="' . asset('public/frontend/images/messanger-icon.png') . '" style="height: 30px;width: 30px;"> Messenger</a>';
            } else {
                $contect_messenger = '';
            }
            $template = $templates[0];
            $temp = $this->templateLoader->load($template['name']);
            $project_last = \DB::table('projects')->orderby('id', 'desc')->first();
            if (count($project_last)) {
                $project_ref = $project_last->id + 1;
            } else {
                $project_ref = 1;
            }
            $pages = array();
            $p_name = generateRandomString(10);
            $uuid = generateRandomString(36);
            $base = '<head><base id="base" href="' . url('/') . '/storage/projects/' . $user['id'] . '/' . $uuid . '/">';
            $links_css = '<link rel="stylesheet" href="' . url('/') . '/storage/projects/' . $user['id'] . '/' . $uuid . '/css/framework.css?=' . $p_name . '" id="framework-css"><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" id="font-awesome"><link rel="stylesheet" href="' . url('/') . '/storage/projects/' . $user['id'] . '/' . $uuid . '/css/theme.css?=' . $p_name . '" id="theme-css"><link rel="stylesheet" href="' . url('/') . '/storage/projects/' . $user['id'] . '/' . $uuid . '/css/custom_elements.css?=' . $p_name . '" id="custom-elements-css"><link rel="stylesheet" href="' . url('/') . '/storage/projects/' . $user['id'] . '/' . $uuid . '/css/template.css?=' . $p_name . '" id="template-css"><link rel="stylesheet" href="' . url('/') . '/storage/projects/' . $user['id'] . '/' . $uuid . '/css/styles.css?=' . $p_name . '" id="custom-css"></head>';
            $links_js = '<script id="jquery" src="' . url('/') . '/storage/projects/' . $user['id'] . '/' . $uuid . '/js/jquery.min.js?=' . $p_name . '"></script><script id="framework-js" src="' . url('/') . '/storage/projects/' . $user['id'] . '/' . $uuid . '/js/framework.js?=' . $p_name . '"></script><script id="template-js" src="' . url('/') . '/storage/projects/' . $user['id'] . '/' . $uuid . '/js/template.js?=' . $p_name . '"></script><script id="custom-js" src="' . url('/') . '/storage/projects/' . $user['id'] . '/' . $uuid . '/js/scripts.js?=' . $p_name . '"></script><script async src="https://www.googletagmanager.com/gtag/js?id=UA-115314865-1"></script><script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag("js", new Date());gtag("config", "UA-115314865-1");</script></body>';
            foreach ($temp['pages'] as $key => $value) {
                $pages[$key]['name'] = $value['name'];
                $pages[$key]['html'] = str_replace('<head>', $base, str_replace('</head>', $links_css, str_replace('</body>', $links_js, str_replace('[PAGE_REF]', $value['name'], str_replace('[MESSAGEUSFBMESSENGER]', $contect_messenger, str_replace('#TWITTER#', $user['twitter'], str_replace('#GOOGLEPLUS#', $user['googleplus'], str_replace('#INSTAGRAM#', $user['instagram'], str_replace('#LINKEDIN#', $user['linkedin'], str_replace('#YOUTUBE#', $user['youtube'], str_replace('#PINTEREST#', $user['pinterest'], str_replace('#FACEBOOK#', $user['facebook'], str_replace('PandaSite', $input['business_name'], str_replace('[PROJECT_REF]', $project_ref, str_replace('[UNIQUE_CODE]', $unique_code, $value['html'])))))))))))))));
            }
            $temp['pages'] = $pages;
            $project = array(
                'uuid' => $uuid,
                'name' => $p_name,
                'framework' => $temp['config']['framework'],
                'pages' => $temp['pages'],
                'template' => $temp,
                'ind_id' => $input['industry_id'],
                'id' => $project_ref,
                'forms' => $temp['config']['forms']
            );
            $this->repository->create($project);
            echo url('builder/' . $project_ref);
        } else {
            echo '0';
            exit;
        }
        exit;
    }

}
