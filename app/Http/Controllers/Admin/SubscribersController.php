<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\NewsLetter;
use Illuminate\Http\Request;
use Alert;

class SubscribersController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index() {
        not_permissions_redirect(have_premission(87));
        $data['demos'] = NewsLetter::Orderby('id', 'DESC')->paginate(20);
        $data['total'] = NewsLetter::Orderby('id', 'DESC')->count();
        return view('admin.subscribers.index')->with($data);
    }

    public function destroy($id) {
        not_permissions_redirect(have_premission(88));
        NewsLetter::destroy($id);
        Alert::success('Success Message', 'Subscriber deleted successfully!')->autoclose(3000);
        return redirect('admin/subscribers');
    }

}
