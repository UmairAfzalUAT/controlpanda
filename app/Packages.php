<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Packages extends Model {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'no_of_funnels', 'no_of_pages', 'no_of_visitors', 'no_of_contacts', 'custom_domains', 'a_b_split_tests', 'email_integrations', 'optin_funnels', 'clickpops', 'clickoptin', 'all_advanced_funnels', 'sales_funnels', 'membership_funnels', 'no_of_members', 'auto_webinar_funnels', 'webinar_funnels', 'hangout_funnels', 'order_pages', 'upsell_pages', 'downsale_pages', 'share_your_funnels', 'priority_support', 'priority_template_requests', 'div_class', 'price', 'is_active'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

}
